﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using solarcrm.Configure;
using solarcrm.Startup;
using solarcrm.Test.Base;

namespace solarcrm.GraphQL.Tests
{
    [DependsOn(
        typeof(solarcrmGraphQLModule),
        typeof(solarcrmTestBaseModule))]
    public class solarcrmGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(solarcrmGraphQLTestModule).GetAssembly());
        }
    }
}