﻿using System.Threading.Tasks;
using solarcrm.Security.Recaptcha;

namespace solarcrm.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
