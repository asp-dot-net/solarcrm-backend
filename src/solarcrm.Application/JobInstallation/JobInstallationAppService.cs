﻿using Abp.Domain.Repositories;
using solarcrm.DataVaults.StockItem;
using solarcrm.JobInstallation.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using System.Data;
using Newtonsoft.Json;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using Abp.Timing.Timezone;
using Abp.Runtime.Session;
using solarcrm.Job;
using solarcrm.DataVaults.Leads;
using solarcrm.Jobs.Dto;
using Stripe;
using Abp.Authorization;
using solarcrm.Authorization;
using Abp.AutoMapper;

namespace solarcrm.JobInstallation
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Installation)]
    public class JobInstallationAppService : solarcrmAppServiceBase, IJobInstallationAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<JobInstallationDetail> _jobInstallationDetailRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<Leads> _leadRepository;
        private readonly UserManager _userManager;

        public JobInstallationAppService(IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<JobInstallationDetail> jobInstallationDetailRepository, IRepository<Job.Jobs> jobRepository, IRepository<LeadActivityLogs> leadactivityRepository, IRepository<JobProductItem> jobProductItemRepository, IAbpSession abpSession, UserManager userManager, ITimeZoneConverter timeZoneConverter, IRepository<User, long> userRepository, IRepository<Leads> leadRepository)
        {
            _dbContextProvider = dbContextProvider;
            _jobInstallationDetailRepository = jobInstallationDetailRepository;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _userRepository = userRepository;
            _timeZoneConverter = timeZoneConverter;
            _jobProductItemRepository = jobProductItemRepository;
            _abpSession = abpSession;
            _userManager = userManager;
            _leadRepository = leadRepository;
        }


        public List<InstallationCalendarDto> GetInstallationCalendar(int installerId, DateTime CalendarStartDate, int? orgID, string areaNameFilter, string state)
        {
            var dates = new List<DateTime>();
            var startDate = (_timeZoneConverter.Convert(CalendarStartDate, (int)AbpSession.TenantId));
            var endDate = startDate.Value.AddDays(6).Date;
            for (var dt = startDate; dt <= endDate; dt = dt.Value.AddDays(1))
            {
                dates.Add(dt.Value.Date);
            }
            var installationList = (from dt in dates
                                    select new InstallationCalendarDto
                                    {
                                        InstallationDate = dt.Date,
                                        //InstallationCount = _jobRepository.GetAll()
                                        //        .WhereIf(installerId > 0, x => x.InstallerId == installerId)
                                        //        .WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFk.State == state)
                                        //        .WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFk.Area == areaNameFilter)
                                        //        .Where(x => x.InstallationDate.Value.Date == Convert.ToDateTime(dt.Date.ToShortDateString()) && x.LeadFk.OrganizationId == orgID).Count(),

                                        Jobs = (from jobs in _jobInstallationDetailRepository.GetAll().Include(x => x.JobFKId.LeadFK).Include(x => x.LocationFKId).Include(x => x.JobFKId.LeadFK.CityIdFk).Include(x => x.JobFKId.LeadFK.StateIdFk)
                                                    .Include(x => x.JobFKId.LeadFK.TalukaIdFk).Include(x => x.JobFKId.LeadFK.DistrictIdFk)
                                                    .Where(x => x.InstallStartDate.Value.Date == Convert.ToDateTime(dt.Date.ToShortDateString()))
                                                    .WhereIf(installerId > 0, x => x.JobInstallerID == installerId)
                                                    //.WhereIf(!string.IsNullOrWhiteSpace(areaNameFilter), e => e.LeadFk.Area == areaNameFilter)
                                                    //.WhereIf(!string.IsNullOrWhiteSpace(state), e => e.LeadFk.State == state)
                                                    //.WhereIf(orgID != 0, x => x.JobFKId.OrganizationUnitId == orgID)
                                                select new InstallationJobsDto
                                                {
                                                    JobId = jobs.Id,
                                                    LeadId = jobs.JobFKId.LeadId,
                                                    JobNumber = jobs.JobFKId.JobNumber,

                                                    //PostalCode = jobs.PostalCode,
                                                    // State = jobs.State,
                                                    Address = (jobs.JobFKId.LeadFK.AddressLine1 == null ? "" : jobs.JobFKId.LeadFK.AddressLine1 + ",") + (jobs.JobFKId.LeadFK.AddressLine2 == null ? "" : jobs.JobFKId.LeadFK.AddressLine2 + ",") + (jobs.JobFKId.LeadFK.CityIdFk.Name == "" ? "" : jobs.JobFKId.LeadFK.CityIdFk.Name + "") + (jobs.JobFKId.LeadFK.TalukaIdFk.Name == null ? "" : jobs.JobFKId.LeadFK.TalukaIdFk.Name + ",") + (jobs.JobFKId.LeadFK.DistrictIdFk.Name == null ? "" : jobs.JobFKId.LeadFK.DistrictIdFk.Name + ",") + (jobs.JobFKId.LeadFK.StateIdFk.Name == null ? "" : jobs.JobFKId.LeadFK.StateIdFk.Name + ",") + (jobs.JobFKId.LeadFK.Pincode == 0 ? null : Convert.ToString(jobs.JobFKId.LeadFK.Pincode)),
                                                    InstallationTime = jobs.InstallStartDate,
                                                    InstallerName = _userRepository.GetAll().Where(x => x.Id == jobs.JobInstallerID).FirstOrDefault().FullName,
                                                    CustomerName = jobs.JobFKId.LeadFK.CustomerName //_leadRepository.GetAll().Where(x => x.Id == jobs.LeadId).FirstOrDefault().CompanyName
                                                }).ToList(),
                                        InstallationCount = 0 //Jobs.Dto.Sum(x => x.InstallationCount),  
                                    });


            return new List<InstallationCalendarDto>(
                installationList.ToList()
            );
        }
        //public async Task<List<GetInstallerMapDto>> GetInstallerMap(GetAllInputMapDto input)
        //{
        //    //var InstallerUser = await _userRepository.GetAllListAsync();
        //    try
        //    {

        //        var InstallerUser = _userRepository.GetAll().Select(e => e.Id).ToList();
        //        //var installerpostcode = _installerDetailRepository.GetAll().Where(j => InstallerUser.Contains(j.UserId)).Select(e => Convert.ToInt32(e.PostCode)).ToList();

        //        //List<string> = (from u in InstallerUser
        //        //                join o1 in _installerDetailRepository.GetAll() on u.Id equals o1.UserId
        //        //                where (o1 != null && o1.IsInst == true)
        //        //                select new
        //        //                {
        //        //                    postalcodes = o1.PostCode
        //        //                }).ToList();

        //        var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //        var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //        var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //        IList<string> role = await _userManager.GetRolesAsync(User);
        //        //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).ToList();
        //        //var UserList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).Distinct().ToList();

        //        var PanelDetails = new List<int?>();
        //        if (input.Panelmodel != null)
        //        {
        //            //var Panel = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 1 && e.Model.Contains(input.Panelmodel)).Select(e => e.Id).FirstOrDefault();
        //            PanelDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Panel).Select(e => e.JobId).ToList();
        //        }

        //        //var InvertDetails = new List<int?>();
        //        //if (input.Invertmodel != null)
        //        //{
        //        //    var Inverter = _productItemRepository.GetAll().Where(e => e.ProductTypeId == 2 && e.Model.Contains(input.Invertmodel)).Select(e => e.Id).FirstOrDefault();
        //        //    InvertDetails = _jobProductItemRepository.GetAll().Where(j => j.ProductItemId == Inverter).Select(e => e.JobId).ToList();
        //        //}

        //       // bool GrantedPermissionNames = _userPermissionRepository.GetAll().Where(e => e.Name == "Pages.JobBooking" && e.UserId == AbpSession.UserId && e.IsGranted == true).Any();

        //        DateTime currentDate = DateTime.Now;
        //        DateTime currentnewDate = currentDate.Date.AddDays(-30);


        //        var filteredJobs = _jobRepository.GetAll()
        //            .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.JobNumber.Contains(input.FilterText) || e.LeadFK.MobileNumber.Contains(input.FilterText) || e.LeadFK.EmailId.Contains(input.FilterText) || e.LeadFK.Alt_Phone.Contains(input.FilterText) || e.LeadFK.CustomerName.Contains(input.FilterText))
        //            //.WhereIf(input.HousetypeId != 0, e => e.HouseTypeId == input.HousetypeId)
        //            //.WhereIf(input.Rooftypeid != 0, e => e.RoofTypeId == input.Rooftypeid)
        //            //.WhereIf(input.Jobstatusid != 0, e => e.JobStatusId == input.Jobstatusid)
        //            //.WhereIf(input.Paymenttypeid != 0, e => e.PaymentOptionId == input.Paymenttypeid)
        //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeFrom), e => String.Compare(e.LeadFK.Pincode.ToString(), input.PostalCodeFrom) >= 0)
        //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostalCodeTo), e => String.Compare(e.LeadFK.Pincode.ToString(), input.PostalCodeTo) <= 0)
        //            /////e => e.PostalCode >= input.PostalCodeFrom && e.PostalCode <= input.PostalCodeTo)
        //            .WhereIf(!string.IsNullOrWhiteSpace(input.Panelmodel), e => PanelDetails.Contains(e.Id))
        //            //.WhereIf(!string.IsNullOrWhiteSpace(input.Invertmodel), e => InvertDetails.Contains(e.Id))
        //            //.WhereIf(!string.IsNullOrWhiteSpace(input.State), e => e.State == input.State)
        //            //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.LeadFk.Area == input.AreaNameFilter)
        //            //.WhereIf(!string.IsNullOrWhiteSpace(input.SteetAddressFilter), e => e.LeadFk.Address == input.SteetAddressFilter)
        //            //.WhereIf(input.DateFilterType == "Active" && input.StartDate != null && input.EndDate != null, e => e.ActiveDate.Value.AddHours(10).Date >= SDate.Value.Date && e.ActiveDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //            //.WhereIf(input.DateFilterType == "DepositeReceived" && input.StartDate != null && input.EndDate != null, e => e.DepositeRecceivedDate.Value.AddHours(10).Date >= SDate.Value.Date && e.DepositeRecceivedDate.Value.AddHours(10).Date <= EDate.Value.Date)
        //            //.WhereIf(input.Jobstatusid == 0 && input.Priority == 0, e => e.JobStatusId == 4 || e.JobStatusId == 5)
        //            //.WhereIf(input.Priority == 1, e => e.JobStatusId == 5 && e.ActiveDate <= currentnewDate)
        //            //.WhereIf(input.Priority == 2, e => e.JobStatusId == 4 || (e.JobStatusId == 5 && e.ActiveDate >= currentnewDate))
        //            //.WhereIf(!role.Contains("Admin") && !role.Contains("Installation Manager") && GrantedPermissionNames, e => e.BookingManagerId == AbpSession.UserId)
        //            .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit);

        //        var jobs = (from o in filteredJobs

        //                    join o6 in _leadRepository.GetAll() on o.LeadId equals o6.Id into j6
        //                    from s6 in j6.DefaultIfEmpty()

        //                    let test = getDistanceFromLatLonInKm(o.LeadFK.Latitude == null ? s6.Latitude : o.LeadFK.Latitude, o.LeadFK.Longitude == null ? s6.Longitude : o.LeadFK.Longitude,"statte")
        //                    let Icons = getIcon(o.JobStatusId, o.ActiveDate.Value.Date)
        //                    let nearest2postcode = getNearest(o.PostalCode, installerpostcode)
        //                    let first = nearest2postcode.First()
        //                    let last = nearest2postcode.Last()
        //                    select new GetInstallerMapDto()
        //                    {
        //                        LeadId = s6.Id,
        //                        label = o.JobNumber,
        //                        Company = s6.CompanyName + " - ",
        //                        Status = o.JobStatusFk.Name,
        //                        Adress = o.Address + " " + o.Suburb + " " + o.State + " " + o.PostalCode,
        //                        State = o.State,
        //                        PhoneEmail = s6.Mobile + '/' + s6.Email,
        //                        DepRecDate = o.DepositeRecceivedDate,
        //                        ActiveDate = o.ActiveDate,
        //                        HouseType = o.HouseTypeFk.Name,
        //                        RoofType = o.RoofTypeFk.Name,
        //                        AreaType = o.LeadFk.Area,
        //                        Latitude = o.Latitude == null ? s6.latitude : o.Latitude,
        //                        Longitude = o.Longitude == null ? s6.longitude : o.Longitude,
        //                        //Icon = o.JobStatusId == 4 ? o.ActiveDate.Value.Date <= newDate ? o.JobStatusId == 6 ? JobBook :  icon3 : DepRcv : Active,
        //                        Icon = Icons,
        //                        Distance = test,
        //                        ProductIteams = (from o in _jobProductItemRepository.GetAll().Where(e => e.JobId == o.Id)
        //                                         join o2 in _lookup_productItemRepository.GetAll() on o.ProductItemId equals o2.Id
        //                                         select new GetJobProductItemForEditOutput()
        //                                         {
        //                                             JobNote = o2.ProductTypeFk.Name,
        //                                             ProductItemName = o2.Name,
        //                                             Model = o2.Model,
        //                                             Quantity = o.Quantity
        //                                         }).ToList(),
        //                        postalcodesss = nearest2postcode,
        //                        //FirstNearestInstallerName = firstuser,
        //                        fistpostalcode = first,
        //                        secondpostalcode = last,
        //                        Id = o.Id,
        //                        JobStatusId = o.JobStatusId,
        //                    }).ToList();

        //        foreach (var item in jobs)
        //        {
        //            item.FirstNearestInstallerName = _userRepository.GetAll().Where(e => e.Id == (_installerDetailRepository.GetAll().Where(j => j.PostCode == item.fistpostalcode).Select(j => j.UserId).Distinct().FirstOrDefault())).Select(e => e.FullName).FirstOrDefault();
        //            item.SecondNearestInstallerName = _userRepository.GetAll().Where(e => e.Id == (_installerDetailRepository.GetAll().Where(j => j.PostCode == item.secondpostalcode).Select(j => j.UserId).Distinct().FirstOrDefault())).Select(e => e.FullName).FirstOrDefault();
        //            item.TotalMapData = jobs.Where(e => e.JobStatusId == 4 || e.JobStatusId == 5 || e.JobStatusId == 6).Select(e => e.LeadId).Count();
        //            item.TotalDepRcvMapData = jobs.Where(e => e.JobStatusId == 4 && e.Longitude != null && e.Latitude != null).Select(e => e.LeadId).Count();
        //            item.TotalActiveMapData = jobs.Where(e => e.JobStatusId == 5 && e.Longitude != null && e.Latitude != null).Select(e => e.LeadId).Count();
        //            item.TotalJobBookedMapData = jobs.Where(e => e.JobStatusId == 6 && e.Longitude != null && e.Latitude != null).Select(e => e.LeadId).Count();
        //            item.TotalJobInstallMapData = jobs.Where(e => e.JobStatusId == 8 && e.Longitude != null && e.Latitude != null).Select(e => e.LeadId).Count();
        //            item.Other = jobs.Where(e => e.Longitude == null && e.Latitude == null).Select(e => e.LeadId).Count();
        //            item.ListOfmissingLatlong = jobs.Where(e => e.Longitude == null && e.Latitude == null).Select(e => e.label).ToList();
        //        }

        //        return jobs;
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}
        private static string getIcon(int? jobstatusid, DateTime? Date)
        {
            DateTime currentDate = DateTime.Now;
            DateTime newDate = currentDate.Date.AddDays(-30);

            var icon3 = "https://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
            var Active = "https://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
            var DepRcv = "https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
            var JobBook = "https://www.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png";
            var JobInstalled = "https://www.google.com/intl/en_us/mapfiles/ms/micons/orange-dot.png";

            var Result = "";
            if (jobstatusid == 4)
            {
                Result = DepRcv;
            }
            if (jobstatusid == 5)
            {
                if (Date <= newDate)
                {
                    Result = icon3;
                }
                else
                {
                    Result = Active;
                }
            }
            if (jobstatusid == 6)
            {
                Result = JobBook;
            }
            if (jobstatusid == 8)
            {
                Result = JobInstalled;
            }

            return Result;
        }
        private static double getDistanceFromLatLonInKm(string lat1, string lon1, string States)
        {
            if (lat1 != "" && lon1 != "")
            {
                var R = 6371; // Radius of the earth in km
                var lat2 = 0.0;
                var lon2 = 0.0;

                if (States == "QLD")
                {
                    lat2 = -27.580041680084136;
                    lon2 = 153.03702238177127;
                }
                if (States == "VIC")
                {
                    lat2 = -37.81759262390686;
                    lon2 = 144.8550323686331;
                }
                if (States == "NSW")
                {
                    lat2 = -33.74722076106781;
                    lon2 = 150.90180287033405;
                }
                if (States == "SA")
                {
                    lat2 = -34.828917615461556;
                    lon2 = 138.600999853177;
                }
                if (States == "WA")
                {
                    lat2 = -32.06865473895976;
                    lon2 = 115.9103034972597;
                }

                var d1 = Convert.ToDouble(lat1) * (Math.PI / 180.0);
                var num1 = Convert.ToDouble(lon1) * (Math.PI / 180.0);
                var d2 = lat2 * (Math.PI / 180.0);
                var num2 = lon2 * (Math.PI / 180.0) - num1;
                var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) +
                         Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);
                var ans = 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
                return Math.Round((ans / 1000), 2);
            }
            else
            {
                return 0.00;
            }
            //var dLat = deg2rad(lat2 - Convert.ToDouble(lat1));  // deg2rad below
            //var dLon = deg2rad(lon2 - Convert.ToDouble(lon1));
            //var a =
            //  Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            //  Math.Cos(deg2rad(Convert.ToDouble(lat1))) * Math.Cos(deg2rad(lat2)) *
            //  Math.Sin(dLon / 2) * Math.Cos(dLon / 2);
            //var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            //var d = R * c; // Distance in km

            //return Math.Round((d / 1000),2);
        }

        private static List<string> getNearest(string post, List<int> installerpostcode)
        {
            var s = Convert.ToInt32(post);
            var Listpostcodes = installerpostcode.OrderBy(x => Math.Abs(x - s)).Take(2).ToList();
            var nearest2postcode = (from i in Listpostcodes select i.ToString()).ToList();
            return nearest2postcode;
        }
        public async Task<GetJobInstallForEditOutput> GetJobInstallById(int jobid)
        {
            var output = new GetJobInstallForEditOutput();
            var jobInstallerItem = _jobInstallationDetailRepository.GetAll().Include(x => x.JobFKId).Where(x => x.JobId == jobid).FirstOrDefault();



            if (jobInstallerItem != null)
            {

                output = ObjectMapper.Map<GetJobInstallForEditOutput>(jobInstallerItem);
                output.JobId = Convert.ToInt32(jobid);
                output.InstallerNotes = Convert.ToString(jobInstallerItem.JobFKId.InstallerNotes);
            }
            return output;
        }


        public async Task CreateOrEdit(CreateOrEditJobInstallDto input)
        {
            if (input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        
        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Installation_Create)]
        protected virtual async Task Create(CreateOrEditJobInstallDto input)
        {
            var jobinstaller = ObjectMapper.Map<JobInstallationDetail>(input);
            if (jobinstaller != null && jobinstaller.JobId != 0)
            {
                var jobdata = await _jobRepository.FirstOrDefaultAsync(input.JobId);
                if (jobdata != null)
                {
                    jobdata.InstallerNotes = Convert.ToString(input.InstallerNotes);

                    var JobId = await _jobRepository.UpdateAsync(jobdata);
                    if (JobId.Id != 0)
                    {
                        jobinstaller.StoreLocationId = input.StoreLocationId;
                        await _jobInstallationDetailRepository.InsertAndGetIdAsync(jobinstaller);

                        //Add Activity Log Installer
                        LeadActivityLogs leadactivity = new LeadActivityLogs();
                        leadactivity.LeadActionId = 13; // Job Installation
                        //leadactivity.LeadAcitivityID = 0; // confusion
                        leadactivity.SectionId = 52;
                        leadactivity.LeadActionNote = "Job Installation Created";
                        leadactivity.LeadId = jobdata.LeadId;
                        leadactivity.OrganizationUnitId = jobdata.OrganizationUnitId;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        await _leadactivityRepository.InsertAsync(leadactivity);

                    }
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Installation_Edit)]
        protected virtual async Task Update(CreateOrEditJobInstallDto input)
        {
            int leadid = 0;

            var jobdateinst = await _jobInstallationDetailRepository.GetAll().Include(x => x.JobFKId).Where(x => x.JobId == input.JobId && x.Id == input.Id).FirstOrDefaultAsync();
            JobInstallationDetail InstallationOldData = new JobInstallationDetail();
            InstallationOldData = jobdateinst.Copy();
            if (jobdateinst != null)
            {
                leadid = jobdateinst.JobFKId.LeadId;
                //jobdateinst = ObjectMapper.Map<JobInstallationDetail>(input);
                string InstallNotes = "";
                if (jobdateinst.Id != 0)
                {
                    var jobdata = await _jobRepository.FirstOrDefaultAsync(input.JobId);
                    if (jobdata != null)
                    {
                        InstallNotes = jobdata.InstallerNotes;
                        jobdata.InstallerNotes = Convert.ToString(input.InstallerNotes);

                        var JobId = await _jobRepository.UpdateAsync(jobdata);
                        if (JobId.Id != 0)
                        {
                            jobdateinst.StoreLocationId = input.StoreLocationId;
                            jobdateinst.JobInstallerID = input.JobInstallerID;
                            jobdateinst.InstallStartDate = input.InstallStartDate;
                            jobdateinst.InstallCompleteDate = input.InstallCompleteDate;
                            var JobInstallId = await _jobInstallationDetailRepository.UpdateAsync(jobdateinst);

                            #region Activity Log
                            //Add Activity Log
                            LeadActivityLogs leadactivity = new LeadActivityLogs();
                            leadactivity.LeadActionId = 13;
                            //leadactivity.LeadAcitivityID = 0;
                            leadactivity.SectionId = 52;
                            leadactivity.LeadActionNote = "Job Installation Modified";
                            leadactivity.LeadId = leadid;
                            if (AbpSession.TenantId != null)
                            {
                                leadactivity.TenantId = (int)AbpSession.TenantId;
                            }
                            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);

                            var List = new List<LeadtrackerHistory>();
                            if (InstallationOldData.InstallStartDate != input.InstallStartDate)
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = "InstallStartDate";
                                objAuditInfo.PrevValue = InstallationOldData.InstallStartDate.ToString();
                                objAuditInfo.CurValue = input.InstallStartDate.ToString();
                                objAuditInfo.Action = "Edit";
                                objAuditInfo.LeadId = jobdateinst.JobFKId.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                            if (InstallationOldData.StoreLocationId != input.StoreLocationId)
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = "StoreLocationId";
                                objAuditInfo.PrevValue = InstallationOldData.StoreLocationId.ToString();
                                objAuditInfo.CurValue = jobdateinst.StoreLocationId.ToString();
                                objAuditInfo.Action = "Edit";
                                objAuditInfo.LeadId = jobdateinst.JobFKId.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                            if (InstallationOldData.JobInstallerID != input.JobInstallerID)
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = "JobInstallerID";
                                objAuditInfo.PrevValue = InstallationOldData.JobInstallerID.ToString();
                                objAuditInfo.CurValue = jobdateinst.JobInstallerID.ToString();
                                objAuditInfo.Action = "Edit";
                                objAuditInfo.LeadId = jobdateinst.JobFKId.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                            if (InstallNotes != input.InstallerNotes)
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = "JobInstallerID";
                                objAuditInfo.PrevValue = InstallationOldData.JobInstallerID.ToString();
                                objAuditInfo.CurValue = jobdateinst.JobInstallerID.ToString();
                                objAuditInfo.Action = "Edit";
                                objAuditInfo.LeadId = jobdateinst.JobFKId.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                            if (InstallationOldData.InstallCompleteDate != input.InstallCompleteDate)
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = "InstallCompleteDate";
                                objAuditInfo.PrevValue = InstallationOldData.InstallCompleteDate.ToString();
                                objAuditInfo.CurValue = input.InstallCompleteDate.ToString();
                                objAuditInfo.Action = "Edit";
                                objAuditInfo.LeadId = jobdateinst.JobFKId.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                            await _dbContextProvider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
                            await _dbContextProvider.GetDbContext().SaveChangesAsync();
                            #endregion
                        }
                    }                                
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_jobs_Installation_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _jobInstallationDetailRepository.DeleteAsync(input.Id);
        }
    }
}
