﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Net.Mail;
using Abp.Organizations;
using Microsoft.AspNetCore.Hosting;
using solarcrm.Authorization.Roles;
using solarcrm.Authorization.Users;
using solarcrm.Common;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.Variation;
using solarcrm.EntityFrameworkCore;
using solarcrm.Job;
using solarcrm.Job.JobVariation;
using solarcrm.Job.Quotation;
using solarcrm.MultiTenancy;
using solarcrm.Notifications;
using solarcrm.Organizations;
using solarcrm.Quotation;
using solarcrm.Quotations.Dtos;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using solarcrm.Jobs;
using solarcrm.DataVaults.Price;
using Newtonsoft.Json;
using solarcrm.DataVaults.StockItem;
using Api2Pdf;
using System.Web;
using Abp.Runtime.Security;
using Abp.Notifications;
using solarcrm.Configuration;
using Abp.Authorization;
using solarcrm.Authorization;

namespace solarcrm.Quotations
{
    public class QuotationsAppService : solarcrmAppServiceBase, IQuotationsAppService
    {
        private readonly IRepository<QuotationData> _quotationRepository;
        private readonly IRepository<QuotationDataDetails> _quotationDataDetailRepqository;
        private readonly IRepository<Prices> _priceRepqository;
        private readonly IRepository<QuotationDataDetailProduct> _quotationDataDetailProductRepqository;
        private readonly IRepository<StockItem> _stockItemRepqository;

        private readonly IRepository<QuotationDataDetailVariations> _quotationDataDetailVariationRepqository;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<Job.Jobs, int> _jobRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        // private readonly IRepository<ProductItem> _ProductItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<QuotationLinkHistory> _quotationLinkHistoryRepository;       
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<OrganizationUnit, long> _OrganizationRepository;
        private readonly IRepository<BankDetailsOrganization> _bankOrganizationDetailsRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IAppConfigurationAccessor _configurationAccessor;
        // private readonly IRepository<QuotationData> _quotationDataRepqository;
        private readonly IJobAppService _jobAppService;
        //private readonly IRepository<QuotationQunityAndModelDetail> _quotationQunityAndModelDetailRepqository;
        //private readonly IRepository<QuotationTemplate> _quotationTemplateRepqository;
        //private readonly IReportAppService _reportAppService;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public QuotationsAppService(IRepository<QuotationData> quotationRepository,
           IBinaryObjectManager binaryObjectManager,
           ITempFileCacheManager tempFileCacheManager,
           IRepository<Job.Jobs, int> lookup_jobRepository
          , IRepository<QuotationDataDetailVariations> quotationDataDetailVariationRepqository
           , IRepository<QuotationDataDetails> quotationDataDetailRepqository
       , IRepository<QuotationDataDetailProduct> quotationDataDetailProductRepqository
            , IRepository<Prices> priceRepqository
           , IRepository<User, long> userRepository
           , IRepository<LeadActivityLogs> leadactivityRepository
           , IAppFolders appFolders
            , IJobAppService jobAppService
           , IEmailSender emailSender
           , IRepository<Leads> leadRepository
           , IWebHostEnvironment env
           , IRepository<Tenant> tenantRepository
           , IRepository<JobProductItem> jobProductItemRepository
           , IRepository<Variation> variationRepository
            , IRepository<StockItem> stockItemRepqository
           , IRepository<JobVariation> jobVariationRepository
           , IRepository<UserRole, long> userroleRepository
           , IRepository<Role> roleRepository
           , IAppNotifier appNotifier
            , IAppConfigurationAccessor configurationAccessor
           , IRepository<BankDetailsOrganization> bankOrganizationDetailsRepository
           , IUnitOfWorkManager unitOfWorkManager,
           IRepository<OrganizationUnit, long> OrganizationRepository,
           IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
           ICommonLookupAppService CommonDocumentSaveRepository,
           IRepository<QuotationDataDetails> quotationDetailRepository,
           IRepository<QuotationLinkHistory> quotationLinkHistoryRepository,
           IDbContextProvider<solarcrmDbContext> dbContextProvider
           )
        {
            _quotationDataDetailRepqository = quotationDataDetailRepqository;
            _quotationDataDetailProductRepqository = quotationDataDetailProductRepqository;
            _quotationDataDetailVariationRepqository = quotationDataDetailVariationRepqository;
            _quotationRepository = quotationRepository;
            _priceRepqository = priceRepqository;
            _binaryObjectManager = binaryObjectManager;
            _tempFileCacheManager = tempFileCacheManager;
            _jobRepository = lookup_jobRepository;
            _userRepository = userRepository;
            _leadactivityRepository = leadactivityRepository;
            _emailSender = emailSender;
            _leadRepository = leadRepository;
            _jobAppService = jobAppService;
            _env = env;
            _tenantRepository = tenantRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _variationRepository = variationRepository;
            _stockItemRepqository = stockItemRepqository;
            _jobVariationRepository = jobVariationRepository;
            _bankOrganizationDetailsRepository = bankOrganizationDetailsRepository;
            _userroleRepository = userroleRepository;
            _roleRepository = roleRepository;
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _OrganizationRepository = OrganizationRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _quotationDataDetailRepqository = quotationDetailRepository;
            _quotationLinkHistoryRepository = quotationLinkHistoryRepository;
            _configurationAccessor = configurationAccessor;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetQuotationDataForViewDto>> GetAll(GetAllQuotationDataInput input)
        {
            var filteredQuotations = _quotationRepository.GetAll()
                        .WhereIf(input.JobId > 0, e => false || e.QuoteJobId == input.JobId).OrderBy("id desc");

            //var pagedAndFilteredQuotations = filteredQuotations
            //    .OrderBy(input.Sorting ?? "id desc")
            //    .PageBy(input);

            var quotations = from o in filteredQuotations

                             join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                             from s3 in j3.DefaultIfEmpty()

                             select new GetQuotationDataForViewDto()
                             {
                                 Quotation = new QuotationDataDto
                                 {
                                     JobId = o.QuoteJobId,
                                     IsSigned = o.IsQuotateSigned,
                                     QuoteDate = o.QuotationGenerateDate,
                                     Id = o.Id,
                                     CreatedBy = s3.FullName,
                                     QuoteNumber = o.QuotationNumber,
                                     QuoteMode = o.QuoteMode,
                                 }
                             };

            var totalCount = await filteredQuotations.CountAsync();

            return new PagedResultDto<GetQuotationDataForViewDto>(
                totalCount,
                await quotations.ToListAsync()
            );
        }

        public async Task<GetQuotationDataForViewDto> GetQuotationForView(int id)
        {
            var quotation = await _quotationRepository.GetAsync(id);

            var output = new GetQuotationDataForViewDto { Quotation = ObjectMapper.Map<QuotationDataDto>(quotation) };

            return output;
        }

        // [AbpAuthorize(AppPermissions.Pages_Quotations_Edit, AppPermissions.Pages_Jobs)]
        public async Task<GetQuotationDataForEditOutput> GetQuotationForEdit(EntityDto input)
        {
            var depositOption = await _quotationRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetQuotationDataForEditOutput { DepositOption = ObjectMapper.Map<CreateOrEditQuotationDataDto>(depositOption) };

            return output;
        }
        public async Task CreateOrEdit(CreateOrEditQuotationDataDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Quotation_Create, AppPermissions.Pages_Tenant_Jobs)]
        protected virtual async Task Create(CreateOrEditQuotationDataDto input)
        {


            var job = _jobRepository.GetAll().Include(x => x.LeadFK)
                .Include(x => x.LeadFK.DiscomIdFk).
                Include(x => x.LeadFK.HeightStructureIdFk).
                Include(x => x.LeadFK.DivisionIdFk).Where(e => e.Id == input.JobId).FirstOrDefault();
            // var organizationid = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).Select(e => e.OrganizationUnitId).FirstOrDefault();
            var LastQuoteNumber = _quotationRepository.GetAll().Where(e => e.JobsIdFK.Id == job.Id).OrderByDescending(e => e.Id).Select(e => e.QuotationNumber).FirstOrDefault();

            var PrevQuoteNumber1 = 10001;
            if (LastQuoteNumber != null)
            {
                PrevQuoteNumber1 = Convert.ToInt32(LastQuoteNumber) + 1;
            }

            var quotation = ObjectMapper.Map<QuotationData>(input);

            quotation.QuoteJobId = (int)input.JobId;
            quotation.QuoteMode = input.QuoteMode;
            quotation.QuotationGenerateDate = DateTime.UtcNow;
            quotation.QuotationNumber = Convert.ToString(PrevQuoteNumber1);
            quotation.IsQuotateSigned = false;
            quotation.TenantId = (int)AbpSession.TenantId;
            quotation.OrganizationUnitId = input.OrganizationUnitId;
            //quotation.CreatorUserId = 1;
            var quotationId = await _quotationRepository.InsertAndGetIdAsync(quotation);

            //var jobData = _jobRepository.GetAll().Where(x=>x.Id == quotationId).FirstOrDefault();
            #region Add quotation data in new tab
            //QuotationDataDto quotationDataDto = await GetQuotationData(organizationid, quotationId, job);

            //  var pricedata = _priceRepqository.GetAll().Where(x=>x.ActualPrice == job.ActualSystemCost).FirstOrDefault();

            var quotationData = ObjectMapper.Map<QuotationDataDetails>(job);
            var AssignedUserData = _userRepository.GetAll().Where(x => x.Id == job.LeadFK.ChanelPartnerID).FirstOrDefault();
            quotationData.Id = 0;
            quotationData.CustomerName = job.LeadFK.CustomerName;
            quotationData.Mobile = job.LeadFK.MobileNumber;
            quotationData.Email = job.LeadFK.EmailId;
            quotationData.Address1 = job.LeadFK.AddressLine1;
            quotationData.Address2 = job.LeadFK.AddressLine2;
            quotationData.QuoteDataId = quotationId;
            quotationData.SolarAdvisorName = AssignedUserData.FullName;
            quotationData.SolarAdvisorMobile = AssignedUserData.PhoneNumber;
            quotationData.SolarAdvisorEmail = AssignedUserData.EmailAddress;
            quotationData.PvCapacityKw = Convert.ToString(job.PvCapacityKw);
            quotationData.Subcidy40 = input.Subcidy40;
            quotationData.Subcidy20 = input.Subcidy20;
            quotationData.BillToPay = input.BillToPay;
            quotationData.DepositeRequired = job.DepositeAmount;
            quotationData.DiscomName = job.LeadFK.DiscomIdFk.Name;
            quotationData.HeightOfStructure = job.LeadFK.HeightStructureIdFk.Name;
            quotationData.DivisionName = job.LeadFK.DivisionIdFk.Name;
            var quotationDataId = await _quotationDataDetailRepqository.InsertAndGetIdAsync(quotationData);

            //List<JobProductItem> productList = new List<JobProductItem>();
            //productList = _jobProductItemRepository.GetAll().Where(x => x.JobId == job.Id).ToList();
            if (input.JobProductItemList != null)
            {
                foreach (var item in input.JobProductItemList)
                {
                    var data = JsonConvert.DeserializeObject<JobProductItem>(item.ToString());
                    if (data.Id != 0)
                    {
                        data.Id = 0;
                        var modelDetail = ObjectMapper.Map<QuotationDataDetailProduct>(data);
                        var productData = _stockItemRepqository.GetAll().Include(x => x.StockCategoryFk).Where(x => x.Id == data.ProductItemId).FirstOrDefault();
                        modelDetail.QuotationDetailId = quotationDataId;
                        modelDetail.QuoteProductType = productData == null ? "" : productData.StockCategoryFk.Name;
                        modelDetail.QuoteProductName = productData == null ? "" : productData.Name;
                        modelDetail.QuoteProductModelNumber = productData == null ? "" : productData.Model;
                        modelDetail.QuoteProductQuantity = Convert.ToString(data.Quantity);
                        await _quotationDataDetailProductRepqository.InsertAndGetIdAsync(modelDetail);
                    }

                }
            }


            if (input.JobVariationItemList != null)
            {
                foreach (var item in input.JobVariationItemList)
                {
                    var data = JsonConvert.DeserializeObject<JobVariation>(item.ToString());
                    if (data.Id != 0)
                    {
                        data.Id = 0;
                        var modelDetail = ObjectMapper.Map<QuotationDataDetailVariations>(data);
                        modelDetail.QuotationDetailId = quotationDataId;
                        modelDetail.QuoteVarationName = _variationRepository.GetAll().Where(x => x.Id == data.VariationId).FirstOrDefault().Name; //modelDetail.QuoteVarationName;
                        modelDetail.QuoteVarationCost = data.Cost;
                        await _quotationDataDetailVariationRepqository.InsertAndGetIdAsync(modelDetail);
                    }

                }
            }

            #endregion
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            LeadActivityLogs leadactivity = new LeadActivityLogs();
            leadactivity.LeadActionId = 12;
            leadactivity.SectionId = 35;
            leadactivity.LeadActionNote = input.QuoteMode == 0 ? "New Regular Quotation Added" : "New Loan Quotation Added";
            leadactivity.LeadId = Convert.ToInt32(job.LeadId);
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Quotation_Edit, AppPermissions.Pages_Tenant_Jobs)]
        protected virtual async Task Update(CreateOrEditQuotationDataDto input)
        {
            var quotation = await _quotationRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, quotation);
        }
        [UnitOfWork]
        public async Task<QuotationDataDetailsDto> QuotationData(string STR)
        {
            QuotationDataDetailsDto signatureRequestDto = new QuotationDataDetailsDto();

            if (STR != null)
            {
                var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));              
                var ID = IDs.Split(",");
                int TenantId = Convert.ToInt32(ID[0]);
                int JobId = Convert.ToInt32(ID[1]);
                int Quotation = Convert.ToInt32(ID[2]);
                int OrgId = Convert.ToInt32(ID[3]);
                using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                {
                    var JobDetail = _jobRepository.GetAll().Where(e => e.Id == JobId).FirstOrDefault();
                    var Lead = _leadRepository.GetAll().Where(e => e.Id == JobDetail.LeadId).FirstOrDefault();
                    signatureRequestDto.QuoteCustomerName = Lead.CustomerName;
                    bool IsExpiried = false;
                    bool IsSigned = false;
                    var IsExpired = _quotationLinkHistoryRepository.GetAll().Where(e => e.Token == STR && e.QuotationId == Quotation).OrderByDescending(e => e.Id).FirstOrDefault();

                    if (IsExpired != null)
                    {

                        if (IsExpired.CreationTime.AddHours(24) <= DateTime.UtcNow)
                        {
                            IsExpiried = true;
                        }
                        if (IsExpired.Expired == true)
                        {
                            IsSigned = true;
                        }
                        if (IsExpiried == false && IsSigned == false)
                        {
                            var quoteData = _quotationDataDetailRepqository.GetAll().Include(x => x.QuoteDataIdFK).Where(x => x.QuoteDataId == Quotation).FirstOrDefault();

                            signatureRequestDto = ObjectMapper.Map<QuotationDataDetailsDto>(quoteData); // new QuotationDataDetailsDto { QuotationData = ObjectMapper.Map<QuotationDataDetailsDto>(quoteData) };
                                                                                                        //output.QuotationData = quoteData.QuoteDataIdFK.fi;
                            signatureRequestDto.QuoteNumber = quoteData.QuoteDataIdFK.QuotationNumber;
                            signatureRequestDto.QuoteGenerateDate = (DateTime?)quoteData.QuoteDataIdFK.QuotationGenerateDate;
                            signatureRequestDto.IsQuotateSigned = quoteData.QuoteDataIdFK.IsQuotateSigned;
                            signatureRequestDto.QuotateSignedDate = quoteData.QuoteDataIdFK.QuotationSignedDate;
                            signatureRequestDto.QuotateSignedImagePath = quoteData.QuoteDataIdFK.QuotationSignedImagePath;
                            signatureRequestDto.JobProductItemList = ObjectMapper.Map<List<object>>(_quotationDataDetailProductRepqository.GetAll().Where(x => x.QuotationDetailId == quoteData.Id).ToList());
                            signatureRequestDto.JobVariationItemList = ObjectMapper.Map<List<object>>(_quotationDataDetailVariationRepqository.GetAll().Where(x => x.QuotationDetailId == quoteData.Id).ToList());
                            signatureRequestDto.OrganizationDetails = ObjectMapper.Map<object>(_OrganizationRepository.GetAll().Where(x => x.Id == OrgId).ToList());
                            signatureRequestDto.OrganizationbankDetails = ObjectMapper.Map<List<object>>(_bankOrganizationDetailsRepository.GetAll().Where(x => x.BankDetailOrganizationId == OrgId).ToList());                           
                        }
                        else
                        {
                            signatureRequestDto.IsQuotateExpiry = IsExpiried;
                            signatureRequestDto.IsQuotateSigned = IsSigned;
                        }
                    }
                    else
                    {
                        signatureRequestDto.IsQuotateExpiry = IsExpiried;
                        signatureRequestDto.IsQuotateSigned = IsSigned;
                    }
                }
            }
            else
            {
                signatureRequestDto.IsQuotateExpiry = true;
                signatureRequestDto.IsQuotateSigned = false;
            }
            return signatureRequestDto;
        }
        public async Task<QuotationDataDetailsDto> GetQuoteForView(int quoteId, int organizationId)
        {
            var quoteData = _quotationDataDetailRepqository.GetAll().Include(x => x.QuoteDataIdFK).Where(x => x.QuoteDataId == quoteId).FirstOrDefault();
            var output = ObjectMapper.Map<QuotationDataDetailsDto>(quoteData); // new QuotationDataDetailsDto { QuotationData = ObjectMapper.Map<QuotationDataDetailsDto>(quoteData) };

            if (quoteData != null && output != null)
            {                                                                                   //output.QuotationData = quoteData.QuoteDataIdFK.fi;
                output.QuoteNumber = quoteData.QuoteDataIdFK.QuotationNumber;
                output.QuoteGenerateDate = (DateTime?)quoteData.QuoteDataIdFK.QuotationGenerateDate;
                output.IsQuotateSigned = quoteData.QuoteDataIdFK.IsQuotateSigned;
                output.QuotateSignedDate = (DateTime?)quoteData.QuoteDataIdFK.QuotationSignedDate;
                output.QuotateSignedImagePath = quoteData.QuoteDataIdFK.QuotationSignedImagePath;
                output.JobProductItemList = ObjectMapper.Map<List<object>>(_quotationDataDetailProductRepqository.GetAll().Where(x => x.QuotationDetailId == quoteData.Id).ToList());
                output.JobVariationItemList = ObjectMapper.Map<List<object>>(_quotationDataDetailVariationRepqository.GetAll().Where(x => x.QuotationDetailId == quoteData.Id).ToList());
                output.OrganizationDetails = ObjectMapper.Map<object>(_OrganizationRepository.GetAll().Where(x => x.Id == quoteData.QuoteDataIdFK.OrganizationUnitId).ToList());
                output.OrganizationbankDetails = ObjectMapper.Map<List<object>>(_bankOrganizationDetailsRepository.GetAll().Where(x => x.BankDetailOrganizationId == organizationId).ToList());
               
            }
            return output;
        }
        public async Task SendQuotation(CreateOrEditQuotationDataDto input)
        {
            LeadActivityLogs leadactivity = new LeadActivityLogs();
            leadactivity.SectionId = 0;
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            var quotation = _quotationRepository.GetAll().Where(e => e.QuoteJobId == input.JobId).OrderByDescending(e => e.Id).FirstOrDefault();

            var job = _jobRepository.GetAll().Where(e => e.Id == quotation.QuoteJobId).FirstOrDefault();
            var lead = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefault();
            var Body = job.TenantId + "," + job.Id + "," + quotation.Id;
            var smsBody = "";
            //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
            var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase));
            var token1 = SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase);

            string BaseURL = _configurationAccessor.Configuration["App:ClientSignatureRootAddress"];
            //System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];

            Body = BaseURL + token;
            //Body = "http://localhost:4200/app/main/signature/customer-signature?STR=" + token;

            QuotationLinkHistory quotationLinkHistory = new QuotationLinkHistory();
            quotationLinkHistory.Expired = false;
            quotationLinkHistory.QuotationId = quotation.Id;
            quotationLinkHistory.Token = token1;
            await _quotationLinkHistoryRepository.InsertAndGetIdAsync(quotationLinkHistory);
            string FinalBody = "";
            string FinalSMSBody = "";
        }

        public async Task SaveSignature(SaveCustomerSignature input)
        {
            try
            {
                var FileName = DateTime.Now.Ticks + "_CustomerSignature.png";
                var IMage = input.ImageData.Split(new[] { ',' }, 21);
                var buffer = new byte[2097152];
                byte[] ByteArray = Convert.FromBase64String(IMage[1]);
                var IDs = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(input.EncString, AppConsts.DefaultPassPhrase));
                ///var IDs = SimpleStringCipher.Instance.Decrypt(input.EncString, AppConsts.DefaultPassPhrase);
                var ID = IDs.Split(",");
                int TenantId = Convert.ToInt32(ID[0]);
                int JobId = Convert.ToInt32(ID[1]);
                int Quotation = Convert.ToInt32(ID[2]);

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                    {
                        var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(ByteArray, FileName, "CustomerSignature", JobId, 0, AbpSession.TenantId);
                        if (Quotation != null)
                        {
                            var quotation = _quotationRepository.GetAll().Where(e => e.Id == Quotation).OrderByDescending(e => e.Id).FirstOrDefault();
                            var leadid = _jobRepository.GetAll().Include(x => x.LeadFK).Where(e => e.Id == quotation.QuoteJobId).Select(e => e.LeadId).FirstOrDefault();
                            var organizationid = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.OrganizationUnitId).FirstOrDefault();
                            //var SignedQuoteFileName = _reportAppService.Quotation(JobId, Quotation, filepath.FinalFilePath, quotation.QuoteNumber, organizationid) + ".pdf";

                            if (quotation != null)
                            {
                                quotation.QuotationSignLatitude = input.QuoteSignLatitude;
                                quotation.QuotationSignLongitude = input.QuoteSignLongitude;
                                quotation.QuotationSignedIP = input.QuoteSignIP;
                                quotation.QuotationSignedFileName = FileName;
                                quotation.QuotationSignedImagePath = filepath.FinalFilePath;
                                quotation.IsQuotateSigned = true;
                                quotation.QuotationSignedDate = DateTime.UtcNow;
                                await _quotationRepository.UpdateAsync(quotation);
                            }
                        }

                        var Link = _quotationLinkHistoryRepository.GetAll().Where(e => e.Token == input.EncString && e.QuotationId == Quotation);
                        if (Link != null)
                        {
                            foreach (var item in Link)
                            {
                                item.Expired = true;
                                await _quotationLinkHistoryRepository.UpdateAsync(item);
                            }
                        }

                        //await _jobAppService.JobActive(JobId);
                        var Lead = _leadRepository.GetAll().Where(e => e.Id == _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.LeadId).FirstOrDefault()).FirstOrDefault();
                        var User_List = _userRepository.GetAll().ToList();
                        // var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == Lead.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();
                        var AssignedUser = _userRepository.GetAll().Where(e => e.Id == Lead.ChanelPartnerID).FirstOrDefault();

                        string msg = string.Format("Customer Siagnture Received For {0} Job", filepath.JobNumber);
                        await _appNotifier.LeadAssiged(AssignedUser, msg, NotificationSeverity.Info);

                        var UserList = (from user in User_List
                                        join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                        from ur in urJoined.DefaultIfEmpty()

                                        join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                        from us in usJoined.DefaultIfEmpty()

                                            //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                            //from ut in utJoined.DefaultIfEmpty()

                                            where us != null && us.DisplayName == "Manager"
                                        select user);

                        foreach (var item in UserList)
                        {
                            await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                        }
                        uow.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //[AbpAuthorize(AppPermissions.Pages_Quotations_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _quotationRepository.DeleteAsync(input.Id);
        }

        public async Task<string> GeneratePdf(string JobNumber, object QuoteString)
        {
           
            var a2pClient = new Api2Pdf.Api2Pdf("7e764038-dd32-4543-8249-22b867fc6964");

            var options = new ChromeHtmlToPdfOptions
            {
                Delay = 0,
                PuppeteerWaitForMethod = "WaitForNavigation",
                PuppeteerWaitForValue = "Load",
                UsePrintCss = true,
                Landscape = false,
                PrintBackground = true,
                DisplayHeaderFooter = false,
                Width = "8.27in",
                Height = "11.69in",
                MarginBottom = "0in",
                MarginTop = "0in",
                MarginLeft = "0in",
                MarginRight = "0in",
                PageRanges = "1-10000",
                Scale = 1,
                OmitBackground = false
            };

            var request = new ChromeHtmlToPdfRequest
            {
                Html = QuoteString.ToString().Replace("\"", "'"),
                FileName = "Quote_" + JobNumber + ".pdf",
                Inline = true,
                Options = options,
                UseCustomStorage = false,
                //Storage =
                //            {
                //	Method = "PUT",
                //	Url = "https://presignedurl",
                //	ExtraHTTPHeaders =
                //  {

                //  }
                //}
            };

            var apiResponse = a2pClient.Chrome.HtmlToPdf(request);

            return apiResponse.FileUrl;
        }

    }
}
