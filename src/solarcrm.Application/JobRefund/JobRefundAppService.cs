﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.EntityFrameworkCore;
using solarcrm.Job.JobRefunds;
using solarcrm.JobRefund.Dto;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Abp.Domain.Uow;
using Abp.Organizations;
using solarcrm.Quotations.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Linq.Dynamic.Core;
namespace solarcrm.JobRefund
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund)]
    public class JobRefundAppService : solarcrmAppServiceBase, IJobRefundAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<JobRefunds> _jobRefundRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;

        private readonly IAbpSession _abpSession;
        // private readonly IRepository<Leads> _leadRepository;        
        private readonly UserManager _userManager;

        public JobRefundAppService(IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<JobRefunds> jobRefundRepository, IRepository<Job.Jobs> jobRepository, IRepository<LeadActivityLogs> leadactivityRepository, IAbpSession abpSession, UserManager userManager, ITimeZoneConverter timeZoneConverter, IRepository<User, long> userRepository)
        {
            _dbContextProvider = dbContextProvider;
            _jobRefundRepository = jobRefundRepository;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _userRepository = userRepository;
            _timeZoneConverter = timeZoneConverter;
            //_jobProductItemRepository = jobProductItemRepository;
            _abpSession = abpSession;
            _userManager = userManager;
            //_leadRepository = leadRepository;
        }
        public async Task<List<GetJobRefundForViewDto>> GetAll(GetAllJobRefundInput input)
        {
            List<GetJobRefundForViewDto> refundpayment = new List<GetJobRefundForViewDto>();
            var filteredQuotations = _jobRefundRepository.GetAll().Include(x => x.RefundResIDFK).Include(x => x.PaymentTypeIDFK)
                        .WhereIf(input.JobId > 0, e => false || e.RefundJobId == input.JobId).OrderBy("id desc");

            refundpayment = await(from o in filteredQuotations
                             select new GetJobRefundForViewDto()
                             {
                                 jobRefund = new JobRefundDto
                                 {
                                     Id = o.Id,
                                     RefundJobId = o.RefundJobId,
                                     RefundReasonId = o.RefundReasonId,
                                     PaymentTypeId = o.PaymentTypeId,
                                     RefundPaidAmount = o.RefundPaidAmount,
                                     RefundPaidDate = o.RefundPaidDate,
                                     RefundPaymentType = o.PaymentTypeIDFK.Name,
                                     RefundReasonName = o.RefundResIDFK.Name,
                                     RefundPaidNotes = o.RefundPaidNotes,
                                     IsRefundVerify = o.IsRefundVerify,
                                     RefundVerifyDate = o.RefundVerifyDate,
                                     RefundVerifyNotes = o.RefundVerifyNotes,
                                     RefundVerifyById = o.RefundVerifyById,
                                     BankName = o.BankName,
                                     BankAccountHolderName = o.BankAccountHolderName,
                                     BankBrachName = o.BankBrachName,
                                     BankAccountNumber = o.BankAccountNumber,                                    
                                     RefundRecPath = o.RefundRecPath,
                                     ReceiptNo = o.ReceiptNo,
                                     JobRefundEmailSend = o.JobRefundEmailSend,
                                     JobRefundEmailSendDate = o.JobRefundEmailSendDate,
                                     JobRefundSmsSend = o.JobRefundSmsSend,
                                     JobRefundSmsSendDate = o.JobRefundSmsSendDate,
                                 }
                             }).ToListAsync();
            return refundpayment;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund)]
        public async Task<GetJobRefundForViewDto> GetJobRefundForView(int id)
        {
            var JobRefund = await _jobRefundRepository.GetAsync(id);

            var output = new GetJobRefundForViewDto { jobRefund = ObjectMapper.Map<JobRefundDto>(JobRefund) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund_Edit, AppPermissions.Pages_Tenant_Jobs_Refund)]
        public async Task<GetJobRefundForEditOutput> GetJobRefundForEdit(EntityDto input)
        {
            var jobRefunds = await _jobRefundRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetJobRefundForEditOutput { JobRefunds = ObjectMapper.Map<CreateOrEditJobRefundDto>(jobRefunds) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditJobRefundDto input)
        {
            if (input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund_Create)]
        protected virtual async Task Create(CreateOrEditJobRefundDto input)
        {
            var jobRefund = ObjectMapper.Map<JobRefunds>(input);
            if (jobRefund != null && jobRefund.RefundJobId != 0)
            {
                var RefundJobID = await _jobRefundRepository.InsertAndGetIdAsync(jobRefund);

                //Add Activity Log Installer
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 15; // Job Refund
                                                //leadactivity.LeadAcitivityID = 0; // confusion
                leadactivity.SectionId = 55;
                leadactivity.LeadActionNote = "Job Refund Created";
                leadactivity.LeadId = input.leadid;
                leadactivity.OrganizationUnitId = input.OrgID;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund_Edit)]
        protected virtual async Task Update(CreateOrEditJobRefundDto input)
        {
            int leadid = 0;

            var jobRefund = _jobRefundRepository.GetAll().Include(x => x.JobIDFK).Where(x => x.RefundJobId == input.RefundJobId && x.Id == input.Id).FirstOrDefault();
            if (jobRefund != null)
            {
                //jobRefund = ObjectMapper.Map<JobRefunds>(input);
                jobRefund.RefundPaidDate = input.RefundPaidDate;
                jobRefund.RefundPaidAmount = input.RefundPaidAmount;
                jobRefund.RefundPaidNotes = input.RefundPaidNotes;
                jobRefund.RefundReasonId = input.RefundReasonId;
                jobRefund.PaymentTypeId = input.PaymentTypeId;
                jobRefund.IsRefundVerify= input.IsRefundVerify;
                jobRefund.RefundVerifyDate = input.RefundVerifyDate;
                jobRefund.RefundVerifyNotes= input.RefundVerifyNotes;
                jobRefund.RefundVerifyById = input.RefundVerifyById;
                jobRefund.RefundRecPath =   input.RefundRecPath;
                jobRefund.BankName = input.BankName;
                jobRefund.BankBrachName = input.BankBrachName;
                jobRefund.BankAccountNumber = input.BankAccountNumber;
                jobRefund.BankAccountHolderName = input.BankAccountHolderName;
                jobRefund.IFSC_Code = input.IFSC_Code;
                jobRefund.ReceiptNo = input.ReceiptNo;
                jobRefund.JobRefundEmailSend = input.JobRefundEmailSend;
                jobRefund.JobRefundEmailSendDate = input.JobRefundEmailSendDate;
                jobRefund.JobRefundSmsSend = input.JobRefundSmsSend;
                jobRefund.JobRefundSmsSendDate = input.JobRefundSmsSendDate;

                if (jobRefund.Id != 0)
                {
                    //var JobInstallId = await _jobRefundRepository.UpdateAsync(jobRefund);

                    #region Activity Log
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 15;
                    //leadactivity.LeadAcitivityID = 0;
                    leadactivity.SectionId = 55;
                    leadactivity.LeadActionNote = "Job Refund Modified";
                    leadactivity.LeadId = jobRefund.JobIDFK.LeadId;// input.leadid;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                    var List = new List<LeadtrackerHistory>();

                    try
                    {
                        //List<JobInstallation.JobInstallationDetail> objAuditOld_OnlineUserMst = new List<JobInstallation.JobInstallationDetail>();
                        //List<CreateOrEditJobInstallDto> objAuditNew_OnlineUserMst = new List<CreateOrEditJobInstallDto>();

                        //DataTable Dt_Target = new DataTable();
                        //DataTable Dt_Source = new DataTable();



                        //objAuditOld_OnlineUserMst.Add(jobdate);
                        //objAuditNew_OnlineUserMst.Add(input);

                        ////Add Activity Log
                        //LeadtrackerHistory leadActivityLog = new LeadtrackerHistory();
                        //if (AbpSession.TenantId != null)
                        //{
                        //    leadActivityLog.TenantId = (int)AbpSession.TenantId;
                        //}

                        //object newvalue = objAuditNew_OnlineUserMst;
                        //Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                        //if (objAuditOld_OnlineUserMst != null)
                        //{
                        //    Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                        //}
                        //foreach (DataRow dr_S in Dt_Source.Rows)
                        //{
                        //    foreach (DataColumn dc_S in Dt_Source.Columns)
                        //    {

                        //        if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                        //        {
                        //            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                        //            objAuditInfo.FieldName = dc_S.ColumnName;
                        //            objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                        //            objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                        //            //objAuditInfo.Action = "Edit";
                        //            objAuditInfo.Action = Dt_Target.Rows[0][dc_S.ColumnName].ToString() == "" ? "Add" : "Edit";
                        //            objAuditInfo.LeadId = jobdate.JobFKId.LeadId;
                        //            objAuditInfo.LeadActionId = leadactid;
                        //            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                        //            List.Add(objAuditInfo);
                        //        }
                        //    }
                        //}

                        //await _dbContextProvider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
                        //await _dbContextProvider.GetDbContext().SaveChangesAsync();
                    }
                    catch
                    {

                    }
                    #endregion


                }
            }
        }
      

        [AbpAuthorize(AppPermissions.Pages_Tenant_jobs_Refund_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _jobRefundRepository.DeleteAsync(input.Id);
        }
    }
}
