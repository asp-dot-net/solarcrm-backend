﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using solarcrm.ApplicationSettings.Dto;
using solarcrm.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.LeadActivityLog;

namespace solarcrm.ApplicationSettings
{
    public class ApplicationSettingsAppService : solarcrmAppServiceBase, IApplicationSettingsAppService
    {
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;

        public ApplicationSettingsAppService(
             IRepository<Leads> leadRepository
            , IRepository<LeadActivityLogs> leadactivityRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            )
        {
            _leadRepository = leadRepository;
            _leadactivityRepository = leadactivityRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
        }
        public async Task SendSMS(SendSMSInput input)
        {
            if (AbpSession.TenantId != null)
            {
                SendSMSRequestInput smsReq = new SendSMSRequestInput();

                ExtendOrganizationUnit orgSmsCriteria = new ExtendOrganizationUnit();
                
                    var leadid = _leadactivityRepository.GetAll().Where(e => e.Id == input.ActivityId && e.TenantId == (int)AbpSession.TenantId).Select(e => e.LeadId).FirstOrDefault();
                    var orgID = _leadRepository.GetAll().Where(e => e.Id == leadid).Select(e => e.OrganizationUnitId).FirstOrDefault();
                    orgSmsCriteria = _extendedOrganizationUnitRepository.FirstOrDefault(X => X.Id == orgID);
                

               // var applicationSetting = _applicationSettingRepository.FirstOrDefault(X => X.TenantId == (int)AbpSession.TenantId);
                string authCode = orgSmsCriteria.FoneDynamicsAccountSid + ":" + orgSmsCriteria.FoneDynamicsToken;
                byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(authCode);
                string headerToken = "BASIC " + System.Convert.ToBase64String(data);
                smsReq.From = orgSmsCriteria.FoneDynamicsPhoneNumber;
                smsReq.To = input.PhoneNumber;
                //smsReq.To = "+919924860723";
                smsReq.Text = input.Text;
                //smsReq.ExternalId = "1";

                if (input.promoresponseID > 0)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&promoresid=" + input.promoresponseID + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }
               
                else if (input.ActivityId > 0)
                {
                    smsReq.WebhookUri = "http://app.thesolarproduct.com/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&sectionid=" + input.SectionID;
                    //smsReq.WebhookUri = "https://oldblueshop62.conveyor.cloud/SMS?tenantid=" + AbpSession.TenantId + "&actionId=" + input.ActivityId + "&sectionid=" + input.SectionID;
                    smsReq.WebhookMethod = "POST";
                }

                string restURL = "https://api.fonedynamics.com/v2/Properties/";// + orgSmsCriteria.FoneDynamicsPropertySid + "/Messages";
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(smsReq), Encoding.UTF8, "application/json");
                    httpClient.DefaultRequestHeaders.Add("Authorization", headerToken);
                    using (var response = await httpClient.PostAsync(restURL, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        var userObj = JObject.Parse(apiResponse);
                        var userGuid = Convert.ToString(userObj["Message"]["MessageSid"]);
                        if (input.ActivityId != 0)
                        {

                                var Activity = _leadactivityRepository.GetAll().Where(e => e.Id == input.ActivityId).FirstOrDefault();
                                Activity.MessageId = userGuid;
                                await _leadactivityRepository.UpdateAsync(Activity);
                            
                        }
                        //test
                    }
                }
            }
        }
    }
}
