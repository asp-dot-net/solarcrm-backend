﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Organizations;
using Abp.Runtime.Session;
using Abp.Threading;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.EntityFrameworkCore;
using solarcrm.MultiTenancy;

namespace solarcrm
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class solarcrmAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }
        public OrganizationUnitManager OrganizationUnitManager { get; set; }
        public UserManager UserManager { get; set; }
        public solarcrmDbContext dbContecxt { get; set; }
      
        protected solarcrmAppServiceBase()
        {
            LocalizationSourceName = solarcrmConsts.LocalizationSourceName;            
        }

        protected virtual async Task DefaultMasterScriptRun()
        {
            var sql = System.IO.File.ReadAllText("wwwroot\\DbScript\\SolarCrmDbMasterScript.Sql");
            if (sql != null)
                dbContecxt.Database.ExecuteSqlRaw(sql);
        }
        protected virtual async Task<User> GetCurrentUserAsync()
        {
            var user = await UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected virtual User GetCurrentUser()
        {
            return AsyncHelper.RunSync(GetCurrentUserAsync);
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
            }
        }

        protected virtual Tenant GetCurrentTenant()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetById(AbpSession.GetTenantId());
            }
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}