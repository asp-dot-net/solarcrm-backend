﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.EntityFrameworkCore;
using solarcrm.JobInstallation;
using solarcrm.Tracker.InstallationTracker.Dto;
using solarcrm.Tracker.InstallationTracker.Exporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace solarcrm.Tracker.InstallationTracker
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Installation)]
    public class InstallationTrackerAppService : solarcrmAppServiceBase, IInstallationTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IInstallationTrackerExcelExporter _InstallTrackerExcelExporter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<JobInstallationDetail> _jobInstallationDetailRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public InstallationTrackerAppService(IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<JobInstallationDetail> jobInstallationDetailRepository,
            IInstallationTrackerExcelExporter InstallTrackerExcelExporter,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus, int> lookup_leadStatusRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            UserManager userManager,
            ITimeZoneConverter timeZoneConverter,
            IRepository<LeadDocuments> leadDocumentRepository,        
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _InstallTrackerExcelExporter = InstallTrackerExcelExporter;
            _userRepository = userRepository;
            _userManager = userManager;
            _jobInstallationDetailRepository = jobInstallationDetailRepository;
            _leadDocumentRepository = leadDocumentRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
        }
        public async Task<PagedResultDto<GetInstallationTrackerForViewDto>> GetAllInstallationTracker(GetAllInstallationTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);

            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            var jobnumberlist = new List<int>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            var FollowupList = new List<int>();
            var NextFollowupList = new List<int>();
            var Assign = new List<int>();

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }

            var filteredJob = _jobInstallationDetailRepository.GetAll().Include(e => e.JobFKId.LeadFK)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobFKId.JobNumber.Contains(input.Filter))
                 .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobFKId.LeadFK.CustomerName.Contains(input.Filter) || e.JobFKId.LeadFK.EmailId.Contains(input.Filter) || e.JobFKId.LeadFK.Alt_Phone.Contains(input.Filter) ||
                                e.JobFKId.LeadFK.MobileNumber.Contains(input.Filter) || e.JobFKId.LeadFK.AddressLine1.Contains(input.Filter) || e.JobFKId.LeadFK.AddressLine2.Contains(input.Filter) || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.Id)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                             .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.JobFKId.LeadFK.LeadStatusId))
                              .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.JobFKId.ApplicationStatusId))
                              //.WhereIf(input.otpPendingFilter != null, e => input.otpPendingFilter == e.OtpVerifed)
                              .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "yes", e => e.JobFKId.LastComment != null)
                              .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "no", e => e.JobFKId.LastComment == null)
                              //.WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId))
                              //.WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId))
                              //  .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId))
                              //  .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId))
                              //.WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId))
                              // .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.LeadFK.TenantId))
                              .WhereIf(input.applicationstatusTrackerFilter == 1, e => e.JobFKId.ApplicationDate != null && e.JobFKId.ApplicationNumber != null) //Completed                                                                                                                                                                                                                              

                              //.WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "queryraisedon" && input.StartDate != null, e => e.LastCommentDate >= SDate.Value.Date && e.LastCommentDate <= EDate.Value.Date)
                              //.WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "nextfollowup" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
                              //.WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.LeadFK.ChanelPartnerID))

                              .Where(e => e.JobFKId.LeadFK.OrganizationUnitId == input.OrganizationUnit && e.InstallStartDate != null);

            var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o3 in _lookup_leadStatusRepository.GetAll() on o.JobFKId.LeadFK.LeadStatusId equals o3.Id into j3
                       from s3 in j3.DefaultIfEmpty()

                       join o4 in _organizationUnitRepository.GetAll() on o.JobFKId.OrganizationUnitId equals o4.Id into j4
                       from s4 in j4.DefaultIfEmpty()

                       select new GetInstallationTrackerForViewDto()
                       {
                           installTracker = new InstallationTrackerDto
                           {
                               Id = o.Id,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.JobFKId.LeadFK),
                               ProjectNumber = o.JobFKId.JobNumber,
                               //JobStatus = o.JobStatusFk.Name,
                               CustomerName = o.JobFKId.LeadFK.CustomerName,
                               Address = (o.JobFKId.LeadFK.AddressLine1 == null ? "" : o.JobFKId.LeadFK.AddressLine1 + ", ") + (o.JobFKId.LeadFK.AddressLine2 == null ? "" : o.JobFKId.LeadFK.AddressLine2 + ", ") + (o.JobFKId.LeadFK.CityIdFk.Name == null ? "" : o.JobFKId.LeadFK.CityIdFk.Name + ", ") + (o.JobFKId.LeadFK.TalukaIdFk.Name == null ? "" : o.JobFKId.LeadFK.TalukaIdFk.Name + ", ") + (o.JobFKId.LeadFK.DiscomIdFk.Name == null ? "" : o.JobFKId.LeadFK.DiscomIdFk.Name + ", ") + (o.JobFKId.LeadFK.StateIdFk.Name == null ? "" : o.JobFKId.LeadFK.StateIdFk.Name + "-") + (o.JobFKId.LeadFK.Pincode == null ? "" : o.JobFKId.LeadFK.Pincode),
                               Mobile = o.JobFKId.LeadFK.MobileNumber,
                               SubDivision = o.JobFKId.LeadFK.SubDivisionIdFk.Name,
                               JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.JobFKId.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                               LeadStatusName = o.JobFKId.LeadFK.LeadStatusIdFk.Status,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.JobFKId.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobFKId.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Notes = o.JobFKId.LeadFK.Notes,
                               LastModificationTime = o.LastModificationTime,
                               CreationTime = (DateTime)o.CreationTime,
                               //QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                               LeadStatusColorClass = o.JobFKId.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.JobFKId.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobFKId.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.JobFKId.ApplicationStatusIdFK,
                           },

                       };

            var totalCount = await filteredJob.CountAsync();

            return new PagedResultDto<GetInstallationTrackerForViewDto>(totalCount, await jobs.ToListAsync());
        }

        public async Task<InstallationTrackerQuickViewDto> GetInstallationQuickView(EntityDto input)
        {
            try
            {
                var job = _jobRepository.GetAll().Include(e => e.LeadFK).
                    Include(e => e.LeadFK.DivisionIdFk).
                    Include(e => e.LeadFK.SubDivisionIdFk).
                    Include(e => e.LeadFK.DiscomIdFk).
                    Include(e => e.LeadFK.CityIdFk).
                    Include(e => e.LeadFK.TalukaIdFk).
                    Include(e => e.LeadFK.DistrictIdFk).
                    Include(e => e.LeadFK.StateIdFk).
                    Where(e => e.Id == input.Id).FirstOrDefault();

                var output = new InstallationTrackerQuickViewDto
                {

                    ProjectNumber = job.JobNumber,
                    //Latitude = (decimal?)job.LeadFK.Latitude,
                    //Longitude = (decimal?)job.LeadFK.Longitude,
                    //AvgmonthlyBill = (int?)job.LeadFK.AvgmonthlyBill,
                    //AvgmonthlyUnit = (int?)job.LeadFK.AvgmonthlyUnit,
                    //CustomerName = job.LeadFK.CustomerName,
                    //ConsumerNumber = job.LeadFK.ConsumerNumber,
                    //SubDivisionName = job.LeadFK.SubDivisionIdFk.Name, //leads.SubDivisionId != null ? _subDivisionRepository.FirstOrDefaultAsync((int)leads.SubDivisionId).Result.Name : "",
                    //DivisionName = job.LeadFK.DivisionIdFk.Name, //leads.DivisionId != null ? _divisionRepository.FirstOrDefaultAsync((int)leads.DivisionId).Result.Name : "",
                    //DiscomName = job.LeadFK.DiscomIdFk.Name, //leads.DiscomId != null ? _discomRepository.FirstOrDefaultAsync((int)leads.DiscomId).Result.Name : "",

                    //SystemSize = job.PvCapacityKw, //pendding
                    //MeterPhase = job.PhaseOfInverter,
                    //Category = job.LeadFK.Category,
                    //EmailId = job.LeadFK.EmailId,
                    //MobileNumber = job.LeadFK.MobileNumber,
                    //AddressLine1 = job.LeadFK.AddressLine1,
                    //AddressLine2 = job.LeadFK.AddressLine2,
                    //CityName = job.LeadFK.CityIdFk.Name, // leads.CityId != null ? _cityRepository.FirstOrDefaultAsync((int)leads.CityId).Result.Name : "",
                    //TalukaName = job.LeadFK.TalukaIdFk.Name, // leads.TalukaId != null ? _talukaRepository.FirstOrDefaultAsync((int)leads.TalukaId).Result.Name : "",
                    //DistrictName = job.LeadFK.DistrictIdFk.Name, // leads.DistrictId != null ? _districtRepository.FirstOrDefaultAsync((int)leads.DistrictId).Result.Name : "",
                    //StateName = job.LeadFK.StateIdFk.Name, // leads.StateId != null ? _stateRepository.FirstOrDefaultAsync((int)leads.StateId).Result.Name : "",
                    //Pincode = job.LeadFK.Pincode,
                    //CommonMeter = job.LeadFK.CommonMeter,

                    //LeadDocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(_leadDocumentRepository.GetAll().Where(x => x.LeadDocumentId == job.LeadId).ToList())),
                };
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
