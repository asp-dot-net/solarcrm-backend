﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.InstallationTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.InstallationTracker.Exporting
{
    public class InstallationTrackerExcelExporter : NpoiExcelExporterBase, IInstallationTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public InstallationTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetInstallationTrackerForViewDto> apptrack)
        {
            return CreateExcelPackage(
                "InstallationTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InstallationTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("LeadStatus"),
                        L("ApplicationStatus"),
                        L("CustomerName"),
                        L("Address"),
                        L("Mobile"),
                        L("ActivityDescription"),
                        L("ReminderTime"),                      
                        L("Comment")
                        );
                    AddObjects(
                        sheet, apptrack,
                        _ => _.installTracker.ProjectNumber,
                        _ => _.installTracker.LeadStatusName,
                        _ => _.installTracker.ApplicationStatusdata,
                        _ => _.installTracker.CustomerName,
                        _ => _.installTracker.Address,
                        _ => _.installTracker.Mobile,
                        _ => _.installTracker.FollowDiscription,
                        _ => _.installTracker.FollowDate,
                        _ => _.installTracker.Comment
                        );
                });
        }
    }
}
