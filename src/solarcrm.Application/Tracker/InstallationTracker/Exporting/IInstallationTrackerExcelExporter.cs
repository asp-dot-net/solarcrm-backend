﻿using solarcrm.Dto;
using solarcrm.Tracker.InstallationTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.InstallationTracker.Exporting
{
    public interface IInstallationTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetInstallationTrackerForViewDto> installTracker);
    }
}
