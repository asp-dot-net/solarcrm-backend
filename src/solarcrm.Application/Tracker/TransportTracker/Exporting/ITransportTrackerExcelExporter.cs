﻿using solarcrm.Dto;
using solarcrm.Tracker.RefundTracker.Dto;
using solarcrm.Tracker.TransportTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.TransportTracker.Exporting
{
    public interface ITransportTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetTransportTrackerForViewDto> transportTracker);
    }
}
