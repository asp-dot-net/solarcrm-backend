﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.RefundTracker.Dto;
using solarcrm.Tracker.RefundTracker.Exporting;
using solarcrm.Tracker.TransportTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.TransportTracker.Exporting
{
    public class TransportTrackerExcelExporter : NpoiExcelExporterBase, ITransportTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TransportTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetTransportTrackerForViewDto> transporttrack)
        {
            return CreateExcelPackage(
                "TransportTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("TransportTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("CustomerName"),
                         L("Mobile"),
                        L("Address"),
                        L("VehicalType"),
                        L("RegoNumber"),
                        L("DriverName"),
                        L("DispatchDate")
                        );
                    AddObjects(
                        sheet, transporttrack,
                        _ => _.transportTracker.ProjectNumber,
                        _ => _.transportTracker.CustomerName,
                        _ => _.transportTracker.Mobile,
                        _ => _.transportTracker.Address,
                        _ => _.transportTracker.VehicalType,
                        _ => _.transportTracker.Regonumber,
                        _ => _.transportTracker.DriverName,
                        _ => _.transportTracker.DispatchDate
                        );
                });
        }
    }
}
