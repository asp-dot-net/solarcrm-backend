﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solarcrm.Transport;
using solarcrm.Dispatch;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Tracker.TransportTracker.Dto;
using Abp.Application.Services.Dto;
using solarcrm.Job;
using solarcrm.DataVaults.Leads.Dto;

using solarcrm.Payments;
using solarcrm.JobInstallation;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.TransportTracker.Exporting;
using Abp.Authorization;
using solarcrm.Authorization;

namespace solarcrm.Tracker.TransportTracker
{
    public class TransportTrackerAppService : solarcrmAppServiceBase, ITransportTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<TransportDetails> _transportDetailsRepository;
        private readonly IRepository<DispatchMaterialDetails> _dispatchMaterialDetailsRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobInstallationDetail> _jobInstallRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<SSPaymentDetails> _ssPaymentDetailsRepository;
        private readonly IRepository<STCPaymentDetails> _stcPaymentDetailsRepository;
        private readonly ITransportTrackerExcelExporter _transportTrackerExcelExporter;
        // private readonly IRepository<Leads> _leadRepository;        
        private readonly UserManager _userManager;

        public TransportTrackerAppService(IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<TransportDetails> transportDetailsRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<DispatchMaterialDetails> dispatchMaterialDetailsRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IAbpSession abpSession, UserManager userManager,
            IRepository<JobInstallationDetail> jobInstallRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<User, long> userRepository,
            IRepository<SSPaymentDetails> ssPaymentDetailsRepository,
            ITransportTrackerExcelExporter transportTrackerExcelExporter,
        IRepository<STCPaymentDetails> stcPaymentDetailsRepository)
        {
            _dbContextProvider = dbContextProvider;
            _transportDetailsRepository = transportDetailsRepository;
            _jobRepository = jobRepository;
            _dispatchMaterialDetailsRepository = dispatchMaterialDetailsRepository;
            _leadactivityRepository = leadactivityRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _userRepository = userRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
            _jobInstallRepository = jobInstallRepository;
            _ssPaymentDetailsRepository = ssPaymentDetailsRepository;
            _stcPaymentDetailsRepository = stcPaymentDetailsRepository;
            _transportTrackerExcelExporter = transportTrackerExcelExporter;
            _userManager = userManager;
        }
        public async Task<PagedResultDto<GetTransportTrackerForViewDto>> GetAllTransportTracker(GetAllTransportTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<DateTime> dispatchDatelist = new List<DateTime>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);


            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            if (input.BillTypeFilter == "ss" || input.BillTypeFilter == "both")
            {
                sspaymentlist = _ssPaymentDetailsRepository.GetAll().Include(x => x.PaymentIdFK).Select(x => x.PaymentIdFK.PaymentJobId).ToList();
            }
            if (input.BillTypeFilter == "stc" || input.BillTypeFilter == "both")
            {
                stcpaymentlist = _stcPaymentDetailsRepository.GetAll().Include(x => x.PaymentIdFK).Select(x => x.PaymentIdFK.PaymentJobId).ToList();
            }
            if (input.FilterbyDate == "dispatchdate")
            {
                dispatchDatelist = _dispatchMaterialDetailsRepository.GetAll().Select(x => x.DispatchDate).ToList();
            }
            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var filteredtransport = _transportDetailsRepository.GetAll().Include(x => x.TransportJobsIdFK).Include(x => x.VehicalIdFK)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TransportJobsIdFK.JobNumber.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.CustomerName.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.TransportJobsIdFK.LeadId)))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "stc", e => stcpaymentlist.Contains(e.TransportJobId))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "ss", e => sspaymentlist.Contains(e.TransportJobId))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "both", e => stcpaymentlist.Contains(e.TransportJobId) && sspaymentlist.Contains(e.TransportJobId))
                        .WhereIf(input.VehicalType != null && input.VehicalType.Count() > 0, e => input.VehicalType.Contains((int)e.VehicalIdFK.Id))
                        .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.DiscomId))
                        .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SolarTypeId))
                        .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.ChanelPartnerID))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "transportdate", e => e.TransportDate != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "dispatchdate", e => dispatchDatelist != null)
                              //.WhereIf(input.PaymentTypeIdFilter != null && input.PaymentTypeIdFilter.Count() > 0, e => input.PaymentModeIdFilter.Contains(e.PaymentJobId)) //pending                                                                                                                                                                   
                              .Where(e => e.TransportJobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);//6 for Ready to Dispatch lead status

            var pagedAndFilteredTransport = filteredtransport.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var transportDetails = from o in pagedAndFilteredTransport
                                     
                                   select new GetTransportTrackerForViewDto()
                                   {
                                       transportTracker = new TransportTrackerDto
                                       {
                                           Id = o.Id,
                                           leaddata = ObjectMapper.Map<LeadsDto>(o.TransportJobsIdFK.LeadFK),
                                           JobId = o.TransportJobId,
                                           ProjectNumber = o.TransportJobsIdFK.JobNumber,
                                           CustomerName = o.TransportJobsIdFK.LeadFK == null ? null : o.TransportJobsIdFK.LeadFK.CustomerName,
                                           Address = (o.TransportJobsIdFK.LeadFK.AddressLine1 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine1 + ", ") + (o.TransportJobsIdFK.LeadFK.AddressLine2 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine2 + ", ") + (o.TransportJobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.TransportJobsIdFK.LeadFK.Pincode == null ? "" : o.TransportJobsIdFK.LeadFK.Pincode),
                                           Mobile = o.TransportJobsIdFK.LeadFK == null ? null : o.TransportJobsIdFK.LeadFK.MobileNumber,
                                           VehicalType = o.VehicalIdFK.VehicalType,
                                           Regonumber = o.VehicalIdFK.VehicalRegNo,
                                           DriverName = o.DriverName,
                                           LeadStatusName = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk == null ? null : o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                           FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.TransportJobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                           FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                           Notes = o.TransportJobsIdFK.LeadFK.Notes,
                                           Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                           TransportDate = (DateTime?)o.TransportDate,
                                           DispatchDate = (DateTime?)_dispatchMaterialDetailsRepository.GetAll().Where(x => x.DispatchJobId == o.TransportJobId).FirstOrDefault().DispatchDate, //s4.DispatchDate, 

                                       },
                                       transportTrackerSummaryCount = transportTrackerSummaryCount(filteredtransport.ToList())
                                   };
            var totalCount = await filteredtransport.CountAsync();

            return new PagedResultDto<GetTransportTrackerForViewDto>(totalCount, await transportDetails.ToListAsync()) ;
        }
        public TransportTrackerSummaryCount transportTrackerSummaryCount(List<TransportDetails> filteredLeads)
        {
            var output = new TransportTrackerSummaryCount();
            if (filteredLeads.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);
                var installCompletcount = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).ToList().Count();
                output.TotalPanel = Convert.ToString(panelCount);
                output.InstallationComplet = Convert.ToString(installCompletcount);
                output.TotalKilowatt = Convert.ToString(filteredLeads.Sum(x => x.TransportJobsIdFK.PvCapacityKw));
            }
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditTransportDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_me)]
        protected virtual async Task Create(CreateOrEditTransportDto input)
        {
            var transportDetails = ObjectMapper.Map<TransportDetails>(input);
            if (transportDetails != null && input.DispatchJobList != null && input.DispatchDate != null)
            {
                var jobDispatch = _dispatchMaterialDetailsRepository.GetAll().Include(x => x.DispatchJobsIdFK).Where(x => input.DispatchJobList.Contains(x.DispatchJobId)).ToList();
                if (jobDispatch != null)
                {
                    //var result = from dispatch in jobDispatch
                    //             select new DispatchMaterialDetails()
                    //             {
                    //                 Id= dispatch.Id,
                    //                 DispatchHeightStructureId = dispatch.DispatchHeightStructureId,
                    //                 DispatchJobId = dispatch.DispatchJobId,
                    //                 DispatchTypeId = dispatch.DispatchTypeId,
                    //                 DispatchDate = input.DispatchDate,
                    //                 DispatchNotes = dispatch.DispatchNotes,
                    //                 LastModificationTime = DateTime.Now,
                    //                 LastModifierUserId = AbpSession.UserId
                    //             };
                    foreach (var item in jobDispatch)
                    {
                        item.DispatchDate = input.DispatchDate;
                        _dispatchMaterialDetailsRepository.UpdateAsync(item).Wait();
                        //await _dbContextProvider.GetDbContext().dispatchMaterialDetails.UpdateAsync(item);
                        await _dbContextProvider.GetDbContext().SaveChangesAsync();
                    }

                }

                transportDetails.VehicalId = input.VehicalId;
                transportDetails.DriverName = input.DriverName;
                transportDetails.TransportDate = input.TransportDate;
                transportDetails.TransportNote = input.TransportNote;
                List<TransportDetails> data = new List<TransportDetails>();
                List<LeadActivityLogs> dataactivity = new List<LeadActivityLogs>();
                foreach (var item in input.DispatchJobList)
                {
                    transportDetails.TransportJobId = item;
                    data.Add(transportDetails);

                    if (transportDetails != null)
                    {
                        //Add Activity Log Installer
                        LeadActivityLogs leadactivity = new LeadActivityLogs();
                        leadactivity.LeadActionId = 15;
                        leadactivity.SectionId = 63;
                        leadactivity.LeadActionNote = "Transport Details Created";
                        leadactivity.LeadId = jobDispatch.Where(x => x.DispatchJobId == item).FirstOrDefault().DispatchJobsIdFK.LeadId;
                        leadactivity.OrganizationUnitId = input.OrgID;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        dataactivity.Add(leadactivity);
                        //await _leadactivityRepository.InsertAsync(leadactivity);
                    }
                }
                await _dbContextProvider.GetDbContext().leadActivityLogs.AddRangeAsync(dataactivity);
                await _dbContextProvider.GetDbContext().transportDetails.AddRangeAsync(data);
                await _dbContextProvider.GetDbContext().SaveChangesAsync();

            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund_Edit)]
        protected virtual async Task Update(CreateOrEditTransportDto input)
        {
            var transportDetails = _transportDetailsRepository.GetAll().Include(x => x.TransportJobsIdFK).Where(x => x.TransportJobId == input.TransportJobId && x.Id == input.Id).FirstOrDefault();
            if (transportDetails != null && input.DispatchDate != null && input.DispatchJobList != null)
            {
                var jobDispatch = await _dispatchMaterialDetailsRepository.GetAll().Where(x => input.DispatchJobList.Contains(x.DispatchJobId)).ToListAsync();
                if (jobDispatch != null)
                {
                    //jobDispatch.Select(x => x.DispatchDate = input.DispatchDate); // = input.DispatchDate;
                    var result = from dispatch in jobDispatch
                                 select new DispatchMaterialDetails()
                                 {
                                     DispatchHeightStructureId = dispatch.DispatchHeightStructureId,
                                     DispatchJobId = dispatch.DispatchJobId,
                                     DispatchTypeId = dispatch.DispatchTypeId,
                                     DispatchDate = input.DispatchDate,
                                     DispatchNotes = dispatch.DispatchNotes,
                                     LastModificationTime = DateTime.Now
                                 };
                    //await _dispatchMaterialDetailsRepository.InsertAndGetIdAsync(jobDispatch);
                    _dbContextProvider.GetDbContext().dispatchMaterialDetails.UpdateRange(result);
                    //await _dbContextProvider.GetDbContext().SaveChangesAsync();
                }

                List<TransportDetails> data = new List<TransportDetails>();
                List<LeadActivityLogs> dataactivity = new List<LeadActivityLogs>();
                foreach (var item in input.DispatchJobList)
                {
                    transportDetails.VehicalId = input.VehicalId;
                    transportDetails.DriverName = input.DriverName;
                    transportDetails.TransportDate = input.TransportDate;
                    transportDetails.TransportNote = input.TransportNote;
                    transportDetails.TransportJobId = item;
                    data.Add(transportDetails);

                    if (transportDetails != null)
                    {
                        //Add Activity Log Installer
                        LeadActivityLogs leadactivity = new LeadActivityLogs();
                        leadactivity.LeadActionId = 15;
                        leadactivity.SectionId = 63;
                        leadactivity.LeadActionNote = "Transport Details Created";
                        leadactivity.LeadId = jobDispatch.Where(x => x.DispatchJobId == item).FirstOrDefault().DispatchJobsIdFK.LeadId;
                        leadactivity.OrganizationUnitId = input.OrgID;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        dataactivity.Add(leadactivity);
                        //await _leadactivityRepository.InsertAsync(leadactivity);
                    }
                }
                _dbContextProvider.GetDbContext().leadActivityLogs.UpdateRange(dataactivity);
                _dbContextProvider.GetDbContext().transportDetails.UpdateRange(data);
                await _dbContextProvider.GetDbContext().SaveChangesAsync();

                //var TransportItem = await _transportDetailsRepository.UpdateAsync(transportDetails);
                //#region Activity Log
                ////Add Activity Log
                //LeadActivityLogs leadactivity = new LeadActivityLogs();
                //leadactivity.LeadActionId = 15;
                ////leadactivity.LeadAcitivityID = 0;
                //leadactivity.SectionId = 63;
                //leadactivity.LeadActionNote = "Transport Details Modified";
                //leadactivity.LeadId = transportDetails.TransportJobsIdFK.LeadId;// input.leadid;
                //if (AbpSession.TenantId != null)
                //{
                //    leadactivity.TenantId = (int)AbpSession.TenantId;
                //}
                //var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                //var List = new List<LeadtrackerHistory>();
                //#endregion
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_MeterConnect_ExportToExcel)]
        public async Task<FileDto> GetTransportTrackerToExcel(GetAllTransportTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<DateTime> dispatchDatelist = new List<DateTime>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);


            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            if (input.BillTypeFilter == "ss" || input.BillTypeFilter == "both")
            {
                sspaymentlist = _ssPaymentDetailsRepository.GetAll().Include(x => x.PaymentIdFK).Select(x => x.PaymentIdFK.PaymentJobId).ToList();
            }
            if (input.BillTypeFilter == "stc" || input.BillTypeFilter == "both")
            {
                stcpaymentlist = _stcPaymentDetailsRepository.GetAll().Include(x => x.PaymentIdFK).Select(x => x.PaymentIdFK.PaymentJobId).ToList();
            }
            if (input.FilterbyDate == "dispatchdate")
            {
                dispatchDatelist = _dispatchMaterialDetailsRepository.GetAll().Select(x => x.DispatchDate).ToList();
            }
            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var filteredtransport = _transportDetailsRepository.GetAll().Include(x => x.TransportJobsIdFK).Include(x => x.VehicalIdFK)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TransportJobsIdFK.JobNumber.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.CustomerName.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.TransportJobsIdFK.LeadId)))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "stc", e => stcpaymentlist.Contains(e.TransportJobId))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "ss", e => sspaymentlist.Contains(e.TransportJobId))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "both", e => stcpaymentlist.Contains(e.TransportJobId) && sspaymentlist.Contains(e.TransportJobId))
                        .WhereIf(input.VehicalType != null && input.VehicalType.Count() > 0, e => input.VehicalType.Contains((int)e.VehicalIdFK.Id))
                        .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.DiscomId))
                        .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SolarTypeId))
                        .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.ChanelPartnerID))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "transportdate", e => e.TransportDate != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "dispatchdate", e => dispatchDatelist != null)
                              //.WhereIf(input.PaymentTypeIdFilter != null && input.PaymentTypeIdFilter.Count() > 0, e => input.PaymentModeIdFilter.Contains(e.PaymentJobId)) //pending                                                                                                                                                                   
                              .Where(e => e.TransportJobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);//6 for Ready to Dispatch lead status

            var pagedAndFilteredTransport = filteredtransport.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var transportDetails = from o in pagedAndFilteredTransport

                                   select new GetTransportTrackerForViewDto()
                                   {
                                       transportTracker = new TransportTrackerDto
                                       {
                                           Id = o.Id,
                                           leaddata = ObjectMapper.Map<LeadsDto>(o.TransportJobsIdFK.LeadFK),
                                           JobId = o.TransportJobId,
                                           ProjectNumber = o.TransportJobsIdFK.JobNumber,
                                           CustomerName = o.TransportJobsIdFK.LeadFK == null ? null : o.TransportJobsIdFK.LeadFK.CustomerName,
                                           Address = (o.TransportJobsIdFK.LeadFK.AddressLine1 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine1 + ", ") + (o.TransportJobsIdFK.LeadFK.AddressLine2 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine2 + ", ") + (o.TransportJobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.TransportJobsIdFK.LeadFK.Pincode == null ? "" : o.TransportJobsIdFK.LeadFK.Pincode),
                                           Mobile = o.TransportJobsIdFK.LeadFK == null ? null : o.TransportJobsIdFK.LeadFK.MobileNumber,
                                           VehicalType = o.VehicalIdFK.VehicalType,
                                           Regonumber = o.VehicalIdFK.VehicalRegNo,
                                           DriverName = o.DriverName,
                                           LeadStatusName = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk == null ? null : o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                           FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.TransportJobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                           FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                           Notes = o.TransportJobsIdFK.LeadFK.Notes,
                                           Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                           TransportDate = (DateTime?)o.TransportDate,
                                           DispatchDate = (DateTime?)_dispatchMaterialDetailsRepository.GetAll().Where(x => x.DispatchJobId == o.TransportJobId).FirstOrDefault().DispatchDate, //s4.DispatchDate, 

                                       },
                                       transportTrackerSummaryCount = transportTrackerSummaryCount(filteredtransport.ToList())
                                   };
            var transporttrackerListDtos = await transportDetails.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _transportTrackerExcelExporter.ExportToFile(transporttrackerListDtos);
            }
            else
            {
                return _transportTrackerExcelExporter.ExportToFile(transporttrackerListDtos);
            }

        }
    }
}
