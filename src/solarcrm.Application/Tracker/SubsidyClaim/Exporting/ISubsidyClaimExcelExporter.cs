﻿using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.SubsidyClaim.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyClaim.Exporting
{
    public interface ISubsidyClaimExcelExporter
    {
        FileDto ExportToFile(List<GetSubsidyClaimForViewDto> subsidyClaim);
    }
}
