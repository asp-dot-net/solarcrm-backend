﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.SubsidyClaim.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyClaim.Exporting
{
    public class SubsidyClaimExcelExporter : NpoiExcelExporterBase, ISubsidyClaimExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SubsidyClaimExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetSubsidyClaimForViewDto> claimtrack)
        {
            return CreateExcelPackage(
                "SubsidyClaimDetails.xlsx",
                 excelPackage =>
                 {
                     var sheet = excelPackage.CreateSheet(L("SubsidyClaimDetails"));

                     AddHeader(
                         sheet,
                         L("ClaimNumber"),
                         L("Division"), 
                         L("TotalApplications"),
                         L("TotalProjectCost"),
                         L("SubsidyAmount"),
                         L("PGBDeduction"),
                         L("PenaltyDeduction"),
                         L("Anyotherduesdeduction"),
                         L("NetSubsidyPayable"),
                         L("ClaimStatus"),
                         L("VerifiedOrNot"),
                         L("filesubmitdate")
                         );
                     AddObjects(
                         sheet, claimtrack,
                         _ => _.subsidyClaim.SubsidyClaimNumber,
                         _ => _.subsidyClaim.DivisionName,
                         _ => _.subsidyClaim.ClaimTotalProjectCost,
                         _ => _.subsidyClaim.ClaimTotalSubsidyAmount,
                         _ => _.subsidyClaim.ClaimPBGDeduction,
                         _ => _.subsidyClaim.ClaimPenaltyDeduction,
                         _ => _.subsidyClaim.ClaimAnyotherduesdeduction,
                         _ => _.subsidyClaim.ClaimNetSubsidyPayable,
                         _ => _.subsidyClaim.ClaimStatus,
                         _ => _.subsidyClaim.ClaimVerifiedORnot,
                         _ => _.subsidyClaim.FileSubmitDate
                         );
                 });
        }
    }
}
