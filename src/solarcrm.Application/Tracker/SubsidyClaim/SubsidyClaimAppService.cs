﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults;
using solarcrm.Dispatch;
using solarcrm.EntityFrameworkCore;
using solarcrm.Job;
using solarcrm.JobInstallation;
using solarcrm.MeterConnect;
using solarcrm.Payments;
using solarcrm.Subsidy;
using solarcrm.Tracker.SubsidyTracker.Dto;
using solarcrm.Tracker.SubsidyTracker;
using solarcrm.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solarcrm.Tracker.SubsidyClaim.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Collections.Extensions;
using System.Data;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using NPOI.SS.Formula.Functions;
using Abp.Authorization;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.Authorization;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.SubsidyTracker.Exporting;
using solarcrm.Tracker.SubsidyClaim.Exporting;

namespace solarcrm.Tracker.SubsidyClaim
{
    public class SubsidyClaimAppService : solarcrmAppServiceBase, ISubsidyClaimAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<SubsidyClaimDetails> _subsidyCliamDetailsRepository;
        private readonly IRepository<SubsidyClaimProjectDetails> _subsidyclaimProjectDetailsRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobInstallationDetail> _jobInstallRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<SSPaymentDetails> _ssPaymentDetailsRepository;
        private readonly IRepository<STCPaymentDetails> _stcPaymentDetailsRepository;
        private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<MeterConnectDetails> _meterConnectDetailsRepository;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailsJobRepository;
        private readonly UserManager _userManager;
        private readonly ISubsidyClaimExcelExporter _SubsidyClaimExcelExporter;
        public SubsidyClaimAppService(IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<TransportDetails> transportDetailsRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<DispatchMaterialDetails> dispatchMaterialDetailsRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IAbpSession abpSession, UserManager userManager,
            IRepository<JobInstallationDetail> jobInstallRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<User, long> userRepository,
            IRepository<SSPaymentDetails> ssPaymentDetailsRepository,
            IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<STCPaymentDetails> stcPaymentDetailsRepository,
            IRepository<MeterConnectDetails> meterConnectDetailsRepository,
            IRepository<SubsidyClaimDetails> subsidyCliamDetailsRepository,
            IRepository<SubsidyClaimProjectDetails> subsidyclaimProjectDetailsRepository,
        IRepository<PaymentDetailsJob> paymentDetailsJobRepository,
        ISubsidyClaimExcelExporter SubsidyClaimExcelExporter)
        {
            _dbContextProvider = dbContextProvider;
            _subsidyCliamDetailsRepository = subsidyCliamDetailsRepository;
            _jobRepository = jobRepository;
            _subsidyclaimProjectDetailsRepository = subsidyclaimProjectDetailsRepository;
            _leadactivityRepository = leadactivityRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _userRepository = userRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
            _jobInstallRepository = jobInstallRepository;
            _ssPaymentDetailsRepository = ssPaymentDetailsRepository;
            _stcPaymentDetailsRepository = stcPaymentDetailsRepository;
            _applicationstatusRepository = applicationstatusRepository;
            _meterConnectDetailsRepository = meterConnectDetailsRepository;
            _paymentDetailsJobRepository = paymentDetailsJobRepository;
            _userManager = userManager;
            _SubsidyClaimExcelExporter = SubsidyClaimExcelExporter;
        }
        public async Task<PagedResultDto<GetSubsidyClaimForViewDto>> GetAllSubsidyClaim(GetAllSubsidyClaimInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<DateTime> dispatchDatelist = new List<DateTime>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

            var jobInstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).Select(x => x.JobId);
            var InstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null);
            var MeterConnectList = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId);
            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            //var PaymentDataList = _paymentDetailsJobRepository.GetAll();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
           // var paymentDetail = _paymentDetailsJobRepository.GetAll().ToList();
            var productItem = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk).Where(x=> x.ProductItemFk.CategoryId == 2).ToList();
            
            var filteredsubsidy = _subsidyclaimProjectDetailsRepository.GetAll().Include(x => x.SubsidyJobFKId).Include(x => x.SubsidyClaimFKId).Include(x => x.SubsidyJobFKId.LeadFK)
                .Include(x => x.SubsidyJobFKId.LeadFK.SubDivisionIdFk).Include(x => x.SubsidyJobFKId.LeadFK.DiscomIdFk).Include(x => x.SubsidyJobFKId.LeadFK.CircleIdFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.SubsidyJobFKId.JobNumber.Contains(input.Filter)
                                || e.SubsidyJobFKId.LeadFK.CustomerName.Contains(input.Filter) || e.SubsidyJobFKId.LeadFK.EmailId.Contains(input.Filter)
                                || e.SubsidyJobFKId.LeadFK.Alt_Phone.Contains(input.Filter) || e.SubsidyJobFKId.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.SubsidyJobFKId.LeadId)))
                        .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.SubsidyJobFKId.LeadFK.LeadStatusId))
                        .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.SubsidyJobFKId.ApplicationStatusId))
                        .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(1)))
                        .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(2)))
                        .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                        .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                        .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.DiscomId))
                        //.WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.CircleId))
                        .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.DivisionId))
                        //.WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SubDivisionId))
                        .WhereIf(input.employeeIdFilter != null && input.employeeIdFilter.Count() > 0, e => input.employeeIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.ChanelPartnerID))
                        .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.SolarTypeId))
                        .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.ChanelPartnerID))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidyclaimdate", e => MeterConnectList.Select(x => x.MeterAgreementSentDate) != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidypaiddate", e => MeterConnectList.Select(x => x.MeterSubmissionDate) != null)
                        .Where(e => e.SubsidyJobFKId.LeadFK.OrganizationUnitId == input.OrganizationUnit);

            var pagedAndFilteredClaim = filteredsubsidy.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var subsidyDetails = (from o in pagedAndFilteredClaim
                                   
                                  join bp in productItem on o.SubsidyJobId equals bp.JobId into j3
                                  from j in j3.DefaultIfEmpty()

                                  group o by o.SubsidyClaimId into newGroup
                                  //join o3 in _subsidyCliamDetailsRepository.GetAll() on o.SubsidyClaimId equals o3.Id into j3
                                  //    from s3 in j3.DefaultIfEmpty()
                                  //join o3 in _jobProductItemRepository.GetAll() on newGroup.FirstOrDefault().SubsidyJobId equals o3.JobId into j2
                                  //from s2 in j2.DefaultIfEmpty()
                                  //join bp in productItem on newGroup.FirstOrDefault().SubsidyJobId equals bp.JobId

                                  select new GetSubsidyClaimForViewDto()
                                  {
                                      subsidyClaim = new SubsidyClaimDto
                                      {
                                          Id = newGroup.Select(x => x.Id).FirstOrDefault(),
                                          leaddata = ObjectMapper.Map<LeadsDto>(newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK),
                                          Scheme = newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK.AreaType,
                                          DiscomName = newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK.DiscomIdFk.Name,
                                          DivisionName = newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK.DivisionIdFk.Name,
                                          SubsidyClaimNumber = newGroup.FirstOrDefault().SubsidyClaimFKId.SubsidyClaimNo,
                                          ClaimTotalApplications = Convert.ToInt32(newGroup.Select(x => x.SubsidyJobFKId.Id).Count()),
                                          ClaimRegisterCapacityKW = newGroup.Sum(x => x.SubsidyJobFKId.PvCapacityKw),
                                          ClaimInstalledPVModuleCapacityKW = newGroup.Sum(x => x.SubsidyJobFKId.PvCapacityKw),
                                          ClaimInstalledInverterCapacityKW = 0, //productItem.Select(x => x.JobId == 22).ToList().Sum(),
                                          // //productItem.ToList().Select(x => x.JobId).Contains(newGroup.FirstOrDefault().SubsidyJobId).Sum(x=>x.),
                                          //productItem.Where(x=>x.JobId == (int?)newGroup.FirstOrDefault().SubsidyJobId).Select(x=>x.ProductItemFk.Size).Sum(), //productItem.Where(x=> newGroup.Select(x => x.SubsidyJobId).Contains(Convert.ToInt32(x.JobId))).Sum(x => x.Id), //_jobProductItemRepository.GetAll().Include(x => x.ProductItemFk).Where(x => x.JobId == o.SubsidyJobId && x.Quantity == 1).FirstOrDefault().ProductItemFk.Size,
                                          ClaimTotalProjectCost = newGroup.Sum(x => x.SubsidyJobFKId.NetSystemCost),
                                          ClaimTotalSubsidyAmount = newGroup.Sum(x=>x.SubsidyJobFKId.JobTotalSubsidyAmount),  //paymentDetail.Where(e => newGroup.Select(x => x.SubsidyJobId).Contains(e.PaymentJobId)).Sum(e => e.LessSubsidy),
                                          ClaimPBGDeduction = Convert.ToDecimal(((newGroup.Sum(x => x.SubsidyJobFKId.NetSystemCost)) * Convert.ToDecimal(0.03))),
                                          ClaimPenaltyDeduction = 0,
                                          ClaimAnyotherduesdeduction = 0,
                                          ClaimNetSubsidyPayable = 0,// Convert.ToDecimal(((newGroup.Sum(x => x.SubsidyJobFKId.NetSystemCost) * Convert.ToDecimal(0.03)) - paymentDetail.Where(x => newGroup.Select(x => x.SubsidyJobId).Contains(x.PaymentJobId)).Sum(x => x.LessSubsidy))),
                                          ClaimStatus = "",
                                          ClaimVerifiedORnot = "",
                                          ClaimHandOverPerson = "",
                                          Date = "",
                                          FileSubmitDate = DateTime.Now,
                                      },
                                      subsidyClaimSummaryCount = subsidyClaimSummaryCount(filteredsubsidy.ToList())
                                  });//.GroupBy(x => x.subsidyClaim.SubsidyClaimId);
            var totalCount = await filteredsubsidy.GroupBy(x=>x.SubsidyClaimId).CountAsync();
            return new PagedResultDto<GetSubsidyClaimForViewDto>(totalCount, await subsidyDetails.ToListAsync());
        }
        public SubsidyClaimSummaryCount subsidyClaimSummaryCount(List<SubsidyClaimProjectDetails> filteredLeads)
        {
            var output = new SubsidyClaimSummaryCount();
            if (filteredLeads.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);
                var installCompletcount = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).ToList().Count();
                output.TotalSubsidyReceived = Convert.ToString(panelCount);
                output.TotalSubsidyInProcess = Convert.ToString(installCompletcount);
                output.TotalProjectsClaimed = Convert.ToString(installCompletcount);
                output.TotalKilowatt = "";
            }
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_ExportToExcel)]
        public async Task<FileDto> GetSubsidyClaimToExcel(GetAllSubsidyTrackerInput input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
                List<int> sspaymentlist = new List<int>();
                List<int> stcpaymentlist = new List<int>();
                List<int> FollowupList = new List<int>();
                List<int> NextFollowupList = new List<int>();
                List<int> Assign = new List<int>();
                List<int> jobnumberlist = new List<int>();
                List<DateTime> dispatchDatelist = new List<DateTime>();
                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                var User_List = _userRepository.GetAll();
                var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

                var jobInstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).Select(x => x.JobId);
                var InstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null);
                var MeterConnectList = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId);
                var job_list = _jobRepository.GetAll();
                //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
                var PaymentDataList = _paymentDetailsJobRepository.GetAll();
                if (input.Filter != null)
                {
                    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                }

                if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
                {
                    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                }
                var paymentDetail = _paymentDetailsJobRepository.GetAll().ToList();
                var productItem = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk);
                var filteredsubsidy = _subsidyclaimProjectDetailsRepository.GetAll().Include(x => x.SubsidyJobFKId).Include(x => x.SubsidyClaimFKId).Include(x => x.SubsidyJobFKId.LeadFK)
                    .Include(x => x.SubsidyJobFKId.LeadFK.SubDivisionIdFk).Include(x => x.SubsidyJobFKId.LeadFK.DiscomIdFk).Include(x => x.SubsidyJobFKId.LeadFK.CircleIdFk)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.SubsidyJobFKId.JobNumber.Contains(input.Filter)
                                    || e.SubsidyJobFKId.LeadFK.CustomerName.Contains(input.Filter) || e.SubsidyJobFKId.LeadFK.EmailId.Contains(input.Filter)
                                    || e.SubsidyJobFKId.LeadFK.Alt_Phone.Contains(input.Filter) || e.SubsidyJobFKId.LeadFK.MobileNumber.Contains(input.Filter)
                                    //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                    //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                    || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.SubsidyJobFKId.LeadId)))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.SubsidyJobFKId.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.SubsidyJobFKId.ApplicationStatusId))
                            .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(1)))
                            .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(2)))
                            .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                            .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.DiscomId))
                            //.WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.CircleId))
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.DivisionId))
                            //.WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SubDivisionId))
                            .WhereIf(input.employeeIdFilter != null && input.employeeIdFilter.Count() > 0, e => input.employeeIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.ChanelPartnerID))
                            .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.SolarTypeId))
                            .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.SubsidyJobFKId.LeadFK.ChanelPartnerID))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidyclaimdate", e => MeterConnectList.Select(x => x.MeterAgreementSentDate) != null)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidypaiddate", e => MeterConnectList.Select(x => x.MeterSubmissionDate) != null)
                            .Where(e => e.SubsidyJobFKId.LeadFK.OrganizationUnitId == input.OrganizationUnit);

                var pagedAndFilteredClaim = filteredsubsidy.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                var subsidyDetails = (from o in pagedAndFilteredClaim
                                      group o by o.SubsidyClaimId into newGroup
                                      //join o3 in _subsidyCliamDetailsRepository.GetAll() on o.SubsidyClaimId equals o3.Id into j3
                                      //    from s3 in j3.DefaultIfEmpty()

                                      select new GetSubsidyClaimForViewDto()
                                      {
                                          subsidyClaim = new SubsidyClaimDto
                                          {
                                              Id = newGroup.Select(x => x.Id).FirstOrDefault(),
                                              leaddata = ObjectMapper.Map<LeadsDto>(newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK),
                                              Scheme = newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK.AreaType,
                                              DiscomName = newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK.DiscomIdFk.Name,
                                              DivisionName = newGroup.FirstOrDefault().SubsidyJobFKId.LeadFK.DivisionIdFk.Name,
                                              SubsidyClaimNumber = newGroup.FirstOrDefault().SubsidyClaimFKId.SubsidyClaimNo,
                                              ClaimTotalApplications = Convert.ToInt32(newGroup.Select(x => x.SubsidyJobFKId.Id).Count()),
                                              ClaimRegisterCapacityKW = newGroup.Sum(x => x.SubsidyJobFKId.PvCapacityKw),
                                              ClaimInstalledPVModuleCapacityKW = newGroup.Sum(x => x.SubsidyJobFKId.PvCapacityKw),
                                              //ClaimInstalledInverterCapacityKW = productItem.Where(x=> newGroup.Select(x => x.SubsidyJobId).Contains(Convert.ToInt32(x.JobId))).Sum(x => x.ProductItemFk.Size), //_jobProductItemRepository.GetAll().Include(x => x.ProductItemFk).Where(x => x.JobId == o.SubsidyJobId && x.Quantity == 1).FirstOrDefault().ProductItemFk.Size,
                                              ClaimTotalProjectCost = newGroup.Sum(x => x.SubsidyJobFKId.NetSystemCost),
                                              //ClaimTotalSubsidyAmount = paymentDetail.Where(e=>(newGroup.Select(x => x.SubsidyJobId).Contains(e.PaymentJobId))).Sum(e=>e.LessSubsidy),
                                              ClaimPBGDeduction = Convert.ToDecimal(((newGroup.Sum(x => x.SubsidyJobFKId.NetSystemCost)) * Convert.ToDecimal(0.03))),
                                              ClaimPenaltyDeduction = 0,
                                              ClaimAnyotherduesdeduction = 0,
                                              //ClaimNetSubsidyPayable = Convert.ToDecimal(((newGroup.Sum(x => x.SubsidyJobFKId.NetSystemCost) * Convert.ToDecimal(0.03)) - paymentDetail.Where(x => newGroup.Select(x => x.SubsidyJobId).Contains(x.PaymentJobId)).Sum(x => x.LessSubsidy))),
                                              ClaimStatus = "",
                                              ClaimVerifiedORnot = "",
                                              ClaimHandOverPerson = "",
                                              Date = "",
                                              FileSubmitDate = DateTime.Now,
                                          },
                                          subsidyClaimSummaryCount = subsidyClaimSummaryCount(filteredsubsidy.ToList())
                                      });//.GroupBy(x => x.subsidyClaim.SubsidyClaimId);
                var totalCount = await filteredsubsidy.CountAsync();
                var applicationtrackerListDtos = subsidyDetails.ToList();
                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _SubsidyClaimExcelExporter.ExportToFile(applicationtrackerListDtos);
                }
                else
                {
                    return _SubsidyClaimExcelExporter.ExportToFile(applicationtrackerListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
