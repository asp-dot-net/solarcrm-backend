﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Organizations;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.EntityFrameworkCore;
using solarcrm.Job.JobRefunds;
using solarcrm.Tracker.RefundTracker.Dto;
using solarcrm.Tracker.RefundTracker.Exporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solarcrm.Jobs.Dto;
using solarcrm.Organizations;
using System.Data;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.DataVaults.Leads.Dto;
using Newtonsoft.Json;
using Abp.Authorization;
using solarcrm.Authorization;
using solarcrm.Dto;
using solarcrm.ApplicationTracker.Dto;

namespace solarcrm.Tracker.RefundTracker
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Refund)]
    public class RefundTrackerAppService : solarcrmAppServiceBase, IRefundTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<JobRefunds> _jobRefundRepository;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRefundTrackerExcelExporter _RefundTrackerExcelExporter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly IRepository<DocumentList> _documentRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        // private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public RefundTrackerAppService(IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<JobRefunds> jobRefundRepository,
            IRepository<Job.Jobs> jobRepository,
            IRefundTrackerExcelExporter RefundTrackerExcelExporter,
            IRepository<User, long> userRepository,
            IRepository<DocumentList> documentRepository,
            IRepository<LeadStatus, int> lookup_leadStatusRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            UserManager userManager,
            ITimeZoneConverter timeZoneConverter,
            IRepository<LeadDocuments> leadDocumentRepository,
            // IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _jobRefundRepository = jobRefundRepository;
            _RefundTrackerExcelExporter = RefundTrackerExcelExporter;
            _userRepository = userRepository;
            _userManager = userManager;
            _documentRepository = documentRepository;
            _leadDocumentRepository = leadDocumentRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            //   _applicationstatusRepository = applicationstatusRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Refund)]
        public async Task<PagedResultDto<GetRefundTrackerForViewDto>> GetAllRefundTracker(GetAllRefundTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);

            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            var jobnumberlist = new List<int>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            var FollowupList = new List<int>();
            var NextFollowupList = new List<int>();
            var Assign = new List<int>();

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }

            var DocumentListMasterData = _documentRepository.GetAll();
            var DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id);

            var leadDoclistid = new List<int>();
            var LeadDocument = _leadDocumentRepository.GetAll();


            // var DocumentListMasterData = _documentRepository.GetAll();
            // var DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id);

            // var leadDoclistid = new List<int>();
            // var LeadDocument = _leadDocumentRepository.GetAll().Where(x => x.IsActive == false);

            if (DocumentListData.Count() != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }
            var filteredJob = _jobRefundRepository.GetAll().Include(e => e.JobIDFK).Include(e => e.JobIDFK.LeadFK).Include(x => x.JobIDFK.LeadFK.LeadStatusIdFk)
                .Include(x => x.JobIDFK.ApplicationStatusIdFK)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobIDFK.JobNumber.Contains(input.Filter)
                                || e.JobIDFK.LeadFK.CustomerName.Contains(input.Filter) || e.JobIDFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.JobIDFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.JobIDFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.JobIDFK.LeadId)))                
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.JobIDFK.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.JobIDFK.ApplicationStatusId))
                            .WhereIf(input.refundPenddingFilter != null && input.refundPenddingFilter == 1, e => e.RefundPaidDate != null && e.PaymentTypeId != 0 && e.ReceiptNo != null) // 1 for Refunded
                            .WhereIf(input.refundPenddingFilter != null && input.refundPenddingFilter == 2, e => e.IsRefundVerify == false) // 2 for Pendding
                            .WhereIf(input.refundVerifyFilter != null && input.refundVerifyFilter == 1, e => e.IsRefundVerify == false) // 1 for Not Verify
                            .WhereIf(input.refundVerifyFilter != null && input.refundVerifyFilter == 2, e => e.IsRefundVerify == true) // 2 for Verify
                            .WhereIf(input.refundReasonFilter != 0, e => input.refundReasonFilter == e.RefundReasonId)
                            .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.JobIDFK.LeadFK.ChanelPartnerID))
                            .Where(e => e.JobIDFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);

            var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o3 in _lookup_leadStatusRepository.GetAll() on o.JobIDFK.LeadFK.LeadStatusId equals o3.Id into j3
                       from s3 in j3.DefaultIfEmpty()

                           //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       select new GetRefundTrackerForViewDto()
                       {
                           refundTracker = new RefundTrackerDto
                           {
                               Id = o.Id,
                               RefundJobId = o.RefundJobId,
                               RefundReasonId = o.RefundReasonId,
                               PaymentTypeId = o.PaymentTypeId,
                               RefundPaidAmount = o.RefundPaidAmount,
                               RefundPaidDate = o.RefundPaidDate,
                               RefundPaymentType = o.PaymentTypeIDFK.Name,
                               RefundReasonName = o.RefundResIDFK.Name,
                               RefundPaidNotes = o.RefundPaidNotes,
                               IsRefundVerify = o.IsRefundVerify,
                               RefundVerifyDate = o.RefundVerifyDate,
                               RefundVerifyNotes = o.RefundVerifyNotes,
                               RefundVerifyById = o.RefundVerifyById,
                               BankName = o.BankName,
                               BankAccountHolderName = o.BankAccountHolderName,
                               BankBrachName = o.BankBrachName,
                               BankAccountNumber = o.BankAccountNumber,
                               RefundRecPath = o.RefundRecPath,
                               ReceiptNo = o.ReceiptNo,
                               JobRefundEmailSend = o.JobRefundEmailSend,
                               JobRefundEmailSendDate = o.JobRefundEmailSendDate,
                               JobRefundSmsSend = o.JobRefundSmsSend,
                               JobRefundSmsSendDate = o.JobRefundSmsSendDate,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.JobIDFK.LeadFK),
                               ProjectNumber = o.JobIDFK.JobNumber,
                               //JobStatus = o.JobStatusFk.Name,
                               CustomerName = o.JobIDFK.LeadFK.CustomerName,
                               Address = (o.JobIDFK.LeadFK.AddressLine1 == null ? "" : o.JobIDFK.LeadFK.AddressLine1 + ", ") + (o.JobIDFK.LeadFK.AddressLine2 == null ? "" : o.JobIDFK.LeadFK.AddressLine2 + ", ") + (o.JobIDFK.LeadFK.CityIdFk.Name == null ? "" : o.JobIDFK.LeadFK.CityIdFk.Name + ", ") + (o.JobIDFK.LeadFK.TalukaIdFk.Name == null ? "" : o.JobIDFK.LeadFK.TalukaIdFk.Name + ", ") + (o.JobIDFK.LeadFK.DiscomIdFk.Name == null ? "" : o.JobIDFK.LeadFK.DiscomIdFk.Name + ", ") + (o.JobIDFK.LeadFK.StateIdFk.Name == null ? "" : o.JobIDFK.LeadFK.StateIdFk.Name + "-") + (o.JobIDFK.LeadFK.Pincode == null ? "" : o.JobIDFK.LeadFK.Pincode),
                               Mobile = o.JobIDFK.LeadFK.MobileNumber,
                               LeadStatusName = o.JobIDFK.LeadFK.LeadStatusIdFk.Status,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.JobIDFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobIDFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Notes = o.JobIDFK.LeadFK.Notes,
                               LastModificationTime = o.LastModificationTime,
                               CreationTime = (DateTime)o.CreationTime,
                               //QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                               LeadStatusColorClass = o.JobIDFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.JobIDFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobIDFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.JobIDFK.ApplicationStatusIdFK,
                           },
                           refundTrackerSummaryCount = refundTrackerSummaryCount(filteredJob.ToList()),
                           //documentListName = (ObjectMapper.Map<List<DocumentListDto>>(DocumentListMasterData.Where(x => DocumentListData.Contains(x.Id)).ToList())),
                           //leaddocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(LeadDocument.Where(x => x.LeadDocumentId == o.LeadId && DocumentListData.Contains(x.DocumentId)).ToList())),

                           //ReminderTime = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId).OrderByDescending(e => e.LeadId == o.Id && e.LeadActionId == 8).FirstOrDefault().CreationTime,
                           ////(DateTime?)_reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                           //ActivityDescription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                           //ActivityComment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                           //ApplicationStatusName = _applicationstatusRepository.GetAll().Where(x => x.Id == s6.ApplicationStatusId).FirstOrDefault(), // the add the goverment status
                       };

            var totalCount = await filteredJob.CountAsync();

            return new PagedResultDto<GetRefundTrackerForViewDto>(totalCount, await jobs.ToListAsync());
        }
        public RefundTrackerSummaryCount refundTrackerSummaryCount(List<JobRefunds> filteredLeads)
        {
            var output = new RefundTrackerSummaryCount();
            output.Total = Convert.ToString(filteredLeads.Sum(x=>x.RefundPaidAmount));
            output.Pending = Convert.ToString(filteredLeads.Where(e => e.IsRefundVerify == false || e.IsRefundVerify == null).Sum(x=>x.RefundPaidAmount));
            output.Refunded = Convert.ToString(filteredLeads.Where(e => e.IsRefundVerify == true).Sum(x => x.RefundPaidAmount));
            return output;
        }
        //public async Task<ApplicationTrackerSummaryCount> getapplicationTrackerSummaryCount(GetAllLeadsInput input)
        //{
        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //    int UserId = (int)AbpSession.UserId;

        //    var DocumentListData = await _documentRepository.GetAll().Where(x => x.IsATFlag == true).Select(x => x.Id).ToListAsync();
        //    var leadDoclistid = new List<int>();
        //    var LeadDocument = await _leadDocumentRepository.GetAll().ToListAsync();
        //    if (DocumentListData.Count != 0)
        //    {
        //        leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
        //    }
        //    //var Job_list = _jobRepository.GetAll();
        //    var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.ChanelPartnerID == UserId && e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
        //    //      var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true);

        //    //var jobnumberlist = new List<int?>();
        //    var filteredLeads = _jobRepository.GetAll().Include(e => e.LeadFK)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFK.FirstName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter) || e.LeadFK.AddressLine1.Contains(input.Filter))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.FirstNameFilter), e => e.LeadFK.FirstName == input.FirstNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.LeadFK.EmailId == input.EmailFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.LeadFK.Alt_Phone == input.PhoneFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.LeadFK.MobileNumber == input.MobileFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.LeadFK.AddressLine1.Contains(input.AddressFilter))


        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
        //                .WhereIf(input.OrganizationUnit != null, e => e.OrganizationUnitId == input.OrganizationUnit);
        //    //.Where(e => !statusIds.Contains(e.LeadStatusId))
        //    //.WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => joblist.Contains(e.Id))
        //    //.WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId));


        //    //var LeadidList = Job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();
        //    var output = new ApplicationTrackerSummaryCount();

        //    output.Total = Convert.ToString(filteredLeads.Count());
        //    output.ApplicationPending = Convert.ToString(filteredLeads.Where(e => e.LeadFK.IsVerified && leadDoclistid.Count != 0 && e.ApplicationDate == null && e.ApplicationNumber == null).Count());
        //    output.OTP_Pending = Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
        //    output.ApplicationSubmited = Convert.ToString(filteredLeads.Where(e => e.ApplicationDate != null).Count());
        //    output.FeasiblityApproved = ""; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
        //    output.MeterInstalled = ""; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
        //    output.SubcidyClaimed = ""; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
        //    //output.Sold = Convert.ToString(filteredLeads.Where(e => LeadidList.Contains(e.Id)).Count());
        //    return output;
        //}

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Refund_QuickView)]
        public async Task<RefundTrackerQuickViewDto> GetRefundQuickView(EntityDto input)
        {
            try
            {
                //var leads = _leadRepository.FirstOrDefault(input.Id);
                var job = _jobRefundRepository.GetAll().Include(e => e.JobIDFK).

                    Where(e => e.Id == input.Id).FirstOrDefault();

                var output = new RefundTrackerQuickViewDto
                {
                    //Job = ObjectMapper.Map<CreateOrEditJobDto>(job),

                    ProjectNumber = job.JobIDFK.JobNumber,
                    Latitude = (decimal?)job.JobIDFK.LeadFK.Latitude,
                    Longitude = (decimal?)job.JobIDFK.LeadFK.Longitude,
                    AvgmonthlyBill = (int?)job.JobIDFK.LeadFK.AvgmonthlyBill,
                    AvgmonthlyUnit = (int?)job.JobIDFK.LeadFK.AvgmonthlyUnit,
                    CustomerName = job.JobIDFK.LeadFK.CustomerName,
                    ConsumerNumber = job.JobIDFK.LeadFK.ConsumerNumber,
                    SubDivisionName = job.JobIDFK.LeadFK.SubDivisionIdFk.Name, //leads.SubDivisionId != null ? _subDivisionRepository.FirstOrDefaultAsync((int)leads.SubDivisionId).Result.Name : "",
                    DivisionName = job.JobIDFK.LeadFK.DivisionIdFk.Name, //leads.DivisionId != null ? _divisionRepository.FirstOrDefaultAsync((int)leads.DivisionId).Result.Name : "",
                    DiscomName = job.JobIDFK.LeadFK.DiscomIdFk.Name, //leads.DiscomId != null ? _discomRepository.FirstOrDefaultAsync((int)leads.DiscomId).Result.Name : "",

                    SystemSize = job.JobIDFK.PvCapacityKw, //pendding
                    MeterPhase = job.JobIDFK.PhaseOfInverter,
                    Category = job.JobIDFK.LeadFK.Category,
                    EmailId = job.JobIDFK.LeadFK.EmailId,
                    MobileNumber = job.JobIDFK.LeadFK.MobileNumber,
                    AddressLine1 = job.JobIDFK.LeadFK.AddressLine1,
                    AddressLine2 = job.JobIDFK.LeadFK.AddressLine2,
                    CityName = job.JobIDFK.LeadFK.CityIdFk.Name, // leads.CityId != null ? _cityRepository.FirstOrDefaultAsync((int)leads.CityId).Result.Name : "",
                    TalukaName = job.JobIDFK.LeadFK.TalukaIdFk.Name, // leads.TalukaId != null ? _talukaRepository.FirstOrDefaultAsync((int)leads.TalukaId).Result.Name : "",
                    DistrictName = job.JobIDFK.LeadFK.DistrictIdFk.Name, // leads.DistrictId != null ? _districtRepository.FirstOrDefaultAsync((int)leads.DistrictId).Result.Name : "",
                    StateName = job.JobIDFK.LeadFK.StateIdFk.Name, // leads.StateId != null ? _stateRepository.FirstOrDefaultAsync((int)leads.StateId).Result.Name : "",
                    Pincode = job.JobIDFK.LeadFK.Pincode,
                    CommonMeter = job.JobIDFK.LeadFK.CommonMeter,
                    BankAccountNumber = job.JobIDFK.LeadFK.BankAccountNumber,
                    BankAccountHolderName = job.JobIDFK.LeadFK.BankAccountHolderName,
                    BankName = job.JobIDFK.LeadFK.BankName,
                    BankBrachName = job.JobIDFK.LeadFK.BankBrachName,
                    IFSC_Code = job.JobIDFK.LeadFK.IFSC_Code,
                    LeadDocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(_leadDocumentRepository.GetAll().Where(x => x.LeadDocumentId == job.JobIDFK.LeadId).ToList())),
                };
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Refund_LeadViewDetails)]
        public async Task RefundDetailsEdit(CreateOrEditJobDto input, int SectionId)
        {
            //var leads = _leadRepository.GetAll().Where(x =>x.Id == (int?)input.Id);
            var job = await _jobRepository.FirstOrDefaultAsync(e => e.Id == input.Id);

            if (job != null)
            {
                job.OtpVerifed = input.OtpVerifed;
                job.GUVNLRegisterationNo = input.GUVNLRegisterationNo;
                job.ApplicationNumber = input.ApplicationNumber;
                job.ApplicationDate = input.ApplicationDate;
                //job.SignApplicationPath = input.SignApplicationPath;

                #region Activity Log
                //Add Activity Log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 11;
                //leadactivity.LeadAcitivityID = 0;
                leadactivity.SectionId = SectionId;
                leadactivity.LeadActionNote = "Job Application Tracker Details Modified";
                leadactivity.LeadId = job.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                var List = new List<LeadtrackerHistory>();

                try
                {
                    List<Job.Jobs> objAuditOld_OnlineUserMst = new List<Job.Jobs>();
                    List<CreateOrEditJobDto> objAuditNew_OnlineUserMst = new List<CreateOrEditJobDto>();

                    DataTable Dt_Target = new DataTable();
                    DataTable Dt_Source = new DataTable();



                    objAuditOld_OnlineUserMst.Add(job);
                    objAuditNew_OnlineUserMst.Add(input);

                    //Add Activity Log
                    LeadtrackerHistory leadActivityLog = new LeadtrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        leadActivityLog.TenantId = (int)AbpSession.TenantId;
                    }

                    object newvalue = objAuditNew_OnlineUserMst;
                    Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                    if (objAuditOld_OnlineUserMst != null)
                    {
                        Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                    }
                    foreach (DataRow dr_S in Dt_Source.Rows)
                    {
                        foreach (DataColumn dc_S in Dt_Source.Columns)
                        {

                            if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = dc_S.ColumnName;
                                objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                                //objAuditInfo.Action = "Edit";
                                objAuditInfo.Action = Dt_Target.Rows[0][dc_S.ColumnName].ToString() == "" ? "Add" : "Edit";
                                objAuditInfo.LeadId = job.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                        }
                    }
                }
                catch
                {
                }
                #endregion


            }

            return;
        }

        //Application Tracker Excel Export
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_ExportToExcel)]
        public async Task<FileDto> GetRefundTrackerToExcel(GetAllRefundTrackerInput input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                var User_List = _userRepository.GetAll();
                var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
                IList<string> role = await _userManager.GetRolesAsync(User);

                var job_list = _jobRepository.GetAll();
                //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
                var jobnumberlist = new List<int>();
                if (input.Filter != null)
                {
                    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                }
                var FollowupList = new List<int>();
                var NextFollowupList = new List<int>();
                var Assign = new List<int>();

                //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
                //{
                //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
                //}
                if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
                {
                    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                }
                var DocumentListMasterData = _documentRepository.GetAll();
                var DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id);

                var leadDoclistid = new List<int>();
                var LeadDocument = _leadDocumentRepository.GetAll().Where(x => x.IsActive == false);

                if (DocumentListData.Count() != 0)
                {
                    leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
                }
                var filteredJob = _jobRefundRepository.GetAll().Include(e => e.JobIDFK).Include(e => e.JobIDFK.LeadFK).Include(x => x.JobIDFK.LeadFK.LeadStatusIdFk)
                 .Include(x => x.JobIDFK.ApplicationStatusIdFK)
                             .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobIDFK.JobNumber.Contains(input.Filter)
                                 || e.JobIDFK.LeadFK.CustomerName.Contains(input.Filter) || e.JobIDFK.LeadFK.EmailId.Contains(input.Filter)
                                 || e.JobIDFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.JobIDFK.LeadFK.MobileNumber.Contains(input.Filter)
                                 //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                 //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                 || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.JobIDFK.LeadId)))
                             .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.JobIDFK.LeadFK.LeadStatusId))
                             .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.JobIDFK.ApplicationStatusId))
                             .WhereIf(input.refundPenddingFilter != null && input.refundPenddingFilter == 1, e => e.RefundPaidDate != null && e.PaymentTypeId != 0 && e.ReceiptNo != null) // 1 for Refunded
                             .WhereIf(input.refundPenddingFilter != null && input.refundPenddingFilter == 2, e => e.IsRefundVerify == false) // 2 for Pendding
                             .WhereIf(input.refundVerifyFilter != null && input.refundVerifyFilter == 1, e => e.IsRefundVerify == false) // 1 for Not Verify
                             .WhereIf(input.refundVerifyFilter != null && input.refundVerifyFilter == 2, e => e.IsRefundVerify == true) // 2 for Verify
                             .WhereIf(input.refundReasonFilter != 0, e => input.refundReasonFilter == e.RefundReasonId)
                             .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.JobIDFK.LeadFK.ChanelPartnerID))
                             .Where(e => e.JobIDFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);

                var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                var jobs = from o in pagedAndFilteredJob

                           join o3 in _lookup_leadStatusRepository.GetAll() on o.JobIDFK.LeadFK.LeadStatusId equals o3.Id into j3
                           from s3 in j3.DefaultIfEmpty()

                           select new GetRefundTrackerForViewDto()
                           {
                               refundTracker = new RefundTrackerDto
                               {
                                   Id = o.Id,
                                   RefundJobId = o.RefundJobId,
                                   RefundReasonId = o.RefundReasonId,
                                   PaymentTypeId = o.PaymentTypeId,
                                   RefundPaidAmount = o.RefundPaidAmount,
                                   RefundPaidDate = o.RefundPaidDate,
                                   RefundPaymentType = o.PaymentTypeIDFK.Name,
                                   RefundReasonName = o.RefundResIDFK.Name,
                                   RefundPaidNotes = o.RefundPaidNotes,
                                   IsRefundVerify = o.IsRefundVerify,
                                   RefundVerifyDate = o.RefundVerifyDate,
                                   RefundVerifyNotes = o.RefundVerifyNotes,
                                   RefundVerifyById = o.RefundVerifyById,
                                   BankName = o.BankName,
                                   BankAccountHolderName = o.BankAccountHolderName,
                                   BankBrachName = o.BankBrachName,
                                   BankAccountNumber = o.BankAccountNumber,
                                   RefundRecPath = o.RefundRecPath,
                                   ReceiptNo = o.ReceiptNo,
                                   JobRefundEmailSend = o.JobRefundEmailSend,
                                   JobRefundEmailSendDate = o.JobRefundEmailSendDate,
                                   JobRefundSmsSend = o.JobRefundSmsSend,
                                   JobRefundSmsSendDate = o.JobRefundSmsSendDate,
                                   leaddata = ObjectMapper.Map<LeadsDto>(o.JobIDFK.LeadFK),
                                   ProjectNumber = o.JobIDFK.JobNumber,
                                   //JobStatus = o.JobStatusFk.Name,
                                   CustomerName = o.JobIDFK.LeadFK.CustomerName,
                                   Address = (o.JobIDFK.LeadFK.AddressLine1 == null ? "" : o.JobIDFK.LeadFK.AddressLine1 + ", ") + (o.JobIDFK.LeadFK.AddressLine2 == null ? "" : o.JobIDFK.LeadFK.AddressLine2 + ", ") + (o.JobIDFK.LeadFK.CityIdFk.Name == null ? "" : o.JobIDFK.LeadFK.CityIdFk.Name + ", ") + (o.JobIDFK.LeadFK.TalukaIdFk.Name == null ? "" : o.JobIDFK.LeadFK.TalukaIdFk.Name + ", ") + (o.JobIDFK.LeadFK.DiscomIdFk.Name == null ? "" : o.JobIDFK.LeadFK.DiscomIdFk.Name + ", ") + (o.JobIDFK.LeadFK.StateIdFk.Name == null ? "" : o.JobIDFK.LeadFK.StateIdFk.Name + "-") + (o.JobIDFK.LeadFK.Pincode == null ? "" : o.JobIDFK.LeadFK.Pincode),
                                   Mobile = o.JobIDFK.LeadFK.MobileNumber,
                                   LeadStatusName = o.JobIDFK.LeadFK.LeadStatusIdFk.Status,
                                   FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.JobIDFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                   FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobIDFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                   Notes = o.JobIDFK.LeadFK.Notes,
                                   LastModificationTime = o.LastModificationTime,
                                   CreationTime = (DateTime)o.CreationTime,
                                   //QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                                   LeadStatusColorClass = o.JobIDFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                   LeadStatusIconClass = o.JobIDFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                   Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobIDFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                   ApplicationStatusdata = o.JobIDFK.ApplicationStatusIdFK,
                               },
                           };

                var refundtrackerListDtos = await jobs.ToListAsync();

                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _RefundTrackerExcelExporter.ExportToFile(refundtrackerListDtos);
                }
                else
                {
                    return _RefundTrackerExcelExporter.ExportToFile(refundtrackerListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
