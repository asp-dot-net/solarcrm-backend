﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.RefundTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.RefundTracker.Exporting
{
    public class RefundTrackerExcelExporter : NpoiExcelExporterBase, IRefundTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RefundTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetRefundTrackerForViewDto> refundtrack)
        {
            return CreateExcelPackage(
                "RefundTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("RefundTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),                       
                        L("CustomerName"),
                         L("RefundRequest"),
                        L("Amount"),
                        L("RefundReason"),
                        L("RefundMethod"),
                        L("FollowupDescription"),
                        L("NextFollowupDate"),                      
                        L("Comment")
                        );
                    AddObjects(
                        sheet, refundtrack,
                        _ => _.refundTracker.ProjectNumber,
                        _ => _.refundTracker.CustomerName,
                        _ => _.refundTracker.RefundPaidDate,
                        _ => _.refundTracker.RefundPaidAmount,
                        _ => _.refundTracker.RefundReasonName,
                        _ => _.refundTracker.RefundPaymentType,
                        _ => _.refundTracker.FollowDiscription,
                        _ => _.refundTracker.FollowDate,                      
                        _ => _.refundTracker.Comment
                        );
                });

        }
    }
}
