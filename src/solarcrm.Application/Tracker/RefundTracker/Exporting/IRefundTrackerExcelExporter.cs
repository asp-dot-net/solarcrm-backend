﻿using solarcrm.Dto;
using solarcrm.Tracker.RefundTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.RefundTracker.Exporting
{
    public interface IRefundTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetRefundTrackerForViewDto> refundTracker);
    }
}
