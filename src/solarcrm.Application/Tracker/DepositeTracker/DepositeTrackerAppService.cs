﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Organizations;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.Tracker.DepositeTracker.Dto;
using solarcrm.Tracker.DepositeTracker.Exporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Dto;

namespace solarcrm.Tracker.DepositeTracker
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Deposit)]
    public class DepositeTrackerAppService : solarcrmAppServiceBase, IDepositTrackerAppService
    {
        // private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        //private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        //private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        //private readonly IDepositTrackerExcelExporter _DepositTrackerExcelExporter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly IRepository<DocumentList> _documentRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        // private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IDepositeTrackerExcelExporter _depositeTrackerExcelExporter;
        public DepositeTrackerAppService(
            //IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            //IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            //IDbContextProvider<solarcrmDbContext> dbContextProvider,
            //IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            //IDepositTrackerExcelExporter DepositTrackerExcelExporter,
            IRepository<User, long> userRepository,
            IRepository<DocumentList> documentRepository,
            IRepository<LeadStatus, int> lookup_leadStatusRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            UserManager userManager,
            ITimeZoneConverter timeZoneConverter,
            IRepository<LeadDocuments> leadDocumentRepository,
            // IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IDepositeTrackerExcelExporter depositeTrackerExcelExporter
        )
        {
            //_dbContextProvider = dbContextProvider;
            _leadactivityRepository = leadactivityRepository;
            // _leadHistoryLogRepository = leadHistoryLogRepository;
            // _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            //_DepositTrackerExcelExporter = DepositTrackerExcelExporter;
            _userRepository = userRepository;
            _userManager = userManager;
            _documentRepository = documentRepository;
            _leadDocumentRepository = leadDocumentRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
            _depositeTrackerExcelExporter = depositeTrackerExcelExporter;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Deposit)]
        public async Task<PagedResultDto<GetDepositTrackerForViewDto>> GetAllDepositeTracker(GetAllDepositTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> PenddingDocumentList = new List<int>();
            List<int> DocumentListData = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> leadDoclistid = new List<int>();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);

            var job_list = _jobRepository.GetAll();
            var LeadDocument = _leadDocumentRepository.GetAll();
            var DocumentListMasterData = _documentRepository.GetAll();

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }

            if (DocumentListMasterData != null || DocumentListMasterData.Count() > 0)
            {
                DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id).ToList();
            }

            if (DocumentListData.Count() != 0 && LeadDocument.Count() != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }

            if (DocumentListData.Count() != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }
            //pending filter data
            if (leadDoclistid != null || leadDoclistid.Count() > 0)
            {
                PenddingDocumentList = leadDoclistid.GroupBy(x => x).Where(g => g.Count() == DocumentListData.Count()).Select(y => y.Key).ToList();
            }
            var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                || e.LeadFK.Alt_Phone.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.LeadId)))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.ApplicationStatusId))
                            .WhereIf(input.otpPendingFilter != null, e => input.otpPendingFilter.ToLower() == e.OtpVerifed.ToLower()) //OTPpending Filter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "yes", e => (e.LastComment != null || e.LastComment != "")) //Query RaisedFilter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "no", e => (e.LastComment == null || e.LastComment == "")) //Query RaisedFilter
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId)) // discomlist filter
                            .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId)) // circlelist filter
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId)) //devisionlist filter
                            .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId)) //subdivisiionlist filter
                            .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId)) //solartypelist filter
                            .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.LeadFK.ChanelPartnerID)) //employee with CPlist filter
                            .WhereIf(input.applicationstatusTrackerFilter == 1, e => e.ApplicationDate != null && PenddingDocumentList.Contains(e.LeadId) && (e.ApplicationNumber != null || e.ApplicationNumber != "")) //Completed
                            .WhereIf(input.applicationstatusTrackerFilter == 2, e => e.LeadFK.IsVerified && PenddingDocumentList.Contains(e.LeadId) && e.ApplicationDate == null && (e.ApplicationNumber == null || e.ApplicationNumber == "")) //pending                                                                                                                                                                                                    
                            .WhereIf(input.applicationstatusTrackerFilter == 4, e => e.LeadFK.IsVerified && (PenddingDocumentList.Contains(e.LeadId) == false)) //Awaiting                                                                                                                                                              
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "queryraisedon" && input.StartDate != null, e => e.LastCommentDate >= SDate.Value.Date && e.LastCommentDate <= EDate.Value.Date)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "nextfollowup" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
                            .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit && (e.DepositeAmount != null || e.DepositeAmount != 0));

            var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadFK.LeadStatusId equals o3.Id into j3
                       from s3 in j3.DefaultIfEmpty()

                       join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                       from s4 in j4.DefaultIfEmpty()

                       select new GetDepositTrackerForViewDto()
                       {
                           depositTracker = new DepositTrackerDto
                           {
                               Id = o.Id,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                               ProjectNumber = o.JobNumber,
                               //JobStatus = o.JobStatusFk.Name,
                               CustomerName = o.LeadFK.CustomerName,
                               Address = (o.LeadFK.AddressLine1 == null ? "" : o.LeadFK.AddressLine1 + ", ") + (o.LeadFK.AddressLine2 == null ? "" : o.LeadFK.AddressLine2 + ", ") + (o.LeadFK.CityIdFk.Name == null ? "" : o.LeadFK.CityIdFk.Name + ", ") + (o.LeadFK.TalukaIdFk.Name == null ? "" : o.LeadFK.TalukaIdFk.Name + ", ") + (o.LeadFK.DiscomIdFk.Name == null ? "" : o.LeadFK.DiscomIdFk.Name + ", ") + (o.LeadFK.StateIdFk.Name == null ? "" : o.LeadFK.StateIdFk.Name + "-") + (o.LeadFK.Pincode == null ? "" : o.LeadFK.Pincode),
                               Mobile = o.LeadFK.MobileNumber,
                               SubDivision = o.LeadFK.SubDivisionIdFk.Name,
                               JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                               LeadStatusName = o.LeadFK.LeadStatusIdFk.Status,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Notes = o.LeadFK.Notes,
                               LastModificationTime = o.LastModificationTime,
                               CreationTime = (DateTime)o.CreationTime,
                               QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                               LeadStatusColorClass = o.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.ApplicationStatusIdFK,
                           },
                           documentListName = (ObjectMapper.Map<List<DocumentListDto>>(DocumentListMasterData.Where(x => DocumentListData.Contains(x.Id)).ToList())),
                           leaddocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(LeadDocument.Where(x => x.LeadDocumentId == o.LeadId && DocumentListData.Contains(x.DocumentId)).ToList())),
                       };

            var totalCount = await filteredJob.CountAsync();

            return new PagedResultDto<GetDepositTrackerForViewDto>(totalCount, await jobs.ToListAsync());
        }
        //public async Task<DepositTrackerQuickViewDto> GetDepositeQuickView(EntityDto input)
        //{
        //    try
        //    {
        //        var job = _jobRepository.GetAll().Include(e => e.LeadFK).
        //            Include(e => e.LeadFK.DivisionIdFk).
        //            Include(e => e.LeadFK.SubDivisionIdFk).
        //            Include(e => e.LeadFK.DiscomIdFk).
        //            Include(e => e.LeadFK.CityIdFk).
        //            Include(e => e.LeadFK.TalukaIdFk).
        //            Include(e => e.LeadFK.DistrictIdFk).
        //            Include(e => e.LeadFK.StateIdFk).
        //            Where(e => e.Id == input.Id).FirstOrDefault();

        //        var output = new DepositTrackerQuickViewDto
        //        {

        //            ProjectNumber = job.JobNumber,
        //            Latitude = (decimal?)job.LeadFK.Latitude,
        //            Longitude = (decimal?)job.LeadFK.Longitude,
        //            AvgmonthlyBill = (int?)job.LeadFK.AvgmonthlyBill,
        //            AvgmonthlyUnit = (int?)job.LeadFK.AvgmonthlyUnit,
        //            CustomerName = job.LeadFK.CustomerName,
        //            ConsumerNumber = job.LeadFK.ConsumerNumber,
        //            SubDivisionName = job.LeadFK.SubDivisionIdFk.Name, //leads.SubDivisionId != null ? _subDivisionRepository.FirstOrDefaultAsync((int)leads.SubDivisionId).Result.Name : "",
        //            DivisionName = job.LeadFK.DivisionIdFk.Name, //leads.DivisionId != null ? _divisionRepository.FirstOrDefaultAsync((int)leads.DivisionId).Result.Name : "",
        //            DiscomName = job.LeadFK.DiscomIdFk.Name, //leads.DiscomId != null ? _discomRepository.FirstOrDefaultAsync((int)leads.DiscomId).Result.Name : "",

        //            SystemSize = job.PvCapacityKw, //pendding
        //            MeterPhase = job.PhaseOfInverter,
        //            Category = job.LeadFK.Category,
        //            EmailId = job.LeadFK.EmailId,
        //            MobileNumber = job.LeadFK.MobileNumber,
        //            AddressLine1 = job.LeadFK.AddressLine1,
        //            AddressLine2 = job.LeadFK.AddressLine2,
        //            CityName = job.LeadFK.CityIdFk.Name, // leads.CityId != null ? _cityRepository.FirstOrDefaultAsync((int)leads.CityId).Result.Name : "",
        //            TalukaName = job.LeadFK.TalukaIdFk.Name, // leads.TalukaId != null ? _talukaRepository.FirstOrDefaultAsync((int)leads.TalukaId).Result.Name : "",
        //            DistrictName = job.LeadFK.DistrictIdFk.Name, // leads.DistrictId != null ? _districtRepository.FirstOrDefaultAsync((int)leads.DistrictId).Result.Name : "",
        //            StateName = job.LeadFK.StateIdFk.Name, // leads.StateId != null ? _stateRepository.FirstOrDefaultAsync((int)leads.StateId).Result.Name : "",
        //            Pincode = job.LeadFK.Pincode,
        //            CommonMeter = job.LeadFK.CommonMeter,

        //            //LeadDocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(_leadDocumentRepository.GetAll().Where(x => x.LeadDocumentId == job.LeadId).ToList())),
        //        };
        //        return output;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        ////Deposit Tracker Excel Export
        //// [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Deposit_ExportToExcel)]
        //public async Task<FileDto> GetDepositeTrackerToExcel(GetAllDepositTrackerInput input)
        //{
        //    try
        //    {
        //        var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //        var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //        var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
        //        var User_List = _userRepository.GetAll();
        //        var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
        //        IList<string> role = await _userManager.GetRolesAsync(User);

        //        var job_list = _jobRepository.GetAll();
        //        //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
        //        var jobnumberlist = new List<int>();
        //        if (input.Filter != null)
        //        {
        //            jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
        //        }
        //        var FollowupList = new List<int>();
        //        var NextFollowupList = new List<int>();
        //        var Assign = new List<int>();

        //        //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
        //        //{
        //        //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
        //        //}
        //        if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
        //        {
        //            NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
        //        }
        //        var DocumentListMasterData = _documentRepository.GetAll();
        //        var DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id);

        //        var leadDoclistid = new List<int>();
        //        var LeadDocument = _leadDocumentRepository.GetAll().Where(x => x.IsActive == false);

        //        if (DocumentListData.Count() != 0)
        //        {
        //            leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
        //        }
        //        var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
        //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter))
        //             .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter) || e.LeadFK.Alt_Phone.Contains(input.Filter) ||
        //                            e.LeadFK.MobileNumber.Contains(input.Filter) || e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.Id)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
        //                         .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
        //                          .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.ApplicationStatusId))
        //                          .WhereIf(input.otpPendingFilter != null, e => input.otpPendingFilter == e.OtpVerifed)
        //                          .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "yes", e => e.LastComment != null)
        //                          .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "no", e => e.LastComment == null)
        //                           .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId))
        //                           .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId))
        //                             .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId))
        //                             .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId))
        //                           .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId))
        //                            .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.LeadFK.TenantId))
        //                          .WhereIf(input.applicationstatusTrackerFilter == 1, e => e.ApplicationDate != null && e.ApplicationNumber != null) //Completed
        //                          .WhereIf(input.applicationstatusTrackerFilter == 2, e => e.LeadFK.IsVerified && leadDoclistid.Count != 0 && e.ApplicationDate == null && e.ApplicationNumber == null) //pending                                                                                                                                                                                                    
        //                          .WhereIf(input.applicationstatusTrackerFilter == 4, e => e.LeadFK.IsVerified || !e.LeadFK.IsVerified && leadDoclistid.Count == 0) //Awaiting                                                                                                                                                              
        //                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "queryraisedon" && input.StartDate != null, e => e.LastCommentDate >= SDate.Value.Date && e.LastCommentDate <= EDate.Value.Date)
        //                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "nextfollowup" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
        //                          .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit && e.FirstDepositRecivedDate != null);

        //        var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

        //        var jobs = from o in pagedAndFilteredJob

        //                   join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadFK.LeadStatusId equals o3.Id into j3
        //                   from s3 in j3.DefaultIfEmpty()

        //                   join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
        //                   from s4 in j4.DefaultIfEmpty()

        //                   select new GetDepositTrackerForViewDto()
        //                   {
        //                       depositTracker = new DepositTrackerDto
        //                       {
        //                           Id = o.Id,
        //                           leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
        //                           ProjectNumber = o.JobNumber,
        //                           //JobStatus = o.JobStatusFk.Name,
        //                           CustomerName = o.LeadFK.CustomerName,
        //                           Address = (o.LeadFK.AddressLine1 == null ? "" : o.LeadFK.AddressLine1 + ", ") + (o.LeadFK.AddressLine2 == null ? "" : o.LeadFK.AddressLine2 + ", ") + (o.LeadFK.CityIdFk.Name == null ? "" : o.LeadFK.CityIdFk.Name + ", ") + (o.LeadFK.TalukaIdFk.Name == null ? "" : o.LeadFK.TalukaIdFk.Name + ", ") + (o.LeadFK.DiscomIdFk.Name == null ? "" : o.LeadFK.DiscomIdFk.Name + ", ") + (o.LeadFK.StateIdFk.Name == null ? "" : o.LeadFK.StateIdFk.Name + "-") + (o.LeadFK.Pincode == null ? "" : o.LeadFK.Pincode),
        //                           Mobile = o.LeadFK.MobileNumber,
        //                           SubDivision = o.LeadFK.SubDivisionIdFk.Name,
        //                           JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
        //                           LeadStatusName = o.LeadFK.LeadStatusIdFk.Status,
        //                           Notes = o.LeadFK.Notes,
        //                           LastModificationTime = o.LastModificationTime,
        //                           CreationTime = (DateTime)o.CreationTime,
        //                           QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
        //                           LeadStatusColorClass = o.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
        //                           LeadStatusIconClass = o.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
        //                           FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
        //                           FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
        //                           Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
        //                           ApplicationStatusdata = o.ApplicationStatusIdFK,
        //                       },
        //                       documentListName = (ObjectMapper.Map<List<DocumentListDto>>(DocumentListMasterData.Where(x => DocumentListData.Contains(x.Id)).ToList())),
        //                       leaddocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(LeadDocument.Where(x => x.LeadDocumentId == o.LeadId && DocumentListData.Contains(x.DocumentId)).ToList())),

        //                       // _applicationstatusRepository.GetAll().Where(x => x.Id == o.ApplicationStatusId && x.IsActive == true).FirstOrDefault(),                           
        //                   };

        //        var deposittrackerListDtos = await jobs.ToListAsync();

        //        if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
        //        {
        //            return _DepositTrackerExcelExporter.ExportToFile(deposittrackerListDtos);
        //        }
        //        else
        //        {
        //            return _DepositTrackerExcelExporter.ExportToFile(deposittrackerListDtos);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //}

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Deposit_ExportToExcel)]
        public async Task<FileDto> GetDepositeTrackerToExcel(GetAllDepositTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> PenddingDocumentList = new List<int>();
            List<int> DocumentListData = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> leadDoclistid = new List<int>();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);

            var job_list = _jobRepository.GetAll();
            var LeadDocument = _leadDocumentRepository.GetAll();
            var DocumentListMasterData = _documentRepository.GetAll();

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }

            if (DocumentListMasterData != null || DocumentListMasterData.Count() > 0)
            {
                DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id).ToList();
            }

            if (DocumentListData.Count() != 0 && LeadDocument.Count() != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }

            if (DocumentListData.Count() != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }
            //pending filter data
            if (leadDoclistid != null || leadDoclistid.Count() > 0)
            {
                PenddingDocumentList = leadDoclistid.GroupBy(x => x).Where(g => g.Count() == DocumentListData.Count()).Select(y => y.Key).ToList();
            }
            var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                || e.LeadFK.Alt_Phone.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.LeadId)))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.ApplicationStatusId))
                            .WhereIf(input.otpPendingFilter != null, e => input.otpPendingFilter.ToLower() == e.OtpVerifed.ToLower()) //OTPpending Filter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "yes", e => (e.LastComment != null || e.LastComment != "")) //Query RaisedFilter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "no", e => (e.LastComment == null || e.LastComment == "")) //Query RaisedFilter
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId)) // discomlist filter
                            .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId)) // circlelist filter
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId)) //devisionlist filter
                            .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId)) //subdivisiionlist filter
                            .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId)) //solartypelist filter
                            .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.LeadFK.ChanelPartnerID)) //employee with CPlist filter
                            .WhereIf(input.applicationstatusTrackerFilter == 1, e => e.ApplicationDate != null && PenddingDocumentList.Contains(e.LeadId) && (e.ApplicationNumber != null || e.ApplicationNumber != "")) //Completed
                            .WhereIf(input.applicationstatusTrackerFilter == 2, e => e.LeadFK.IsVerified && PenddingDocumentList.Contains(e.LeadId) && e.ApplicationDate == null && (e.ApplicationNumber == null || e.ApplicationNumber == "")) //pending                                                                                                                                                                                                    
                            .WhereIf(input.applicationstatusTrackerFilter == 4, e => e.LeadFK.IsVerified && (PenddingDocumentList.Contains(e.LeadId) == false)) //Awaiting                                                                                                                                                              
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "queryraisedon" && input.StartDate != null, e => e.LastCommentDate >= SDate.Value.Date && e.LastCommentDate <= EDate.Value.Date)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "nextfollowup" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
                            .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit && (e.DepositeAmount != null || e.DepositeAmount != 0));

            var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);
            var jobs = from o in pagedAndFilteredJob

                       join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadFK.LeadStatusId equals o3.Id into j3
                       from s3 in j3.DefaultIfEmpty()

                       join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                       from s4 in j4.DefaultIfEmpty()

                       select new GetDepositTrackerForViewDto()
                       {
                           depositTracker = new DepositTrackerDto
                           {
                               Id = o.Id,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                               ProjectNumber = o.JobNumber,
                               //JobStatus = o.JobStatusFk.Name,
                               CustomerName = o.LeadFK.CustomerName,
                               Address = (o.LeadFK.AddressLine1 == null ? "" : o.LeadFK.AddressLine1 + ", ") + (o.LeadFK.AddressLine2 == null ? "" : o.LeadFK.AddressLine2 + ", ") + (o.LeadFK.CityIdFk.Name == null ? "" : o.LeadFK.CityIdFk.Name + ", ") + (o.LeadFK.TalukaIdFk.Name == null ? "" : o.LeadFK.TalukaIdFk.Name + ", ") + (o.LeadFK.DiscomIdFk.Name == null ? "" : o.LeadFK.DiscomIdFk.Name + ", ") + (o.LeadFK.StateIdFk.Name == null ? "" : o.LeadFK.StateIdFk.Name + "-") + (o.LeadFK.Pincode == null ? "" : o.LeadFK.Pincode),
                               Mobile = o.LeadFK.MobileNumber,
                               SubDivision = o.LeadFK.SubDivisionIdFk.Name,
                               JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                               LeadStatusName = o.LeadFK.LeadStatusIdFk.Status,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Notes = o.LeadFK.Notes,
                               LastModificationTime = o.LastModificationTime,
                               CreationTime = (DateTime)o.CreationTime,
                               QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                               LeadStatusColorClass = o.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.ApplicationStatusIdFK,
                           },
                           documentListName = (ObjectMapper.Map<List<DocumentListDto>>(DocumentListMasterData.Where(x => DocumentListData.Contains(x.Id)).ToList())),
                           leaddocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(LeadDocument.Where(x => x.LeadDocumentId == o.LeadId && DocumentListData.Contains(x.DocumentId)).ToList())),



                       };

            var depositetrackerListDtos = await jobs.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _depositeTrackerExcelExporter.ExportToFile(depositetrackerListDtos);
            }
            else
            {
                return _depositeTrackerExcelExporter.ExportToFile(depositetrackerListDtos);
            }
        }
    }
}
