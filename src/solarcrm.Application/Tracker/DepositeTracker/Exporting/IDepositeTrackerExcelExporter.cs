﻿using solarcrm.Dto;
using solarcrm.Tracker.DepositeTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.DepositeTracker.Exporting
{
    public interface IDepositeTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetDepositTrackerForViewDto> depositTracker);
    }
}
