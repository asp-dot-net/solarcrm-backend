﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.DepositeTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.DepositeTracker.Exporting
{
    public class DepositeTrackerExcelExporter : NpoiExcelExporterBase, IDepositeTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DepositeTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetDepositTrackerForViewDto> depositetrack)
        {
            return CreateExcelPackage(
                "DepositTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("DepositTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("LeadStatus"),
                        L("ApplicationStatus"),
                        L("CustomerName"),
                        L("Address"),
                        L("Mobile"),
                        L("FollowupDiscription"),
                        L("NextFollowupdate"),
                        L("QueryDescription"),
                        L("Comment")
                        );
                    AddObjects(
                        sheet, depositetrack,
                        _ => _.depositTracker.ProjectNumber,
                        _ => _.depositTracker.LeadStatusName,
                        _ => _.depositTracker.ApplicationStatusdata,
                        _ => _.depositTracker.CustomerName,
                        _ => _.depositTracker.Address,
                        _ => _.depositTracker.Mobile,
                        _ => _.depositTracker.FollowDiscription,
                        _ => _.depositTracker.FollowDate,
                        _ => _.depositTracker.QuesryDescription,
                        _ => _.depositTracker.Comment
                        );
                });
        }
    }
}
