﻿using Abp.Localization.Sources;
using Abp.Localization;
using NPOI.SS.UserModel;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.MISReport.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using solarcrm.Tracker.SubsidyTracker.Importing.Dto;

namespace solarcrm.Tracker.SubsidyTracker.Importing
{
    public class SubsidyClaimReportListExcelDataReader : NpoiExcelImporterBase<ImportSubsidyClaimReportDto>, ISubsidyClaimReportListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public SubsidyClaimReportListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(solarcrmConsts.LocalizationSourceName);
        }

        public List<ImportSubsidyClaimReportDto> GetClaimReportFromExcel(byte[] fileBytes, string filename)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow, filename);
        }

        private ImportSubsidyClaimReportDto ProcessExcelRow(ISheet worksheet, int row)
        {
            //if (IsRowEmpty(worksheet, row))
            //{
            //    return null;
            //}

            var exceptionMessage = new StringBuilder();
            var SubSidyItem = new ImportSubsidyClaimReportDto();

            IRow roww = worksheet.GetRow(row);
            //List<ICell> cells = roww.Cells;
            //List<string> rowData = new List<string>();
            ////Sheet.ActiveCell.Column;
            //for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
            //{
            //    ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
            //    rowData.Add(Convert.ToString(cell));
            //}
            DataTable dt = new DataTable();
            IRow headerRow = worksheet.GetRow(0);
            // dt.Columns.AddRange(new DataColumn[] { headerRow.Cells });
            //for (int i = (worksheet.FirstRowNum + 1); i <= worksheet.LastRowNum; i++)
            {
                IRow row1 = worksheet.GetRow(row);
                DataRow dataRow = dt.NewRow();
                for (int j = row1.FirstCellNum; j < row1.LastCellNum; j++)
                {
                    string columnName = headerRow.GetCell(j).ToString();
                    NPOI.SS.UserModel.ICell cell = roww.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                    string strValue = Convert.ToString(cell); // row1.GetCell(j);
                    string collumn = Regex.Replace(columnName, @"[^0-9a-zA-Z]+", "");
                    dataRow.Table.Columns.Add(collumn);
                    dataRow[collumn] = strValue;
                }
                dt.Rows.Add(dataRow);
            }
            try
            {
                if (dt.Rows.Count > 0)
                {
                    try { SubSidyItem.SubsidyClaimNo = Convert.ToString(dt.Rows[0]["SubsidyClaimNo"]).Trim(); } catch { }
                    try { SubSidyItem.SubsidyProjectNo = Convert.ToString(dt.Rows[0]["ProjectNumber"]).Trim(); } catch { }
                    //try { MisItem.GUVNLRegistrationNo = Convert.ToString(dt.Rows[0]["GUVNLRegistrationNo"]).Trim(); } catch { }
                    //try { MisItem.Lat = Convert.ToString(dt.Rows[0]["Lat"]); } catch { }
                    
                }
            }
            catch (System.Exception exception)
            {
                SubSidyItem.Exception = exception.Message;
            }

            return SubSidyItem;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }

    }
}
