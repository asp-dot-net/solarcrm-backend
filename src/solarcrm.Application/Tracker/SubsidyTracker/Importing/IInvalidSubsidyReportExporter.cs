﻿using solarcrm.Dto;
using solarcrm.Tracker.SubsidyTracker.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyTracker.Importing
{
    public interface IInvalidSubsidyReportExporter
    {
        FileDto ExportToFile(List<ImportSubsidyClaimReportDto> MisReportListDtos);
    }
}
