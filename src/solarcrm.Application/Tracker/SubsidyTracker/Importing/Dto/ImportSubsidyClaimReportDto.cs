﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyTracker.Importing.Dto
{
    public class ImportSubsidyClaimReportDto : EntityDto<int?>
    {
        public virtual int? SubsidyDivisionId { get; set; }
        public virtual string SubsidyClaimNo { get; set; }
        public virtual string SubsidyProjectNo { get; set; }
        public virtual string SubsidyNote { get; set; }
        public virtual DateTime? SubcidyClaimDate { get; set; }
        public virtual bool IsSubcidyFileVerify { get; set; }
        public virtual bool IsSubcidyReceived { get; set; }
        public virtual string OCCopyPath { get; set; }
        public virtual DateTime? ClaimFileSubmissionDate { get; set; }
        public virtual string SubcidyReceiptsPath { get; set; }
        public virtual int OrganizationUnitId { get; set; }

        public virtual int TenantId { get; set; }
        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
