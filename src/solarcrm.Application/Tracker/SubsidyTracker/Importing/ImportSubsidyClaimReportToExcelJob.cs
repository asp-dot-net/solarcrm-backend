﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Localization;
using Abp.Threading;
using Abp.UI;
using solarcrm.Common;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.Notifications;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Abp.ObjectMapping;
using solarcrm.Subsidy;
using solarcrm.Tracker.SubsidyTracker.Dto;
using solarcrm.Tracker.SubsidyTracker.Importing.Dto;
using Microsoft.EntityFrameworkCore;

namespace solarcrm.Tracker.SubsidyTracker.Importing
{
    public class ImportSubsidyClaimReportToExcelJob : BackgroundJob<ImportSubsidyClaimReportFromExcelJobArgs>, ITransientDependency
    {
        private readonly IRepository<Job.Jobs> _jobRepository;
        
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidSubsidyReportExporter _invalidSubsidyClaimReportExporter;
        private readonly IRepository<SubsidyClaimDetails> _subsidyClaimDetailsrepository;
        private readonly IRepository<SubsidyClaimProjectDetails> _subsidyClaimProjectDetailsrepository;
        private readonly IRepository<SubsidyClaimFileUploadLists> _subsidyClaimUploadrepository;       
        private readonly IRepository<SubsidyActivityLogs> _subsidyActivityLogRepository;
       // private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly ISubsidyClaimReportListExcelDataReader _subsidyClaimReportListExcelDataReader;
       // private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private int _sectionId = 45;

        public ImportSubsidyClaimReportToExcelJob(
            ISubsidyClaimReportListExcelDataReader subsidyClaimReportListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IRepository<SubsidyClaimFileUploadLists> subsidyClaimUploadrepository,
            IRepository<SubsidyClaimDetails> subsidyClaimDetailsrepository,
            IRepository<SubsidyClaimProjectDetails> subsidyClaimProjectDetailsrepository,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidSubsidyReportExporter invalidSubsidyClaimReportExporter,        
            IRepository<SubsidyActivityLogs> subsidyActivityLogRepository,           
            IRepository<Job.Jobs> jobRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,         
            ICommonLookupAppService CommonDocumentSaveRepository
            //IDbContextProvider<solarcrmDbContext> dbContextProvider
            )
        {
            _subsidyClaimReportListExcelDataReader = subsidyClaimReportListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;            
            _unitOfWorkManager = unitOfWorkManager;
            _subsidyClaimDetailsrepository = subsidyClaimDetailsrepository;
            _subsidyClaimProjectDetailsrepository = subsidyClaimProjectDetailsrepository;
            _invalidSubsidyClaimReportExporter = invalidSubsidyClaimReportExporter;
            //_abpSession = abpSession;
            //_userRepository = userRepository;
            _subsidyActivityLogRepository = subsidyActivityLogRepository;
            _subsidyClaimUploadrepository = subsidyClaimUploadrepository;
            _jobRepository = jobRepository;
            //_leadactivityRepository = leadactivityRepository;            
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
           // _dbContextProvider = dbContextProvider;
        }

        [UnitOfWork]
        public override void Execute(ImportSubsidyClaimReportFromExcelJobArgs args)
        {
            var stockItems = GetSubsidyClaimReportListFromExcelOrNull(args);
            if (stockItems == null || !stockItems.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                //var dd =  _jobRepository.GetAll();
                var invalidSubsidyClaimItems = new List<ImportSubsidyClaimReportDto>();

                foreach (var subsidyItem in stockItems)
                {
                    if (subsidyItem.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateOrUpdateSubsidyClaimReportAsync(subsidyItem, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            //Lead.Exception = exception.Message;
                            invalidSubsidyClaimItems.Add(subsidyItem);
                        }
                        //catch (Exception e)
                        catch
                        {
                            //Lead.Exception = e.ToString();
                            invalidSubsidyClaimItems.Add(subsidyItem);
                        }
                    }
                    else
                    {
                        invalidSubsidyClaimItems.Add(subsidyItem);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        //if(invalidMISItems.Count != 0)
                        {
                            AsyncHelper.RunSync(() => ProcessImportSubsidyClaimReportResultAsync(args, invalidSubsidyClaimItems));
                        }

                    }
                    uow.Complete();
                    //call update service Application Status
                    //AsyncHelper.RunSync(() => SubsidyClaimReportServiceUpdate(args));
                }


            }
        }


        private List<ImportSubsidyClaimReportDto> GetSubsidyClaimReportListFromExcelOrNull(ImportSubsidyClaimReportFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        string filename = Path.GetExtension(file.Description);
                        var files = _subsidyClaimReportListExcelDataReader.GetClaimReportFromExcel(file.Bytes, filename);

                        return files;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateOrUpdateSubsidyClaimReportAsync(ImportSubsidyClaimReportDto input, ImportSubsidyClaimReportFromExcelJobArgs args)
        {
            SubsidyClaimDetails SubsidyItem = new SubsidyClaimDetails();
            try
            {
                Job.Jobs projectdata = new Job.Jobs();
                int SubsidyClaimId = 0;
                //var JobStatusId = 0;
                using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
                {

                    if (input.SubsidyProjectNo != null)
                    {
                        projectdata = await _jobRepository.GetAll().Include(x=>x.LeadFK).Where(e => e.JobNumber.ToLower() == input.SubsidyProjectNo.ToLower()).FirstOrDefaultAsync();
                    }
                    if (projectdata.Id == 0)
                    {
                        input.Exception = "Project Number Found";
                        throw new Exception("Project Number Not Found");
                    }

                    if (input.SubsidyClaimNo != "" && projectdata.Id != 0)
                    {
                        var result = await _subsidyClaimDetailsrepository.GetAll().Where(e => e.SubsidyClaimNo.ToLower() == input.SubsidyClaimNo.Trim().ToLower()).FirstOrDefaultAsync();
                        if (result == null)
                        {
                            SubsidyItem = _objectMapper.Map<SubsidyClaimDetails>(result);
                            input.SubsidyDivisionId = projectdata.LeadFK.DivisionId;
                            var SubsidyItemNew = _objectMapper.Map<SubsidyClaimDetails>(input);
                            SubsidyItemNew.TenantId = (int)args.TenantId;                           
                            SubsidyItemNew.CreatorUserId = (int)args.User.UserId;
                            SubsidyClaimId = await _subsidyClaimDetailsrepository.InsertAndGetIdAsync(SubsidyItemNew);

                            ////Add MIS Activity Log
                            //MISActivityLogs misActivityLog = new MISActivityLogs();
                            //misActivityLog.TenantId = (int)args.TenantId;
                            //misActivityLog.SectionId = _sectionId;
                            //misActivityLog.LeadActionId = 1; //1-Created, 2-Updated, 3-Deleted
                            //misActivityLog.MISId = misIItemId;
                            //misActivityLog.MISActionNote = "MIS Items Created From Excel";
                            //misActivityLog.ProjectName = MISItemNew.ProjectName;
                            //misActivityLog.CreatorUserId = (int)args.User.UserId;
                            //await _misActivityLogRepository.InsertAsync(misActivityLog);
                        }
                        if (result != null)
                        {
                            var claimproject = await _subsidyClaimProjectDetailsrepository.GetAll().Where(x => x.SubsidyClaimId == result.Id && x.SubsidyJobId == projectdata.Id).FirstOrDefaultAsync();                            //_subsidyClaimProjectDetailsrepository
                            if (claimproject == null)
                            {
                                SubsidyClaimProjectDetails subsidyproject = new SubsidyClaimProjectDetails();
                                subsidyproject.SubsidyJobId = projectdata.Id;
                                subsidyproject.SubsidyClaimId = result.Id;
                                await _subsidyClaimProjectDetailsrepository.InsertAsync(subsidyproject);
                            }
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendInvalidExcelNotification(ImportSubsidyClaimReportFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToSubsidyClaimsReport", solarcrmConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }
        
        private async Task ProcessImportSubsidyClaimReportResultAsync(ImportSubsidyClaimReportFromExcelJobArgs args, List<ImportSubsidyClaimReportDto> invalidClaimItem)
        {
            string FilestatusUpload = "";
            if (invalidClaimItem.Any())
            {
                var file = _invalidSubsidyClaimReportExporter.ExportToFile(invalidClaimItem);
                await _appNotifier.SomeSubsidyClaimReportCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
                FilestatusUpload = "Failed";
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllSubsidyClaimsreportDataSuccessfullyImportedFromExcel", solarcrmConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
                FilestatusUpload = "Success";
            }

            //MISReport file save name
            var ext = ".xlsx";
            string filename = DateTime.Now.Ticks + "_" + "SubsidyClaimReportImportFiles" + ext;
            var filebyte = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(filebyte.Bytes, filename, "SubsidyClaimReportImportFiles", 0, 0, args.TenantId);

            SubsidyClaimFileUploadLists files = new SubsidyClaimFileUploadLists();
            files.CreatorUserId = (int)args.User.UserId;
            files.SubsidyTenantId = (int)args.TenantId;
            files.SubsidyFileName = filepath.filename;
            files.SubsidyFilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + "SubsidyClaimReportImportFiles" + "\\";
            files.SubsidyFileStatus = FilestatusUpload;
            await _subsidyClaimUploadrepository.InsertAsync(files);
        }
    }
}
