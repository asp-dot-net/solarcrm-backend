﻿using Abp.Dependency;
using solarcrm.MISReport.Importing.Dto;
using solarcrm.Tracker.SubsidyTracker.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyTracker.Importing
{
    public interface ISubsidyClaimReportListExcelDataReader : ITransientDependency
    {
        List<ImportSubsidyClaimReportDto> GetClaimReportFromExcel(byte[] fileBytes, string filename);
    }
}
