﻿using Abp.Dependency;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.MISReport.Importing.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.SubsidyTracker.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyTracker.Importing
{
    public class InvalidSubsidyReportExporter : NpoiExcelExporterBase, IInvalidSubsidyReportExporter, ITransientDependency
    {
        public InvalidSubsidyReportExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportSubsidyClaimReportDto> subsidyClaimReportListDtos)
        {
            return CreateExcelPackage(
                "InvalidSubsidyClaimReportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("InvalidSubsidyClaimReportImports"));

                    AddHeader(
                        sheet,

                        L("SubsidyClaimNo"),
                        L("SubsidyProjectNo"),
                        L("Refuse Reason")
                    );

                    AddObjects(
                        sheet, subsidyClaimReportListDtos,

                        _ => _.SubsidyClaimNo,
                        _ => _.SubsidyProjectNo,                        
                        _ => _.Exception
                    );

                    for (var i = 0; i < 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
