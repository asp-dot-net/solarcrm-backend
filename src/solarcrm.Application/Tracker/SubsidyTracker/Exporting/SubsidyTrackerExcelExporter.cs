﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.SubsidyTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyTracker.Exporting
{
    public class SubsidyTrackerExcelExporter : NpoiExcelExporterBase, ISubsidyTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SubsidyTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetSubsidyTrackerForViewDto> subtrack)
        {
            return CreateExcelPackage(
                "SubsidyTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("SubsidyTracker"));

                    AddHeader(
                        sheet,
                        L("ClaimNumber"),
                        L("Division"),
                        L("totalProjectCost"),
                        L("subsidyAmount"),
                        L("netPayableAmount"),
                        L("bankableAmount"),
                        L("actualAmountRec"),
                        L("subcidyFileVerify"),
                        L("OCCopy"),
                        L("subcidyReceived"),
                        L("subcidyReceipts")
                        );
                    AddObjects(
                        sheet, subtrack,
                        _ => _.subsidyTracker.ClaimNumber,
                        _ => _.subsidyTracker.Division,
                        _ => _.subsidyTracker.TotalProjectCost,
                        _ => _.subsidyTracker.SubsidyAmount,
                        _ => _.subsidyTracker.NetPayableAmount,
                        _ => _.subsidyTracker.BankableAmount,
                        _ => _.subsidyTracker.ActualAmountRec,
                        _ => _.subsidyTracker.SubcidyFileVerify,
                        _ => _.subsidyTracker.OCCopy,
                        _ => _.subsidyTracker.SubcidyReceived,
                        _ => _.subsidyTracker.SubcidyReceipts
                        );
                });
        }
    }
}
