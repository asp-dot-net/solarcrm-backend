﻿using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.SubsidyTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyTracker.Exporting
{
    public interface ISubsidyTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetSubsidyTrackerForViewDto> subsidyTracker);
    }
}
