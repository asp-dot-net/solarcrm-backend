﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.Leads.Dto;
using Abp.Timing.Timezone;
using solarcrm.Payments;
using solarcrm.Tracker.SubsidyTracker.Dto;
using solarcrm.Transport;
using solarcrm.Dispatch;
using solarcrm.Job;
using solarcrm.JobInstallation;
using solarcrm.DataVaults;
using solarcrm.MeterConnect;
using Abp.Runtime.Session;
using solarcrm.Subsidy;
using solarcrm.Authorization;
using solarcrm.Dto;
using solarcrm.Tracker.SubsidyClaim.Dto;
using solarcrm.Tracker.SubsidyClaim.Exporting;
using solarcrm.Tracker.SubsidyTracker.Exporting;

namespace solarcrm.Tracker.SubsidyTracker
{
    public class SubsidyTrackerAppService : solarcrmAppServiceBase, ISubsidyTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<SubsidyClaimDetails> _subsidyCliamDetailsRepository;
        private readonly IRepository<SubsidyClaimProjectDetails> _subsidyclaimProjectDetailsRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobInstallationDetail> _jobInstallRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<SSPaymentDetails> _ssPaymentDetailsRepository;
        private readonly IRepository<STCPaymentDetails> _stcPaymentDetailsRepository;
        private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<MeterConnectDetails> _meterConnectDetailsRepository;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailsJobRepository;
        private readonly ISubsidyTrackerExcelExporter _SubsidyTrackerExcelExporter;
        private readonly UserManager _userManager;

        public SubsidyTrackerAppService(IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<TransportDetails> transportDetailsRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<DispatchMaterialDetails> dispatchMaterialDetailsRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IAbpSession abpSession, UserManager userManager,
            IRepository<JobInstallationDetail> jobInstallRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<User, long> userRepository,
            IRepository<SSPaymentDetails> ssPaymentDetailsRepository,
            IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<STCPaymentDetails> stcPaymentDetailsRepository,
            IRepository<MeterConnectDetails> meterConnectDetailsRepository,
            IRepository<SubsidyClaimDetails> subsidyCliamDetailsRepository,
            IRepository<SubsidyClaimProjectDetails> subsidyclaimProjectDetailsRepository,
            ISubsidyTrackerExcelExporter SubsidyTrackerExcelExporter,
        IRepository<PaymentDetailsJob> paymentDetailsJobRepository)
        {
            _dbContextProvider = dbContextProvider;
            _subsidyCliamDetailsRepository = subsidyCliamDetailsRepository;
            _jobRepository = jobRepository;
            _subsidyclaimProjectDetailsRepository = subsidyclaimProjectDetailsRepository;
            _leadactivityRepository = leadactivityRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _userRepository = userRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
            _jobInstallRepository = jobInstallRepository;
            _ssPaymentDetailsRepository = ssPaymentDetailsRepository;
            _stcPaymentDetailsRepository = stcPaymentDetailsRepository;
            _applicationstatusRepository = applicationstatusRepository;
            _meterConnectDetailsRepository = meterConnectDetailsRepository;
            _paymentDetailsJobRepository = paymentDetailsJobRepository;
            _SubsidyTrackerExcelExporter = SubsidyTrackerExcelExporter;
            _userManager = userManager;
        }
        public async Task<PagedResultDto<GetSubsidyTrackerForViewDto>> GetAllSubsidyTracker(GetAllSubsidyTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<DateTime> dispatchDatelist = new List<DateTime>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

            var jobInstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).Select(x => x.JobId);
            var InstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null);
            var MeterConnectList = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId);
            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            var PaymentDataList = _paymentDetailsJobRepository.GetAll();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var filteredsubsidy = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId.JobFKId).Include(x => x.InstallationFKId.JobFKId.LeadFK)
                .Include(x => x.InstallationFKId.JobFKId.LeadFK.SubDivisionIdFk).Include(x => x.InstallationFKId.JobFKId.LeadFK.DiscomIdFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.InstallationFKId.JobFKId.JobNumber.Contains(input.Filter)
                                || e.InstallationFKId.JobFKId.LeadFK.CustomerName.Contains(input.Filter) || e.InstallationFKId.JobFKId.LeadFK.EmailId.Contains(input.Filter)
                                || e.InstallationFKId.JobFKId.LeadFK.Alt_Phone.Contains(input.Filter) || e.InstallationFKId.JobFKId.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.InstallationFKId.JobFKId.LeadId)))
                        .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.InstallationFKId.JobFKId.LeadFK.LeadStatusId))
                        .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.InstallationFKId.JobFKId.ApplicationStatusId))
                        .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(1)))
                        .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(2)))
                        .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                        .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                        .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.DiscomId))
                        //.WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.CircleId))
                        .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.DivisionId))
                        //.WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SubDivisionId))
                        .WhereIf(input.employeeIdFilter != null && input.employeeIdFilter.Count() > 0, e => input.employeeIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.ChanelPartnerID))
                        .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.SolarTypeId))
                        .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.ChanelPartnerID))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidyclaimdate", e => MeterConnectList.Select(x => x.MeterAgreementSentDate) != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidypaiddate", e => MeterConnectList.Select(x => x.MeterSubmissionDate) != null)
                        .Where(e => e.InstallationFKId.JobFKId.LeadFK.OrganizationUnitId == input.OrganizationUnit && e.MeterApplicationReadyStatus == 3);

            var pagedAndFilteredTransport = filteredsubsidy.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var subsidyDetails = from o in pagedAndFilteredTransport
                                       join o3 in _subsidyclaimProjectDetailsRepository.GetAll().Include(x=>x.SubsidyClaimFKId) on o.InstallationFKId.JobId equals o3.SubsidyJobId into j3
                                       from s3 in j3.DefaultIfEmpty()

                                   select new GetSubsidyTrackerForViewDto()
                                   {
                                       subsidyTracker = new SubsidyTrackerDto
                                       {
                                           Id = o.Id,
                                           leaddata = ObjectMapper.Map<LeadsDto>(o.InstallationFKId.JobFKId.LeadFK),
                                           //JobId = o.TransportJobId,
                                           ClaimNumber = s3.SubsidyClaimFKId.SubsidyClaimNo,  //o.TransportJobsIdFK.JobNumber,
                                           Division = o.InstallationFKId.JobFKId.LeadFK.DivisionIdFk.Name,
                                           TotalProjectCost = o.InstallationFKId.JobFKId.NetSystemCost,
                                           SubsidyAmount = PaymentDataList.Where(x=>x.PaymentJobId == o.InstallationFKId.JobId).FirstOrDefault().LessSubsidy,
                                           NetPayableAmount = PaymentDataList.Where(x => x.PaymentJobId == o.InstallationFKId.JobId).FirstOrDefault().NetAmount,
                                           //BankableAmount = InstallCompleted.Where(x => x.JobId == o.TransportJobId).FirstOrDefault().Id,                                           
                                           //ActualAmountRec = o.TransportJobsIdFK.LeadFK.SubDivisionIdFk.Name,
                                           //SubcidyFileVerify = s3.SubsidyClaimFKId.IsSubcidyFileVerify,
                                           //OCCopy = Convert.ToString(s3.SubsidyClaimFKId.OCCopyPath),
                                           //SubcidyReceived = s3.SubsidyClaimFKId.IsSubcidyReceived,
                                           //SubcidyReceipts = s3.SubsidyClaimFKId.SubcidyReceiptsPath     
                                       },

                                       subsidyTrackerSummaryCount = subsidyTrackerSummaryCount(filteredsubsidy.ToList())
                                   };
            var totalCount = await filteredsubsidy.CountAsync();
            return new PagedResultDto<GetSubsidyTrackerForViewDto>(totalCount, await subsidyDetails.ToListAsync());
        }
        public SubsidyTrackerSummaryCount subsidyTrackerSummaryCount(List<MeterConnectDetails> filteredLeads)
        {
            var output = new SubsidyTrackerSummaryCount();
            if (filteredLeads.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);
                var installCompletcount = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).ToList().Count();
                output.TotalSubsidyReceived = Convert.ToString(panelCount);
                output.TotalSubsidyInProcess = Convert.ToString(installCompletcount);
                output.TotalProjectsClaimed = Convert.ToString(installCompletcount); 
                output.TotalKilowatt = "";               
            }
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_ExportToExcel)]
        public async Task<FileDto> GetSubsidyTrackerToExcel(GetAllSubsidyTrackerInput input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
                List<int> sspaymentlist = new List<int>();
                List<int> stcpaymentlist = new List<int>();
                List<int> FollowupList = new List<int>();
                List<int> NextFollowupList = new List<int>();
                List<int> Assign = new List<int>();
                List<int> jobnumberlist = new List<int>();
                List<DateTime> dispatchDatelist = new List<DateTime>();
                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                var User_List = _userRepository.GetAll();
                var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

                var jobInstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).Select(x => x.JobId);
                var InstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null);
                var MeterConnectList = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId);
                var job_list = _jobRepository.GetAll();
                //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
                var PaymentDataList = _paymentDetailsJobRepository.GetAll();
                if (input.Filter != null)
                {
                    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                }

                if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
                {
                    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                }
                var filteredsubsidy = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId.JobFKId).Include(x => x.InstallationFKId.JobFKId.LeadFK)
                    .Include(x => x.InstallationFKId.JobFKId.LeadFK.SubDivisionIdFk).Include(x => x.InstallationFKId.JobFKId.LeadFK.DiscomIdFk)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.InstallationFKId.JobFKId.JobNumber.Contains(input.Filter)
                                    || e.InstallationFKId.JobFKId.LeadFK.CustomerName.Contains(input.Filter) || e.InstallationFKId.JobFKId.LeadFK.EmailId.Contains(input.Filter)
                                    || e.InstallationFKId.JobFKId.LeadFK.Alt_Phone.Contains(input.Filter) || e.InstallationFKId.JobFKId.LeadFK.MobileNumber.Contains(input.Filter)
                                    //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                    //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                    || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.InstallationFKId.JobFKId.LeadId)))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.InstallationFKId.JobFKId.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.InstallationFKId.JobFKId.ApplicationStatusId))
                            .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(1)))
                            .WhereIf(input.subsidyOcCopyFilter != null && input.subsidyOcCopyFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(2)))
                            .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                            .WhereIf(input.subsidyReceivedFilter != null && input.subsidyReceivedFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.DiscomId))
                            //.WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.CircleId))
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.DivisionId))
                            //.WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SubDivisionId))
                            .WhereIf(input.employeeIdFilter != null && input.employeeIdFilter.Count() > 0, e => input.employeeIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.ChanelPartnerID))
                            .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.SolarTypeId))
                            .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.InstallationFKId.JobFKId.LeadFK.ChanelPartnerID))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidyclaimdate", e => MeterConnectList.Select(x => x.MeterAgreementSentDate) != null)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "subsidypaiddate", e => MeterConnectList.Select(x => x.MeterSubmissionDate) != null)
                            .Where(e => e.InstallationFKId.JobFKId.LeadFK.OrganizationUnitId == input.OrganizationUnit && e.MeterApplicationReadyStatus == 3);

                var pagedAndFilteredTransport = filteredsubsidy.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                var subsidyDetails = from o in pagedAndFilteredTransport
                                     join o3 in _subsidyclaimProjectDetailsRepository.GetAll().Include(x => x.SubsidyClaimFKId) on o.InstallationFKId.JobId equals o3.SubsidyJobId into j3
                                     from s3 in j3.DefaultIfEmpty()

                                     select new GetSubsidyTrackerForViewDto()
                                     {
                                         subsidyTracker = new SubsidyTrackerDto
                                         {
                                             Id = o.Id,
                                             leaddata = ObjectMapper.Map<LeadsDto>(o.InstallationFKId.JobFKId.LeadFK),
                                             //JobId = o.TransportJobId,
                                             ClaimNumber = s3.SubsidyClaimFKId.SubsidyClaimNo,  //o.TransportJobsIdFK.JobNumber,
                                             Division = o.InstallationFKId.JobFKId.LeadFK.DivisionIdFk.Name,
                                             TotalProjectCost = o.InstallationFKId.JobFKId.NetSystemCost,
                                             SubsidyAmount = PaymentDataList.Where(x => x.PaymentJobId == o.InstallationFKId.JobId).FirstOrDefault().LessSubsidy,
                                             NetPayableAmount = PaymentDataList.Where(x => x.PaymentJobId == o.InstallationFKId.JobId).FirstOrDefault().NetAmount,
                                             //BankableAmount = InstallCompleted.Where(x => x.JobId == o.TransportJobId).FirstOrDefault().Id,                                           
                                             //ActualAmountRec = o.TransportJobsIdFK.LeadFK.SubDivisionIdFk.Name,
                                             //SubcidyFileVerify = s3.SubsidyClaimFKId.IsSubcidyFileVerify,
                                             //OCCopy = Convert.ToString(s3.SubsidyClaimFKId.OCCopyPath),
                                             //SubcidyReceived = s3.SubsidyClaimFKId.IsSubcidyReceived,
                                             //SubcidyReceipts = s3.SubsidyClaimFKId.SubcidyReceiptsPath     
                                         },

                                         subsidyTrackerSummaryCount = subsidyTrackerSummaryCount(filteredsubsidy.ToList())
                                     };
                var totalCount = await filteredsubsidy.CountAsync();
                var subsidytrackerListDtos = subsidyDetails.ToList();
                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _SubsidyTrackerExcelExporter.ExportToFile(subsidytrackerListDtos);
                }
                else
                {
                    return _SubsidyTrackerExcelExporter.ExportToFile(subsidytrackerListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public async Task<GetSubsidyDetailsForEditOutput> GetMeterConnectForEdit(EntityDto input)
        //{
        //    var meterConnect = await _meterConnectDetailsRepository.FirstOrDefaultAsync(x => x.InstallationId == input.Id);
        //    var output = new GetMeterConnectDetailsForEditOutput { meterConnectDto = ObjectMapper.Map<CreateOrEditMeterConnectDto>(meterConnect) };
        //    return output;
        //}
    }
}
