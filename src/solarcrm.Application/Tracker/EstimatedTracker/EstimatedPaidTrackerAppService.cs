﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Organizations;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.EntityFrameworkCore;
using solarcrm.Tracker.EstimatePaidTracker;
using solarcrm.Tracker.EstimatePaidTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Jobs.Dto;
using Newtonsoft.Json;
using Abp.Authorization;
using solarcrm.Authorization;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.EstimatedTracker.Exporting;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;

namespace solarcrm.Tracker.EstimatedTracker
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid)]
    public class EstimatedPaidTrackerAppService : solarcrmAppServiceBase, IEstimatePaidTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;


        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<Job.JobProductItem> _jobProductItemRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IEstimatedTrackerExcelExporter _EstimatedTrackerExcelExporter;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public EstimatedPaidTrackerAppService(
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<Job.JobProductItem> jobProductItemRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<Job.JobVariation.JobVariation> jobVariationRepository,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus, int> lookup_leadStatusRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            UserManager userManager,
            ITimeZoneConverter timeZoneConverter,
            IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IEstimatedTrackerExcelExporter EstimatedTrackerExcelExporter
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _userRepository = userRepository;
            _userManager = userManager;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _applicationstatusRepository = applicationstatusRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _EstimatedTrackerExcelExporter = EstimatedTrackerExcelExporter;
            _timeZoneConverter = timeZoneConverter;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid)]
        public async Task<PagedResultDto<GetEstimatedTrackerForViewDto>> GetAllEstimatedTracker(GetAllEstimatedTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            // var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            var jobnumberlist = new List<int>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                  || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                  || e.LeadFK.MobileNumber.Contains(input.Filter) 
                                  //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                  //|| e.LeadFK.AddressLine1.Contains(input.Filter)|| e.LeadFK.AddressLine2.Contains(input.Filter) 
                                  || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.Id)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                             .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                             .WhereIf(input.applicationstatusTrackerFilter != null && input.applicationstatusTrackerFilter.Count() > 0, e => input.applicationstatusTrackerFilter.Contains((int)e.ApplicationStatusId))
                             //.WhereIf(input.priorityFilter != null && input.priorityFilter.ToLower() == "high", e=> e.DepositNotes == "high") //Pending 
                             //.WhereIf(input.priorityFilter != null && input.priorityFilter.ToLower() == "medium", e => e.DepositNotes == "medium")
                             //.WhereIf(input.priorityFilter != null && input.priorityFilter.ToLower() == "low", e => e.DepositNotes == "low")
                             .WhereIf(input.estimatedPaidFilter != null && input.estimatedPaidFilter.ToLower() == "paid", e => e.EastimatePaidStatus.ToLower() == "paid")
                             .WhereIf(input.estimatedPaidFilter != null && input.estimatedPaidFilter.ToLower() == "notpaid", e => e.EastimatePaidStatus == null || e.EastimatePaidStatus == "" || e.EastimatePaidStatus.ToLower() == "notpaid")
                             .WhereIf(input.estimatedPayResonseFilter != null && input.estimatedPayResonseFilter.ToLower() == "bycustomer", e => e.EastimatePaymentResponse.ToLower() == "bycustomer")
                             .WhereIf(input.estimatedPayResonseFilter != null && input.estimatedPayResonseFilter.ToLower() == "bycompany", e => e.EastimatePaymentResponse.ToLower() == "bycompany")
                             .WhereIf(input.estimatedPayResonseFilter != null && input.estimatedPayResonseFilter.ToLower() == "bycp", e => e.EastimatePaymentResponse.ToLower() == "bycp")
                             .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId))
                             .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId))
                             .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId))
                             .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId))
                             .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId))
                             .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.LeadFK.TenantId))
                             .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "estimateDueDate" && input.StartDate != null, e => e.EastimateDueDate != null)
                             .WhereIf(input.ManualPaymentFilter != null && input.ManualPaymentFilter.ToLower() == "yes", e => e.EastimatePaymentRefNumber != null && e.EastimatePayDate != null && !string.IsNullOrEmpty(e.EstimatePaymentReceiptPath))
                             .WhereIf(input.ManualPaymentFilter != null && input.ManualPaymentFilter.ToLower() == "no", e => e.EastimatePaymentRefNumber == null && e.EastimatePayDate == null && string.IsNullOrEmpty(e.EstimatePaymentReceiptPath))

                             //.WhereIf(input.employeeCpList != null, e => input.employeeCpList.Contains((int)e.LeadFK.ChanelPartnerID))
                             .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit);//&& e.ApplicationStatusId == 4

            var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                       from s4 in j4.DefaultIfEmpty()

                       select new GetEstimatedTrackerForViewDto()
                       {
                           EstimateTracker = new EstimatedPaidTrackerDto
                           {
                               Id = o.Id,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                               ProjectNumber = o.JobNumber,
                               CustomerName = Convert.ToString(o.LeadFK.CustomerName),
                               Mobile = Convert.ToString(o.LeadFK.MobileNumber),
                               Discom = o.LeadFK.DiscomIdFk == null ? null: Convert.ToString(o.LeadFK.DiscomIdFk.Name),
                               ConsumerNumber = (long?)o.LeadFK.ConsumerNumber,
                               EastimateQuoteNo = (int?)o.EastimateQuoteNumber,
                               EastimateQuoteAmount = (int?)o.EastimateQuoteAmount,
                               EastimateDueDate = (DateTime?)o.EastimateDueDate,
                               EastimatePayResponse = o.EastimatePaymentResponse,
                               EastimatePaidStatus = o.EastimatePaidStatus,
                               EstimatePaymentReceiptPath = o.EstimatePaymentReceiptPath,
                               PanaltyDays = (int?)o.PenaltyDaysPending,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.ApplicationStatusIdFK,
                               PaymentLink = o.LeadFK.DiscomIdFk.PaymentLink,                              
                           },
                           estimatePadiTrackerSummaryCount = estimatedPaidTrackerSummaryCount(filteredJob.ToList())
                       };

            var totalCount = await filteredJob.CountAsync();

            return new PagedResultDto<GetEstimatedTrackerForViewDto>(totalCount, await jobs.ToListAsync());
        }
        public EstimatePaidTrackerSummaryCount estimatedPaidTrackerSummaryCount(List<Job.Jobs> filteredLeads)
        {
            var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x=>x.Quantity);
            EstimatePaidTrackerSummaryCount output = new EstimatePaidTrackerSummaryCount();
            output.Total = Convert.ToString(filteredLeads.Sum(x => x.TotalJobCost));            
            output.FeasiblityApproved = Convert.ToString(filteredLeads.Where(e => e.ApplicationStatusId == 4).Count());
            output.EastimatePaid = Convert.ToString(filteredLeads.Where(e => e.EastimatePaidStatus == "Paid").Count());
            output.DueDate = Convert.ToString(filteredLeads.Where(e => e.EastimateDueDate != null).Count());            
            output.TotalKillowatt = Convert.ToString(filteredLeads.Sum(e => e.PvCapacityKw));
            output.TotalPanel = Convert.ToString(panelCount);            
            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_QuickView)]
        public async Task<EstimatedTrackerQuickViewDto> GetEstimatedQuickView(EntityDto input)
        {
            try
            {
                //var leads = await _leadRepository.FirstOrDefaultAsync(input.Id);
                var job = await _jobRepository.GetAll().Include(e => e.LeadFK).Include(e => e.LeadFK.DiscomIdFk).Where(e => e.Id == input.Id).FirstOrDefaultAsync();
                var User_List = _userRepository.GetAll();
                var output = new EstimatedTrackerQuickViewDto
                {
                    Id = job.Id,
                    ProjectNumber = job.JobNumber,
                    CustomerName = job.LeadFK.CustomerName,
                    ConsumerNumber = job.LeadFK.ConsumerNumber,
                    MobileNumber = job.LeadFK.MobileNumber,
                    DiscomeName = job.LeadFK.DiscomIdFk.Name,
                    CpName = User_List.Where(x => x.Id == job.LeadFK.ChanelPartnerID).FirstOrDefault().FullName,
                    CpMobileNumber = User_List.Where(x => x.Id == job.LeadFK.ChanelPartnerID).FirstOrDefault().PhoneNumber,
                    EstimatePayementRefNumber = job.EastimatePaymentRefNumber,
                    EastimateQuoteNo = job.EastimateQuoteNumber,
                    EastimatePaidStatus = job.EastimatePaidStatus,
                    EastimateQuoteAmount = job.EastimateQuoteAmount,
                    EastimatePayDate = job.EastimatePayDate,
                    EastimateDueDate = job.EastimateDueDate,
                    EastimatePayResponse = job.EastimatePaymentResponse,
                    EastimatePayReceiptPath = job.EstimatePaymentReceiptPath
                };
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_ChangePayResponse)]
        public async Task AddToEstimatePayResponse(GetProjectTransferForEstimatePayResponseOutput input)
        {
            //var ChangedOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();

            foreach (var jobid in input.JobIds)
            {
                var job = await _jobRepository.FirstOrDefaultAsync(jobid);
                job.EastimatePaymentResponse = Convert.ToString(input.PayResponse);
                job.OrganizationUnitId = (int)input.OrganizationID;
                job.TenantId = (int)AbpSession.TenantId;
                job.LastModificationTime = DateTime.UtcNow;
                await _jobRepository.UpdateAsync(job);

                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 34;
                leadactivity.SectionId = 38;
                leadactivity.LeadActionNote = "Added Estimated Pay Response the " + input.PayResponse;
                leadactivity.LeadId = job.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }

                leadactivity.CreatorUserId = AbpSession.UserId;
                await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_PaymentReciept, AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_QuickView)]
        public async Task EstimatedDetailsEdit(CreateOrEditJobDto input)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(e => e.Id == input.Id);

            if (job != null)
            {
                job.EastimatePaymentRefNumber = input.EastimatePaymentRefNumber;
                job.EastimatePaidStatus = input.EastimatePaidStatus;
                job.EastimateQuoteNumber = input.EastimateQuoteNumber;
                job.EastimateQuoteAmount = input.EastimateQuoteAmount;
                job.EastimateDueDate = input.EastimateDueDate;
                job.EastimatePaymentResponse = input.EastimatePaymentResponse;

                #region Activity Log
                //Add Activity Log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 11;
                //leadactivity.LeadAcitivityID = 0;
                leadactivity.SectionId = 39;
                leadactivity.LeadActionNote = "Estimated Tracker Details Modified";
                leadactivity.LeadId = job.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                var List = new List<LeadtrackerHistory>();

                try
                {
                    List<Job.Jobs> objAuditOld_OnlineUserMst = new List<Job.Jobs>();
                    List<CreateOrEditJobDto> objAuditNew_OnlineUserMst = new List<CreateOrEditJobDto>();

                    DataTable Dt_Target = new DataTable();
                    DataTable Dt_Source = new DataTable();



                    objAuditOld_OnlineUserMst.Add(job);
                    objAuditNew_OnlineUserMst.Add(input);

                    //Add Activity Log
                    LeadtrackerHistory leadActivityLog = new LeadtrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        leadActivityLog.TenantId = (int)AbpSession.TenantId;
                    }

                    object newvalue = objAuditNew_OnlineUserMst;
                    Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                    if (objAuditOld_OnlineUserMst != null)
                    {
                        Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                    }
                    foreach (DataRow dr_S in Dt_Source.Rows)
                    {
                        foreach (DataColumn dc_S in Dt_Source.Columns)
                        {

                            if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = dc_S.ColumnName;
                                objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                                //objAuditInfo.Action = "Edit";
                                objAuditInfo.Action = Dt_Target.Rows[0][dc_S.ColumnName].ToString() == "" ? "Add" : "Edit";
                                objAuditInfo.LeadId = job.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                        }
                    }
                }
                catch
                {
                }
                #endregion


            }

            return;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_ExportToExcel)]
        public async Task<FileDto> GetEstimatedTrackerToExcel(GetAllEstimatedTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            // var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            var jobnumberlist = new List<int>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }


            var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                  || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                  || e.LeadFK.MobileNumber.Contains(input.Filter)
                                  //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                  //|| e.LeadFK.AddressLine1.Contains(input.Filter)|| e.LeadFK.AddressLine2.Contains(input.Filter) 
                                  || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.Id)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                             .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                             .WhereIf(input.applicationstatusTrackerFilter != null && input.applicationstatusTrackerFilter.Count() > 0, e => input.applicationstatusTrackerFilter.Contains((int)e.ApplicationStatusId))
                             //.WhereIf(input.priorityFilter != null && input.priorityFilter.ToLower() == "high", e=> e.DepositNotes == "high") //Pending 
                             //.WhereIf(input.priorityFilter != null && input.priorityFilter.ToLower() == "medium", e => e.DepositNotes == "medium")
                             //.WhereIf(input.priorityFilter != null && input.priorityFilter.ToLower() == "low", e => e.DepositNotes == "low")
                             .WhereIf(input.estimatedPaidFilter != null && input.estimatedPaidFilter.ToLower() == "paid", e => e.EastimatePaidStatus.ToLower() == "paid")
                             .WhereIf(input.estimatedPaidFilter != null && input.estimatedPaidFilter.ToLower() == "notpaid", e => e.EastimatePaidStatus == null || e.EastimatePaidStatus == "" || e.EastimatePaidStatus.ToLower() == "notpaid")
                             .WhereIf(input.estimatedPayResonseFilter != null && input.estimatedPayResonseFilter.ToLower() == "bycustomer", e => e.EastimatePaymentResponse.ToLower() == "bycustomer")
                             .WhereIf(input.estimatedPayResonseFilter != null && input.estimatedPayResonseFilter.ToLower() == "bycompany", e => e.EastimatePaymentResponse.ToLower() == "bycompany")
                             .WhereIf(input.estimatedPayResonseFilter != null && input.estimatedPayResonseFilter.ToLower() == "bycp", e => e.EastimatePaymentResponse.ToLower() == "bycp")
                             .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId))
                             .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId))
                             .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId))
                             .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId))
                             .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId))
                             .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.LeadFK.TenantId))
                             .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "estimateDueDate" && input.StartDate != null, e => e.EastimateDueDate != null)
                             .WhereIf(input.ManualPaymentFilter != null && input.ManualPaymentFilter.ToLower() == "yes", e => e.EastimatePaymentRefNumber != null && e.EastimatePayDate != null && !string.IsNullOrEmpty(e.EstimatePaymentReceiptPath))
                             .WhereIf(input.ManualPaymentFilter != null && input.ManualPaymentFilter.ToLower() == "no", e => e.EastimatePaymentRefNumber == null && e.EastimatePayDate == null && string.IsNullOrEmpty(e.EstimatePaymentReceiptPath))
                             //.WhereIf(input.employeeCpList != null, e => input.employeeCpList.Contains((int)e.LeadFK.ChanelPartnerID))
                             .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit);


            var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                       from s4 in j4.DefaultIfEmpty()

                       select new GetEstimatedTrackerForViewDto()
                       {
                           EstimateTracker = new EstimatedPaidTrackerDto
                           {
                               Id = o.Id,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                               ProjectNumber = o.JobNumber,
                               CustomerName = Convert.ToString(o.LeadFK.CustomerName),
                               Mobile = Convert.ToString(o.LeadFK.MobileNumber),
                               Discom = Convert.ToString(o.LeadFK.DiscomIdFk.Name),
                               ConsumerNumber = (long?)o.LeadFK.ConsumerNumber,
                               EastimateQuoteNo = (int?)o.EastimateQuoteNumber,
                               EastimateQuoteAmount = (int?)o.EastimateQuoteAmount,
                               EastimateDueDate = (DateTime?)o.EastimateDueDate,
                               EastimatePayResponse = o.EastimatePaymentResponse,
                               EastimatePaidStatus = o.EastimatePaidStatus,
                               EstimatePaymentReceiptPath = o.EstimatePaymentReceiptPath,
                               PanaltyDays = (int?)o.PenaltyDaysPending,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.ApplicationStatusIdFK,
                               PaymentLink = o.LeadFK.DiscomIdFk.PaymentLink
                           },
                       };

            var applicationtrackerListDtos = await jobs.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _EstimatedTrackerExcelExporter.ExportToFile(applicationtrackerListDtos);
            }
            else
            {
                return _EstimatedTrackerExcelExporter.ExportToFile(applicationtrackerListDtos);
            }
        }

    }
}
