﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.EstimatePaidTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.EstimatedTracker.Exporting
{
    public class EstimatedTrackerExcelExporter : NpoiExcelExporterBase, IEstimatedTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public EstimatedTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetEstimatedTrackerForViewDto> estimatedtrack)
        {
            return CreateExcelPackage(
                "EstimatedTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("EstimatedTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("CustomerName"),
                        L("Mobile"),
                        L("Discom"),
                        L("ConsumerNumber"),
                        L("EastimateQuoteNo"),
                        L("EastimateQuoteAmount"),
                        L("EastimateDueDate"),                       
                        L("EastimatePayResponse"),
                        L("EastimatePaidStatus"),
                        L("PanaltyDays"),
                        L("PaymentLink"),
                        L("FollowDiscription"),
                        L("FollowDate"),
                        L("Comment")
                        );
                    AddObjects(
                        sheet, estimatedtrack,
                        _ => _.EstimateTracker.ProjectNumber,
                        _ => _.EstimateTracker.CustomerName,
                        _ => _.EstimateTracker.Mobile,
                        _ => _.EstimateTracker.Discom,
                        _ => _.EstimateTracker.ConsumerNumber,
                        _ => _.EstimateTracker.EastimateQuoteNo,
                        _ => _.EstimateTracker.EastimateQuoteAmount,
                        _ => _.EstimateTracker.EastimateDueDate,
                        _ => _.EstimateTracker.EastimatePayResponse,
                        _ => _.EstimateTracker.EastimatePaidStatus,
                        _ => _.EstimateTracker.PanaltyDays,
                         _ => _.EstimateTracker.PaymentLink,
                        _ => _.EstimateTracker.FollowDiscription,
                        _ => _.EstimateTracker.FollowDate,
                        _ => _.EstimateTracker.Comment
                        );
                });
        }
    }
}
