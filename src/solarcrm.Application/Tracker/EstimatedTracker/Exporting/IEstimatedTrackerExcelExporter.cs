﻿
using solarcrm.Dto;
using solarcrm.Tracker.EstimatePaidTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.EstimatedTracker.Exporting
{
    public interface IEstimatedTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetEstimatedTrackerForViewDto> estimateTracker);
    }
}
