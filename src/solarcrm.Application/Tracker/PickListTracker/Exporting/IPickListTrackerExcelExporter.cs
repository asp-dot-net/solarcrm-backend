﻿using solarcrm.Dto;
using solarcrm.Tracker.PickListTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.PickListTracker.Exporting
{
    public interface IPickListTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetPickListTrackerForViewDto> picklistTracker);
    }
}
