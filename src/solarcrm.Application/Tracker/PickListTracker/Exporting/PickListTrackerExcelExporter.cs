﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.PickListTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.PickListTracker.Exporting
{
    public class PickListTrackerExcelExporter : NpoiExcelExporterBase, IPickListTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PickListTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetPickListTrackerForViewDto> picklisttrack)
        {
            return CreateExcelPackage(
                "PickListTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("PickListTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("LeadStatus"),
                        L("ApplicationStatus"),
                        L("CustomerName"),
                        L("Address"),
                        L("Mobile"),
                        L("ActivityDescription"),
                        L("ReminderTime"),
                        L("QueryDescription"),
                        L("Comment")
                        );
                    AddObjects(
                        sheet, picklisttrack
                        //_ => _.appTracker.ProjectNumber,
                        //_ => _.appTracker.LeadStatusName,
                        //_ => _.appTracker.ApplicationStatusdata,
                        //_ => _.appTracker.CustomerName,
                        //_ => _.appTracker.Address,
                        //_ => _.appTracker.Mobile,
                        //_ => _.appTracker.FollowDiscription,
                        //_ => _.appTracker.FollowDate,
                        //_ => _.appTracker.QuesryDescription,
                        //_ => _.appTracker.Comment
                        );
                });

        }
    }
    }
