﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.Timing.Timezone;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.Dispatch;
using solarcrm.EntityFrameworkCore;
using solarcrm.Tracker.PickListTracker.Dto;
using solarcrm.Tracker.PickListTracker.Exporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using System.Data;
using System.Linq.Dynamic.Core;
using Api2Pdf;
using solarcrm.PickList;
using Abp.Authorization;
using solarcrm.Authorization;
using solarcrm.JobInstallation;
using solarcrm.Job;
using solarcrm.Payments;
using solarcrm.DataVaults.StockItem;
using solarcrm.Organizations;
using solarcrm.DataVaults.HeightStructure;
using solarcrm.DataVaults.DispatchType;
using solarcrm.Tracker.DispatchTracker.Dto;

namespace solarcrm.Tracker.PickListTracker
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_PickList)]
    public class PickListTrackerAppService : solarcrmAppServiceBase, IPickListTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<PickListData> _picklistdataRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<DispatchMaterialDetails> _dispatchMaterialDetailsRepository;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IPickListTrackerExcelExporter _PickListTrackerExcelExporter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<JobInstallationDetail> _jobInstallationDetailRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<OrganizationUnit, long> _OrganizationRepository;
        private readonly IRepository<BankDetailsOrganization> _bankOrganizationDetailsRepository;
        private readonly IRepository<DispatchType> _dispatchTypeRepository;
        private readonly IRepository<HeightStructure> _heightStructureRepository;
        private readonly IRepository<DispatchProductItem> _dispatchProductRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public PickListTrackerAppService(IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<DispatchType> dispatchTypeRepository,
            IRepository<HeightStructure> heightStructureRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<PickListData> picklistdataRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<StockItem> stockItemRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<DispatchMaterialDetails> dispatchMaterialDetailsRepository,
            IRepository<PaymentDetailsJob> paymentDetailRepository,
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IPickListTrackerExcelExporter PickListTrackerExcelExporter,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus, int> lookup_leadStatusRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            UserManager userManager,
            ITimeZoneConverter timeZoneConverter,
            // IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<JobInstallationDetail> jobInstallationDetailRepository,
            IRepository<OrganizationUnit, long> OrganizationRepository,
            IRepository<BankDetailsOrganization> bankOrganizationDetailsRepository,
            IRepository<DispatchProductItem> dispatchProductRepository
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadactivityRepository = leadactivityRepository;
            _paymentDetailRepository = paymentDetailRepository;
            _picklistdataRepository = picklistdataRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _jobRepository = jobRepository;
            _leadRepository = leadRepository;
            _stockItemRepository = stockItemRepository;
            _dispatchMaterialDetailsRepository = dispatchMaterialDetailsRepository;
            _PickListTrackerExcelExporter = PickListTrackerExcelExporter;
            _userRepository = userRepository;
            _userManager = userManager;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _jobInstallationDetailRepository = jobInstallationDetailRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
            _OrganizationRepository = OrganizationRepository;
            _bankOrganizationDetailsRepository = bankOrganizationDetailsRepository;
            _dispatchTypeRepository = dispatchTypeRepository;
            _heightStructureRepository = heightStructureRepository;
            _dispatchProductRepository = dispatchProductRepository;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_PickList)]
        public async Task<PagedResultDto<GetPickListTrackerForViewDto>> GetAllPickListTracker(GetAllPickListTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> jobnumberlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);

            List<Job.Jobs> job_list = await _jobRepository.GetAllListAsync();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }


            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var filteredPickList = _dispatchMaterialDetailsRepository.GetAll().Include(e => e.DispatchJobsIdFK).Include(e => e.DispatchJobsIdFK.LeadFK).Include(x => x.DispatchJobsIdFK.LeadFK.LeadStatusIdFk)
                .Include(x => x.DispatchJobsIdFK.ApplicationStatusIdFK)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DispatchJobsIdFK.JobNumber.Contains(input.Filter)
                                || e.DispatchJobsIdFK.LeadFK.CustomerName.Contains(input.Filter) || e.DispatchJobsIdFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.DispatchJobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.DispatchJobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.DispatchJobsIdFK.LeadId)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.DispatchJobsIdFK.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.DispatchJobsIdFK.ApplicationStatusId))
                            .WhereIf(input.otpPendingFilter != null, e => input.otpPendingFilter.ToLower() == e.DispatchJobsIdFK.OtpVerifed.ToLower()) //OTPpending Filter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "yes", e => (e.DispatchJobsIdFK.LastComment != null || e.DispatchJobsIdFK.LastComment != "")) //Query RaisedFilter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "no", e => (e.DispatchJobsIdFK.LastComment == null || e.DispatchJobsIdFK.LastComment == "")) //Query RaisedFilter                            
                            .Where(e => e.DispatchJobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit && e.DispatchDate != null);

            var pagedAndFilteredJob = filteredPickList.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o3 in _lookup_leadStatusRepository.GetAll() on o.DispatchJobsIdFK.LeadFK.LeadStatusId equals o3.Id into j3
                       from s3 in j3.DefaultIfEmpty()

                           //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                           //from s4 in j4.DefaultIfEmpty()

                       select new GetPickListTrackerForViewDto()
                       {
                           picklistTracker = new PickListTrackerDto
                           {
                               Id = o.Id,
                               DispatchDate = o.DispatchDate,
                               DispatchJobId = o.DispatchJobId,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.DispatchJobsIdFK.LeadFK),
                               ProjectNumber = o.DispatchJobsIdFK.JobNumber,
                               //JobStatus = o.JobStatusFk.Name,
                               CustomerName = o.DispatchJobsIdFK.LeadFK.CustomerName,
                               Address = (o.DispatchJobsIdFK.LeadFK.AddressLine1 == null ? "" : o.DispatchJobsIdFK.LeadFK.AddressLine1 + ", ") + (o.DispatchJobsIdFK.LeadFK.AddressLine2 == null ? "" : o.DispatchJobsIdFK.LeadFK.AddressLine2 + ", ") + (o.DispatchJobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.DispatchJobsIdFK.LeadFK.Pincode == null ? "" : o.DispatchJobsIdFK.LeadFK.Pincode),
                               Mobile = o.DispatchJobsIdFK.LeadFK.MobileNumber,
                               LeadStatusName = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.Status,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.DispatchJobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.DispatchJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Notes = o.DispatchJobsIdFK.LeadFK.Notes,
                               LastModificationTime = o.LastModificationTime,
                               CreationTime = (DateTime)o.CreationTime,
                               //QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                               LeadStatusColorClass = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.DispatchJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.DispatchJobsIdFK.ApplicationStatusIdFK,
                           },
                           pickListTrackerSummaryCount = picklistTrackerSummaryCount(filteredPickList.ToList())
                       };
            var totalCount = await filteredPickList.CountAsync();
            return new PagedResultDto<GetPickListTrackerForViewDto>(totalCount, await jobs.ToListAsync());
        }
        public PickListTrackerSummaryCount picklistTrackerSummaryCount(List<DispatchMaterialDetails> filteredPickList)
        {
            var output = new PickListTrackerSummaryCount();
            if (filteredPickList.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);

                output.Total = Convert.ToString(filteredPickList.Sum(x => x.DispatchJobsIdFK.TotalJobCost));
                output.TotalPanel = Convert.ToString(panelCount);
                output.TotalKillowatt = Convert.ToString(filteredPickList.Sum(x => x.DispatchJobsIdFK.PvCapacityKw));
            }
            return output;
        }

        public async Task<List<GetPickListTrackerForViewDto>> GetAll(int input)
        {
            JobInstallationDetail installationdetail = new JobInstallationDetail();
            var filteredQuotations = _dispatchMaterialDetailsRepository.GetAll().Include(x => x.DispatchHeightStructureIdFK).Include(x => x.DispatchTypeIdFK)
                        .WhereIf(input > 0, e => false || e.DispatchJobId == input).OrderBy("id desc");
            installationdetail = _jobInstallationDetailRepository.GetAll().Where(x => x.JobId == input).FirstOrDefault();
            List<GetPickListTrackerForViewDto> piclkistData = new List<GetPickListTrackerForViewDto>();
            if (installationdetail != null)
            {
                 piclkistData = (from o in filteredQuotations
                                   select new GetPickListTrackerForViewDto()
                                   {
                                       picklistTracker = new PickListTrackerDto
                                       {
                                           Id = o.Id,
                                           HeightStructureName = o.DispatchHeightStructureIdFK.Name,
                                           PickListType = o.DispatchTypeIdFK.Name,
                                           InstallationDate = installationdetail.InstallStartDate,
                                           InstallerName = _userRepository.GetAll().Where(x => x.Id == installationdetail.JobInstallerID).FirstOrDefault().Name,
                                           CreatedBy = _userRepository.GetAll().Where(x => x.Id == o.CreatorUserId).FirstOrDefault().Name,
                                           DispatchDate = o.DispatchDate == DateTime.MinValue ? null : o.DispatchDate,
                                       }
                                   }).ToList();
            }           
            return piclkistData;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_PickList_QuickView)]
        public async Task<GetPickListItemForEditOutput> GetPickLitsForView(EntityDto input)
        {
            var picklistData = await _picklistdataRepository.GetAll().Include(x => x.BillOfMaterialIdFK.StockItemFk).Where(x => x.PickListDisptchId == input.Id).ToListAsync(); //await
            var dispatchStockitem = await _dispatchProductRepository.GetAll().Include(x=>x.ProductItemFk).Where(x=>x.DispatchId == input.Id).ToListAsync();
            var output = new GetPickListItemForEditOutput
            {
                picklistStockItem = ObjectMapper.Map<List<CreateOrEditDispatchProductItemDto>>(dispatchStockitem),
                picklistItem = ObjectMapper.Map<List<CreateOrEditPicklistItemDto>>(picklistData)
            };
            foreach (var item in output.picklistItem)
            {
                item.MaterialName = picklistData.Where(x => x.BillOfMaterialId == item.BillOfMaterialId).FirstOrDefault().BillOfMaterialIdFK.StockItemFk.Name;
            }
            return output;
        }
        //[AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_SSBillAddorEdit)]
        public async Task PickListItemForAddOrEdit(List<PickListData> input)
        {

            List<PickListData> picklist = new List<PickListData>();
            //var yy = ObjectMapper.Map<List<CreateOrEditPicklistItemDto>>(input);
            _dbContextProvider.GetDbContext().pickListData.UpdateRange(input);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();
        }

        public async Task<GetPickListItemForEditOutput> EditViewPickList(EntityDto input)
        {
            var picklist = await _picklistdataRepository.GetAll().Where(x => x.Id == input.Id).Select(x => new
            {
                MaterialName = x.BillOfMaterialIdFK.StockItemFk.Name,
                PickListQty = x.PickListQty,
                PickListDisptchId = x.PickListDisptchId,
                BillOfMaterialId = x.BillOfMaterialId
            }).ToListAsync();

            var output = new GetPickListItemForEditOutput { picklistItem = ObjectMapper.Map<List<CreateOrEditPicklistItemDto>>(picklist) };

            return output;
        }
        public async Task<GetPickListItemForEditOutput> UpdatePickList(List<CreateOrEditPicklistItemDto> input)
        {
            List<PickListData> picklist = new List<PickListData>();
            foreach (var item in input)
            {
                PickListData data = new PickListData();
                data.BillOfMaterialId = item.BillOfMaterialId;
                data.PickListDisptchId = item.PickListDisptchId;
                data.PickListQty = item.PickListQty;
                picklist.Add(data);
            }
            _dbContextProvider.GetDbContext().pickListData.UpdateRange(picklist);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();
            return new GetPickListItemForEditOutput { };
        }
        public async Task<PickListQuotationDataDetailsDto> PickListQuotationData(int JobId, int dispatchId, int organizationId)
        {
            PickListQuotationDataDetailsDto picklistDto = new PickListQuotationDataDetailsDto();

            JobInstallationDetail JobDetail = await _jobInstallationDetailRepository.GetAll().Include(x => x.JobFKId).Where(e => e.JobId == JobId).FirstOrDefaultAsync();
            Leads Lead = await _leadRepository.GetAll().Where(e => e.Id == JobDetail.JobFKId.LeadId).FirstOrDefaultAsync();
            DispatchMaterialDetails dispatchDetail = _dispatchMaterialDetailsRepository.GetAll().Include(x => x.DispatchHeightStructureIdFK).Include(x => x.DispatchStockItemIdFK)
                .Include(x => x.DispatchStockItemIdFK.StockCategoryFk).Where(x => x.DispatchJobId == JobId && x.Id == dispatchId).FirstOrDefault();
            PaymentDetailsJob Paymentdata = await _paymentDetailRepository.GetAll().Where(x => x.PaymentJobId == JobId).FirstOrDefaultAsync();
            User userdetails = await _userRepository.GetAll().Where(x => x.Id == JobDetail.JobInstallerID).FirstOrDefaultAsync();
            if (JobDetail != null && Lead != null)
            {
                picklistDto.CustomerName = Lead.CustomerName;
                picklistDto.Address = Lead.AddressLine1;
                picklistDto.Mobile = Lead.MobileNumber;
                picklistDto.AltPhone = Lead.Alt_Phone;
                picklistDto.ProjectNo = JobDetail.JobFKId == null ? null : JobDetail.JobFKId.JobNumber;
                picklistDto.ApplicationNo = JobDetail.JobFKId == null ? null: JobDetail.JobFKId.ApplicationNumber;
                picklistDto.SystemSize = JobDetail.JobFKId == null ? null : JobDetail.JobFKId.PvCapacityKw;
                picklistDto.StructureHeight = dispatchDetail.DispatchHeightStructureIdFK == null ? null : dispatchDetail.DispatchHeightStructureIdFK.Name;
                picklistDto.InstallationDate = JobDetail.InstallStartDate;
                picklistDto.InstallerName = userdetails == null ? null : userdetails.FullName;
                picklistDto.InstallerMobile = userdetails == null ? null : userdetails.PhoneNumber;
                picklistDto.InstallerNote = JobDetail.JobFKId == null ? null : JobDetail.JobFKId.InstallerNotes;
                picklistDto.TotalSystemCost = Paymentdata.NetAmount;
                picklistDto.PaidToDate = Paymentdata.PaidTillDate;
                picklistDto.BalanceOwing = Paymentdata.BalanceOwing;
                picklistDto.PickedBy = "";
                picklistDto.PickedDate = DateTime.Now;
                picklistDto.JobProductItemList = ObjectMapper.Map<List<object>>(_dispatchProductRepository.GetAll().Where(x => x.DispatchId == dispatchDetail.Id).Select(x => new
                {
                    itemName = x.ProductItemFk.StockCategoryFk == null ? null : x.ProductItemFk.StockCategoryFk.Name,
                    itemDescription = x.ProductItemFk == null ? null : x.ProductItemFk.Name,
                    itemQty = x.Quantity
                }).ToList());
                picklistDto.JobPickList = ObjectMapper.Map<List<object>>(_picklistdataRepository.GetAll().Where(x => x.PickListDisptchId == dispatchDetail.Id).Select(x => new
                {
                    productname = x.BillOfMaterialIdFK == null ? null : x.BillOfMaterialIdFK.StockItemFk.Name,
                    productQty = x.PickListQty,
                    productCategory = x.BillOfMaterialIdFK == null ? null : x.BillOfMaterialIdFK.StockItemFk.StockCategoryFk.Name
                }).ToList());
                picklistDto.OrganizationDetails = ObjectMapper.Map<object>(_OrganizationRepository.GetAll().Where(x => x.Id == organizationId).ToList());
                picklistDto.OrganizationbankDetails = ObjectMapper.Map<List<object>>(_bankOrganizationDetailsRepository.GetAll().Where(x => x.BankDetailOrganizationId == 5).ToList());
            }
            return picklistDto;
        }
        //[AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_PickList_Comment)]
        public async Task<string> GeneratePickListPdf(string JobNumber, object PickListString)
        {

            var a2pClient = new Api2Pdf.Api2Pdf("7e764038-dd32-4543-8249-22b867fc6964");

            var options = new ChromeHtmlToPdfOptions
            {
                Delay = 0,
                PuppeteerWaitForMethod = "WaitForNavigation",
                PuppeteerWaitForValue = "Load",
                UsePrintCss = true,
                Landscape = false,
                PrintBackground = true,
                DisplayHeaderFooter = false,
                Width = "8.27in",
                Height = "11.69in",
                MarginBottom = "0in",
                MarginTop = "0in",
                MarginLeft = "0in",
                MarginRight = "0in",
                PageRanges = "1-10000",
                Scale = 1,
                OmitBackground = false
            };

            var request = new ChromeHtmlToPdfRequest
            {
                Html = PickListString.ToString().Replace("\"", "'"),
                FileName = "PickList_" + JobNumber + ".pdf",
                Inline = true,
                Options = options,
                UseCustomStorage = false,
            };

            var apiResponse = a2pClient.Chrome.HtmlToPdf(request);

            return apiResponse.FileUrl;
        }
    }
}
