﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.DepositeTracker.Dto;
using solarcrm.Tracker.DepositeTracker.Exporting;
using solarcrm.Tracker.DispatchTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.DispatchTracker.Exporting
{
    public class DispatchTrackerExcelExporter : NpoiExcelExporterBase, IDispatchTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DispatchTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetDispatchItemForViewDto> dispatchtrack)
        {
            return CreateExcelPackage(
                "DispatchTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("DispatchTracker");

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("CustomerName"),
                        L("Mobile"),
                        L("LatestDispatchDate"),
                        L("VerifyPickList"),
                        L("ExtraMaterialApproved"),
                        L("Comment")
                        );
                    AddObjects(
                        sheet, dispatchtrack,
                        _ => _.dispatchItem.ProjectNumber,
                        _ => _.dispatchItem.CustomerName,
                        _ => _.dispatchItem.Mobile,
                        _ => _.dispatchItem.DispatchDate,
                        _ => _.dispatchItem.IsActive,
                        _ => _.dispatchItem.IsApprovedMaterial,
                        _ => _.dispatchItem.Comment
                        );
                });
        }

    }
}
