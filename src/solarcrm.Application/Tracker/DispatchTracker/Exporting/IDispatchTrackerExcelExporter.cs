﻿using solarcrm.Dto;
using solarcrm.Tracker.DispatchTracker.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.DispatchTracker.Exporting
{
    public interface IDispatchTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetDispatchItemForViewDto> depositTracker);
    }
}
