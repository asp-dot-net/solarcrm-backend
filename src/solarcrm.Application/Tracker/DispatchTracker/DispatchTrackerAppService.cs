﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Timing.Timezone;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.DataVaults.Price;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.StockItem;
using solarcrm.DataVaults.Variation;
using solarcrm.EntityFrameworkCore;
using solarcrm.Job;
using solarcrm.Payments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.BillOfMaterial;
using solarcrm.PickList;
using Abp.Threading;
using Abp.Authorization;
using solarcrm.Authorization;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.Job.JobVariation;
using solarcrm.Dispatch;
using solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial;
using solarcrm.Tracker.DispatchTracker.Dto.DispatchStockTransfer;
using solarcrm.Tracker.DispatchTracker.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.DispatchTracker.Exporting;
using Stripe;

namespace solarcrm.Tracker.DispatchTracker
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Dispatch)]
    public class DispatchTrackerAppService : solarcrmAppServiceBase, IDispatchAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<BillOfMaterial> _billofMaterialRepository;
        private readonly IRepository<PickListData> _picklistdataRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<DispatchMaterialDetails> _dispatchMaterialDetailsRepository;
        private readonly IRepository<DispatchExtraMaterials> _dispatchExtraMaterialDetailsRepository;
        private readonly IRepository<DispatchStockMaterialTransfer> _dispatchStocktransferMaterialDetailsRepository;
        private readonly IRepository<JobProductItem> _productItemdataepository;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailjobRepository;
        private readonly IRepository<PaymentReceiptUploadJob> _paymentReceiptUploadjobRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<StockCategory> _stockCategoryRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<JobVariation> _jobVariationRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<SSPaymentDetails> _ssPaymentDetailsRepository;
        private readonly IRepository<STCPaymentDetails> _stcPaymentDetailsRepository;
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly IRepository<Prices> _priceRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<DocumentList> _documentRepository;
        private readonly IRepository<DispatchProductItem> _dispatchProductItemRepository;
        private readonly IRepository<DispatchReturnMaterials> _dispatchReturnRepository;
        private readonly IDispatchTrackerExcelExporter _DispatchTrackerExcelExporter;
        public DispatchTrackerAppService(
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<BillOfMaterial> billofMaterialRepository,
            IRepository<PickListData> picklistdataRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<JobProductItem> productItemdataepository,
            IRepository<DispatchMaterialDetails> dispatchMaterialDetailsRepository,
            IRepository<SSPaymentDetails> ssPaymentDetailsRepository,
            IRepository<STCPaymentDetails> stcPaymentDetailsRepository,
            IRepository<PaymentDetailsJob> paymentDetailjobRepository,
            IRepository<PaymentReceiptUploadJob> paymentReceiptUploadjobRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<StockItem> stockItemRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<StockCategory> stockCategoryRepository,
            IRepository<Variation> variationRepository,
            IRepository<JobVariation> jobVariationRepository,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus> leadStatusRepository,
            IRepository<Prices> priceRepository,
            IRepository<DataVaulteActivityLog> dataVaultsActivityLogRepository,
            IRepository<DocumentList> documentRepository,
            IRepository<LeadDocuments> leadDocumentRepository,
            IRepository<DispatchExtraMaterials> dispatchExtraMaterialDetailsRepository,
            IRepository<DispatchStockMaterialTransfer> dispatchStocktransferMaterialDetailsRepository,
            ITimeZoneConverter timeZoneConverter,
             IRepository<DispatchProductItem> dispatchProductItemRepository,
            IRepository<DispatchReturnMaterials> dispatchReturnRepository,
            IDispatchTrackerExcelExporter DispatchTrackerExcelExporter
        )
        {
            _dispatchProductItemRepository = dispatchProductItemRepository;
            _dispatchReturnRepository = dispatchReturnRepository;
            _dbContextProvider = dbContextProvider;
            _billofMaterialRepository = billofMaterialRepository;
            _picklistdataRepository = picklistdataRepository;
            _jobRepository = jobRepository;
            _productItemdataepository = productItemdataepository;
            _paymentDetailjobRepository = paymentDetailjobRepository;
            _paymentReceiptUploadjobRepository = paymentReceiptUploadjobRepository;
            _ssPaymentDetailsRepository = ssPaymentDetailsRepository;
            _dispatchMaterialDetailsRepository = dispatchMaterialDetailsRepository;
            _stcPaymentDetailsRepository = stcPaymentDetailsRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _stockItemRepository = stockItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _stockCategoryRepository = stockCategoryRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _userRepository = userRepository;
            _leadStatusRepository = leadStatusRepository;
            _priceRepository = priceRepository;
            _dataVaultsActivityLogRepository = dataVaultsActivityLogRepository;
            _documentRepository = documentRepository;
            _leadDocumentRepository = leadDocumentRepository;
            _timeZoneConverter = timeZoneConverter;
            _dispatchExtraMaterialDetailsRepository = dispatchExtraMaterialDetailsRepository;
            _dispatchStocktransferMaterialDetailsRepository = dispatchStocktransferMaterialDetailsRepository;
            _DispatchTrackerExcelExporter = DispatchTrackerExcelExporter;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Dispatch)]
        public async Task<PagedResultDto<GetDispatchItemForViewDto>> GetAllDispatch(GetAllDispatchItemsInput input)
        {
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            var SDate = _timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            var EDate = _timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);


            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            var jobnumberlist = new List<int>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            if (input.FilterbyDate == "billdate")
            {
                sspaymentlist = _ssPaymentDetailsRepository.GetAll().Include(x => x.PaymentIdFK).Where(x => x.SSDate != null).Select(x => x.PaymentIdFK.PaymentJobId).ToList();
                stcpaymentlist = _stcPaymentDetailsRepository.GetAll().Include(x => x.PaymentIdFK).Where(x => x.STCDate != null).Select(x => x.PaymentIdFK.PaymentJobId).ToList();
            }

            var FollowupList = new List<int>();
            var NextFollowupList = new List<int>();
            var Assign = new List<int>();

            var paymentdetails = await _paymentDetailjobRepository.GetAll().Include(x => x.JobsIdFK).ToListAsync();
            var dispatchdata = await _dispatchMaterialDetailsRepository.GetAll().ToListAsync();
            var filtereddispatch = _dispatchMaterialDetailsRepository.GetAll().Include(e => e.DispatchJobsIdFK).Include(e => e.DispatchJobsIdFK.ApplicationStatusIdFK).Include(e => e.DispatchJobsIdFK.LeadFK).Include(e => e.DispatchJobsIdFK.LeadFK.SolarTypeIdFk)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DispatchJobsIdFK.JobNumber.Contains(input.Filter)
                                || e.DispatchJobsIdFK.LeadFK.CustomerName.Contains(input.Filter) || e.DispatchJobsIdFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.DispatchJobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.DispatchJobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || jobnumberlist.Count != 0 && jobnumberlist.Contains(e.DispatchJobsIdFK.LeadId))
                 .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.DispatchJobsIdFK.LeadFK.LeadStatusId))
                 .WhereIf(input.LeadApplicationStatusFilter != null && input.LeadApplicationStatusFilter.Count() > 0, e => input.LeadApplicationStatusFilter.Contains((int)e.DispatchJobsIdFK.ApplicationStatusId))
                 .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.DiscomId))
                 .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.CircleId))
                 .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.DivisionId))
                 .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.SubDivisionId))
                 .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains(e.DispatchJobsIdFK.LeadFK.SolarTypeId))
                 .WhereIf(input.EmployeeIdFilter != null && input.EmployeeIdFilter.Count() > 0, e => input.EmployeeIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.ChanelPartnerID))
                 .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "billdate", e => sspaymentlist.Contains(e.DispatchJobId) || stcpaymentlist.Contains(e.DispatchJobId))
                 .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "dispatchdate", e => e.DispatchDate != (DateTime?)null && e.DispatchDate != DateTime.MinValue) //Completed                              
                 .Where(e => e.DispatchJobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit && e.DispatchJobsIdFK.LeadFK.LeadStatusId == 6 && e.DispatchDate == DateTime.MinValue);//6 for Ready to Dispatch lead status

            var pagedAndFilteredJob = filtereddispatch.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var dispatchTracker = from o in pagedAndFilteredJob

                                      //dispatch tracker are show the Data for SS bill generated
                                  join o3 in _ssPaymentDetailsRepository.GetAll() on o.Id equals o3.SSPaymentId into j3
                                  from s3 in j3.DefaultIfEmpty()

                                      //join o4 in _dispatchMaterialDetailsRepository.GetAll() on o.PaymentJobId equals o4.DispatchJobId into j4
                                      //from s4 in j4.DefaultIfEmpty()

                                      //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                                      //from s4 in j4.DefaultIfEmpty()

                                  select new GetDispatchItemForViewDto()
                                  {
                                      dispatchItem = new DispatchDto
                                      {
                                          Id = o.Id,
                                          leaddata = ObjectMapper.Map<LeadsDto>(o.DispatchJobsIdFK.LeadFK),
                                          JobId = o.DispatchJobsIdFK.Id,
                                          ProjectNumber = o.DispatchJobsIdFK.JobNumber,
                                          CustomerName = o.DispatchJobsIdFK.LeadFK.CustomerName,
                                          Address = (o.DispatchJobsIdFK.LeadFK.AddressLine1 == null ? "" : o.DispatchJobsIdFK.LeadFK.AddressLine1 + ", ") + (o.DispatchJobsIdFK.LeadFK.AddressLine2 == null ? "" : o.DispatchJobsIdFK.LeadFK.AddressLine2 + ", ") + (o.DispatchJobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.DispatchJobsIdFK.LeadFK.Pincode == null ? "" : o.DispatchJobsIdFK.LeadFK.Pincode),
                                          Mobile = o.DispatchJobsIdFK.LeadFK.MobileNumber,
                                          Email = o.DispatchJobsIdFK.LeadFK.EmailId,
                                          dispatchJobId = o.DispatchJobsIdFK.Id,
                                          CpMobile = _userRepository.GetAll().Where(e => e.Id == o.DispatchJobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.PhoneNumber).FirstOrDefault(),
                                          LeadStatusName = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                          FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.DispatchJobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                          FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.DispatchJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                          Notes = o.DispatchJobsIdFK.LeadFK.Notes,
                                          LeadStatusColorClass = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                          LeadStatusIconClass = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                          Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.DispatchJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                          IsActive = o.IsActive,
                                          DispatchDate = (DateTime?)_dispatchMaterialDetailsRepository.GetAll().Where(x => x.DispatchJobId == o.DispatchJobId).FirstOrDefault().DispatchDate == DateTime.MinValue ? null : (DateTime?)_dispatchMaterialDetailsRepository.GetAll().Where(x => x.DispatchJobId == o.DispatchJobId).FirstOrDefault().DispatchDate,
                                          isExtraMaterialApproved = o.IsExtraMaterialApproved,
                                      },
                                      dispatchTrackerSummaryCount = dispatchTrackerSummaryCount(paymentdetails.ToList())
                                  };

            var totalCount = await filtereddispatch.CountAsync();

            return new PagedResultDto<GetDispatchItemForViewDto>(totalCount, await dispatchTracker.ToListAsync());
        }

        public DispatchTrackerSummaryCount dispatchTrackerSummaryCount(List<PaymentDetailsJob> filteredLeads)
        {
            var output = new DispatchTrackerSummaryCount();
            if (filteredLeads.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);

                output.Total = Convert.ToString(filteredLeads.Sum(x => x.NetAmount));
                output.TotalOwingAmount = Convert.ToString(filteredLeads.Sum(x => x.BalanceOwing));
                output.TotalPanels = Convert.ToString(panelCount);
                output.TotalKilowatt = Convert.ToString(filteredLeads.Sum(x => x.JobsIdFK.PvCapacityKw));
            }
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Dispatch_ExportToExcel)]
        public async Task<FileDto> GetDispatchTrackerToExcel(GetAllDispatchItemsInput input)
        {
            var SDate = _timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId);
            var EDate = _timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId);

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);


            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);
            var jobnumberlist = new List<int>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            var FollowupList = new List<int>();
            var NextFollowupList = new List<int>();
            var Assign = new List<int>();

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var paymentdetails = await _paymentDetailjobRepository.GetAll().Include(x => x.JobsIdFK).ToListAsync();
            var dispatchdata = await _dispatchMaterialDetailsRepository.GetAll().ToListAsync();
            var filtereddispatch = _dispatchMaterialDetailsRepository.GetAll().Include(e => e.DispatchJobsIdFK).Include(e => e.DispatchJobsIdFK.ApplicationStatusIdFK).Include(e => e.DispatchJobsIdFK.LeadFK).Include(e => e.DispatchJobsIdFK.LeadFK.SolarTypeIdFk)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DispatchJobsIdFK.JobNumber.Contains(input.Filter)
                                || e.DispatchJobsIdFK.LeadFK.CustomerName.Contains(input.Filter) || e.DispatchJobsIdFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.DispatchJobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.DispatchJobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || jobnumberlist.Count != 0 && jobnumberlist.Contains(e.DispatchJobsIdFK.LeadId))
                 .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.DispatchJobsIdFK.LeadFK.LeadStatusId))
                 .WhereIf(input.LeadApplicationStatusFilter != null && input.LeadApplicationStatusFilter.Count() > 0, e => input.LeadApplicationStatusFilter.Contains((int)e.DispatchJobsIdFK.ApplicationStatusId))
                 .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.DiscomId))
                 .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.CircleId))
                 .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.DivisionId))
                 .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.SubDivisionId))
                 .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains(e.DispatchJobsIdFK.LeadFK.SolarTypeId))
                 .WhereIf(input.EmployeeIdFilter != null && input.EmployeeIdFilter.Count() > 0, e => input.EmployeeIdFilter.Contains((int)e.DispatchJobsIdFK.LeadFK.ChanelPartnerID))
                 .Where(e => e.DispatchJobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit && e.DispatchJobsIdFK.LeadFK.LeadStatusId == 6 && e.DispatchDate == DateTime.MinValue);//6 for Ready to Dispatch lead status

            var pagedAndFilteredJob = filtereddispatch.OrderBy(input.Sorting ?? "id desc").PageBy(input);
            var dispatchTracker = from o in pagedAndFilteredJob

                                      //dispatch tracker are show the Data for SS bill generated
                                  join o3 in _ssPaymentDetailsRepository.GetAll() on o.Id equals o3.SSPaymentId into j3
                                  from s3 in j3.DefaultIfEmpty()

                                  select new GetDispatchItemForViewDto()
                                  {
                                      dispatchItem = new DispatchDto
                                      {
                                          Id = o.Id,
                                          leaddata = ObjectMapper.Map<LeadsDto>(o.DispatchJobsIdFK.LeadFK),
                                          JobId = o.DispatchJobsIdFK.Id,
                                          ProjectNumber = o.DispatchJobsIdFK.JobNumber,
                                          CustomerName = o.DispatchJobsIdFK.LeadFK.CustomerName,
                                          Address = (o.DispatchJobsIdFK.LeadFK.AddressLine1 == null ? "" : o.DispatchJobsIdFK.LeadFK.AddressLine1 + ", ") + (o.DispatchJobsIdFK.LeadFK.AddressLine2 == null ? "" : o.DispatchJobsIdFK.LeadFK.AddressLine2 + ", ") + (o.DispatchJobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.DispatchJobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.DispatchJobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.DispatchJobsIdFK.LeadFK.Pincode == null ? "" : o.DispatchJobsIdFK.LeadFK.Pincode),
                                          Mobile = o.DispatchJobsIdFK.LeadFK.MobileNumber,
                                          Email = o.DispatchJobsIdFK.LeadFK.EmailId,
                                          dispatchJobId = o.DispatchJobsIdFK.Id,
                                          CpMobile = _userRepository.GetAll().Where(e => e.Id == o.DispatchJobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.PhoneNumber).FirstOrDefault(),
                                          LeadStatusName = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                          FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.DispatchJobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                          FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.DispatchJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                          Notes = o.DispatchJobsIdFK.LeadFK.Notes,
                                          LeadStatusColorClass = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                          LeadStatusIconClass = o.DispatchJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                          Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.DispatchJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                          IsActive = o.IsActive,
                                          DispatchDate = (DateTime?)_dispatchMaterialDetailsRepository.GetAll().Where(x => x.DispatchJobId == o.DispatchJobId).FirstOrDefault().DispatchDate == DateTime.MinValue ? null : (DateTime?)_dispatchMaterialDetailsRepository.GetAll().Where(x => x.DispatchJobId == o.DispatchJobId).FirstOrDefault().DispatchDate, //s4.DispatchDate, 

                                      },
                                      dispatchTrackerSummaryCount = dispatchTrackerSummaryCount(paymentdetails.ToList())
                                  };
            var dispatchtrackerListDtos = await dispatchTracker.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _DispatchTrackerExcelExporter.ExportToFile(dispatchtrackerListDtos);
            }
            else
            {
                return _DispatchTrackerExcelExporter.ExportToFile(dispatchtrackerListDtos);
            }

        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Application_QuickView)]
        public async Task<DispatchTrackerQuickViewDto> GetDispatchQuickView(EntityDto input)
        {
            try
            {
                //var leads = _leadRepository.FirstOrDefault(input.Id);
                var dispatchView = _dispatchMaterialDetailsRepository.GetAll().Include(e => e.DispatchJobsIdFK.LeadFK).
                   Include(e => e.DispatchJobsIdFK.LeadFK.DivisionIdFk).
                   Include(e => e.DispatchJobsIdFK.LeadFK.SubDivisionIdFk).
                   Include(e => e.DispatchJobsIdFK.LeadFK.DiscomIdFk).
                   Include(e => e.DispatchJobsIdFK.LeadFK.HeightStructureIdFk).
                   Where(e => e.Id == input.Id).FirstOrDefault();
                var output = new DispatchTrackerQuickViewDto();

                if (dispatchView != null)
                {
                    var cpDetails = await _userRepository.GetAll().Where(e => e.Id == dispatchView.DispatchJobsIdFK.LeadFK.ChanelPartnerID).FirstOrDefaultAsync();
                    var jobProductItem = await _productItemdataepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.JobId == dispatchView.DispatchJobId).ToListAsync();
                    var dispatchDetails = await _dispatchMaterialDetailsRepository.GetAll().Include(x => x.DispatchHeightStructureIdFK).Where(e => e.Id == dispatchView.Id).FirstOrDefaultAsync();
                    var dispatchExtraMaterial = await _dispatchExtraMaterialDetailsRepository.GetAll().Include(x => x.StockItemIdFK).Include(x => x.UserManagerIdFK).Include(x => x.MaterialApproveByIdFK).Where(x => x.DispatchId == dispatchDetails.Id).ToListAsync();
                    output = new DispatchTrackerQuickViewDto
                    {
                        ProjectNumber = dispatchView.DispatchJobsIdFK.JobNumber,
                        CustomerName = dispatchView.DispatchJobsIdFK.LeadFK.CustomerName,
                        ConsumerNumber = dispatchView.DispatchJobsIdFK.LeadFK.ConsumerNumber,
                        ChanelPartnerName = cpDetails.FullName,
                        ChanelPartnerMobileNo = cpDetails.PhoneNumber,
                        DiscomName = dispatchView.DispatchJobsIdFK.LeadFK.DiscomIdFk.Name,
                        SystemSize = dispatchView.DispatchJobsIdFK.PvCapacityKw, //pendding
                        MobileNumber = dispatchView.DispatchJobsIdFK.LeadFK.MobileNumber,
                        NoOfPanel = jobProductItem.Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).FirstOrDefault().Quantity,
                        PanelBrandName = jobProductItem.Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).FirstOrDefault().ProductItemFk.Name,
                        NoOfInverter = jobProductItem.Where(x => x.ProductItemFk.StockCategoryFk.Id == 2).FirstOrDefault().Quantity,
                        InverterBrand = jobProductItem.Where(x => x.ProductItemFk.StockCategoryFk.Id == 2).FirstOrDefault().ProductItemFk.Name,
                        StructureHeight = dispatchView.DispatchJobsIdFK.LeadFK.HeightStructureIdFk.Name,
                        StructureAmount = dispatchView.DispatchJobsIdFK.LeadFK.StrctureAmmount,
                        EastimatePayementRefferanceNo = dispatchView.DispatchJobsIdFK.EastimatePaymentRefNumber,
                        EastimatePaymentStatus = dispatchView.DispatchJobsIdFK.EastimatePaidStatus,
                        EastimateQuoteNo = dispatchView.DispatchJobsIdFK.EastimateQuoteNumber,
                        EastimateQuoteAmount = dispatchView.DispatchJobsIdFK.EastimateQuoteAmount,
                        EastimateDueDate = dispatchView.DispatchJobsIdFK.EastimateDueDate,
                        EastimatePayResponse = dispatchView.DispatchJobsIdFK.EastimatePaymentResponse,
                        DispatchDate = (DateTime?)dispatchDetails.DispatchDate == DateTime.MinValue ? null : (DateTime?)dispatchDetails.DispatchDate,
                        HeightofStructure = dispatchDetails.DispatchHeightStructureIdFK.Name,
                        DispatchNote = dispatchDetails.DispatchNotes,
                        DispatchReturnDate = dispatchDetails.DispatchReturnDate,
                        DispatchReturnNote = dispatchDetails.DispatchReturnNotes,
                        ExtraMaterialNotes = dispatchDetails.ExtraMaterialNotes,
                        IsExtraMaterialApproved = dispatchDetails.IsExtraMaterialApproved,
                        VerifyBy = dispatchDetails.LastModifierUserId == null ? null : _userRepository.GetAll().Where(x => x.Id == dispatchDetails.LastModifierUserId).FirstOrDefault().Name,
                        ApprovadBy = dispatchDetails.LastModificationTime == null ? null : _userRepository.GetAll().Where(x => x.Id == dispatchDetails.LastModifierUserId).FirstOrDefault().Name,
                        ApprovadDate = dispatchDetails.IsExtraMaterialApproved == true ? dispatchDetails.LastModificationTime : null,
                        ExtraMaterialApprovedNotes = dispatchDetails.ExtraMaterialApprovedNotes,
                        ExtraMaterialList = dispatchExtraMaterial.Count == 0 ? null : ObjectMapper.Map<List<DispatchExtraMaterialDto>>(dispatchExtraMaterial
                        .Select(doc => new DispatchExtraMaterialDto
                        {
                            StockItemName = doc.StockItemIdFK.Name,
                            Quantity = doc.Quantity,
                            ManagerName = doc.UserManagerIdFK.Name,
                        })),
                    };
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<GetDispatchForEditOutput> GetDispatchForEdit(EntityDto input)
        {
            var jobDispatch = await _dispatchMaterialDetailsRepository.FirstOrDefaultAsync(x => x.DispatchJobId == input.Id);

            var output = new GetDispatchForEditOutput { Dispatch = ObjectMapper.Map<CreateOrEditDispatchDto>(jobDispatch) };

            return output;
        }

        public async Task Verifypicklist(CreateOrEditDispatchDto input)
        {
            var jobDispatch = await _dispatchMaterialDetailsRepository.FirstOrDefaultAsync(x => x.Id == input.Id);
            if (jobDispatch != null)
            {
                jobDispatch.IsActive = input.IsActive;
                var DispatchItem = await _dispatchMaterialDetailsRepository.UpdateAsync(jobDispatch);
                if (DispatchItem != null)
                {
                    //Add Activity Log Installer
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 15;
                    leadactivity.SectionId = 63;
                    leadactivity.LeadActionNote = "Dispatch Extra Material Verify";
                    leadactivity.LeadId = input.leadid;
                    leadactivity.OrganizationUnitId = input.OrgID;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }
        public async Task CreateOrEdit(CreateOrEditDispatchDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Dispatch)]
        protected virtual async Task Create(CreateOrEditDispatchDto input)
        {
            var jobDispatch = ObjectMapper.Map<DispatchMaterialDetails>(input);
            if (jobDispatch != null && jobDispatch.DispatchJobId != 0)
            {
                jobDispatch.DispatchJobId = input.DispatchJobId;
                jobDispatch.DispatchStockItemId = input.DispatchStockItemId;
                jobDispatch.DispatchHeightStructureId = input.DispatchHeightStructureId;
                jobDispatch.DispatchTypeId = input.DispatchTypeId;
                jobDispatch.DispatchDate = input.DispatchDate;
                jobDispatch.DispatchNotes = input.DispatchNotes;
                jobDispatch.DispatchReturnDate = input.DispatchReturnDate;
                jobDispatch.DispatchReturnNotes = input.DispatchReturnNotes;
                jobDispatch.ExtraMaterialNotes = input.ExtraMaterialNotes;
                jobDispatch.IsExtraMaterialApproved = input.IsExtraMaterialApproved;
                jobDispatch.ExtraMaterialApprovedNotes = input.ExtraMaterialApprovedNotes;
                var DispatchItem = await _dispatchMaterialDetailsRepository.InsertAndGetIdAsync(jobDispatch);

                if (DispatchItem != null)
                {
                    if (input.dispathproductlist.Count > 0)
                    {
                        foreach (var item in input.dispathproductlist)
                        {
                            item.DispatchId = DispatchItem;
                            var Dispatchproduct = ObjectMapper.Map<DispatchProductItem>(item);
                            await _dispatchProductItemRepository.InsertAndGetIdAsync(Dispatchproduct);
                        }

                    }
                    //picklist datasave
                    //remove picklist data                        
                    RemovePicklist(jobDispatch.Id);

                    //get stock item
                    var stockID = await _stockItemRepository.FirstOrDefaultAsync(x => x.CategoryId == 1); //1 for Panel

                    //get panel qty
                    if (stockID != null)
                    {
                        var panelItem = await _productItemdataepository.GetAll().Where(x => x.JobId == input.DispatchJobId && x.ProductItemId.ToString().Contains(stockID.Id.ToString())).FirstOrDefaultAsync();

                        //get picklist data
                        var materialItem = await _billofMaterialRepository.GetAll().Where(x => x.HeightStructureId == input.DispatchHeightStructureId).ToListAsync(); // && (x.Panel_S == panelItem.Quantity || x.Panel_E == panelItem.Quantity)).ToListAsync();

                        if (materialItem != null)
                        {
                            List<PickListData> picklist = new List<PickListData>();
                            foreach (var item in materialItem)
                            {
                                PickListData data = new PickListData();
                                data.BillOfMaterialId = item.Id;
                                data.PickListDisptchId = jobDispatch.Id;
                                data.PickListQty = item.Quantity;
                                picklist.Add(data);
                            }
                            await _dbContextProvider.GetDbContext().pickListData.AddRangeAsync(picklist);
                            await _dbContextProvider.GetDbContext().SaveChangesAsync();
                        }
                    }

                    //Add Activity Log Installer
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 15;
                    leadactivity.SectionId = 62;
                    leadactivity.LeadActionNote = "Job Dispatch Created";
                    leadactivity.LeadId = input.leadid;
                    leadactivity.OrganizationUnitId = input.OrgID;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund_Edit)]
        protected virtual async Task Update(CreateOrEditDispatchDto input)
        {

            var jobDispatch = _dispatchMaterialDetailsRepository.GetAll().Include(x => x.DispatchJobsIdFK).Where(x => x.DispatchJobId == input.DispatchJobId && x.Id == input.Id).FirstOrDefault();
            if (jobDispatch != null)
            {
                jobDispatch.DispatchJobId = input.DispatchJobId;
                jobDispatch.DispatchStockItemId = input.DispatchStockItemId;
                jobDispatch.DispatchHeightStructureId = input.DispatchHeightStructureId;
                jobDispatch.DispatchTypeId = input.DispatchTypeId;
                jobDispatch.DispatchDate = input.DispatchDate;
                jobDispatch.DispatchNotes = input.DispatchNotes;
                jobDispatch.IsActive = input.IsActive;
                jobDispatch.ExtraMaterialNotes = input.ExtraMaterialNotes;
                jobDispatch.IsExtraMaterialApproved = input.IsExtraMaterialApproved;
                jobDispatch.ExtraMaterialApprovedNotes = input.ExtraMaterialApprovedNotes;
                int SectionId = 22;
                if (jobDispatch.Id != 0)
                {
                    #region Activity Log
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 11;
                    //leadactivity.LeadAcitivityID = 0;
                    leadactivity.SectionId = SectionId;
                    leadactivity.LeadActionNote = "Dispatch Job Modified";
                    leadactivity.LeadId = input.leadid;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                    var List = new List<LeadtrackerHistory>();
                    //add the dispatch Product Item
                    if (input.dispathproductlist.Count > 0)
                    {
                        var ProductItemList = _stockItemRepository.GetAll();
                        var ProductType = _stockCategoryRepository.GetAll();

                        var IdList = input.dispathproductlist.Where(x => x.ProductItemId != 0).Select(e => e.Id).ToList();
                        var existingData = _dispatchProductItemRepository.GetAll().AsNoTracking().Where(x => x.DispatchId == input.Id).ToList();

                        if (existingData != null)
                        {
                            foreach (var item in existingData)
                            {
                                if (IdList != null)
                                {
                                    bool containsItem = IdList.Any(x => x == item.Id);
                                    if (containsItem == false)
                                    {
                                        LeadtrackerHistory jobhistory1 = new LeadtrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory1.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory1.FieldName = "Product Name";
                                        jobhistory1.PrevValue = ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.Name).FirstOrDefault();
                                        jobhistory1.CurValue = "Deleted";
                                        jobhistory1.Action = "Products Delete";
                                        jobhistory1.LastModificationTime = DateTime.Now;
                                        jobhistory1.LeadId = input.leadid;
                                        jobhistory1.LeadActionId = leadactid;
                                        List.Add(jobhistory1);

                                        LeadtrackerHistory jobhistory2 = new LeadtrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory2.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory2.FieldName = "Product Model";
                                        jobhistory2.PrevValue = ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.Model).FirstOrDefault();
                                        jobhistory2.CurValue = "Deleted";
                                        jobhistory2.Action = "Products Delete";
                                        jobhistory2.LastModificationTime = DateTime.Now;
                                        jobhistory2.LeadId = input.leadid;
                                        jobhistory2.LeadActionId = leadactid;
                                        List.Add(jobhistory2);

                                        LeadtrackerHistory jobhistory3 = new LeadtrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory3.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory3.FieldName = "Product Quantity";
                                        jobhistory3.PrevValue = item.Quantity.ToString();
                                        jobhistory3.CurValue = "Deleted";
                                        jobhistory3.Action = "Products Delete";
                                        jobhistory3.LastModificationTime = DateTime.Now;
                                        jobhistory3.LeadId = input.leadid;
                                        jobhistory3.LeadActionId = leadactid;
                                        List.Add(jobhistory3);

                                        LeadtrackerHistory jobhistory4 = new LeadtrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory2.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory4.FieldName = "Product Type";
                                        jobhistory4.PrevValue = ProductType.Where(x => x.Id == (ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.CategoryId).FirstOrDefault())).Select(x => x.Name).FirstOrDefault();
                                        jobhistory4.CurValue = "Deleted";
                                        jobhistory4.Action = "Products Delete";
                                        jobhistory4.LastModificationTime = DateTime.Now;
                                        jobhistory4.LeadId = input.leadid;
                                        jobhistory4.LeadActionId = leadactid;
                                        List.Add(jobhistory4);
                                        _dispatchProductItemRepository.Delete(x => x.Id == item.Id);
                                    }
                                }
                            }
                        }
                        foreach (var jobproduct in input.dispathproductlist)
                        {
                            if (jobproduct.ProductItemId != 0 && jobproduct.Quantity != 0)
                            {
                                if (jobproduct.Id != null && jobproduct.Id != 0)
                                {
                                    var existData = _dispatchProductItemRepository.GetAll().AsNoTracking().Where(x => x.Id == jobproduct.Id).FirstOrDefault();
                                    var olddetail = ObjectMapper.Map<DispatchProductItem>(jobproduct);
                                    olddetail.DispatchId = input.Id;

                                    _dispatchProductItemRepository.Update(olddetail);

                                    if (jobproduct.ProductItemId != null && jobproduct.ProductItemId != 0 && existData.ProductItemId != jobproduct.ProductItemId)
                                    {
                                        var existProductName = ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.Name).FirstOrDefault();
                                        var currentProductName = ProductItemList.Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Name).FirstOrDefault();

                                        var existModel = ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.Model).FirstOrDefault();
                                        var currentModel = ProductItemList.Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Model).FirstOrDefault();

                                        var existProductType = ProductType.Where(x => x.Id == (ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.CategoryId).FirstOrDefault())).Select(x => x.Name).FirstOrDefault();
                                        var currentProductType = ProductType.Where(x => x.Id == jobproduct.ProductTypeId).Select(x => x.Name).FirstOrDefault();

                                        if (existProductName != currentProductName)
                                        {
                                            LeadtrackerHistory jobhistory1 = new LeadtrackerHistory();
                                            if (AbpSession.TenantId != null)
                                            {
                                                jobhistory1.TenantId = (int)AbpSession.TenantId;
                                            }
                                            jobhistory1.FieldName = "Product Name";
                                            jobhistory1.PrevValue = existProductName;
                                            jobhistory1.CurValue = currentProductName;
                                            jobhistory1.Action = "Products Edit";
                                            jobhistory1.LastModificationTime = DateTime.Now;
                                            jobhistory1.LeadId = input.leadid;
                                            jobhistory1.LeadActionId = leadactid;
                                            List.Add(jobhistory1);
                                        }

                                        if (existModel != currentModel)
                                        {
                                            LeadtrackerHistory jobhistory2 = new LeadtrackerHistory();
                                            if (AbpSession.TenantId != null)
                                            {
                                                jobhistory2.TenantId = (int)AbpSession.TenantId;
                                            }
                                            jobhistory2.FieldName = "Product Model";
                                            jobhistory2.PrevValue = existModel;
                                            jobhistory2.CurValue = currentModel;
                                            jobhistory2.Action = "Products Edit";
                                            jobhistory2.LastModificationTime = DateTime.Now;
                                            jobhistory2.LeadId = input.leadid;
                                            jobhistory2.LeadActionId = leadactid;
                                            List.Add(jobhistory2);
                                        }

                                        if (existProductType != currentProductType)
                                        {
                                            LeadtrackerHistory jobhistory3 = new LeadtrackerHistory();
                                            if (AbpSession.TenantId != null)
                                            {
                                                jobhistory3.TenantId = (int)AbpSession.TenantId;
                                            }
                                            jobhistory3.FieldName = "Product Type";
                                            jobhistory3.PrevValue = existProductType;
                                            jobhistory3.CurValue = currentProductType;
                                            jobhistory3.Action = "Products Edit";
                                            jobhistory3.LastModificationTime = DateTime.Now;
                                            jobhistory3.LeadId = input.leadid;
                                            jobhistory3.LeadActionId = leadactid;
                                            List.Add(jobhistory3);
                                        }
                                    }

                                    if (jobproduct.Quantity != null && existData.Quantity != jobproduct.Quantity)
                                    {
                                        LeadtrackerHistory jobhistory = new LeadtrackerHistory();
                                        if (AbpSession.TenantId != null)
                                        {
                                            jobhistory.TenantId = (int)AbpSession.TenantId;
                                        }
                                        jobhistory.FieldName = "Product Item Quantity";
                                        jobhistory.PrevValue = existData.Quantity.ToString();
                                        jobhistory.CurValue = jobproduct.Quantity.ToString();
                                        jobhistory.Action = "Products Edit";
                                        jobhistory.LastModificationTime = DateTime.Now;
                                        jobhistory.LeadId = input.leadid;
                                        jobhistory.LeadActionId = leadactid;
                                        List.Add(jobhistory);
                                    }
                                }
                                else
                                {
                                    var olddetail = ObjectMapper.Map<DispatchProductItem>(jobproduct);
                                    olddetail.DispatchId = input.Id;
                                    _dispatchProductItemRepository.Insert(olddetail);
                                }
                            }
                        }
                    }
                    //remove picklist data                        
                    RemovePicklist(jobDispatch.Id);

                    //Get Stock Item
                    var stockID = await _stockItemRepository.FirstOrDefaultAsync(x => x.CategoryId == 2); //2 for Panel

                    var panelItem = await _productItemdataepository.GetAll().Where(x => x.JobId == input.DispatchJobId && x.ProductItemId.ToString().Contains(stockID.Id.ToString())).FirstOrDefaultAsync();

                    //add the picklist item
                    if (panelItem != null)
                    {
                        var materialItem = await _billofMaterialRepository.GetAll().Where(x => x.HeightStructureId == input.DispatchHeightStructureId).ToListAsync();// && (x.Panel_S == panelItem.Quantity || x.Panel_E == panelItem.Quantity)).ToListAsync();

                        if (materialItem != null)
                        {
                            List<PickListData> picklist = new List<PickListData>();
                            foreach (var item in materialItem)
                            {
                                PickListData data = new PickListData();
                                data.BillOfMaterialId = item.Id;
                                data.PickListDisptchId = jobDispatch.Id;
                                data.PickListQty = item.Quantity;
                                picklist.Add(data);
                            }


                            await _dbContextProvider.GetDbContext().pickListData.AddRangeAsync(picklist);
                            await _dbContextProvider.GetDbContext().SaveChangesAsync();
                        }
                    }
                    #endregion
                }
            }
        }
        public async Task RemovePicklist(int dispatchID)
        {
            AsyncHelper.RunSync(async () => await _picklistdataRepository.DeleteAsync(x => x.PickListDisptchId == dispatchID));
        }

        public async Task DeleteDispatch(EntityDto input, int leadId, int OrgId)
        {
            if (input.Id != 0)
            {
                await RemovePicklist(input.Id);

                await _dispatchMaterialDetailsRepository.DeleteAsync(input.Id);

                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 15;
                leadactivity.SectionId = 62;
                leadactivity.LeadActionNote = "Job Dispatch Deleted";
                leadactivity.LeadId = leadId;
                leadactivity.OrganizationUnitId = OrgId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }

        }
        //Extra Material
        public async Task<List<DispatchExtraMaterialDto>> GetAllDispatchExtraMaterial(int dispatchId)
        {
            var ExtraItemList = await _dispatchExtraMaterialDetailsRepository.GetAll().Include(x => x.DispatchMaterialDetailsIdFK).Where(x => x.DispatchId == dispatchId).ToListAsync();
            var output = (from dt in ExtraItemList
                          select new DispatchExtraMaterialDto
                          {
                              Id = dt.Id,
                              DispatchId = dt.DispatchId,
                              DispatchStockItemId = dt.DispatchStockItemId,
                              Quantity = dt.Quantity,
                              UserManagerId = dt.UserManagerId,
                              MaterilApproveById = dt.MaterilApproveById,
                              ExtraMaterialNotes = dt.DispatchMaterialDetailsIdFK.ExtraMaterialNotes,
                              IsExtraMaterialApproved = dt.DispatchMaterialDetailsIdFK.IsExtraMaterialApproved,
                              ExtraMaterialApprovedNotes = dt.DispatchMaterialDetailsIdFK.ExtraMaterialApprovedNotes
                          }).ToList();
            return output;
        }
        public async Task<GetDispatchExtraMaterialForEditOutput> GetDispatchExtraMaterialForEdit(EntityDto input)
        {
            var extraMaterial = await _dispatchExtraMaterialDetailsRepository.GetAll().Include(x => x.DispatchMaterialDetailsIdFK).Where(x => x.DispatchId == input.Id).FirstOrDefaultAsync();
            var output = new GetDispatchExtraMaterialForEditOutput { ExtraMaterial = ObjectMapper.Map<CreateOrEditDispatchExtraMaterialDto>(extraMaterial) };

            return output;
        }

        public async Task CreateOrEditExtraMaterial(List<DispatchExtraMaterialDto> input)
        {
            int SectionId = 62;
            if (input != null && input.Count > 0)
            {
                List<LeadtrackerHistory> List = new List<LeadtrackerHistory>();
                var stoclItem = await _stockItemRepository.GetAllListAsync();
                var IdList = input.Select(e => e.Id).ToList();
                var dispatchData = _dispatchMaterialDetailsRepository.FirstOrDefaultAsync(input[0].DispatchId).Result;
                if (dispatchData != null)
                {
                    dispatchData.ExtraMaterialNotes = input[0].ExtraMaterialNotes;
                }
                if (dispatchData != null && input[0].IsExtraMaterialApproved)
                {
                    dispatchData.IsExtraMaterialApproved = input[0].IsExtraMaterialApproved;
                    dispatchData.ExtraMaterialApprovedNotes = input[0].ExtraMaterialApprovedNotes;
                }
                await _dispatchMaterialDetailsRepository.UpdateAsync(dispatchData);

                var existingData = _dispatchExtraMaterialDetailsRepository.GetAll().Where(x => x.DispatchId == input[0].DispatchId).ToList();
                if (existingData != null)
                {
                    if (existingData != null)
                    {
                        foreach (var item in existingData)
                        {
                            if (IdList != null)
                            {
                                bool containsItem = IdList.Any(x => x == item.Id);
                                if (containsItem == false)
                                {
                                    #region Activity Log
                                    //Add Activity Log
                                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                                    leadactivity.LeadActionId = 11;
                                    //leadactivity.LeadAcitivityID = 0;
                                    leadactivity.SectionId = SectionId;
                                    leadactivity.LeadId = input[0].leadid;
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadactivity.TenantId = (int)AbpSession.TenantId;
                                    }
                                    leadactivity.LeadActionNote = "Dispatch Extra Material Deleted";
                                    var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);

                                    #endregion

                                    LeadtrackerHistory leadHistory1 = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadHistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    leadHistory1.FieldName = "Stock Item Name";
                                    leadHistory1.PrevValue = stoclItem.Where(x => x.Id == item.DispatchStockItemId).Select(x => x.Name).FirstOrDefault();
                                    leadHistory1.CurValue = "Deleted";
                                    leadHistory1.Action = "Stock Item Delete";
                                    leadHistory1.LastModificationTime = DateTime.Now;
                                    leadHistory1.LeadId = input[0].leadid;
                                    leadHistory1.LeadActionId = leadactid;
                                    List.Add(leadHistory1);

                                    LeadtrackerHistory leadHistory = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadHistory.TenantId = (int)AbpSession.TenantId;
                                    }
                                    leadHistory.FieldName = "Stock Item Quantity";
                                    leadHistory.PrevValue = item.Quantity.ToString();
                                    leadHistory.CurValue = "Deleted";
                                    leadHistory.Action = "Stock Item Delete";
                                    leadHistory.LastModificationTime = DateTime.Now;
                                    leadHistory.LeadId = input[0].leadid;
                                    leadHistory.LeadActionId = leadactid;
                                    List.Add(leadHistory);
                                    _dispatchExtraMaterialDetailsRepository.Delete(x => x.Id == item.Id);
                                }
                            }
                        }
                    }
                    var existData2 = await _dispatchExtraMaterialDetailsRepository.GetAll().Include(x => x.UserManagerIdFK).Where(x => x.DispatchId == input[0].DispatchId).ToListAsync();
                    foreach (var ExtraItem in input)
                    {
                        if (ExtraItem.Id != 0)
                        {
                            #region Activity Log
                            //Add Activity Log
                            LeadActivityLogs leadactivity = new LeadActivityLogs();
                            leadactivity.LeadActionId = 11;
                            //leadactivity.LeadAcitivityID = 0;
                            leadactivity.SectionId = SectionId;
                            leadactivity.LeadId = input[0].leadid;
                            if (AbpSession.TenantId != null)
                            {
                                leadactivity.TenantId = (int)AbpSession.TenantId;
                            }
                            leadactivity.LeadActionNote = "Dispatch Extra Material Modified";
                            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                            #endregion
                            var existData = existData2.Where(x => x.Id == ExtraItem.Id).FirstOrDefault();
                            existData.DispatchStockItemId = ExtraItem.Id;
                            existData.Quantity = (decimal)ExtraItem.Quantity;
                            existData.UserManagerId = ExtraItem.UserManagerId;
                            existData.MaterilApproveById = ExtraItem.MaterilApproveById;
                            var olddetail = ObjectMapper.Map<DispatchExtraMaterials>(existData);
                            _dispatchExtraMaterialDetailsRepository.Update(olddetail);
                            if (ExtraItem.DispatchId != 0 && existData.DispatchId == ExtraItem.DispatchId)
                            {
                                var existMaterial = stoclItem.Where(x => x.Id == existData.DispatchStockItemId).Select(x => x.Name).FirstOrDefault();
                                var currentMaterial = stoclItem.Where(x => x.Id == ExtraItem.DispatchStockItemId).Select(x => x.Name).FirstOrDefault();
                                if (existData.DispatchStockItemId != ExtraItem.DispatchStockItemId)
                                {
                                    LeadtrackerHistory leadHistory1 = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadHistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    leadHistory1.FieldName = "Stock Item Name";
                                    leadHistory1.PrevValue = existMaterial;
                                    leadHistory1.CurValue = currentMaterial;
                                    leadHistory1.Action = "Extra Material Edit";
                                    leadHistory1.LastModificationTime = DateTime.Now;
                                    leadHistory1.LeadId = input[0].leadid;
                                    leadHistory1.LeadActionId = leadactid;
                                    List.Add(leadHistory1);
                                }
                                if (existData.Quantity != ExtraItem.Quantity)
                                {
                                    LeadtrackerHistory leadHistory1 = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadHistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    leadHistory1.FieldName = "Quantity";
                                    leadHistory1.PrevValue = Convert.ToString(existData.Quantity);
                                    leadHistory1.CurValue = Convert.ToString(ExtraItem.Quantity);
                                    leadHistory1.Action = "Extra Material Edit";
                                    leadHistory1.LastModificationTime = DateTime.Now;
                                    leadHistory1.LeadId = input[0].leadid;
                                    leadHistory1.LeadActionId = leadactid;
                                    List.Add(leadHistory1);
                                }
                                if (existData.UserManagerId != ExtraItem.UserManagerId)
                                {
                                    var managerName = await _userRepository.FirstOrDefaultAsync((long)ExtraItem.UserManagerId);
                                    LeadtrackerHistory leadHistory1 = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadHistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    leadHistory1.FieldName = "Manager Name";
                                    leadHistory1.PrevValue = existData.UserManagerIdFK.Name;
                                    leadHistory1.CurValue = managerName.Name;
                                    leadHistory1.Action = "Extra Material Edit";
                                    leadHistory1.LastModificationTime = DateTime.Now;
                                    leadHistory1.LeadId = input[0].leadid;
                                    leadHistory1.LeadActionId = leadactid;
                                    List.Add(leadHistory1);
                                }
                            }
                        }
                        else
                        {
                            if (ExtraItem.DispatchId > 0)
                            {
                                var olddetail = ObjectMapper.Map<DispatchExtraMaterials>(ExtraItem);

                                _dispatchExtraMaterialDetailsRepository.Insert(olddetail);
                            }
                        }
                    }
                }
            }
        }

        public async Task<GetDispatchMaterialTransferForEditOutput> GetDispatchMaterialTransferForEdit(EntityDto input)
        {
            var extraMaterial = await _dispatchStocktransferMaterialDetailsRepository.FirstOrDefaultAsync(x => x.DispatchId == input.Id);
            var output = new GetDispatchMaterialTransferForEditOutput { materialTransferDto = ObjectMapper.Map<CreateOrEditDispatchStockMaterialTransferDto>(extraMaterial) };

            return output;
        }
        public async Task CreateOrEditTransferMaterial(DispatchMaterialTransferDto input)
        {
            if (input.Id == 0)
            {
                await CreateMaterialTransfer(input);
            }
            else
            {
                await UpdateMaterialTransfer(input);
            }
        }
        protected virtual async Task CreateMaterialTransfer(DispatchMaterialTransferDto input)
        {
            var MaterialTransfer = ObjectMapper.Map<DispatchStockMaterialTransfer>(input);
            if (MaterialTransfer != null && MaterialTransfer.DispatchId != 0)
            {
                var TransferItem = await _dispatchStocktransferMaterialDetailsRepository.InsertAndGetIdAsync(MaterialTransfer);
                if (TransferItem != 0)
                {
                    //Add Activity Log Installer
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 15;
                    leadactivity.SectionId = 63;
                    leadactivity.LeadActionNote = "Dispatch Material Transfer Created";
                    leadactivity.LeadId = input.leadid;
                    leadactivity.OrganizationUnitId = input.OrgID;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

            }
        }

        protected virtual async Task UpdateMaterialTransfer(DispatchMaterialTransferDto input)
        {
            var transferMaterial = _dispatchStocktransferMaterialDetailsRepository.FirstOrDefault(input.DispatchId);
            if (transferMaterial != null && transferMaterial.DispatchId != 0)
            {
                if (transferMaterial != null)
                {
                    transferMaterial.JobNumber = input.JobNumber;
                    transferMaterial.Reason = input.Reason;

                }
                var TransferItem = await _dispatchStocktransferMaterialDetailsRepository.UpdateAsync(transferMaterial);
                if (TransferItem != null)
                {
                    //Add Activity Log Installer
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 15;
                    leadactivity.SectionId = 63;
                    leadactivity.LeadActionNote = "Dispatch Transfer Material Updated";
                    leadactivity.LeadId = input.leadid;
                    leadactivity.OrganizationUnitId = input.OrgID;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }

    }
}
