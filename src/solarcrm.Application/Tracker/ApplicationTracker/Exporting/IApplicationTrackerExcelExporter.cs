﻿using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.ApplicationTracker.Exporting
{
    public interface IApplicationTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetApplicationTrackerForViewDto> appTracker);
    }
}
