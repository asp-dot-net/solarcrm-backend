﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.ApplicationTracker.Exporting
{
    public class ApplicationTrackerExcelExporter : NpoiExcelExporterBase, IApplicationTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ApplicationTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetApplicationTrackerForViewDto> apptrack)
        {
            return CreateExcelPackage(
                "ApplicationTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ApplicationTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("LeadStatus"),
                        L("ApplicationStatus"),
                        L("CustomerName"),
                        L("Address"),
                        L("Mobile"),
                        L("ApplicationNo"),
                        L("FollowDiscription"),
                        L("FollowDate"),
                        L("QueryDescription"),
                        L("Comment")
                        );
                    AddObjects(
                        sheet, apptrack,
                        _ => _.appTracker.ProjectNumber,
                        _ => _.appTracker.LeadStatusName,
                        _ => _.appTracker.ApplicationStatusdata,
                        _ => _.appTracker.CustomerName,
                        _ => _.appTracker.Address,
                        _ => _.appTracker.Mobile,
                        _ => _.appTracker.ApplicationNumber,
                        _ => _.appTracker.FollowDiscription,
                        _ => _.appTracker.FollowDate,
                        _ => _.appTracker.QuesryDescription,
                        _ => _.appTracker.Comment
                        );
                });
        }
    }
}
