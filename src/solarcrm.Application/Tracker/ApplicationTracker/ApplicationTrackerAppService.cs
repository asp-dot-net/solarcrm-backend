﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Newtonsoft.Json;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.EntityFrameworkCore;
using solarcrm.Jobs.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Authorization.Users;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.DataVaults.LeadStatus;
using Abp.Organizations;
using Abp.Timing.Timezone;
using solarcrm.Dto;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Payments;
using AutoMapper;
using solarcrm.DataVaults.Vehical.Dto;
using solarcrm.DataVaults.Vehical;
using NPOI.SS.Formula.Functions;
using Org.BouncyCastle.Asn1.X509;

namespace solarcrm.ApplicationTracker
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Application)]
    public class ApplicationTrackerAppService : solarcrmAppServiceBase, IApplicationTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;

        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IApplicationTrackerExcelExporter _AppTrackerExcelExporter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly IRepository<DocumentList> _documentRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        // private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailjobRepository;
        private readonly IRepository<PaymentReceiptUploadJob> _PaymentReceiptUploadjobRepository;
        public ApplicationTrackerAppService(IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IApplicationTrackerExcelExporter AppTrackerExcelExporter,
            IRepository<User, long> userRepository,
            IRepository<DocumentList> documentRepository,
            IRepository<LeadStatus, int> lookup_leadStatusRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            UserManager userManager,
            ITimeZoneConverter timeZoneConverter,
            IRepository<LeadDocuments> leadDocumentRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<PaymentDetailsJob> paymentDetailjobRepository,
            IRepository<PaymentReceiptUploadJob> PaymentReceiptUploadjobRepository
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _AppTrackerExcelExporter = AppTrackerExcelExporter;
            _userRepository = userRepository;
            _userManager = userManager;
            _documentRepository = documentRepository;
            _leadDocumentRepository = leadDocumentRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
            _paymentDetailjobRepository = paymentDetailjobRepository;
            _PaymentReceiptUploadjobRepository = PaymentReceiptUploadjobRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Application)]
        public async Task<PagedResultDto<GetApplicationTrackerForViewDto>> GetAllApplicationTracker(GetAllApplicationTrackerInput input)
        {

            List<int> PaymentReceiptupload = new List<int>();
            List<int> DocumentListData = new List<int>();
            List<int> leadDoclistid = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<int> PenddingDocumentList = new List<int>();
            //IList<string> Userrole;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            var job_list = _jobRepository.GetAll();
            var LeadDocument = _leadDocumentRepository.GetAll();
            var DocumentListMasterData = _documentRepository.GetAll();
            //if (User != null)
            //{
            //    Userrole = await _userManager.GetRolesAsync(User);
            //}

            if (!string.IsNullOrEmpty(input.Filter))
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate" && leadactivity_list != null)
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }

            PaymentReceiptupload = _PaymentReceiptUploadjobRepository.GetAll().Include(x => x.PaymentIdFK).Select(x => x.PaymentIdFK.PaymentJobId).Distinct().ToList();

            if (DocumentListMasterData != null || DocumentListMasterData.Count() > 0)
            {
                DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id).ToList();
            }

            if (DocumentListData.Count() != 0 && LeadDocument.Count() != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }
            //pending filter data
            if (leadDoclistid != null || leadDoclistid.Count() > 0)
            {
                PenddingDocumentList = leadDoclistid.GroupBy(x => x).Where(g => g.Count() == DocumentListData.Count()).Select(y => y.Key).ToList();
            }

            var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                || e.LeadFK.Alt_Phone.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.LeadId)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.ApplicationStatusId))
                            .WhereIf(input.otpPendingFilter != null, e => input.otpPendingFilter.ToLower() == e.OtpVerifed.ToLower()) //OTPpending Filter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "yes", e => (e.LastComment != null || e.LastComment != "")) //Query RaisedFilter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "no", e => (e.LastComment == null || e.LastComment == "")) //Query RaisedFilter
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId)) // discomlist filter
                            .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId)) // circlelist filter
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId)) //devisionlist filter
                            .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId)) //subdivisiionlist filter
                            .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId)) //solartypelist filter
                            .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.LeadFK.ChanelPartnerID)) //employee with CPlist filter
                            .WhereIf(input.applicationstatusTrackerFilter == 1, e => e.ApplicationDate != null && PenddingDocumentList.Contains(e.LeadId) && (e.ApplicationNumber != null || e.ApplicationNumber != "")) //Completed
                            .WhereIf(input.applicationstatusTrackerFilter == 2, e => e.LeadFK.IsVerified && PenddingDocumentList.Contains(e.LeadId) && e.ApplicationDate == null && (e.ApplicationNumber == null || e.ApplicationNumber == "")) //pending                                                                                                                                                                                                    
                            .WhereIf(input.applicationstatusTrackerFilter == 4, e => e.LeadFK.IsVerified && (PenddingDocumentList.Contains(e.LeadId) == false)) //Awaiting                                                                                                                                                              
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "queryraisedon" && input.StartDate != null, e => e.LastCommentDate >= SDate.Value.Date && e.LastCommentDate <= EDate.Value.Date)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "nextfollowup" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
                            //.WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "quatationaccepteddate" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
                            .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit && PaymentReceiptupload.Contains(e.Id));

            var pagedAndFilteredJob = filteredJob
                .OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadFK.LeadStatusId equals o3.Id into j3
                       from s3 in j3.DefaultIfEmpty()

                       join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                       from s4 in j4.DefaultIfEmpty()

                       select new GetApplicationTrackerForViewDto()
                       {
                           appTracker = new ApplicationTrackerDto
                           {
                               Id = o.Id,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                               ProjectNumber = o.JobNumber,
                               //JobStatus = o.JobStatusFk.Name,
                               CustomerName = o.LeadFK.CustomerName,
                               Address = (o.LeadFK.AddressLine1 == null ? "" : o.LeadFK.AddressLine1 + ", ") + (o.LeadFK.AddressLine2 == null ? "" : o.LeadFK.AddressLine2 + ", ") + (o.LeadFK.CityIdFk.Name == null ? "" : o.LeadFK.CityIdFk.Name + ", ") + (o.LeadFK.TalukaIdFk.Name == null ? "" : o.LeadFK.TalukaIdFk.Name + ", ") + (o.LeadFK.DiscomIdFk.Name == null ? "" : o.LeadFK.DiscomIdFk.Name + ", ") + (o.LeadFK.StateIdFk.Name == null ? "" : o.LeadFK.StateIdFk.Name + "-") + (o.LeadFK.Pincode == null ? "" : o.LeadFK.Pincode),
                               Mobile = o.LeadFK.MobileNumber,
                               SubDivision = o.LeadFK.SubDivisionIdFk == null ? null : o.LeadFK.SubDivisionIdFk.Name,
                               JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                               LeadStatusName = o.LeadFK.LeadStatusIdFk == null ? null : o.LeadFK.LeadStatusIdFk.Status,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Notes = o.LeadFK.Notes,
                               LastModificationTime = o.LastModificationTime,
                               CreationTime = (DateTime)o.CreationTime,
                               QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                               LeadStatusColorClass = o.LeadFK.LeadStatusIdFk == null ? null : o.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.LeadFK.LeadStatusIdFk == null ? null : o.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.ApplicationStatusIdFK,
                               ApplicationNumber = o.ApplicationNumber,
                           },
                           documentListName = (ObjectMapper.Map<List<DocumentListDto>>(DocumentListMasterData.Where(x => DocumentListData.Contains(x.Id)).ToList())),
                           leaddocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(LeadDocument.Where(x => x.LeadDocumentId == o.LeadId && DocumentListData.Contains(x.DocumentId)).ToList())),
                           applicationTrackerSummaryCount = applicationTrackerSummaryCount(filteredJob.ToList(), input, leadDoclistid),
                           //ReminderTime = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId).OrderByDescending(e => e.LeadId == o.Id && e.LeadActionId == 8).FirstOrDefault().CreationTime,
                           ////(DateTime?)_reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,

                       };

            var totalCount = await filteredJob.CountAsync();

            return new PagedResultDto<GetApplicationTrackerForViewDto>(totalCount, await jobs.ToListAsync());
        }
        public ApplicationTrackerSummaryCount applicationTrackerSummaryCount(List<Job.Jobs> filteredLeads, GetAllApplicationTrackerInput input, List<int> leadDoclistid)
        {
            ApplicationTrackerSummaryCount output = new ApplicationTrackerSummaryCount();
            output.Total = Convert.ToString(filteredLeads.Sum(x => x.TotalJobCost));
            output.ApplicationPending = Convert.ToString(filteredLeads.Where(e => e.LeadFK.IsVerified && leadDoclistid.Count != 0 && e.ApplicationDate == null && e.ApplicationNumber == null).Count());
            output.OTP_Pending = Convert.ToString(filteredLeads.Where(e => e.OtpVerifed == "pending").Count());
            output.ApplicationSubmited = Convert.ToString(filteredLeads.Where(e => e.ApplicationDate != null).Count());
            output.FeasiblityApproved = Convert.ToString(filteredLeads.Where(e => e.ApplicationStatusId == 4).Count()); // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
            output.MeterInstalled = "0"; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
            output.SubcidyClaimed = "0"; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());

            return output;
        }
        public async Task<ApplicationTrackerSummaryCount> getapplicationTrackerSummaryCount(GetAllLeadsInput input)
        {
            ApplicationTrackerSummaryCount output = new ApplicationTrackerSummaryCount();
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            int UserId = (int)AbpSession.UserId;

            var DocumentListData = await _documentRepository.GetAll().Where(x => x.IsATFlag == true).Select(x => x.Id).ToListAsync();
            var leadDoclistid = new List<int>();
            var LeadDocument = await _leadDocumentRepository.GetAll().ToListAsync();
            if (DocumentListData.Count != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }
            //var Job_list = _jobRepository.GetAll();
            var leadactive_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.ChanelPartnerID == UserId && e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            //      var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true);

            //var jobnumberlist = new List<int?>();
            var filteredLeads = _jobRepository.GetAll().Include(e => e.LeadFK)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.LeadFK.FirstName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter) || e.LeadFK.AddressLine1.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FirstNameFilter), e => e.LeadFK.FirstName == input.FirstNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.LeadFK.EmailId == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.LeadFK.Alt_Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.LeadFK.MobileNumber == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.LeadFK.AddressLine1.Contains(input.AddressFilter))


                        //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationUnitId == input.OrganizationUnit);
            //.Where(e => !statusIds.Contains(e.LeadStatusId))
            //.WhereIf(input.JobStatusID != null && input.JobStatusID.Count() > 0, e => joblist.Contains(e.Id))
            //.WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId));


            //var LeadidList = Job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();


            output.Total = Convert.ToString(filteredLeads.Count());
            output.ApplicationPending = Convert.ToString(filteredLeads.Where(e => e.LeadFK.IsVerified && leadDoclistid.Count != 0 && e.ApplicationDate == null && e.ApplicationNumber == null).Count());
            output.OTP_Pending = Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
            output.ApplicationSubmited = Convert.ToString(filteredLeads.Where(e => e.ApplicationDate != null).Count());
            output.FeasiblityApproved = ""; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
            output.MeterInstalled = ""; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
            output.SubcidyClaimed = ""; // Convert.ToString(filteredLeads.Where(e => e.OtpVerifed.ToLower() == "pending").Count());
            //output.Sold = Convert.ToString(filteredLeads.Where(e => LeadidList.Contains(e.Id)).Count());
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Application_QuickView)]
        public async Task<ApplicationTrackerQuickViewDto> GetApplicationQuickView(EntityDto input)
        {
            try
            {
                //var leads = _leadRepository.FirstOrDefault(input.Id);
                ApplicationTrackerQuickViewDto output = new ApplicationTrackerQuickViewDto();
                List<int> documentListData = await _documentRepository.GetAll().Where(x => x.IsATFlag == true).Select(x => x.Id).ToListAsync();
                var job = _jobRepository.GetAll().Include(e => e.LeadFK).
                    Include(e => e.LeadFK.DivisionIdFk).
                    Include(e => e.LeadFK.SubDivisionIdFk).
                    Include(e => e.LeadFK.DiscomIdFk).
                    Include(e => e.LeadFK.CityIdFk).
                    Include(e => e.LeadFK.TalukaIdFk).
                    Include(e => e.LeadFK.DistrictIdFk).
                    Include(e => e.LeadFK.StateIdFk).
                    Where(e => e.Id == input.Id).FirstOrDefault();
                if (job != null)
                {
                    output = new ApplicationTrackerQuickViewDto
                    {
                        //Job = ObjectMapper.Map<CreateOrEditJobDto>(job),

                        ProjectNumber = job.JobNumber,
                        Latitude = job.LeadFK == null ? null : (decimal?)job.LeadFK.Latitude,
                        Longitude = job.LeadFK == null ? null : (decimal?)job.LeadFK.Longitude,
                        AvgmonthlyBill = job.LeadFK == null ? null : (int?)job.LeadFK.AvgmonthlyBill,
                        AvgmonthlyUnit = job.LeadFK == null ? null : (int?)job.LeadFK.AvgmonthlyUnit,
                        CustomerName = job.LeadFK == null ? null : job.LeadFK.CustomerName,
                        ConsumerNumber = job.LeadFK == null ? null : job.LeadFK.ConsumerNumber,
                        SubDivisionName = job.LeadFK.SubDivisionIdFk == null ? null : job.LeadFK.SubDivisionIdFk.Name, //leads.SubDivisionId != null ? _subDivisionRepository.FirstOrDefaultAsync((int)leads.SubDivisionId).Result.Name : "",
                        DivisionName = job.LeadFK.DivisionIdFk == null ? null : job.LeadFK.DivisionIdFk.Name, //leads.DivisionId != null ? _divisionRepository.FirstOrDefaultAsync((int)leads.DivisionId).Result.Name : "",
                        DiscomName = job.LeadFK.DiscomIdFk == null ? null : job.LeadFK.DiscomIdFk.Name, //leads.DiscomId != null ? _discomRepository.FirstOrDefaultAsync((int)leads.DiscomId).Result.Name : "",

                        SystemSize = job.PvCapacityKw, //pendding
                        MeterPhase = job.PhaseOfInverter,
                        Category = job.LeadFK == null ? null : job.LeadFK.Category,
                        EmailId = job.LeadFK == null ? null : job.LeadFK.EmailId,
                        MobileNumber = job.LeadFK == null ? null : job.LeadFK.MobileNumber,
                        AddressLine1 = job.LeadFK == null ? null : job.LeadFK.AddressLine1,
                        AddressLine2 = job.LeadFK == null ? null : job.LeadFK.AddressLine2,
                        CityName = job.LeadFK.CityIdFk == null ? null : job.LeadFK.CityIdFk.Name,
                        TalukaName = job.LeadFK.TalukaIdFk == null ? null : job.LeadFK.TalukaIdFk.Name,
                        DistrictName = job.LeadFK.DistrictIdFk == null ? null : job.LeadFK.DistrictIdFk.Name,
                        StateName = job.LeadFK.StateIdFk == null ? null : job.LeadFK.StateIdFk.Name,
                        Pincode = job.LeadFK == null ? null : job.LeadFK.Pincode,
                        CommonMeter = job.LeadFK == null ? false : job.LeadFK.CommonMeter,
                        BankAccountNumber = job.LeadFK == null ? null : job.LeadFK.BankAccountNumber,
                        BankAccountHolderName = job.LeadFK == null ? null : job.LeadFK.BankAccountHolderName,
                        BankName = job.LeadFK == null ? null : job.LeadFK.BankName,
                        BankBrachName = job.LeadFK == null ? null : job.LeadFK.BankBrachName,
                        IFSC_Code = job.LeadFK == null ? null : job.LeadFK.IFSC_Code,
                        LeadDocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(_leadDocumentRepository.GetAll().Include(x => x.DocumentIdFk).Where(x => x.LeadDocumentId == job.LeadId && documentListData.Contains(x.DocumentId)).ToList()
                    .Select(doc => new LeadDocumentDto
                    {
                        Title = doc.DocumentIdFk.Name,
                        DocumentNumber = doc.DocumentNumber,
                        LeadDocumentPath = doc.LeadDocumentPath,
                        CreatedDate = doc.CreationTime,
                        CreatedBy = _userRepository.FirstOrDefault((long)doc.CreatorUserId).Name
                    })))
                    };
                }

                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Application_LeadViewDetails)]
        public async Task ApplicationDetailsEdit(CreateOrEditJobDto input, int SectionId)
        {
            //var leads = _leadRepository.GetAll().Where(x =>x.Id == (int?)input.Id);
            var job = await _jobRepository.FirstOrDefaultAsync(e => e.Id == input.Id);

            if (job != null)
            {
                job.OtpVerifed = input.OtpVerifed;
                job.GUVNLRegisterationNo = input.GUVNLRegisterationNo;
                job.ApplicationNumber = input.ApplicationNumber;
                job.ApplicationDate = input.ApplicationDate;
                //job.SignApplicationPath = input.SignApplicationPath;

                #region Activity Log
                //Add Activity Log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 11;
                //leadactivity.LeadAcitivityID = 0;
                leadactivity.SectionId = SectionId;
                leadactivity.LeadActionNote = "Job Application Tracker Details Modified";
                leadactivity.LeadId = job.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                var List = new List<LeadtrackerHistory>();

                try
                {
                    List<Job.Jobs> objAuditOld_OnlineUserMst = new List<Job.Jobs>();
                    List<CreateOrEditJobDto> objAuditNew_OnlineUserMst = new List<CreateOrEditJobDto>();

                    DataTable Dt_Target = new DataTable();
                    DataTable Dt_Source = new DataTable();



                    objAuditOld_OnlineUserMst.Add(job);
                    objAuditNew_OnlineUserMst.Add(input);

                    //Add Activity Log
                    LeadtrackerHistory leadActivityLog = new LeadtrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        leadActivityLog.TenantId = (int)AbpSession.TenantId;
                    }

                    object newvalue = objAuditNew_OnlineUserMst;
                    Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                    if (objAuditOld_OnlineUserMst != null)
                    {
                        Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                    }
                    foreach (DataRow dr_S in Dt_Source.Rows)
                    {
                        foreach (DataColumn dc_S in Dt_Source.Columns)
                        {

                            if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                            {
                                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                objAuditInfo.FieldName = dc_S.ColumnName;
                                objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                                //objAuditInfo.Action = "Edit";
                                objAuditInfo.Action = Dt_Target.Rows[0][dc_S.ColumnName].ToString() == "" ? "Add" : "Edit";
                                objAuditInfo.LeadId = job.LeadId;
                                objAuditInfo.LeadActionId = leadactid;
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                List.Add(objAuditInfo);
                            }
                        }
                    }
                }
                catch
                {
                }
                #endregion


            }

            return;
        }

        //Application Tracker Excel Export
        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Application_ExportToExcel)]
        public async Task<FileDto> GetApplicationTrackerToExcel(GetAllApplicationTrackerInput input)
        {
            List<int> PaymentReceiptupload = new List<int>();
            List<int> DocumentListData = new List<int>();
            List<int> leadDoclistid = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<int> PenddingDocumentList = new List<int>();
            //IList<string> Userrole;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            var job_list = _jobRepository.GetAll();
            var LeadDocument = _leadDocumentRepository.GetAll();
            var DocumentListMasterData = _documentRepository.GetAll();
            //if (User != null)
            //{
            //    Userrole = await _userManager.GetRolesAsync(User);
            //}

            if (!string.IsNullOrEmpty(input.Filter))
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate" && leadactivity_list != null)
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }

            PaymentReceiptupload = _PaymentReceiptUploadjobRepository.GetAll().Include(x => x.PaymentIdFK).Select(x => x.PaymentIdFK.PaymentJobId).Distinct().ToList();

            if (DocumentListMasterData != null || DocumentListMasterData.Count() > 0)
            {
                DocumentListData = DocumentListMasterData.Where(x => x.IsATFlag == true).Select(x => x.Id).ToList();
            }

            if (DocumentListData.Count() != 0 && LeadDocument.Count() != 0)
            {
                leadDoclistid = LeadDocument.Where(x => DocumentListData.Contains(x.DocumentId)).Select(x => x.LeadDocumentId).ToList();
            }
            //pending filter data
            if (leadDoclistid != null || leadDoclistid.Count() > 0)
            {
                PenddingDocumentList = leadDoclistid.GroupBy(x => x).Where(g => g.Count() == DocumentListData.Count()).Select(y => y.Key).ToList();
            }

            var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                || e.LeadFK.Alt_Phone.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.LeadId)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.ApplicationStatusId))
                            .WhereIf(input.otpPendingFilter != null, e => input.otpPendingFilter.ToLower() == e.OtpVerifed.ToLower()) //OTPpending Filter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "yes", e => (e.LastComment != null || e.LastComment != "")) //Query RaisedFilter
                            .WhereIf(input.queryRaisedFilter != null && input.queryRaisedFilter.ToLower() == "no", e => (e.LastComment == null || e.LastComment == "")) //Query RaisedFilter
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId)) // discomlist filter
                            .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId)) // circlelist filter
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId)) //devisionlist filter
                            .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId)) //subdivisiionlist filter
                            .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId)) //solartypelist filter
                            .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.LeadFK.ChanelPartnerID)) //employee with CPlist filter
                            .WhereIf(input.applicationstatusTrackerFilter == 1, e => e.ApplicationDate != null && PenddingDocumentList.Contains(e.LeadId) && (e.ApplicationNumber != null || e.ApplicationNumber != "")) //Completed
                            .WhereIf(input.applicationstatusTrackerFilter == 2, e => e.LeadFK.IsVerified && PenddingDocumentList.Contains(e.LeadId) && e.ApplicationDate == null && (e.ApplicationNumber == null || e.ApplicationNumber == "")) //pending                                                                                                                                                                                                    
                            .WhereIf(input.applicationstatusTrackerFilter == 4, e => e.LeadFK.IsVerified && (PenddingDocumentList.Contains(e.LeadId) == false)) //Awaiting                                                                                                                                                              
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "queryraisedon" && input.StartDate != null, e => e.LastCommentDate >= SDate.Value.Date && e.LastCommentDate <= EDate.Value.Date)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "nextfollowup" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
                            //.WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "quatationaccepteddate" && input.StartDate != null, e => NextFollowupList.Contains(e.Id))
                            .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit && PaymentReceiptupload.Contains(e.Id));

            var pagedAndFilteredJob = filteredJob
                .OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob

                       join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadFK.LeadStatusId equals o3.Id into j3
                       from s3 in j3.DefaultIfEmpty()

                       join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                       from s4 in j4.DefaultIfEmpty()

                       select new GetApplicationTrackerForViewDto()
                       {
                           appTracker = new ApplicationTrackerDto
                           {
                               Id = o.Id,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                               ProjectNumber = o.JobNumber,
                               //JobStatus = o.JobStatusFk.Name,
                               CustomerName = o.LeadFK.CustomerName,
                               Address = (o.LeadFK.AddressLine1 == null ? "" : o.LeadFK.AddressLine1 + ", ") + (o.LeadFK.AddressLine2 == null ? "" : o.LeadFK.AddressLine2 + ", ") + (o.LeadFK.CityIdFk.Name == null ? "" : o.LeadFK.CityIdFk.Name + ", ") + (o.LeadFK.TalukaIdFk.Name == null ? "" : o.LeadFK.TalukaIdFk.Name + ", ") + (o.LeadFK.DiscomIdFk.Name == null ? "" : o.LeadFK.DiscomIdFk.Name + ", ") + (o.LeadFK.StateIdFk.Name == null ? "" : o.LeadFK.StateIdFk.Name + "-") + (o.LeadFK.Pincode == null ? "" : o.LeadFK.Pincode),
                               Mobile = o.LeadFK.MobileNumber,
                               SubDivision = o.LeadFK.SubDivisionIdFk == null ? null : o.LeadFK.SubDivisionIdFk.Name,
                               JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                               LeadStatusName = o.LeadFK.LeadStatusIdFk == null ? null : o.LeadFK.LeadStatusIdFk.Status,
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               Notes = o.LeadFK.Notes,
                               LastModificationTime = o.LastModificationTime,
                               CreationTime = (DateTime)o.CreationTime,
                               QuesryDescription = o.LastComment, // add karavnu govt.status excel marhi                             
                               LeadStatusColorClass = o.LeadFK.LeadStatusIdFk == null ? null : o.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.LeadFK.LeadStatusIdFk == null ? null : o.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.ApplicationStatusIdFK,
                               ApplicationNumber = o.ApplicationNumber,
                           },
                           documentListName = (ObjectMapper.Map<List<DocumentListDto>>(DocumentListMasterData.Where(x => DocumentListData.Contains(x.Id)).ToList())),
                           leaddocumentList = (ObjectMapper.Map<List<LeadDocumentDto>>(LeadDocument.Where(x => x.LeadDocumentId == o.LeadId && DocumentListData.Contains(x.DocumentId)).ToList())),
                           applicationTrackerSummaryCount = applicationTrackerSummaryCount(filteredJob.ToList(), input, leadDoclistid),
                           //ReminderTime = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId).OrderByDescending(e => e.LeadId == o.Id && e.LeadActionId == 8).FirstOrDefault().CreationTime,
                           ////(DateTime?)_reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,

                       };
            var applicationtrackerListDtos = await jobs.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _AppTrackerExcelExporter.ExportToFile(applicationtrackerListDtos);
            }
            else
            {
                return _AppTrackerExcelExporter.ExportToFile(applicationtrackerListDtos);
            }

        }
    }
}
