﻿using solarcrm.Dto;
using solarcrm.Tracker.MeterConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.MeterConnectTracker.Exporting
{
    public interface IMeterConnectTrackerExcelExporter
    {
        FileDto ExportToFile(List<GetMeterConnectTrackerForViewDto> meterconnectTracker);
    }
}
