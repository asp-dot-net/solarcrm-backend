﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Tracker.MeterConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.MeterConnectTracker.Exporting
{
    public class MeterConnectTrackerExcelExporter : NpoiExcelExporterBase, IMeterConnectTrackerExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public MeterConnectTrackerExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetMeterConnectTrackerForViewDto> metertrack)
        {
            return CreateExcelPackage(
                "MeterConnectTracker.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("MeterConnectTracker"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("LeadStatus"),
                        L("ApplicationStatus"),
                        L("CustomerName"),
                        L("Kilowatt"),
                        L("InstallationDate"),
                        L("SubDivision"),
                        L("Discom"),
                        L("ConsumerNumber"),
                        L("PanaltyDays"),
                        L("FollowDate"),
                        L("FollowDescription"),
                        L("Comment")
                        );
                    AddObjects(
                        sheet, metertrack,
                        _ => _.meterconnectTracker.ProjectNumber,
                        _ => _.meterconnectTracker.LeadStatusName,
                        _ => _.meterconnectTracker.ApplicationStatusdata,
                        _ => _.meterconnectTracker.CustomerName,
                        _ => _.meterconnectTracker.KiloWatt,
                        _ => _.meterconnectTracker.InstallationDate,
                        _ => _.meterconnectTracker.SubDivision,
                        _ => _.meterconnectTracker.Discom,
                        _ => _.meterconnectTracker.ConsumerNo,
                        _ => _.meterconnectTracker.PenaltyDays,
                         _ => _.meterconnectTracker.FollowDate,
                          _ => _.meterconnectTracker.FollowDiscription,
                           _ => _.meterconnectTracker.Comment
                        );
                });
        }
    }
}
