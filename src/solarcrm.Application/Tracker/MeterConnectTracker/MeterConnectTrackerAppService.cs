﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Dispatch;
using solarcrm.EntityFrameworkCore;
using solarcrm.Job;
using solarcrm.JobInstallation;
using solarcrm.Payments;
using solarcrm.Transport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using solarcrm.Tracker.MeterConnect;
using solarcrm.Tracker.MeterConnect.Dto;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.DataVaults;
using solarcrm.MeterConnect;
using solarcrm.DataVaults.LeadHistory;
using Abp.Authorization;
using solarcrm.Authorization;
using solarcrm.Dto;
using solarcrm.Tracker.MeterConnectTracker.Exporting;

namespace solarcrm.Tracker.MeterConnectTracker
{
    public class MeterConnectTrackerAppService : solarcrmAppServiceBase, IMeterConnectTrackerAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<TransportDetails> _transportDetailsRepository;
        private readonly IRepository<DispatchMaterialDetails> _dispatchMaterialDetailsRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<JobInstallationDetail> _jobInstallRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<SSPaymentDetails> _ssPaymentDetailsRepository;
        private readonly IRepository<STCPaymentDetails> _stcPaymentDetailsRepository;
        private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<MeterConnectDetails> _meterConnectDetailsRepository;
        private readonly IMeterConnectTrackerExcelExporter _MeterconnectTrackerExcelExporter;
        // private readonly IRepository<Leads> _leadRepository;        
        private readonly UserManager _userManager;
       
        public MeterConnectTrackerAppService(IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<TransportDetails> transportDetailsRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<DispatchMaterialDetails> dispatchMaterialDetailsRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IAbpSession abpSession, UserManager userManager,
            IRepository<JobInstallationDetail> jobInstallRepository,
            ITimeZoneConverter timeZoneConverter,
            IRepository<User, long> userRepository,
            IRepository<SSPaymentDetails> ssPaymentDetailsRepository,
            IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<STCPaymentDetails> stcPaymentDetailsRepository,
            IMeterConnectTrackerExcelExporter MeterconnectTrackerExcelExporter,
            IRepository<MeterConnectDetails> meterConnectDetailsRepository)
        {
            _dbContextProvider = dbContextProvider;
            _transportDetailsRepository = transportDetailsRepository;
            _jobRepository = jobRepository;
            _dispatchMaterialDetailsRepository = dispatchMaterialDetailsRepository;
            _leadactivityRepository = leadactivityRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _userRepository = userRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _commentLeadActivityLog = commentLeadActivityLog;
            _timeZoneConverter = timeZoneConverter;
            _jobInstallRepository = jobInstallRepository;
            _ssPaymentDetailsRepository = ssPaymentDetailsRepository;
            _stcPaymentDetailsRepository = stcPaymentDetailsRepository;
            _applicationstatusRepository = applicationstatusRepository;
            _meterConnectDetailsRepository = meterConnectDetailsRepository;
            _MeterconnectTrackerExcelExporter = MeterconnectTrackerExcelExporter;
            _userManager = userManager;
        }
        public async Task<PagedResultDto<GetMeterConnectTrackerForViewDto>> GetAllMeterConnectTracker(GetAllMeterConnectTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<DateTime> dispatchDatelist = new List<DateTime>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

            var jobInstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).Select(x => x.JobId);
            var InstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null);
            var MeterConnectList = _meterConnectDetailsRepository.GetAll().Include(x=>x.InstallationFKId);
            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var filteredtransport = _transportDetailsRepository.GetAll().Include(x => x.TransportJobsIdFK).Include(x => x.VehicalIdFK).Include(x => x.TransportJobsIdFK.LeadFK)
                .Include(x => x.TransportJobsIdFK.LeadFK.SubDivisionIdFk).Include(x => x.TransportJobsIdFK.LeadFK.DiscomIdFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TransportJobsIdFK.JobNumber.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.CustomerName.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter)  
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.TransportJobsIdFK.LeadId)))
                        .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.TransportJobsIdFK.LeadFK.LeadStatusId))
                        .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.TransportJobsIdFK.ApplicationStatusId))
                        .WhereIf(input.meteapplicationReadyFilter != null && input.meteapplicationReadyFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(1) && (MeterConnectList.Any(x => x.InstallationFKId.JobId == e.TransportJobId && x.MeterApplicationReadyStatus == 1)))) // Yes
                        .WhereIf(input.meteapplicationReadyFilter != null && input.meteapplicationReadyFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(2) && (MeterConnectList.Any(x => x.InstallationFKId.JobId == e.TransportJobId && x.MeterApplicationReadyStatus == 2 && e.TransportJobId != x.InstallationFKId.JobId)))) // No
                        .WhereIf(input.meteapplicationReadyFilter != null && input.meteapplicationReadyFilter == 3, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3) && (MeterConnectList.Any(x => x.InstallationFKId.JobId == e.TransportJobId && x.MeterApplicationReadyStatus == 3)))) // Submited
                        .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.DiscomId))
                        .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.CircleId))
                        .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e=> input.DivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.DivisionId))
                        .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SubDivisionId))
                        .WhereIf(input.employeeIdFilter != null && input.employeeIdFilter.Count() > 0, e => input.employeeIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.ChanelPartnerID))
                        .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SolarTypeId))
                        .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.ChanelPartnerID))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "meterfilesentdate", e => MeterConnectList.Select(x=>x.MeterAgreementSentDate) != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "meterfilesubmitteddate", e => MeterConnectList.Select(x => x.MeterSubmissionDate) != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "instllationcompleteddate", e => jobInstallCompleted.Contains(e.TransportJobId))                                        
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "panaltydays", e => e.TransportJobsIdFK.PenaltyDaysPending != null)
                        .Where(e => e.TransportJobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit && jobInstallCompleted.Contains(e.TransportJobId));

              var pagedAndFilteredTransport = filteredtransport.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var transportDetails = from o in pagedAndFilteredTransport

                                       //join o3 in _jobInstallRepository.GetAll() on o.TransportJobId equals o3.JobInstallerID into j3
                                       //from s3 in j3.DefaultIfEmpty()

                                       //join o2 in _meterConnectDetailsRepository.GetAll() on s3.Id equals o2.InstallationId into j2
                                       //from s2 in j2.DefaultIfEmpty()
                                   select new GetMeterConnectTrackerForViewDto()
                                   {
                                       meterconnectTracker = new MeterConnectTrackerDto
                                       {
                                           Id = o.Id,
                                           leaddata = ObjectMapper.Map<LeadsDto>(o.TransportJobsIdFK.LeadFK),
                                           JobId = o.TransportJobId,
                                           ProjectNumber = o.TransportJobsIdFK.JobNumber,
                                           CustomerName = o.TransportJobsIdFK.LeadFK.CustomerName,
                                           Address = (o.TransportJobsIdFK.LeadFK.AddressLine1 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine1 + ", ") + (o.TransportJobsIdFK.LeadFK.AddressLine2 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine2 + ", ") + (o.TransportJobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.TransportJobsIdFK.LeadFK.Pincode == null ? "" : o.TransportJobsIdFK.LeadFK.Pincode),
                                           Mobile = o.TransportJobsIdFK.LeadFK.MobileNumber,
                                           KiloWatt = o.TransportJobsIdFK.PvCapacityKw,
                                           InstallationId = InstallCompleted.Where(x => x.JobId == o.TransportJobId).FirstOrDefault().Id,
                                           //InstallationDate = _jobInstallRepository.FirstOrDefault(x => x.JobId == o.TransportJobId).InstallCompleteDate,
                                           SubDivision = o.TransportJobsIdFK.LeadFK.SubDivisionIdFk.Name,
                                           Discom = o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name,
                                           ConsumerNo = Convert.ToString(o.TransportJobsIdFK.LeadFK.ConsumerNumber),
                                           PenaltyDays = o.TransportJobsIdFK.PenaltyDaysPending,
                                           LeadStatusName = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                           FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.TransportJobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                           FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                           Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                           LeadStatusColorClass = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                           LeadStatusIconClass = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                           ApplicationStatusdata = _applicationstatusRepository.GetAll().Where(x => x.Id == o.TransportJobsIdFK.ApplicationStatusId && x.IsActive == true).FirstOrDefault(),
                                       },

                                       meterConnectTrackerSummaryCount = meterConnectTrackerSummaryCount(filteredtransport.ToList())
                                   };
            var totalCount = await filteredtransport.CountAsync();

            return new PagedResultDto<GetMeterConnectTrackerForViewDto>(totalCount, await transportDetails.ToListAsync());
        }
        public MeterConnectTrackerSummaryCount meterConnectTrackerSummaryCount(List<TransportDetails> filteredLeads)
        {
            var output = new MeterConnectTrackerSummaryCount();
            if (filteredLeads.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);
                var installCompletcount = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).ToList().Count();
                output.Total = Convert.ToString(panelCount);
                output.FeasiblityApproved = Convert.ToString(installCompletcount);
                output.InstalltionCompleted = Convert.ToString(installCompletcount); //Convert.ToString(filteredLeads.Sum(x => x.TransportJobsIdFK.PvCapacityKw));
                output.MeterfileSent = "";
                output.MeterfileSubmitted = "";
            }
            return output;
        }

        public async Task<GetMeterConnectDetailsForEditOutput> GetMeterConnectForEdit(EntityDto input)
        {
            var meterConnect = await _meterConnectDetailsRepository.FirstOrDefaultAsync(x => x.InstallationId == input.Id);
            var output = new GetMeterConnectDetailsForEditOutput { meterConnectDto = ObjectMapper.Map<CreateOrEditMeterConnectDto>(meterConnect) };
            return output;
        }
        public async Task CreateOrEdit(CreateOrEditMeterConnectDto input)
        {
            if (input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        //[AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_Dispatch)]
        protected virtual async Task Create(CreateOrEditMeterConnectDto input)
        {
            var meterconnect = ObjectMapper.Map<MeterConnectDetails>(input);
            if (meterconnect != null && meterconnect.InstallationId != 0)
            {               
                var MeterConnectItem = await _meterConnectDetailsRepository.InsertAndGetIdAsync(meterconnect);
                if (MeterConnectItem != 0)
                {
                    //Add Activity Log Installer
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 15;
                    leadactivity.SectionId = 62;
                    leadactivity.LeadActionNote = "Meter Connect Details Created";
                    leadactivity.LeadId = input.leadid;
                    leadactivity.OrganizationUnitId = input.OrgID;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }
        //[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Refund_Edit)]
        protected virtual async Task Update(CreateOrEditMeterConnectDto input)
        {
            var meterConnect = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId).Where(x => x.InstallationId == input.InstallationId && x.Id == input.Id).FirstOrDefault();
            if (meterConnect != null)
            {
                meterConnect.MeterAgreementGivenTo = input.MeterAgreementGivenTo;
                meterConnect.MeterAgreementNo = input.MeterAgreementNo;
                meterConnect.MeterAgreementSentDate = input.MeterAgreementSentDate;
                meterConnect.MeterApplicationReadyStatus = input.MeterApplicationReadyStatus;
                meterConnect.MeterSubmissionDate = input.MeterSubmissionDate;
                var MeterConnectItem = await _meterConnectDetailsRepository.UpdateAsync(meterConnect);
                int SectionId = 44;
                if (MeterConnectItem.Id != 0)
                {
                    #region Activity Log
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 11;
                    //leadactivity.LeadAcitivityID = 0;
                    leadactivity.SectionId = SectionId;
                    leadactivity.LeadActionNote = "Meter Connect Details Modified";
                    leadactivity.LeadId = input.leadid;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
                    var List = new List<LeadtrackerHistory>();
                    //add the dispatch Product Item
                    //if (leadactid != 0)
                    //{
                    //    LeadtrackerHistory leadHistory1 = new LeadtrackerHistory();
                    //    if (AbpSession.TenantId != null)
                    //    {
                    //        leadHistory1.TenantId = (int)AbpSession.TenantId;
                    //    }
                    //    if (meterConnect.MeterAgreementGivenTo != input.MeterAgreementGivenTo)
                    //    {
                    //        leadHistory1.FieldName = "MeterAgreementGivenTo";
                    //        leadHistory1.PrevValue = meterConnect.MeterAgreementGivenTo;
                    //        leadHistory1.CurValue = "Deleted";
                    //        leadHistory1.Action = "Stock Item Delete";
                    //        leadHistory1.LastModificationTime = DateTime.Now;
                    //        leadHistory1.LeadId = input[0].leadid;
                    //        leadHistory1.LeadActionId = leadactid;
                    //        List.Add(leadHistory1);

                    //    }

                    //}

                    #endregion
                }
            }
        }

        public async Task<SelfCertificateDetailDto> SelfCertficateDetails(EntityDto input, int organizationId)
        {
            var job = await _jobRepository.GetAll().Include(x => x.LeadFK).Include(x => x.LeadFK.DiscomIdFk).Include(x => x.LeadFK.CircleIdFk)
                    .Include(x => x.LeadFK.DivisionIdFk).Include(x => x.LeadFK.SubDivisionIdFk).Where(x => x.LeadId == input.Id).FirstOrDefaultAsync();
            var stockItem = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk).Where(x => x.JobId == job.Id).ToList();
            var output = new SelfCertificateDetailDto();
            if (job != null)
            {
            //    var jobDetails = await _jobRepository.GetAll().Include(x=>x.LeadFK).Include(x => x.LeadFK.DiscomIdFk).Include(x => x.LeadFK.CircleIdFk)
            //        .Include(x => x.LeadFK.DivisionIdFk).Include(x => x.LeadFK.SubDivisionIdFk).Where(x => x.Id == job.Id).FirstOrDefaultAsync();
                output = new SelfCertificateDetailDto
                {
                    ConsumerNumber = job.LeadFK.ConsumerNumber,
                    DiscomName = job.LeadFK.DiscomIdFk == null ? null: job.LeadFK.DiscomIdFk.Name,
                    CertificateDate = DateTime.Now,
                    AgencyName = "",
                    GUVNLnumber = job.GUVNLRegisterationNo,
                    Customername = job.LeadFK.CustomerName,
                    CircleName = job.LeadFK.CircleIdFk == null ? null : job.LeadFK.CircleIdFk.Name,
                    DivisionName = job.LeadFK.DivisionIdFk == null ? null : job.LeadFK.DivisionIdFk.Name,
                    SubDivisionName = job.LeadFK.SubDivisionIdFk == null ? null : job.LeadFK.SubDivisionIdFk.Name,
                    Customeraddress1 = job.LeadFK.AddressLine1,
                    Customeraddress2 = job.LeadFK.AddressLine2,
                    ContractedLoadKW = job.PvCapacityKw,
                    ContractPhase = job.PhaseOfInverter,
                    
                    SolarPVModuleMake = stockItem.Where(x=>x.ProductItemFk.CategoryId == 1).FirstOrDefault().ProductItemFk.Manufacturer,
                    PVModelNo = stockItem.Where(x => x.ProductItemFk.CategoryId == 1).FirstOrDefault().ProductItemFk.Model,
                    TypeOfPVModule = stockItem.Where(x => x.ProductItemFk.CategoryId == 1).FirstOrDefault().ProductItemFk.Series,
                    RatedCapacityofSolarModuleinwattmorethan250wp = stockItem.Where(x => x.ProductItemFk.CategoryId == 1).FirstOrDefault().ProductItemFk.Size,
                    PVNoofModules = stockItem.Where(x => x.ProductItemFk.CategoryId == 1).FirstOrDefault().Quantity,
                    TotalPVCapacityinstalledinKwp = stockItem.Where(x => x.ProductItemFk.CategoryId == 1).FirstOrDefault().Quantity * stockItem.Where(x => x.ProductItemFk.CategoryId == 1).FirstOrDefault().ProductItemFk.Size,
                    
                    MakeOfInverter = stockItem.Where(x => x.ProductItemFk.CategoryId == 2).FirstOrDefault().ProductItemFk.Manufacturer,
                    IVModelNo = stockItem.Where(x => x.ProductItemFk.CategoryId == 2).FirstOrDefault().ProductItemFk.Model,
                    TypeOfInverter = stockItem.Where(x => x.ProductItemFk.CategoryId == 2).FirstOrDefault().ProductItemFk.Phase.ToString(),
                    RatedACoutputofInverterKiloWatt = stockItem.Where(x => x.ProductItemFk.CategoryId == 2).FirstOrDefault().ProductItemFk.Size,
                    SerialNoofInverter = stockItem.Where(x => x.ProductItemFk.CategoryId == 2).FirstOrDefault().ProductItemFk.Series,
                   
                };
            }
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Tracker_MeterConnect_ExportToExcel)]
        public async Task<FileDto> GetApplicationTrackerToExcel(GetAllMeterConnectTrackerInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<DateTime> dispatchDatelist = new List<DateTime>();
            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);

            var jobInstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null).Select(x => x.JobId);
            var InstallCompleted = _jobInstallRepository.GetAll().Where(x => x.InstallCompleteDate != null);
            var MeterConnectList = _meterConnectDetailsRepository.GetAll().Include(x => x.InstallationFKId);
            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var filteredtransport = _transportDetailsRepository.GetAll().Include(x => x.TransportJobsIdFK).Include(x => x.VehicalIdFK).Include(x => x.TransportJobsIdFK.LeadFK)
                .Include(x => x.TransportJobsIdFK.LeadFK.SubDivisionIdFk).Include(x => x.TransportJobsIdFK.LeadFK.DiscomIdFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TransportJobsIdFK.JobNumber.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.CustomerName.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.EmailId.Contains(input.Filter)
                                || e.TransportJobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) || e.TransportJobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.TransportJobsIdFK.LeadId)))
                        .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.TransportJobsIdFK.LeadFK.LeadStatusId))
                        .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.TransportJobsIdFK.ApplicationStatusId))
                        .WhereIf(input.meteapplicationReadyFilter != null && input.meteapplicationReadyFilter == 1, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(1)))
                        .WhereIf(input.meteapplicationReadyFilter != null && input.meteapplicationReadyFilter == 2, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(2)))
                        .WhereIf(input.meteapplicationReadyFilter != null && input.meteapplicationReadyFilter == 3, e => (MeterConnectList.Select(x => x.MeterApplicationReadyStatus).Contains(3)))
                        .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.DiscomId))
                        .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.CircleId))
                        .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.DivisionId))
                        .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SubDivisionId))
                        .WhereIf(input.employeeIdFilter != null && input.employeeIdFilter.Count() > 0, e => input.employeeIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.ChanelPartnerID))
                        .WhereIf(input.solarTypeIdFilter != null && input.solarTypeIdFilter.Count() > 0, e => input.solarTypeIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.SolarTypeId))
                        .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.TransportJobsIdFK.LeadFK.ChanelPartnerID))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "meterfilesentdate", e => MeterConnectList.Select(x => x.MeterAgreementSentDate) != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "meterfilesubmitteddate", e => MeterConnectList.Select(x => x.MeterSubmissionDate) != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "instllationcompleteddate", e => jobInstallCompleted.Contains(e.TransportJobId))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "panaltydays", e => e.TransportJobsIdFK.PenaltyDaysPending != null)
                        .Where(e => e.TransportJobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit && jobInstallCompleted.Contains(e.TransportJobId));

            var pagedAndFilteredTransport = filteredtransport.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var meterConnectDetails = from o in pagedAndFilteredTransport
                                       //join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                                       //from s3 in j3.DefaultIfEmpty()

                                   select new GetMeterConnectTrackerForViewDto()
                                   {
                                       meterconnectTracker = new MeterConnectTrackerDto
                                       {
                                           Id = o.Id,
                                           leaddata = ObjectMapper.Map<LeadsDto>(o.TransportJobsIdFK.LeadFK),
                                           JobId = o.TransportJobId,
                                           ProjectNumber = o.TransportJobsIdFK.JobNumber,
                                           CustomerName = o.TransportJobsIdFK.LeadFK.CustomerName,
                                           Address = (o.TransportJobsIdFK.LeadFK.AddressLine1 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine1 + ", ") + (o.TransportJobsIdFK.LeadFK.AddressLine2 == null ? "" : o.TransportJobsIdFK.LeadFK.AddressLine2 + ", ") + (o.TransportJobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.TransportJobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.TransportJobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.TransportJobsIdFK.LeadFK.Pincode == null ? "" : o.TransportJobsIdFK.LeadFK.Pincode),
                                           Mobile = o.TransportJobsIdFK.LeadFK.MobileNumber,
                                           KiloWatt = o.TransportJobsIdFK.PvCapacityKw,
                                           InstallationId = InstallCompleted.Where(x => x.JobId == o.TransportJobId).FirstOrDefault().Id,
                                           //InstallationDate = _jobInstallRepository.FirstOrDefault(x => x.JobId == o.TransportJobId).InstallCompleteDate,
                                           SubDivision = o.TransportJobsIdFK.LeadFK.SubDivisionIdFk.Name,
                                           Discom = o.TransportJobsIdFK.LeadFK.DiscomIdFk.Name,
                                           ConsumerNo = Convert.ToString(o.TransportJobsIdFK.LeadFK.ConsumerNumber),
                                           PenaltyDays = o.TransportJobsIdFK.PenaltyDaysPending,
                                           LeadStatusName = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                           FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.TransportJobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                           FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                           Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.TransportJobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                           LeadStatusColorClass = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                           LeadStatusIconClass = o.TransportJobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                           ApplicationStatusdata = _applicationstatusRepository.GetAll().Where(x => x.Id == o.TransportJobsIdFK.ApplicationStatusId && x.IsActive == true).FirstOrDefault(),
                                       },

                                       meterConnectTrackerSummaryCount = meterConnectTrackerSummaryCount(filteredtransport.ToList())
                                   };
            var applicationtrackerListDtos = await meterConnectDetails.ToListAsync();
            if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
            {
                return _MeterconnectTrackerExcelExporter.ExportToFile(applicationtrackerListDtos);
            }
            else
            {
                return _MeterconnectTrackerExcelExporter.ExportToFile(applicationtrackerListDtos);
            }
        }
    }
}
