﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Roles;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.Circle;
using solarcrm.DataVaults.City;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.Division;
using solarcrm.DataVaults.HeightStructure;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.DataVaults.LeadType;
using solarcrm.DataVaults.SolarType;
using solarcrm.DataVaults.SubDivision;
using solarcrm.Dto;
using solarcrm.MobileService.MCommon.Dto;
using solarcrm.MobileService.MCommonRespose;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace solarcrm.MobileService.MCommon
{
    public class MobileCommonAppService : solarcrmAppServiceBase, IMobileCommonAppService
    {
        private readonly IRepository<SolarType> _solarTypeRepository;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<SubDivision> _subDivisionRepository;
        private readonly IRepository<Division> _divisionRepository;
        private readonly IRepository<Circle> _circleRepository;
        private readonly IRepository<Discom> _discomRepository;
        private readonly IRepository<HeightStructure> _heightStructureRepository;
        private readonly IRepository<LeadSource> _leadSourceRepository;
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly IRepository<LeadType> _leadTypeRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserDetail> _userDetailsRepository;
        private readonly IRepository<Leads> _leadRepository;
        //private readonly BaseResponse _response;
        //public BaseResponse response;
        public MobileCommonAppService(
            IRepository<SolarType> solarTypeRepository,
            IRepository<City> cityRepository,
            IRepository<SubDivision> subDivisionRepository,
            IRepository<Division> divisionRepository,
            IRepository<Circle> circleRepository,
            IRepository<Discom> discomRepository,
            IRepository<HeightStructure> heightStructureRepository,
            IRepository<LeadSource> leadSourceRepository,
            IRepository<LeadStatus> leadStatusRepository,
            IRepository<LeadType> leadTypeRepository,
            IRepository<User, long> userRepository,
            IRepository<UserDetail> userDetailsRepository,
            IRepository<Leads> leadRepository
            //BaseResponse response
            )
        {
            _solarTypeRepository = solarTypeRepository;
            _cityRepository = cityRepository;
            _subDivisionRepository = subDivisionRepository;
            _divisionRepository = divisionRepository;
            _circleRepository = circleRepository;
            _discomRepository = discomRepository;
            _heightStructureRepository = heightStructureRepository;
            _leadSourceRepository = leadSourceRepository;
            _leadStatusRepository = leadStatusRepository;
            _leadTypeRepository = leadTypeRepository;
            _userRepository = userRepository;
            _userDetailsRepository = userDetailsRepository;
            _leadRepository = leadRepository;
            //_response = response;
        }

        public async Task<List<NamingString>> GetAllSolarTypeForDropdown()
        {
            var allSolarType = _solarTypeRepository.GetAll().Where(x => x.IsActive == true);

            var solarTypes = from o in allSolarType
                             select new NamingString()
                             {
                                 Name = o.Name
                             };

            return new List<NamingString>(await solarTypes.ToListAsync());
        }

        public async Task<List<NamingString>> SearchCity(StringFilter input)
        {
            if (string.IsNullOrWhiteSpace(input.FlterText))
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "City can't be null");
            }

            var serachCity = _cityRepository.GetAll().Where(x => x.IsActive == true && x.Name.ToLower().StartsWith(input.FlterText.ToLower()));

            var cities = from o in serachCity
                         select new NamingString()
                         {
                             Name = o.Name
                         };

            return new List<NamingString>(await cities.ToListAsync());
        }

        public async Task<CityResult> CityResult(StringFilter input)
        {
            if (string.IsNullOrWhiteSpace(input.FlterText))
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "City can't be null");
            }

            var serachCity = _cityRepository.GetAll().Where(x => x.IsActive == true && x.Name.ToLower() == input.FlterText.ToLower());

            var result = from o in serachCity
                         select new CityResult()
                         {
                             City = o.Name,
                             Taluka = o.TalukaFk.Name,
                             District = o.TalukaFk.DistrictFk.Name,
                             State = o.TalukaFk.DistrictFk.StateFk.Name
                         };

            return await result.FirstOrDefaultAsync();
        }

        public async Task<List<NamingString>> SearchSubDivision(StringFilter input)
        {
            if (string.IsNullOrWhiteSpace(input.FlterText))
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Sub division can't be null");
            }

            var serachSubDivision = _subDivisionRepository.GetAll().Where(x => x.IsActive == true && x.Name.ToLower().StartsWith(input.FlterText.ToLower()));

            var subDivisions = from o in serachSubDivision
                               select new NamingString()
                               {
                                   Name = o.Name
                               };

            return new List<NamingString>(await subDivisions.ToListAsync());
        }

        public async Task<SubDivisionResult> SubDivisionResult(StringFilter input)
        {
            if (string.IsNullOrWhiteSpace(input.FlterText))
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Sub division can't be null");
            }

            var serachSubDivision = _subDivisionRepository.GetAll().Where(x => x.IsActive == true && x.Name.ToLower() == input.FlterText.ToLower());

            var result = from o in serachSubDivision
                         select new SubDivisionResult()
                         {
                             SubDivision = o.Name,
                             Division = o.DivisionFk.Name,
                             Circle = o.DivisionFk.CircleFk.Name,
                             Discom = o.DivisionFk.CircleFk.DiscomIdFk.Name
                         };

            return await result.FirstOrDefaultAsync();
        }

        public async Task<List<NamingString>> GetAllDivisionForDropdown()
        {
            var allDivision = _divisionRepository.GetAll().Where(x => x.IsActive == true);

            var result = from o in allDivision
                         select new NamingString()
                         {
                             Name = o.Name
                         };

            return new List<NamingString>(await result.ToListAsync());
        }

        public async Task<List<NamingString>> GetAllCircleForDropdown()
        {
            var result = _circleRepository.GetAll().Where(x => x.IsActive == true);

            var returnResult = from o in result
                               select new NamingString()
                               {
                                   Name = o.Name
                               };

            return new List<NamingString>(await returnResult.ToListAsync());
        }

        public async Task<List<NamingString>> GetAllDiscomForDropdown()
        {
            var result = _discomRepository.GetAll().Where(x => x.IsActive == true);

            var returnResult = from o in result
                               select new NamingString()
                               {
                                   Name = o.Name
                               };

            return new List<NamingString>(await returnResult.ToListAsync());
        }

        public async Task<List<NamingString>> GetAllCategoryForDropdown()
        {
            var returnResult = new List<NamingString> {
                new NamingString { Name = "SC" },
                new NamingString { Name = "ST" },
                new NamingString { Name = "OBC" },
                new NamingString { Name = "General" }
            };

            await Task.CompletedTask;

            return returnResult;
        }

        public async Task<List<NamingString>> GetAllHeightOfStructureForDropdown()
        {
            var result = _heightStructureRepository.GetAll().Where(x => x.IsActive == true);

            var returnResult = from o in result
                               select new NamingString()
                               {
                                   Name = o.Name
                               };

            return new List<NamingString>(await returnResult.ToListAsync());
        }
        //Lead Source Dropdown
        public async Task<List<NamingString>> GetAllLeadSourceForDropdown()
        {
            var result = _leadSourceRepository.GetAll().Where(x => x.IsActive == true);

            var returnResult = from o in result
                               select new NamingString()
                               {
                                   Name = o.Name
                               };

            return new List<NamingString>(await returnResult.ToListAsync());
        }
        //Lead Status Dropdown
        public async Task<List<NamingString>> GetAllLeadStatusForDropdown()
        {
            var result = _leadStatusRepository.GetAll().Where(x => x.IsActive == true);

            var returnResult = from o in result
                               select new NamingString()
                               {
                                   Name = o.Status
                               };

            return new List<NamingString>(await returnResult.ToListAsync());
        }
        //Lead Type Dropdown
        public async Task<List<NamingString>> GetAllLeadTypeForDropdown()
        {
            var result = _leadTypeRepository.GetAll().Where(x => x.IsActive == true);

            var returnResult = from o in result
                               select new NamingString()
                               {
                                   Name = o.Name
                               };

            return new List<NamingString>(await returnResult.ToListAsync());
        }

        public async Task<List<NamingString>> SearchChannelPartner(StringFilter input)
        {
            if (string.IsNullOrWhiteSpace(input.FlterText))
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Channel Partner can't be null");
            }

            var returnResult = from u in _userRepository.GetAll()
                               join ud in _userDetailsRepository.GetAll() on u.Id equals (int)ud.UserId
                               where u.IsActive == true && ud.UserType == 2 && u.Name.Contains(input.FlterText.ToLower()) //Channel Partner
                               select new NamingString()
                               {
                                   Name = u.FullName == null || u.FullName == null ? "" : u.FullName.ToString()
                               };

            return new List<NamingString>(await returnResult.ToListAsync());
        }
       
        public async Task<BaseResponse> CheckDuplicateConsumerNoMobile(DuplicateConsumerFilter filter)
        {
            BaseResponse response = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(filter.solartype))
                {
                    int solartypeid = await _solarTypeRepository.GetAll().Where(e => e.Name.Contains(filter.solartype.ToLower().Trim())  && e.IsDeleted != true).Select(x=>x.Id).FirstOrDefaultAsync();
                    if (solartypeid != 0)
                    {
                        var Checkduplicate = await _leadRepository.GetAll().Where(e => e.ConsumerNumber == filter.consumerNumber && e.SolarTypeId == solartypeid && e.IsDeleted != true).FirstOrDefaultAsync();
                        if (Checkduplicate != null)
                        {
                            response = new BaseResponse(HttpStatusCode.OK, true, Checkduplicate, "Consumer Number Already Exist!");
                        }
                        else
                        {
                            response = new BaseResponse(HttpStatusCode.OK, true, Checkduplicate, "");
                        }
                    }
                    else
                    {
                        response = new BaseResponse(HttpStatusCode.NotFound, true, null, "Solar type Not Found!");
                    }
                }
                else
                {
                    response = new BaseResponse(HttpStatusCode.OK, true, null,"Please Enter the Solar Type");
                }
            }
            catch (System.Exception ex)
            {
                response = new BaseResponse(HttpStatusCode.OK, true, ex.InnerException, ex.Message);
            }
            return response;
        }
    }
}
