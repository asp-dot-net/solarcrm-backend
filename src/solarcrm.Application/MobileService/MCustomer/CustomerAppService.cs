﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.Leads;
using solarcrm.MobileService.MCustomer.Dto;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.SolarType;
using solarcrm.DataVaults.HeightStructure;
using System.Collections.Generic;
using solarcrm.DataVaults.LeadActivityLog;
using Abp.Notifications;
using Abp.Authorization.Users;
using Abp.UI;
using solarcrm.DataVaults.City;
using solarcrm.DataVaults.SubDivision;
using solarcrm.DataVaults.Division;
using solarcrm.DataVaults.Circle;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.LeadType;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.Authorization.Users;
using Microsoft.AspNetCore.Identity;
using System.Data;
using solarcrm.DataVaults.LeadHistory;
using Newtonsoft.Json;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using System.Net.Http;
using solarcrm.MobileService.MCommonRespose;
using System.Net;

namespace solarcrm.MobileService.MCustomer
{
    public class CustomerAppService : solarcrmAppServiceBase, ICustomerAppService
    {
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<SolarType> _solarTypeRepository;
        private readonly IRepository<HeightStructure> _heightStructureRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<SubDivision> _subDivisionRepository;
        private readonly IRepository<Division> _divisionRepository;
        private readonly IRepository<Circle> _circleRepository;
        private readonly IRepository<Discom> _discomRepository;
        private readonly IRepository<LeadSource> _leadSourceRepository;
        private readonly IRepository<LeadType> _leadTypeRepository;
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        // private readonly BaseResponse _response;
        public CustomerAppService(
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<SolarType> solarTypeRepository,
            IRepository<HeightStructure> heightStructureRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<City> cityRepository,
            IRepository<SubDivision> subDivisionRepository,
            IRepository<Division> divisionRepository,
            IRepository<Circle> circleRepository,
            IRepository<Discom> discomRepository,
            IRepository<LeadSource> leadSourceRepository,
            IRepository<LeadType> leadTypeRepository,
            IRepository<LeadStatus> leadStatusRepository,
            IRepository<User, long> userRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider
            //BaseResponse response
            )
        {
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _solarTypeRepository = solarTypeRepository;
            _heightStructureRepository = heightStructureRepository;
            _leadactivityRepository = leadactivityRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _cityRepository = cityRepository;
            _subDivisionRepository = subDivisionRepository;
            _divisionRepository = divisionRepository;
            _circleRepository = circleRepository;
            _discomRepository = discomRepository;
            _leadSourceRepository = leadSourceRepository;
            _leadTypeRepository = leadTypeRepository;
            _leadStatusRepository = leadStatusRepository;
            _userRepository = userRepository;
            _dbContextProvider = dbContextProvider;
            //_response = response;cre
        }

        public async Task<PagedResultDto<CustomerDto>> GetAll(GetAllCustomerInput input)
        {
            IdentityResult identityResult = new IdentityResult();

            try
            {
                var filteredCustomer = _leadRepository.GetAll().Where(e => e.IsActive == true).WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => (e.FirstName + ' ' + e.LastName).Contains(input.Filter) || e.MobileNumber.Contains(input.Filter));

                var pagedAndFilteredCustomer = filteredCustomer.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                var customers = (from o in pagedAndFilteredCustomer
                                 select new CustomerDto()
                                 {
                                     Id = o.Id,
                                     CustomerName = o.FirstName + " " + o.LastName,
                                     Address1 = (o.AddressLine1 == null ? "" : o.AddressLine1 + ",") + (o.AddressLine2 == null ? "" : o.AddressLine2),
                                     Address2 = (o.CityIdFk.Name == "" ? "" : o.CityIdFk.Name + ", ") + (o.TalukaIdFk.Name == null ? "" : o.TalukaIdFk.Name + ", ") + (o.DistrictIdFk.Name == null ? "" : o.DistrictIdFk.Name),
                                     Address3 = (o.StateIdFk.Name == null ? "" : o.StateIdFk.Name + " - ") + (o.Pincode == 0 ? "" : Convert.ToString(o.Pincode)),
                                     Mobile = o.MobileNumber,
                                     LeadStatus = o.LeadStatusIdFk.Status,
                                     LeadStatusColorClass = o.LeadStatusIdFk.LeadStatusColorClass,
                                     //ApplicationStatus = _jobRepository.GetAll().Where(e => e.LeadId == o.Id).FirstOrDefault().ApplicationStatusFk.Name,
                                     //ApplicationStatusColorClass = _jobRepository.GetAll().Where(e => e.LeadId == o.Id).FirstOrDefault().ApplicationStatusFk.ColorClass
                                     IsVerified = o.IsVerified,
                                     IsJob = _jobRepository.GetAll().Where(x => x.LeadId == o.Id).Count() > 0 ? true : false
                                 });

                var totalCount = await filteredCustomer.CountAsync();
                return new PagedResultDto<CustomerDto>(totalCount, await customers.ToListAsync());
            }
            catch (Exception ex)
            {
                throw new Abp.UI.UserFriendlyException(401, ex.Message, ex.InnerException.Message);
            }
        }

        public async Task<GetCustomerForViewDto> GetCustomerForView(NullableIdDto<int> input)
        {
            if (!input.Id.HasValue)
            {
                return null;
            }
            else
            {
                var Customer = await _leadRepository.GetAsync((int)input.Id);

                var Job = _jobRepository.GetAll().Where(e => e.LeadId == input.Id).FirstOrDefault();
                var SolarType = _solarTypeRepository.GetAll().Where(e => e.Id == Customer.SolarTypeId).FirstOrDefault();
                var HeightStructure = _heightStructureRepository.GetAll().Where(e => e.Id == Customer.HeightStructureId).FirstOrDefault();

                var output = new GetCustomerForViewDto
                {
                    JobNumber = Job != null ? Job.JobNumber : "",
                    //Latitude = Customer.Latitude.ToString(),
                    //Longitude = Customer.Longitude.ToString(),
                    //SolarType = SolarType != null ? SolarType.Name : "",
                    //HightOfStructure = HeightStructure != null ? HeightStructure.Name : "",
                    //StructureAmount = Customer.StrctureAmmount.ToString(),
                    //CommonMeter = Customer.CommonMeter == true ? "Yes" : "No",
                    //RequireCapacity = Customer.RequiredCapacity.ToString(),
                    //ConsumerNumber = Customer.ConsumerNumber.ToString(),

                    TechnicalDetails = new List<TechnicalDetailsDto>() {
                        new TechnicalDetailsDto()
                        {
                            Title = "Latitude",
                            Value = Customer.Latitude.ToString()
                        },
                        new TechnicalDetailsDto()
                        {
                            Title = "Longitude",
                            Value = Customer.Longitude.ToString()
                        },
                        new TechnicalDetailsDto()
                        {
                            Title = "Solar Type",
                            Value = SolarType != null ? SolarType.Name : ""
                        }
                        ,new TechnicalDetailsDto()
                        {
                            Title = "Hight Of Structure",
                            Value = HeightStructure != null ? HeightStructure.Name : ""
                        },
                        new TechnicalDetailsDto()
                        {
                            Title = "Strcture Ammount",
                            Value = Customer.StrctureAmmount.ToString()
                        },
                        new TechnicalDetailsDto()
                        {
                            Title = "Common Meter",
                            Value = Customer.CommonMeter == true ? "Yes" : "No"
                        },
                        new TechnicalDetailsDto()
                        {
                            Title = "Required Capacity",
                            Value = Customer.RequiredCapacity.ToString()
                        },
                        new TechnicalDetailsDto()
                        {
                            Title = "Consumer Number",
                            Value = Customer.ConsumerNumber.ToString()
                        }
                    }
                };
                return output;
            }
        }
        public async Task<GetCustomerForEditOutput> GetCustomerForEdit(NullableIdDto<int> input)
        {
            try
            {
                if (input.Id != 0)
                {
                    var leads = await _leadRepository.GetAll().Where(x => x.Id == input.Id)
                        .Include(x => x.CityIdFk)
                        .Include(x => x.TalukaIdFk)
                        .Include(x => x.DistrictIdFk)
                        .Include(x => x.StateIdFk)
                        .Include(x => x.DivisionIdFk)
                        .Include(x => x.SubDivisionIdFk)
                        .Include(x => x.SolarTypeIdFk)
                        .Include(x => x.CircleIdFk)
                        .Include(x => x.DiscomIdFk)
                        .Include(x => x.HeightStructureIdFk)
                        .Include(x => x.LeadSourceIdFk)
                        .Include(x => x.LeadStatusIdFk)
                        .Include(x => x.LeadTypeIdFk)              //Inquiry type
                        .FirstOrDefaultAsync();

                    //await _leadRepository.FirstOrDefaultAsync((int)input.Id);
                    if (leads != null)
                    {
                        var output = new GetCustomerForEditOutput { Customer = ObjectMapper.Map<CreateOrEditCustomerDto>(leads) };
                        if (leads.SolarTypeIdFk != null)
                        {
                            output.Customer.SolarType = leads.SolarTypeIdFk.Name;
                        }
                        if (leads.CityIdFk.Name != null)
                        {
                            output.Customer.City = leads.CityIdFk.Name;
                        }
                        if (leads.TalukaIdFk != null)
                        {
                            output.Customer.Taluka = leads.TalukaIdFk.Name;
                        }
                        if (leads.DistrictIdFk.Name != null)
                        {
                            output.Customer.District = leads.DistrictIdFk.Name;
                        }
                        if (leads.StateIdFk != null)
                        {
                            output.Customer.State = leads.StateIdFk.Name;
                        }
                        if (leads.DivisionIdFk != null)
                        {
                            output.Customer.Division = leads.DivisionIdFk.Name;
                        }
                        if (leads.SubDivisionIdFk != null)
                        {
                            output.Customer.SubDivision = leads.SubDivisionIdFk.Name;
                        }
                        if (leads.CircleIdFk != null)
                        {
                            output.Customer.Circle = leads.CircleIdFk.Name;
                        }
                        if (leads.DiscomIdFk != null)
                        {
                            output.Customer.Discom = leads.DiscomIdFk.Name;
                        }
                        if (leads.HeightStructureIdFk != null)
                        {
                            output.Customer.HeightStructure = leads.HeightStructureIdFk.Name;
                        }
                        if (leads.LeadSourceIdFk != null)
                        {
                            output.Customer.LeadSource = leads.LeadSourceIdFk.Name;
                        }
                        if (leads.ChanelPartnerID != 0)
                        {
                            var result = _userRepository.GetAll().Where(e => e.Id == leads.ChanelPartnerID).FirstOrDefault();
                            if (result != null)
                            {
                                output.Customer.ChanelPartner = result.FullName; //(int?)result.Id;
                            }
                            //else
                            //{
                            //    throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Chanel Partner is invalid");
                            //}
                            //output.Customer.ChanelPartner = _userRepository.GetAll().Where(x => x.Id == leads.ChanelPartnerID).FirstOrDefault().FullName;
                        }
                        return output;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Customer not found", "");
                    }
                }
                else
                {
                    throw new UserFriendlyException(500, "Customer Id is invalid", "");
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(500, ex.Message, "");
            }
        }
        public async Task<BaseResponse> CreateOrEdit(CreateOrEditCustomerDto input)
        {
            BaseResponse result;//= new BaseResponse();
            if (input.Id == null || input.Id == 0)
            {
                result = await Create(input);
            }
            else
            {
                result = await Update(input);
            }
            return result;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Create, AppPermissions.Pages_Tenant_Leads_ManageLead_Create)]
        protected virtual async Task<BaseResponse> Create(CreateOrEditCustomerDto input)
        {
            //HttpResponseMessage response = new HttpResponseMessage();
            BaseResponse response;
            try
            {
                //Get UserId
                long UserId = (long)AbpSession.UserId;

                var customer = ObjectMapper.Map<Leads>(input);

                //Get TanentId
                if (AbpSession.TenantId != null)
                {
                    customer.TenantId = (int?)AbpSession.TenantId;
                }

                //Get Organazation for Current User
                var userOrg = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == UserId).ToList();
                if (userOrg.Any())
                {
                    customer.OrganizationUnitId = (int)userOrg[0].OrganizationUnitId;
                }
                else
                {
                    throw new UserFriendlyException(500, "Ooppps! There is a problem!", "organization not found for this user");
                }

                //Get CityId, TalukaId, DistrictId, StateId By City Name
                if (!string.IsNullOrWhiteSpace(input.City))
                {
                    var cityList = _cityRepository.GetAll().Include(x => x.TalukaFk).Include(x => x.TalukaFk.DistrictFk).Include(x => x.TalukaFk.DistrictFk.StateFk).Where(e => e.Name == input.City).FirstOrDefault();
                    if (cityList != null)
                    {
                        customer.CityId = cityList.Id;
                        customer.TalukaId = cityList.TalukaId;
                        customer.DistrictId = cityList.TalukaFk.DistrictId;
                        customer.StateId = cityList.TalukaFk.DistrictFk.StateId;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "City name is invalid");
                    }
                }
                else
                {
                    customer.CityId = null;
                    customer.TalukaId = null;
                    customer.DistrictId = null;
                    customer.StateId = null;
                }

                //Get LeadType By Name
                if (!string.IsNullOrWhiteSpace(input.LeadType))
                {
                    var result = _leadTypeRepository.GetAll().Where(e => e.Name == input.LeadType).FirstOrDefault();
                    if (result != null)
                    {
                        customer.LeadTypeId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Lead Type is invalid");
                    }
                }
                else
                {
                    customer.LeadTypeId = 0;
                }

                //Get LeadSource By Name
                if (!string.IsNullOrWhiteSpace(input.LeadSource))
                {
                    var result = _leadSourceRepository.GetAll().Where(e => e.Name == input.LeadSource).FirstOrDefault();
                    if (result != null)
                    {
                        customer.LeadSourceId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Lead Source is invalid");
                    }
                }
                else
                {
                    customer.LeadSourceId = 0;
                }

                //Get LeadStatus By Name
                if (!string.IsNullOrWhiteSpace(input.LeadStatus))
                {
                    var result = _leadStatusRepository.GetAll().Where(e => e.Status == input.LeadStatus).FirstOrDefault();
                    if (result != null)
                    {
                        customer.LeadStatusId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Lead Status is invalid");
                    }
                }
                else
                {
                    customer.LeadStatusId = 0;
                }

                //Get SolarType By Name
                if (!string.IsNullOrWhiteSpace(input.SolarType))
                {
                    var result = _solarTypeRepository.GetAll().Where(e => e.Name == input.SolarType).FirstOrDefault();
                    if (result != null)
                    {
                        customer.SolarTypeId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Solar Type is invalid");
                    }
                }
                else
                {
                    customer.SolarTypeId = 0;
                }

                //Get DiscomId By Name
                if (!string.IsNullOrWhiteSpace(input.Discom))
                {
                    var result = _discomRepository.GetAll().Where(e => e.Name == input.Discom).FirstOrDefault();
                    if (result != null)
                    {
                        customer.DiscomId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Discom is invalid");
                    }
                }
                else
                {
                    customer.DiscomId = null;
                }

                //Get DivisionId By Name
                if (!string.IsNullOrWhiteSpace(input.Division))
                {
                    var result = _divisionRepository.GetAll().Where(e => e.Name == input.Division).FirstOrDefault();
                    if (result != null)
                    {
                        customer.DivisionId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Division is invalid");
                    }
                }
                else
                {
                    customer.DivisionId = null;
                }

                //Get SubDivision Id By Name
                if (!string.IsNullOrWhiteSpace(input.SubDivision))
                {
                    var result = _subDivisionRepository.GetAll().Where(e => e.Name == input.SubDivision).FirstOrDefault();
                    if (result != null)
                    {
                        customer.SubDivisionId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "SubDivision is invalid");
                    }
                }
                else
                {
                    customer.SubDivisionId = null;
                }

                //Get Circle Id By Name
                if (!string.IsNullOrWhiteSpace(input.Circle))
                {
                    var result = _circleRepository.GetAll().Where(e => e.Name == input.Circle).FirstOrDefault();
                    if (result != null)
                    {
                        customer.CircleId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Circle is invalid");
                    }
                }
                else
                {
                    customer.CircleId = null;
                }

                //Get HeightStructure Id By Name
                if (!string.IsNullOrWhiteSpace(input.HeightStructure))
                {
                    var result = _heightStructureRepository.GetAll().Where(e => e.Name == input.HeightStructure).FirstOrDefault();
                    if (result != null)
                    {
                        customer.HeightStructureId = result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "HeightStructure is invalid");
                    }
                }
                else
                {
                    customer.HeightStructureId = null;
                }

                //Get ChanelPartner Id By Name
                if (input.ChanelPartner != "0" && input.ChanelPartner != null)
                {
                    var result = _userRepository.GetAll().Where(e => e.Name == input.ChanelPartner).FirstOrDefault();
                    if (result != null)
                    {
                        customer.ChanelPartnerID = (int?)result.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Chanel Partner is invalid");
                    }
                }
                else
                {
                    customer.ChanelPartnerID = null;
                }

                customer.IsVerified = false;

                if (!string.IsNullOrEmpty(customer.FirstName) && !string.IsNullOrEmpty(customer.LastName) && !string.IsNullOrEmpty(customer.MobileNumber) &&
                    customer.SolarTypeId.ToString() != null && customer.ConsumerNumber != null && !string.IsNullOrEmpty(customer.AddressLine1) && !string.IsNullOrEmpty(customer.AddressLine2) &&
                    customer.CityId != null && customer.StateId != null && customer.DistrictId != null && customer.StateId != null && customer.Pincode != null && customer.SubDivisionId != null && customer.DivisionId != null && customer.CircleId != null && customer.DiscomId != null && customer.Latitude != null && customer.Longitude != null && customer.RequiredCapacity != null && customer.StrctureAmmount != null && customer.HeightStructureId != null && customer.CommonMeter.ToString() != null)
                {
                    customer.IsVerified = true;
                }

                customer.RequiredCapacity = input.RequiredCapacity == null ? 0 : input.RequiredCapacity;
                customer.IsActive = true;
                var customerId = await _leadRepository.InsertAndGetIdAsync(customer);
                //if (customerId != null)
                //{
                //    //await NewLeadNotify(customer);
                //}
                //Add Activity Log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 1;
                leadactivity.SectionId = 35;
                leadactivity.LeadActionNote = "Customer Created from Mobile";
                leadactivity.ActionFrom = "Mobile";
                leadactivity.LeadId = customer.Id;
                leadactivity.OrganizationUnitId = customer.OrganizationUnitId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var Response = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
                response = new BaseResponse(HttpStatusCode.OK, true, null, "Successfully Created the Customer !");
                //HttpStatusCode statusCode, object result = null, string errorMessage = null
            }
            catch (System.Exception ex)
            {
                response = new BaseResponse(HttpStatusCode.BadRequest, false, null, ex.Message);
                //throw new UserFriendlyException(500, "Ooppps! There is a problem!", ex.Message);
            }

            return response;
            //throw new UserFriendlyException(201, "Successfuly Insert Record", "");
        }

        protected virtual async Task<BaseResponse> Update(CreateOrEditCustomerDto input)
        {

            BaseResponse response;
            try
            {
                //Get UserId
                long UserId = (long)AbpSession.UserId;
                List<Leads> objAuditOld_Customer = new List<Leads>();
                List<CreateOrEditCustomerDto> objAuditNew_Customer = new List<CreateOrEditCustomerDto>();


                DataTable Dt_Target = new DataTable();
                DataTable Dt_Source = new DataTable();

                var List = new List<LeadtrackerHistory>();
                var customer = await _leadRepository.FirstOrDefaultAsync((int)input.Id);

                if (customer != null)
                {
                    customer = ObjectMapper.Map(input, customer);

                    //Get TanentId
                    if (AbpSession.TenantId != null)
                    {
                        customer.TenantId = (int?)AbpSession.TenantId;
                    }

                    //Get Organazation for Current User
                    var userOrg = _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == UserId).ToList();
                    if (userOrg.Any())
                    {
                        customer.OrganizationUnitId = (int)userOrg[0].OrganizationUnitId;
                    }
                    else
                    {
                        throw new UserFriendlyException(500, "Ooppps! There is a problem!", "organization not found for this user");
                    }

                    //Get CityId, TalukaId, DistrictId, StateId By City Name
                    if (!string.IsNullOrWhiteSpace(input.City))
                    {
                        var cityList = _cityRepository.GetAll().Include(x => x.TalukaFk).Include(x => x.TalukaFk.DistrictFk).Include(x => x.TalukaFk.DistrictFk.StateFk).Where(e => e.Name == input.City).FirstOrDefault();
                        if (cityList != null)
                        {
                            customer.CityId = cityList.Id;
                            customer.TalukaId = cityList.TalukaId;
                            customer.DistrictId = cityList.TalukaFk.DistrictId;
                            customer.StateId = cityList.TalukaFk.DistrictFk.StateId;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "City name is invalid");
                        }
                    }
                    else
                    {
                        customer.CityId = null;
                        customer.TalukaId = null;
                        customer.DistrictId = null;
                        customer.StateId = null;
                    }

                    //Get LeadType By Name
                    if (!string.IsNullOrWhiteSpace(input.LeadType))
                    {
                        var result = _leadTypeRepository.GetAll().Where(e => e.Name == input.LeadType).FirstOrDefault();
                        if (result != null)
                        {
                            customer.LeadTypeId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Lead Type is invalid");
                        }
                    }
                    else
                    {
                        customer.LeadTypeId = 0;
                    }

                    //Get LeadSource By Name
                    if (!string.IsNullOrWhiteSpace(input.LeadSource))
                    {
                        var result = _leadSourceRepository.GetAll().Where(e => e.Name == input.LeadSource).FirstOrDefault();
                        if (result != null)
                        {
                            customer.LeadSourceId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Lead Source is invalid");
                        }
                    }
                    else
                    {
                        customer.LeadSourceId = 0;
                    }

                    //Get LeadStatus By Name
                    if (!string.IsNullOrWhiteSpace(input.LeadStatus))
                    {
                        var result = _leadStatusRepository.GetAll().Where(e => e.Status == input.LeadStatus).FirstOrDefault();
                        if (result != null)
                        {
                            customer.LeadStatusId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Lead Status is invalid");
                        }
                    }
                    else
                    {
                        customer.LeadStatusId = 0;
                    }

                    //Get SolarType By Name
                    if (!string.IsNullOrWhiteSpace(input.SolarType))
                    {
                        var result = _solarTypeRepository.GetAll().Where(e => e.Name == input.SolarType).FirstOrDefault();
                        if (result != null)
                        {
                            customer.SolarTypeId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Solar Type is invalid");
                        }
                    }
                    else
                    {
                        customer.SolarTypeId = 0;
                    }

                    //Get DiscomId By Name
                    if (!string.IsNullOrWhiteSpace(input.Discom))
                    {
                        var result = _discomRepository.GetAll().Where(e => e.Name == input.Discom).FirstOrDefault();
                        if (result != null)
                        {
                            customer.DiscomId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Discom is invalid");
                        }
                    }
                    else
                    {
                        customer.DiscomId = null;
                    }

                    //Get DivisionId By Name
                    if (!string.IsNullOrWhiteSpace(input.Division))
                    {
                        var result = _divisionRepository.GetAll().Where(e => e.Name == input.Division).FirstOrDefault();
                        if (result != null)
                        {
                            customer.DivisionId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Division is invalid");
                        }
                    }
                    else
                    {
                        customer.DivisionId = null;
                    }

                    //Get SubDivision Id By Name
                    if (!string.IsNullOrWhiteSpace(input.SubDivision))
                    {
                        var result = _subDivisionRepository.GetAll().Where(e => e.Name == input.SubDivision).FirstOrDefault();
                        if (result != null)
                        {
                            customer.SubDivisionId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "SubDivision is invalid");
                        }
                    }
                    else
                    {
                        customer.SubDivisionId = null;
                    }

                    //Get Circle Id By Name
                    if (!string.IsNullOrWhiteSpace(input.Circle))
                    {
                        var result = _circleRepository.GetAll().Where(e => e.Name == input.Circle).FirstOrDefault();
                        if (result != null)
                        {
                            customer.CircleId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Circle is invalid");
                        }
                    }
                    else
                    {
                        customer.CircleId = null;
                    }

                    //Get HeightStructure Id By Name
                    if (!string.IsNullOrWhiteSpace(input.HeightStructure))
                    {
                        var result = _heightStructureRepository.GetAll().Where(e => e.Name == input.HeightStructure).FirstOrDefault();
                        if (result != null)
                        {
                            customer.HeightStructureId = result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "HeightStructure is invalid");
                        }
                    }
                    else
                    {
                        customer.HeightStructureId = null;
                    }

                    //Get ChanelPartner Id By Name
                    if (input.ChanelPartner != "0" && input.ChanelPartner != null)
                    {
                        var result = _userRepository.GetAll().Where(e => e.Name == input.ChanelPartner).FirstOrDefault();
                        if (result != null)
                        {
                            customer.ChanelPartnerID = (int?)result.Id;
                        }
                        else
                        {
                            throw new UserFriendlyException(500, "Ooppps! There is a problem!", "Chanel Partner is invalid");
                        }
                    }
                    else
                    {
                        customer.ChanelPartnerID = null;
                    }

                    customer.IsVerified = false;
                    if (!string.IsNullOrEmpty(customer.FirstName) && !string.IsNullOrEmpty(customer.LastName) && !string.IsNullOrEmpty(customer.MobileNumber) &&
                        customer.SolarTypeId != null && customer.ConsumerNumber != null && !string.IsNullOrEmpty(customer.AddressLine1) && !string.IsNullOrEmpty(customer.AddressLine2) &&
                        customer.CityId != null && customer.TalukaId != null && customer.DistrictId != null && customer.StateId != null && customer.Pincode != null && customer.SubDivisionIdFk != null && customer.DivisionId != null &&
                        customer.CircleId != null && customer.DiscomId != null && customer.Latitude != null && customer.Longitude != null && customer.RequiredCapacity != null && customer.StrctureAmmount != null && customer.HeightStructureId != null &&
                        customer.CommonMeter != null)
                    {
                        customer.IsVerified = true;
                    }

                    //  var leadold = _leadRepository.GetAll().Where(e => e.Id == input.Id).ToList();
                    objAuditOld_Customer = _leadRepository.GetAll().Where(e => e.Id == input.Id).ToList(); //.Add(leadold);
                    objAuditNew_Customer.Add(input);
                    //objAuditNew_OnlineUserMst.add;
                    //Add Activity Log
                    LeadtrackerHistory dataVaulteActivityLog = new LeadtrackerHistory();
                    if (AbpSession.TenantId != null)
                    {
                        dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                    }
                    //dataVaulteActivityLog.SectionId = 29; // lead Pages Name
                    //dataVaulteActivityLog.ActionId = 2; // Updated
                    //dataVaulteActivityLog.ActionNote = "Lead Updated";
                    //dataVaulteActivityLog.SectionValueId = input.Id;
                    //var leadsActivityLogId = await _leadHistoryLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);



                    //DataVaults.LeadActivityLog.LeadActivityLogs leadactivity = new DataVaults.LeadActivityLog.LeadActivityLogs();
                    //leadactivity.LeadActionId = 2;
                    ////leadactivity.LeadAcitivityID = 0;
                    //leadactivity.SectionId = 29;
                    //leadactivity.LeadActionNote = "Customer Modified";
                    //leadactivity.LeadId = customer.Id;
                    //if (AbpSession.TenantId != null)
                    //{
                    //    leadactivity.TenantId = (int)AbpSession.TenantId;
                    //}
                    //var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);


                    //#region edithistory
                    //try
                    //{
                    //    object newvalue = objAuditNew_Customer;
                    //    Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                    //    if (objAuditOld_Customer != null)
                    //    {
                    //        Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_Customer), (typeof(DataTable)));
                    //    }
                    //    foreach (DataRow dr_S in Dt_Source.Rows)
                    //    {
                    //        foreach (DataColumn dc_S in Dt_Source.Columns)
                    //        {

                    //            if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                    //            {
                    //                LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                    //                objAuditInfo.FieldName = dc_S.ColumnName;
                    //                objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                    //                objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                    //                objAuditInfo.Action = "Edit";
                    //                objAuditInfo.LeadId = customer.Id;
                    //                objAuditInfo.LeadActionId = leadactid;
                    //                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                    //                List.Add(objAuditInfo);
                    //            }
                    //        }
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    throw ex;
                    //}

                    //#endregion
                    //await _dbContextProvider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
                    //await _dbContextProvider.GetDbContext().SaveChangesAsync();

                    ObjectMapper.Map(input, customer);
                    //response.IsSuccessStatusCode = true;


                }
                else
                {
                    response = new BaseResponse(HttpStatusCode.OK, true, null, "not found the Customer !");
                }

                response = new BaseResponse(HttpStatusCode.OK, true, null, "Successfully Updated the Customer !");
                //HttpStatusCode statusCode, object result = null, string errorMessage = null
            }
            catch (System.Exception ex)
            {
                response = new BaseResponse(HttpStatusCode.BadRequest, false, null, ex.Message);
                //throw new UserFriendlyException(500, "Ooppps! There is a problem!", ex.Message);
            }
            return response;
            //throw new UserFriendlyException(201, "Successfuly Insert Record", "");
        }
        //public virtual async Task<bool> NewLeadNotify(Leads input)
        //{
        //    //var User_List = _userRepository.GetAll();
        //    //var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

        //    //var managerId = await _userDetailsRepository.GetAll().Where(x => x.UserId == input.CreatorUserId).Select(x => x.ManageBy).FirstOrDefaultAsync();
        //    //var RoleName = (from user in User_List
        //    //                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
        //    //                from ur in urJoined.DefaultIfEmpty()
        //    //                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
        //    //                from us in usJoined.DefaultIfEmpty()
        //    //                where (us != null && user.Id == input.CreatorUserId)
        //    //                select (us.DisplayName));

        //    //if (RoleName.Contains("Manager"))
        //    //{
        //    //    var NotifyToUser1 = _userRepository.GetAll().Where(u => u.Id == input.ChanelPartnerID).FirstOrDefault();
        //    //    string msg1 = string.Format("New Lead Created {0}.", input.CustomerName);
        //    //    await _appNotifier.NewLead(NotifyToUser1, msg1, NotificationSeverity.Info);
        //    //}
        //    //else if (RoleName.Contains("Channel Partners"))
        //    //{
        //    //    var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == managerId).FirstOrDefault();
        //    //    string msg = string.Format("New Lead Created {0}.", input.CustomerName);
        //    //    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
        //    //}

        //    //return true;
        //}
    }
}
