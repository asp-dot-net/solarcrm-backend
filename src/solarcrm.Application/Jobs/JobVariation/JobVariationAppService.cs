﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DataVaults.Variation;
using solarcrm.Job.JobVariation;
using solarcrm.Jobs.JobVariation.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Jobs.JobVariation
{
    public class JobVariationAppService : solarcrmAppServiceBase, IJobVariationAppService
    {
        private readonly IRepository<Job.JobVariation.JobVariation> _jobVariationRepository;
        private readonly IRepository<Variation, int> _variationRepository;
        private readonly IRepository<Job.Jobs, int> _jobRepository;

        public JobVariationAppService(IRepository<Job.JobVariation.JobVariation> jobVariationRepository, IRepository<Variation, int> variationRepository, IRepository<Job.Jobs, int> jobRepository)
        {
            _jobVariationRepository = jobVariationRepository;
            _variationRepository = variationRepository;
            _jobRepository = jobRepository;
        }

		public async Task<List<GetJobVariationForEditOutput>> GetJobVariationByJobId(int jobid)
		{
           List<Job.JobVariation.JobVariation> jobVariation = _jobVariationRepository.GetAll().Where(x => x.JobId == jobid).ToList();

            List<GetJobVariationForEditOutput> output = new List<GetJobVariationForEditOutput>();

			if (jobVariation != null)
			{
                foreach (var item in jobVariation)
                {
                    var outobj = new GetJobVariationForEditOutput();
                    outobj.JobVariation = ObjectMapper.Map<CreateOrEditJobVariationDto>(item);
                    outobj.JobVariation.Id = item.Id;
                    if (outobj.JobVariation.VariationId != null)
                    {
                        var _variation = await _variationRepository.FirstOrDefaultAsync((int)outobj.JobVariation.VariationId);
                        outobj.VariationName = _variation?.Name?.ToString();
                    }
                    output.Add(outobj);
                }

            }

            return output;
		}

		//[AbpAuthorize(AppPermissions.Pages_JobVariations_Edit)]
		public async Task<GetJobVariationForEditOutput> GetJobVariationForEdit(EntityDto input)
		{
			var jobVariation = await _jobVariationRepository.FirstOrDefaultAsync(input.Id);

			var output = new GetJobVariationForEditOutput { JobVariation = ObjectMapper.Map<CreateOrEditJobVariationDto>(jobVariation) };

			if (output.JobVariation.VariationId != null)
			{
				var _lookupVariation = await _variationRepository.FirstOrDefaultAsync((int)output.JobVariation.VariationId);
				output.VariationName = _lookupVariation?.Name?.ToString();
			}

			return output;
		}

		public async Task CreateOrEdit(CreateOrEditJobVariationDto input)
		{
			if (input.Id == null)
			{
				await Create(input);
			}
			else
			{
				await Update(input);
			}
		}

		//[AbpAuthorize(AppPermissions.Pages_JobVariations_Create)]
		protected virtual async Task Create(CreateOrEditJobVariationDto input)
		{
			var jobVariation = ObjectMapper.Map<Job.JobVariation.JobVariation>(input);

			if (AbpSession.TenantId != null)
			{
				jobVariation.TenantId = (int)AbpSession.TenantId;
			}

			await _jobVariationRepository.InsertAsync(jobVariation);
		}

		//[AbpAuthorize(AppPermissions.Pages_JobVariations_Edit)]
		protected virtual async Task Update(CreateOrEditJobVariationDto input)
		{
			var jobVariation = await _jobVariationRepository.FirstOrDefaultAsync((int)input.Id);
			ObjectMapper.Map(input, jobVariation);
		}

		//[AbpAuthorize(AppPermissions.Pages_JobVariations_Delete)]
		public async Task Delete(EntityDto input)
		{
			await _jobVariationRepository.DeleteAsync(input.Id);
		}

		public async Task<List<JobVariationLookupTableDto>> GetAllVariationForTableDropdown(int Id)
		{
			var jobVariation = await _jobVariationRepository.GetAll().Where(x=>x.JobId == Id).Select(x=>x.VariationId).ToListAsync();
			return await _variationRepository.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true && !jobVariation.Contains(x.Id))
                .Select(variation => new JobVariationLookupTableDto
				{
					Id = variation.Id,
					DisplayName = variation == null || variation.Name == null ? "" : variation.Name.ToString(),
					ActionName = variation == null || variation.Action == null ? "" : variation.Action.ToString()
				}).ToListAsync();
		}
	}
}
