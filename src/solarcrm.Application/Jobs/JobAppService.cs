﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Newtonsoft.Json;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Circle;
using solarcrm.DataVaults.City;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.District;
using solarcrm.DataVaults.Division;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.State;
using solarcrm.DataVaults.StockItem;
using solarcrm.DataVaults.SubDivision;
using solarcrm.DataVaults.Taluka;
using solarcrm.EntityFrameworkCore;
using solarcrm.Jobs.Dto;
using solarcrm.Organizations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Job;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.Variation;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.DataVaults.Price;
using solarcrm.DataVaults.Price.Dto;
using Abp.Timing.Timezone;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Dispatch;
using solarcrm.JobInstallation;
using Microsoft.AspNetCore.Identity;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Jobs.Exporting;

namespace solarcrm.Jobs
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs)]
    public class JobAppService : solarcrmAppServiceBase, IJobAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<Taluka> _talukaRepository;
        private readonly IRepository<District> _districtRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendOrganizationUnitRepository;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<SubDivision> _subDivisionRepository;
        private readonly IRepository<Division> _divisionRepository;
        private readonly IRepository<Discom> _discomRepository;
        private readonly IRepository<Circle> _circleRepository;
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<StockCategory> _stockCategoryRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<Job.JobVariation.JobVariation> _jobVariationRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly IRepository<DispatchMaterialDetails> _dispatchMaterialRepository;
        private readonly IRepository<JobInstallationDetail> _installationRepository;
        private readonly IRepository<Prices> _priceRepository;
        private readonly IJobsExcelExporter _JobsExcelExporter;
        private readonly IRepository<UserDetail> _userDetailsRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public JobAppService(
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<City> cityRepository,
            IRepository<Taluka> talukaRepository,
            IRepository<District> districtRepository,
            IRepository<State> stateRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<ExtendOrganizationUnit, long> extendOrganizationUnitRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<SubDivision> subDivisionRepository,
            IRepository<Division> divisionRepository,
            IRepository<Discom> discomRepository,
            IRepository<Circle> circleRepository,
            IRepository<StockItem> stockItemRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<StockCategory> stockCategoryRepository,
            IRepository<Variation> variationRepository,
            IRepository<Job.JobVariation.JobVariation> jobVariationRepository,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus> leadStatusRepository,
            IRepository<DispatchMaterialDetails> dispatchMaterialRepository,
            IRepository<JobInstallationDetail> installationRepository,
            IRepository<Prices> priceRepository,
            IJobsExcelExporter JobsExcelExporter,
        IRepository<UserDetail> userDetailsRepository,
        ITimeZoneConverter timeZoneConverter
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _cityRepository = cityRepository;
            _talukaRepository = talukaRepository;
            _districtRepository = districtRepository;
            _stateRepository = stateRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _extendOrganizationUnitRepository = extendOrganizationUnitRepository;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _subDivisionRepository = subDivisionRepository;
            _divisionRepository = divisionRepository;
            _discomRepository = discomRepository;
            _circleRepository = circleRepository;
            _stockItemRepository = stockItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _stockCategoryRepository = stockCategoryRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _userRepository = userRepository;
            _leadStatusRepository = leadStatusRepository;
            _dispatchMaterialRepository = dispatchMaterialRepository;
            _installationRepository = installationRepository;
            _userDetailsRepository = userDetailsRepository;
            _priceRepository = priceRepository;
            _JobsExcelExporter = JobsExcelExporter;
            _timeZoneConverter = timeZoneConverter;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs)]
        public async Task<PagedResultDto<GetJobForViewDto>> GetAll(GetAllJobInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> jobnumberlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> dispatchmaterial_list = new List<int>();
            List<int> installbooking_list = new List<int>();
            List<int> installcompleted_list = new List<int>();
            List<CommonLookupDto> installername_list = new List<CommonLookupDto>();
            var User_List = _userRepository.GetAll();
            var job_list = _jobRepository.GetAll();
            var installation_list = _installationRepository.GetAll();

            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            dispatchmaterial_list = _dispatchMaterialRepository.GetAll().Where(x => x.DispatchDate != null).Select(x => x.DispatchJobId).ToList();
            if (!string.IsNullOrEmpty(input.Filter))
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            if (!string.IsNullOrEmpty(input.InstallerNameFilter))
            {
                installername_list = (from user in User_List
                                      join userDetails in _userDetailsRepository.GetAll() on user.Id equals (long)userDetails.UserId into uJoined
                                      from userDetails in uJoined.DefaultIfEmpty()
                                      where (userDetails.UserType == 3) // 3 for userType Installer
                                      select new CommonLookupDto
                                      {
                                          DisplayName = user.Name + " " + user.Surname,
                                      }).ToList();
            }
            CommonLookupDto installernamefilter = new CommonLookupDto();
            installernamefilter.DisplayName = input.InstallerNameFilter;
            if (!string.IsNullOrEmpty((input.FilterbyDate)) && input.FilterbyDate == "installbookingdate")
            {
                installbooking_list = installation_list.Where(x => x.InstallStartDate != null).Select(x => x.JobId).ToList();
            }
            if (!string.IsNullOrEmpty((input.FilterbyDate)) && input.FilterbyDate == "installcompletedate")
            {
                installcompleted_list = installation_list.Where(x => x.InstallCompleteDate != null).Select(x => x.JobId).ToList();
            }
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "nextfollowupdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            //var filteredJob = _jobRepository.GetAll().Include(x => x.LeadFK).Include(x => x.ApplicationStatusIdFK).WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter));
            var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                || e.LeadFK.Alt_Phone.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.LeadId)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                            .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.ApplicationStatusId))
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId)) // discomlist filter
                            .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId)) // circlelist filter
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId)) //devisionlist filter
                            .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId)) //subdivisiionlist filter
                            .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId)) //solartypelist filter
                            .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.LeadFK.ChanelPartnerID)) //employee with CPlist filter
                            .WhereIf(input.DistrictIdFilter != null && input.DistrictIdFilter.Count() > 0, e => input.DistrictIdFilter.Contains((int)e.LeadFK.DistrictId)) // districelist filter
                            .WhereIf(input.TalukaIdFilter != null && input.TalukaIdFilter.Count() > 0, e => input.TalukaIdFilter.Contains((int)e.LeadFK.TalukaId))
                            .WhereIf(input.CityIdFilter != null && input.CityIdFilter.Count() > 0, e => input.CityIdFilter.Contains((int)e.LeadFK.CityId))
                            .WhereIf(input.InstallerNameFilter != null && installernamefilter.DisplayName != null, e => installername_list.Contains(installernamefilter))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "projectopened", e => e.LeadFK.LeadStatusId == 1)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "quoteaccepted", e => e.EastimateQuoteNumber != null)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "depositreceived", e => e.DepositRecivedDate != null)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "installbookingdate", e => installbooking_list.Contains(e.Id))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "installcompletedate", e => installcompleted_list.Contains(e.Id))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "dispatchdate", e => dispatchmaterial_list.Contains(e.Id))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "applicaitonsubmitdate", e => e.ApplicationDate != null)
                            .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit);

            var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobs = from o in pagedAndFilteredJob
                       select new GetJobForViewDto()
                       {
                           Job = new JobDto
                           {
                               Id = o.Id,
                               LeadId = o.LeadId,
                               leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                               ProjectNumber = o.JobNumber,
                               CustomerName = o.LeadFK.CustomerName,
                               Address = (o.LeadFK.AddressLine1 == null ? "" : o.LeadFK.AddressLine1 + ", ") + (o.LeadFK.AddressLine2 == null ? "" : o.LeadFK.AddressLine2 + ", ") + (o.LeadFK.CityIdFk.Name == null ? "" : o.LeadFK.CityIdFk.Name + ", ") + (o.LeadFK.TalukaIdFk.Name == null ? "" : o.LeadFK.TalukaIdFk.Name + ", ") + (o.LeadFK.DiscomIdFk.Name == null ? "" : o.LeadFK.DiscomIdFk.Name + ", ") + (o.LeadFK.StateIdFk.Name == null ? "" : o.LeadFK.StateIdFk.Name + "-") + (o.LeadFK.Pincode == null ? "" : o.LeadFK.Pincode),
                               Mobile = o.LeadFK.MobileNumber,
                               SubDivision = o.LeadFK.SubDivisionIdFk.Name,
                               //JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                               FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                               FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                               PanelyDays = o.PenaltyDaysPending,
                               LeadStatus = o.LeadFK.LeadStatusIdFk.Status,
                               AssignUserName = User_List.Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                               LeadCreatedName = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                               //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                               LeadStatusColorClass = o.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                               LeadStatusIconClass = o.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                               Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                               ApplicationStatusdata = o.ApplicationStatusIdFK,

                           },
                           jobSummaryCount = JobSummaryCount(filteredJob.ToList()),
                       };

            var totalCount = await filteredJob.CountAsync();

            return new PagedResultDto<GetJobForViewDto>(totalCount, await jobs.ToListAsync());
        }

        public jobSummaryCount JobSummaryCount(List<Job.Jobs> filteredJobs)
        {
            var output = new jobSummaryCount();
            if (filteredJobs.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);
                output.TotalJob = Convert.ToInt16(filteredJobs.Count());
                output.Kilowatt = Convert.ToInt16(filteredJobs.Sum(e => e.PvCapacityKw));
                output.TotalPanel = Convert.ToInt16(panelCount);
                                                                                                                                                  //output.AvgStructureamount = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 3).Count());
                                                                                                                                                  //output.AvgPricePerkilowatt = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 4).Count());
                                                                                                                                                  //output.DocumentReceived = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 6).Count());
                                                                                                                                                  //output.DepositeReceived = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 7).Count());
                                                                                                                                                  //output.ReadyDispatch = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 8).Count());
                                                                                                                                                  //output.InstalltionBooked = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 9).Count());
                                                                                                                                                  //output.InstalltionCompleted = Convert.ToString(filteredJobs.Count());
                                                                                                                                                  //output.SubcidyClaimed = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 10).Count());
                                                                                                                                                  //output.SubcidyReceived = Convert.ToString(filteredJobs.Where(e => e.LeadStatusId == 11).Count());
                                                                                                                                                  //output.Sold = Convert.ToString(filteredLeads.Where(e => LeadidList.Contains(e.Id)).Count());
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Sales_Edit)]
        public async Task<GetJobForEditOutput> GetJobForEdit(EntityDto input)
        {
            var leads = _leadRepository.GetAll().Include(x => x.SolarTypeIdFk).Where(x => x.Id == input.Id).FirstOrDefault(); //await _leadRepository.FirstOrDefaultAsync(input.Id);
            var jobdata = await _jobRepository.FirstOrDefaultAsync(e => e.LeadId == input.Id);

            var output = new GetJobForEditOutput
            {
                Job = jobdata == null ? new CreateOrEditJobDto() : ObjectMapper.Map<CreateOrEditJobDto>(jobdata),

                JobType = leads.SolarTypeId,
                AddressLine1 = leads.AddressLine1,
                AddressLine2 = leads.AddressLine2,
                City = leads.CityId != null ? _cityRepository.FirstOrDefaultAsync((int)leads.CityId).Result.Name : "",
                Taluka = leads.TalukaId != null ? _talukaRepository.FirstOrDefaultAsync((int)leads.TalukaId).Result.Name : "",
                District = leads.DistrictId != null ? _districtRepository.FirstOrDefaultAsync((int)leads.DistrictId).Result.Name : "",
                State = leads.StateId != null ? _stateRepository.FirstOrDefaultAsync((int)leads.StateId).Result.Name : "",
                Pincode = leads.Pincode,
                StrctureAmmount = leads.StrctureAmmount,
                ConsumerNumber = leads.ConsumerNumber,
                SubDivision = leads.SubDivisionId != null ? _subDivisionRepository.FirstOrDefaultAsync((int)leads.SubDivisionId).Result.Name : "",
                Division = leads.DivisionId != null ? _divisionRepository.FirstOrDefaultAsync((int)leads.DivisionId).Result.Name : "",
                Discom = leads.DiscomId != null ? _discomRepository.FirstOrDefaultAsync((int)leads.DiscomId).Result.Name : "",
                Circle = leads.CircleId != null ? _circleRepository.FirstOrDefaultAsync((int)leads.CircleId).Result.Name : "",

                LeadStatus = leads.LeadStatusId != 0 ? _leadStatusRepository.FirstOrDefaultAsync((int)leads.LeadStatusId).Result.Status : "",
                LeadStatusColorClass = leads.LeadStatusId != 0 ? _leadStatusRepository.FirstOrDefaultAsync((int)leads.LeadStatusId).Result.LeadStatusColorClass : "",
                LeadStatusIconClass = leads.LeadStatusId != 0 ? _leadStatusRepository.FirstOrDefaultAsync((int)leads.LeadStatusId).Result.LeadStatusIconClass : "",
                AreaType = leads.AreaType,
                IsManualPricingSubcidy = leads.SolarTypeIdFk.IsManualPricing
            };

            //GetJobForEditOutput output = new GetJobForEditOutput();
            //output.AddressLine1 = leads.AddressLine1;
            //output.AddressLine2 = leads.AddressLine2;
            //output.City = leads.CityId != null ? _cityRepository.FirstOrDefaultAsync((int)leads.CityId).Result.Name : "";
            //output.Taluka = leads.TalukaId != null ? _talukaRepository.FirstOrDefaultAsync((int)leads.TalukaId).Result.Name : "";
            //output.District = leads.DistrictId != null ? _districtRepository.FirstOrDefaultAsync((int)leads.DistrictId).Result.Name : "";
            //output.State = leads.StateId != null ? _stateRepository.FirstOrDefaultAsync((int)leads.StateId).Result.Name : "";
            //output.Pincode = leads.Pincode;

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Sales_Create)]
        public async Task CreateOrEdit(CreateOrEditJobDto input, int SectionId)
        {
            if (input.Id == 0)
            {
                await Create(input, SectionId);
            }
            else
            {
                await Update(input, SectionId);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Sales_Create)]
        protected virtual async Task Create(CreateOrEditJobDto input, int SectionId)
        {
            var Lead = _leadRepository.GetAll().Where(e => e.Id == input.LeadId).FirstOrDefault();
            Lead.LeadStatusId = 2; //Project Created

            await _leadRepository.UpdateAsync(Lead);

            var OrgDetails = _extendOrganizationUnitRepository.GetAll().Where(e => e.Id == Lead.OrganizationUnitId).Select(e => new { e.Organization_ProjectId, e.OrganizationCode }).FirstOrDefault();

            var PrevJobNumber = (from j in _jobRepository.GetAll()
                                 where (j.OrganizationUnitId == Lead.OrganizationUnitId)
                                 select new { j.JobNumber, j.Id })
                                .OrderByDescending(e => e.Id).FirstOrDefault();

            var JobNumber = OrgDetails.OrganizationCode + OrgDetails.Organization_ProjectId;
            if (PrevJobNumber != null)
            {
                if (PrevJobNumber.JobNumber != null)
                {
                    var JobNumberSplit = PrevJobNumber.JobNumber.Substring(OrgDetails.OrganizationCode.Length);
                    JobNumber = OrgDetails.OrganizationCode + Convert.ToString(Convert.ToInt32(JobNumberSplit) + 1);
                }
            }

            var job = ObjectMapper.Map<Job.Jobs>(input);

            job.JobNumber = JobNumber;

            job.OrganizationUnitId = Lead.OrganizationUnitId;

            if (AbpSession.TenantId != null)
            {
                job.TenantId = (int)AbpSession.TenantId;
            }

            var JobId = await _jobRepository.InsertAndGetIdAsync(job);

            //Add Activity Log
            LeadActivityLogs leadactivity = new LeadActivityLogs();
            leadactivity.LeadActionId = 10;
            //leadactivity.LeadAcitivityID = 0; // confusion
            leadactivity.SectionId = SectionId;
            leadactivity.LeadActionNote = "Job Created";
            leadactivity.LeadId = job.LeadId;
            leadactivity.OrganizationUnitId = job.OrganizationUnitId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Sales_Edit)]
        protected virtual async Task Update(CreateOrEditJobDto input, int SectionId)
        {
            var job = await _jobRepository.FirstOrDefaultAsync((int)input.Id);

            #region Activity Log
            //Add Activity Log
            LeadActivityLogs leadactivity = new LeadActivityLogs();
            leadactivity.LeadActionId = 11;
            //leadactivity.LeadAcitivityID = 0;
            leadactivity.SectionId = SectionId;
            leadactivity.LeadActionNote = "Job Modified";
            leadactivity.LeadId = job.LeadId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);
            var List = new List<LeadtrackerHistory>();

            try
            {
                List<Job.Jobs> objAuditOld_OnlineUserMst = new List<Job.Jobs>();
                List<CreateOrEditJobDto> objAuditNew_OnlineUserMst = new List<CreateOrEditJobDto>();

                DataTable Dt_Target = new DataTable();
                DataTable Dt_Source = new DataTable();



                objAuditOld_OnlineUserMst.Add(job);
                objAuditNew_OnlineUserMst.Add(input);

                //Add Activity Log
                LeadtrackerHistory leadActivityLog = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    leadActivityLog.TenantId = (int)AbpSession.TenantId;
                }

                object newvalue = objAuditNew_OnlineUserMst;
                Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                if (objAuditOld_OnlineUserMst != null)
                {
                    Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                }
                foreach (DataRow dr_S in Dt_Source.Rows)
                {
                    foreach (DataColumn dc_S in Dt_Source.Columns)
                    {

                        if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                        {
                            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                            objAuditInfo.FieldName = dc_S.ColumnName;
                            objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                            objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                            //objAuditInfo.Action = "Edit";
                            objAuditInfo.Action = Dt_Target.Rows[0][dc_S.ColumnName].ToString() == "" ? "Add" : "Edit";
                            objAuditInfo.LeadId = job.LeadId;
                            objAuditInfo.LeadActionId = leadactid;
                            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            List.Add(objAuditInfo);
                        }
                    }
                }

            }
            catch
            {
            }
            #endregion

            #region JobProduct Details
            if (input.JobProductItems != null)
            {
                var ProductItemList = _stockItemRepository.GetAll();
                var ProductType = _stockCategoryRepository.GetAll();

                var IdList = input.JobProductItems.Where(x => x.ProductItemId != 0).Select(e => e.Id).ToList();
                var existingData = _jobProductItemRepository.GetAll().AsNoTracking().Where(x => x.JobId == job.Id).ToList();

                if (existingData != null)
                {
                    foreach (var item in existingData)
                    {
                        if (IdList != null)
                        {
                            bool containsItem = IdList.Any(x => x == item.Id);
                            if (containsItem == false)
                            {
                                LeadtrackerHistory jobhistory1 = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory1.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory1.FieldName = "Product Name";
                                jobhistory1.PrevValue = ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.Name).FirstOrDefault();
                                jobhistory1.CurValue = "Deleted";
                                jobhistory1.Action = "Products Delete";
                                jobhistory1.LastModificationTime = DateTime.Now;
                                jobhistory1.LeadId = job.LeadId;
                                jobhistory1.LeadActionId = leadactid;
                                List.Add(jobhistory1);

                                LeadtrackerHistory jobhistory2 = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory2.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory2.FieldName = "Product Model";
                                jobhistory2.PrevValue = ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.Model).FirstOrDefault();
                                jobhistory2.CurValue = "Deleted";
                                jobhistory2.Action = "Products Delete";
                                jobhistory2.LastModificationTime = DateTime.Now;
                                jobhistory2.LeadId = job.LeadId;
                                jobhistory2.LeadActionId = leadactid;
                                List.Add(jobhistory2);

                                LeadtrackerHistory jobhistory3 = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory3.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory3.FieldName = "Product Quantity";
                                jobhistory3.PrevValue = item.Quantity.ToString();
                                jobhistory3.CurValue = "Deleted";
                                jobhistory3.Action = "Products Delete";
                                jobhistory3.LastModificationTime = DateTime.Now;
                                jobhistory3.LeadId = job.LeadId;
                                jobhistory3.LeadActionId = leadactid;
                                List.Add(jobhistory3);

                                LeadtrackerHistory jobhistory4 = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory2.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory4.FieldName = "Product Type";
                                jobhistory4.PrevValue = ProductType.Where(x => x.Id == (ProductItemList.Where(x => x.Id == item.ProductItemId).Select(x => x.CategoryId).FirstOrDefault())).Select(x => x.Name).FirstOrDefault();
                                jobhistory4.CurValue = "Deleted";
                                jobhistory4.Action = "Products Delete";
                                jobhistory4.LastModificationTime = DateTime.Now;
                                jobhistory4.LeadId = job.LeadId;
                                jobhistory4.LeadActionId = leadactid;
                                List.Add(jobhistory4);
                                _jobProductItemRepository.Delete(x => x.Id == item.Id);
                            }
                        }
                    }
                }

                foreach (var jobproduct in input.JobProductItems)
                {
                    if (jobproduct.ProductItemId != 0 && jobproduct.Quantity != 0)
                    {
                        if (jobproduct.Id != null && jobproduct.Id != 0)
                        {
                            var existData = _jobProductItemRepository.GetAll().AsNoTracking().Where(x => x.Id == jobproduct.Id).FirstOrDefault();
                            var olddetail = ObjectMapper.Map<JobProductItem>(jobproduct);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }

                            _jobProductItemRepository.Update(olddetail);

                            if (jobproduct.ProductItemId != null && jobproduct.ProductItemId != 0 && existData.ProductItemId != jobproduct.ProductItemId)
                            {
                                var existProductName = ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.Name).FirstOrDefault();
                                var currentProductName = ProductItemList.Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Name).FirstOrDefault();

                                var existModel = ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.Model).FirstOrDefault();
                                var currentModel = ProductItemList.Where(x => x.Id == jobproduct.ProductItemId).Select(x => x.Model).FirstOrDefault();

                                var existProductType = ProductType.Where(x => x.Id == (ProductItemList.Where(x => x.Id == existData.ProductItemId).Select(x => x.CategoryId).FirstOrDefault())).Select(x => x.Name).FirstOrDefault();
                                var currentProductType = ProductType.Where(x => x.Id == jobproduct.ProductTypeId).Select(x => x.Name).FirstOrDefault();

                                if (existProductName != currentProductName)
                                {
                                    LeadtrackerHistory jobhistory1 = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory1.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory1.FieldName = "Product Name";
                                    jobhistory1.PrevValue = existProductName;
                                    jobhistory1.CurValue = currentProductName;
                                    jobhistory1.Action = "Products Edit";
                                    jobhistory1.LastModificationTime = DateTime.Now;
                                    jobhistory1.LeadId = job.LeadId;
                                    jobhistory1.LeadActionId = leadactid;
                                    List.Add(jobhistory1);
                                }

                                if (existModel != currentModel)
                                {
                                    LeadtrackerHistory jobhistory2 = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory2.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory2.FieldName = "Product Model";
                                    jobhistory2.PrevValue = existModel;
                                    jobhistory2.CurValue = currentModel;
                                    jobhistory2.Action = "Products Edit";
                                    jobhistory2.LastModificationTime = DateTime.Now;
                                    jobhistory2.LeadId = job.LeadId;
                                    jobhistory2.LeadActionId = leadactid;
                                    List.Add(jobhistory2);
                                }

                                if (existProductType != currentProductType)
                                {
                                    LeadtrackerHistory jobhistory3 = new LeadtrackerHistory();
                                    if (AbpSession.TenantId != null)
                                    {
                                        jobhistory3.TenantId = (int)AbpSession.TenantId;
                                    }
                                    jobhistory3.FieldName = "Product Type";
                                    jobhistory3.PrevValue = existProductType;
                                    jobhistory3.CurValue = currentProductType;
                                    jobhistory3.Action = "Products Edit";
                                    jobhistory3.LastModificationTime = DateTime.Now;
                                    jobhistory3.LeadId = job.LeadId;
                                    jobhistory3.LeadActionId = leadactid;
                                    List.Add(jobhistory3);
                                }
                            }

                            if (jobproduct.Quantity != null && existData.Quantity != jobproduct.Quantity)
                            {
                                LeadtrackerHistory jobhistory = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    jobhistory.TenantId = (int)AbpSession.TenantId;
                                }
                                jobhistory.FieldName = "Product Item Quantity";
                                jobhistory.PrevValue = existData.Quantity.ToString();
                                jobhistory.CurValue = jobproduct.Quantity.ToString();
                                jobhistory.Action = "Products Edit";
                                jobhistory.LastModificationTime = DateTime.Now;
                                jobhistory.LeadId = job.LeadId;
                                jobhistory.LeadActionId = leadactid;
                                List.Add(jobhistory);
                            }
                        }
                        else
                        {
                            var olddetail = ObjectMapper.Map<JobProductItem>(jobproduct);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }
                            _jobProductItemRepository.Insert(olddetail);
                        }
                    }

                }
            }
            #endregion

            #region Job Variation Details
            if (input.JobVariation != null)
            {
                var variationList = _variationRepository.GetAll();
                var IdList = input.JobVariation.Select(e => e.Id).ToList();
                var existingData = _jobVariationRepository.GetAll().AsNoTracking().Where(x => x.JobId == job.Id).ToList();

                if (existingData != null)
                {
                    foreach (var item in existingData)
                    {
                        if (IdList != null)
                        {
                            bool containsItem = IdList.Any(x => x == item.Id);
                            if (containsItem == false)
                            {
                                LeadtrackerHistory leadHistory1 = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    leadHistory1.TenantId = (int)AbpSession.TenantId;
                                }
                                leadHistory1.FieldName = "Variation Name";
                                leadHistory1.PrevValue = variationList.Where(x => x.Id == item.VariationId).Select(x => x.Name).FirstOrDefault();
                                leadHistory1.CurValue = "Deleted";
                                leadHistory1.Action = "Varition Delete";
                                leadHistory1.LastModificationTime = DateTime.Now;
                                leadHistory1.LeadId = job.LeadId;
                                leadHistory1.LeadActionId = leadactid;
                                List.Add(leadHistory1);

                                LeadtrackerHistory leadHistory = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    leadHistory.TenantId = (int)AbpSession.TenantId;
                                }
                                leadHistory.FieldName = "Variation Cost";
                                leadHistory.PrevValue = item.Cost.ToString();
                                leadHistory.CurValue = "Deleted";
                                leadHistory.Action = "Varition Delete";
                                leadHistory.LastModificationTime = DateTime.Now;
                                leadHistory.LeadId = job.LeadId;
                                leadHistory.LeadActionId = leadactid;
                                List.Add(leadHistory);

                                _jobVariationRepository.Delete(x => x.Id == item.Id);
                            }
                        }
                    }
                }

                foreach (var jobvariation in input.JobVariation)
                {
                    if (jobvariation.Id != null && jobvariation.Id != 0)
                    {
                        var existData = _jobVariationRepository.GetAll().AsNoTracking().Where(x => x.Id == jobvariation.Id).FirstOrDefault();
                        var olddetail = ObjectMapper.Map<Job.JobVariation.JobVariation>(jobvariation);
                        olddetail.JobId = job.Id;
                        if (AbpSession.TenantId != null)
                        {
                            olddetail.TenantId = (int)AbpSession.TenantId;
                        }

                        _jobVariationRepository.Update(olddetail);

                        if (jobvariation.VariationId != null && jobvariation.VariationId != 0 && existData.VariationId != jobvariation.VariationId)
                        {
                            var existVariation = variationList.Where(x => x.Id == existData.VariationId).Select(x => x.Name).FirstOrDefault();
                            var currentVariation = variationList.Where(x => x.Id == jobvariation.VariationId).Select(x => x.Name).FirstOrDefault();

                            if (existVariation != currentVariation)
                            {
                                LeadtrackerHistory leadHistory1 = new LeadtrackerHistory();
                                if (AbpSession.TenantId != null)
                                {
                                    leadHistory1.TenantId = (int)AbpSession.TenantId;
                                }
                                leadHistory1.FieldName = "Variation Name";
                                leadHistory1.PrevValue = existVariation;
                                leadHistory1.CurValue = currentVariation;
                                leadHistory1.Action = "Varition Edit";
                                leadHistory1.LastModificationTime = DateTime.Now;
                                leadHistory1.LeadId = job.LeadId;
                                leadHistory1.LeadActionId = leadactid;
                                List.Add(leadHistory1);
                            }
                        }

                        if (jobvariation.Cost != null && existData.Cost != jobvariation.Cost)
                        {
                            LeadtrackerHistory leadHistory = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                leadHistory.TenantId = (int)AbpSession.TenantId;
                            }
                            leadHistory.FieldName = "Variation Cost";
                            leadHistory.PrevValue = existData.Cost.ToString();
                            leadHistory.CurValue = jobvariation.Cost.ToString();
                            leadHistory.Action = "Varition Edit";
                            leadHistory.LastModificationTime = DateTime.Now;
                            leadHistory.LeadId = job.LeadId;
                            leadHistory.LeadActionId = leadactid;
                            List.Add(leadHistory);
                        }
                    }
                    else
                    {
                        if (jobvariation.VariationId > 0)
                        {
                            var olddetail = ObjectMapper.Map<Job.JobVariation.JobVariation>(jobvariation);
                            olddetail.JobId = job.Id;
                            if (AbpSession.TenantId != null)
                            {
                                olddetail.TenantId = (int)AbpSession.TenantId;
                            }
                            _jobVariationRepository.Insert(olddetail);
                        }
                    }
                }
            }
            #endregion

            if (List.Count > 0)
            {
                await _dbContextProvider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
                await _dbContextProvider.GetDbContext().SaveChangesAsync();
            }

            ObjectMapper.Map(input, job);
        }

        public async Task<List<string>> GetProductItemList(int productTypeId, string productItem)
        {
            //var warehouseid = _warehouselocationRepository.GetAll().Where(e => e.state == state).Select(e => e.Id).FirstOrDefault();
            //var productiteamids = _productiteamlocationRepository.GetAll().Where(e => e.WarehouselocationId == warehouseid && e.SalesTag == true).Select(e => e.ProductItemId).Distinct().ToList();
            //var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);

            //if (!role.Contains("Admin"))
            //{
            //    return await _productItemRepository.GetAll()
            //.WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
            //.Where(e => e.ProductTypeId == productTypeId && e.Active == true && productiteamids.Contains(e.Id))
            //.Select(productItems => productItems == null || productItems.Name == null ? "" : productItems.Name.ToString()).ToListAsync();
            //}
            //else
            //{

            var productItemsList = await _stockItemRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(productItem), c => c.Name.ToLower().Contains(productItem.ToLower()))
                            .Where(e => e.CategoryId == productTypeId && e.Active == true)
                            .Select(productItems => productItems == null || productItems.Name == null ? "" : productItems.Name.ToString()).ToListAsync();

            return productItemsList;

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Sales_ExportToExcel)]
        public async Task<FileDto> GetJobsToExcel(GetAllJobInput input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
                List<int> jobnumberlist = new List<int>();
                List<int> FollowupList = new List<int>();
                List<int> NextFollowupList = new List<int>();
                List<int> dispatchmaterial_list = new List<int>();
                List<int> installbooking_list = new List<int>();
                List<int> installcompleted_list = new List<int>();
                List<CommonLookupDto> installername_list = new List<CommonLookupDto>();
                var User_List = _userRepository.GetAll();
                var job_list = _jobRepository.GetAll();
                var installation_list = _installationRepository.GetAll();

                var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
                dispatchmaterial_list = _dispatchMaterialRepository.GetAll().Where(x => x.DispatchDate != null).Select(x => x.DispatchJobId).ToList();
                if (!string.IsNullOrEmpty(input.Filter))
                {
                    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                }
                if (!string.IsNullOrEmpty(input.InstallerNameFilter))
                {
                    installername_list = (from user in User_List
                                          join userDetails in _userDetailsRepository.GetAll() on user.Id equals (long)userDetails.UserId into uJoined
                                          from userDetails in uJoined.DefaultIfEmpty()
                                          where (userDetails.UserType == 3) // 3 for userType Installer
                                          select new CommonLookupDto
                                          {
                                              DisplayName = user.Name + " " + user.Surname,
                                          }).ToList();
                }
                CommonLookupDto installernamefilter = new CommonLookupDto();
                installernamefilter.DisplayName = input.InstallerNameFilter;
                if (input.FilterbyDate == "installbookingdate")
                {
                    installbooking_list = installation_list.Where(x => x.InstallStartDate != null).Select(x => x.JobId).ToList();
                }
                if (input.FilterbyDate == "installcompletedate")
                {
                    installcompleted_list = installation_list.Where(x => x.InstallCompleteDate != null).Select(x => x.JobId).ToList();
                }
                if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "nextfollowupdate")
                {
                    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                }
                //var filteredJob = _jobRepository.GetAll().Include(x => x.LeadFK).Include(x => x.ApplicationStatusIdFK).WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobNumber.Contains(input.Filter));
                var filteredJob = _jobRepository.GetAll().Include(e => e.LeadFK)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobNumber.Contains(input.Filter)
                                    || e.LeadFK.CustomerName.Contains(input.Filter) || e.LeadFK.EmailId.Contains(input.Filter)
                                    || e.LeadFK.Alt_Phone.Contains(input.Filter) || e.LeadFK.MobileNumber.Contains(input.Filter)
                                    //|| e.LeadFK.ConsumerNumber == Convert.ToInt64(input.Filter)
                                    //|| e.LeadFK.AddressLine1.Contains(input.Filter) || e.LeadFK.AddressLine2.Contains(input.Filter) 
                                    || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.LeadId)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                                .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadFK.LeadStatusId))
                                .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.ApplicationStatusId))
                                .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.LeadFK.DiscomId)) // discomlist filter
                                .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.LeadFK.CircleId)) // circlelist filter
                                .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.LeadFK.DivisionId)) //devisionlist filter
                                .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.LeadFK.SubDivisionId)) //subdivisiionlist filter
                                .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.LeadFK.SolarTypeId)) //solartypelist filter
                                .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.LeadFK.ChanelPartnerID)) //employee with CPlist filter
                                .WhereIf(input.DistrictIdFilter != null && input.DistrictIdFilter.Count() > 0, e => input.DistrictIdFilter.Contains((int)e.LeadFK.DistrictId)) // districelist filter
                                .WhereIf(input.TalukaIdFilter != null && input.TalukaIdFilter.Count() > 0, e => input.TalukaIdFilter.Contains((int)e.LeadFK.TalukaId))
                                .WhereIf(input.CircleIdFilter != null && input.CityIdFilter.Count() > 0, e => input.CityIdFilter.Contains((int)e.LeadFK.CityId))
                                .WhereIf(input.InstallerNameFilter != null && installernamefilter.DisplayName != null, e => installername_list.Contains(installernamefilter))
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "projectopened", e => e.LeadFK.LeadStatusId == 1)
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "quoteaccepted", e => e.EastimateQuoteNumber != null)
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "depositreceived", e => e.DepositRecivedDate != null)
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "installbookingdate", e => installbooking_list.Contains(e.Id))
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "installcompletedate", e => installcompleted_list.Contains(e.Id))
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "dispatchdate", e => dispatchmaterial_list.Contains(e.Id))
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "applicaitonsubmitdate", e => e.ApplicationDate != null)
                                .Where(e => e.LeadFK.OrganizationUnitId == input.OrganizationUnit);

                var pagedAndFilteredJob = filteredJob.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                var jobs = from o in pagedAndFilteredJob
                           select new GetJobForViewDto()
                           {
                               Job = new JobDto
                               {
                                   Id = o.Id,
                                   LeadId = o.LeadId,
                                   leaddata = ObjectMapper.Map<LeadsDto>(o.LeadFK),
                                   ProjectNumber = o.JobNumber,
                                   CustomerName = o.LeadFK.CustomerName,
                                   Address = (o.LeadFK.AddressLine1 == null ? "" : o.LeadFK.AddressLine1 + ", ") + (o.LeadFK.AddressLine2 == null ? "" : o.LeadFK.AddressLine2 + ", ") + (o.LeadFK.CityIdFk.Name == null ? "" : o.LeadFK.CityIdFk.Name + ", ") + (o.LeadFK.TalukaIdFk.Name == null ? "" : o.LeadFK.TalukaIdFk.Name + ", ") + (o.LeadFK.DiscomIdFk.Name == null ? "" : o.LeadFK.DiscomIdFk.Name + ", ") + (o.LeadFK.StateIdFk.Name == null ? "" : o.LeadFK.StateIdFk.Name + "-") + (o.LeadFK.Pincode == null ? "" : o.LeadFK.Pincode),
                                   Mobile = o.LeadFK.MobileNumber,
                                   SubDivision = o.LeadFK.SubDivisionIdFk.Name,
                                   //JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                   FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                   FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                   PanelyDays = o.PenaltyDaysPending,
                                   LeadStatus = o.LeadFK.LeadStatusIdFk.Status,
                                   AssignUserName = User_List.Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                   LeadCreatedName = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                   //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                   LeadStatusColorClass = o.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                   LeadStatusIconClass = o.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                   Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                   ApplicationStatusdata = o.ApplicationStatusIdFK,
                               }
                           };
                var jobsListDtos = await jobs.ToListAsync();
                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _JobsExcelExporter.ExportToFile(jobsListDtos);
                }
                else
                {
                    return _JobsExcelExporter.ExportToFile(jobsListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
