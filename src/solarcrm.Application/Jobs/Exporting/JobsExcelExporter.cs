﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Jobs.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Jobs.Exporting
{
    public class JobsExcelExporter : NpoiExcelExporterBase, IJobsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetJobForViewDto> jobsExport)
        {
            return CreateExcelPackage(
                "Jobs.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Jobs"));

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        //L("LeadStatus"),
                        L("ApplicationStatus"),
                        L("CustomerName"),
                        L("Address"),
                        L("Mobile"),
                        L("ActivityDescription"),
                        L("ReminderTime"),
                        //L("QueryDescription"),
                        L("Comment")
                        );
                    AddObjects(
                        sheet, jobsExport,
                        _ => _.Job.ProjectNumber,
                        //_ => _.Job.LeadStatusName,
                        _ => _.Job.ApplicationStatusdata,
                        _ => _.Job.CustomerName,
                        _ => _.Job.Address,
                        _ => _.Job.Mobile,
                        _ => _.Job.FollowDiscription,
                        _ => _.Job.FollowDate,
                        //_ => _.Job.QuesryDescription,
                        _ => _.Job.Comment
                        );
                });
        }
    }
}
