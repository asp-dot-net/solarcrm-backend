﻿using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using solarcrm.Jobs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Jobs.Exporting
{
    public interface IJobsExcelExporter
    {
        FileDto ExportToFile(List<GetJobForViewDto> jobExport);
    }
}
