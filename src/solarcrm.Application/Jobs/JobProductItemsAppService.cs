﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.StockItem;
using solarcrm.Job;
using solarcrm.Jobs.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;

namespace solarcrm.Jobs
{
    public class JobProductItemsAppService : solarcrmAppServiceBase, IJobProductItemsAppService
    {
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<StockItem> _stockItemRepository;

        public JobProductItemsAppService(IRepository<JobProductItem> jobProductItemRepository, IRepository<Job.Jobs> jobRepository, IRepository<StockItem> stockItemRepository)
        {
            _jobProductItemRepository = jobProductItemRepository;
            _jobRepository = jobRepository;
            _stockItemRepository = stockItemRepository;
        }

        public async Task<List<GetJobProductItemForEditOutput>> GetJobProductItemByJobId(int jobid)
        {
            List<GetJobProductItemForEditOutput> output = new List<GetJobProductItemForEditOutput>();
            var jobProductItem = _jobProductItemRepository.GetAll().Where(x => x.JobId == jobid).ToList();

            if (jobProductItem != null)
            {
                foreach (var item in jobProductItem)
                {
                    GetJobProductItemForEditOutput outobj = new GetJobProductItemForEditOutput();
                    outobj.JobProductItem = ObjectMapper.Map<CreateOrEditJobProductItemDto>(item);

                    if (outobj.JobProductItem.ProductItemId != null)
                    {
                        var _lookupProductItem = await _stockItemRepository.GetAll().Include(x => x.StockCategoryFk).Where(x => x.Id == (int)outobj.JobProductItem.ProductItemId).FirstOrDefaultAsync();// .FirstOrDefaultAsync((int)outobj.JobProductItem.ProductItemId);
                        outobj.ProductItemName = _lookupProductItem?.Name?.ToString();
                        outobj.JobProductItem.ProductTypeId = _lookupProductItem?.CategoryId;
                        outobj.ProductTypeName = _lookupProductItem.StockCategoryFk.Name;
                        outobj.Size = _lookupProductItem?.Size;
                        outobj.Model = _lookupProductItem?.Model?.ToString();
                        outobj.ID = item.Id;
                    }
                    output.Add(outobj);
                }
            }
            return output;
        }

        public async Task<PagedResultDto<GetJobProductItemForViewDto>> GetAll(GetAllJobProductItemsInput input)
        {
            var filteredJobProductItems = _jobProductItemRepository.GetAll()
                        .Include(e => e.JobFk)
                        .Include(e => e.ProductItemFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ProductItemNameFilter), e => e.ProductItemFk != null && e.ProductItemFk.Name == input.ProductItemNameFilter);

            var pagedAndFilteredJobProductItems = filteredJobProductItems
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var jobProductItems = from o in pagedAndFilteredJobProductItems
                                  join o1 in _jobRepository.GetAll() on o.JobId equals o1.Id into j1
                                  from s1 in j1.DefaultIfEmpty()

                                  join o2 in _stockItemRepository.GetAll() on o.ProductItemId equals o2.Id into j2
                                  from s2 in j2.DefaultIfEmpty()

                                  select new GetJobProductItemForViewDto()
                                  {
                                      JobProductItem = new JobProductItemDto
                                      {
                                          Id = o.Id
                                      },
                                      ProductItemName = s2 == null || s2.Name == null ? "" : s2.Name.ToString()
                                  };

            var totalCount = await filteredJobProductItems.CountAsync();

            return new PagedResultDto<GetJobProductItemForViewDto>(
                totalCount,
                await jobProductItems.ToListAsync()
            );
        }

        //[AbpAuthorize(AppPermissions.Pages_JobProductItems_Edit)]
        public async Task<GetJobProductItemForEditOutput> GetJobProductItemForEdit(EntityDto input)
        {
            var jobProductItem = await _jobProductItemRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetJobProductItemForEditOutput { JobProductItem = ObjectMapper.Map<CreateOrEditJobProductItemDto>(jobProductItem) };

            //if (output.JobProductItem.JobId != null)
            //{
            //	var _lookupJob = await _jobRepository.FirstOrDefaultAsync((int)output.JobProductItem.JobId);
            //	output.JobNote = _lookupJob?.Note?.ToString();
            //}

            if (output.JobProductItem.ProductItemId != null)
            {
                var _lookupProductItem = await _stockItemRepository.FirstOrDefaultAsync((int)output.JobProductItem.ProductItemId);
                output.ProductItemName = _lookupProductItem?.Name?.ToString();
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditJobProductItemDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_JobProductItems_Create)]
        protected virtual async Task Create(CreateOrEditJobProductItemDto input)
        {
            var jobProductItem = ObjectMapper.Map<JobProductItem>(input);


            if (AbpSession.TenantId != null)
            {
                jobProductItem.TenantId = (int)AbpSession.TenantId;
            }


            await _jobProductItemRepository.InsertAsync(jobProductItem);
        }

        //[AbpAuthorize(AppPermissions.Pages_JobProductItems_Edit)]
        protected virtual async Task Update(CreateOrEditJobProductItemDto input)
        {
            var jobProductItem = await _jobProductItemRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, jobProductItem);
        }

        //[AbpAuthorize(AppPermissions.Pages_JobProductItems_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _jobProductItemRepository.DeleteAsync(input.Id);
        }

        public async Task<List<JobProductItemJobLookupTableDto>> GetAllJobForTableDropdown()
        {
            return await _jobRepository.GetAll()
                .Select(job => new JobProductItemJobLookupTableDto
                {
                    Id = job.Id,
                    //DisplayName = job == null || job.Note == null ? "" : job.Note.ToString()
                }).ToListAsync();
        }

        public async Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdown()
        {
            return await _stockItemRepository.GetAll().Where(x => x.Active == true)
                .Select(productItem => new JobProductItemProductItemLookupTableDto
                {
                    Id = productItem.Id,
                    Size = productItem.Size,
                    ProductTypeId = productItem.CategoryId,
                    Model = productItem.Model,
                    DisplayName = productItem == null || productItem.Name == null ? "" : productItem.Name.ToString()
                }).ToListAsync();
        }


        public async Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdownForEdit()
        {
            return await _stockItemRepository.GetAll()
                .Select(productItem => new JobProductItemProductItemLookupTableDto
                {
                    Id = productItem.Id,
                    Size = productItem.Size,
                    ProductTypeId = productItem.CategoryId,
                    Model = productItem.Model,
                    DisplayName = productItem == null || productItem.Name == null ? "" : productItem.Name.ToString()
                }).ToListAsync();
        }

    }
}
