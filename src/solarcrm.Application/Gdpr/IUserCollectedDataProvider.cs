﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using solarcrm.Dto;

namespace solarcrm.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}
