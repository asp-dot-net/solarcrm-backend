﻿using Abp.Domain.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.LeadActivityLog;
using System.Net.Mail;
using solarcrm.Authorization.Users;
using Abp.Net.Mail;
using Abp.UI;
using RestSharp;
using solarcrm.Organizations;
using System.Web;
using Abp.Runtime.Security;
using solarcrm.Configuration;
using solarcrm.DocumentRequests.Dto;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.Leads;
using Abp.Organizations;
using Twilio.TwiML.Messaging;
using Abp.Domain.Uow;

namespace solarcrm.DocumentRequests
{
    public class DocumentRequestAppService : solarcrmAppServiceBase, IDocumentRequestAppService
    {
        private readonly IRepository<DocumentRequest> _documentRequestRepository;
        private readonly IRepository<DocumentRequestLinkHistory> _documentRequestLinkHistoryRepository;
        private readonly IRepository<DocumentList> _documentListRepository;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<SMS_ServiceProviderOrganization> _smsProviderUnitRepository;
        private readonly IAppConfigurationAccessor _configurationAccessor;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public DocumentRequestAppService(
            IRepository<DocumentRequest> documentRequestRepository
            ,IRepository<DocumentRequestLinkHistory> documentRequestLinkHistoryRepository
            , IRepository<DocumentList> documentListRepository
            , IRepository<Leads> leadRepository
            , IRepository<OrganizationUnit, long> organizationRepository
            , IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository
            , IRepository<LeadActivityLogs> leadactivityRepository
            , IRepository<User, long> userRepository
            , IEmailSender emailSender
            , IRepository<SMS_ServiceProviderOrganization> smsProviderUnitRepository
            , IAppConfigurationAccessor configurationAccessor
            , IUnitOfWorkManager unitOfWorkManager
            ) 
        {
            _documentRequestRepository = documentRequestRepository;
            _documentRequestLinkHistoryRepository = documentRequestLinkHistoryRepository;
            _documentListRepository = documentListRepository;
            _leadRepository = leadRepository;
            _organizationRepository = organizationRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _leadactivityRepository = leadactivityRepository;
            _userRepository = userRepository;
            _emailSender = emailSender;
            _smsProviderUnitRepository = smsProviderUnitRepository;
            _configurationAccessor = configurationAccessor;
            _unitOfWorkManager = unitOfWorkManager;

        }

        public async Task SendDocumentRequestForm(SendDocumentRequestInputDto input)
        {
            try
            {
                if (input.LeadId != 0)
                {
                    var documentsTypes = _documentListRepository.FirstOrDefault(input.DocTypeId);

                    DocumentRequest documentRequest = new DocumentRequest();
                    documentRequest.LeadId = input.LeadId;
                    documentRequest.DocTypeId = input.DocTypeId;
                    documentRequest.IsSubmitted = false;
                    var docRequestId = await _documentRequestRepository.InsertAndGetIdAsync(documentRequest);

                    var lead = await _leadRepository.GetAll().Where(e => e.Id == input.LeadId).FirstOrDefaultAsync();
                    var SmsConfiguration = await _smsProviderUnitRepository.GetAll().Where(e => e.SMSProviderOrganizationId == lead.OrganizationUnitId).FirstOrDefaultAsync();
                    var CurrentUser = await _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefaultAsync();
                    var assignedToUser = await _userRepository.GetAll().Where(u => u.Id == lead.ChanelPartnerID).FirstOrDefaultAsync();
                    var orgName = _organizationRepository.GetAll().Where(e => e.Id == lead.OrganizationUnitId).Select(e => e.DisplayName).FirstOrDefault();
                    var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    var leadassignusername = UserDetail == null ? "" : UserDetail.Name;

                    var Body = lead.TenantId + "," + lead.Id + "," + docRequestId + "," + input.SectionId + "," + input.DocTypeId;


                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.SectionId = input.SectionId;
                    if (input.SendMode == "SMS")
                    {
                        leadactivity.IsMark = false;
                        leadactivity.LeadActionNote = "Document Request Sent On SMS For " + documentsTypes.Name;
                    }
                    else
                    {
                        leadactivity.IsMark = null;
                        leadactivity.LeadActionNote = "Document Request Sent On Email For " + documentsTypes.Name;
                    }
                    leadactivity.LeadActionId = input.SendMode == "SMS" ? 6 : 7;
                    leadactivity.LeadId = Convert.ToInt32(input.LeadId);
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    //var leadActivity = _leadactivityRepository.GetAll().Where(u => u.Id == input.ActivityId).FirstOrDefault();


                    //Email Activity
                    if (input.SendMode == "Email" && !string.IsNullOrEmpty(lead.EmailId))
                    {
                        //var lead = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefault();
                        //var QuoteUrl = lead.TenantId + "," + lead.Id + "," + docRequestId;
                        //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
                        var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase));
                        var token1 = SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase);

                        string BaseURL = _configurationAccessor.Configuration["App:DocumentRequestRootAddress"];

                        //Body = "http://localhost:4200/account/request-document?STR=" + token;
                        Body = BaseURL + token;

                        DocumentRequestLinkHistory documentRequestLinkHistory = new DocumentRequestLinkHistory();
                        documentRequestLinkHistory.Expired = false;
                        documentRequestLinkHistory.DocumentRequestId = docRequestId;
                        documentRequestLinkHistory.Token = token1;// token;
                        await _documentRequestLinkHistoryRepository.InsertAsync(documentRequestLinkHistory);

                        var OrgLogo = _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationUnitId).Select(e => (e.Organization_LogoFilePath + e.Organization_LogoFileName)).FirstOrDefault();
                        OrgLogo = solarcrmConsts.ViewDocumentPath + (OrgLogo != null ? OrgLogo.Replace("\\", "/") : "");
                        string FinalBody = "<!doctype html><html lang='en'><head> <meta charset='utf-8'> <meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'> <meta name='viewport' content='width=device-width,initial-scale=1,shrink-to-fit=no'> <link rel='preconnect' href='https://fonts.googleapis.com'> <link rel='preconnect' href='https://fonts.gstatic.com' crossorigin> <link href='https://fonts.googleapis.com/css2?family=Roboto&display=swap' rel='stylesheet'> <title>Inline CSS Email</title></head><style>@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 400; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 500; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 700; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlfBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 900; font-display: swap; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtfBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2'); unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2'); unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2'); unicode-range: U+1F00-1FFF}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2'); unicode-range: U+0370-03FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2'); unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2'); unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF}@font-face{font-family: Roboto; font-style: normal; font-weight: 300; src: url(https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD}body{margin: 0; padding: 0; background: #E8F0FF; font-family: Roboto}</style><body style='background:#E8F0FF;margin:0;padding:0'> <div style='background:#E8F0FF;margin:0;padding:20px 0'> <div style='margin:10px auto;max-width:600px;min-width:320px'> <div style='margin:0 10px;max-width:600px;min-width:320px;padding:0;background-color:#fff;border:1px solid #e1defa;border-radius:10px;overflow:hidden'> <table style='width:100%;border:0;border-spacing:0;margin:0 auto'> <tr> <td style='text-align:center;padding:30px 0 20px'> <img src='" + OrgLogo + "' style='max-width:180px'> </td></tr><tr> <td style='text-align:center;padding:10px 0'><img src='https://thesolarproduct.com/assets/common/images/contract.png' style='width:35%'></td></tr><tr> <td style='font-size:22px;font-weight:700;font-family:Roboto,sans-serif;color:#000;padding:0 30px;text-align:center;font-weight:900'>Hello " + lead.CustomerName + ",</td></tr><tr> <td style='height:20px'></td></tr><tr> <td style='text-align:center;font-size:14px;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;padding:0 0'> <div style='font-size:.9rem;font-family:Roboto,sans-serif;color:#1a1a1a;margin:0 25px 30px;line-height:28px'>Your Solar Advisor <strong style='border-bottom:1px dotted #000'>" + leadassignusername + "</strong> From <strong style='border-bottom:1px dotted #000'>" + orgName + "</strong> is requesting " + documentsTypes.Name + "</div><div style='padding:20px 0'> <div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;margin-bottom:30px;line-height:24px'><a href='" + Body + "' style='text-decoration:none;text-decoration:none;background:#6A60D4;font-weight:700;color:#fff;letter-spacing:1px;font-size:16px;text-transform:uppercase;border-radius:25px;height:50px;width:320px;display:inline-block;line-height:50px'>Click Here To Upload Document</a></div><div style='font-size:18px;font-weight:400;font-family:Roboto,sans-serif;color:#000;margin-bottom:20px;line-height:24px;font-weight:700'>OR</div><div style='font-size:14px;font-weight:400;font-family:Roboto,sans-serif;line-height:24px'><a href='" + Body + "' style='text-decoration:none;color:#000;font-weight:900'>" + Body + "</a></div></div><div style='font-size:.9rem;font-weight:400;font-family:Roboto,sans-serif;color:#1a1a1a;margin:20px 25px 25px;line-height:24px'><strong>Note: </strong>This link is valid for <strong>24 hour</strong> from the time it was sent to you and can be used to upload the documents once.</div><div style='background:#6a60d452;font-family:Roboto,sans-serif;font-size:12px;color:#000;line-height:22px;border-top:1px solid #e2ebf1;padding:10px 0'>Thanks,<br>" + orgName + "</td></tr></table> </div></div></div></body></html>";

                        var FromEmail = await _extendedOrganizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationUnitId).Select(e => e.Organization_defaultFromAddress).FirstOrDefaultAsync();


                        if (lead.EmailId != "")
                        {
                            await _emailSender.SendAsync(new MailMessage
                            {
                                From = new MailAddress(FromEmail),
                                To = { lead.EmailId }, ////{ "hiral.prajapati@meghtechnologies.com" },
                                Subject = "Document Request.",
                                Body = FinalBody,
                                IsBodyHtml = true
                            });
                        }
                        else
                        {
                            throw new UserFriendlyException(L("NotFoundEmailId"));
                        }
                    }
                    //SMS Activity
                    if (input.SendMode == "SMS" && lead.MobileNumber != "")
                    {

                        var baseUrl = SmsConfiguration.msgtype;// solarcrmConsts.SMSBaseUrl; //"https://insprl.com";
                        var client = new RestClient(baseUrl);
                        var request = new RestRequest("/api/sms/send-msg", Method.Post);
                        var requesturl = new RestRequest("/api/url/get-short-url", Method.Post);
                        //request.RequestFormat = DataFormat.Json;
                        //request.AddParameter("Application/Json", myObject, ParameterType.RequestBody);
                        //request.AddUrlSegment("name", "Gaurav");
                        //request.AddUrlSegment("fathername", "Lt. Sh. Ramkrishan");
                        //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
                        var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase));
                        var token1 = SimpleStringCipher.Instance.Encrypt(Body, AppConsts.DefaultPassPhrase);
                        string BaseURL = _configurationAccessor.Configuration["App:ClientSignatureRootAddress"];
                        Body = BaseURL + token;
                        var ClickHere = Body;

                        var smsBody = "Dear " + lead.CustomerName + "\n\nYour Solar Advisor " + leadassignusername + " From " + orgName + " PTY LTD is requesting " + documentsTypes.Name + "." + "\n\nTo Upload documents please click below link \n" + ClickHere + "\n\nRegards\n" + orgName;


                        if (baseUrl != "" && SmsConfiguration != null && smsBody != null)
                        {

                            requesturl.AddHeader("content-type", "application/x-www-form-urlencoded");
                            requesturl.AddHeader("Authorization", SmsConfiguration.SMS_AuthorizationKey); //"SyMTYj4X9&8CA*P6s1iDl^75"
                            requesturl.AddParameter("client_id", SmsConfiguration.Client_Id); //"Csprl_DP2AEBO9FQN4UKT"
                            requesturl.AddParameter("long_url", Body); //"APSOLR"
                            requesturl.AddParameter("tag_type", "random");
                            requesturl.AddParameter("tag_keyword", "");//"Dear [Pravin test], Greetings from Australian Premium Solar (India) Pvt. Ltd. Your application for solar rooftop system is successfully registered with us. Thank you for choosing APS. Feel free to call on [07966006600] for any query. Regards, Team APS");
                            requesturl.AddParameter("domain", "");  //"txn"
                            requesturl.AddParameter("expiry_date", "");
                            var response = await client.ExecuteAsync(requesturl);

                            //input.Body.ToString().Replace("#linkurl", smsBody);
                        }

                        DocumentRequestLinkHistory documentRequestLinkHistory = new DocumentRequestLinkHistory();
                        documentRequestLinkHistory.Expired = false;
                        documentRequestLinkHistory.DocumentRequestId = docRequestId;
                        documentRequestLinkHistory.Token = token;
                        await _documentRequestLinkHistoryRepository.InsertAsync(documentRequestLinkHistory);


                        if (SmsConfiguration != null)
                        {
                            request.AddHeader("content-type", "application/x-www-form-urlencoded");
                            request.AddHeader("Authorization", SmsConfiguration.SMS_AuthorizationKey); //"SyMTYj4X9&8CA*P6s1iDl^75"
                            request.AddParameter("client_id", SmsConfiguration.Client_Id); //"Csprl_DP2AEBO9FQN4UKT"
                            request.AddParameter("sender_id", SmsConfiguration.Sender_Id); //"APSOLR"
                            request.AddParameter("mobile", lead.MobileNumber);
                            request.AddParameter("message", smsBody);//"Dear [Pravin test], Greetings from Australian Premium Solar (India) Pvt. Ltd. Your application for solar rooftop system is successfully registered with us. Thank you for choosing APS. Feel free to call on [07966006600] for any query. Regards, Team APS");
                            request.AddParameter("msgtype", "txn");  //"txn"
                            request.AddParameter("temp_id", 1707165460373476708);
                            //request.AddParameter("unicode", "0");
                            //request.AddParameter("flash", "0");
                            //request.AddParameter("campaign_name", "My campaign 1001");
                            var response = await client.ExecuteAsync(request);
                        }
                        else
                        {
                            throw new UserFriendlyException(L("PleaseEnterSMSProviderConfiguration"));
                        }
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("PleaseSelectLeador"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [UnitOfWork]
        public async Task<DocumentRequestDto> DocumentRequestData(string STR)
        {
            DocumentRequestDto documentRequestDto = new DocumentRequestDto();

            if (STR != null)
            {
                var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));

                var Id = Ids.Split(",");
                int TenantId = Convert.ToInt32(Id[0]);
                int leadId = Convert.ToInt32(Id[1]);
                int DocRequestId = Convert.ToInt32(Id[2]);
                int SectionId = Convert.ToInt32(Id[3]);

                using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                {
                    bool IsExpiried = false;
                    bool IsSubmitted = false;

                    var IsExpired = _documentRequestLinkHistoryRepository.GetAll().Where(e => e.DocumentRequestId == DocRequestId).OrderByDescending(e => e.Id).FirstOrDefault();

                    if (IsExpired != null)
                    {
                        if (IsExpired.CreationTime.AddHours(24) <= DateTime.UtcNow)
                        {
                            IsExpiried = true;
                        }
                        if (IsExpired.Expired == true)
                        {
                            IsSubmitted = true;
                        }

                        if (IsExpiried == false && IsSubmitted == false)
                        {
                            var documentRequest = _documentRequestRepository.GetAll().Include(e => e.DocTypeFk).Where(e => e.Id == DocRequestId).FirstOrDefault();
                            var UserDetail = await _userRepository.FirstOrDefaultAsync(u => u.Id == documentRequest.CreatorUserId);
                            documentRequestDto.Expiry = IsExpiried;
                            documentRequestDto.IsSubmitted = IsSubmitted;
                            documentRequestDto.RequestedBy = UserDetail.FullName.ToString();
                            documentRequestDto.DocumnetType = documentRequest.DocTypeFk.Name;

                            return documentRequestDto;
                        }
                        else
                        {
                            documentRequestDto.Expiry = IsExpiried;
                            documentRequestDto.IsSubmitted = IsSubmitted;
                            return documentRequestDto;
                        }
                    }
                    else
                    {
                        documentRequestDto.Expiry = IsExpiried;
                        documentRequestDto.IsSubmitted = IsSubmitted;
                        return documentRequestDto;
                    }
                }
            }
            else
            {
                documentRequestDto.Expiry = true;
                documentRequestDto.IsSubmitted = false;
                return documentRequestDto;
            }
        }

    }
}
