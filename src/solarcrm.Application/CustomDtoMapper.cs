using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.DynamicEntityProperties;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using IdentityServer4.Extensions;
using solarcrm.Auditing.Dto;
using solarcrm.Authorization.Accounts.Dto;
using solarcrm.Authorization.Delegation;
using solarcrm.Authorization.Permissions.Dto;
using solarcrm.Authorization.Roles;
using solarcrm.Authorization.Roles.Dto;
using solarcrm.Authorization.Users;
using solarcrm.Authorization.Users.Delegation.Dto;
using solarcrm.Authorization.Users.Dto;
using solarcrm.Authorization.Users.Importing.Dto;
using solarcrm.Authorization.Users.Profile.Dto;
using solarcrm.Chat;
using solarcrm.Chat.Dto;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.LeadSource.Dto;
using solarcrm.DataVaults.LeadType;
using solarcrm.DataVaults.LeadType.Dto;
using solarcrm.DataVaults.Teams;
using solarcrm.DataVaults.Teams.Dto;
using solarcrm.Entity.Dto;
using solarcrm.Editions;
using solarcrm.Editions.Dto;
using solarcrm.Friendships;
using solarcrm.Friendships.Cache;
using solarcrm.Friendships.Dto;
using solarcrm.Localization.Dto;
using solarcrm.MultiTenancy;
using solarcrm.MultiTenancy.Dto;
using solarcrm.MultiTenancy.HostDashboard.Dto;
using solarcrm.MultiTenancy.Payments;
using solarcrm.MultiTenancy.Payments.Dto;
using solarcrm.Notifications.Dto;
using solarcrm.Organizations.Dto;
using solarcrm.Sessions.Dto;
using solarcrm.WebHooks.Dto;

using solarcrm.DataVaults.Locations;
using solarcrm.DataVaults.Locations.Dto;

using solarcrm.DataVaults.District.Dto;
using solarcrm.DataVaults.District;
using solarcrm.DataVaults.State;
using solarcrm.DataVaults.State.Dto;
using solarcrm.DataVaults.Taluka.Dto;
using solarcrm.DataVaults.Taluka;
using solarcrm.DataVaults.City.Dto;
using solarcrm.DataVaults.City;
using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.HoldReasons.Dto;
using solarcrm.DataVaults.HoldReasons;
using solarcrm.DataVaults.RejectReasons.Dto;
using solarcrm.DataVaults.RejectReasons;
using solarcrm.DataVaults.JobCancellationReason.Dto;
using solarcrm.DataVaults.JobCancellationReason;
using solarcrm.DataVaults.SubDivision.Dto;
using solarcrm.DataVaults.SubDivision;
using solarcrm.DataVaults.Division.Dto;
using solarcrm.DataVaults.Division;
using solarcrm.DataVaults.TransactionType.Dto;
using solarcrm.DataVaults.TransactionType;
using solarcrm.DataVaults.StockItem.Importing.Dto;
using solarcrm.DataVaults.StockItem;
using solarcrm.DataVaults.ProjectStatus;
using solarcrm.DataVaults.ProjectStatus.Dto;
using solarcrm.DataVaults.ProjectType;
using solarcrm.DataVaults.ProjectType.Dto;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.StockCategory.Dto;
using solarcrm.DataVaults.InvoiceType;
using solarcrm.DataVaults.InvoiceType.Dto;
using solarcrm.DataVaults.Discom.Dto;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.StockItem.Dto;
using solarcrm.DataVaults.DocumentLibrary.Dto;
using solarcrm.DataVaults.DocumentLibrary;
using solarcrm.DataVaults.EmailTemplates.Dto;
using solarcrm.DataVaults.EmailTemplates;
using solarcrm.DataVaults.SmsTemplates.Dto;
using solarcrm.DataVaults.SmsTemplates;
using solarcrm.DataVaults.StockItemLocation.Dto;
using solarcrm.DataVaults.SolarType.Dto;
using solarcrm.DataVaults.SolarType;
using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.DataVaults.HeightStructure;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.Tender.Dto;
using solarcrm.DataVaults.Tender;
using solarcrm.DataVaults.Price.Dto;
using solarcrm.DataVaults.Price;
using solarcrm.DataVaults.Circle.Dto;
using solarcrm.DataVaults.Circle;
using solarcrm.DataVaults.LeadsActivityLog.Dto;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.Organizations;
using solarcrm.DataVaults.Department;
using solarcrm.DataVaults.Department.Dto;
using solarcrm.DataVaults.Leads.Importing.Dto;
using solarcrm.Jobs.Dto;
using solarcrm.Job;
using solarcrm.DataVaults.Variation.Dto;
using solarcrm.DataVaults.Variation;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLogMasterVault.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Job.JobVariation;
using solarcrm.Jobs.JobVariation.Dto;
using solarcrm.DataVaults.CustomeTag.Dto;
using solarcrm.DataVaults.CustomeTag;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.MISReport.Dto;
using solarcrm.MISReport.Importing.Dto;
using solarcrm.Quotations.Dtos;
using solarcrm.Quotation;
using solarcrm.Job.Quotation;
using solarcrm.DataVaults.PdfTemplates.Dto;
using solarcrm.DataVaults.PdfTemplates;
using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.DataVaults.PaymentMode;
using solarcrm.DataVaults.PaymentType.Dto;
using solarcrm.DataVaults.PaymentType;
using solarcrm.Payments.Dto;
using solarcrm.Payments;
using solarcrm.MobileService.MCustomer.Dto;
using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.DataVaults.BillOfMaterial;
using System.Collections.Generic;
using solarcrm.JobInstallation.Dto;
using solarcrm.JobInstallation;
using solarcrm.JobRefund.Dto;
using solarcrm.Job.JobRefunds;
using solarcrm.Dispatch;
using solarcrm.Tracker.PickListTracker.Dto;
using solarcrm.PickList;
using solarcrm.DataVaults.RefundReasons.Dto;
using solarcrm.DataVaults.RefundReasons;
using solarcrm.DataVaults.DispatchType.Dto;
using solarcrm.DataVaults.DispatchType;
using solarcrm.Tracker.TransportTracker.Dto;
using solarcrm.Transport;
using solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial;
using solarcrm.Tracker.DispatchTracker.Dto.DispatchStockTransfer;
using solarcrm.Tracker.DispatchTracker.Dto;
using solarcrm.MeterConnect;
using solarcrm.Tracker.MeterConnect.Dto;
using solarcrm.Subsidy;
using solarcrm.Tracker.SubsidyTracker.Importing.Dto;

namespace solarcrm
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();

            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();
            configuration.CreateMap<UserDetailsDto, UserDetail>().ReverseMap();
            //configuration.CreateMap<UserDetail, UserDetailsDto>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //MISReport
            configuration.CreateMap<MISReport.MISReport, MISReportDto>();
            configuration.CreateMap<MISReport.MISReport, CreateOrEditMISReportDto>();
            configuration.CreateMap<ImportMISReportDto, MISReport.MISReport> ().ReverseMap();

            //OrganizationUnit
            configuration.CreateMap<CreateOrganizationUnitInput, UpdateOrganizationUnitInput>().ReverseMap();
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();
            configuration.CreateMap<BankDetailsOrganization, CreateOrEditOrgBankDetailsDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicProperty, DynamicPropertyDto>().ReverseMap();
            configuration.CreateMap<DynamicPropertyValue, DynamicPropertyValueDto>().ReverseMap();
            configuration.CreateMap<DynamicEntityProperty, DynamicEntityPropertyDto>()
                .ForMember(dto => dto.DynamicPropertyName,
                    options => options.MapFrom(entity => entity.DynamicProperty.DisplayName.IsNullOrEmpty() ? entity.DynamicProperty.PropertyName : entity.DynamicProperty.DisplayName));
            configuration.CreateMap<DynamicEntityPropertyDto, DynamicEntityProperty>();

            configuration.CreateMap<DynamicEntityPropertyValue, DynamicEntityPropertyValueDto>().ReverseMap();
            
            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
            //Location
            configuration.CreateMap<LocationsDto, Locations>().ReverseMap();
            configuration.CreateMap<CreateOrEditLocationsDto, Locations>().ReverseMap();

            //LeadSource
            configuration.CreateMap<LeadSourceDto, LeadSource>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeadSourceDto, LeadSource>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeadSourceDto, MultipleFieldOrganizationUnitSelection>().ReverseMap();

            //LeadType
            configuration.CreateMap<LeadTypeDto, LeadType>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeadTypeDto, LeadType>().ReverseMap();

            //Document List
            configuration.CreateMap<DocumentListDto, DocumentList>().ReverseMap();
            configuration.CreateMap<CreateOrEditDocumentListDto, DocumentList>().ReverseMap();

            //SalesTeam
            configuration.CreateMap<TeamsDto, Teams>().ReverseMap();
            configuration.CreateMap<CreateOrEditTeamsDto, Teams>().ReverseMap();

            //Division
            configuration.CreateMap<DivisionDto, Division>().ReverseMap();
            configuration.CreateMap<CreateOrEditDivisionDto, Division>().ReverseMap();

            //Sub Division
            configuration.CreateMap<SubDivisionDto, SubDivision>().ReverseMap();
            configuration.CreateMap<CreateOrEditSubDivisionDto, SubDivision>().ReverseMap();

            //Transaction Type
            configuration.CreateMap<TransactionTypeDto, TransactionType>().ReverseMap();
            configuration.CreateMap<CreateOrEditTransactionTypeDto, TransactionType>().ReverseMap();

            //District
            configuration.CreateMap<DistrictDto, District>().ReverseMap();
            configuration.CreateMap<CreateOrEditDistrictDto, District>().ReverseMap();
            
            //State
            configuration.CreateMap<StateDto, State>().ReverseMap();
            configuration.CreateMap<CreateOrEditStateDto, State>().ReverseMap();
            
            //Taluka
            configuration.CreateMap<TalukaDto, Taluka>().ReverseMap();
            configuration.CreateMap<CreateOrEditTalukaDto, Taluka>().ReverseMap();
            
            //City
            configuration.CreateMap<CityDto, City>().ReverseMap();
            configuration.CreateMap<CreateOrEditCityDto, City>().ReverseMap();

            //HoldReasons
            configuration.CreateMap<HoldReasonsDto, HoldReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditHoldReasonsDto, HoldReasons>().ReverseMap();

            //CancelReasons
            configuration.CreateMap<CancelReasonsDto, CancelReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditCancelReasonsDto, CancelReasons>().ReverseMap();

            //Reject Reasons
            configuration.CreateMap<RejectReasonsDto, RejectReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditRejectReasonsDto, RejectReasons>().ReverseMap();
            
            //Reject Reasons
            configuration.CreateMap<RejectReasonsDto, RejectReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditRejectReasonsDto, RejectReasons>().ReverseMap();
            
            //Reject Reasons
            configuration.CreateMap<JobCancellationReasonDto, JobCancellationReason>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobCancellationReasonDto, JobCancellationReason>().ReverseMap();

            //Project Status
            configuration.CreateMap<ProjectStatusDto, ProjectStatus>().ReverseMap();
            configuration.CreateMap<CreateOrEditProjectStatusDto, ProjectStatus>().ReverseMap();
            
            //Project Type
            configuration.CreateMap<ProjectTypeDto, ProjectType>().ReverseMap();
            configuration.CreateMap<CreateOrEditProjectTypeDto, ProjectType>().ReverseMap();

            //Document library
            configuration.CreateMap<DocumentLibraryDto, DocumentLibrary>().ReverseMap();
            configuration.CreateMap<CreateOrEditDocumentLibraryDto, DocumentLibrary>().ReverseMap();

            //Email template
            configuration.CreateMap<EmailTemplateDto, EmailTemplates>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmailTemplateDto, EmailTemplates>().ReverseMap();

            //SMS template
            configuration.CreateMap<SmsTemplateDto, SmsTemplates>().ReverseMap();
            configuration.CreateMap<CreateOrEditSmsTemplateDto, SmsTemplates>().ReverseMap();

            //Stock Category
            configuration.CreateMap<StockCategoryDto, StockCategory>().ReverseMap();
            configuration.CreateMap<CreateOrEditStockCategoryDto, StockCategory>().ReverseMap();

            //Invoice Type
            configuration.CreateMap<InvoiceTypeDto, InvoiceType>().ReverseMap();
            configuration.CreateMap<CreateOrEditInvoiceTypeDto, InvoiceType>().ReverseMap();
            
            //Invoice Type
            configuration.CreateMap<InvoiceTypeDto, InvoiceType>().ReverseMap();
            configuration.CreateMap<CreateOrEditInvoiceTypeDto, InvoiceType>().ReverseMap();

            //Discom
            configuration.CreateMap<DiscomDto, Discom>().ReverseMap();
            configuration.CreateMap<CreateOrEditDiscomDto, Discom>().ReverseMap();

            //Tender
            configuration.CreateMap<TenderDto, Tender>().ReverseMap();
            configuration.CreateMap<CreateOrEditTenderDto, Tender>().ReverseMap();

            //Tender
            configuration.CreateMap<PriceDto, Prices>().ReverseMap();
            configuration.CreateMap<ProductPriceDto, Prices>().ReverseMap();
            configuration.CreateMap<CreateOrEditPriceDto, Prices>().ReverseMap();

            //Circle
            configuration.CreateMap<CircleDto, Circle>().ReverseMap();
            configuration.CreateMap<CreateOrEditCircleDto, Circle>().ReverseMap();

            //LeadActivityLog
            configuration.CreateMap<ActivityLogInput, LeadActivityLogs>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, NotifyLeadActivityLog>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, ToDoLeadActivityLog>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, ReminderLeadActivityLog>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, EmailLeadActivityLog>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, SmsLeadActivityLog>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, LeadActivityLogs>().ReverseMap();
            configuration.CreateMap<ActivityLogInput, CommentLeadActivityLog>().ReverseMap();

            configuration.CreateMap<CreateOrEditLeadsActivityLogDto, LeadActivityLogs>().ReverseMap();

            //Stock Item
            configuration.CreateMap<ImportStockItemDto, StockItem>().ReverseMap();
            configuration.CreateMap<StockItemDto, StockItem>().ReverseMap();
            configuration.CreateMap<CreateOrEditStockItemDto, StockItem>().ReverseMap();

            //Stock Item Location
            configuration.CreateMap<CreateOrEditStockItemLocationDto, StockItemLocation>().ReverseMap();

            //SolarType
            configuration.CreateMap<SolarTypeDto, SolarType>().ReverseMap();
            configuration.CreateMap<CreateOrEditSolarTypeDto, SolarType>().ReverseMap();

            //PaymentMode
            configuration.CreateMap<PaymentModeDto, PaymentMode>().ReverseMap();
            configuration.CreateMap<CreateOrEditPaymentModeDto, PaymentMode>().ReverseMap();

            //PaymentType
            configuration.CreateMap<PaymentTypeDto, PaymentType>().ReverseMap();
            configuration.CreateMap<CreateOrEditPaymentTypeDto, PaymentType>().ReverseMap();

            //HeightStructure
            configuration.CreateMap<HeightStructureDto, HeightStructure>().ReverseMap();
            configuration.CreateMap<CreateOrEditHeightStructureDto, HeightStructure>().ReverseMap();

            //Leads
            configuration.CreateMap<LeadsDto, Leads>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeadsDto, Leads>().ReverseMap();
            configuration.CreateMap<OrganizationUnitDto, ExtendOrganizationUnit>().ReverseMap();
            configuration.CreateMap<ImportLeadDto, Leads>().ReverseMap();
            configuration.CreateMap<LeadDocumentDto, LeadDocuments>().ReverseMap();
            configuration.CreateMap<CreateOrEditCustomerDto, Leads>().ReverseMap();
            
            //Department
            configuration.CreateMap<DepartmentDto, Department>().ReverseMap();
            configuration.CreateMap<CreateOrEditDepartmentDto, Department>().ReverseMap();

            //Jobs
            configuration.CreateMap<JobDto, Job.Jobs>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobDto, Job.Jobs>().ReverseMap();

            //Jobs Productf
            configuration.CreateMap<CreateOrEditJobProductItemDto, JobProductItem>().ReverseMap();
            configuration.CreateMap<JobProductItemDto, JobProductItem>().ReverseMap();

            //Location
            configuration.CreateMap<VariationDto, Variation>().ReverseMap();
            configuration.CreateMap<CreateOrEditVariationDto, Variation>().ReverseMap();
            
            //DataVaulteActivityLog
            configuration.CreateMap<DataVaultLogDto, DataVaulteActivityLog>().ReverseMap();
            configuration.CreateMap<DataVaultHistoriesLogDto, DataVaultsHistory>().ReverseMap();

            //Job Variation
            configuration.CreateMap<CreateOrEditJobVariationDto, JobVariation>().ReverseMap();
            configuration.CreateMap<JobVariationDto, JobVariation>().ReverseMap();

            //CustomeDTO
            configuration.CreateMap<CreateOrEditCustomeTagDto, CustomeTag>().ReverseMap();
            configuration.CreateMap<CustomeTagDto, CustomeTag>().ReverseMap();

            //Quotation
            configuration.CreateMap<CreateOrEditQuotationDataDto, QuotationData>().ReverseMap();
            configuration.CreateMap<QuotationDataDetails, Job.Jobs>().ReverseMap();
            configuration.CreateMap<QuotationDataDetailProduct, JobProductItem>().ReverseMap();
            configuration.CreateMap<QuotationDataDetailVariations, JobVariation>().ReverseMap();
            configuration.CreateMap<QuotationDataDetailsDto, QuotationDataDetails>().ReverseMap();

            //Pdf template
            configuration.CreateMap<PdfTemplateDto, PdfTemplates>().ReverseMap();
            configuration.CreateMap<CreateOrEditPdfTemplateDto, PdfTemplates>().ReverseMap();


            //payment Details Upload
            configuration.CreateMap<PaymentDetailsJob, PaymentDetailsJob>().ReverseMap();
            configuration.CreateMap<CreateOrEditPaymentDto, PaymentDetailsJob>().ReverseMap();

            configuration.CreateMap<PaymentReceiptItemDto, PaymentReceiptUploadJob>().ReverseMap();
            configuration.CreateMap<CreateOrEditPaymentReceiptItemsDto, PaymentReceiptUploadJob>().ReverseMap();

            //Bill of Material
            configuration.CreateMap<BillOfMaterialDto, BillOfMaterial>().ReverseMap();
            configuration.CreateMap<CreateOrEditBillOfMaterialDto, BillOfMaterial>().ReverseMap();
            // configuration.CreateMap<List<CreateOrEditHeightStructureDto>, HeightStructure>().ReverseMap();

            //Job Install Mapper
            configuration.CreateMap<JobInstallationDto, JobInstallationDetail>().ReverseMap();
            configuration.CreateMap<CreateOrEditJobInstallDto, JobInstallationDetail>().ReverseMap();
            configuration.CreateMap<GetJobInstallForEditOutput, JobInstallationDetail>().ReverseMap();

            //job refund 
            configuration.CreateMap<CreateOrEditJobRefundDto, JobRefunds>().ReverseMap();
            configuration.CreateMap<JobRefundDto, JobRefunds>().ReverseMap();

            //Dispatch Material
            configuration.CreateMap<CreateOrEditDispatchDto, DispatchMaterialDetails>().ReverseMap();
            configuration.CreateMap<DispatchDto, DispatchMaterialDetails>().ReverseMap();
            configuration.CreateMap<CreateOrEditDispatchProductItemDto, DispatchProductItem>().ReverseMap();

            //DispatchExtra Material
            configuration.CreateMap<DispatchExtraMaterialDto, DispatchExtraMaterials>().ReverseMap();
            //configuration.CreateMap<DispatchDto, DispatchMaterialDetails>().ReverseMap();

            // Dispatch Transfer Material  
            configuration.CreateMap<DispatchMaterialTransferDto, DispatchStockMaterialTransfer>().ReverseMap();
            configuration.CreateMap<CreateOrEditDispatchStockMaterialTransferDto, DispatchStockMaterialTransfer>().ReverseMap();

            //picklist
            configuration.CreateMap<CreateOrEditPicklistItemDto, PickListData>().ReverseMap();
            configuration.CreateMap<CreateOrEditPicklistItemDto, GetPickListItemForEditOutput>().ReverseMap();

            //Refund Master
            configuration.CreateMap<CreateOrEditRefundReasonsDto, RefundReasons>().ReverseMap();
            configuration.CreateMap<RefundReasonsDto, RefundReasons>().ReverseMap();

            //Dispatch Type Master
            configuration.CreateMap<CreateOrEditDispatchTypeDto, DispatchType>().ReverseMap();
            configuration.CreateMap<DispatchTypeDto, DispatchType>().ReverseMap();

            //Transport Tracker  MeterConnectDetails
            configuration.CreateMap<CreateOrEditTransportDto, TransportDetails>().ReverseMap();
            configuration.CreateMap<TransportTrackerDto, TransportDetails>().ReverseMap();

            //Meter Connect
            configuration.CreateMap<CreateOrEditMeterConnectDto, MeterConnectDetails>().ReverseMap();
            configuration.CreateMap<MeterConnectDto, MeterConnectDetails>().ReverseMap();

            // subsidy claim
            //configuration.CreateMap<ImportMISReportDto, SubsidyClaimDetails>().ReverseMap();
            configuration.CreateMap<ImportSubsidyClaimReportDto, SubsidyClaimDetails>().ReverseMap();

        }
    }
}
