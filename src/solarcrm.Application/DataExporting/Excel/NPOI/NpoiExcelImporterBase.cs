﻿using System;
using System.Collections.Generic;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;

namespace solarcrm.DataExporting.Excel.NPOI
{
    public abstract class NpoiExcelImporterBase<TEntity>
    {
        private IWorkbook workbook;
        protected List<TEntity> ProcessExcelFile(byte[] fileBytes, Func<ISheet, int, TEntity> processExcelRow, string filename, bool useOldExcelFormat = false)
        {
            var entities = new List<TEntity>();

            //IWorkbook workbook;

            using (var stream = new MemoryStream(fileBytes))
            {
                if (filename.ToLower() == ".xls")
                {
                    useOldExcelFormat = true;
                }

                workbook = useOldExcelFormat ? new HSSFWorkbook(stream) : new XSSFWorkbook(stream);
                for (var i = 0; i < workbook.NumberOfSheets; i++)
                {
                    var entitiesInWorksheet = ProcessWorksheet(workbook.GetSheetAt(i), processExcelRow);
                    entities.AddRange(entitiesInWorksheet);
                }
            }

            return entities;
        }

        private List<TEntity> ProcessWorksheet(ISheet worksheet, Func<ISheet, int, TEntity> processExcelRow)
        {
            var entities = new List<TEntity>();

            var rowEnumerator = worksheet.GetRowEnumerator();
            rowEnumerator.Reset();

            var i = 0;
            while (rowEnumerator.MoveNext())
            {
                if (i == 0)
                {
                    //Skip header
                    i++;
                    continue;
                }
                try
                {
                    var entity = processExcelRow(worksheet, i++);
                    if (entity != null)
                    {
                        entities.Add(entity);
                    }
                }
                catch (Exception ex)
                {
                    //ignore
                }
            }

            return entities;
        }
    }
}
