﻿using System.Collections.Generic;
using solarcrm.Authorization.Users.Importing.Dto;
using solarcrm.Dto;

namespace solarcrm.Authorization.Users.Importing
{
    public interface IInvalidUserExporter
    {
        FileDto ExportToFile(List<ImportUserDto> userListDtos);
    }
}
