﻿using System.Collections.Generic;
using solarcrm.Authorization.Users.Importing.Dto;
using Abp.Dependency;

namespace solarcrm.Authorization.Users.Importing
{
    public interface IUserListExcelDataReader: ITransientDependency
    {
        List<ImportUserDto> GetUsersFromExcel(byte[] fileBytes, string filename);
    }
}
