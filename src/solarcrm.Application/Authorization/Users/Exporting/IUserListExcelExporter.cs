using System.Collections.Generic;
using solarcrm.Authorization.Users.Dto;
using solarcrm.Dto;

namespace solarcrm.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}