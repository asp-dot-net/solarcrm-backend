﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Organizations;
using solarcrm.Authorization;
using solarcrm.Organizations.Dto;
using System.Linq.Dynamic.Core;
using Abp.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Roles;
using System.IO;
using Abp.UI;
using solarcrm.Storage;
using Microsoft.AspNetCore.Hosting;
using solarcrm.MultiTenancy;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace solarcrm.Organizations
{
    [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits)]
    public class OrganizationUnitAppService : solarcrmAppServiceBase, IOrganizationUnitAppService
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly OrganizationUnitManager _organizationUnitManager;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IRepository<ExtendOrganizationUnit, long> _extendedOrganizationUnitRepository;
        private readonly IRepository<SMS_ServiceProviderOrganization> _smsProviderUnitRepository;
        private readonly IRepository<BankDetailsOrganization> _bankDetailsOrgUnitRepository;
        private readonly RoleManager _roleManager;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        public OrganizationUnitAppService(
            OrganizationUnitManager organizationUnitManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            RoleManager roleManager,
            ITempFileCacheManager tempFileCacheManager,
            IWebHostEnvironment env,
            IRepository<Tenant> tenantRepository,
             IRepository<ExtendOrganizationUnit, long> extendedOrganizationUnitRepository,
            IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository,
            IRepository<SMS_ServiceProviderOrganization> smsProviderUnitRepository,
            IRepository<BankDetailsOrganization> bankDetailsOrgUnitRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _organizationUnitManager = organizationUnitManager;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _roleManager = roleManager;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _extendedOrganizationUnitRepository = extendedOrganizationUnitRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _env = env;
            _tenantRepository = tenantRepository;
            _smsProviderUnitRepository = smsProviderUnitRepository;
            _bankDetailsOrgUnitRepository = bankDetailsOrgUnitRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<ListResultDto<OrganizationUnitDto>> GetOrganizationUnits()
        {
            try
            {
                // var organizationUnits1 = await _organizationUnitRepository.GetAllListAsync();
                var organizationUnits = await _extendedOrganizationUnitRepository.GetAllListAsync();

                var organizationUnitMemberCounts = await _userOrganizationUnitRepository.GetAll()
                    .GroupBy(x => x.OrganizationUnitId)
                    .Select(groupedUsers => new
                    {
                        organizationUnitId = groupedUsers.Key,
                        count = groupedUsers.Count()
                    }).ToDictionaryAsync(x => x.organizationUnitId, y => y.count);

                var organizationUnitRoleCounts = await _organizationUnitRoleRepository.GetAll()
                    .GroupBy(x => x.OrganizationUnitId)
                    .Select(groupedRoles => new
                    {
                        organizationUnitId = groupedRoles.Key,
                        count = groupedRoles.Count()
                    }).ToDictionaryAsync(x => x.organizationUnitId, y => y.count);

                return new ListResultDto<OrganizationUnitDto>(
                    organizationUnits.Select(ou =>
                    {
                        var organizationUnitDto = ObjectMapper.Map<OrganizationUnitDto>(ou);
                        var smsProvider = _smsProviderUnitRepository.GetAll().Where(e => e.SMSProviderOrganizationId == ou.Id).FirstOrDefault();
                        organizationUnitDto.MemberCount = organizationUnitMemberCounts.ContainsKey(ou.Id) ? organizationUnitMemberCounts[ou.Id] : 0;
                        organizationUnitDto.RoleCount = organizationUnitRoleCounts.ContainsKey(ou.Id) ? organizationUnitRoleCounts[ou.Id] : 0;
                        organizationUnitDto.ProjectId = ou.Organization_ProjectId;
                        organizationUnitDto.OrganizationCode = ou.OrganizationCode;
                        organizationUnitDto.defaultFromAddress = ou.Organization_defaultFromAddress;
                        organizationUnitDto.defaultFromDisplayName = ou.Organization_defaultFromDisplayName;
                        organizationUnitDto.Email = ou.Organization_Email;
                        organizationUnitDto.website = ou.Organization_Website;
                        organizationUnitDto.LogoFileName = ou.Organization_LogoFileName;
                        organizationUnitDto.LogoFilePath = ou.Organization_LogoFilePath;
                        organizationUnitDto.Mobile = ou.Organization_Mobile;
                        organizationUnitDto.paymentMethod = ou.paymentMethod;
                        organizationUnitDto.Address = ou.Organization_Address;
                        organizationUnitDto.cardNumber = ou.cardNumber;
                        organizationUnitDto.expiryDateMonth = ou.expiryDateMonth;
                        organizationUnitDto.expiryDateYear = ou.expiryDateYear;
                        organizationUnitDto.cvn = ou.cvn;
                        organizationUnitDto.cardholderName = ou.cardholderName;
                        organizationUnitDto.MerchantId = ou.MerchantId;
                        organizationUnitDto.PublishKey = ou.PublishKey;
                        organizationUnitDto.SecrateKey = ou.SecrateKey;
                        organizationUnitDto.GSTNumber = ou.Organization_GSTNumber;
                        organizationUnitDto.PANNumber = ou.Organization_PANNumber;
                        organizationUnitDto.CINNumber = ou.Organization_CIN_Number;
                        organizationUnitDto.SMS_authorizationKey = smsProvider == null ? "" : smsProvider.SMS_AuthorizationKey; //_smsProviderUnitRepository.GetAll().Where(e => e.SMSProviderOrganizationId == ou.Id).Select(e => e.SMS_AuthorizationKey).FirstOrDefault(); //Convert.ToString(AutKey);
                        organizationUnitDto.sms_clientId = smsProvider == null ? "" : smsProvider.Client_Id; // _smsProviderUnitRepository.GetAll().Where(e => e.SMSProviderOrganizationId == ou.Id).Select(e => e.Client_Id).FirstOrDefault(); 
                        organizationUnitDto.sms_senderId = smsProvider == null ? "" : smsProvider.Sender_Id;// _smsProviderUnitRepository.GetAll().Where(e => e.SMSProviderOrganizationId == ou.Id).Select(e => e.Sender_Id).FirstOrDefault();
                        organizationUnitDto.sms_msgType = smsProvider == null ? "" : smsProvider.msgtype;// _smsProviderUnitRepository.GetAll().Where(e => e.SMSProviderOrganizationId == ou.Id).Select(e => e.msgtype).FirstOrDefault();
                        organizationUnitDto.OrgBankList = ObjectMapper.Map<List<CreateOrEditOrgBankDetailsDto>>(_bankDetailsOrgUnitRepository.GetAll().Where(e => e.BankDetailOrganizationId == ou.Id));
                        return organizationUnitDto;
                    }).ToList());
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        public async Task<PagedResultDto<OrganizationUnitUserListDto>> GetOrganizationUnitUsers(GetOrganizationUnitUsersInput input)
        {
            var query = from ouUser in _userOrganizationUnitRepository.GetAll()
                        join ou in _organizationUnitRepository.GetAll() on ouUser.OrganizationUnitId equals ou.Id
                        join user in UserManager.Users on ouUser.UserId equals user.Id
                        where ouUser.OrganizationUnitId == input.Id
                        select new
                        {
                            ouUser,
                            user
                        };

            var totalCount = await query.CountAsync();
            var items = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<OrganizationUnitUserListDto>(
                totalCount,
                items.Select(item =>
                {
                    var organizationUnitUserDto = ObjectMapper.Map<OrganizationUnitUserListDto>(item.user);
                    organizationUnitUserDto.AddedTime = item.ouUser.CreationTime;
                    return organizationUnitUserDto;
                }).ToList());
        }

        public async Task<PagedResultDto<OrganizationUnitRoleListDto>> GetOrganizationUnitRoles(GetOrganizationUnitRolesInput input)
        {
            var query = from ouRole in _organizationUnitRoleRepository.GetAll()
                        join ou in _organizationUnitRepository.GetAll() on ouRole.OrganizationUnitId equals ou.Id
                        join role in _roleManager.Roles on ouRole.RoleId equals role.Id
                        where ouRole.OrganizationUnitId == input.Id
                        select new
                        {
                            ouRole,
                            role
                        };

            var totalCount = await query.CountAsync();
            var items = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<OrganizationUnitRoleListDto>(
                totalCount,
                items.Select(item =>
                {
                    var organizationUnitRoleDto = ObjectMapper.Map<OrganizationUnitRoleListDto>(item.role);
                    organizationUnitRoleDto.AddedTime = item.ouRole.CreationTime;
                    return organizationUnitRoleDto;
                }).ToList());
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> CreateOrganizationUnit(CreateOrganizationUnitInput input)
        {
            var organizationUnit = new OrganizationUnit(AbpSession.TenantId, input.DisplayName, input.ParentId);
            ExtendOrganizationUnit extend = new ExtendOrganizationUnit();
            if (input.FileTokenOrg != null || input.FileTokenQrCode != null)
            {
                var organizationUnitDto = ObjectMapper.Map<UpdateOrganizationUnitInput>(input);
                //UpdateOrganizationUnitInput inputs = new UpdateOrganizationUnitInput();
                filedirectory(organizationUnitDto);
                input.Organization_LogoFilePath = organizationUnitDto.Organization_LogoFilePath;
            }
          
            extend.Organization_GSTNumber = input.OrganizationGSTNumber;
            extend.Organization_PANNumber = input.OrganizationPANNumber;
            extend.Organization_CIN_Number = input.OrganizationCINNumber;
            extend.Organization_Address = input.Organization_Address;
            extend.Code = input.OrganizationCode; //OrgCode.ToString().PadLeft(5, '0');
            extend.OrganizationCode = input.OrganizationCode;
            extend.Organization_ContactNo = input.Organization_ContactNo;
            extend.DisplayName = input.DisplayName;
            extend.Organization_ProjectId = input.ProjectId;
            extend.TenantId = AbpSession.TenantId;
            extend.Organization_Address = input.Organization_Address;
            extend.Organization_defaultFromAddress = input.Organization_defaultFromAddress;
            extend.Organization_defaultFromDisplayName = input.Organization_defaultFromDisplayName;
            extend.Organization_Website = input.Organization_Website;
            extend.Organization_Mobile = input.Organization_Mobile;
            extend.Organization_Email = input.Organization_Email;
            extend.Organization_LogoFileName = input.Organization_LogoFileName;
            extend.Organization_LogoFilePath = input.Organization_LogoFilePath;
            extend.paymentMethod = input.paymentMethod;
            extend.cardNumber = input.cardNumber;
            extend.expiryDateMonth = input.expiryDateMonth;
            extend.expiryDateYear = input.expiryDateYear;
            extend.cvn = input.cvn;
            extend.cardholderName = input.cardholderName;
            extend.MerchantId = input.MerchantId;
            extend.PublishKey = input.PublishKey;
            extend.SecrateKey = input.SecrateKey;
           
            try
            {
                //extend.smsProvider.SMSProviderOrganizationId = 
                var organizationid = await _extendedOrganizationUnitRepository.InsertAndGetIdAsync(extend);
                await CurrentUnitOfWork.SaveChangesAsync();

                if (organizationid != 0)
                {
                    SMS_ServiceProviderOrganization sms = new SMS_ServiceProviderOrganization();

                    sms.SMSProviderOrganizationId = organizationid;
                    sms.SMS_AuthorizationKey = input.SMS_authorizationKey;
                    sms.Client_Id = input.sms_clientId;
                    sms.Sender_Id = input.sms_senderId;
                    sms.msgtype = input.sms_msgType;

                    await _smsProviderUnitRepository.InsertAndGetIdAsync(sms);
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }


            return null;
            //return ObjectMapper.Map<OrganizationUnitDto>(organizationUnit);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> UpdateOrganizationUnit(UpdateOrganizationUnitInput input)
        {
            var orglogoFileName = input.Organization_LogoFileName;
            var orglogoFilePath = input.Organization_LogoFilePath;
           
            var extend = await _extendedOrganizationUnitRepository.GetAsync(input.Id);

            if (input.FileTokenOrg != null || input.FileTokenQrCode != null)
            {
                filedirectory(input);
            }
            //ExtendOrganizationUnit extend = new ExtendOrganizationUnit();
            try
            {
                extend.DisplayName = input.DisplayName;
                extend.Organization_GSTNumber = input.OrganizationGSTNumber;
                extend.Organization_PANNumber = input.OrganizationPANNumber;
                extend.Organization_CIN_Number = input.OrganizationCINNumber;
                extend.Organization_Address = input.Organization_Address;
                extend.Code = input.OrganizationCode; //OrgCode.ToString().PadLeft(5, '0');
                extend.OrganizationCode = input.OrganizationCode;
                extend.Organization_ContactNo = input.Organization_ContactNo;
                extend.DisplayName = input.DisplayName;
                extend.Organization_ProjectId = input.ProjectId;
                extend.TenantId = AbpSession.TenantId;
                extend.Organization_Address = input.Organization_Address;
                extend.Organization_defaultFromAddress = input.Organization_defaultFromAddress;
                extend.Organization_defaultFromDisplayName = input.Organization_defaultFromDisplayName;
                extend.Organization_Website = input.Organization_Website;
                extend.Organization_Mobile = input.Organization_Mobile;
                extend.Organization_Email = input.Organization_Email;
                extend.Organization_LogoFileName = input.Organization_LogoFileName;//== null ? orglogoFileName : input.Organization_LogoFileName;
                extend.Organization_LogoFilePath = input.Organization_LogoFilePath;//== null ? orglogoFilePath : input.Organization_LogoFilePath;
                extend.paymentMethod = input.paymentMethod;
                extend.cardNumber = input.cardNumber;
                extend.expiryDateMonth = input.expiryDateMonth;
                extend.expiryDateYear = input.expiryDateYear;
                extend.cvn = input.cvn;
                extend.cardholderName = input.cardholderName;
                extend.MerchantId = input.MerchantId;
                extend.PublishKey = input.PublishKey;
                extend.SecrateKey = input.SecrateKey;
                //extend.smsProvider.SMS_AuthorizationKey = input.SMS_authorizationKey;

                //extend.smsProvider.SMSProviderOrganizationId = 
                //await _organizationUnitManager.CreateAsync(extend);
                //await CurrentUnitOfWork.SaveChangesAsync();
                await _extendedOrganizationUnitRepository.UpdateAsync(extend);

                if (input.Id != 0)
                {
                    SMS_ServiceProviderOrganization sms = new SMS_ServiceProviderOrganization();
                    var smsprovider = await _smsProviderUnitRepository.GetAll().Where(x => x.SMSProviderOrganizationId == (int)input.Id).FirstOrDefaultAsync();
                    // var smsprovider =  await _smsProviderUnitRepository.GetAsync((int)input.Id);
                    if (smsprovider != null)
                    {
                        //sms.SMSProviderOrganizationId = organizationid;
                        smsprovider.SMS_AuthorizationKey = input.SMS_authorizationKey;
                        smsprovider.Client_Id = input.sms_clientId;
                        smsprovider.Sender_Id = input.sms_senderId;
                        smsprovider.msgtype = input.sms_msgType;

                        //await _dbContextProvider.GetDbContext().sms_ServiceProviderOrganization.AddRangeAsync(smsprovider);
                        //await _dbContextProvider.GetDbContext().SaveChangesAsync();
                        await _smsProviderUnitRepository.UpdateAsync(smsprovider);
                    }
                    else
                    {
                        sms.SMSProviderOrganizationId = input.Id;
                        sms.SMS_AuthorizationKey = input.SMS_authorizationKey;
                        sms.Client_Id = input.sms_clientId;
                        sms.Sender_Id = input.sms_senderId;
                        sms.msgtype = input.sms_msgType;
                        await _smsProviderUnitRepository.InsertAsync(sms);
                    }

                    BankDetailsOrganization bankDetails = new BankDetailsOrganization();
                    {

                        foreach (var item in input.BankDetailsList)
                        {
                            var bankdetailsupdate = await _bankDetailsOrgUnitRepository.GetAll().Where(x => x.BankDetailOrganizationId == (int)input.Id && x.Id == item.Id).FirstOrDefaultAsync();
                            if (bankdetailsupdate != null)
                            {
                                bankdetailsupdate.BankDetailOrganizationId = (int)input.Id;
                                bankdetailsupdate.OrgBankName = item.OrgBankName;
                                bankdetailsupdate.OrgBankBrachName = item.OrgBankBrachName;
                                bankdetailsupdate.OrgBankAccountNumber = item.OrgBankAccountNumber;
                                bankdetailsupdate.OrgBankAccountHolderName = item.OrgBankAccountHolderName;
                                bankdetailsupdate.OrgBankIFSC_Code = item.OrgBankIFSC_Code;
                                bankdetailsupdate.OrgBankQrCode_path = input.Organization_QrCodeFilePath == null ? item.OrgBankQrCode_path : input.Organization_QrCodeFilePath;
                                _bankDetailsOrgUnitRepository.InsertOrUpdate(bankdetailsupdate);
                            }
                            else
                            {
                                bankDetails.BankDetailOrganizationId = (int)input.Id;
                                bankDetails.OrgBankName = item.OrgBankName;
                                bankDetails.OrgBankBrachName = item.OrgBankBrachName;
                                bankDetails.OrgBankAccountNumber = item.OrgBankAccountNumber;
                                bankDetails.OrgBankAccountHolderName = item.OrgBankAccountHolderName;
                                bankDetails.OrgBankIFSC_Code = item.OrgBankIFSC_Code;
                                bankDetails.TenantId = AbpSession.TenantId;
                                bankDetails.OrgBankQrCode_path = input.Organization_QrCodeFilePath == null ? item.OrgBankQrCode_path : input.Organization_QrCodeFilePath;
                                _bankDetailsOrgUnitRepository.InsertOrUpdate(bankDetails);
                            }
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return await CreateOrganizationUnitDto(extend);
        }
        public string filedirectory(UpdateOrganizationUnitInput input)
        {
            string FileToken;
            string FileFolder;
            string InputFileName;
            if (input.FileTokenOrg != null)
            {
                FileToken = input.FileTokenOrg;
                InputFileName = input.Organization_LogoFileName;
                FileFolder = "OrganizationWiseLogo";
            }
            else
            {
                FileToken = input.FileTokenQrCode;
                InputFileName = input.Organization_QrCodeFileName;
                FileFolder = "OrganizationBankQrCode";
            }
            var ByteArray = _tempFileCacheManager.GetFile(FileToken);
            var filename = "";
            if (InputFileName.Contains("+"))
            {
                filename = InputFileName.Replace("+", "_");
            }
            else
            {
                filename = InputFileName.Replace(" ", "_");
            }
            if (ByteArray == null)
            {
                throw new UserFriendlyException("There is no such file with the token: " + FileToken);
            }

            if (ByteArray.Length > MaxFileBytes)
            {
                throw new UserFriendlyException("Document size exceeded");
            }

            var ext = Path.GetExtension(InputFileName);

            //input.FileType = DateTime.Now.Ticks + ext;
            var Path1 = _env.WebRootPath;
            var MainFolder = Path1;
            MainFolder = MainFolder + "\\Documents";
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();


            string TenantPath = MainFolder + "\\" + TenantName;
            string JobPath = TenantPath;
            string SignaturePath = "";

            SignaturePath = JobPath + "\\" + FileFolder;

            var FinalFilePath = SignaturePath + "\\" + filename;
            if (input.FileTokenOrg != null)
            {
                input.Organization_LogoFilePath = "\\Documents" + "\\" + TenantName + "\\" + FileFolder + "\\" + filename;  //"\\Documents" + FinalFilePath;
            }
            else
            {
                input.Organization_QrCodeFilePath = "\\Documents" + "\\" + TenantName + "\\" + FileFolder + "\\" + filename;  //"\\Documents" + FinalFilePath;
            }


            if (System.IO.Directory.Exists(MainFolder))
            {
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(MainFolder);
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);
                    if (System.IO.Directory.Exists(JobPath))
                    {
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(JobPath);
                        if (System.IO.Directory.Exists(SignaturePath))
                        {
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(SignaturePath);
                            using (MemoryStream mStream = new MemoryStream(ByteArray))
                            {
                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                            }
                        }
                    }
                }
            }
            return "";
        }
        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> MoveOrganizationUnit(MoveOrganizationUnitInput input)
        {
            await _organizationUnitManager.MoveAsync(input.Id, input.NewParentId);

            return await CreateOrganizationUnitDto(
                await _organizationUnitRepository.GetAsync(input.Id)
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task DeleteOrganizationUnit(EntityDto<long> input)
        {
            await _userOrganizationUnitRepository.DeleteAsync(x => x.OrganizationUnitId == input.Id);
            await _organizationUnitRoleRepository.DeleteAsync(x => x.OrganizationUnitId == input.Id);
            await _organizationUnitManager.DeleteAsync(input.Id);
        }


        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task RemoveUserFromOrganizationUnit(UserToOrganizationUnitInput input)
        {
            await UserManager.RemoveFromOrganizationUnitAsync(input.UserId, input.OrganizationUnitId);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles)]
        public async Task RemoveRoleFromOrganizationUnit(RoleToOrganizationUnitInput input)
        {
            await _roleManager.RemoveFromOrganizationUnitAsync(input.RoleId, input.OrganizationUnitId);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task AddUsersToOrganizationUnit(UsersToOrganizationUnitInput input)
        {
            foreach (var userId in input.UserIds)
            {
                await UserManager.AddToOrganizationUnitAsync(userId, input.OrganizationUnitId);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles)]
        public async Task AddRolesToOrganizationUnit(RolesToOrganizationUnitInput input)
        {
            foreach (var roleId in input.RoleIds)
            {
                await _roleManager.AddToOrganizationUnitAsync(roleId, input.OrganizationUnitId, AbpSession.TenantId);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindOrganizationUnitUsersInput input)
        {
            var userIdsInOrganizationUnit = _userOrganizationUnitRepository.GetAll()
                .Where(uou => uou.OrganizationUnitId == input.OrganizationUnitId)
                .Select(uou => uou.UserId);

            var query = UserManager.Users
                .Where(u => !userIdsInOrganizationUnit.Contains(u.Id))
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            var userCount = await query.CountAsync();
            var users = await query
                .OrderBy(u => u.Name)
                .ThenBy(u => u.Surname)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<NameValueDto>(
                userCount,
                users.Select(u =>
                    new NameValueDto(
                        u.FullName + " (" + u.EmailAddress + ")",
                        u.Id.ToString()
                    )
                ).ToList()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles)]
        public async Task<PagedResultDto<NameValueDto>> FindRoles(FindOrganizationUnitRolesInput input)
        {
            var roleIdsInOrganizationUnit = _organizationUnitRoleRepository.GetAll()
                .Where(uou => uou.OrganizationUnitId == input.OrganizationUnitId)
                .Select(uou => uou.RoleId);

            var query = _roleManager.Roles
                .Where(u => !roleIdsInOrganizationUnit.Contains(u.Id))
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.DisplayName.Contains(input.Filter) ||
                        u.Name.Contains(input.Filter)
                );

            var roleCount = await query.CountAsync();
            var users = await query
                .OrderBy(u => u.DisplayName)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<NameValueDto>(
                roleCount,
                users.Select(u =>
                    new NameValueDto(
                        u.DisplayName,
                        u.Id.ToString()
                    )
                ).ToList()
            );
        }

        private async Task<OrganizationUnitDto> CreateOrganizationUnitDto(OrganizationUnit organizationUnit)
        {
            var dto = ObjectMapper.Map<OrganizationUnitDto>(organizationUnit);
            dto.MemberCount = await _userOrganizationUnitRepository.CountAsync(uou => uou.OrganizationUnitId == organizationUnit.Id);
            return dto;
        }
    }
}