﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Division.Exporting;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.Division.Dto;
using Abp.Authorization;
using solarcrm.Authorization;
using Abp.Application.Services.Dto;
using solarcrm.MISReport.Exporting;
using solarcrm.MISReport.Dto;
using solarcrm.Authorization.Users;
using System;
using solarcrm.Common;

namespace solarcrm.MISReport
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_MISReport)]
    public class MISReportAppService : solarcrmAppServiceBase, IMISReportAppService
    {

        private readonly IRepository<MISReport> _misReportRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<MISFileUploadList.MISFileUploadLists> _misFileUploadRepository;
        private readonly IMISReportExcelExporter _misReportExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public MISReportAppService(IRepository<MISReport> misReportRepository, IRepository<User, long> userRepository, IMISReportExcelExporter misReportExcelExporter,
            IRepository<MISFileUploadList.MISFileUploadLists> misFileUploadRepository,
        IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _misReportRepository = misReportRepository;
            _misFileUploadRepository = misFileUploadRepository;
            _userRepository = userRepository;
            _misReportExcelExporter = misReportExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

       
        public async Task<PagedResultDto<GetMISReportForViewDto>> GetAll(GetAllMISReportInput input)
        {
            var filteredmisReport = _misReportRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.ConsumerNo.Contains(input.Filter));

            var pagedAndFilteredmisReport = filteredmisReport.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var misReport = from o in pagedAndFilteredmisReport
                            select new GetMISReportForViewDto()
                            {
                                MISReport = new MISReportDto
                                {
                                    Id = o.Id,
                                    ProjectName = o.ProjectName,
                                    ApplicationNo = o.ApplicationNo,
                                    GUVNLRegistrationNo = o.GUVNLRegistrationNo,
                                    Lat = o.Lat,
                                    Long = o.Long,
                                    PVCapacity = o.PVCapacity,
                                    DiscomName = o.DiscomName,
                                    ConsumerNo = o.ConsumerNo,
                                    TariffCategory = o.TariffCategory,
                                    Circle = o.Circle,
                                    DivisionZone = o.DivisionZone,
                                    Subdivision = o.Subdivision,
                                    SanctionedContractLoad = o.SanctionedContractLoad,
                                    PhaseofproposedSolarInverter = o.PhaseofproposedSolarInverter,
                                    Category = o.Category,
                                    ConsumerEmail = o.ConsumerEmail,
                                    ConsumerMobile = o.ConsumerMobile,
                                    NamePrefix = o.NamePrefix,
                                    FirstName = o.FirstName,
                                    MiddleName = o.MiddleName,
                                    LastName = o.LastName,
                                    LandlineNo = o.LandlineNo,
                                    StreetHouseNo = o.StreetHouseNo,
                                    Taluka = o.Taluka,
                                    District = o.District,
                                    CityVillage = o.CityVillage,
                                    State = o.State,
                                    Pin = o.Pin,
                                    CommonMeter = o.CommonMeter,
                                    AadhaarNoEntered = o.AadhaarNoEntered,
                                    OTPVerifiedon = o.OTPVerifiedon,
                                    SignedDocumentUploadedDate = o.SignedDocumentUploadedDate,
                                    LastComment = o.LastComment,
                                    LastCommentDate = o.LastCommentDate,
                                    LastCommentRepliedDate = o.LastCommentRepliedDate,
                                    DocumentVerifiedDate = o.DocumentVerifiedDate,
                                    EstimateQuotationNo = o.EstimateQuotationNo,
                                    DisComEstimateAmount = o.DisComEstimateAmount,
                                    DueDate = o.DueDate,
                                    PaymentReceived = o.PaymentReceived,
                                    PaymentMadeon = o.PaymentMadeon,
                                    SelfCertification = o.SelfCertification,
                                    CEIDrawingApplicationID = o.CEIDrawingApplicationID,
                                    CEIDrawingApplicationApprovalDate = o.CEIDrawingApplicationApprovalDate,
                                    BidirectionalMeterMake = o.BidirectionalMeterMake,
                                    BidirectionalMeterNo = o.BidirectionalMeterNo,
                                    SolarMeterMake = o.SolarMeterMake,
                                    SolarMeterNo = o.SolarMeterNo,
                                    DateofInstallationofSolarMeter = o.DateofInstallationofSolarMeter,
                                    AgreementSigningDate = o.AgreementSigningDate,
                                    ApplicationStatus = o.ApplicationStatus,
                                    IntimationReceivedStatus = o.IntimationReceivedStatus,
                                    IntimationApprovedDate = o.IntimationApprovedDate,
                                    IntimationStatus = o.IntimationStatus,
                                    PenaltyDaysPending = (int?)Convert.ToInt32(o.PenaltyDaysPending),
                                    PCRCode = o.PCRCode,
                                    PCRSubmittedDate = o.PCRSubmittedDate,


                                    //CreatedDate = o.CreationTime,

                                }
                            };

            var totalCount = await filteredmisReport.CountAsync();

            return new PagedResultDto<GetMISReportForViewDto>(totalCount, await misReport.ToListAsync());
        }

        public async Task<PagedResultDto<GetMISReportFileUploadForViewDto>> GetAllMisFileImportList(GetAllMISReportInput input)
        {
            var filteredmisReport = _misFileUploadRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.FileName.Contains(input.Filter));

            var pagedAndFilteredmisReport = filteredmisReport.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var misReport = from o in pagedAndFilteredmisReport
                            select new GetMISReportFileUploadForViewDto()
                            {
                                MISReportFileUpload = new MISFileUploadListDto
                                {
                                    Id = o.Id,
                                    FileName = o.FileName,
                                    FilePath = o.FilePath,
                                    CreationTime = o.CreationTime,  
                                    CreatorUserId =  o.CreatorUserId,
                                    userName = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                }
                            };
            var totalCount = await filteredmisReport.CountAsync();
            return new PagedResultDto<GetMISReportFileUploadForViewDto>(totalCount, await misReport.ToListAsync());
        }
        //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Edit)]
        public async Task<GetMISReportForEditOutput> GetMISReportForEdit(EntityDto input)
        {
            var misReport = await _misReportRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMISReportForEditOutput { MISReport = ObjectMapper.Map<CreateOrEditMISReportDto>(misReport) };

            return output;
        }

        // [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_ExportToExcel)]
        public async Task<FileDto> GetMISReportToExcel(GetAllMISReportForExcelInput input)
        {
            var filteredDiscom = _misReportRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ConsumerNo.Contains(input.Filter));

            var query = (from o in filteredDiscom
                         select new GetMISReportForViewDto()
                         {
                             MISReport = new MISReportDto
                             {
                                 Id = o.Id,
                                 ConsumerNo = o.ConsumerNo,
                             }
                         });

            var MisListDtos = await query.ToListAsync();

            return _misReportExcelExporter.ExportToFile(MisListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditMISReportDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Create)]
        protected virtual async Task Create(CreateOrEditMISReportDto input)
        {
            var MisItem = ObjectMapper.Map<MISReport>(input);

            if (AbpSession.TenantId != null)
            {
                // MisItem.TenantId = (int)AbpSession.TenantId;
            }

            var discomId = await _misReportRepository.InsertAndGetIdAsync(MisItem);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 45; // MIS Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "MIS Report Created";
            dataVaulteActivityLog.SectionValueId = discomId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        // [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Edit)]
        protected virtual async Task Update(CreateOrEditMISReportDto input)
        {
            var misReport = await _misReportRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            int SectionId = 45; // Discom Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Mis Report Updated";
            dataVaulteActivityLog.SectionValueId = input.Id;
            var misReportListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();



            //if (division.CircleId != input.CircleId)
            //{
            //    DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
            //    if (AbpSession.TenantId != null)
            //    {
            //        dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
            //    }
            //    dataVaultsHistory.FieldName = "CircleId";
            //    dataVaultsHistory.PrevValue = division.CircleId.ToString();
            //    dataVaultsHistory.CurValue = input.CircleId.ToString();
            //    dataVaultsHistory.Action = "Edit";
            //    dataVaultsHistory.ActionId = 2;
            //    dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
            //    dataVaultsHistory.SectionId = SectionId;// Pages name
            //    dataVaultsHistory.SectionValueId = input.Id;
            //    List.Add(dataVaultsHistory);
            //}

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, misReport);
        }

        // [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _misReportRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 45; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "MIS Report Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
