﻿using Abp.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MISReport.Importing.Dto
{
    public interface IMISReportListExcelDataReader : ITransientDependency
    {
        List<ImportMISReportDto> GetMISReportFromExcel(byte[] fileBytes, string filename, string GovermenrAgency);
    }
}
