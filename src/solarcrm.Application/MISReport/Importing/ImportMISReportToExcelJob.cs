﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Threading;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.EntityFrameworkCore;
using solarcrm.MISReport.Dto;
using solarcrm.MISReport.Importing.Dto;
using solarcrm.Notifications;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using solarcrm.MISReport.MISActivityLog;
using solarcrm.MISReport.MISFileUploadList;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults;
using solarcrm.Common;
using System.IO;

namespace solarcrm.MISReport.Importing
{
    public class ImportMISReportToExcelJob : BackgroundJob<ImportMISReportFromExcelJobArgs>, ITransientDependency
    {
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IMISReportListExcelDataReader _misReportListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidMISReportExporter _invalidMISReportExporter;
        //private readonly IAbpSession _abpSession;
        //private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<MISReport> _misReportrepository;
        private readonly IRepository<MISFileUploadLists> _misFileUploadRepository;
        private readonly IRepository<MISActivityLogs> _misActivityLogRepository;

        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<ApplicationStatus> _applicationRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private int _sectionId = 45;

        public ImportMISReportToExcelJob(
            IMISReportListExcelDataReader misReportListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IRepository<MISReport> misReportrepository,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidMISReportExporter invalidMisReportExporter,
            //IAbpSession abpSession,
            //IRepository<User, long> userRepository,
            IRepository<MISActivityLogs> misActivityLogRepository,
            IRepository<MISFileUploadLists> misFileUploadRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<ApplicationStatus> applicationRepository,
            ICommonLookupAppService CommonDocumentSaveRepository,
        IDbContextProvider<solarcrmDbContext> dbContextProvider
            )
        {
            _misReportListExcelDataReader = misReportListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _misReportrepository = misReportrepository;
            _unitOfWorkManager = unitOfWorkManager;
            _invalidMISReportExporter = invalidMisReportExporter;
            //_abpSession = abpSession;
            //_userRepository = userRepository;
            _misActivityLogRepository = misActivityLogRepository;
            _misFileUploadRepository = misFileUploadRepository;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _applicationRepository = applicationRepository;
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _dbContextProvider = dbContextProvider;
        }

        [UnitOfWork]
        public override void Execute(ImportMISReportFromExcelJobArgs args)
        {
            var stockItems = GetMISReportListFromExcelOrNull(args);
            if (stockItems == null || !stockItems.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                //var dd =  _jobRepository.GetAll();
                var invalidMISItems = new List<ImportMISReportDto>();

                foreach (var misItem in stockItems)
                {
                    if (misItem.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateOrUpdateMisReportAsync(misItem, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            //Lead.Exception = exception.Message;
                            invalidMISItems.Add(misItem);
                        }
                        //catch (Exception e)
                        catch
                        {
                            //Lead.Exception = e.ToString();
                            invalidMISItems.Add(misItem);
                        }
                    }
                    else
                    {
                        invalidMISItems.Add(misItem);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        //if(invalidMISItems.Count != 0)
                        {
                            AsyncHelper.RunSync(() => ProcessImportMISReportResultAsync(args, invalidMISItems));
                        }

                    }
                    uow.Complete();
                    //call update service Application Status
                    AsyncHelper.RunSync(() => MISReportServiceUpdate(args));
                }
            }
        }


        private List<ImportMISReportDto> GetMISReportListFromExcelOrNull(ImportMISReportFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));                      
                        string filename = Path.GetExtension(file.Description);
                        string agency = args.GovermentAgency;
                        var files    = _misReportListExcelDataReader.GetMISReportFromExcel(file.Bytes, filename, agency);
                        return files;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateOrUpdateMisReportAsync(ImportMISReportDto input, ImportMISReportFromExcelJobArgs args)
        {
            MISReport MISItem = new MISReport();


            try
            {
                var ApplicationStatusId = 0;
                //var JobStatusId = 0;
                using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
                {

                    if (input.ApplicationStatus != null)
                    {
                        ApplicationStatusId = await _applicationRepository.GetAll().Where(e => e.ApplicationStatusName.ToLower() == input.ApplicationStatus.ToLower()).Select(e => e.Id).FirstOrDefaultAsync();
                    }
                    if (ApplicationStatusId == 0)
                    {
                        input.Exception = "Application Status Found";
                        throw new Exception("Application Status Not Found");
                    }


                    //if (input.ProjectName != null)
                    //{
                    //    JobStatusId = await _jobRepository.GetAll().Where(e => e.JobNumber.ToLower() == input.ProjectName.ToLower()).Select(e => e.Id).FirstOrDefaultAsync();
                    //}

                    //if (JobStatusId == 0)
                    //{
                    //    input.Exception = "Project Number Not Found";
                    //    throw new Exception("Project Number Not Found");
                    //}
                    if (input.ProjectName != "")
                    {
                        var result = await _misReportrepository.GetAll().Where(e => e.ProjectName.ToLower() == input.ProjectName.Trim().ToLower()).FirstOrDefaultAsync();
                        if (result == null)
                        {
                            MISItem = _objectMapper.Map<MISReport>(result);
                            var MISItemNew = _objectMapper.Map<MISReport>(input);
                            MISItemNew.TenantId = (int)args.TenantId;
                            MISItemNew.isUpdated = false; //true for done And False for Pending
                                                          //MISItemNew.OrganizationUnitId = (int)args.o
                            MISItemNew.CreatorUserId = (int)args.User.UserId;

                            var misIItemId = await _misReportrepository.InsertAndGetIdAsync(MISItemNew);

                            ////Add MIS Activity Log
                            //MISActivityLogs misActivityLog = new MISActivityLogs();
                            //misActivityLog.TenantId = (int)args.TenantId;
                            //misActivityLog.SectionId = _sectionId;
                            //misActivityLog.LeadActionId = 1; //1-Created, 2-Updated, 3-Deleted
                            //misActivityLog.MISId = misIItemId;
                            //misActivityLog.MISActionNote = "MIS Items Created From Excel";
                            //misActivityLog.ProjectName = MISItemNew.ProjectName;
                            //misActivityLog.CreatorUserId = (int)args.User.UserId;
                            //await _misActivityLogRepository.InsertAsync(misActivityLog);
                        }
                        else
                        {
                            var MisID = result.Id;
                            var MISItemNew = _objectMapper.Map(input, result);
                            MISItemNew.Id = MisID;
                            MISItemNew.TenantId = (int)args.TenantId;
                            MISItemNew.isUpdated = false;
                            //MISItemNew.OrganizationUnitId = input.Name.Trim();                   
                            MISItemNew.CreatorUserId = (int)args.User.UserId;

                            await _misReportrepository.UpdateAsync(MISItemNew);

                            ////Add MIS Activity Log
                            //MISActivityLogs misActivityLog = new MISActivityLogs();
                            //misActivityLog.TenantId = (int)args.TenantId;
                            //misActivityLog.SectionId = _sectionId;
                            //misActivityLog.LeadActionId = 2; //1-Created, 2-Updated, 3-Deleted
                            //misActivityLog.MISActionNote = "MIS Items Updated From Excel";
                            //misActivityLog.MISId = MISItemNew.Id;
                            //misActivityLog.ProjectName = MISItemNew.ProjectName;
                            //misActivityLog.CreatorUserId = (int)args.User.UserId;
                            //await _misActivityLogRepository.InsertAsync(misActivityLog);                      
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendInvalidExcelNotification(ImportMISReportFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToMISReport", solarcrmConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }
        private async Task MISReportServiceUpdate(ImportMISReportFromExcelJobArgs args)
        {
            try
            {
                using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
                {

                    //Project Data Update service
                    //Add MIS LogHistory


                    var List = new List<LeadtrackerHistory>();
                    List<MISReport> MisListItem = new List<MISReport>();

                    MisListItem = await _misReportrepository.GetAll().Where(x => x.isUpdated == false).ToListAsync();
                    foreach (var MisItem in MisListItem)
                    {
                        if (MisItem != null)
                        {
                            //find the job number
                            var jobdata = await _jobRepository.GetAll().Include(x => x.LeadFK).Where(x => x.JobNumber == MisItem.ProjectName).FirstOrDefaultAsync();

                            List<Job.Jobs> objAuditOld_MisReportDat = new List<Job.Jobs>();
                            List<Job.Jobs> objAuditNew_MisReportDat = new List<Job.Jobs>();
                            DataTable Dt_Target = new DataTable();
                            DataTable Dt_Source = new DataTable();

                            if (jobdata != null)
                            {
                                //add job modified actvity Log
                                LeadActivityLogs leadactivity = new LeadActivityLogs();
                                leadactivity.LeadActionId = 2;
                                //leadactivity.LeadAcitivityID = 0;
                                try { leadactivity.SectionId = _sectionId; } catch { }
                                try { leadactivity.LeadActionNote = "Job Modified"; } catch { }
                                try { leadactivity.LeadId = jobdata.LeadId; } catch { }
                                if ((int)args.TenantId != null)
                                {
                                    leadactivity.TenantId = (int)args.TenantId;
                                }
                                var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);

                                objAuditOld_MisReportDat.Add(jobdata);

                                //job data update the excel wise
                                try { jobdata.ApplicationNumber = Convert.ToString(MisItem.ApplicationNo); } catch { }
                                try { jobdata.GUVNLRegisterationNo = Convert.ToString(MisItem.GUVNLRegistrationNo); } catch { }
                                try { jobdata.PhaseOfInverter = Convert.ToString(MisItem.PhaseofproposedSolarInverter); } catch { }
                                try { jobdata.PvCapacityKw = (decimal?)Convert.ToDecimal(MisItem.PVCapacity); } catch { }
                                try { jobdata.ApplicationDate = (DateTime?)MisItem.OTPVerifiedon; } catch { }
                                try { jobdata.EastimateQuoteNumber = (int?)Convert.ToInt32(MisItem.EstimateQuotationNo); } catch { }
                                try { jobdata.EastimateQuoteAmount = (decimal?)Convert.ToDecimal(MisItem.DisComEstimateAmount); } catch { }
                                try { jobdata.EastimateDueDate = (DateTime?)MisItem.DueDate; } catch { }
                                try { jobdata.EastimatePayDate = (DateTime?)MisItem.PaymentMadeon; } catch { }
                                try { jobdata.EastimatePaidStatus = Convert.ToString(MisItem.PaymentReceived); } catch { }
                                try { jobdata.LastComment = Convert.ToString(MisItem.LastComment); } catch { }
                                try { jobdata.LastCommentDate = (DateTime?)MisItem.LastCommentDate; } catch { }
                                try { jobdata.LastCommentRepliedDate = (DateTime?)MisItem.LastCommentRepliedDate; } catch { }
                                try { jobdata.DocumentVerifiedDate = (DateTime?)MisItem.DocumentVerifiedDate; } catch { }
                                try { jobdata.PenaltyDaysPending = (int?)Convert.ToInt32(MisItem.PenaltyDaysPending); } catch { }
                                try { jobdata.LeadFK.Latitude = Convert.ToDecimal(MisItem.Lat); } catch { }
                                try { jobdata.LeadFK.Longitude = Convert.ToDecimal(MisItem.Long); } catch { }

                                jobdata.ApplicationStatusId = await _applicationRepository.GetAll().Where(x => MisItem.ApplicationStatus.Contains(x.ApplicationStatusName)).Select(x => x.Id).FirstOrDefaultAsync();
                                objAuditNew_MisReportDat.Add(jobdata);
                                var jobId = await _jobRepository.UpdateAsync(jobdata);

                                //Update the MisReport IsUpdate Flag true
                                MisItem.isUpdated = true;
                                await _misReportrepository.UpdateAsync(MisItem);

                                //#region edithistory
                                try
                                {
                                    object newvalue = objAuditNew_MisReportDat;
                                    JsonConvert.SerializeObject(newvalue, Formatting.Indented,
                                        new JsonSerializerSettings
                                        {
                                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                        });

                                    //Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue, Formatting.Indented,
                                    //    new JsonSerializerSettings
                                    //    {
                                    //        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                    //    }), (typeof(DataTable)));

                                    //if (objAuditOld_MisReportDat != null)
                                    //{
                                    //    Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_MisReportDat), (typeof(DataTable)));
                                    //}
                                    //foreach (DataRow dr_S in Dt_Source.Rows)
                                    //{
                                    //    foreach (DataColumn dc_S in Dt_Source.Columns)
                                    //    {
                                    //        if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                                    //        {
                                    //            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                    //            objAuditInfo.FieldName = dc_S.ColumnName;
                                    //            objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                    //            objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                                    //            objAuditInfo.Action = "Edit";
                                    //            objAuditInfo.LeadId = jobdata.LeadId;
                                    //            //objAuditInfo.LeadActionId = leadactid;
                                    //            objAuditInfo.TenantId = (int)args.TenantId;
                                    //            List.Add(objAuditInfo);
                                    //        }
                                    //    }
                                    //}
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                                //#endregion

                                ////Add MIS Activity Log
                                MISActivityLogs misActivityLog = new MISActivityLogs();
                                misActivityLog.TenantId = (int)args.TenantId;
                                misActivityLog.SectionId = _sectionId;
                                misActivityLog.LeadActionId = 2; //1-Created, 2-Updated, 3-Deleted
                                misActivityLog.MISId = MisItem.Id;
                                misActivityLog.MISActionNote = "MIS Items Modified";
                                misActivityLog.ProjectName = MisItem.ProjectName;
                                misActivityLog.CreatorUserId = (int)args.User.UserId;
                                await _misActivityLogRepository.InsertAsync(misActivityLog);


                                // await _dbContextProvider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
                                // await _dbContextProvider.GetDbContext().SaveChangesAsync();

                                //ObjectMapper.Map(input, leads);
                            }
                        }
                    }
                    // uow.Complete();                
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //await Task.Delay(5000);

        }
        private async Task ProcessImportMISReportResultAsync(ImportMISReportFromExcelJobArgs args, List<ImportMISReportDto> invalidMisItem)
        {
            string FilestatusUpload = "";
            if (invalidMisItem.Any())
            {
                var file = _invalidMISReportExporter.ExportToFile(invalidMisItem);
                await _appNotifier.SomeMISReportCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
                FilestatusUpload = "Failed";
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllMISreportDataSuccessfullyImportedFromExcel", solarcrmConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
                FilestatusUpload = "Success";
            }

            //MISReport file save name
            var ext = ".xlsx";
            string filename = DateTime.Now.Ticks + "_" + "MISReportImportFiles" + ext;
            var filebyte = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
            var filepath = _CommonDocumentSaveRepository.SaveCommonDocument(filebyte.Bytes, filename, "MISReportImportFiles", 0, 0, args.TenantId);

            MISFileUploadLists files = new MISFileUploadLists();
            files.CreatorUserId = (int)args.User.UserId;
            files.TenantId = (int)args.TenantId;
            files.FileName = filepath.filename;
            files.FilePath = "\\Documents" + "\\" + filepath.TenantName + "\\" + "MISReportImportFiles" + "\\";
            files.FileStatus = FilestatusUpload;
            await _misFileUploadRepository.InsertAsync(files);
        }
    }
}
