﻿using Abp.Dependency;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.MISReport.Importing.Dto;
using solarcrm.Storage;
using System.Collections.Generic;


namespace solarcrm.MISReport.Importing
{
    public class InvalidMISReportExporter : NpoiExcelExporterBase, IInvalidMISReportExporter, ITransientDependency
    {
        public InvalidMISReportExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportMISReportDto> misReportListDtos)
        {
            return CreateExcelPackage(
                "InvalidMISReportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("InvalidMISReportImports"));

                    AddHeader(
                        sheet,                       
                        L("ProjectName"),                       
                        L("ApplicationNo"),
                        L("GUVNLRegistrationNo"),
                        L("Lat"),
                        L("Long"),
                        L("PVCapacity"),                        
                        L("DiscomName"),
                        L("ConsumerNo"),
                        L("TariffCategory"),
                        L("Circle"),
                        L("DivisionZone"),
                        L("Subdivision"),
                        L("SanctionedContractLoad"),
                        L("PhaseofproposedSolarInverter"),
                        L("Category"),                       
                        L("ConsumerEmail"),
                        L("ConsumerMobile"),
                        L("NamePrefix"),
                        L("FirstName"),
                        L("MiddleName"),
                        L("LastName"),
                        L("LandlineNo"),
                        L("StreetHouseNo"),
                        L("Taluka"),
                        L("District"),
                        L("CityVillage"),
                        L("State"),
                        L("Pin"),
                        L("CommonMeter"),
                        L("AadhaarNoEntered"),
                        L("OTPVerifiedon"),
                        L("SignedDocumentUploadedDate"),
                        L("LastComment"),
                        L("LastCommentDate"),
                        L("LastCommentRepliedDate"),
                        L("DocumentVerifiedDate"),
                        L("EstimateQuotationNo"),
                        L("DisComEstimateAmount"),
                        L("DueDate"),
                        L("PaymentReceived"),
                        L("PaymentMadeon"),
                        L("SelfCertification"),
                        L("CEIDrawingApplicationID"),
                        L("CEIDrawingApplicationApprovalDate"),
                        L("BidirectionalMeterMake"),
                        L("BidirectionalMeterNo"),
                        L("SolarMeterMake"),
                        L("SolarMeterNo"),
                        L("DateofInstallationofSolarMeter"),
                        L("AgreementSigningDate"),
                        L("ApplicationStatus"),
                        L("IntimationReceivedStatus"),
                        L("IntimationApprovedDate"),
                        L("IntimationStatus"),
                        L("PenaltyDaysPending"),
                        L("PCRCode"),
                        L("PCRSubmittedDate"),

                        L("TimeStamp"),
                        L("Scheme"),
                        L("ProjectArea"),
                        L("ProjectAreaType"),
                        L("AvgMonthlyBill"),
                        L("EnergyConsumption"),
                        L("ExistingPVCapacityKW"),
                        L("InstallerName"),
                        L("InstallerCategory"),
                        L("InstallerEmail"),
                        L("InstallerMobile"),
                        L("EmpanelmentNo"),
                        L("WhowillprovidetheNetMeter"),
                        L("CommunicationAddress"),
                        L("WhetherthePremisesisownedorRented"),
                        L("DepositQuotationNo"),
                        L("DepositQuotationAmount_inRS"),
                        L("DepositDueDate"),
                        L("DepositPaymentPaidon"),
                        L("IntimationRejectedDate"),
                        L("PVCapacityBand"),
                        L("PassportSizePhoto"),
                        L("ElectricityBill"),
                        L("SolarPVsystemOwnedbytheConsumer"),
                        L("DontWantSubsidy"),
                        L("FieldReportStatusReceivedfromDisComon"),
                        L("CEIInspectionApplicationID"),
                        L("CEIInspectionApplicationApprovalDate"),
                        L("WorkOrderNo"),
                        L("WorkOrderDate"),
                        L("WorkStartDate"),
                        L("WorkEndtDate"),
                        L("SanctionID"),
                        L("GovermentAgency"),

                        L("Refuse Reason")
                    );

                    AddObjects(
                        sheet, misReportListDtos,
                       
                        _ => _.ProjectName,                     
                        _ => _.ApplicationNo,
                        _ => _.GUVNLRegistrationNo,
                        _ => _.Lat,
                        _ => _.Long,
                        _ => _.PVCapacity,                      
                        _ => _.DiscomName,
                        _ => _.ConsumerNo,
                        _ => _.TariffCategory,
                        _ => _.Circle,
                        _ => _.DivisionZone,
                        _ => _.Subdivision,
                        _ => _.SanctionedContractLoad,
                        _ => _.PhaseofproposedSolarInverter,
                        _ => _.Category,                      
                        _ => _.ConsumerEmail,
                        _ => _.ConsumerMobile,
                        _ => _.NamePrefix,
                        _ => _.FirstName,
                        _ => _.MiddleName,
                        _ => _.LastName,
                        _ => _.LandlineNo,
                        _ => _.StreetHouseNo,
                        _ => _.Taluka,
                        _ => _.District,
                        _ => _.CityVillage,
                        _ => _.State,
                        _ => _.Pin,
                        _ => _.CommonMeter,
                        _ => _.AadhaarNoEntered,
                        _ => _.OTPVerifiedon,
                        _ => _.SignedDocumentUploadedDate,
                        _ => _.LastComment,
                        _ => _.LastCommentDate,
                        _ => _.LastCommentRepliedDate,
                        _ => _.DocumentVerifiedDate,
                        _ => _.EstimateQuotationNo,
                        _ => _.DisComEstimateAmount,
                        _ => _.DueDate,
                        _ => _.PaymentReceived,
                        _ => _.PaymentMadeon,
                        _ => _.SelfCertification,
                        _ => _.CEIDrawingApplicationID,
                        _ => _.CEIDrawingApplicationApprovalDate,
                        _ => _.BidirectionalMeterMake,
                        _ => _.BidirectionalMeterNo,
                        _ => _.SolarMeterMake,
                        _ => _.SolarMeterNo,
                        _ => _.DateofInstallationofSolarMeter,
                        _ => _.AgreementSigningDate,
                        _ => _.ApplicationStatus,
                        _ => _.IntimationReceivedStatus,
                        _ => _.IntimationApprovedDate,
                        _ => _.IntimationStatus,
                        _ => _.PenaltyDaysPending,
                        _ => _.PCRCode,
                        _ => _.PCRSubmittedDate,
                        _ => _.Exception
                    );

                    for (var i = 0; i < 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
