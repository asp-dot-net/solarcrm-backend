﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MISReport.Importing.Dto
{
    public class ImportMISReportDto : EntityDto<int?>
    {
        public string ProjectName { get; set; }
        public string ApplicationNo { get; set; }
        public string GUVNLRegistrationNo { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string PVCapacity { get; set; }
        public string DiscomName { get; set; }
        public string ConsumerNo { get; set; }
        public string TariffCategory { get; set; }
        public string Circle { get; set; }
        public string DivisionZone { get; set; }
        public string Subdivision { get; set; }
        public string SanctionedContractLoad { get; set; }
        public string PhaseofproposedSolarInverter { get; set; }
        public string Category { get; set; }
        public string ConsumerEmail { get; set; }
        public string ConsumerMobile { get; set; }
        public string NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string LandlineNo { get; set; }
        public string StreetHouseNo { get; set; }
        public string Taluka { get; set; }
        public string District { get; set; }
        public string CityVillage { get; set; }
        public string State { get; set; }
        public string Pin { get; set; }
        public string CommonMeter { get; set; }
        public string AadhaarNoEntered { get; set; }
        public DateTime? OTPVerifiedon { get; set; }
        public DateTime? SignedDocumentUploadedDate { get; set; }
        public string LastComment { get; set; }
        public DateTime? LastCommentDate { get; set; }
        public DateTime? LastCommentRepliedDate { get; set; }
        public DateTime? DocumentVerifiedDate { get; set; }
        public string EstimateQuotationNo { get; set; }
        public string DisComEstimateAmount { get; set; }
        public DateTime? DueDate { get; set; }
        public string PaymentReceived { get; set; }
        public DateTime? PaymentMadeon { get; set; }
        public DateTime? SelfCertification { get; set; }
        public string CEIDrawingApplicationID { get; set; }
        public DateTime? CEIDrawingApplicationApprovalDate { get; set; }
        public string BidirectionalMeterMake { get; set; }
        public string BidirectionalMeterNo { get; set; }
        public string SolarMeterMake { get; set; }
        public string SolarMeterNo { get; set; }
        public DateTime? DateofInstallationofSolarMeter { get; set; }
        public DateTime? AgreementSigningDate { get; set; }
        public string ApplicationStatus { get; set; }
        public DateTime? IntimationReceivedStatus { get; set; }
        public DateTime? IntimationApprovedDate { get; set; }
        public string IntimationStatus { get; set; }
        public string PenaltyDaysPending { get; set; }
        public string PCRCode { get; set; }
        public DateTime? PCRSubmittedDate { get; set; }

        // new added field
        public DateTime? TimeStamp { get; set; }
        public string Scheme { get; set; }
        public string ProjectArea { get; set; }
        public string ProjectAreaType { get; set; }
        public string AvgMonthlyBill { get; set; }
        public string EnergyConsumption { get; set; }
        public string ExistingPVCapacityKW { get; set; }

        public string InstallerName { get; set; }
        public string InstallerCategory { get; set; }
        public string InstallerEmail { get; set; }
        public string InstallerMobile { get; set; }
        public string EmpanelmentNo { get; set; }
        public string WhowillprovidetheNetMeter { get; set; }
        public string CommunicationAddress { get; set; }
        public string WhetherthePremisesisownedorRented { get; set; }
        public string DepositQuotationNo { get; set; }
        public string DepositQuotationAmount_inRS { get; set; }
        public DateTime? DepositDueDate { get; set; }
        public string DepositPaymentPaidon { get; set; }
        public DateTime? IntimationRejectedDate { get; set; }

        //New Field Added GEDA
        public string PVCapacityBand { get; set; }
        public string PassportSizePhoto { get; set; }
        public string ElectricityBill { get; set; }
        public string SolarPVsystemOwnedbytheConsumer { get; set; }
        public string DontWantSubsidy { get; set; }
        public DateTime? FieldReportStatusReceivedfromDisComon { get; set; }
        public long? CEIInspectionApplicationID { get; set; }
        public DateTime? CEIInspectionApplicationApprovalDate { get; set; }
        public long? WorkOrderNo { get; set; }
        public DateTime? WorkOrderDate { get; set; }
        public DateTime? WorkStartDate { get; set; }
        public DateTime? WorkEndDate { get; set; }
        public long? SanctionID { get; set; }
        public int GovermentAgency { get; set; }
        public virtual int OrganizationUnitId { get; set; }

        public virtual int TenantId { get; set; }
        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
