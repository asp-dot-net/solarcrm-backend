﻿using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MISReport.Importing.Dto
{
    public interface IInvalidMISReportExporter
    {
        FileDto ExportToFile(List<ImportMISReportDto> MisReportListDtos);
    }
}
