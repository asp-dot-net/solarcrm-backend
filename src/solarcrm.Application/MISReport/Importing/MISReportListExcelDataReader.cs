﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using NPOI.XWPF.UserModel;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.MISReport.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace solarcrm.MISReport.Importing
{
    public class MISReportListExcelDataReader : NpoiExcelImporterBase<ImportMISReportDto>, IMISReportListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public MISReportListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(solarcrmConsts.LocalizationSourceName);
        }

        string Agencyname = "";
        public List<ImportMISReportDto> GetMISReportFromExcel(byte[] fileBytes, string filename, string GovermenrAgency)
        {
            Agencyname = GovermenrAgency;
            return ProcessExcelFile(fileBytes, ProcessExcelRow, filename);
        }
        private ImportMISReportDto ProcessExcelRow(ISheet worksheet, int row)
        {
            //if (IsRowEmpty(worksheet, row))
            //{
            //    return null;
            //}

            //var exceptionMessage = new StringBuilder();
            var MisItem = new ImportMISReportDto();

            IRow roww = worksheet.GetRow(row);
            //List<ICell> cells = roww.Cells;
            //List<string> rowData = new List<string>();
            ////Sheet.ActiveCell.Column;
            //for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
            //{
            //    ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
            //    rowData.Add(Convert.ToString(cell));
            //}
            DataTable dt = new DataTable();
            IRow headerRow = worksheet.GetRow(0);
            // dt.Columns.AddRange(new DataColumn[] { headerRow.Cells });
            //for (int i = (worksheet.FirstRowNum + 1); i <= worksheet.LastRowNum; i++)
            {
                IRow row1 = worksheet.GetRow(row);
                DataRow dataRow = dt.NewRow();
                for (int j = row1.FirstCellNum; j < row1.LastCellNum; j++)
                {
                    try
                    {
                        string columnName = Convert.ToString(headerRow.GetCell(j));
                        NPOI.SS.UserModel.ICell cell = roww.GetCell(j, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        string strValue = Convert.ToString(cell); // row1.GetCell(j);
                        string collumn = Regex.Replace(columnName, @"[^0-9a-zA-Z]+", "");
                        dataRow.Table.Columns.Add(collumn);
                        dataRow[collumn] = strValue;
                    }
                    catch  {  }                    
                }
                dt.Rows.Add(dataRow);
            }
            try
            {
                if (dt.Rows.Count > 0)
                {
                    try { MisItem.ProjectName = Convert.ToString(dt.Rows[0]["projectname"]).Trim(); } catch { }
                    try { MisItem.ApplicationNo = Convert.ToString(dt.Rows[0]["ApplicationNo"]).Trim(); } catch { }
                    try { MisItem.GUVNLRegistrationNo = Agencyname == "1" ? Convert.ToString(dt.Rows[0]["GUVNLRegistrationNo"]).Trim() : Convert.ToString(dt.Rows[0]["GEDARegistrationNo"]).Trim(); } catch { }
                    try { MisItem.Lat = Convert.ToString(dt.Rows[0]["Lat"]); } catch { }
                    try { MisItem.Long = Convert.ToString(dt.Rows[0]["Long"]); } catch { }
                    try { MisItem.PVCapacity = Convert.ToString(dt.Rows[0]["PVCapacity"]); } catch { }
                    try { MisItem.DiscomName = Convert.ToString(dt.Rows[0]["DiscomName"]).Trim(); } catch { }
                    try { MisItem.ConsumerNo = Convert.ToString(dt.Rows[0]["ConsumerNo"]); } catch { }
                    try { MisItem.TariffCategory = Convert.ToString(dt.Rows[0]["TariffCategory"]).Trim(); } catch { }
                    try { MisItem.Circle = Convert.ToString(dt.Rows[0]["Circle"]).Trim(); } catch { }
                    try { MisItem.DivisionZone = Convert.ToString(dt.Rows[0]["DivisionZone"]).Trim(); } catch { }
                    try { MisItem.Subdivision = Convert.ToString(dt.Rows[0]["Subdivision"]).Trim(); } catch { }
                    try { MisItem.SanctionedContractLoad = Convert.ToString(dt.Rows[0]["SanctionedContractLoad"]); } catch { }
                    try { MisItem.PhaseofproposedSolarInverter = Convert.ToString(dt.Rows[0]["PhaseofproposedSolarInverter"]).Trim(); } catch { }
                    try { MisItem.Category = Convert.ToString(dt.Rows[0]["Category"]).Trim(); } catch { }
                    try { MisItem.ConsumerEmail = Convert.ToString(dt.Rows[0]["ConsumerEmail"]).Trim(); } catch { }
                    try { MisItem.ConsumerMobile = Convert.ToString(dt.Rows[0]["ConsumerMobile"]).Trim(); } catch { }
                    try { MisItem.NamePrefix = Convert.ToString(dt.Rows[0]["NamePrefix"]).Trim(); } catch { }
                    try { MisItem.FirstName = Convert.ToString(dt.Rows[0]["FirstName"]).Trim(); } catch { }
                    try { MisItem.MiddleName = Convert.ToString(dt.Rows[0]["MiddleName"]).Trim(); } catch { }
                    try { MisItem.LastName = Convert.ToString(dt.Rows[0]["LastName"]).Trim(); } catch { }
                    try { MisItem.LandlineNo = Convert.ToString(dt.Rows[0]["LandlineNo"]).Trim(); } catch { }
                    try { MisItem.StreetHouseNo = Convert.ToString(dt.Rows[0]["StreetHouseNo"]).Trim(); } catch { }
                    try { MisItem.Taluka = Convert.ToString(dt.Rows[0]["Taluka"]).Trim(); } catch { }
                    try { MisItem.District = Convert.ToString(dt.Rows[0]["District"]).Trim(); } catch { }
                    try { MisItem.CityVillage = Convert.ToString(dt.Rows[0]["CityVillage"]).Trim(); } catch { }
                    try { MisItem.State = Convert.ToString(dt.Rows[0]["State"]).Trim(); } catch { }
                    try { MisItem.Pin = Convert.ToString(dt.Rows[0]["Pin"]).Trim(); } catch { }
                    try { MisItem.CommonMeter = Convert.ToString(dt.Rows[0]["CommonMeter"]).Trim(); } catch { }
                    try { MisItem.AadhaarNoEntered = Convert.ToString(dt.Rows[0]["AadhaarNoEntered"]).Trim(); } catch { }
                    try { MisItem.OTPVerifiedon = (dt.Rows[0]["OTPVerifiedon"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["OTPVerifiedon"])); } catch { }
                    try { MisItem.SignedDocumentUploadedDate = (dt.Rows[0]["SignedDocumentUploadedDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["SignedDocumentUploadedDate"])); } catch { }
                    try { MisItem.LastComment = Convert.ToString(dt.Rows[0]["LastComment"]).Trim(); } catch { }
                    try { MisItem.LastCommentDate = (dt.Rows[0]["LastCommentDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["LastCommentDate"])); } catch { }
                    try { MisItem.LastCommentRepliedDate = (dt.Rows[0]["LastCommentRepliedDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["LastCommentRepliedDate"])); } catch { }
                    try { MisItem.DocumentVerifiedDate = (dt.Rows[0]["DocumentVerifiedDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["DocumentVerifiedDate"])); } catch { }
                    try { MisItem.EstimateQuotationNo = Convert.ToString(dt.Rows[0]["EstimateQuotationNo"]).Trim(); } catch { }
                    try { MisItem.DisComEstimateAmount = Convert.ToString(dt.Rows[0]["DisComEstimateAmount"]).Trim(); } catch { }
                    try { MisItem.DueDate = (dt.Rows[0]["DueDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["DueDate"])); } catch { }
                    try { MisItem.PaymentReceived = Convert.ToString(dt.Rows[0]["PaymentReceived"]).Trim(); } catch { }
                    try { MisItem.PaymentMadeon = (dt.Rows[0]["PaymentMadeon"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["PaymentMadeon"])); } catch { }
                    try { MisItem.SelfCertification = (dt.Rows[0]["SelfCertification"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["SelfCertification"])); } catch { }
                    try { MisItem.CEIDrawingApplicationID = Convert.ToString(dt.Rows[0]["CEIDrawingApplicationID"]).Trim(); } catch { }
                    try { MisItem.CEIDrawingApplicationApprovalDate = (dt.Rows[0]["CEIDrawingApplicationApprovalDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["CEIDrawingApplicationApprovalDate"])); } catch { }
                    try { MisItem.BidirectionalMeterMake = Convert.ToString(dt.Rows[0]["BidirectionalMeterMake"]).Trim(); } catch { }
                    try { MisItem.BidirectionalMeterNo = Convert.ToString(dt.Rows[0]["BidirectionalMeterNo"]).Trim(); } catch { }
                    try { MisItem.SolarMeterMake = Convert.ToString(dt.Rows[0]["BidirectionalMeterNo"]).Trim(); } catch { }
                    try { MisItem.SolarMeterNo = Convert.ToString(dt.Rows[0]["SolarMeterNo"]).Trim(); } catch { }
                    try { MisItem.DateofInstallationofSolarMeter = (dt.Rows[0]["DateofInstallationofSolarMeter"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["DateofInstallationofSolarMeter"])); } catch { }
                    try { MisItem.AgreementSigningDate = (dt.Rows[0]["AgreementSigningDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["AgreementSigningDate"])); } catch { }
                    try { MisItem.ApplicationStatus = Convert.ToString(dt.Rows[0]["ApplicationStatus"]).Trim(); } catch { }
                    try { MisItem.IntimationReceivedStatus = (dt.Rows[0]["IntimationReceivedStatus"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["IntimationReceivedStatus"])); } catch { }
                    try { MisItem.IntimationApprovedDate = (dt.Rows[0]["IntimationApprovedDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["LastComIntimationApprovedDatement"])); } catch { }
                    try { MisItem.IntimationStatus = Convert.ToString(dt.Rows[0]["IntimationStatus"]).Trim(); } catch { }
                    try { MisItem.PenaltyDaysPending = Convert.ToString(dt.Rows[0]["PenaltyDaysPending"]).Trim(); } catch { }
                    try { MisItem.PCRCode = Convert.ToString(dt.Rows[0]["PCRCode"]).Trim(); } catch { }
                    try { MisItem.PCRSubmittedDate = (dt.Rows[0]["PCRSubmittedDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["PCRSubmittedDate"])); } catch { }

                    // new added field
                    try { MisItem.TimeStamp = (dt.Rows[0]["TimeStamp"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["TimeStamp"])); } catch { }
                    try { MisItem.Scheme = Convert.ToString(dt.Rows[0]["Scheme"]).Trim(); } catch { }
                    try { MisItem.ProjectArea = Convert.ToString(dt.Rows[0]["ProjectArea"]).Trim(); } catch { }
                    try { MisItem.ProjectAreaType = Convert.ToString(dt.Rows[0]["ProjectAreaType"]).Trim(); } catch { }
                    try { MisItem.AvgMonthlyBill = Convert.ToString(dt.Rows[0]["AvgMonthlyBill"]).Trim(); } catch { }
                    try { MisItem.EnergyConsumption = Convert.ToString(dt.Rows[0]["EnergyConsumption"]).Trim(); } catch { }
                    try { MisItem.ExistingPVCapacityKW = Convert.ToString(dt.Rows[0]["ExistingPVCapacityKW"]).Trim(); } catch { }
                    try { MisItem.InstallerName = Convert.ToString(dt.Rows[0]["InstallerName"]).Trim(); } catch { }
                    try { MisItem.InstallerCategory = Convert.ToString(dt.Rows[0]["InstallerCategory"]).Trim(); } catch { }
                    try { MisItem.InstallerEmail = Convert.ToString(dt.Rows[0]["InstallerEmail"]).Trim(); } catch { }
                    try { MisItem.InstallerMobile = Convert.ToString(dt.Rows[0]["InstallerMobile"]).Trim(); } catch { }
                    try { MisItem.EmpanelmentNo = Convert.ToString(dt.Rows[0]["EmpanelmentNo"]).Trim(); } catch { }
                    try { MisItem.WhowillprovidetheNetMeter = Convert.ToString(dt.Rows[0]["WhowillprovidetheNetMeter"]).Trim(); } catch { }
                    try { MisItem.CommunicationAddress = Convert.ToString(dt.Rows[0]["CommunicationAddress"]).Trim(); } catch { }
                    try { MisItem.WhetherthePremisesisownedorRented = Convert.ToString(dt.Rows[0]["WhetherthePremisesisownedorRented"]).Trim(); } catch { }
                    try { MisItem.DepositQuotationNo = Convert.ToString(dt.Rows[0]["DepositQuotationNo"]).Trim(); } catch { }
                    try { MisItem.DepositQuotationAmount_inRS = Convert.ToString(dt.Rows[0]["DepositQuotationAmountinRS"]).Trim(); } catch { }
                    try { MisItem.DepositDueDate = (dt.Rows[0]["DepositDueDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["DepositDueDate"])); } catch { }
                    try { MisItem.DepositPaymentPaidon = Convert.ToString(dt.Rows[0]["DepositPaymentPaidon"]).Trim(); } catch { }
                    try { MisItem.IntimationRejectedDate = (dt.Rows[0]["IntimationRejectedDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["IntimationRejectedDate"])); } catch { }

                    try { MisItem.PVCapacityBand = Convert.ToString(dt.Rows[0]["PVCapacityBand"]).Trim(); } catch { }
                    try { MisItem.PassportSizePhoto = Convert.ToString(dt.Rows[0]["PassportSizePhoto"]).Trim(); } catch { }
                    try { MisItem.ElectricityBill = Convert.ToString(dt.Rows[0]["ElectricityBill"]).Trim(); } catch { }
                    try { MisItem.SolarPVsystemOwnedbytheConsumer = Convert.ToString(dt.Rows[0]["SolarPVsystemOwnedbytheConsumer"]).Trim(); } catch { }
                    try { MisItem.DontWantSubsidy = Convert.ToString(dt.Rows[0]["DontWantSubsidy"]).Trim(); } catch { }
                    try { MisItem.FieldReportStatusReceivedfromDisComon = (dt.Rows[0]["FieldReportStatusReceivedfromDisComon"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["FieldReportStatusReceivedfromDisComon"])); } catch { }
                    try { MisItem.CEIInspectionApplicationID = Convert.ToInt32(dt.Rows[0]["CEIInspectionApplicationID"]); } catch { }
                    try { MisItem.CEIInspectionApplicationApprovalDate = (dt.Rows[0]["CEIInspectionApplicationApprovalDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["CEIInspectionApplicationApprovalDate"])); } catch { }
                    try { MisItem.WorkOrderNo = Convert.ToInt64(dt.Rows[0]["WorkOrderNo"]); } catch { }
                    try { MisItem.WorkOrderDate = (dt.Rows[0]["WorkOrderDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["WorkOrderDate"])); } catch { }
                    try { MisItem.WorkStartDate = (dt.Rows[0]["WorkStartDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["WorkStartDate"])); } catch { }
                    try { MisItem.WorkEndDate = (dt.Rows[0]["WorkEndDate"] == null ? (DateTime?)null : (DateTime?)Convert.ToDateTime(dt.Rows[0]["WorkEndDate"])); } catch { }
                    try { MisItem.SanctionID = Convert.ToInt64(dt.Rows[0]["SanctionID"]); } catch { }
                    try { MisItem.GovermentAgency = Convert.ToInt32(Agencyname); } catch { }
                }
            }
            catch (System.Exception exception)
            {
                MisItem.Exception = exception.Message;
            }

            return MisItem;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }

    }
}
