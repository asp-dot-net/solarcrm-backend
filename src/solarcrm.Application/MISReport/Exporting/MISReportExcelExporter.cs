﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.MISReport.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MISReport.Exporting
{
    public class MISReportExcelExporter : NpoiExcelExporterBase, IMISReportExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public MISReportExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetMISReportForViewDto> misReport)
        {
            return CreateExcelPackage(
                "MISReport.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("MISReport"));
                    AddHeader(
                        sheet,
                        L("ProjectName"),
                        L("ApplicationNo"),
                        L("GUVNLRegistrationNo"),
                        L("Lat"),
                        L("Long"),
                        L("PVCapacity"),
                        L("DiscomName"),
                        L("ConsumerNo"),
                        L("TariffCategory"),
                        L("Circle"),
                        L("DivisionZone"),
                        L("Subdivision"),
                        L("SanctionedContractLoad"),
                        L("PhaseofproposedSolarInverter"),
                        L("Category"),
                        L("ConsumerEmail"),
                        L("ConsumerMobile"),
                        L("NamePrefix"),
                        L("FirstName"),
                        L("MiddleName"),
                        L("LastName"),
                        L("LandlineNo"),
                        L("StreetHouseNo"),
                        L("Taluka"),
                        L("District"),
                        L("CityVillage"),
                        L("State"),
                        L("Pin"),
                        L("CommonMeter"),
                        L("AadhaarNoEntered"),
                        L("OTPVerifiedon"),
                        L("SignedDocumentUploadedDate"),
                        L("LastComment"),
                        L("LastCommentDate"),
                        L("LastCommentRepliedDate"),
                        L("DocumentVerifiedDate"),
                        L("EstimateQuotationNo"),
                        L("DisComEstimateAmount"),
                        L("DueDate"),
                        L("PaymentReceived"),
                        L("PaymentMadeon"),
                        L("SelfCertification"),
                        L("CEIDrawingApplicationID"),
                        L("CEIDrawingApplicationApprovalDate"),
                        L("BidirectionalMeterMake"),
                        L("BidirectionalMeterNo"),
                        L("SolarMeterMake"),
                        L("SolarMeterNo"),
                        L("DateofInstallationofSolarMeter"),
                        L("AgreementSigningDate"),
                        L("ApplicationStatus"),
                        L("IntimationReceivedStatus"),
                        L("IntimationApprovedDate"),
                        L("IntimationStatus"),
                        L("PenaltyDaysPending"),
                        L("PCRCode"),
                        L("PCRSubmittedDate")
                        );

                    AddObjects(
                        sheet, misReport,
                        _ => _.MISReport.ProjectName,
                        _ => _.MISReport.ApplicationNo,
                        _ => _.MISReport.Lat,
                        _ => _.MISReport.Long,
                        _ => _.MISReport.PVCapacity,
                        _ => _.MISReport.DiscomName,
                        _ => _.MISReport.ConsumerNo,
                        _ => _.MISReport.TariffCategory,
                        _ => _.MISReport.Circle,
                        _ => _.MISReport.DivisionZone,
                        _ => _.MISReport.Subdivision,
                        _ => _.MISReport.SanctionedContractLoad,
                        _ => _.MISReport.PhaseofproposedSolarInverter,
                        _ => _.MISReport.Category,
                        _ => _.MISReport.ConsumerEmail,
                        _ => _.MISReport.ConsumerMobile,
                        _ => _.MISReport.NamePrefix,
                        _ => _.MISReport.FirstName,
                        _ => _.MISReport.MiddleName,
                        _ => _.MISReport.LastName,
                        _ => _.MISReport.LandlineNo,
                        _ => _.MISReport.StreetHouseNo,
                        _ => _.MISReport.Taluka,
                        _ => _.MISReport.District,
                        _ => _.MISReport.CityVillage,
                        _ => _.MISReport.State,
                        _ => _.MISReport.Pin,
                        _ => _.MISReport.CommonMeter,
                        _ => _.MISReport.AadhaarNoEntered,
                        _ => _.MISReport.OTPVerifiedon,
                        _ => _.MISReport.SignedDocumentUploadedDate,
                        _ => _.MISReport.LastComment,
                        _ => _.MISReport.LastCommentDate,
                        _ => _.MISReport.LastCommentRepliedDate,
                        _ => _.MISReport.DocumentVerifiedDate,
                        _ => _.MISReport.EstimateQuotationNo,
                        _ => _.MISReport.DisComEstimateAmount,
                        _ => _.MISReport.DueDate,
                        _ => _.MISReport.PaymentReceived,
                        _ => _.MISReport.PaymentMadeon,
                        _ => _.MISReport.SelfCertification,
                        _ => _.MISReport.CEIDrawingApplicationID,
                        _ => _.MISReport.CEIDrawingApplicationApprovalDate,
                        _ => _.MISReport.BidirectionalMeterMake,
                        _ => _.MISReport.BidirectionalMeterNo,
                        _ => _.MISReport.SolarMeterMake,
                        _ => _.MISReport.SolarMeterNo,
                        _ => _.MISReport.DateofInstallationofSolarMeter,
                        _ => _.MISReport.AgreementSigningDate,
                        _ => _.MISReport.ApplicationStatus,
                        _ => _.MISReport.IntimationReceivedStatus,
                        _ => _.MISReport.IntimationApprovedDate,
                        _ => _.MISReport.IntimationStatus,
                        _ => _.MISReport.PenaltyDaysPending,
                        _ => _.MISReport.PCRCode,
                        _ => _.MISReport.PCRSubmittedDate
                        );
                });
        }
    }
}
