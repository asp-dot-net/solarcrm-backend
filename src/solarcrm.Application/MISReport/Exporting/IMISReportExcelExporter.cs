﻿using solarcrm.Dto;
using solarcrm.MISReport.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MISReport.Exporting
{
    public interface IMISReportExcelExporter
    {
        FileDto ExportToFile(List<GetMISReportForViewDto> division);
    }
}
