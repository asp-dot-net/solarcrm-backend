﻿using System.Collections.Generic;
using solarcrm.Auditing.Dto;
using solarcrm.Dto;

namespace solarcrm.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportToFile(List<EntityChangeListDto> entityChangeListDtos);
    }
}
