﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.Runtime.Session;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Roles;
using solarcrm.Authorization.Users;
using solarcrm.Authorization.Users.Dto;
using solarcrm.Common.Dto;
using solarcrm.DataVaults;
using solarcrm.DataVaults.BillOfMaterial;
using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.Circle;
using solarcrm.DataVaults.City;
using solarcrm.DataVaults.Department;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.DispatchType;
using solarcrm.DataVaults.District;
using solarcrm.DataVaults.Division;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.EmailTemplates;
using solarcrm.DataVaults.EmailTemplates.Dto;
using solarcrm.DataVaults.HeightStructure;
using solarcrm.DataVaults.LeadActivity;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.DataVaults.LeadType;
using solarcrm.DataVaults.Locations;
using solarcrm.DataVaults.MaterialTransferReason;
using solarcrm.DataVaults.PaymentMode;
using solarcrm.DataVaults.PaymentType;
using solarcrm.DataVaults.RefundReasons;
using solarcrm.DataVaults.RejectReasons;
using solarcrm.DataVaults.SmsTemplates;
using solarcrm.DataVaults.SmsTemplates.Dto;
using solarcrm.DataVaults.SolarType;
using solarcrm.DataVaults.State;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.StockItem;
using solarcrm.DataVaults.SubDivision;
using solarcrm.DataVaults.Taluka;

using solarcrm.DataVaults.Teams;
using solarcrm.DataVaults.Tender;
using solarcrm.DataVaults.UserType;
using solarcrm.DataVaults.Vehical;
using solarcrm.Dto;
using solarcrm.Editions;
using solarcrm.Editions.Dto;
using solarcrm.EntityFrameworkCore;
using solarcrm.MultiTenancy;
using solarcrm.Organizations;
using solarcrm.Organizations.Dto;


namespace solarcrm.Common
{
    // [AbpAuthorize]
    public class CommonLookupAppService : solarcrmAppServiceBase, ICommonLookupAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly EditionManager _editionManager;
        private readonly IRepository<StockCategory> _stockCategoryRepository;
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IRepository<StockItemLocation> _stockItemLocationRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<District> _districtRepository;
        private readonly IRepository<Taluka> _talukaRepository;
        private readonly IRepository<City> _cityRepository;
        private readonly IRepository<SolarType> _solartypeRepository;
        private readonly IRepository<HeightStructure> _heightstructureRepository;
        private readonly IRepository<Discom> _discomRepository;
        private readonly IRepository<Division> _divisionRepository;
        private readonly IRepository<SubDivision> _subdivisionRepository;
        private readonly IRepository<Teams> _teamRepositoryy;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<LeadType> _leadTypeRepository;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IRepository<LeadSource, int> _lookup_leadSourceRepository;
        private readonly IRepository<LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<CancelReasons, int> _lookup_cancelReasonsRepository;
        private readonly IRepository<RejectReasons, int> _lookup_rejectReasonsRepository;
        private readonly IRepository<SmsTemplates, int> _lookup_smsTemplatesRepository;
        private readonly IRepository<EmailTemplates, int> _lookup_emailTemplatesRepository;
        private readonly IRepository<LeadActivity, int> _lookup_leadActivityRepository;
        private readonly IRepository<DocumentList> _documentListRepository;
        private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly IRepository<ApplicationStatus> _lookup_applicationstatusRepository;
        private readonly IRepository<UserType> _userTypeRepository;
        private readonly IRepository<Department> _departmentRepository;
        private readonly IRepository<Tender> _tenderRepository;
        private readonly IRepository<PaymentMode> _paymentModeRepository;
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IRepository<RefundReasons> _refundReasonsRepository;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Job.Jobs, int> _jobRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<Circle> _circleRepository;
        private readonly IRepository<UserDetail> _userDetailsRepository;
        private readonly IRepository<MultipleFieldOrganizationUnitSelection> _multipleFieldOrganizationUnitSelectionRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<BillOfMaterial> _billofMaterialRepository;
        private readonly IRepository<Locations> _locationRepository;
        private readonly IRepository<DispatchType> _dispatchTypeRepository;
        private readonly IRepository<Vehical> _vehicalRepository;
        private readonly IRepository<MaterialTransferReason> _materialTransferRepository;
        private const int MaxFileBytes = 5242880; //5MB
        public CommonLookupAppService(EditionManager editionManager,
            IRepository<StockItem> stockItemRepository,
            IRepository<StockCategory> stockCategoryRepository,
            IRepository<StockItemLocation> stockItemLocationRepository,
            IRepository<State> StateRepository,
            IRepository<District> DistrictRepository,
            IRepository<Taluka> TalukaRepository,
            IRepository<City> CityRepository,
            IRepository<SolarType> SolarTypeRepository,
            IRepository<HeightStructure> HeightStructureRepository,
            IRepository<Discom> DiscomRepository,
            IRepository<Division> DivisionRepository,
            IRepository<SubDivision> SubDivisionRepository,
            IRepository<Teams> TeamRepositoryy,
            IRepository<DocumentList> documentListRepository,
            IRepository<ApplicationStatus> applicationstatus,
            IRepository<Leads> leadsRepository,
            IRepository<PaymentMode> paymentModeRepository,
            IRepository<PaymentType> paymentTypeRepository,
            IRepository<RefundReasons> refundReasonsRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository,
            IRepository<LeadStatus> lookup_leadStatusRepository,
            IRepository<LeadSource, int> lookup_leadSourceRepository,
            IRepository<CancelReasons, int> lookup_cancelReasonsRepository,
            IRepository<RejectReasons, int> lookup_rejectReasonsRepository,
            IRepository<SmsTemplates, int> lookup_smsTemplatesRepository,
            IRepository<EmailTemplates, int> lookup_emailTemplatesRepository,
            IRepository<LeadActivity, int> lookup_leadActivityRepository,
            IRepository<User, long> userRepository,
            IRepository<UserRole, long> userroleRepository,
            IRepository<Role> roleRepository,
            IRepository<Tender> tenderRepository,
            IRepository<LeadType> leadTypeRepository,
            IRepository<BillOfMaterial> billofMaterialRepository,
            UserManager userManager,
            IRepository<UserType> userTypeRepository,
            IRepository<Department> departmentRepository,
            IRepository<SolarType> solarTypeRepository,
            RoleManager roleManager,
            IRepository<Circle> circleRepository,
            IRepository<UserDetail> userDetailsRepository,
            IRepository<MultipleFieldOrganizationUnitSelection> multipleFieldOrganizationUnitSelectionRepository,
            IRepository<LeadDocuments> leadDocumentRepository,
            IRepository<Job.Jobs, int> jobRepository,
            IRepository<Tenant> tenantRepository,
            IRepository<Locations> locationRepository,
            IRepository<DispatchType> dispatchTypeRepository,
        IWebHostEnvironment env,
        IDbContextProvider<solarcrmDbContext> dbContextProvider,
        IRepository<Vehical> vehicalRepository,
        IRepository<MaterialTransferReason> materialTransferRepository
            )
        {
            _editionManager = editionManager;
            _dbContextProvider = dbContextProvider;
            _stockItemRepository = stockItemRepository;
            _stockCategoryRepository = stockCategoryRepository;
            _stockItemLocationRepository = stockItemLocationRepository;
            _stateRepository = StateRepository;
            _districtRepository = DistrictRepository;
            _talukaRepository = TalukaRepository;
            _cityRepository = CityRepository;
            _solartypeRepository = SolarTypeRepository;
            _heightstructureRepository = HeightStructureRepository;
            _discomRepository = DiscomRepository;
            _divisionRepository = DivisionRepository;
            _subdivisionRepository = SubDivisionRepository;
            _teamRepositoryy = TeamRepositoryy;
            //_UserWiseEmailOrgsRepository = UserWiseEmailOrgsRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _userManager = userManager;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_cancelReasonsRepository = lookup_cancelReasonsRepository;
            _lookup_rejectReasonsRepository = lookup_rejectReasonsRepository;
            _lookup_smsTemplatesRepository = lookup_smsTemplatesRepository;
            _lookup_emailTemplatesRepository = lookup_emailTemplatesRepository;
            _lookup_leadActivityRepository = lookup_leadActivityRepository;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _userroleRepository = userroleRepository;
            _tenderRepository = tenderRepository;
            _documentListRepository = documentListRepository;
            _lookup_applicationstatusRepository = applicationstatus;
            _leadTypeRepository = leadTypeRepository;
            _leadRepository = leadsRepository;
            _userTypeRepository = userTypeRepository;
            _departmentRepository = departmentRepository;
            _roleManager = roleManager;
            _circleRepository = circleRepository;
            _userDetailsRepository = userDetailsRepository;
            _jobRepository = jobRepository;
            _tenantRepository = tenantRepository;
            _paymentModeRepository = paymentModeRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _refundReasonsRepository = refundReasonsRepository;
            _env = env;
            _billofMaterialRepository = billofMaterialRepository;
            _multipleFieldOrganizationUnitSelectionRepository = multipleFieldOrganizationUnitSelectionRepository;
            _leadDocumentRepository = leadDocumentRepository;
            _locationRepository = locationRepository;
            _dispatchTypeRepository = dispatchTypeRepository;
            _vehicalRepository = vehicalRepository;
            _materialTransferRepository = materialTransferRepository;
        }

        public async Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false)
        {
            var subscribableEditions = (await _editionManager.Editions.Cast<SubscribableEdition>().ToListAsync())
                .WhereIf(onlyFreeItems, e => e.IsFree)
                .OrderBy(e => e.MonthlyPrice);

            return new ListResultDto<SubscribableEditionComboboxItemDto>(
                subscribableEditions.Select(e => new SubscribableEditionComboboxItemDto(e.Id.ToString(), e.DisplayName, e.IsFree)).ToList()
            );
        }

        //get current user role
        public async Task<string> GetCurrentUserRole()
        {
            var User_List = _userRepository.GetAll();
            var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            //IList<string> role = await _userManager.GetRolesAsync(User);

            //return role[0];
            IList<string> role = (from user in User_List
                                  join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                  from ur in urJoined.DefaultIfEmpty()
                                  join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                  from us in usJoined.DefaultIfEmpty()
                                  where (us != null && user.Id == AbpSession.UserId)
                                  select (us.DisplayName)).ToList();

            return role[0];

        }
        //get current user Name
        public async Task<string> GetCurrentUserIdName()
        {
            return _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).Select(e => e.UserName).FirstOrDefault();
        }
        //get current user id
        public async Task<int> GetCurrentUserId()
        {
            return (int)AbpSession.UserId;
        }
        public async Task<List<UserListDto>> GetAllUserList()
        {
            try
            {
                var UserList = await _userRepository.GetAll()
                       .Select(users => new UserListDto
                       {
                           Id = users.Id,
                           UserName = users.UserName,
                           Name = users.Name,
                           EmailAddress = users.EmailAddress
                       }).OrderBy(e => e.Id).ToListAsync();
                return UserList;
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input)
        {
            if (AbpSession.TenantId != null)
            {
                //Prevent tenants to get other tenant's users.
                input.TenantId = AbpSession.TenantId;
            }

            using (CurrentUnitOfWork.SetTenantId(input.TenantId))
            {
                var query = UserManager.Users
                    .WhereIf(
                        !input.Filter.IsNullOrWhiteSpace(),
                        u =>
                            u.Name.Contains(input.Filter) ||
                            u.Surname.Contains(input.Filter) ||
                            u.UserName.Contains(input.Filter) ||
                            u.EmailAddress.Contains(input.Filter)
                    ).WhereIf(input.ExcludeCurrentUser, u => u.Id != AbpSession.GetUserId());

                var userCount = await query.CountAsync();
                var users = await query
                    .OrderBy(u => u.Name)
                    .ThenBy(u => u.Surname)
                    .PageBy(input)
                    .ToListAsync();

                return new PagedResultDto<NameValueDto>(
                    userCount,
                    users.Select(u =>
                        new NameValueDto(
                            u.FullName + " (" + u.EmailAddress + ")",
                            u.Id.ToString()
                            )
                        ).ToList()
                    );
            }
        }

        public GetDefaultEditionNameOutput GetDefaultEditionName()
        {
            return new GetDefaultEditionNameOutput
            {
                Name = EditionManager.DefaultEditionName
            };
        }
        public async Task<List<CommonLookupDto>> GetCheckDuplicateConsumerNo(long consumerNumber, int solartype)
        {
            List<Leads> Checkduplicate = new List<Leads>();
            List<CommonLookupDto> duplicate = new List<CommonLookupDto>();
            Checkduplicate = await _leadRepository.GetAll().Include(x => x.SolarTypeIdFk).Where(e => e.ConsumerNumber == consumerNumber && e.SolarTypeId == solartype && e.IsActive).ToListAsync();
            if (Checkduplicate.Count() > 0)
            {
                duplicate = (from o in Checkduplicate
                             select new CommonLookupDto()
                             {
                                 DisplayName = o.CustomerName,
                                 labeldata = o.SolarTypeIdFk == null ? null : o.SolarTypeIdFk.Name,
                                 Id = o.Id
                             }).ToList();
            }
            return new List<CommonLookupDto>(duplicate);
        }

        #region CommonDropDownList

        public async Task<List<CommonLookupDto>> GetAllVehicalForDropdown()
        {
            List<Vehical> allvehical = new List<Vehical>();
            List<CommonLookupDto> vehicalType = new List<CommonLookupDto>();
            allvehical = await _vehicalRepository.GetAll().ToListAsync();
            if (allvehical.Count() > 0)
            {
                vehicalType = (from o in allvehical
                               select new CommonLookupDto()
                               {
                                   DisplayName = o.VehicalType,
                                   Id = o.Id,
                                   labeldata = o.VehicalRegNo
                               }).ToList();
            }
            return new List<CommonLookupDto>(vehicalType);
        }
        public async Task<List<CommonLookupDto>> GetAllMaterialTransferReasonForDropdown()
        {
            List<MaterialTransferReason> allmaterailTransfer = new List<MaterialTransferReason>();
            List<CommonLookupDto> transfer = new List<CommonLookupDto>();
            allmaterailTransfer = await _materialTransferRepository.GetAll().ToListAsync();
            if (allmaterailTransfer.Count() > 0)
            {
                transfer = (from o in allmaterailTransfer
                            select new CommonLookupDto()
                            {
                                DisplayName = o.Name,
                                Id = o.Id
                            }).ToList();
            }

            return new List<CommonLookupDto>(transfer);
        }
        public async Task<List<CommonLookupDto>> GetAllDispatchTypeForDropdown()
        {
            List<DispatchType> allDispatchType = new List<DispatchType>();
            List<CommonLookupDto> dispatchType = new List<CommonLookupDto>();
            allDispatchType = await _dispatchTypeRepository.GetAll().ToListAsync();
            if (allDispatchType.Count() > 0)
            {
                dispatchType = (from o in allDispatchType
                                select new CommonLookupDto()
                                {
                                    DisplayName = o.Name,
                                    Id = o.Id
                                }).ToList();
            }
            return new List<CommonLookupDto>(dispatchType);
        }
        public async Task<List<CommonLookupDto>> GetAllLocationForDropdown()
        {
            List<Locations> allLocation = new List<Locations>();
            List<CommonLookupDto> locationList = new List<CommonLookupDto>();
            allLocation = await _locationRepository.GetAll().Where(x => x.IsActive == true).ToListAsync();
            if (allLocation.Count() > 0)
            {
                locationList = (from o in allLocation
                                select new CommonLookupDto()
                                {
                                    DisplayName = o.Name,
                                    Id = o.Id
                                }).ToList();
            }
            return new List<CommonLookupDto>(locationList);
        }
        public async Task<List<CommonLookupDto>> GetAllJobRefundForDropdown()
        {
            List<RefundReasons> allRefundReason = new List<RefundReasons>();
            List<CommonLookupDto> refundReasonList = new List<CommonLookupDto>();
            allRefundReason = await _refundReasonsRepository.GetAll().Where(x => x.IsActive == true).ToListAsync();
            if (allRefundReason.Count() > 0)
            {
                refundReasonList = (from o in allRefundReason
                                    select new CommonLookupDto()
                                    {
                                        DisplayName = o.Name,
                                        Id = o.Id
                                    }).ToList();
            }
            return new List<CommonLookupDto>(refundReasonList);
        }
        public async Task<List<CommonLookupDto>> GetAllTenderForDropdown()
        {
            List<Tender> tenderdata = new List<Tender>();
            List<CommonLookupDto> tenderList = new List<CommonLookupDto>();
            tenderdata = await _tenderRepository.GetAll().Where(x => x.IsActive && x.IsDeleted != true).ToListAsync();
            if (tenderdata.Count() > 0)
            {
                tenderList = (from o in tenderdata
                              select new CommonLookupDto()
                              {
                                  DisplayName = o.Name,
                                  Id = o.Id
                              }).ToList();
            }

            return new List<CommonLookupDto>(tenderList);
        }

        public async Task<List<CommonLookupDto>> GetSolarTypeWiseTenderForDropdown(int[] solarTypeId)
        {
            List<Tender> tenderdata = new List<Tender>();
            List<CommonLookupDto> tenderList = new List<CommonLookupDto>();
            tenderdata = await _tenderRepository.GetAll()
                //.WhereIf(!string.IsNullOrWhiteSpace(solarTypeId), e => e.SolarTypeId.Contains(solarTypeId))
                .Where(x => x.IsActive && x.IsDeleted != true && solarTypeId.Contains(x.SolarTypeId)).ToListAsync();
            if (tenderdata.Count() > 0)
            {
                tenderList = (from o in tenderdata
                              select new CommonLookupDto()
                              {
                                  DisplayName = o.Name,
                                  Id = o.Id
                              }).ToList();
            }
            return new List<CommonLookupDto>(tenderList);
        }

        public async Task<List<CommonLookupDto>> GetAllStockItemForDropdown()
        {
            List<StockItem> allStockItem = new List<StockItem>();
            List<CommonLookupDto> stockStock = new List<CommonLookupDto>();
            allStockItem = await _stockItemRepository.GetAll().Where(x => x.Active && x.IsDeleted != true).ToListAsync();
            if (allStockItem.Count() > 0)
            {
                stockStock = (from o in allStockItem
                              select new CommonLookupDto()
                              {
                                  DisplayName = o.Name,
                                  Id = o.Id
                              }).ToList();
            }
            return new List<CommonLookupDto>(stockStock);
        }
        public async Task<List<CommonLookupDto>> GetAllStockCategorForDropdown()
        {
            List<StockCategory> allStockCategory = new List<StockCategory>();
            List<CommonLookupDto> stockCategory = new List<CommonLookupDto>();
            allStockCategory = await _stockCategoryRepository.GetAll().Where(x => x.IsActive && x.IsDeleted != true).ToListAsync();
            if (allStockCategory.Count() > 0)
            {
                stockCategory = (from o in allStockCategory
                                 select new CommonLookupDto()
                                 {
                                     DisplayName = o.Name,
                                     Id = o.Id
                                 }).ToList();

            }
            return new List<CommonLookupDto>(stockCategory);
        }
        public async Task<List<CommonLookupDto>> GetAllStockItemLocationForDropdown()
        {
            List<StockItemLocation> allStockItemLocation = new List<StockItemLocation>();
            List<CommonLookupDto> stockCategory = new List<CommonLookupDto>();
            allStockItemLocation = await _stockItemLocationRepository.GetAll().Include(x => x.LocationsFk).Where(x => x.IsActive && x.IsDeleted != true).ToListAsync();
            if (allStockItemLocation.Count() > 0)
            {
                stockCategory = (from o in allStockItemLocation
                                 select new CommonLookupDto()
                                 {
                                     DisplayName = o.LocationsFk == null ? null : o.LocationsFk.Name,
                                     Id = o.LocationsFk.Id
                                 }).ToList();

            }
            return new List<CommonLookupDto>(stockCategory);
        }
        public async Task<List<CommonLookupDto>> GetAllCityWiseData(string suburb)
        {
            List<City> allcityItem = new List<City>();
            List<CommonLookupDto> suburbs = new List<CommonLookupDto>();
            bool checkvalue = int.TryParse(suburb, out int value);
            allcityItem = await _cityRepository.GetAll().Include(x => x.TalukaFk).ToListAsync();
            var queryResult = (from p in allcityItem
                               where !p.IsDeleted && p.IsActive && (checkvalue == true ? p.Id == Convert.ToInt32(suburb) : p.Name.ToLower().StartsWith(suburb.ToLower()))
                               select new
                               {
                                   Id = p.Id,
                                   Name = p.Name + "(" + p.TalukaFk.Name + ")",
                                   TalukaId = p.TalukaId
                               });
            //var result = new List<City>(); // = await _cityRepository.GetAllListAsync();//.Where(c => c.Id == Convert.ToInt32(suburb) && c.IsActive == true && c.IsDeleted != true); 
            //if (int.TryParse(suburb, out int value))
            //{
            // var result1 = await _cityRepository.GetAll().Where(c => c.Id == Convert.ToInt32(suburb) && c.IsActive == true && c.IsDeleted != true).Select(e => new
            //        { 
            //          Id = e.Id,
            //          Name = e.Name,
            //          TalukaId = e.TalukaId
            //        }).ToListAsync();
            //}
            //else
            //{
            //    result = await _cityRepository.GetAll().Where(c => (c.Name.ToLower().StartsWith(suburb.ToLower())) && c.IsActive == true && c.IsDeleted != true).ToListAsync();
            //}
            if (queryResult.Count() > 0)
            {
                suburbs = (from o in queryResult
                               //join o1 in _talukaRepository.GetAll() on o.TalukaId equals o1.Id into j1
                               //join o2 in _districtRepository.GetAll() on o1 equals o2.Id into j2
                               //from s1 in j1.DefaultIfEmpty()
                           select new CommonLookupDto()
                           {
                               Id = o.Id,
                               value = o.TalukaId,
                               DisplayName = o.Name.ToString() // + " || " + s1.Name.ToString() //+ " || " + s1.Name
                           }).ToList();
            }
            return new List<CommonLookupDto>(suburbs.ToList());
            //return await suburbs
            //    .Select(ResultDto => ResultDto == null || ResultDto.DisplayName == null ? "" : ResultDto.DisplayName.ToString()).ToListAsync();

        }

        public async Task<List<CommonLookupDto>> GetAllStateForDropdown(int id)
        {
            List<State> allStateItem = new List<State>();
            List<CommonLookupDto> stateList = new List<CommonLookupDto>();
            allStateItem = await _stateRepository.GetAll().ToListAsync();
            var queryResult = (from x in allStateItem
                               where !x.IsDeleted && x.IsActive && x.IsDeleted != true && x.Id == id || (x.Id != 0 && id == 0)
                               select new
                               {
                                   Id = x.Id,
                                   Name = x.Name
                               });
            if (queryResult.Count() > 0)
            {
                stateList = (from o in queryResult
                             select new CommonLookupDto()
                             {
                                 DisplayName = o.Name,
                                 Id = o.Id
                             }).ToList();

            }
            return new List<CommonLookupDto>(stateList);
        }
        public async Task<List<CommonLookupDto>> GetAllDistrictForDropdown(int id)
        {
            List<District> allDistrictItem = new List<District>();
            List<CommonLookupDto> districts = new List<CommonLookupDto>();
            allDistrictItem = await _districtRepository.GetAll().ToListAsync();
            var queryResult = (from x in allDistrictItem
                               where !x.IsDeleted && x.IsActive && x.IsDeleted != true && x.Id == id || (x.Id != 0 && id == 0)
                               select new
                               {
                                   Id = x.Id,
                                   Name = x.Name,
                                   StateId = x.StateId
                               });
            if (queryResult.Count() > 0)
            {
                districts = (from o in queryResult
                             select new CommonLookupDto()
                             {
                                 DisplayName = o.Name,
                                 Id = o.Id,
                                 value = o.StateId
                             }).ToList();
            }
            return new List<CommonLookupDto>(districts);
        }
        public async Task<List<CommonLookupDto>> GetAllTalukaForDropdown(int[] id)
        {
            List<Taluka> allTalukaItem = new List<Taluka>();
            List<CommonLookupDto> Talukas = new List<CommonLookupDto>();
            allTalukaItem = await _talukaRepository.GetAll().ToListAsync();
            var queryResult = (from x in allTalukaItem
                               where !x.IsDeleted && x.IsActive && x.IsDeleted != true && (id.Contains(x.Id) || id.Contains(x.DistrictId)) || (x.Id != 0 && id.Contains(0))
                               select new
                               {
                                   Id = x.Id,
                                   Name = x.Name,
                                   DistrictId = x.DistrictId
                               });
            if (queryResult.Count() > 0)
            {
                Talukas = (from o in queryResult
                           select new CommonLookupDto()
                           {
                               DisplayName = o.Name,
                               Id = o.Id,
                               value = o.DistrictId
                           }).ToList();
            }
            return new List<CommonLookupDto>(Talukas);
        }
        public async Task<List<CommonLookupDto>> GetAllLeadTypeForDropdown()
        {
            List<LeadType> allLeadType = new List<LeadType>();
            List<CommonLookupDto> leadtypelist = new List<CommonLookupDto>();
            allLeadType = await _leadTypeRepository.GetAll().ToListAsync();
            var queryResult = (from x in allLeadType
                               where !x.IsDeleted && x.IsActive && x.IsDeleted != true
                               select new
                               {
                                   Id = x.Id,
                                   Name = x.Name
                               });
            if (queryResult.Count() > 0)
            {
                leadtypelist = (from o in queryResult
                                select new CommonLookupDto()
                                {
                                    DisplayName = o.Name,
                                    Id = o.Id
                                }).ToList();

            }
            return new List<CommonLookupDto>(leadtypelist);
        }

        public async Task<List<CommonLookupDto>> GetAllSolarTypeForDropdown()
        {
            List<SolarType> allsolartype = new List<SolarType>();
            List<CommonLookupDto> solartypes = new List<CommonLookupDto>();
            allsolartype = await _solartypeRepository.GetAll().Where(x => x.IsActive && x.IsDeleted != true).ToListAsync();
            if (allsolartype.Count() > 0)
            {

                solartypes = (from o in allsolartype
                              select new CommonLookupDto()
                              {
                                  DisplayName = o.Name,
                                  Id = o.Id
                              }).ToList();

            }
            return new List<CommonLookupDto>(solartypes);
        }
        public async Task<List<CommonLookupDto>> GetAllDocumentLibratyForDropdown(int Id)
        {
            List<int> leadDocumentId = new List<int>();
            List<DocumentList> documentlibrary = new List<DocumentList>();
            List<CommonLookupDto> documentlib = new List<CommonLookupDto>();

            leadDocumentId = await _leadDocumentRepository.GetAll().Where(x => x.LeadDocumentId == Id).Select(x => x.DocumentId).ToListAsync();

            documentlibrary = await _documentListRepository.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true && !leadDocumentId.Contains(x.Id)).ToListAsync();
            if (documentlibrary.Count() > 0)
            {
                documentlib = (from o in documentlibrary
                               select new CommonLookupDto()
                               {
                                   DisplayName = o.Name,
                                   Id = o.Id,
                                   labeldata = o.DocumenSize + "#" + o.DocumenFormate + "#" + o.DocumenStorageUnit,

                               }).ToList();
            }
            return new List<CommonLookupDto>(documentlib);
        }
        public async Task<List<CommonLookupDto>> GetAllApplicationStatusForDropdown()
        {
            List<ApplicationStatus> applicationstatus = new List<ApplicationStatus>();
            List<CommonLookupDto> appstatus = new List<CommonLookupDto>();

            applicationstatus = await _lookup_applicationstatusRepository.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true).ToListAsync();
            if (applicationstatus.Count() > 0)
            {
                appstatus = (from o in applicationstatus
                             select new CommonLookupDto()
                             {
                                 DisplayName = o.ApplicationStatusName,
                                 Id = o.Id
                             }).ToList();
            }
            return new List<CommonLookupDto>(appstatus);
        }
        public async Task<List<CommonLookupDto>> GetAllTeamForDropdown()
        {
            List<Teams> allteam = new List<Teams>();
            List<CommonLookupDto> teams = new List<CommonLookupDto>();

            allteam = await _teamRepositoryy.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true).ToListAsync();
            if (allteam.Count() > 0)
            {
                teams = (from o in allteam
                         select new CommonLookupDto()
                         {
                             DisplayName = o.Name,
                             Id = o.Id
                         }).ToList();
            }
            return new List<CommonLookupDto>(teams);
        }

        public List<CommonLookupDto> GetAllTableForDropdown()
        {
            //solarcrmDbContext db = new solarcrmDbContext();
            //DbContextOptions<solarcrmDbContext> options = new DbContextOptions<solarcrmDbContext>();
            //using (var db = new solarcrmDbContext(options))
            //{
            // var results = _dbContextProvider.GetDbContext().Database.ExecuteSqlRawAsync("SELECT name FROM sys.tables ORDER BY name");

            List<CommonLookupDto> tablename1 = new List<CommonLookupDto>();
            var tableNames = _dbContextProvider.GetDbContext().Model.GetEntityTypes().Distinct();
            //.Select(t => t.GetTableName())
            //.Distinct();

            if (tableNames.Count() > 0)
            {

            }
            tablename1 = (from o in tableNames.ToList()
                          select new CommonLookupDto()
                          {
                              DisplayName = o.GetTableName(),   //Select(x => x.GetType().Name).ToList() //o.ToString/.FirstOrDefault().ToString(),
                              labeldata = o.Name //o.FirstOrDefault()//o
                          }).ToList();


            return new List<CommonLookupDto>(tablename1);
            //}
            // return new List<CommonLookupDto>(await teams.ToListAsync());
        }


        public List<CommonLookupDto> GetAllTableFieldForDropdown(string clrEntityType)
        {


            var entityTypes = _dbContextProvider.GetDbContext().Model.FindEntityType(clrEntityType).GetProperties()
                        .Select(property => property.Name)
                        .ToArray();
            //var entityType = _dbContextProvider.GetDbContext().Model.FindEntityType(clrEntityType).ClrType; // _dbContextProvider.GetDbContext().Model.FindEntityType("Chadwick.Database.Entities." + clrEntityType);

            DataTable dt = new DataTable();


            // Column info 
            foreach (var property in entityTypes)
            {
                dt.Columns.Add(property);

            };

            string[] columnNames = (from dc in dt.Columns.Cast<DataColumn>()
                                    select dc.ColumnName).ToArray();

            var tablename1 = from o in columnNames
                             select new CommonLookupDto()
                             {
                                 DisplayName = o,
                                 Id = 0 //o.FirstOrDefault()//o
                             };
            return new List<CommonLookupDto>(tablename1);
        }
        public async Task<List<CommonLookupDto>> GetAllHeightStructureForDropdown()
        {
            List<HeightStructure> allheight = new List<HeightStructure>();
            List<CommonLookupDto> heights = new List<CommonLookupDto>();
            allheight = await _heightstructureRepository.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true).ToListAsync();
            if (allheight.Count() > 0)
            {
                heights = (from o in allheight
                           select new CommonLookupDto()
                           {
                               DisplayName = o.Name,
                               Id = o.Id
                           }).ToList();
            }
            return new List<CommonLookupDto>(heights);
        }
        public async Task<List<CommonLookupDto>> GetAllPaymentModeDropdown()
        {
            List<PaymentMode> allpaymentMode = new List<PaymentMode>();
            List<CommonLookupDto> paymentMode = new List<CommonLookupDto>();

            allpaymentMode = await _paymentModeRepository.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true).ToListAsync();
            if (allpaymentMode.Count() > 0)
            {
                paymentMode = (from o in allpaymentMode
                               select new CommonLookupDto()
                               {
                                   DisplayName = o.Name,
                                   Id = o.Id
                               }).ToList();
            }
            return new List<CommonLookupDto>(paymentMode);
        }
        public async Task<List<CommonLookupDto>> GetAllPaymentTypeDropdown()
        {
            List<PaymentType> allpaymentType = new List<PaymentType>();
            List<CommonLookupDto> paymentType = new List<CommonLookupDto>();
            allpaymentType = await _paymentTypeRepository.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true).ToListAsync();
            if (allpaymentType.Count() > 0)
            {
                paymentType = (from o in allpaymentType
                               select new CommonLookupDto()
                               {
                                   DisplayName = o.Name,
                                   Id = o.Id
                               }).ToList();
            }
            return new List<CommonLookupDto>(paymentType);
        }

        public async Task<List<CommonLookupDto>> GetAllDivisionFilterDropdown(int id)
        {
            List<Division> divisionResult = new List<Division>();
            List<CommonLookupDto> divisionList = new List<CommonLookupDto>();
            divisionResult = await _divisionRepository.GetAll().ToListAsync();
            if (id != 0)
            {
                divisionResult = divisionResult.Where(x => x.IsActive == true && x.IsDeleted != true && x.Id == id).ToList();
            }
            else
            {
                divisionResult = divisionResult.Where(x => x.IsActive == true && x.IsDeleted != true).ToList();
            }
            if (divisionResult.Count() > 0)
            {
                divisionList = (from o in divisionResult  //.Where(x=>x.Id == id)
                                select new CommonLookupDto()
                                {
                                    DisplayName = o.Name,
                                    Id = o.Id,
                                    value = o.CircleId
                                }).ToList();
            }
            return new List<CommonLookupDto>(divisionList);
        }
        public async Task<List<CommonLookupDto>> GetAllSubdivisionWiseData(string suburb)
        {
            List<SubDivision> subdivisionResult = new List<SubDivision>();
            List<CommonLookupDto> suburbs = new List<CommonLookupDto>();
            subdivisionResult = await _subdivisionRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(suburb), c => (c.Name.ToLower().Contains(suburb.ToLower()) || c.Id.ToString() == suburb) && c.IsActive == true && c.IsDeleted != true).ToListAsync();
            if (subdivisionResult.Count() > 0)
            {
                suburbs = (from o in subdivisionResult
                           join o1 in _divisionRepository.GetAll() on o.DivisionId equals o1.Id into j1
                           //join o2 in _districtRepository.GetAll() on o1 equals o2.Id into j2
                           from s1 in j1.DefaultIfEmpty()
                           select new CommonLookupDto()
                           {
                               Id = o.Id,
                               value = o.DivisionId,
                               DisplayName = o.Name.ToString() //+ " || " + s1.Name.ToString() //+ " || " + s1.Name
                           }).ToList();
            }
            return new List<CommonLookupDto>(suburbs);
        }
        public async Task<List<CommonLookupDto>> GetAllDiscomFilterDropdown(int id)
        {
            List<Discom> discomResult = new List<Discom>();
            List<CommonLookupDto> discomlist = new List<CommonLookupDto>();
            discomResult = await _discomRepository.GetAll().Where(x => x.IsActive == true && x.IsDeleted != true && x.Id == id || x.Id != id).ToListAsync();
            if (discomResult.Count() > 0)
            {
                discomlist = (from o in discomResult
                              select new CommonLookupDto()
                              {
                                  DisplayName = o.Name,
                                  Id = o.Id,
                              }).ToList();
            }
            return new List<CommonLookupDto>(discomlist);
        }
        public async Task<List<CommonLookupDto>> GetCircleForDivisionWiseDropdown(int id)
        {
            List<Circle> circle = new List<Circle>();
            List<CommonLookupDto> circles = new List<CommonLookupDto>();
            circle = await _circleRepository.GetAll().Include(x=>x.DiscomIdFk).Where(x => x.IsActive == true && x.IsDeleted != true && x.Id == id).ToListAsync();
            if (circle.Count() > 0)
            {
                circles = (from o in circle
                           select new CommonLookupDto()
                           {
                               DisplayName = o.Name,
                               Id = o.Id,
                               value = o.DiscomIdFk == null ? 0 : o.DiscomIdFk.Id
                           }).ToList();
            }
            return new List<CommonLookupDto>(circles);
        }
        public async Task<List<CommonLookupDto>> GetDiscomWiseCircleFilterForDropdown(int[] discomFilterId)
        {
            List<Circle> circledata = new List<Circle>();
            List<CommonLookupDto> circleList = new List<CommonLookupDto>();
            circledata = await _circleRepository.GetAll()
               .Where(x => x.IsActive == true && x.IsDeleted != true && discomFilterId.Contains((int)x.DiscomId)).ToListAsync();
            if (circledata.Count() > 0)
            {
                circleList = (from o in circledata
                              select new CommonLookupDto()
                              {
                                  DisplayName = o.Name,
                                  Id = o.Id
                              }).ToList();
            }

            return new List<CommonLookupDto>(circleList);
        }
        public async Task<List<CommonLookupDto>> GetCircleWiseDivisionFilterForDropdown(int[] circleFilterId)
        {
            List<Division> divisiondata = new List<Division>();
            List<CommonLookupDto> divisionList = new List<CommonLookupDto>();
            divisiondata = await _divisionRepository.GetAll()
                .Where(x => x.IsActive == true && x.IsDeleted != true && circleFilterId.Contains((int)x.CircleId)).ToListAsync();
            if (divisiondata.Count() > 0)
            {
                divisionList = (from o in divisiondata
                                select new CommonLookupDto()
                                {
                                    DisplayName = o.Name,
                                    Id = o.Id
                                }).ToList();
            }
            return new List<CommonLookupDto>(divisionList);
        }
        public async Task<List<CommonLookupDto>> GetDivisionWiseSubDivisionFilterForDropdown(int[] divisionFilterId)
        {
            List<SubDivision> subdivisiondata = new List<SubDivision>();
            List<CommonLookupDto> subdivisionList = new List<CommonLookupDto>();
             subdivisiondata = await _subdivisionRepository.GetAll()
                .Where(x => x.IsActive == true && x.IsDeleted != true && divisionFilterId.Contains((int)x.DivisionId)).ToListAsync();
            if (subdivisiondata.Count() > 0)
            {
                subdivisionList = (from o in subdivisiondata
                                      select new CommonLookupDto()
                                      {
                                          DisplayName = o.Name,
                                          Id = o.Id
                                      }).ToList();
            }
            return new List<CommonLookupDto>(subdivisionList);
        }
        public async Task<List<CommonLookupDto>> GetAllLeadStatusDropdown()
        {
            List<CommonLookupDto> leadList = new List<CommonLookupDto>();
            leadList =  await _lookup_leadStatusRepository.GetAll()
                        .Select(leadStatus => new CommonLookupDto
                        {
                            Id = leadStatus.Id,
                            DisplayName = leadStatus == null || leadStatus.Status == null ? "" : leadStatus.Status.ToString()
                        }).ToListAsync();
            return leadList;
        }

        public async Task<List<CommonLookupDto>> GetAllLeadActivityDropdown()
        {
            List<CommonLookupDto> leadActivityList = new List<CommonLookupDto>();
            leadActivityList = await _lookup_leadActivityRepository.GetAll()
                        .Select(leadSource => new CommonLookupDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.LeadActivityName == null ? "" : leadSource.LeadActivityName.ToString()
                        }).ToListAsync();
            return leadActivityList;
        }
        public async Task<List<CommonLookupDto>> GetAllLeadSourceDropdown()
        {
            List<CommonLookupDto> leadsourceList = new List<CommonLookupDto>();
            leadsourceList = await _lookup_leadSourceRepository.GetAll()
                        .Select(leadSource => new CommonLookupDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
            return leadsourceList;
        }

        public async Task<List<CommonLookupDto>> GetAllLeadSourceOrganizationWise(int OrgId)
        {
            List<int?> multiorg = new List<int?>();
            List<LeadSource> leadsource = new List<LeadSource>();
            List<CommonLookupDto> leads = new List<CommonLookupDto>();
            multiorg = await _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.LeadSourceOrganizationId != null && x.OrganizationUnitId == OrgId).Select(x => x.LeadSourceOrganizationId).ToListAsync();

            leadsource = await _lookup_leadSourceRepository.GetAll().Where(x => multiorg.Contains(x.Id)).ToListAsync();

            leads = (from o in leadsource
                         select new CommonLookupDto()
                         {
                             Id = o.Id,
                             DisplayName = o == null || o.Name == null ? "" : o.Name.ToString()
                         }
                         ).ToList();

            return leads;
            //return await _lookup_leadSourceRepository.GetAll()
            //            .Select(leadSource => new CommonLookupDto
            //            {
            //                Id = leadSource.Id,
            //                DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
            //            }).ToListAsync();
        }
        public async Task<List<CommonLookupDto>> GetAllCancelReasonDropdown()
        {
            List<CommonLookupDto> cancelreasonList = new List<CommonLookupDto>();
            cancelreasonList = await _lookup_cancelReasonsRepository.GetAll()
                        .Select(leadSource => new CommonLookupDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
            return cancelreasonList;
        }

        public async Task<List<CommonLookupDto>> GetAllRejectReasonDropdown()
        {
            List<CommonLookupDto> rejectlreasonList = new List<CommonLookupDto>();
            rejectlreasonList = await _lookup_rejectReasonsRepository.GetAll()
                        .Select(leadSource => new CommonLookupDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
            return rejectlreasonList;
        }
        //Email Template Dropdown
        public async Task<List<CommonLookupDto>> GetAllEmailTemplateDropdown(int organizationId)
        {
            List<CommonLookupDto> emailTemplateList = new List<CommonLookupDto>();
            emailTemplateList = await _lookup_emailTemplatesRepository.GetAll().Where(x => x.OrganizationUnitId == organizationId)
                        .Select(leadSource => new CommonLookupDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.TemplateName == null ? "" : leadSource.TemplateName.ToString()
                        }).ToListAsync();
            return emailTemplateList;
        }
        public async Task<GetEmailTemplateForEditOutput> GetEmailTemplateForEditForEmail(EntityDto input)
        {
            var Email = await _lookup_emailTemplatesRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetEmailTemplateForEditOutput { EmailTemplates = ObjectMapper.Map<CreateOrEditEmailTemplateDto>(Email) };
            //GetEmailTemplateForEdit output = new GetEmailTemplateForEdit();
            //output.EmailTemplate.Body == Email.Body;
            //output.EmailTemplate.Subject == Email.Subject;
            //output.EmailTemplate.Id == Email.Id;
            //output.EmailTemplate.Body == Email.Body;
            return output;
        }
        //SMS Template Dropdown
        public async Task<List<CommonLookupDto>> GetAllSMSTemplateDropdown(int organizationId)
        {
            return await _lookup_smsTemplatesRepository.GetAll().Where(x => x.OrganizationUnitId == organizationId)
                        .Select(leadSource => new CommonLookupDto
                        {
                            Id = leadSource.Id,
                            DisplayName = leadSource == null || leadSource.Name == null ? "" : leadSource.Name.ToString()
                        }).ToListAsync();
        }
        public async Task<List<CommonLookupDto>> GetDiscomForDropdown()
        {
            var discom = _discomRepository.GetAll().Where(x => x.IsActive == true);

            var discoms = from o in discom
                          select new CommonLookupDto()
                          {
                              DisplayName = o.Name,
                              Id = o.Id,
                              //value = o.DiscomId
                          };

            return new List<CommonLookupDto>(await discoms.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> getDepartmentForDropdown()
        {
            var department = _departmentRepository.GetAll().Where(x => x.IsActive == true);

            var departmentList = from o in department
                                 select new CommonLookupDto()
                                 {
                                     Id = o.Id,
                                     DisplayName = o.Name
                                 };

            return new List<CommonLookupDto>(await departmentList.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> GetCircleForDropdown()
        {
            var circle = _circleRepository.GetAll().Where(x => x.IsActive == true);

            var circles = from o in circle
                          select new CommonLookupDto()
                          {
                              DisplayName = o.Name,
                              Id = o.Id
                          };

            return new List<CommonLookupDto>(await circles.ToListAsync());
        }
        #endregion


        //SMS Template Editor
        public async Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEditForSms(EntityDto input)
        {
            var smsTemplate = await _lookup_smsTemplatesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSmsTemplateForEditOutput { SmsTemplate = ObjectMapper.Map<CreateOrEditSmsTemplateDto>(smsTemplate) };

            return output;
        }
        public async Task<List<OrganizationUnitDto>> GetOrganizationUnit()
        {
            int UserId = (int)AbpSession.UserId;



            // var OrganizationList = new List<OrganizationUnitDto>();
            return await _organizationUnitRepository.GetAll()
                         .Select(organize => new OrganizationUnitDto
                         {
                             Id = organize.Id,
                             Code = organize.Code,
                             DisplayName = organize == null || organize.DisplayName == null ? "" : organize.DisplayName.ToString(),
                             ParentId = organize.ParentId
                         }).OrderBy(e => e.Id).ToListAsync();

        }


        public async Task<List<OrganizationUnitDto>> GetOrganizationUnitUserWise()
        {
            int UserId = (int)AbpSession.UserId;

            // var OrganizationList = new List<OrganizationUnitDto>();
            var OrganizationList = await _userOrganizationUnitRepository.GetAll().Where(x => x.UserId == UserId).Select(x => x.OrganizationUnitId).ToListAsync();
            var OrgList = await _organizationUnitRepository.GetAll().Where(x => OrganizationList.Contains(x.Id)).ToListAsync();

            var OraganizationData = (from o in OrgList
                                     select new OrganizationUnitDto()
                                     {
                                         Id = o.Id,
                                         Code = o.Code,
                                         DisplayName = o == null || o.DisplayName == null ? "" : o.DisplayName.ToString(),
                                         ParentId = o.ParentId
                                     }
                         ).ToList();
            return OraganizationData;
        }
        ////Get organization wise sales Manager
        public async Task<List<CommonLookupDto>> GetSalesManagerUser(int OId)
        {
            int UserId = (int)AbpSession.UserId;
            //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == UserId).Select(e => e.TeamId).ToList();
            //var UsersList = _userTeamRepository.GetAll().Where(e => TeamId.Contains(e.TeamId)).Select(e => e.UserId).ToList();

            var User_List = _userRepository
           .GetAll();

            var RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName));

            var CurrentRoleName = "Manager";
            //if (RoleName.Contains("Manager"))
            //{
            //    //User_List.Where(e => UsersList.Contains(e.Id));
            //    CurrentRoleName = "Channel Partners";
            //}

            var UserList = new List<CommonLookupDto>();
            try
            {
                //UserList = (from user in User_List
                //            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                //            from ur in urJoined.DefaultIfEmpty()
                //            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                //            from us in usJoined.DefaultIfEmpty()
                //            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                //            from uo in uoJoined.DefaultIfEmpty()
                //            where us != null && us.DisplayName == CurrentRoleName && uo != null && uo.OrganizationUnitId == OId
                //            select new CommonLookupDto()
                //            {
                //                value = user.Id,
                //                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                //            }).ToList();

                if (RoleName.Contains("Admin"))
                {
                    UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()
                                where us != null && us.DisplayName == CurrentRoleName && uo != null && uo.OrganizationUnitId == OId
                                select new CommonLookupDto()
                                {
                                    value = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).ToList();
                }
                else
                {
                    UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()

                                join ud in _userDetailsRepository.GetAll() on user.Id equals (int)ud.UserId into urDetails
                                from ud in urDetails.DefaultIfEmpty()

                                    //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                    //from ut in utJoined.DefaultIfEmpty()
                                where us != null && us.DisplayName == CurrentRoleName && uo != null && uo.OrganizationUnitId == OId && ud.UserId == UserId  //&& ud.ManageBy == UserId // && TeamId.Contains(ut.TeamId)
                                select new CommonLookupDto()
                                {
                                    value = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).ToList();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }


            return new List<CommonLookupDto>(
                UserList
            );
        }

        public async Task<List<CommonLookupDto>> GetSalesManagerForFilter(int OId, int? TeamId)
        {

            var User_List = _userRepository
          .GetAll();
            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);



            var UserList = new List<CommonLookupDto>();
            //UserList = (from user in User_List
            //            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //            from ur in urJoined.DefaultIfEmpty()
            //            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //            from us in usJoined.DefaultIfEmpty()
            //            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
            //            from uo in uoJoined.DefaultIfEmpty()
            //                //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
            //                //from ut in utJoined.DefaultIfEmpty()
            //            where (us != null && us.DisplayName == "Manager" && uo.OrganizationUnitId == OId)
            //            select new CommonLookupDto()
            //            {
            //                value = user.Id,
            //                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
            //            }).ToList();
            try
            {
                if (role.Contains("Admin"))
                {
                    UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()
                                    //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                    //from ut in utJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Manager" && uo.OrganizationUnitId == OId)
                                select new CommonLookupDto()
                                {
                                    value = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).ToList();
                }
                else
                {
                    UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()
                                    //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                    //from ut in utJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Manager" && uo != null && uo.OrganizationUnitId == OId) //&& ut.TeamId == Team) // 
                                select new CommonLookupDto()
                                {
                                    value = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).ToList();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


            return UserList;
        }

        public async Task<List<CommonLookupDto>> GetSalesRepForFilter(int OId, int? TeamId)
        {
            var User_List = _userRepository
             .GetAll();

            var User = User_List.Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            //var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);


            var UserList = new List<CommonLookupDto>();
            //UserList = (from user in User_List
            //            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
            //            from ur in urJoined.DefaultIfEmpty()
            //            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
            //            from us in usJoined.DefaultIfEmpty()
            //            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
            //            from uo in uoJoined.DefaultIfEmpty()
            //                //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
            //                //from ut in utJoined.DefaultIfEmpty()
            //            where (us != null && us.DisplayName == "Channel Partners" && uo.OrganizationUnitId == OId) //&& ut.TeamId == TeamId
            //            select new CommonLookupDto()
            //            {
            //                value = user.Id,
            //                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
            //            }).ToList();
            try
            {
                if (role.Contains("Admin"))
                {
                    UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()
                                    //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                    //from ut in utJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Channel Partners" && uo.OrganizationUnitId == OId) //&& ut.TeamId == TeamId
                                select new CommonLookupDto()
                                {
                                    value = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).ToList();
                }
                else
                {
                    UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                from uo in uoJoined.DefaultIfEmpty()
                                    //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                    //from ut in utJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Channel Partners" && uo != null && uo.OrganizationUnitId == OId) //&& ut.TeamId == Team) // 
                                select new CommonLookupDto()
                                {
                                    value = user.Id,
                                    DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                                }).ToList();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return UserList;
        }

        public async Task<List<CommonLookupDto>> GetSalesRepBySalesManagerid(int? id, int? OId)
        {
            var User = _userRepository.GetAll().Where(u => u.Id == id).FirstOrDefault();

            var cplist = await _userDetailsRepository.GetAll().Where(x => x.ManageBy == id).Select(x => x.UserId).ToListAsync();
            //var Team = _userTeamRepository.GetAll().Where(e => e.UserId == User.Id).Select(e => e.TeamId).FirstOrDefault();
            var User_List = _userRepository.GetAll();

            var UserList = new List<CommonLookupDto>();
            {
                UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                            from uo in uoJoined.DefaultIfEmpty()

                                //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                //from ut in utJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Channel Partners" && uo != null && uo.OrganizationUnitId == OId && cplist.Contains((int)user.Id))// && ut.TeamId == Team) // 
                            select new CommonLookupDto()
                            {
                                value = user.Id,
                                DisplayName = user.FullName == null || user.FullName == null ? "" : user.FullName.ToString()
                            }).ToList();
                //DistinctBy(d => d.Id).OrderBy(d => d.DisplayName).
            }
            return UserList;
        }

        public async Task<List<CommonLookupDto>> GetAllUserTypes()
        {
            var userType = _userTypeRepository.GetAll().Where(x => x.IsActive == true);

            var userTypes = from o in userType
                            select new CommonLookupDto()
                            {
                                DisplayName = o.Name,
                                Id = o.Id,
                            };

            return new List<CommonLookupDto>(await userTypes.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> GetDistrictByState(int id)
        {
            var district = _districtRepository.GetAll().Where(x => x.IsActive == true && x.StateId == id);

            var districtLists = from o in district
                                select new CommonLookupDto()
                                {
                                    DisplayName = o.Name,
                                    Id = o.Id,
                                    value = o.StateId
                                };

            return new List<CommonLookupDto>(await districtLists.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> GetTalukaByDistrict(int id)
        {
            var taluka = _talukaRepository.GetAll().Where(x => x.IsActive == true && x.DistrictId == id);

            var talukaLists = from o in taluka
                              select new CommonLookupDto()
                              {
                                  DisplayName = o.Name,
                                  Id = o.Id,
                                  value = o.DistrictId
                              };

            return new List<CommonLookupDto>(await talukaLists.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> GetCityByTaluka(int id)
        {
            var city = _cityRepository.GetAll().Where(x => x.IsActive == true && x.TalukaId == id);

            var cityLists = from o in city
                            select new CommonLookupDto()
                            {
                                DisplayName = o.Name,
                                Id = o.Id,
                                value = o.TalukaId
                            };

            return new List<CommonLookupDto>(await cityLists.ToListAsync());
        }
        public async Task<List<CommonLookupDto>> GetBillOfMaterial()
        {
            var city = _billofMaterialRepository.GetAll().Include(x => x.StockItemFk).Where(x => x.Active == true);

            var cityLists = from o in city
                            select new CommonLookupDto()
                            {
                                DisplayName = o.StockItemFk.Name,
                                Id = o.StockItemId
                            };

            return new List<CommonLookupDto>(await cityLists.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> GetEmployees(int id)
        {
            //var users = _userRepository.GetAll().Where(x => x.IsActive == true);
            var u = from user in _userRepository.GetAll()
                    join userDetails in _userDetailsRepository.GetAll() on user.Id equals (long)userDetails.UserId into uJoined
                    from userDetails in uJoined.DefaultIfEmpty()
                    where (userDetails.UserType == id)
                    select new CommonLookupDto
                    {
                        DisplayName = user.Name + " " + user.Surname,
                        Id = (int)user.Id
                    };

            //var userType = _userTypeRepository.GetAll().Where(x => x.Id == id).Select(x => new { });

            //var userLists = from o in users
            //                select new CommonLookupDto()
            //                {
            //                    DisplayName = o.Name + " " + o.Surname,
            //                    Id = (int)o.Id
            //                };

            return new List<CommonLookupDto>(await u.ToListAsync());
        }

        public async Task<List<CommonLookupDto>> GetEmployeesByRole(string RoleName)
        {
            var RoleId = _roleManager.Roles.Where(x => x.DisplayName == RoleName).Select(x => x.Id).FirstOrDefault();

            var users = _userRepository.GetAll().Where(x => x.IsActive == true && x.Roles.Any(r => r.RoleId == RoleId));

            var userLists = from o in users
                            select new CommonLookupDto()
                            {
                                DisplayName = o.Name + " " + o.Surname,
                                Id = (int)o.Id
                            };

            return new List<CommonLookupDto>(await userLists.ToListAsync());
        }



        public DocumentReturnValuesDto SaveCommonDocument(byte[] ByteArray, string filename, string SectionName, int? JobId, int? DocumentTypeId, int? TenantId)
        {
            try
            {
                if (ByteArray == null)
                {
                    throw new UserFriendlyException("There is no such file");
                }

                if (ByteArray.Length > MaxFileBytes)
                {
                    throw new UserFriendlyException("Document size exceeded");
                }

                ///var Path1 = _env.WebRootPath;
                //var MainFolder = @"f:\thesolarproductdocs";


                var Path1 = _env.WebRootPath;
                var MainFolder = Path1;
                MainFolder = MainFolder + "\\Documents";

                string TenantPath = "";
                string JobPath = "";
                string SignaturePath = "";
                var FinalFilePath = "";
                var TenantName = "";
                var JobNumber = "";
                if (SectionName == "DocumentLibrary" || SectionName == "Installer" || SectionName == "MISReport" || SectionName == "InvoiceImportFiles" || SectionName == "MyInstallerOrganizationDoc" || SectionName == "ProductItem" || SectionName == "ServiceSubCategory" || SectionName == "Service")
                {
                    if (AbpSession.TenantId != null)
                    {
                        TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    }
                    else
                    {
                        TenantName = _tenantRepository.GetAll().Where(e => e.Id == TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    }
                    TenantPath = MainFolder + "\\" + TenantName;
                    JobPath = TenantPath;
                    SignaturePath = JobPath + "\\" + SectionName;
                    FinalFilePath = SignaturePath + "\\" + filename;
                }
                else
                {
                    JobNumber = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.JobNumber).FirstOrDefault();
                    var tenantid = _jobRepository.GetAll().Where(e => e.Id == JobId).Select(e => e.TenantId).FirstOrDefault();
                    TenantName = _tenantRepository.GetAll().Where(e => e.Id == tenantid).Select(e => e.TenancyName).FirstOrDefault();

                    TenantPath = MainFolder + "\\" + TenantName;
                    JobPath = TenantPath + "\\" + JobNumber;
                    SignaturePath = "";
                    if (DocumentTypeId == 1 && SectionName == "Quotation")
                    {
                        SignaturePath = JobPath + "\\NearMap";
                    }
                    else if (DocumentTypeId != 1 && SectionName == "Quotation")
                    {
                        SignaturePath = JobPath + "\\Documents";
                    }
                    else
                    {
                        SignaturePath = JobPath + "\\" + SectionName;
                    }
                    FinalFilePath = SignaturePath + "\\" + filename;

                }

                if (System.IO.Directory.Exists(MainFolder))
                {
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(MainFolder);
                    if (System.IO.Directory.Exists(TenantPath))
                    {
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(TenantPath);
                        if (System.IO.Directory.Exists(JobPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(JobPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(ByteArray))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                                }
                            }
                        }
                    }
                }

                var doc = new DocumentReturnValuesDto();
                doc.FinalFilePath = FinalFilePath;
                doc.SignaturePath = SignaturePath;
                doc.filename = filename;
                doc.TenantName = TenantName;
                doc.JobNumber = JobNumber;
                return doc;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
