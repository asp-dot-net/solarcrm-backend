﻿using Abp.Domain.Repositories;
using Abp.Net.Mail;
using RestSharp;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadsActivityLog.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Common
{
    public class GeneralMethod : solarcrmAppServiceBase
    {
        private readonly IEmailSender _emailSender;
        //private readonly IRepository<AbpUserOrganizationUnits> _leadActivityLogRepository;
        
        public GeneralMethod(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }
        public async Task Sendsms(ActivityLogInput input)
        {
            try
            {
                var baseUrl = solarcrmConsts.SMSBaseUrl; //"https://insprl.com";
                var client = new RestClient(baseUrl);
                var request = new RestRequest("/api/sms/send-msg", Method.Post);
                //request.RequestFormat = DataFormat.Json;
                //request.AddParameter("Application/Json", myObject, ParameterType.RequestBody);
                //request.AddUrlSegment("name", "Gaurav");
                //request.AddUrlSegment("fathername", "Lt. Sh. Ramkrishan");


                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("Authorization", "SyMTYj4X9&8CA*P6s1iDl^75");
                request.AddParameter("client_id", "Csprl_DP2AEBO9FQN4UKT");
                request.AddParameter("sender_id", "APSOLR");
                request.AddParameter("mobile", input.Mobile);
                request.AddParameter("message", input.Body);//"Dear [Pravin test], Greetings from Australian Premium Solar (India) Pvt. Ltd. Your application for solar rooftop system is successfully registered with us. Thank you for choosing APS. Feel free to call on [07966006600] for any query. Regards, Team APS");
                request.AddParameter("msgtype", "txn");
                request.AddParameter("temp_id", "1707164602807396143");
                request.AddParameter("unicode", "0");
                request.AddParameter("flash", "0");
                //request.AddParameter("campaign_name", "My campaign 1001");
                var response = await client.ExecuteAsync(request);
            }
            catch(Exception ex)
            {
                throw ex;
            }           
        }

        public async Task SendEmail(ActivityLogInput input)
        {
            try
            {
                await _emailSender.SendAsync(new MailMessage
                {
                    From = new MailAddress(input.EmailFrom),
                    To = { input.EmailId }, ////{ "hiral.prajapati@meghtechnologies.com" },
                    Subject = input.Subject,//EmailBody.TemplateName + "The Solar Product Lead Notification",
                    Body = input.Body,
                    IsBodyHtml = true
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }
    }
}
