﻿using solarcrm.DataVaults.State.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.State.Exporting
{
    public interface IStateExcelExporter
    {
        FileDto ExportToFile(List<GetStateForViewDto> state);
    }
}
