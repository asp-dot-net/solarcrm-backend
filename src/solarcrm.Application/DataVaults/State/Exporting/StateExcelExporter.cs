﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.State.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.State.Exporting
{
    public class StateExcelExporter : NpoiExcelExporterBase, IStateExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StateExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStateForViewDto> leadType)
        {
            return CreateExcelPackage(
                "State.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("State"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, leadSource,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, leadType,
                        _ => _.states.Name
                        );
                });
        }
    }
}
