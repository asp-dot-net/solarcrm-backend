﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.State.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.State.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Common;

namespace solarcrm.DataVaults.State
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_State)]
    public class StateAppService : solarcrmAppServiceBase, IStateAppService
    {
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<District.District> _districtRepository;
        private readonly IRepository<Taluka.Taluka> _talukaRepository;
        private readonly IRepository<City.City> _cityRepository;

        private readonly IStateExcelExporter _stateExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public StateAppService(IRepository<State> StateRepository, IRepository<District.District> DistrictRepository, IRepository<Taluka.Taluka> TalukaRepository, IRepository<City.City> CityRepository, IStateExcelExporter StateExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _stateRepository = StateRepository;
            _districtRepository = DistrictRepository;
            _talukaRepository = TalukaRepository;
            _cityRepository = CityRepository;
            _stateExcelExporter = StateExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task CreateOrEdit(CreateOrEditStateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);


            }
            else
            {
                await Update(input);
            }
        }

       
        public async Task<PagedResultDto<GetStateForViewDto>> GetAll(GetAllStateInput input)
        {
            var filteredState = _stateRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredState = filteredState.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var States = from o in pagedAndFilteredState
                         select new GetStateForViewDto()
                         {
                             states = new StateDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime,

                             }
                         };

            var totalCount = await filteredState.CountAsync();

            return new PagedResultDto<GetStateForViewDto>(totalCount, await States.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_State_Edit)]
        public async Task<GetStateForEditOutput> GetStateForEdit(EntityDto input)
        {
            var states = await _stateRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStateForEditOutput { states = ObjectMapper.Map<CreateOrEditStateDto>(states) };

            return output;
        }

        public async Task<GetStateForViewDto> GetStateForView(int id)
        {
            var states = await _stateRepository.GetAsync(id);

            var output = new GetStateForViewDto { states = ObjectMapper.Map<StateDto>(states) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_State_ExportToExcel)]
        public async Task<FileDto> GetStateToExcel(GetAllStateForExcelInput input)
        {
            var filteredState = _stateRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredState
                         select new GetStateForViewDto()
                         {
                             states = new StateDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var stateListDtos = await query.ToListAsync();

            return _stateExcelExporter.ExportToFile(stateListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_State_Create)]
        protected virtual async Task Create(CreateOrEditStateDto input)
        {
            var state = ObjectMapper.Map<State>(input);

            if (AbpSession.TenantId != null)
            {
                state.TenantId = (int?)AbpSession.TenantId;
            }

            var stateId = await _stateRepository.InsertAndGetIdAsync(state);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 13; // State Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "State Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = stateId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_State_Edit)]
        protected virtual async Task Update(CreateOrEditStateDto input)
        {
            var states = await _stateRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 13; // state Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "State Updated Name :-" +states.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var stateActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (states.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = states.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stateActivityLogId;
                dataVaultsHistory.SectionId = 13; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (states.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = states.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = stateActivityLogId;
                dataVaultsHistory.SectionId = 13;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, states);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_State_Delete)]
        public async Task Delete(EntityDto input)
        {
            var StatteResult = await _stateRepository.FirstOrDefaultAsync(input.Id);
            if (input.Id != 0)
            {
                var districtListId = _districtRepository.GetAll().Where(x => x.StateId == input.Id).ToList();
                if (districtListId != null)
                {
                    foreach (var item in districtListId)
                    {
                        var talukaListId = _talukaRepository.GetAll().Where(x => x.DistrictId == item.Id).ToList();

                        if (talukaListId != null)
                        {
                            foreach (var item1 in talukaListId)
                            {
                                var cityListId = _cityRepository.GetAll().Where(x => x.TalukaId == item1.Id).ToList();
                                if (cityListId != null)
                                {
                                    foreach (var item2 in cityListId)
                                    {
                                        await _cityRepository.DeleteAsync(item2.Id);
                                    }

                                }
                                await _talukaRepository.DeleteAsync(item1.Id);
                            }

                        }
                        await _districtRepository.DeleteAsync(item.Id);
                    }
                }

                await _stateRepository.DeleteAsync(input.Id);
            }

            // var entityTypes = _dbContextProvider.GetDbContext().Districts.Select(t => t.StateId).ToList();


            //var disctrictList =  _dbContextProvider.GetDbContext().Districts.Select(e => e.StateId.Equals(input.Id)).ToList(); //Where(e => e.StateId.Equals(input.Id));
            //List.Add(entityTypes);
            //await _dbContextProvider.GetDbContext().Districts.AddRangeAsync(List);

            //await _dbContextProvider.GetDbContext().Districts.BulkUpdateAsync(District.District, options => {
            //    options.ColumnInputExpression = customer => new { customer.IsDeleted };
            //    options.ColumnPrimaryKeyExpression = customer => customer.StateId == input.Id;
            //});

            //BulkUpdateAsync(District , x=>x.ColumnPrimaryKeyExpression = List => List.id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 13; // state Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Created
            dataVaulteActivityLog.ActionNote = "State Deleted Name :-" + StatteResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<List<StateLookupTableDto>> GetAllStateForDropdown()
        {
            var allstate = _stateRepository.GetAll().Where(x => x.IsActive == true);

            var state = from o in allstate
                        select new StateLookupTableDto()
                        {
                            DisplayName = o.Name,
                            Id = o.Id
                        };

            return new List<StateLookupTableDto>(await state.ToListAsync());
        }
    }
}
