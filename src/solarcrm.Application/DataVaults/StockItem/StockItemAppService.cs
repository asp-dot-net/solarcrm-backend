﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.StockItem.Dto;
using solarcrm.DataVaults.StockItem.Exporting;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.Dto;
using solarcrm.DataVaults.StockItemLocation.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Organizations;
using Abp.Organizations;
using solarcrm.Common;
using solarcrm.DataVaults.LeadSource;
using System;
using System.IO;
using solarcrm.MultiTenancy;
using solarcrm.Storage;

namespace solarcrm.DataVaults.StockItem
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockItem)]
    public class StockItemAppService : solarcrmAppServiceBase, IStockItemAppService
    {
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IRepository<StockCategory.StockCategory> _stockCategoryRepository;
        private readonly IStockItemExcelExporter _stockItemExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<StockItemLocation> _stockItemLocationRepository;
        private readonly IRepository<Locations.Locations> _locationRepository;
        private readonly IRepository<MultipleFieldOrganizationUnitSelection> _multipleFieldOrganizationUnitSelectionRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager; 
        private readonly int _sectionId = 25; // StockItem Pages Name

        public StockItemAppService(IRepository<StockItem> stockItemRepository, IStockItemExcelExporter stockItemExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<StockCategory.StockCategory> stockCategoryRepository,
        IRepository<MultipleFieldOrganizationUnitSelection> multipleFieldOrganizationUnitSelectionRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
        IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<StockItemLocation> stockItemLocationRepository, IRepository<Locations.Locations> locationRepository, IRepository<Tenant> tenantRepository, ITempFileCacheManager tempFileCacheManager)
        {
            _stockItemRepository = stockItemRepository;
            _stockCategoryRepository = stockCategoryRepository;
            _stockItemExcelExporter = stockItemExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _stockItemLocationRepository = stockItemLocationRepository;
            _multipleFieldOrganizationUnitSelectionRepository = multipleFieldOrganizationUnitSelectionRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _locationRepository = locationRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _tenantRepository = tenantRepository;
        }
        public async Task<PagedResultDto<GetStockItemForViewDto>> GetAll(GetAllStockItemInput input)
        {
            var filteredStockItem = _stockItemRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter) || e.Model.Contains(input.Filter));

            var pagedAndFilteredStockItem = filteredStockItem.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var stockItems = from o in pagedAndFilteredStockItem
                             select new GetStockItemForViewDto()
                             {
                                 StockItem = new StockItemDto
                                 {
                                     Id = o.Id,
                                     Name = o.Name,
                                     Manufacturer = o.Manufacturer,
                                     Model = o.Model,
                                     Series = o.Series,
                                     Size = o.Size,
                                     Phase = o.Phase,
                                     Category = o.StockCategoryFk.Name,
                                     OrganizationUnitsName = _organizationUnitRepository.GetAll().Where(x => _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.StockItemOrganizationId != null && x.StockItemOrganizationId == o.Id).Select(x => x.OrganizationUnitId).ToList().Contains(x.Id)).Select(x => x.DisplayName).ToList(),
                                     Sequence = o.Seq,
                                     UOM = o.UOM,
                                     IsActive = o.Active,
                                     FileName = o.FileName,
                                     FilePath = o.FilePath,
                                 }
                             };

            var totalCount = await stockItems.CountAsync();

            return new PagedResultDto<GetStockItemForViewDto>(totalCount, await stockItems.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockItem_ExportToExcel)]
        public async Task<FileDto> GetStockItemToExcel(GetAllStockItemForExcelInput input)
        {
            var filteredStockItem = _stockItemRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredStockItem
                         select new GetStockItemForViewDto()
                         {
                             StockItem = new StockItemDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 Manufacturer = o.Manufacturer,
                                 Model = o.Model,
                                 Series = o.Series,
                                 Size = o.Size,
                                 Phase = o.Phase,
                                 Category = o.StockCategoryFk.Name,
                                 Sequence = o.Seq,
                                 UOM = o.UOM
                             }
                         });

            var stockItemListDtos = await query.ToListAsync();

            return _stockItemExcelExporter.ExportToFile(stockItemListDtos);
        }

       
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockItem_Edit)]
        public async Task<GetStockItemForEditOutput> GetStockItemForEdit(EntityDto input)
        {
            var stockItem = await _stockItemRepository.FirstOrDefaultAsync(input.Id);

            var stockItemLocation = await _stockItemLocationRepository.GetAll().Where(e => e.StockItemId == stockItem.Id).ToListAsync();

            var output = new GetStockItemForEditOutput
            {
                StockItem = ObjectMapper.Map<CreateOrEditStockItemDto>(stockItem),
                //StockItemLocation = ObjectMapper.Map<List<CreateOrEditStockItemLocationDto>>(stockItemLocation)
            };

            var query = (from o in stockItemLocation
                        select new CreateOrEditStockItemLocationDto()
                        {
                            Id = o.Id,
                            StockItemId = o.StockItemId,
                            LocationId = o.LocationId,
                            LocationName = _locationRepository.GetAll().Where(e => e.Id == o.LocationId).Select(e => e.Name).FirstOrDefault(),
                            MinQty = o.MinQty,
                            IsActive = o.IsActive

                        }).ToList();



            output.StockItem.StockItemLocations = query;
            output.StockItem.OrganizationUnits = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.StockItemOrganizationId == input.Id).Select(x => x.OrganizationUnitId).ToList();
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockItem_Create, AppPermissions.Pages_Tenant_DataVaults_StockItem_Edit)]
        public async Task CreateOrEdit(CreateOrEditStockItemDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Create)]
        protected virtual async Task Create(CreateOrEditStockItemDto input)
        {

            var StockItem = ObjectMapper.Map<StockItem>(input);

            if (AbpSession.TenantId != null)
            {
                StockItem.TenantId = (int)AbpSession.TenantId;
            }
            var stockItemId = await _stockItemRepository.InsertAndGetIdAsync(StockItem);
            var Itemlist = await _stockItemRepository.GetAll().Where(x => x.CategoryId == input.CategoryId).OrderBy(x => x.Seq).ToListAsync();
            if (Itemlist != null)
            {
                var ListUpdate = new List<StockItem>();

                foreach (var item in Itemlist)
                {
                    item.Seq = ListUpdate.Count() + 1;
                    ListUpdate.Add(item);
                    await _stockItemRepository.UpdateAsync(item);
                }
            }
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId;
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Stock Item Created Name :-" +input.Name;
            dataVaulteActivityLog.SectionValueId = stockItemId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);

            if (stockItemId > 0)
            {
                var locationList = await _locationRepository.GetAll().ToListAsync();
                foreach (var location in locationList)
                {
                    CreateOrEditStockItemLocationDto createOrEditStockItemLocationDto = new CreateOrEditStockItemLocationDto();
                    var stockItemLocation = ObjectMapper.Map<StockItemLocation>(createOrEditStockItemLocationDto);
                    if (AbpSession.TenantId != null)
                    {
                        stockItemLocation.TenantId = (int)AbpSession.TenantId;
                    }
                    stockItemLocation.StockItemId = stockItemId;
                    stockItemLocation.LocationId = location.Id;
                    stockItemLocation.MinQty = 0;
                    stockItemLocation.IsActive = true;

                    await _stockItemLocationRepository.InsertAsync(stockItemLocation);

                    foreach (var item in input.OrganizationUnits)
                    {
                        try
                        {
                            MultipleFieldOrganizationUnitSelection leadstock = new MultipleFieldOrganizationUnitSelection();
                            leadstock.StockItemOrganizationId = stockItemId;
                            leadstock.OrganizationUnitId = item;
                            leadstock.TenantId = (int?)AbpSession.TenantId;
                            await _multipleFieldOrganizationUnitSelectionRepository.InsertOrUpdateAsync(leadstock);
                        }
                        catch (System.Exception ex)
                        {
                            throw ex;
                        }
                    }

                    //Add Activity Log
                    DataVaulteActivityLog dataVaulteActivityLog1 = new DataVaulteActivityLog();
                    if (AbpSession.TenantId != null)
                    {
                        dataVaulteActivityLog1.TenantId = (int)AbpSession.TenantId;
                    }
                    dataVaulteActivityLog1.SectionId = _sectionId;
                    dataVaulteActivityLog1.ActionId = (int?)CrudActionConsts.Created; //1-Created, 2-Updated, 3-Deleted
                    dataVaulteActivityLog1.ActionNote = "Stock Item Created for " + location.Name + " Location";
                    dataVaulteActivityLog1.SectionValueId = stockItemId;
                    //dataVaulteActivityLog1.CreatorUserId = (int)args.User.UserId;
                    await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog1);
                }
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Edit)]
        protected virtual async Task Update(CreateOrEditStockItemDto input)
        {
            var stockItem = await _stockItemRepository.FirstOrDefaultAsync((int)input.Id);
            var OrganizationUnits = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.StockItemOrganizationId == stockItem.Id).Select(e => e.OrganizationUnitId).ToList();
            string inputOrg = "";
            string stockItemOrg = "";
            foreach (var organizationUnit in input.OrganizationUnits)
            {
                if (inputOrg == "")
                {
                    inputOrg = _organizationUnitRepository.GetAll().Where(e => e.Id == organizationUnit).Select(e => e.DisplayName).FirstOrDefault();
                }
                else
                {
                    inputOrg = inputOrg + ", " + _organizationUnitRepository.GetAll().Where(e => e.Id == organizationUnit).Select(e => e.DisplayName).FirstOrDefault();
                }
            }
            foreach (var organizationUnit in OrganizationUnits)
            {
                if (stockItemOrg == "")
                {
                    stockItemOrg = _organizationUnitRepository.GetAll().Where(e => e.Id == organizationUnit).Select(e => e.DisplayName).FirstOrDefault();
                }
                else
                {
                    stockItemOrg = stockItemOrg + ", " + _organizationUnitRepository.GetAll().Where(e => e.Id == organizationUnit).Select(e => e.DisplayName).FirstOrDefault();
                }
            }
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId; // stock category Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Stock Item Updated :-" +stockItem.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var stockItemActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            //Seq Update
            var Itemlist = await _stockItemRepository.GetAll().Where(x => x.CategoryId == input.CategoryId).OrderBy(x => x.Seq).ToListAsync();
            if (Itemlist != null)
            {
                var ListUpdate = new List<StockItem>();

                foreach (var item in Itemlist)
                {
                    item.Seq = ListUpdate.Count() + 1;
                    ListUpdate.Add(item);
                    await _stockItemRepository.UpdateAsync(item);
                }
            }
            foreach (var item in input.OrganizationUnits)
            {
                try
                {
                    MultipleFieldOrganizationUnitSelection leadSourcerg = new MultipleFieldOrganizationUnitSelection();

                    var checkorg = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.StockItemOrganizationId == stockItem.Id && x.OrganizationUnitId == item).FirstOrDefault();
                    if (checkorg != null)
                    {
                        checkorg.StockItemOrganizationId = stockItem.Id;
                        checkorg.OrganizationUnitId = item;
                        checkorg.TenantId = (int?)AbpSession.TenantId;
                        await _multipleFieldOrganizationUnitSelectionRepository.InsertOrUpdateAsync(checkorg);
                    }
                    else
                    {
                        leadSourcerg.StockItemOrganizationId = stockItem.Id;
                        leadSourcerg.OrganizationUnitId = item;
                        leadSourcerg.TenantId = (int?)AbpSession.TenantId;
                        await _multipleFieldOrganizationUnitSelectionRepository.InsertOrUpdateAsync(leadSourcerg);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
            var checkorg1 = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.StockItemOrganizationId == stockItem.Id && !input.OrganizationUnits.Contains(x.OrganizationUnitId)).ToList();
            if (checkorg1.Count() > 0)
            {
                foreach (var item in checkorg1)
                {
                    await _multipleFieldOrganizationUnitSelectionRepository.DeleteAsync(item);
                }
            }
            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (stockItem.CategoryId != input.CategoryId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Category";
                dataVaultsHistory.PrevValue = _stockCategoryRepository.GetAll().Where(e => e.Id == stockItem.CategoryId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _stockCategoryRepository.GetAll().Where(e => e.Id == input.CategoryId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = stockItem.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Model != input.Model)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Model";
                dataVaultsHistory.PrevValue = stockItem.Model;
                dataVaultsHistory.CurValue = input.Model;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Manufacturer != input.Manufacturer)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Manufacturer";
                dataVaultsHistory.PrevValue = stockItem.Manufacturer;
                dataVaultsHistory.CurValue = input.Manufacturer;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Series != input.Series)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Series";
                dataVaultsHistory.PrevValue = stockItem.Series;
                dataVaultsHistory.CurValue = input.Series;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Size != input.Size)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Size";
                dataVaultsHistory.PrevValue = stockItem.Size.ToString();
                dataVaultsHistory.CurValue = input.Size.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Phase != input.Phase)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Phase";
                dataVaultsHistory.PrevValue = stockItem.Phase.ToString();
                dataVaultsHistory.CurValue = input.Phase.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Active != input.Active)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Active";
                dataVaultsHistory.PrevValue = stockItem.Active.ToString();
                dataVaultsHistory.CurValue = input.Active.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.Seq != input.Seq)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Seq";
                dataVaultsHistory.PrevValue = stockItem.Seq.ToString();
                dataVaultsHistory.CurValue = input.Seq.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (stockItem.UOM != input.UOM)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "UOM";
                dataVaultsHistory.PrevValue = stockItem.UOM;
                dataVaultsHistory.CurValue = input.UOM;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (OrganizationUnits != input.OrganizationUnits)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "OrganizationUnits";
                dataVaultsHistory.PrevValue = stockItemOrg;
                dataVaultsHistory.CurValue = inputOrg;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = 1;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

           
            #endregion

            ObjectMapper.Map(input, stockItem);

            foreach (var item in input.StockItemLocations)
            {
                var stockItemLocation = await _stockItemLocationRepository.FirstOrDefaultAsync((int)item.Id);

                if (stockItemLocation.MinQty != item.MinQty)
                {
                    DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                    if (AbpSession.TenantId != null)
                    {
                        dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                    }
                    dataVaultsHistory.FieldName = "stockItemLocation.MinQty For " + item.LocationName;
                    dataVaultsHistory.PrevValue = stockItemLocation.MinQty.ToString();
                    dataVaultsHistory.CurValue = item.MinQty.ToString();
                    dataVaultsHistory.Action = "Edit";
                    dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                    dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                    dataVaultsHistory.SectionId = 1;
                    dataVaultsHistory.SectionValueId = input.Id;
                    List.Add(dataVaultsHistory);
                }
                
                if (stockItemLocation.IsActive != item.IsActive)
                {
                    DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                    if (AbpSession.TenantId != null)
                    {
                        dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                    }
                    dataVaultsHistory.FieldName = "stockItemLocation.IsActive For "+item.LocationName;
                    dataVaultsHistory.PrevValue = stockItemLocation.IsActive.ToString();
                    dataVaultsHistory.CurValue = item.IsActive.ToString();
                    dataVaultsHistory.Action = "Edit";
                    dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                    dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                    dataVaultsHistory.SectionId = 1;
                    dataVaultsHistory.SectionValueId = input.Id;
                    List.Add(dataVaultsHistory);
                }

                ObjectMapper.Map(item, stockItemLocation);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockItem_Delete)]
        public async Task Delete(EntityDto input)
        {
            var stockItemResult = await _stockItemRepository.FirstOrDefaultAsync((int)input.Id);
            await _stockItemRepository.DeleteAsync(input.Id);
            await _stockItemLocationRepository.DeleteAsync(o => o.StockItemId == input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId;
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;// Deleted
            dataVaulteActivityLog.ActionNote = "Stock Items Deleted Name :-" + stockItemResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task SaveStockIteamDocument(string FileToken, string FileNames, int id)
        {
            var ProductItems = _stockItemRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
            //string FileName = ProductItems.Model + DateTime.Now.Ticks + ".pdf";
            byte[] ByteArray = _tempFileCacheManager.GetFile(FileToken);
            var Path1 = solarcrm.solarcrmConsts.DocumentPath;
            var MainFolder = Path1;
            MainFolder = MainFolder + "\\Documents";


            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
            string FileName = DateTime.Now.Ticks + "_" + FileNames;
            string TenantPath = MainFolder + "\\" + TenantName + "\\";
            string SignaturePath = TenantPath + "StockItem\\";
            var FinalFilePath = SignaturePath + "\\" + FileName;



            if (System.IO.Directory.Exists(MainFolder))
            {
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(SignaturePath))
                    {
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(SignaturePath);
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);

                    if (System.IO.Directory.Exists(SignaturePath))
                    {
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(SignaturePath);
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                }
            }
            else
            {
                System.IO.Directory.CreateDirectory(MainFolder);
                if (System.IO.Directory.Exists(TenantPath))
                {
                    if (System.IO.Directory.Exists(SignaturePath))
                    {
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(SignaturePath);
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TenantPath);

                    if (System.IO.Directory.Exists(SignaturePath))
                    {
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(SignaturePath);
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), ByteArray);
                        }
                    }
                }
            }


            ProductItems.FileName = FileName;
            ProductItems.FilePath = "\\Documents" + "\\" + TenantName + "\\StockItem\\";
            await _stockItemRepository.UpdateAsync(ProductItems);
        }
    }
}
