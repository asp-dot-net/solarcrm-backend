﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.StockItem.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockItem.Exporting
{
    public class StockItemExcelExporter : NpoiExcelExporterBase, IStockItemExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockItemExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStockItemForViewDto> stockItems)
        {
            return CreateExcelPackage(
                "StockItems.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("StockItems"));

                    AddHeader(
                        sheet,
                        L("Category"),
                        L("Name"),
                        L("Manufacturer"),
                        L("Model"),
                        L("Series"),
                        L("Size"),
                        L("Phase")
                        );

                    AddObjects(
                        sheet, stockItems,
                        _ => _.StockItem.Category,
                        _ => _.StockItem.Name,
                        _ => _.StockItem.Manufacturer,
                        _ => _.StockItem.Model,
                        _ => _.StockItem.Series,
                        _ => _.StockItem.Size,
                        _ => _.StockItem.Phase
                        );
                });
        }
    }
}
