﻿using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solarcrm.DataVaults.StockItem.Dto;

namespace solarcrm.DataVaults.StockItem.Exporting
{
    public interface IStockItemExcelExporter
    {
        FileDto ExportToFile(List<GetStockItemForViewDto> stockItems);
    }
}
