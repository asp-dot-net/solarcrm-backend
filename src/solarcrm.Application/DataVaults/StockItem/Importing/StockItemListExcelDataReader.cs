﻿using Abp.Localization;
using Abp.Localization.Sources;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.StockItem.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.SS.UserModel;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockItem.Importing
{
    public class StockItemListExcelDataReader : NpoiExcelImporterBase<ImportStockItemDto>, IStockItemListExcelDataReader
    {
		private readonly ILocalizationSource _localizationSource;

		public StockItemListExcelDataReader(ILocalizationManager localizationManager)
		{
			_localizationSource = localizationManager.GetSource(solarcrmConsts.LocalizationSourceName);
		}

		public List<ImportStockItemDto> GetStockItemsFromExcel(byte[] fileBytes, string filename)
		{
			return ProcessExcelFile(fileBytes, ProcessExcelRow, filename);
		}

		private ImportStockItemDto ProcessExcelRow(ISheet worksheet, int row)
		{
			if (IsRowEmpty(worksheet, row))
			{
				return null;
			}

			var exceptionMessage = new StringBuilder();
			var stockItem = new ImportStockItemDto();

			IRow roww = worksheet.GetRow(row);
			List<ICell> cells = roww.Cells;
			List<string> rowData = new List<string>();

			for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
			{
				ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
				rowData.Add(cell.ToString());
			}

			try
			{
				stockItem.Name = rowData[0].Trim();
				stockItem.Manufacturer = rowData[1];
				stockItem.Model = rowData[2];
				stockItem.Series = rowData[3];
				stockItem.Size = rowData[4];
				stockItem.Phase = rowData[5];
				stockItem.Category = rowData[6];
				
				//worksheet.Workbook.MissingCellPolicy = MissingCellPolicy.RETURN_BLANK_AS_NULL;
				//lead.CompanyName = GetRequiredValueFromRowOrNull(worksheet, row, 0, nameof(lead.CompanyName), exceptionMessage);
				//lead.Email = GetRequiredValueFromRowOrNull(worksheet, row, 1, nameof(lead.Email), exceptionMessage);
				//worksheet.GetRow(row).Cells[2].SetCellType(CellType.String);
				//lead.Phone = GetRequiredValueFromRowOrNull(worksheet, row, 2, nameof(lead.Email), exceptionMessage);
				//worksheet.GetRow(row).Cells[3].SetCellType(CellType.String);
				//lead.Mobile = GetRequiredValueFromRowOrNull(worksheet, row, 3, nameof(lead.Email), exceptionMessage);
				//lead.Address = GetRequiredValueFromRowOrNull(worksheet, row, 4, nameof(lead.Address), exceptionMessage);
				//lead.Suburb = GetRequiredValueFromRowOrNull(worksheet, row, 5, nameof(lead.Suburb), exceptionMessage);
				//lead.State = GetRequiredValueFromRowOrNull(worksheet, row, 6, nameof(lead.State), exceptionMessage);
				//worksheet.GetRow(row).Cells[7].SetCellType(CellType.String);
				//lead.PostCode = worksheet.GetRow(row).Cells[7]?.StringCellValue;
				//lead.LeadSource = GetRequiredValueFromRowOrNull(worksheet, row, 8, nameof(lead.LeadSource), exceptionMessage);
				//lead.SystemType = GetRequiredValueFromRowOrNull(worksheet, row, 9, nameof(lead.SystemType), exceptionMessage);
				//lead.RoofType = GetRequiredValueFromRowOrNull(worksheet, row, 10, nameof(lead.RoofType), exceptionMessage);
				//lead.AngleType = GetRequiredValueFromRowOrNull(worksheet, row, 11, nameof(lead.AngleType), exceptionMessage);
				//lead.StoryType = GetRequiredValueFromRowOrNull(worksheet, row, 12, nameof(lead.StoryType), exceptionMessage);
				//lead.HouseAgeType = GetRequiredValueFromRowOrNull(worksheet, row, 13, nameof(lead.HouseAgeType), exceptionMessage);
				//lead.Requirements = GetRequiredValueFromRowOrNull(worksheet, row, 14, nameof(lead.Requirements), exceptionMessage);				
			}
			catch (System.Exception exception)
			{
				stockItem.Exception = exception.Message;
			}

			return stockItem;
		}

		private string GetLocalizedExceptionMessagePart(string parameter)
		{
			return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
		}

		private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		{
			var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
			if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
			{
				return cellValue;
			}

			exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
			return null;
		}

		private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		{
			var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

			return cellValue;

		}

		private bool IsRowEmpty(ISheet worksheet, int row)
		{
			var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
			return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
		}
	}
}
