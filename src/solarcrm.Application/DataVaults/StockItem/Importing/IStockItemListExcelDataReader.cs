﻿using Abp.Dependency;
using solarcrm.DataVaults.StockItem.Importing.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.StockItem.Importing
{
    public interface IStockItemListExcelDataReader : ITransientDependency
    {
        List<ImportStockItemDto> GetStockItemsFromExcel(byte[] fileBytes , string filename);
    }
}
