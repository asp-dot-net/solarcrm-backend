﻿using solarcrm.DataVaults.StockItem.Importing.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.StockItem.Importing
{
    public interface IInvalidStockItemExporter
    {
        FileDto ExportToFile(List<ImportStockItemDto> stockItemListDtos);
    }
}
