﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockItem.Importing.Dto
{
    public class ImportStockItemDto
    {
        public string Name { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public string Series { get; set; }

        public string Size { get; set; }

        public string Phase { get; set; }

        public string Category { get; set; }

        //public virtual bool Active { get; set; } //By Default All Items Will Be Is Active

        //public virtual string FileName { get; set; }

        //public virtual string FileType { get; set; }

        //public virtual string FilePath { get; set; }

        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
