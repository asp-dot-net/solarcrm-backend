﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.StockItem.Dto;
using solarcrm.DataVaults.StockItem.Importing.Dto;
using solarcrm.DataVaults.StockItemLocation.Dto;
using solarcrm.EntityFrameworkCore;
using solarcrm.Notifications;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockItem.Importing
{
    public class ImportStockItemsToExcelJob : BackgroundJob<ImportStockItemFromExcelJobArgs>, ITransientDependency
    {
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IStockItemListExcelDataReader _stockItemListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidStockItemExporter _invalidStockItemExporter;
        private readonly IAbpSession _abpSession;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<StockCategory.StockCategory> _stockCategoryrepository;
        private readonly IRepository<StockItemLocation> _stockItemLocationRepository;
        private readonly IRepository<Locations.Locations> _locationRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private int _sectionId = 25;

        public ImportStockItemsToExcelJob(
            IRepository<StockItem> stockItemRepository,
            IStockItemListExcelDataReader stockItemListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidStockItemExporter invalidStockItemExporter,
            IAbpSession abpSession,
            IRepository<User, long> userRepository,
            IRepository<StockCategory.StockCategory> stockCategoryRepository,
            IRepository<StockItemLocation> stockItemLocationRepository,
            IRepository<Locations.Locations> locationRepository,
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider
            )
        {
            _stockItemRepository = stockItemRepository;
            _stockItemListExcelDataReader = stockItemListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _invalidStockItemExporter = invalidStockItemExporter;
            _abpSession = abpSession;
            _userRepository = userRepository;
            _stockCategoryrepository = stockCategoryRepository;
            _stockItemLocationRepository = stockItemLocationRepository;
            _locationRepository = locationRepository;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        [UnitOfWork]
        public override void Execute(ImportStockItemFromExcelJobArgs args)
        {
            var stockItems = GetStockItemListFromExcelOrNull(args);
            if (stockItems == null || !stockItems.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidStockItems = new List<ImportStockItemDto>();

                foreach (var stockItem in stockItems)
                {
                    if (stockItem.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateOrUpdateStockItemAsync(stockItem, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            //Lead.Exception = exception.Message;
                            invalidStockItems.Add(stockItem);
                        }
                        //catch (Exception e)
                        catch
                        {
                            //Lead.Exception = e.ToString();
                            invalidStockItems.Add(stockItem);
                        }
                    }
                    else
                    {
                        invalidStockItems.Add(stockItem);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportLeadsResultAsync(args, invalidStockItems));
                    }

                    uow.Complete();
                }
            }
        }

        private List<ImportStockItemDto> GetStockItemListFromExcelOrNull(ImportStockItemFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        string filename = Path.GetExtension(file.Description);
                        return _stockItemListExcelDataReader.GetStockItemsFromExcel(file.Bytes, filename);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateOrUpdateStockItemAsync(ImportStockItemDto input, ImportStockItemFromExcelJobArgs args)
        {
            StockItem stockItem = new StockItem();

            var categoryId = 0;
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                if (input.Category != null)
                {
                    categoryId = await _stockCategoryrepository.GetAll().Where(e => e.Name.ToUpper() == input.Category.ToUpper()).Select(e => e.Id).FirstOrDefaultAsync();
                }

                if (categoryId == 0)
                {
                    input.Exception = "Category Not Found";
                    throw new Exception("Category Not Found");
                }

                stockItem = await _stockItemRepository.GetAll().Where(e => e.Model.ToUpper() == input.Model.Trim().ToUpper()).FirstOrDefaultAsync();
                if (stockItem == null)
                {
                    var stockItemNew = _objectMapper.Map<StockItem>(input);
                    stockItemNew.TenantId = (int)args.TenantId;
                    stockItemNew.Name = input.Name.Trim();
                    stockItemNew.Manufacturer = input.Manufacturer.Trim();
                    stockItemNew.Model = input.Model.Trim();
                    stockItemNew.Series = input.Series.Trim();
                    stockItemNew.Size = (input.Size.Trim() == null) ? null : Convert.ToDecimal(input.Size.Trim());
                    stockItemNew.Phase = (input.Phase.Trim() == null) ? null : Convert.ToInt32(input.Phase.Trim());
                    stockItemNew.CategoryId = categoryId;
                    stockItemNew.Active = true;
                    stockItemNew.CreatorUserId = (int)args.User.UserId;

                    var stockItemId = await _stockItemRepository.InsertAndGetIdAsync(stockItemNew);
                    //Add Activity Log
                    DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                    dataVaulteActivityLog.TenantId = (int)args.TenantId;
                    dataVaulteActivityLog.SectionId = _sectionId;
                    dataVaulteActivityLog.ActionId = 1; //1-Created, 2-Updated, 3-Deleted
                    dataVaulteActivityLog.ActionNote = "Stock Items Created From Excel";
                    dataVaulteActivityLog.SectionValueId = stockItemId;
                    dataVaulteActivityLog.CreatorUserId = (int)args.User.UserId;
                    await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);

                    if (stockItemId > 0)
                    {
                        var locationList = await _locationRepository.GetAll().ToListAsync();
                        foreach (var location in locationList)
                        {
                            CreateOrEditStockItemLocationDto createOrEditStockItemLocationDto = new CreateOrEditStockItemLocationDto();
                            var stockItemLocation = _objectMapper.Map<StockItemLocation>(createOrEditStockItemLocationDto);
                            stockItemLocation.TenantId = (int)args.TenantId;
                            stockItemLocation.StockItemId = stockItemId;
                            stockItemLocation.LocationId = location.Id;
                            stockItemLocation.MinQty = 0;
                            stockItemLocation.IsActive = true;
                            stockItemLocation.CreatorUserId = (int)args.User.UserId;

                            await _stockItemLocationRepository.InsertAsync(stockItemLocation);

                            //Add Activity Log
                            DataVaulteActivityLog dataVaulteActivityLog1 = new DataVaulteActivityLog();
                            dataVaulteActivityLog1.TenantId = (int)args.TenantId;
                            dataVaulteActivityLog1.SectionId = _sectionId;
                            dataVaulteActivityLog1.ActionId = 1; //1-Created, 2-Updated, 3-Deleted
                            dataVaulteActivityLog1.ActionNote = "Stock Item Created for " + location.Name + " Location From Excel";
                            dataVaulteActivityLog1.SectionValueId = stockItemId;
                            dataVaulteActivityLog1.CreatorUserId = (int)args.User.UserId;
                            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog1);
                        }
                    }
                }
                else
                {
                    //Add Activity Log
                    DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                    dataVaulteActivityLog.TenantId = (int)args.TenantId;
                    dataVaulteActivityLog.SectionId = _sectionId; // stock category Pages Name
                    dataVaulteActivityLog.ActionId = 2; // Updated
                    dataVaulteActivityLog.ActionNote = "Stock Item Updated";
                    dataVaulteActivityLog.SectionValueId = stockItem.Id;
                    dataVaulteActivityLog.CreatorUserId = (int)args.User.UserId;
                    var stockcategoryActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

                    #region Added Log History
                    var List = new List<DataVaultsHistory>();
                    if (stockItem.Name != input.Name.Trim())
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "Name";
                        dataVaultsHistory.PrevValue = stockItem.Name;
                        dataVaultsHistory.CurValue = input.Name.Trim();
                        dataVaultsHistory.Action = "Edit";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = stockItem.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (stockItem.Manufacturer != input.Manufacturer.Trim())
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "Manufacturer";
                        dataVaultsHistory.PrevValue = stockItem.Manufacturer;
                        dataVaultsHistory.CurValue = input.Manufacturer.Trim();
                        dataVaultsHistory.Action = "Edit";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = stockItem.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (stockItem.Series != input.Series.Trim())
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "Series";
                        dataVaultsHistory.PrevValue = stockItem.Series;
                        dataVaultsHistory.CurValue = input.Series.Trim();
                        dataVaultsHistory.Action = "Edit";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = stockItem.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (stockItem.Size.ToString() != input.Size.Trim())
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "Size";
                        dataVaultsHistory.PrevValue = stockItem.Size.ToString();
                        dataVaultsHistory.CurValue = input.Size.Trim();
                        dataVaultsHistory.Action = "Edit";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = stockItem.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (stockItem.Phase.ToString() != input.Phase.Trim())
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "Phase";
                        dataVaultsHistory.PrevValue = stockItem.Phase.ToString();
                        dataVaultsHistory.CurValue = input.Phase.Trim();
                        dataVaultsHistory.Action = "Edit";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = stockItem.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (stockItem.CategoryId != categoryId)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "CategoryId";
                        dataVaultsHistory.PrevValue = stockItem.CategoryId.ToString();
                        dataVaultsHistory.CurValue = categoryId.ToString();
                        dataVaultsHistory.Action = "Edit";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = stockItem.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }

                    await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
                    await _dbContextProvider.GetDbContext().SaveChangesAsync();

                    #endregion

                    stockItem.Name = input.Name.Trim();
                    stockItem.Manufacturer = input.Manufacturer.Trim();
                    stockItem.Series = input.Series.Trim();
                    stockItem.Size = (input.Size.Trim() == null) ? null : Convert.ToDecimal(input.Size.Trim());
                    stockItem.Phase = (input.Phase.Trim() == null) ? null : Convert.ToInt32(input.Phase.Trim());
                    stockItem.CategoryId = categoryId;
                    stockItem.Active = true;
                    stockItem.LastModifierUserId = (int)args.User.UserId;

                    await _stockItemRepository.UpdateAsync(stockItem);
                }
            }
        }

        private void SendInvalidExcelNotification(ImportStockItemFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToStockItem", solarcrmConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportLeadsResultAsync(ImportStockItemFromExcelJobArgs args, List<ImportStockItemDto> invalidStockItem)
        {
            if (invalidStockItem.Any())
            {
                var file = _invalidStockItemExporter.ExportToFile(invalidStockItem);
                await _appNotifier.SomeStockItemCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllStockItemSuccessfullyImportedFromExcel", solarcrmConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }

    }
}
