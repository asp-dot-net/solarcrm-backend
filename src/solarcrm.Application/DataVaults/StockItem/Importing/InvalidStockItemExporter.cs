﻿using Abp.Dependency;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.StockItem.Importing.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockItem.Importing
{
    public class InvalidStockItemExporter : NpoiExcelExporterBase, IInvalidStockItemExporter, ITransientDependency
    {
        public InvalidStockItemExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportStockItemDto> stockItemListDtos)
        {
            return CreateExcelPackage(
                "InvalidStockItemImportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InvalidStockItemImports"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Manufacturer"),
                        L("Model"),
                        L("Series"),
                        L("Size"),
                        L("Phase"),
                        L("Category"),
                        L("Refuse Reason")
                    );

                    AddObjects(
                        sheet, stockItemListDtos,
                        _ => _.Name,
                        _ => _.Manufacturer,
                        _ => _.Model,
                        _ => _.Series,
                        _ => _.Size,
                        _ => _.Phase,
                        _ => _.Category,
                        _ => _.Exception
                    );

                    for (var i = 0; i < 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
