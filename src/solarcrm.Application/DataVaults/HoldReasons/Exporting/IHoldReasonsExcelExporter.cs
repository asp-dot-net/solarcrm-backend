﻿using solarcrm.DataVaults.HoldReasons.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.HoldReasons.Exporting
{
    public interface IHoldReasonsExcelExporter
    {
        FileDto ExportToFile(List<GetHoldReasonsForViewDto> holdreasons);
    }
}
