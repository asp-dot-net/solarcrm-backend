﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.HoldReasons.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.HoldReasons.Exporting
{
    public class HoldReasonsExcelExporter : NpoiExcelExporterBase, IHoldReasonsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public HoldReasonsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetHoldReasonsForViewDto> leadType)
        {
            return CreateExcelPackage(
                "HoldReasons.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("HoldReasons"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                   

                    AddObjects(
                        sheet, leadType,
                        _ => _.holdreasons.Name
                        );
                });
        }
    }
}
