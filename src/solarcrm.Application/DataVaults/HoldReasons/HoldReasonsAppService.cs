﻿using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.HoldReasons.Exporting;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using solarcrm.DataVaults.HoldReasons.Dto;
using System.Linq;
using solarcrm.Dto;
using System.Linq.Dynamic.Core;
using solarcrm.Common;

namespace solarcrm.DataVaults.HoldReasons
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HoldReasons)]
    public class HoldReasonsAppService : solarcrmAppServiceBase, IHoldReasonsAppService
    {
        private readonly IRepository<HoldReasons> _holdReasonsRepository;
        private readonly IHoldReasonsExcelExporter _holdReasonsExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public HoldReasonsAppService(IRepository<HoldReasons> HoldReasonsRepository, IHoldReasonsExcelExporter HoldReasonsExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _holdReasonsRepository = HoldReasonsRepository;
            _holdReasonsExcelExporter = HoldReasonsExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetHoldReasonsForViewDto>> GetAll(GetAllHoldReasonsInput input)
        {
            var filteredHoldReasons = _holdReasonsRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredHoldReasons = filteredHoldReasons.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var holdReasons = from o in pagedAndFilteredHoldReasons
                                select new GetHoldReasonsForViewDto()
                                {
                                    holdreasons = new HoldReasonsDto
                                    {
                                        Name = o.Name,
                                        IsActive = o.IsActive,
                                        Id = o.Id,
                                        CreatedDate = o.CreationTime,

                                    }
                                };

            var totalCount = await filteredHoldReasons.CountAsync();

            return new PagedResultDto<GetHoldReasonsForViewDto>(totalCount, await holdReasons.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_Edit)]
        public async Task<GetHoldReasonsForEditOutput> GetHoldReasonsForEdit(EntityDto input)
        {
            var holdReasons = await _holdReasonsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetHoldReasonsForEditOutput { holdreasons = ObjectMapper.Map<CreateOrEditHoldReasonsDto>(holdReasons) };

            return output;
        }

        public async Task<GetHoldReasonsForViewDto> GetHoldReasonsForView(int id)
        {
            var holdReasons = await _holdReasonsRepository.GetAsync(id);

            var output = new GetHoldReasonsForViewDto { holdreasons = ObjectMapper.Map<HoldReasonsDto>(holdReasons) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_ExportToExcel)]
        public async Task<FileDto> GetHoldReasonsToExcel(GetAllHoldReasonsForExcelInput input)
        {
            var filteredHoldReasons = _holdReasonsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredHoldReasons
                         select new GetHoldReasonsForViewDto()
                         {
                             holdreasons = new HoldReasonsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var holdReasonsListDtos = await query.ToListAsync();

            return _holdReasonsExcelExporter.ExportToFile(holdReasonsListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditHoldReasonsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_Create)]
        protected virtual async Task Create(CreateOrEditHoldReasonsDto input)
        {
            var holdReasons = ObjectMapper.Map<HoldReasons>(input);

            if (AbpSession.TenantId != null)
            {
                holdReasons.TenantId = (int?)AbpSession.TenantId;
            }

            var holdReasonsId = await _holdReasonsRepository.InsertAndGetIdAsync(holdReasons);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 59; // HoldReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Hold Reasons Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = holdReasonsId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_Edit)]
        protected virtual async Task Update(CreateOrEditHoldReasonsDto input)
        {
            var holdReasons = await _holdReasonsRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 59; // HoldReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Hold Reasons Updated Name :-" + holdReasons.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var holdReasonsActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (holdReasons.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = holdReasons.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = holdReasonsActivityLogId;
                dataVaultsHistory.SectionId = 59; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (holdReasons.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = holdReasons.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = holdReasonsActivityLogId;
                dataVaultsHistory.SectionId = 59;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, holdReasons);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_Delete)]
        public async Task Delete(EntityDto input)
        {
            var holdReasonResult = await _holdReasonsRepository.FirstOrDefaultAsync(input.Id);
            await _holdReasonsRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 59; // HoldReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Hold Reasons Deleted Name :-" + holdReasonResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
