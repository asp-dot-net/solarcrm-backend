﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.ProjectStatus.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.ProjectStatus.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.ProjectStatus
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus)]
    public class ProjectStatusAppService : solarcrmAppServiceBase, IProjectStatusAppService
    {
        private readonly IRepository<ProjectStatus> _ProjectStatusRepository;
        private readonly IProjectStatusExcelExporter _ProjectStatusExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public ProjectStatusAppService(IRepository<ProjectStatus> ProjectStatusRepository, IProjectStatusExcelExporter ProjectStatusExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _ProjectStatusRepository = ProjectStatusRepository;
            _ProjectStatusExcelExporter = ProjectStatusExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetProjectStatusForViewDto>> GetAll(GetAllProjectStatusInput input)
        {
            var filteredprojectStatus = _ProjectStatusRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredProjectStatus = filteredprojectStatus.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var projectStatus = from o in pagedAndFilteredProjectStatus
                              select new GetProjectStatusForViewDto()
                             {
                                 ProjectStatus = new ProjectStatusDto
                                 {
                                     Name = o.Name,
                                     IsActive = o.IsActive,
                                     Id = o.Id,
                                     CreatedDate = o.CreationTime
                                 }
                             };

            var totalCount = await filteredprojectStatus.CountAsync();

            return new PagedResultDto<GetProjectStatusForViewDto>(totalCount, await projectStatus.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_Edit)]
        public async Task<GetProjectStatusForEditOutput> GetProjectStatusForEdit(EntityDto input)
        {
            var ProjectStatus = await _ProjectStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProjectStatusForEditOutput { ProjectStatus = ObjectMapper.Map<CreateOrEditProjectStatusDto>(ProjectStatus) };

            return output;
        }

        public async Task<GetProjectStatusForViewDto> GetProjectStatusForView(int id)
        {
            var ProjectStatus = await _ProjectStatusRepository.GetAsync(id);

            var output = new GetProjectStatusForViewDto { ProjectStatus = ObjectMapper.Map<ProjectStatusDto>(ProjectStatus) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_ExportToExcel)]
        public async Task<FileDto> GetProjectStatusToExcel(GetAllProjectStatusForExcelInput input)
        {
            var filteredprojectStatus = _ProjectStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredprojectStatus
                         select new GetProjectStatusForViewDto()
                         {
                             ProjectStatus = new ProjectStatusDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var ProjectStatusListDtos = await query.ToListAsync();

            return _ProjectStatusExcelExporter.ExportToFile(ProjectStatusListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditProjectStatusDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_Create)]
        protected virtual async Task Create(CreateOrEditProjectStatusDto input)
        {
            var ProjectStatus = ObjectMapper.Map<ProjectStatus>(input);

            if (AbpSession.TenantId != null)
            {
                ProjectStatus.TenantId = (int?)AbpSession.TenantId;
            }

            var ProjectStatusId = await _ProjectStatusRepository.InsertAndGetIdAsync(ProjectStatus);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 7; // ProjectStatus Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Project Status Created Name :-" +input.Name;
            dataVaulteActivityLog.SectionValueId = ProjectStatusId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_Edit)]
        protected virtual async Task Update(CreateOrEditProjectStatusDto input)
        {
            var ProjectStatus = await _ProjectStatusRepository.FirstOrDefaultAsync((int)input.Id);
            
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 7; // ProjectStatus Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated;// Updated
            dataVaulteActivityLog.ActionNote = "Project Status Updated Name :-" +ProjectStatus.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var projectstatusActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if(ProjectStatus.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = ProjectStatus.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = projectstatusActivityLogId;
                dataVaultsHistory.SectionId = 7;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if(ProjectStatus.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = ProjectStatus.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = projectstatusActivityLogId;
                dataVaultsHistory.SectionId = 7;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, ProjectStatus);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_Delete)]
        public async Task Delete(EntityDto input)
        {
            var projectstatus = await _ProjectStatusRepository.FirstOrDefaultAsync(input.Id);
            await _ProjectStatusRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 7; // ProjectStatus Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;// Deleted
            dataVaulteActivityLog.ActionNote = "Project Status Deleted Name :-" +projectstatus.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<List<ProjectStatusLookupTableDto>> GetAllProjectStatusForDropdown()
        {
            var allProjectStatus = _ProjectStatusRepository.GetAll().Where(x => x.IsActive == true);

            var ProjectStatus = from o in allProjectStatus
                            select new ProjectStatusLookupTableDto()
                            {
                                DisplayName = o.Name,
                                Id = o.Id
                            };

            return new List<ProjectStatusLookupTableDto>(await ProjectStatus.ToListAsync());
        }
    }
}
