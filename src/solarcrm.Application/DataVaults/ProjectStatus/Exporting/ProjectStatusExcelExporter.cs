﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.ProjectStatus.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ProjectStatus.Exporting
{
    public class ProjectStatusExcelExporter : NpoiExcelExporterBase, IProjectStatusExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProjectStatusExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetProjectStatusForViewDto> ProjectType)
        {
            return CreateExcelPackage(
                "ProjectStatus.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ProjectStatus"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, ProjectType,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, ProjectType,
                        _ => _.ProjectStatus.Name
                        );
                });
        }
    }
}
