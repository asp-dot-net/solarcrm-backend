﻿using solarcrm.DataVaults.ProjectStatus.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.ProjectStatus.Exporting
{
    public interface IProjectStatusExcelExporter
    {
        FileDto ExportToFile(List<GetProjectStatusForViewDto> projectstatus);
    }
}
