﻿using solarcrm.DataVaults.StockCategory.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.StockCategory.Exporting
{
    public interface IStockCategoryExcelExporter
    {
        FileDto ExportToFile(List<GetStockCategoryForViewDto> stockcategory);
    }
}
