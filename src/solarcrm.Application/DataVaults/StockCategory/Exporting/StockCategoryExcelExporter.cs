﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.StockCategory.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockCategory.Exporting
{
    public class StockCategoryExcelExporter : NpoiExcelExporterBase, IStockCategoryExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public StockCategoryExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetStockCategoryForViewDto> ProjectType)
        {
            return CreateExcelPackage(
                "StockCategory.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("StockCategory"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, ProjectType,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, ProjectType,
                        _ => _.StockCategory.Name
                        );
                });
        }
    }
}
