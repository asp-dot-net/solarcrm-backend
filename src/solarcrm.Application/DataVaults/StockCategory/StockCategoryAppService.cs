﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.StockCategory.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.StockCategory.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.StockCategory
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory)]
    public class StockCategoryAppService : solarcrmAppServiceBase, IStockCategoryAppService
    {
        private readonly IRepository<StockCategory> _StockCategoryRepository;
        private readonly IStockCategoryExcelExporter _StockCategoryExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public StockCategoryAppService(IRepository<StockCategory> StockCategoryRepository, IStockCategoryExcelExporter StockCategoryExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _StockCategoryRepository = StockCategoryRepository;
            _StockCategoryExcelExporter = StockCategoryExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
                
        public async Task<PagedResultDto<GetStockCategoryForViewDto>> GetAll(GetAllStockCategoryInput input)
        {
            var filteredstockcategory = _StockCategoryRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredstockcategory = filteredstockcategory.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var stockcategory = from o in pagedAndFilteredstockcategory
                                select new GetStockCategoryForViewDto()
                             {
                                  StockCategory = new StockCategoryDto
                                 {
                                     Name = o.Name,
                                     IsActive = o.IsActive,
                                     Id = o.Id,
                                     CreatedDate = o.CreationTime
                                 }
                             };

            var totalCount = await filteredstockcategory.CountAsync();

            return new PagedResultDto<GetStockCategoryForViewDto>(totalCount, await stockcategory.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Edit)]
        public async Task<GetStockCategoryForEditOutput> GetStockCategoryForEdit(EntityDto input)
        {
            var StockCategory = await _StockCategoryRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetStockCategoryForEditOutput { StockCategory = ObjectMapper.Map<CreateOrEditStockCategoryDto>(StockCategory) };

            return output;
        }

        public async Task<GetStockCategoryForViewDto> GetStockCategoryForView(int id)
        {
            var StockCategory = await _StockCategoryRepository.GetAsync(id);

            var output = new GetStockCategoryForViewDto { StockCategory = ObjectMapper.Map<StockCategoryDto>(StockCategory) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory_ExportToExcel)]
        public async Task<FileDto> GetStockCategoryToExcel(GetAllStockCategoryForExcelInput input)
        {
            var filteredstockcategory = _StockCategoryRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredstockcategory
                         select new GetStockCategoryForViewDto()
                         {
                             StockCategory = new StockCategoryDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var StockCategoryListDtos = await query.ToListAsync();

            return _StockCategoryExcelExporter.ExportToFile(StockCategoryListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditStockCategoryDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Create)]
        protected virtual async Task Create(CreateOrEditStockCategoryDto input)
        {
            var StockCategory = ObjectMapper.Map<StockCategory>(input);

            if (AbpSession.TenantId != null)
            {
                StockCategory.TenantId = (int?)AbpSession.TenantId;
            }

            var StockCategoryId = await _StockCategoryRepository.InsertAndGetIdAsync(StockCategory);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 8; // Stock Category Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Stock Category Created Name :-" +input.Name;
            dataVaulteActivityLog.SectionValueId = StockCategoryId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Edit)]
        protected virtual async Task Update(CreateOrEditStockCategoryDto input)
        {
            var stockcategory = await _StockCategoryRepository.FirstOrDefaultAsync((int)input.Id);
            
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 8; // stock category Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Stock Category Updated Name :-" +stockcategory.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var stockcategoryActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if(stockcategory.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = stockcategory.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                dataVaultsHistory.SectionId = 8;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if(stockcategory.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = stockcategory.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockcategoryActivityLogId;
                dataVaultsHistory.SectionId = 8;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, stockcategory);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Delete)]
        public async Task Delete(EntityDto input)
        {
            var stockcategoryResult = await _StockCategoryRepository.FirstOrDefaultAsync(input.Id);
            await _StockCategoryRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 8; // StockCategory Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;// Deleted
            dataVaulteActivityLog.ActionNote = "Stock Category Deleted Name :-" + stockcategoryResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<List<StockCategoryLookupTableDto>> GetAllStockCategoryForDropdown()
        {
            var allstockcategory = _StockCategoryRepository.GetAll().Where(x => x.IsActive == true);

            var stockcategory = from o in allstockcategory
                            select new StockCategoryLookupTableDto()
                            {
                                DisplayName = o.Name,
                                Id = o.Id
                            };

            return new List<StockCategoryLookupTableDto>(await stockcategory.ToListAsync());
        }
    }
}
