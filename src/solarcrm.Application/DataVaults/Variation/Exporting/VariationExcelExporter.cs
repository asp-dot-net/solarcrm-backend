﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Variation.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System.Collections.Generic;

namespace solarcrm.DataVaults.Variation.Exporting
{
    public class VariationExcelExporter : NpoiExcelExporterBase, IVariationExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public VariationExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetVariationForViewDto> variation)
        {
            return CreateExcelPackage(
                "Variations.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Variation"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Action"),
                        L("IsActive")
                        );

                    AddObjects(
                        sheet, variation,
                        _ => _.Variation.Name,
                        _ => _.Variation.Action,
                        _ => _.Variation.IsActive
                        );
                });
        }
    }
}
