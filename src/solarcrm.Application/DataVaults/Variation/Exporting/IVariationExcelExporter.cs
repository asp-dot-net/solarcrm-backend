﻿using solarcrm.DataVaults.Variation.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Variation.Exporting
{
    public interface IVariationExcelExporter
    {
        FileDto ExportToFile(List<GetVariationForViewDto> variation);
    }
}
