﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Variation.Dto;
using solarcrm.DataVaults.Variation.Exporting;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Common;

namespace solarcrm.DataVaults.Variation
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Variation)]
    public class VariationAppService : solarcrmAppServiceBase, IVariationAppService
    {
        private readonly IRepository<Variation> _variationRepository;
        private readonly IVariationExcelExporter _variationExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly int _sectionId = 28; // StockItem Pages Name


        public VariationAppService(IRepository<Variation> variationRepository, IVariationExcelExporter variationExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _variationRepository = variationRepository;
            _variationExcelExporter = variationExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetVariationForViewDto>> GetAll(GetAllVariationInput input)
        {
            var filteredVariation = _variationRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredVariation = filteredVariation.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var variation = from o in pagedAndFilteredVariation
                             select new GetVariationForViewDto()
                             {
                                 Variation = new VariationDto
                                 {
                                     Id = o.Id,
                                     Name = o.Name,
                                     Action = o.Action,
                                     IsActive = o.IsActive
                                 }
                             };

            var totalCount = await variation.CountAsync();


            return new PagedResultDto<GetVariationForViewDto>(totalCount, await variation.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Variation_ExportToExcel)]
        public async Task<FileDto> GetVariationToExcel(GetAllVariationForExcelInput input)
        {
            var filteredVariation = _variationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredVariation
                         select new GetVariationForViewDto()
                         {
                             Variation = new VariationDto
                             {
                                 Id = o.Id,
                                 Name = o.Name,
                                 Action = o.Action,
                                 IsActive = o.IsActive
                             }
                         });

            var variationDtos = await query.ToListAsync();

            return _variationExcelExporter.ExportToFile(variationDtos);
        }
               
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Variation_Edit)]
        public async Task<GetVariationForEditOutput> GetVariationForEdit(EntityDto input)
        {
            var variation = await _variationRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetVariationForEditOutput
            {
                Variation = ObjectMapper.Map<CreateOrEditVariationDto>(variation),
            };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Variation_Create, AppPermissions.Pages_Tenant_DataVaults_Variation_Edit)]
        public async Task CreateOrEdit(CreateOrEditVariationDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Variation_Create)]
        protected virtual async Task Create(CreateOrEditVariationDto input)
        {
            var Variation = ObjectMapper.Map<Variation>(input);

            if (AbpSession.TenantId != null)
            {
                Variation.TenantId = (int)AbpSession.TenantId;
            }

            var variationId = await _variationRepository.InsertAndGetIdAsync(Variation);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId;
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Variation Created Name :- " + Variation.Name;
            dataVaulteActivityLog.SectionValueId = variationId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Variation_Edit)]
        protected virtual async Task Update(CreateOrEditVariationDto input)
        {
            var variation = await _variationRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId; // stock category Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Variation Updated Name :- " + variation.Name ;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var variationActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (variation.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = variation.Name.ToString();
                dataVaultsHistory.CurValue = input.Name.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Updated
                dataVaultsHistory.ActivityLogId = variationActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (variation.Action != input.Action)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Action";
                dataVaultsHistory.PrevValue = variation.Action;
                dataVaultsHistory.CurValue = input.Action;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Updated
                dataVaultsHistory.ActivityLogId = variationActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (variation.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Active";
                dataVaultsHistory.PrevValue = variation.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Updated
                dataVaultsHistory.ActivityLogId = variationActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();
            #endregion

            ObjectMapper.Map(input, variation);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Variation_Delete)]
        public async Task Delete(EntityDto input)
        {
            var variationResult = await _variationRepository.FirstOrDefaultAsync(input.Id);
            await _variationRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId;
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Variation Deleted Name :- " + variationResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<GetVariationForViewDto> GetVariationForView(int id)
        {
            var variation = await _variationRepository.GetAsync(id);

            var output = new GetVariationForViewDto { Variation = ObjectMapper.Map<VariationDto>(variation) };

            return output;
        }

    }
}
