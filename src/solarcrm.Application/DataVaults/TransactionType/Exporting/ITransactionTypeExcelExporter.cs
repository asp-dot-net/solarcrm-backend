﻿using solarcrm.DataVaults.TransactionType.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.TransactionType.Exporting
{
    public interface ITransactionTypeExcelExporter
    {
        FileDto ExportToFile(List<GetTransactionTypeForViewDto> transactionType);
    }
}
