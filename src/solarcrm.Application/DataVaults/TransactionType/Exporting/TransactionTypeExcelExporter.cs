﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.TransactionType.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.TransactionType.Exporting
{
    public class TransactionTypeExcelExporter : NpoiExcelExporterBase, ITransactionTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TransactionTypeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTransactionTypeForViewDto> transactionType)
        {
            return CreateExcelPackage(
                "TransactionType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("TransactionType"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, transactionType,
                        _ => _.TransactionType.Name
                        );
                });
        }
    }
}
