﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.TransactionType.Dto;
using solarcrm.DataVaults.TransactionType.Exporting;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;

namespace solarcrm.DataVaults.TransactionType
{
    public class TransactionTypeAppService : solarcrmAppServiceBase, ITransactionTypeAppService
    {
        private readonly IRepository<TransactionType> _transactionTypeRepository;
        private readonly ITransactionTypeExcelExporter _transactionTypeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public TransactionTypeAppService(IRepository<TransactionType> transactionTypeRepository, ITransactionTypeExcelExporter transactionTypeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _transactionTypeRepository = transactionTypeRepository;
            _transactionTypeExcelExporter = transactionTypeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task CreateOrEdit(CreateOrEditTransactionTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_TransactionType_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _transactionTypeRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 21; // TransactionType Pages Name
            dataVaulteActivityLog.ActionId = 3; // Deleted
            dataVaulteActivityLog.ActionNote = "Transaction Type Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<PagedResultDto<GetTransactionTypeForViewDto>> GetAll(GetAllTransactionTypeInput input)
        {
            var filteredTransactionType = _transactionTypeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredTransactionType = filteredTransactionType.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var transactionType = from o in pagedAndFilteredTransactionType
                              select new GetTransactionTypeForViewDto()
                              {
                                  TransactionType = new TransactionTypeDto
                                  {
                                      Name = o.Name,
                                      IsActive = o.IsActive,
                                      Id = o.Id
                                  }
                              };

            var totalCount = await filteredTransactionType.CountAsync();

            return new PagedResultDto<GetTransactionTypeForViewDto>(totalCount, await transactionType.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_TransactionType_Edit)]
        public async Task<GetTransactionTypeForEditOutput> GetTransactionTypeForEdit(EntityDto input)
        {
            var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTransactionTypeForEditOutput { TransactionType = ObjectMapper.Map<CreateOrEditTransactionTypeDto>(transactionType) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_TransactionType_ExportToExcel)]
        public async Task<FileDto> GetTransactionTypeToExcel(GetAllTransactionTypeForExcelInput input)
        {
            var filteredTransactionType = _transactionTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredTransactionType
                         select new GetTransactionTypeForViewDto()
                         {
                             TransactionType = new TransactionTypeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var TransactionTypeDtos = await query.ToListAsync();

            return _transactionTypeExcelExporter.ExportToFile(TransactionTypeDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_TransactionType_Create)]
        protected virtual async Task Create(CreateOrEditTransactionTypeDto input)
        {
            var transactionType = ObjectMapper.Map<TransactionType>(input);

            if (AbpSession.TenantId != null)
            {
                transactionType.TenantId = (int?)AbpSession.TenantId;
            }

            var TransactionTypeId = await _transactionTypeRepository.InsertAndGetIdAsync(transactionType);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 21; // Transaction Type Pages Name
            dataVaulteActivityLog.ActionId = 1; // Created
            dataVaulteActivityLog.ActionNote = "Transaction Type Created";
            dataVaulteActivityLog.SectionValueId = TransactionTypeId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_TransactionType_Edit)]
        protected virtual async Task Update(CreateOrEditTransactionTypeDto input)
        {
            var transactionType = await _transactionTypeRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            int SectionId = 21; // Division Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Division Pages Name
            dataVaulteActivityLog.ActionId = 2; // Updated
            dataVaulteActivityLog.ActionNote = "Transaction Type Updated";
            dataVaulteActivityLog.SectionValueId = input.Id;
            var DocumentListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (transactionType.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = transactionType.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (transactionType.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = transactionType.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, transactionType);
        }
    }
}
