﻿
using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.DataVaults.PaymentType.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PaymentType.Exporting
{
    public interface IPaymentTypeExcelExporter
    {
        FileDto ExportToFile(List<GetPaymentTypeForViewDto> paymentType);
    }
}
