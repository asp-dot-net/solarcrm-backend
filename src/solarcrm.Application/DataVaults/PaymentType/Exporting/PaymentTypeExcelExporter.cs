﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;

using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.DataVaults.PaymentType.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PaymentType.Exporting
{
    public class PaymentTypeExcelExporter : NpoiExcelExporterBase, IPaymentTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentTypeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetPaymentTypeForViewDto> paymentType)
        {
            return CreateExcelPackage(
                "PaymentType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("PaymentType");

                    AddHeader(
                        sheet,
                        L("Name")
                      
                        );

                    AddObjects(
                        sheet, paymentType,
                        _ => _.paymentType.Name
                       
                        );
                });
        }
        //public FileDto ExportToFile(List<GetPaymentTypeForViewDto> paymenttype)
        //{
        //    return CreateExcelPackage(
        //       "CMRTest.xlsx",
        //        excelPackage =>
        //        {
        //            var sheet = excelPackage.CreateSheet(L("CMRTest"));
        //            AddHeader(
        //                sheet,
        //                L("Name")
        //                );
        //            AddObjects(
        //                sheet, paymenttype,
        //                _ => _.paymentType.Name
        //                );
        //        });
        //}
    }
}
