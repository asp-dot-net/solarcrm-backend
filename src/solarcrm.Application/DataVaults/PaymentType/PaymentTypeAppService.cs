﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Dto;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.PaymentType.Dto;
using solarcrm.DataVaults.PaymentType.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.PaymentType
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentType)]
    public class PaymentTypeAppService : solarcrmAppServiceBase, IPaymentTypeAppService
    {
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IPaymentTypeExcelExporter _paymentTypeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public PaymentTypeAppService(IRepository<PaymentType> paymentTypeRepository, IPaymentTypeExcelExporter paymentTypeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _paymentTypeRepository = paymentTypeRepository;
            _paymentTypeExcelExporter = paymentTypeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
        public async Task<PagedResultDto<GetPaymentTypeForViewDto>> GetAll(GetAllPaymentTypeInput input)
        {
            var filteredpayBy = _paymentTypeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredPaymentType = filteredpayBy.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var paymentType = from o in pagedAndFilteredPaymentType
                           select new GetPaymentTypeForViewDto()
                           {
                               paymentType = new PaymentTypeDto
                               {
                                   Name = o.Name,
                                   IsActive = o.IsActive,
                                   Id = o.Id,
                                   CreatedDate = o.CreationTime
                               }
                           };

            var totalCount = await filteredpayBy.CountAsync();

            return new PagedResultDto<GetPaymentTypeForViewDto>(totalCount, await paymentType.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentType_Edit)]
        public async Task<GetPaymentTypeForEditOutput> GetPaymentTypeForEdit(EntityDto input)
        {
            var paymentType = await _paymentTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPaymentTypeForEditOutput { paymentType = ObjectMapper.Map<CreateOrEditPaymentTypeDto>(paymentType) };

            return output;
        }

        public async Task<GetPaymentTypeForViewDto> GetPaymentTypeForView(int id)
        {
            var paymentType = await _paymentTypeRepository.GetAsync(id);

            var output = new GetPaymentTypeForViewDto { paymentType = ObjectMapper.Map<PaymentTypeDto>(paymentType) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentType_ExportToExcel)]
        public async Task<FileDto> GetPaymentTypeToExcel(GetAllPaymentTypeForExcelInput input)
        {
            var filteredpaymentType = _paymentTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredpaymentType
                         select new GetPaymentTypeForViewDto()
                         {
                             paymentType = new PaymentTypeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var paymentTypesListDtos = await query.ToListAsync();

            return _paymentTypeExcelExporter.ExportToFile(paymentTypesListDtos);
        }
        public async Task CreateOrEdit(CreateOrEditPaymentTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentType_Create)]
        protected virtual async Task Create(CreateOrEditPaymentTypeDto input)
        {
            var paymentType = ObjectMapper.Map<PaymentType>(input);

            if (AbpSession.TenantId != null)
            {
                paymentType.TenantId = (int?)AbpSession.TenantId;
            }

            var paymentTypeId = await _paymentTypeRepository.InsertAndGetIdAsync(paymentType);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 47; // PaymentType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "payment type Created Name :- " + input.Name;
            dataVaulteActivityLog.SectionValueId = paymentTypeId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentType_Edit)]
        protected virtual async Task Update(CreateOrEditPaymentTypeDto input)
        {
            var paymentType = await _paymentTypeRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 47; // PaymentType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "payment Type Updated Name :- " + input.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var paymentTypeActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (paymentType.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = paymentType.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; 
                dataVaultsHistory.ActivityLogId = paymentTypeActivityLogId;
                dataVaultsHistory.SectionId = 47; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (paymentType.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = paymentType.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Updated
                dataVaultsHistory.ActivityLogId = paymentTypeActivityLogId;
                dataVaultsHistory.SectionId = 47;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, paymentType);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var PaymentTypeResult = await _paymentTypeRepository.FirstOrDefaultAsync(input.Id);
            await _paymentTypeRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 47; // PaymentType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Deleted
            dataVaulteActivityLog.ActionNote = "Payment Type Deleted Name :- " + PaymentTypeResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
