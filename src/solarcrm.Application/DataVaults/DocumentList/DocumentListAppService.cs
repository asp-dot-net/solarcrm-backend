﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.DocumentLists;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.Dto;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.DocumentList.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.DocumentList
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentList)]
    public class DocumentListAppService : solarcrmAppServiceBase, IDocumentListAppService
    {
        private readonly IRepository<DocumentList> _documentListRepository;
        private readonly IDocumentExcelExporter _documentListExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public DocumentListAppService(IRepository<DocumentList> documentListRepository, IDocumentExcelExporter documentListExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _documentListRepository = documentListRepository;
            _documentListExcelExporter = documentListExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetDocumentListForViewDto>> GetAll(GetAllDocumentListInput input)
        {
            var filteredDocumentList = _documentListRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredDocumentList = filteredDocumentList.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var DocumentList = from o in pagedAndFilteredDocumentList
                           select new GetDocumentListForViewDto()
                           {
                               DocumentList = new DocumentListDto
                               {
                                   Name = o.Name,
                                   IsActive = o.IsActive,
                                   IsATFlag = o.IsATFlag,
                                   Id = o.Id,
                                   DocumentShortName = o.DocumentShortName,
                                   CreatedDate = o.CreationTime,
                                   DocumenStorageUnit = o.DocumenStorageUnit,
                                   DocumenSize = o.DocumenSize,
                                   DocumenFormate = o.DocumenFormate,
                               }
                           };

            var totalCount = await filteredDocumentList.CountAsync();

            return new PagedResultDto<GetDocumentListForViewDto>(totalCount, await DocumentList.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentList_Edit)]
        public async Task<GetDocumentListForEditOutput> GetDocumentListForEdit(EntityDto input)
        {
            var DocumentList = await _documentListRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDocumentListForEditOutput { DocumentList = ObjectMapper.Map<CreateOrEditDocumentListDto>(DocumentList) };

            return output;
        }

        public async Task<GetDocumentListForViewDto> GetDocumentListForView(int id)
        {
            var DocumentList = await _documentListRepository.GetAsync(id);

            var output = new GetDocumentListForViewDto { DocumentList = ObjectMapper.Map<DocumentListDto>(DocumentList) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentList_ExportToExcel)]
        public async Task<FileDto> GetDocumentListToExcel(GetAllDocumentListForExcelInput input)
        {
            var filteredDocumentList = _documentListRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredDocumentList
                         select new GetDocumentListForViewDto()
                         {
                             DocumentList = new DocumentListDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var DocumentListsListDtos = await query.ToListAsync();

            return _documentListExcelExporter.ExportToFile(DocumentListsListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditDocumentListDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentList_Create)]
        protected virtual async Task Create(CreateOrEditDocumentListDto input)
        {
            var DocumentList = ObjectMapper.Map<DocumentList>(input);

            if (AbpSession.TenantId != null)
            {
                DocumentList.TenantId = (int?)AbpSession.TenantId;
            }

            var DocumentListId = await _documentListRepository.InsertAndGetIdAsync(DocumentList);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 3; // DocumentList Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created;// Created
            dataVaulteActivityLog.ActionNote = "Document Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = DocumentListId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentList_Edit)]
        protected virtual async Task Update(CreateOrEditDocumentListDto input)
        {
            var DocumentList = await _documentListRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 3; // DocumentList Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Document Updated Name :-" + DocumentList.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var DocumentListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (DocumentList.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = DocumentList.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = 3; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (DocumentList.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = DocumentList.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = 3;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (DocumentList.DocumenSize != input.DocumenSize)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "DocumenSize";
                dataVaultsHistory.PrevValue = DocumentList.DocumenSize;
                dataVaultsHistory.CurValue = input.DocumenSize;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = 3;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (DocumentList.DocumentShortName != input.DocumentShortName)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "DocumentShortName";
                dataVaultsHistory.PrevValue = DocumentList.DocumentShortName;
                dataVaultsHistory.CurValue = input.DocumentShortName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = 3;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (DocumentList.DocumenFormate != input.DocumenFormate)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "DocumenFormate";
                dataVaultsHistory.PrevValue = DocumentList.DocumenFormate;
                dataVaultsHistory.CurValue = input.DocumenFormate;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = 3;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (DocumentList.IsATFlag != input.IsATFlag)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsATFlag";
                dataVaultsHistory.PrevValue = DocumentList.IsATFlag.ToString();
                dataVaultsHistory.CurValue = input.IsATFlag.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = 3;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (DocumentList.DocumenStorageUnit != input.DocumenStorageUnit)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "DocumenStorageUnit";
                dataVaultsHistory.PrevValue = DocumentList.DocumenStorageUnit;
                dataVaultsHistory.CurValue = input.DocumenStorageUnit;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = 3;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, DocumentList);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentList_Delete)]
        public async Task Delete(EntityDto input)
        {
            var documentResult = await _documentListRepository.FirstOrDefaultAsync(input.Id);
            await _documentListRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 3; // DocumentList Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Document Deleted Name :-" + documentResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }


    }
}
