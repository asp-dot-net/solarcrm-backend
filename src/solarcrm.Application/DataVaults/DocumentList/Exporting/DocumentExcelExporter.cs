﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DocumentList.Exporting
{
    public class DocumentExcelExporter : NpoiExcelExporterBase, IDocumentExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DocumentExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDocumentListForViewDto> documentList)
        {
            return CreateExcelPackage(
                "DocumentList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DocumentList"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, leadSource,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, documentList,
                        _ => _.DocumentList.Name
                        );
                });
        }
    }
}
