﻿using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.DocumentList.Exporting
{
    public interface IDocumentExcelExporter
    {
        FileDto ExportToFile(List<GetDocumentListForViewDto> documentList);
    }
}
