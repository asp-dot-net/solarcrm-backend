﻿using solarcrm.DataVaults.SolarType.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.SolarType.Exporting
{
    public interface ISolarTypeExcelExporter
    {
        FileDto ExportToFile(List<GetSolarTypeForViewDto> SolarTypes);
    }
}
