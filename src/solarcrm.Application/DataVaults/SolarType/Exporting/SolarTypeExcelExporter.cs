﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.SolarType.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.SolarType.Exporting
{
    public class SolarTypeExcelExporter : NpoiExcelExporterBase, ISolarTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SolarTypeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetSolarTypeForViewDto> solartype)
        {
            return CreateExcelPackage(
                "SolarType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("SolarType"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, solartype,
                        _ => _.SolarTypes.Name
                        );
                });
        }
    }
}
