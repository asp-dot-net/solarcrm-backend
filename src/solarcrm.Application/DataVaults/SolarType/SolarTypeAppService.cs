﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.SolarType.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.SolarType.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.SolarType
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SolarType)]
    public class SolarTypeAppService : solarcrmAppServiceBase, ISolarTypeAppService
    {
        private readonly IRepository<SolarType> _solarTypeRepository;
        private readonly ISolarTypeExcelExporter _solarTypeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public SolarTypeAppService(IRepository<SolarType> SolarTypeRepository, ISolarTypeExcelExporter SolarTypeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _solarTypeRepository = SolarTypeRepository;
            _solarTypeExcelExporter = SolarTypeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetSolarTypeForViewDto>> GetAll(GetAllSolarTypeInput input)
        {
            var filteredSolarType = _solarTypeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredSolarType = filteredSolarType.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var solartype = from o in pagedAndFilteredSolarType
                            select new GetSolarTypeForViewDto()
                            {
                                SolarTypes = new SolarTypeDto
                                {
                                    Name = o.Name,
                                    IsActive = o.IsActive,
                                    Id = o.Id,
                                    CreatedDate = o.CreationTime
                                }
                            };

            var totalCount = await filteredSolarType.CountAsync();

            return new PagedResultDto<GetSolarTypeForViewDto>(totalCount, await solartype.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SolarType_Edit)]
        public async Task<GetSolarTypeForEditOutput> GetSolarTypeForEdit(EntityDto input)
        {
            var solartype = await _solarTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSolarTypeForEditOutput { SolarTypes = ObjectMapper.Map<CreateOrEditSolarTypeDto>(solartype) };

            return output;
        }
        public async Task<GetSolarTypeForViewDto> GetSolarTypeForView(int id)
        {
            var solartype = await _solarTypeRepository.GetAsync(id);

            var output = new GetSolarTypeForViewDto { SolarTypes = ObjectMapper.Map<SolarTypeDto>(solartype) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SolarType_ExportToExcel)]
        public async Task<FileDto> GetSolarTypeToExcel(GetAllSolarTypeForExcelInput input)
        {
            try
            {
                var filteredSolarType = _solarTypeRepository.GetAll()
                      .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

                var query = (from o in filteredSolarType
                             select new GetSolarTypeForViewDto()
                             {
                                 SolarTypes = new SolarTypeDto
                                 {
                                     Name = o.Name,
                                     IsActive = o.IsActive,
                                     Id = o.Id
                                 }
                             });

                var solarListDtos = await query.ToListAsync();

                return _solarTypeExcelExporter.ExportToFile(solarListDtos);
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }
        public async Task CreateOrEdit(CreateOrEditSolarTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SolarType_Create)]
        protected virtual async Task Create(CreateOrEditSolarTypeDto input)
        {
            var solarTypes = ObjectMapper.Map<SolarType>(input);

            if (AbpSession.TenantId != null)
            {
                solarTypes.TenantId = (int?)AbpSession.TenantId;
            }

            var solarTypeID = await _solarTypeRepository.InsertAndGetIdAsync(solarTypes);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 26; // SolarType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Solar Type Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = solarTypeID;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SolarType_Edit)]
        protected virtual async Task Update(CreateOrEditSolarTypeDto input)
        {
            var solarTypes = await _solarTypeRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 26; // City Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Solar Type Updated Name :-" + solarTypes.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var solerTypesActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (solarTypes.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = solarTypes.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = solerTypesActivityLogId;
                dataVaultsHistory.SectionId = 26; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (solarTypes.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = solarTypes.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = solerTypesActivityLogId;
                dataVaultsHistory.SectionId = 16;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (solarTypes.IsManualPricing != input.IsManualPricing)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsManualPricing";
                dataVaultsHistory.PrevValue = solarTypes.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = solerTypesActivityLogId;
                dataVaultsHistory.SectionId = 16;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, solarTypes);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SolarType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var solarTypeResult = await _solarTypeRepository.FirstOrDefaultAsync(input.Id);
            await _solarTypeRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 26; // Solar Type Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Deleted
            dataVaulteActivityLog.ActionNote = "Solar Type  Deleted Name :-" + solarTypeResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
        public async Task<List<SolarTypeLookupTable>> GetAllSolarTypeForDropdown()
        {
            var allsolartype = _solarTypeRepository.GetAll().Where(x => x.IsActive == true);

            var solartype = from o in allsolartype
                            select new SolarTypeLookupTable()
                            {
                                Name = o.Name,
                                Id = o.Id
                            };

            return new List<SolarTypeLookupTable>(await solartype.ToListAsync());
        }
    }
}
