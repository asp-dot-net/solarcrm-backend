﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Organizations;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.DataVaults.BillOfMaterial.Exporting;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.BillOfMaterial.SampleExporting;
using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.BillOfMaterial
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial)]
    public class BillOfMaterialAppService : solarcrmAppServiceBase, IBillOfMaterialAppService
    {
        private readonly IRepository<BillOfMaterial> _billofMaterialRepository;
        private readonly IBillOfMaterialExcelExporter _billofMaterialExcelExporter;
        private readonly ISampleBillOfMaterialExcelExporter _samplebillofMaterialExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<StockItem.StockItem> _stockItemRepository;
        private readonly IRepository<HeightStructure.HeightStructure> _heightofStructureRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly int _sectionId = 61; // BillOfMaterial Pages Name

        public BillOfMaterialAppService(IRepository<BillOfMaterial> billofMaterialRepository,
            IBillOfMaterialExcelExporter billofMaterialExcelExporter,
            ISampleBillOfMaterialExcelExporter samplebillofMaterialExcelExporter,
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<StockItem.StockItem> stockItemRepository,
            IRepository<HeightStructure.HeightStructure> heightofStructureRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _billofMaterialRepository = billofMaterialRepository;
            _billofMaterialExcelExporter = billofMaterialExcelExporter;
            _stockItemRepository = stockItemRepository;
            _heightofStructureRepository = heightofStructureRepository;
            _samplebillofMaterialExcelExporter = samplebillofMaterialExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _organizationUnitRepository = organizationUnitRepository;
        }

        public async Task<PagedResultDto<GetBillOfMaterialForViewDto>> GetAll(GetAllBillOfMaterialInput input)
        {
            var filteredBillofMaterial = _billofMaterialRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.StockItemFk.Name.Contains(input.Filter) || e.HeightStructureIdFk.Name.Contains(input.Filter));

            var pagedAndFilteredStockItem = filteredBillofMaterial.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var materialItems = from o in pagedAndFilteredStockItem
                                select new GetBillOfMaterialForViewDto()
                                {
                                    BillOfMaterialItem = new BillOfMaterialDto
                                    {
                                        Id = o.Id,
                                        MaterialName = o.StockItemFk.Name,
                                        HeightStructure = o.HeightStructureIdFk.Name,
                                        Panel_S = o.Panel_S,
                                        Panel_E = o.Panel_E,
                                        Quantity = o.Quantity,
                                        IsActive = o.Active
                                    }
                                };

            var totalCount = await materialItems.CountAsync();

            return new PagedResultDto<GetBillOfMaterialForViewDto>(totalCount, await materialItems.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_ExportToExcel)]
        public async Task<FileDto> GetBillOfMaterialToExcel(GetAllBillOfMaterialForExcelInput input)
        {
            var filteredBillofMaterial = _billofMaterialRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.StockItemFk.Name.Contains(input.Filter));

            var query = (from o in filteredBillofMaterial
                         select new GetBillOfMaterialForViewDto()
                         {
                             BillOfMaterialItem = new BillOfMaterialDto
                             {
                                 Id = o.Id,
                                 MaterialName = o.StockItemFk.Name,
                                 HeightStructure = o.HeightStructureIdFk.Name,
                                 Panel_S = o.Panel_S,
                                 Panel_E = o.Panel_E,
                                 Quantity = o.Quantity
                             }
                         });
            var stockItemListDtos = await query.ToListAsync();

            return _billofMaterialExcelExporter.ExportToFile(stockItemListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_ExportToExcelSample)]
        public async Task<FileDto> GetBillOfMaterialToExcelSample(GetAllBillOfMaterialForExcelInput input)
        {
            try
            {
                var filteredBillofMaterial = _stockItemRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

                var query = (from o in filteredBillofMaterial
                             select new GetBillOfMaterialForViewSampleExportDto()
                             {
                                 BillOfMaterialsample = new BillOfMaterialSampleDto
                                 {
                                     StockItem = o.Name,
                                     Panel_S = string.Empty,
                                     Panel_E = string.Empty
                                     //HeightOfStructure = _heightofStructureRepository.GetAll().Where(x => x.IsActive).Select(x => x.Name).ToList()
                                 }
                             }
                             );
                List<CreateOrEditHeightStructureDto> HeightofstructureList = new List<CreateOrEditHeightStructureDto>();

                var structureheightList = _heightofStructureRepository.GetAll().Where(x => x.IsActive).Select(x => x.Name).ToList();
                var stockItemListDtos = await query.ToListAsync();

                return _samplebillofMaterialExcelExporter.ExportToSampleFile(stockItemListDtos, structureheightList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Edit)]
        public async Task<GetBillOfMaterialForEditOutput> GetBillOfMaterialForEdit(EntityDto input)
        {
            var billofMaterial = await _billofMaterialRepository.FirstOrDefaultAsync(input.Id);


            var output = new GetBillOfMaterialForEditOutput
            {
                BillOfMaterialItem = ObjectMapper.Map<CreateOrEditBillOfMaterialDto>(billofMaterial)
            };
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Create, AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Edit)]
        public async Task CreateOrEdit(CreateOrEditBillOfMaterialDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Create)]
        protected virtual async Task Create(CreateOrEditBillOfMaterialDto input)
        {
            var billOfMaterial = ObjectMapper.Map<BillOfMaterial>(input);

            if (AbpSession.TenantId != null)
            {
                billOfMaterial.TenantId = (int)AbpSession.TenantId;
            }

            var billofMterialId = await _billofMaterialRepository.InsertAndGetIdAsync(billOfMaterial);
            var stockItemName = _stockItemRepository.GetAll().Where(e=>e.Id==billOfMaterial.StockItemId).FirstOrDefault().Name;
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId;
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created;
            dataVaulteActivityLog.ActionNote = "Bill Of Material Item Created Name :- ";// + billOfMaterial.StockItemFk.Name ;
            dataVaulteActivityLog.SectionValueId = billofMterialId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Edit)]
        protected virtual async Task Update(CreateOrEditBillOfMaterialDto input)
        {

            var billOfMaterial = await _dbContextProvider.GetDbContext().billOfMaterial.Where(x => x.Id == input.Id).Include(x => x.StockItemFk).Include(x => x.HeightStructureIdFk).FirstOrDefaultAsync();

            var stockItemName = _stockItemRepository.GetAll().Where(e=>e.Id== input.StockItemId).FirstOrDefault().Name;
            var billOfMaterialStockItemName = _stockItemRepository.GetAll().Where(e => e.Id == billOfMaterial.StockItemId).FirstOrDefault().Name;
            var heightStructure = _heightofStructureRepository.GetAll().Where(e => e.Id == input.HeightStructureId).FirstOrDefault().Name;


            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId; // Bill of material Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Bill Of Material Item Updated Name :- " + billOfMaterial.StockItemFk.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var stockItemActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (billOfMaterial.StockItemId != input.StockItemId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "StockItem";
                dataVaultsHistory.PrevValue = billOfMaterialStockItemName;
                dataVaultsHistory.CurValue = stockItemName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (billOfMaterial.HeightStructureId != input.HeightStructureId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "HeightStructure";
                dataVaultsHistory.PrevValue = billOfMaterial.HeightStructureIdFk.Name;
                dataVaultsHistory.CurValue = heightStructure;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (billOfMaterial.Panel_S != input.Panel_S)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Panel_S";
                dataVaultsHistory.PrevValue = Convert.ToString(billOfMaterial.Panel_S);
                dataVaultsHistory.CurValue = Convert.ToString(input.Panel_S);
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (billOfMaterial.Panel_E != input.Panel_E)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Panel_E";
                dataVaultsHistory.PrevValue = Convert.ToString(billOfMaterial.Panel_E);
                dataVaultsHistory.CurValue = Convert.ToString(input.Panel_E);
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (billOfMaterial.Quantity != input.Quantity)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Quantity";
                dataVaultsHistory.PrevValue = Convert.ToString(billOfMaterial.Quantity);
                dataVaultsHistory.CurValue = Convert.ToString(input.Quantity);
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (billOfMaterial.Active != input.Active)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Active";
                dataVaultsHistory.PrevValue = billOfMaterial.Active.ToString();
                dataVaultsHistory.CurValue = input.Active.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = stockItemActivityLogId;
                dataVaultsHistory.SectionId = _sectionId;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();
            #endregion

            ObjectMapper.Map(input, billOfMaterial);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _billofMaterialRepository.DeleteAsync(input.Id);
            //await _stockItemLocationRepository.DeleteAsync(o => o.StockItemId == input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = _sectionId;
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Deleted
            dataVaulteActivityLog.ActionNote = "Bill of Material Items Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
