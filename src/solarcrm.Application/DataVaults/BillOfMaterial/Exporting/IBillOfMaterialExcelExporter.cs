﻿using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.Exporting
{
    public interface IBillOfMaterialExcelExporter
    {
        FileDto ExportToFile(List<GetBillOfMaterialForViewDto> BillOfMaterialItems);
    }
}
