﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.Exporting
{
    public class BillOfMaterialExcelExporter : NpoiExcelExporterBase, IBillOfMaterialExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public BillOfMaterialExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetBillOfMaterialForViewDto> materialItems)
        {
            return CreateExcelPackage(
                "BillOfMaterial.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("BillOfMaterial"));

                    AddHeader(
                        sheet,
                        L("MaterialDesc"),
                        L("HeightOfStructure"),
                        L("PanelS"),
                        L("PanelE"),
                        L("Qty")                        
                        );

                    AddObjects(
                        sheet, materialItems,
                        _ => _.BillOfMaterialItem.MaterialName,
                        _ => _.BillOfMaterialItem.HeightStructure,
                        _ => _.BillOfMaterialItem.Panel_S,
                        _ => _.BillOfMaterialItem.Panel_E,
                        _ => _.BillOfMaterialItem.Quantity
                        );
                });
        }
    }
}
