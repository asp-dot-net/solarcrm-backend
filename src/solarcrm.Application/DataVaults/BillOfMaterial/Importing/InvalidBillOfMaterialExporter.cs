﻿using Abp.Dependency;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.BillOfMaterial.Importing.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.Importing
{
    public class InvalidBillOfMaterialExporter : NpoiExcelExporterBase, IInvalidBillOfMaterialExporter, ITransientDependency
    {
        public InvalidBillOfMaterialExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportBillOfMaterialDto> billofMaterialListDtos)
        {
            return CreateExcelPackage(
                "InvalidbillofMaterialImportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InvalidbillofMaterialImports"));

                    AddHeader(
                        sheet,
                        L("Manufacturer"),
                        L("HeightOfStructure"),
                        L("PanelS"),
                        L("PanelE"),
                        L("Quantity"),
                        L("Refuse Reason")
                    );

                    AddObjects(
                        sheet, billofMaterialListDtos,
                        _ => _.ManufactureDesc,
                        _ => _.HeightOfStructure,
                        _ => _.PanelS,
                        _ => _.PanelE,
                        _ => _.Quantity,
                        _ => _.Exception
                    );

                    for (var i = 0; i < 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

    }
}
