﻿using Abp.Domain.Repositories;
using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.BillOfMaterial.Importing.Dto;
using solarcrm.DataVaults.HeightStructure.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.Importing
{
    public class BillOfMaterialListExcelDataReader : NpoiExcelImporterBase<ImportBillOfMaterialDto>, IBillOfMaterialListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;
        private readonly IRepository<HeightStructure.HeightStructure> _heightofStructureRepository;
        public BillOfMaterialListExcelDataReader(ILocalizationManager localizationManager,
            IRepository<HeightStructure.HeightStructure> heightofStructureRepository)
        {
            _localizationSource = localizationManager.GetSource(solarcrmConsts.LocalizationSourceName);
            _heightofStructureRepository = heightofStructureRepository;
        }

        public List<ImportBillOfMaterialDto> GetBillOfMaterialFromExcel(byte[] fileBytes, string filename)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow, filename);
        }

        private ImportBillOfMaterialDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (IsRowEmpty(worksheet, row))
            {
                return null;
            }

            var exceptionMessage = new StringBuilder();
            var stockItem = new ImportBillOfMaterialDto();

            List<CreateOrEditHeightStructureDto> HeightofstructureList = new List<CreateOrEditHeightStructureDto>();

            var structureheightList = _heightofStructureRepository.GetAll().Where(x => x.IsActive).Select(x => x.Name).ToList();
            IRow roww = worksheet.GetRow(row);
            List<ICell> cells = roww.Cells;
            List<string> rowData = new List<string>();

            for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
            {
                ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                rowData.Add(cell.ToString());
            }

            try
            {
                stockItem.ManufactureDesc = rowData[0].Trim();
                stockItem.HeightOfStructure = rowData[1];
                stockItem.PanelS = Convert.ToInt32(rowData[2]);
                stockItem.PanelE = Convert.ToInt32(rowData[3]);
                stockItem.HeightOfStructureValue = new List<Int32>();
                
                for (int i = 3; i < structureheightList.Count; i++)
                {
                    stockItem.HeightOfStructureValue.Add(Convert.ToInt32(rowData[i]));
                }
            }
            catch (System.Exception exception)
            {
                stockItem.Exception = exception.Message;
            }

            return stockItem;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }

        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;

        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
