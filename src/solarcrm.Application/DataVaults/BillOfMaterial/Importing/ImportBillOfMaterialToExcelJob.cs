﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.DataVaults.BillOfMaterial.Importing.Dto;
using solarcrm.EntityFrameworkCore;
using solarcrm.Notifications;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
namespace solarcrm.DataVaults.BillOfMaterial.Importing
{
    public class ImportBillOfMaterialToExcelJob : BackgroundJob<ImportBillOfMaterialFromExcelJobArgs>, ITransientDependency
    {
        private readonly IRepository<BillOfMaterial> _billofMaterialRepository;
        private readonly IBillOfMaterialListExcelDataReader _billofMaterialListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidBillOfMaterialExporter _invalidBillOfMaterialExporter;
        private readonly IRepository<StockItem.StockItem> _stockItemrepository;
        private readonly IRepository<HeightStructure.HeightStructure> _heightofStructureRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private int _sectionId = 25;

        public ImportBillOfMaterialToExcelJob(
            IRepository<BillOfMaterial> billofMaterialRepository,
            IBillOfMaterialListExcelDataReader billofMaterialListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidBillOfMaterialExporter invalidBillOfMaterialExporter,
            IRepository<StockItem.StockItem> stockItemrepository,
            IRepository<HeightStructure.HeightStructure> heightofStructureRepository,
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider
            )
        {
            _billofMaterialRepository = billofMaterialRepository;
            _billofMaterialListExcelDataReader = billofMaterialListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _invalidBillOfMaterialExporter = invalidBillOfMaterialExporter;
            _stockItemrepository = stockItemrepository;
            _heightofStructureRepository = heightofStructureRepository;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
        }


        [UnitOfWork]
        public override void Execute(ImportBillOfMaterialFromExcelJobArgs args)
        {
            var stockItems = GetBillOfMaterialListFromExcelOrNull(args);
            if (stockItems == null || !stockItems.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidStockItems = new List<ImportBillOfMaterialDto>();

                foreach (var stockItem in stockItems)
                {
                    if (stockItem.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateOrUpdateStockItemAsync(stockItem, args));
                        }
                        catch (UserFriendlyException exception)
                        {
                            //Lead.Exception = exception.Message;
                            invalidStockItems.Add(stockItem);
                        }
                        //catch (Exception e)
                        catch
                        {
                            //Lead.Exception = e.ToString();
                            invalidStockItems.Add(stockItem);
                        }
                    }
                    else
                    {
                        invalidStockItems.Add(stockItem);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportLeadsResultAsync(args, invalidStockItems));
                    }

                    uow.Complete();
                }
            }
        }

        private List<ImportBillOfMaterialDto> GetBillOfMaterialListFromExcelOrNull(ImportBillOfMaterialFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        string filename = Path.GetExtension(file.Description);
                        return _billofMaterialListExcelDataReader.GetBillOfMaterialFromExcel(file.Bytes, filename);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateOrUpdateStockItemAsync(ImportBillOfMaterialDto input, ImportBillOfMaterialFromExcelJobArgs args)
        {
            BillOfMaterial billofMaterialitem = new BillOfMaterial();

            var stockItemId = 0;
            var heightofstructureId = 0;
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                if (input.ManufactureDesc != null)
                {
                    stockItemId = await _stockItemrepository.GetAll().Where(e => e.Name.ToUpper() == input.ManufactureDesc.ToUpper()).Select(e => e.Id).FirstOrDefaultAsync();
                }

                if (stockItemId == 0)
                {
                    input.Exception = "Stock Item Not Found";
                    throw new Exception("Stock Item Not Found");
                }

                if (input.HeightOfStructure != null)
                {
                    heightofstructureId = await _heightofStructureRepository.GetAll().Where(e => e.Name.ToUpper() == input.HeightOfStructure.ToUpper()).Select(e => e.Id).FirstOrDefaultAsync();
                }

                if (heightofstructureId == 0)
                {
                    input.Exception = "Height of Structure Not Found";
                    throw new Exception("Height of Structure Not Found");
                }

                billofMaterialitem = await _billofMaterialRepository.GetAll().Where(e => e.StockItemId == stockItemId).FirstOrDefaultAsync();
                if (billofMaterialitem == null)
                {
                    var HeightofStructureList = await _heightofStructureRepository.GetAll().Where(x => x.IsActive).ToListAsync();
                    if (HeightofStructureList != null)
                    {
                        for (int i = 0; i < input.HeightOfStructureValue.Count; i++)
                        {
                            var billOFMaterialNew = _objectMapper.Map<BillOfMaterial>(input);
                            billOFMaterialNew.TenantId = (int)args.TenantId;
                            billOFMaterialNew.StockItemId = stockItemId;
                            billOFMaterialNew.HeightStructureId = heightofstructureId;//input.HeightOfStructure;
                            billOFMaterialNew.Panel_S = input.PanelS;
                            billOFMaterialNew.Panel_E = input.PanelE;
                            billOFMaterialNew.Quantity = input.HeightOfStructureValue[i];
                            billOFMaterialNew.Active = true;
                            billOFMaterialNew.CreatorUserId = (int)args.User.UserId;
                            var billofMaterialId = await _billofMaterialRepository.InsertAndGetIdAsync(billOFMaterialNew);


                            //Add Activity Log
                            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                            dataVaulteActivityLog.TenantId = (int)args.TenantId;
                            dataVaulteActivityLog.SectionId = _sectionId;
                            dataVaulteActivityLog.ActionId = 1; //1-Created, 2-Updated, 3-Deleted
                            dataVaulteActivityLog.ActionNote = "Bill Of Material Created From Excel";
                            dataVaulteActivityLog.SectionValueId = billofMaterialId;
                            dataVaulteActivityLog.CreatorUserId = (int)args.User.UserId;
                            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
                        }
                    }

                    //if (stockItemId > 0)
                    //{
                    //    var locationList = await _locationRepository.GetAll().ToListAsync();
                    //    foreach (var location in locationList)
                    //    {
                    //        CreateOrEditStockItemLocationDto createOrEditStockItemLocationDto = new CreateOrEditStockItemLocationDto();
                    //        var stockItemLocation = _objectMapper.Map<StockItemLocation>(createOrEditStockItemLocationDto);
                    //        stockItemLocation.TenantId = (int)args.TenantId;
                    //        stockItemLocation.StockItemId = stockItemId;
                    //        stockItemLocation.LocationId = location.Id;
                    //        stockItemLocation.MinQty = 0;
                    //        stockItemLocation.IsActive = true;
                    //        stockItemLocation.CreatorUserId = (int)args.User.UserId;

                    //        await _stockItemLocationRepository.InsertAsync(stockItemLocation);

                    //        //Add Activity Log
                    //        DataVaulteActivityLog dataVaulteActivityLog1 = new DataVaulteActivityLog();
                    //        dataVaulteActivityLog1.TenantId = (int)args.TenantId;
                    //        dataVaulteActivityLog1.SectionId = _sectionId;
                    //        dataVaulteActivityLog1.ActionId = 1; //1-Created, 2-Updated, 3-Deleted
                    //        dataVaulteActivityLog1.ActionNote = "Stock Item Created for " + location.Name + " Location From Excel";
                    //        dataVaulteActivityLog1.SectionValueId = stockItemId;
                    //        dataVaulteActivityLog1.CreatorUserId = (int)args.User.UserId;
                    //        await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog1);
                    //    }
                    //}
                }
            }
        }

        private void SendInvalidExcelNotification(ImportBillOfMaterialFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToStockItem", solarcrmConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportLeadsResultAsync(ImportBillOfMaterialFromExcelJobArgs args, List<ImportBillOfMaterialDto> invalidBillOfMaterial)
        {
            if (invalidBillOfMaterial.Any())
            {
                var file = _invalidBillOfMaterialExporter.ExportToFile(invalidBillOfMaterial);
                await _appNotifier.SomeStockItemCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllBillOfMaterialSuccessfullyImportedFromExcel", solarcrmConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }
    }
}
