﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.Importing.Dto
{
    public class ImportBillOfMaterialDto
    {
        public string ManufactureDesc { get; set; }

        public int? PanelS { get; set; }

        public int? PanelE { get; set; }

        public int? Quantity { get; set; }

        public string HeightOfStructure { get; set; }
        public List<Int32> HeightOfStructureValue { get; set; }

        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
