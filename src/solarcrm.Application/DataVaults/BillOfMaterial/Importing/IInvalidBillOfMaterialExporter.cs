﻿using solarcrm.DataVaults.BillOfMaterial.Importing.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.Importing
{
    public interface IInvalidBillOfMaterialExporter
    {
        FileDto ExportToFile(List<ImportBillOfMaterialDto> billofMaterialListDto);
    }
}
