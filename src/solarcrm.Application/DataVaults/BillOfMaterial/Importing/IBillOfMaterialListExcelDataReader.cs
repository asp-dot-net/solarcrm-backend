﻿using Abp.Dependency;
using solarcrm.DataVaults.BillOfMaterial.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.Importing
{
    public interface IBillOfMaterialListExcelDataReader : ITransientDependency
    {
        List<ImportBillOfMaterialDto> GetBillOfMaterialFromExcel(byte[] fileBytes , string filename);
    }
}
