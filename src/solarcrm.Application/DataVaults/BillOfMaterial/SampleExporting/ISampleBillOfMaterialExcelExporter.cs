﻿using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.SampleExporting
{
    public interface ISampleBillOfMaterialExcelExporter
    {
        FileDto ExportToSampleFile(List<GetBillOfMaterialForViewSampleExportDto> BillOfMaterialSample, List<string> heightofstructure);
    }
}
