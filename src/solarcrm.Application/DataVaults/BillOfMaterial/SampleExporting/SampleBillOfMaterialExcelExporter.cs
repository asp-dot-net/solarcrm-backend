﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial.SampleExporting
{
    public class SampleBillOfMaterialExcelExporter : NpoiExcelExporterBase, ISampleBillOfMaterialExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SampleBillOfMaterialExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToSampleFile(List<GetBillOfMaterialForViewSampleExportDto> materialItems, List<string> heightofstructurelist)
        {
            return CreateExcelPackage(
                "BillOfMaterial.xlsx",
                excelPackage =>
                {
                    try
                    {
                        var sheet = excelPackage.CreateSheet(L("BillOfMaterial"));
                        //var Heightstructure = heightofstructurelist; // materialItems.FirstOrDefault().BillOfMaterialsample.HeightOfStructure;
                        List<string> addHightofstructureList = new List<string>();
                        addHightofstructureList.Add("MaterialDesc");
                        addHightofstructureList.Add("PanelS");
                        addHightofstructureList.Add("PanelE");
                        foreach (var item in heightofstructurelist)
                        {
                            addHightofstructureList.Add(item);
                        }
                        string[] structureheightData1 = addHightofstructureList.ToArray(); //heightofstructurelist.ToArray();
                      

                        AddHeader(
                            sheet,
                            structureheightData1
                            //L("MaterialDesc"),
                            //L("PanelS"),
                            //L("PanelE"),
                          
                            );

                        AddObjects(
                            sheet, materialItems,
                            _ => _.BillOfMaterialsample.StockItem                           
                            );
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                });
        }
    }
}
