﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.LeadSource.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.LeadSource.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using solarcrm.Organizations;
using Abp.Organizations;
using solarcrm.Common;
using solarcrm.DataVaults.City;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using PayPalCheckoutSdk.Orders;

namespace solarcrm.DataVaults.LeadSource
{
    public class LeadSourceAppService : solarcrmAppServiceBase, ILeadSourceAppService
    {
        private readonly IRepository<LeadSource> _leadSourceRepository;
        private readonly ILeadSourceExcelExporter _leadSourceExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IRepository<MultipleFieldOrganizationUnitSelection> _multipleFieldOrganizationUnitSelectionRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private int? cityId;

        public LeadSourceAppService(IRepository<LeadSource> LeadSourceRepository, ILeadSourceExcelExporter leadSourceExcelExporter,
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<MultipleFieldOrganizationUnitSelection> multipleFieldOrganizationUnitSelectionRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _leadSourceRepository = LeadSourceRepository;
            _leadSourceExcelExporter = leadSourceExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _multipleFieldOrganizationUnitSelectionRepository = multipleFieldOrganizationUnitSelectionRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _dbContextProvider = dbContextProvider;
        }

      
        public async Task<PagedResultDto<GetLeadSourceForViewDto>> GetAll(GetAllLeadSourceInput input)
        {
            var filteredLeadSource = _leadSourceRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredLeadSource = filteredLeadSource.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var leadSource = from o in pagedAndFilteredLeadSource
                             select new GetLeadSourceForViewDto()
                             {
                                 LeadSource = new LeadSourceDto
                                 {
                                     Name = o.Name,
                                     IsActive = o.IsActive,
                                     OrganizationUnitsName = _organizationUnitRepository.GetAll().Where(x =>_multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.LeadSourceOrganizationId != null && x.LeadSourceOrganizationId == o.Id).Select(x=>x.OrganizationUnitId).ToList().Contains(x.Id)).Select(x => x.DisplayName).ToList(),
                                     Id = o.Id
                                 }
                             };

            var totalCount = await filteredLeadSource.CountAsync();

            return new PagedResultDto<GetLeadSourceForViewDto>(totalCount, await leadSource.ToListAsync());
        }


        public async Task CreateOrEdit(CreateOrEditLeadSourceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadSource_Edit)]
        public async Task<GetLeadSourceForEditOutput> GetLeadSourceForEdit(EntityDto input)
        {
            var leadSource = await _leadSourceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadSourceForEditOutput { LeadSource = ObjectMapper.Map<CreateOrEditLeadSourceDto>(leadSource) };
            output.LeadSource.OrganizationUnits = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.LeadSourceOrganizationId == input.Id).Select(x => x.OrganizationUnitId).ToList();
            return output;
        }

        public async Task<GetLeadSourceForViewDto> GetLeadSourceForView(int id)
        {
            var leadSource = await _leadSourceRepository.GetAsync(id);

            var output = new GetLeadSourceForViewDto { LeadSource = ObjectMapper.Map<LeadSourceDto>(leadSource) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadSource_ExportToExcel)]
        public async Task<FileDto> GetLeadSourceToExcel(GetAllLeadSourceForExcelInput input)
        {
            var filteredLeadSources = _leadSourceRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredLeadSources
                         select new GetLeadSourceForViewDto()
                         {
                             LeadSource = new LeadSourceDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var leadSourcesListDtos = await query.ToListAsync();

            return _leadSourceExcelExporter.ExportToFile(leadSourcesListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadSource_Create)]
        protected virtual async Task Create(CreateOrEditLeadSourceDto input)
        {
            var leadSource = ObjectMapper.Map<LeadSource>(input);

            if (AbpSession.TenantId != null)
            {
                leadSource.TenantId = (int?)AbpSession.TenantId;
            }

            var leadSourceId =  await _leadSourceRepository.InsertAndGetIdAsync(leadSource);

            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 1; // City Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "leadSource Created Name ;-" + input.Name;
            dataVaulteActivityLog.SectionValueId = leadSourceId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);

            foreach (var item in input.OrganizationUnits)
            {
                try
                {
                    MultipleFieldOrganizationUnitSelection leadSourcerg = new MultipleFieldOrganizationUnitSelection();
                    leadSourcerg.LeadSourceOrganizationId = leadSourceId;
                    leadSourcerg.OrganizationUnitId = item;
                    leadSourcerg.TenantId = (int?)AbpSession.TenantId;
                    await _multipleFieldOrganizationUnitSelectionRepository.InsertOrUpdateAsync(leadSourcerg);
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadSource_Edit)]
        protected virtual async Task Update(CreateOrEditLeadSourceDto input)
        {
            var leadSource = await _leadSourceRepository.FirstOrDefaultAsync((int)input.Id);
            var OrganizationUnits = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.LeadSourceOrganizationId == leadSource.Id).Select(e=>e.OrganizationUnitId).ToList();
            string inputOrg = "";
            string leadSourceOrg = "";
            foreach (var organizationUnit in input.OrganizationUnits)
            {
                if (inputOrg == "")
                {
                    inputOrg = _organizationUnitRepository.GetAll().Where(e=> e.Id==organizationUnit).Select(e=>e.DisplayName).FirstOrDefault();
                }
                else
                {
                    inputOrg = inputOrg + ", " + _organizationUnitRepository.GetAll().Where(e => e.Id == organizationUnit).Select(e => e.DisplayName).FirstOrDefault();
                }
            }
            foreach (var organizationUnit in OrganizationUnits)
            {
                if (leadSourceOrg == "")
                {
                    leadSourceOrg = _organizationUnitRepository.GetAll().Where(e => e.Id == organizationUnit).Select(e => e.DisplayName).FirstOrDefault();
                }
                else
                {
                    leadSourceOrg = leadSourceOrg + ", " + _organizationUnitRepository.GetAll().Where(e => e.Id == organizationUnit).Select(e => e.DisplayName).FirstOrDefault();
                }
            }

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 1; // City Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated;  // Updated
            dataVaulteActivityLog.ActionNote = "leadSource Updated Name ;-" + leadSource.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var leadSourceActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (leadSource.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = leadSource.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = leadSourceActivityLogId;
                dataVaultsHistory.SectionId = 1; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (leadSource.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = leadSource.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = leadSourceActivityLogId;
                dataVaultsHistory.SectionId = 1;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (OrganizationUnits != input.OrganizationUnits)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "OrganizationUnits";
                dataVaultsHistory.PrevValue = leadSourceOrg;
                dataVaultsHistory.CurValue = inputOrg;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = leadSourceActivityLogId;
                dataVaultsHistory.SectionId = 1;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion


            ObjectMapper.Map(input, leadSource);

            //var leadSourcerg = ObjectMapper.Map<MultipleFieldOrganizationUnitSelection>(input);
            //leadSourcerg.LeadSourceOrganizationId = leadSource.Id;
            ////leadSourcerg.OrganizationUnitId = input.OrganizationUnits;
            //await _multipleFieldOrganizationUnitSelectionRepository.InsertOrUpdateAsync(leadSourcerg);

            foreach (var item in input.OrganizationUnits)
            {
                try
                {
                    MultipleFieldOrganizationUnitSelection leadSourcerg = new MultipleFieldOrganizationUnitSelection();

                    var checkorg = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.LeadSourceOrganizationId == leadSource.Id && x.OrganizationUnitId == item).FirstOrDefault();
                    if (checkorg != null)
                    {
                        checkorg.LeadSourceOrganizationId = leadSource.Id;
                        checkorg.OrganizationUnitId = item;
                        checkorg.TenantId = (int?)AbpSession.TenantId;
                        await _multipleFieldOrganizationUnitSelectionRepository.InsertOrUpdateAsync(checkorg);
                    }
                    else
                    {
                        leadSourcerg.LeadSourceOrganizationId = leadSource.Id;
                        leadSourcerg.OrganizationUnitId = item;
                        leadSourcerg.TenantId = (int?)AbpSession.TenantId;
                        await _multipleFieldOrganizationUnitSelectionRepository.InsertOrUpdateAsync(leadSourcerg);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
            var checkorg1 = _multipleFieldOrganizationUnitSelectionRepository.GetAll().Where(x => x.LeadSourceOrganizationId == leadSource.Id && !input.OrganizationUnits.Contains(x.OrganizationUnitId)).ToList();
            if (checkorg1.Count() > 0)
            {
                foreach (var item in checkorg1)
                {
                    await _multipleFieldOrganizationUnitSelectionRepository.DeleteAsync(item);
                }                   
            }



            
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadSource_Delete)]
        public async Task Delete(EntityDto input)
        {
            var leadsourceResult = await _leadSourceRepository.FirstOrDefaultAsync(input.Id);
            await _leadSourceRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 1; // LeadSource Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Lead Source Deleted Name :-" + leadsourceResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);

        }

        public async Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForDropdown()
        {
            var allleadsource = _leadSourceRepository.GetAll().Where(x => x.IsActive == true);

            var leadsource = from o in allleadsource
                             select new LeadSourceLookupTableDto()
                             {
                                 DisplayName = o.Name,
                                 Id = o.Id
                             };

            return new List<LeadSourceLookupTableDto>(await leadsource.ToListAsync());
        }
    }
}
