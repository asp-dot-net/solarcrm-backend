﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.LeadSource.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadSource.Exporting
{
    public class LeadSourceExcelExporter : NpoiExcelExporterBase, ILeadSourceExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LeadSourceExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetLeadSourceForViewDto> leadSource)
        {
            return CreateExcelPackage(
                "LeadSource.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LeadSource"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, leadSource,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, leadSource,
                        _ => _.LeadSource.Name
                        );
                });
        }
    }
}
