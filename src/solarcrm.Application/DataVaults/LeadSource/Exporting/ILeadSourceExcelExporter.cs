﻿using solarcrm.DataVaults.LeadSource.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.LeadSource.Exporting
{
    public interface ILeadSourceExcelExporter
    {
        FileDto ExportToFile(List<GetLeadSourceForViewDto> leadSource);
    }
}
