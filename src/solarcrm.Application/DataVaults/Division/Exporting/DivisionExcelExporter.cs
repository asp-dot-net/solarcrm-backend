﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Division.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Division.Exporting
{
    public class DivisionExcelExporter : NpoiExcelExporterBase, IDivisionExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DivisionExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDivisionForViewDto> division)
        {
            return CreateExcelPackage(
                "Division.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Division"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Circle")
                        );

                    AddObjects(
                        sheet, division,
                        _ => _.Division.Name,
                        _ => _.Division.Circle
                        );
                });
        }
    }
}
