﻿using solarcrm.DataVaults.Division.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.Division.Exporting
{
    public interface IDivisionExcelExporter
    {
        FileDto ExportToFile(List<GetDivisionForViewDto> division);
    }
}
