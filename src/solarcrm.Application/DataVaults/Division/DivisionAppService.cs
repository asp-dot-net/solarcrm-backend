﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Division.Exporting;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.Division.Dto;
using Abp.Authorization;
using solarcrm.Authorization;
using Abp.Application.Services.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.Division
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division)]
    public class DivisionAppService : solarcrmAppServiceBase, IDivisionAppService
    {
        private readonly IRepository<Division> _divisionRepository;
        private readonly IDivisionExcelExporter _divisionExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Circle.Circle> _circleRepository;

        public DivisionAppService(IRepository<Division> divisionRepository, IDivisionExcelExporter divisionExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<Circle.Circle> circleRepository)
        {
            _divisionRepository = divisionRepository;
            _divisionExcelExporter = divisionExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _circleRepository = circleRepository;
        }


        public async Task<PagedResultDto<GetDivisionForViewDto>> GetAll(GetAllDivisionInput input)
        {
            var filteredDivision = _divisionRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredDivision = filteredDivision.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var division = from o in pagedAndFilteredDivision
                           select new GetDivisionForViewDto()
                           {
                               Division = new DivisionDto
                               {
                                   Name = o.Name,
                                   IsActive = o.IsActive,
                                   Id = o.Id,
                                   CreatedDate = o.CreationTime,
                                   Circle = o.CircleFk.Name
                               }
                           };

            var totalCount = await filteredDivision.CountAsync();

            return new PagedResultDto<GetDivisionForViewDto>(totalCount, await division.ToListAsync());
        }

        public async Task CreateOrEdit(CreateOrEditDivisionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

       
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Edit)]
        public async Task<GetDivisionForEditOutput> GetDivisionForEdit(EntityDto input)
        {
            var discom = await _divisionRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDivisionForEditOutput { Division = ObjectMapper.Map<CreateOrEditDivisionDto>(discom) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_ExportToExcel)]
        public async Task<FileDto> GetDivisionToExcel(GetAllDivisionForExcelInput input)
        {
            var filteredDiscom = _divisionRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredDiscom
                         select new GetDivisionForViewDto()
                         {
                             Division = new DivisionDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 Circle = o.CircleFk.Name
                             }
                         });

            var DiscomsListDtos = await query.ToListAsync();

            return _divisionExcelExporter.ExportToFile(DiscomsListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Create)]
        protected virtual async Task Create(CreateOrEditDivisionDto input)
        {
            var division = ObjectMapper.Map<Division>(input);

            if (AbpSession.TenantId != null)
            {
                division.TenantId = (int?)AbpSession.TenantId;
            }

            var divisionResult = await _divisionRepository.InsertAndGetIdAsync(division);
             
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 11; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Division Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = divisionResult;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Edit)]
        protected virtual async Task Update(CreateOrEditDivisionDto input)
        {
            var divisionResult = await _divisionRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            int SectionId = 11; // Discom Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated;  // Updated
            dataVaulteActivityLog.ActionNote = "Division Updated Name :-" + divisionResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var divisionListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (divisionResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = divisionResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (divisionResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = divisionResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (divisionResult.CircleId != input.CircleId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Circle";
                dataVaultsHistory.PrevValue = _circleRepository.GetAll().Where(e => e.Id == divisionResult.CircleId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _circleRepository.GetAll().Where(e => e.Id == input.CircleId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, divisionResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Delete)]
        public async Task Delete(EntityDto input)
        {
            var divisionResult = await _divisionRepository.FirstOrDefaultAsync(input.Id);
            await _divisionRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 11; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Created
            dataVaulteActivityLog.ActionNote = "Division Deleted Name :-" + divisionResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<List<GetAllDivisionForDropdown>> GetAllDivisionForDropdown()
        {
            var allDivision = _divisionRepository.GetAll().Where(x => x.IsActive == true);

            var division = from o in allDivision
                           select new GetAllDivisionForDropdown()
                         {
                             Name = o.Name,
                             Id = o.Id
                         };

            return new List<GetAllDivisionForDropdown>(await division.ToListAsync());
        }
    }
}
