﻿using solarcrm.DataVaults.ActivityLogMasterVault.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Exporting
{
    public interface IDataValultLogExcelExporter
    {
        FileDto ExportToFile(List<GetDataVaultLogForViewDto> dataVaultLog);
    }
}
