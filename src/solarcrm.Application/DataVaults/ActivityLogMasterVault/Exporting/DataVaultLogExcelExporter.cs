﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.ActivityLogMasterVault.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Exporting
{
    public class DataVaultLogExcelExporter : NpoiExcelExporterBase, IDataValultLogExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DataVaultLogExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetDataVaultLogForViewDto> dataVaultLog)
        {
            return CreateExcelPackage(
                "DataVaultLog.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DataVaultLog"));

                    AddHeader(
                        sheet,
                        L("SectionName"),
                        L("LastUpdatedBy"),
                        L("LastUpdateDate"),
                        L("ActionName")
                        );

                    AddObjects(
                        sheet, dataVaultLog,
                        _ => _.DataVault.SectionName,
                         _ => _.DataVault.UserName,
                          _ => _.DataVault.LastModificationTime,
                           _ => _.DataVault.ActionName
                        );
                });
        }

    }
}
