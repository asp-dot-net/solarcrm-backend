﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLogMasterVault.Dto;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using solarcrm.Dto;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.ActivityLogMasterVault.Exporting;
using System;

namespace solarcrm.DataVaults.ActivityLogMasterVault
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ActivityLogMasterVault)]
    public class ActivityLogMasterVaultAppService : solarcrmAppServiceBase, IActivityLogMasterVault
    {
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;       
        //private readonly IDataValultLogExcelExporter _dataValultLogExcelExporter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Sections.Section> _sectionRepository;
        private readonly IRepository<DataVaultsHistory> _dataVaultsHistoryRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public ActivityLogMasterVaultAppService(IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IRepository<User, long> userRepository, IRepository<Sections.Section> sectionRepository,
            IRepository<DataVaultsHistory> dataVaultsHistoryRepository,
            //IDataValultLogExcelExporter dataValultLogExcelExporter,
        IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;             
            //_dataValultLogExcelExporter = dataValultLogExcelExporter;
            _userRepository = userRepository;
            _sectionRepository = sectionRepository;
            _dataVaultsHistoryRepository = dataVaultsHistoryRepository;
            _dbContextProvider = dbContextProvider;

        }

        public async Task<PagedResultDto<GetDataVaultLogForViewDto>> GetAllDataVaultLog(GetAllDataVaultLogInput input)
        {
            //var SectionName = _sectionRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.SectionName.Contains(input.Filter));

            var sectionList = from n in _dataVaultsActivityLogRepository.GetAll()
                              group n by n.SectionId into g
                              select g.OrderByDescending(t => t.CreationTime).Where(x => x.Id != 0)
                              .FirstOrDefault();

            var filteredDataLog = _dataVaultsActivityLogRepository.GetAll().Where(e => sectionList.ToList().Select(x => x.Id).Contains(e.Id));

            var pagedAndFilteredLog = filteredDataLog.OrderBy(input.Sorting ?? "id desc").PageBy(input);


            var dataVaultLog = from o in pagedAndFilteredLog
                               select new GetDataVaultLogForViewDto()
                               {
                                   DataVault = new DataVaultLogDto
                                   {
                                       SectionName = _sectionRepository.GetAll().Where(a => a.SectionId == o.SectionId).Select(x => x.SectionName).FirstOrDefault(),
                                       UserName = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                       LastModificationTime = o.LastModificationTime,
                                       IpAddress = "",
                                       ActionName = o.DataVaultsActionFk.Name,
                                       SectionIsd = o.SectionId,
                                       Id = o.Id
                                   }
                               };

            var dt = dataVaultLog.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.DataVault.SectionName.Contains(input.Filter));

            var totalCount = await dt.CountAsync();
            return new PagedResultDto<GetDataVaultLogForViewDto>(totalCount, await dt.ToListAsync());
         
        }

        public async Task<List<DataVaultLogDto>> GetDataVaultLogForView(int sectionId)
        {
           
            try
            {

                var Result = (from item in _dataVaultsActivityLogRepository.GetAll()
                              where (item.SectionId == sectionId)
                              select new DataVaultLogDto()
                              {
                                  Id = item.Id,
                                  SectionIsd = item.SectionId,
                                  SectionName = _sectionRepository.GetAll().Where(a => a.SectionId == item.SectionId).Select(x => x.SectionName).FirstOrDefault(),
                                  UserName = _userRepository.GetAll().Where(e => e.Id == item.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                  LastModificationTime = item.CreationTime,
                                  ActionName = item.DataVaultsActionFk.Name,
                                  ActionNote = item.ActionNote

                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
           
        }

        public async Task<List<DataVaultHistoriesLogDto>> GetDataVaultHistoriesLogForView(int id)
        {
          
                var Result = (from item in _dataVaultsHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.ActivityLogId == id)
                              select new DataVaultHistoriesLogDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  prevValue = item.PrevValue,
                                  curValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            
            
          
        }

        //public async Task<FileDto> GetDataVaultToExcel(GetAllDataVaultLogForExcelInput input)
        //{
        //    var filteredDepartment = _dataVaultsActivityLogRepository.GetAll()
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.SectionFk.SectionName.Contains(input.Filter));

        //    var query = (from o in filteredDepartment
        //                 select new GetDataVaultLogForViewDto()
        //                 {
        //                     DataVault = new DataVaultLogDto
        //                     {
        //                         SectionName = o.SectionFk.SectionName,
        //                         UserName = o.CreatorUserId.ToString(),
        //                         LastModificationTime = o.LastModificationTime,
        //                         IpAddress = "",
        //                         ActionName = o.DataVaultsActionFk.Name,
        //                         Id = o.Id
        //                     }
        //                 });

        //    var departmentListDtos = await query.ToListAsync();

        //    return _dataValultLogExcelExporter.ExportToFile(departmentListDtos);
        //}

    }
}
