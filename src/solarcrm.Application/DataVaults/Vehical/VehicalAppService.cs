﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Vehical.Dto;
using solarcrm.DataVaults.Vehical.Exporting;
using solarcrm.Dto;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using solarcrm.DataVaults.Tender.Dto;
using solarcrm.DataVaults.Tender.Exporting;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;

namespace solarcrm.DataVaults.Vehical
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Vehical)]
    public class VehicalAppService : solarcrmAppServiceBase, IVehicalAppService
    {
        private readonly IRepository<Vehical> _vehicalRepository;
        private readonly IVehicalExcelExporter _vehicalExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public VehicalAppService(IRepository<Vehical> vehicalRepository, IVehicalExcelExporter vehicalExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _vehicalRepository = vehicalRepository;
            _vehicalExcelExporter = vehicalExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task CreateOrEdit(CreateOrEditVehicalDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Vehical_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _vehicalRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }

            dataVaulteActivityLog.SectionId = 31; // Division Pages Name
            dataVaulteActivityLog.ActionId = 3; // Created
            dataVaulteActivityLog.ActionNote = "Vehical Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<PagedResultDto<GetVehicalForViewDto>> GetAll(GetAllVehicalInput input)
        {
            var filteredVehical = _vehicalRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.VehicalType.Contains(input.Filter));

            var pagedAndFilteredDivision = filteredVehical.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var division = from o in pagedAndFilteredDivision
                           select new GetVehicalForViewDto()
                           {
                               vehical = new VehicalDto
                               {
                                   VehicalType = o.VehicalType,
                                   VehicalRegNo = o.VehicalRegNo,                                   
                                   IsActive = o.IsActive,
                                   Id = o.Id,
                                   CreatedDate = o.CreationTime
                               }
                           };

            var totalCount = await filteredVehical.CountAsync();

            return new PagedResultDto<GetVehicalForViewDto>(totalCount, await division.ToListAsync());
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Vehical_Edit)]
        public async Task<GetVehicalForEditOutput> GetVehicalForEdit(EntityDto input)
        {
            var vehical = await _vehicalRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetVehicalForEditOutput { Vehical = ObjectMapper.Map<CreateOrEditVehicalDto>(vehical) };

            return output;
        }
        public async Task<GetVehicalForViewDto> GetVehicalForView(int id)
        {
            var vehical = await _vehicalRepository.GetAsync(id);

            var output = new GetVehicalForViewDto { vehical = ObjectMapper.Map<VehicalDto>(vehical) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Vehical_ExportToExcel)]
        public async Task<FileDto> GetVehicalToExcel(GetAllVehicalForExcelInput input)
        {
            var filteredTender = _vehicalRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.VehicalType.Contains(input.Filter));

            var query = (from o in filteredTender
                         select new GetVehicalForViewDto()
                         {
                             vehical = new VehicalDto
                             {
                                 VehicalType = o.VehicalType,
                                 VehicalRegNo = o.VehicalRegNo,                                
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                             }
                         });

            var TenderListDtos = await query.ToListAsync();

            return _vehicalExcelExporter.ExportToFile(TenderListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Vehical_Create)]
        protected virtual async Task Create(CreateOrEditVehicalDto input)
        {
            try
            {
                var tender = ObjectMapper.Map<Vehical>(input);

                if (AbpSession.TenantId != null)
                {
                    tender.TenantId = (int?)AbpSession.TenantId;
                }
                var tenderId = await _vehicalRepository.InsertAndGetIdAsync(tender);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 56; // Division Pages Name
                dataVaulteActivityLog.ActionId = 1; // Created
                dataVaulteActivityLog.ActionNote = "Vehical Created";
                dataVaulteActivityLog.SectionValueId = tenderId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Edit)]
        protected virtual async Task Update(CreateOrEditVehicalDto input)
        {
            var division = await _vehicalRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            int SectionId = 56; // Discom Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Discom Pages Name
            dataVaulteActivityLog.ActionId = 2; // Updated
            dataVaulteActivityLog.ActionNote = "Vehical Updated";
            dataVaulteActivityLog.SectionValueId = input.Id;
            var divisionListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (division.VehicalType != input.VehicalType)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "VehicalType";
                dataVaultsHistory.PrevValue = division.VehicalType;
                dataVaultsHistory.CurValue = input.VehicalType;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (division.VehicalRegNo != input.VehicalRegNo)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "VehicalRegNo";
                dataVaultsHistory.PrevValue = division.VehicalType;
                dataVaultsHistory.CurValue = input.VehicalRegNo;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (division.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = division.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, division);
        }
    }
}
