﻿using solarcrm.DataVaults.Vehical.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Vehical.Exporting
{
    public interface IVehicalExcelExporter
    {
        FileDto ExportToFile(List<GetVehicalForViewDto> tender);
    }
}
