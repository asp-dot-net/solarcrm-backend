﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Vehical.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Vehical.Exporting
{
    public class VehicalExcelExporter : NpoiExcelExporterBase, IVehicalExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public VehicalExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetVehicalForViewDto> vehical)
        {
            return CreateExcelPackage(
                "Vehical.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Vehical"));

                    AddHeader(
                        sheet,
                        L("VehicalType"),
                        L("VehicalRegNo")                       
                        );

                    AddObjects(
                        sheet, vehical,
                        _ => _.vehical.VehicalType,
                        _ => _.vehical.VehicalRegNo
                        );
                });
        }
    }
}
