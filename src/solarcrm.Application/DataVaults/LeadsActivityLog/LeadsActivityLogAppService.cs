﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using solarcrm.DataVaults.LeadsActivityLog.Dto;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.ActivityLog;
using Abp.Notifications;
using System.Net.Mail;
using solarcrm.Authorization.Users;
using solarcrm.Notifications;
using Abp.Net.Mail;
using Abp.UI;
using System.Text.RegularExpressions;
using Abp.Timing.Timezone;
using RestSharp;
using solarcrm.Organizations;
using solarcrm.Quotations;
using System.Web;
using Abp.Runtime.Security;
using solarcrm.Quotation;
using solarcrm.Configuration;
using Abp.Extensions;
using System.IO;

namespace solarcrm.DataVaults.LeadsActivityLog
{
    public class LeadsActivityLogAppService : solarcrmAppServiceBase, ILeadsActivityLogAppService
    {
        private readonly IRepository<LeadActivityLogs> _leadActivityLogRepository;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IRepository<Leads.Leads> _leadRepository;
        private readonly IRepository<LeadAction.LeadAction> _leadAction;
        private readonly IRepository<LeadtrackerHistory> _LeadtrackerHistoryRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Sections.Section> _SectionAppService;
        private readonly IUserEmailer _userEmailer;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IRepository<Job.Jobs, int> _jobRepository;
        //private readonly IGeneralMethod _generalMethod;
        private readonly IRepository<SmsLeadActivityLog> _smsleadRepository;
        private readonly IRepository<EmailLeadActivityLog> _emailleadRepository;
        private readonly IRepository<NotifyLeadActivityLog> _notifyleadRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderleadRepository;
        private readonly IRepository<ToDoLeadActivityLog> _todoleadRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentleadRepository;
        private readonly IRepository<LeadActivity.LeadActivity> _leadactivityRepository;
        private readonly IRepository<SMS_ServiceProviderOrganization> _smsProviderUnitRepository;
        //private readonly IApplicationSettingsAppService _applicationSettings;
        private readonly IRepository<QuotationData> _quotationRepository;
        private readonly IRepository<QuotationLinkHistory> _quotationLinkHistoryRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IAppConfigurationAccessor _configurationAccessor;
        public LeadsActivityLogAppService(IRepository<LeadActivityLogs> LeadActivityLogRepository,
             IRepository<LeadtrackerHistory> leadHistoryLogRepository,
             IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
             IRepository<SmsLeadActivityLog> smsLogRepository,
             IRepository<EmailLeadActivityLog> emailLogRepository,
             IRepository<NotifyLeadActivityLog> notifyLogRepository,
             IRepository<ReminderLeadActivityLog> reminderLogRepository,
             IRepository<ToDoLeadActivityLog> todoLogRepository,
             IRepository<Leads.Leads> leadRepository,
             IRepository<LeadActivity.LeadActivity> leadactivityLogRepository,
             IRepository<LeadtrackerHistory> leadtrackHistoryRepository,
             IRepository<CommentLeadActivityLog> commentleadRepository,
             IRepository<LeadAction.LeadAction> leadAction,
             IAppConfigurationAccessor configurationAccessor,
             IRepository<Job.Jobs, int> jobRepository,
             IRepository<User, long> userRepository,
             IAppNotifier appNotifier,
             IUserEmailer userEmailer,
             IEmailSender emailSender,
             IRepository<QuotationData> quotationRepository,
             IRepository<QuotationLinkHistory> quotationLinkHistoryRepository,        
             ITimeZoneConverter timeZoneConverter,
             IRepository<Sections.Section> SectionAppService,
             IRepository<SMS_ServiceProviderOrganization> SMSserviceProviderRepository,
             IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _leadActivityLogRepository = LeadActivityLogRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _leadRepository = leadRepository;
            _userRepository = userRepository;
            _smsleadRepository = smsLogRepository;
            _emailleadRepository = emailLogRepository;
            _notifyleadRepository = notifyLogRepository;
            _reminderleadRepository = reminderLogRepository;
            _todoleadRepository = todoLogRepository;
            _leadactivityRepository = leadactivityLogRepository;
            _LeadtrackerHistoryRepository = leadtrackHistoryRepository;
            _appNotifier = appNotifier;
            _userEmailer = userEmailer;
            _emailSender = emailSender;
            _quotationLinkHistoryRepository = quotationLinkHistoryRepository;
            _quotationRepository = quotationRepository;
            _jobRepository = jobRepository;
            _SectionAppService = SectionAppService;
            _leadAction = leadAction;
            _configurationAccessor = configurationAccessor;
            _commentleadRepository = commentleadRepository;
            //_generalMethod = generalMethod;
            _timeZoneConverter = timeZoneConverter;
            _smsProviderUnitRepository = SMSserviceProviderRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<List<GetLeadsActivityLogForViewDto>> GetAllActivityLog(GetActivityLogInput input)
        {
            //var myleadactivitylog = _leadActivityLogRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnit && e.LeadId == input.LeadId);

            //var pagedAndFilteredCity = myleadactivitylog.OrderBy(input.Sorting ?? "id desc").PageBy(input);
            List<int> leadActionids = new List<int>() { 1, 2, 3, 4, 5 };
            List<int> smsActionids = new List<int>() { 6, 20 };
            List<int> jobActionids = new List<int>() { 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 };
            List<int> promotionActionids = new List<int>() { 21, 22 };
            List<int> serviceids = new List<int>() { 27, 29, 30 };
            var allactionids = _leadAction.GetAll().Select(e => e.Id).ToList();
            var Result = _leadActivityLogRepository
              .GetAll()
              .WhereIf(input.ActionId == 0, L => allactionids.Contains(L.LeadActionId))     //All Activity
              .WhereIf(input.ActionId == 1, L => leadActionids.Contains(L.LeadActionId))    //Lead
              .WhereIf(input.ActionId == 6, L => smsActionids.Contains(L.LeadActionId))     //SMS
              .WhereIf(input.ActionId == 7, L => L.LeadActionId == 7)                       //Email
              .WhereIf(input.ActionId == 8, L => L.LeadActionId == 8)                       //Remider
              .WhereIf(input.ActionId == 9, L => L.LeadActionId == 9)                       //Notify
              .WhereIf(input.ActionId == 10, L => jobActionids.Contains(L.LeadActionId))    //Job
              .WhereIf(input.ActionId == 11, L => promotionActionids.Contains(L.LeadActionId))//Promotion
              .WhereIf(input.ActionId == 24, L => L.LeadActionId == 24)                       //Comment
              .WhereIf(input.ActionId == 27, L => serviceids.Contains(L.LeadActionId))        //Service
              .WhereIf(input.ActionId == 28, L => L.LeadActionId == 28)                       //Review
              .WhereIf(input.CurrentPage != false, L => L.SectionId == input.SectionId)
              .WhereIf(input.OnlyMy != false, L => L.CreatorUserId == AbpSession.UserId)
              .Where(L => L.LeadId == input.LeadId)
              .OrderByDescending(e => e.Id);

            var leads = from o in Result
                        join o1 in _leadAction.GetAll() on o.LeadActionId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()

                        join o2 in _leadRepository.GetAll() on o.LeadId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _userRepository.GetAll() on o.CreatorUserId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _todoleadRepository.GetAll() on o.Id equals o4.LeadActivityLogId into j4
                        from s4 in j4.DefaultIfEmpty()

                        join o5 in _smsleadRepository.GetAll() on o.Id equals o5.LeadActivityLogId into j5
                        from s5 in j5.DefaultIfEmpty()

                        join o6 in _emailleadRepository.GetAll() on o.Id equals o6.LeadActivityLogId into j6
                        from s6 in j6.DefaultIfEmpty()

                        join o7 in _reminderleadRepository.GetAll() on o.Id equals o7.LeadActivityLogId into j7
                        from s7 in j7.DefaultIfEmpty()

                        join o8 in _notifyleadRepository.GetAll() on o.Id equals o8.LeadActivityLogId into j8
                        from s8 in j8.DefaultIfEmpty()

                        join o9 in _commentleadRepository.GetAll() on o.Id equals o9.LeadActivityLogId into j9
                        from s9 in j9.DefaultIfEmpty()

                        select new GetLeadsActivityLogForViewDto()
                        {
                            Id = o.Id,
                            ActionName = s1 == null || s1.LeadActionName == null ? "" : s1.LeadActionName.ToString(),
                            ActionId = o.LeadActionId,
                            ActionNote = o.LeadActionNote,
                            ActivityNote = s5.SmsActivityBody != null ? s5.SmsActivityBody :                                                           //smsnote
                                                           s6.EmailActivityBody != null ? s6.EmailActivityBody :                                       //EmailNote
                                                           s4.TodoActivityNote != null ? s4.TodoActivityNote :                                             //TodoNote
                                                           s7.ReminderActivityNote != null ? s7.ReminderActivityNote :                                 //ReminderNote
                                                           s8.NotifyActivityNote != null ? s8.NotifyActivityNote :                                     //NotifiyNote
                                                           s9.CommentActivityNote != null ? s9.CommentActivityNote : "",                               //CommentNote                           
                            LeadId = o.LeadId,
                            CreationTime = o.CreationTime,
                            ActivityDate = s7.ReminderDate,
                            Section = _SectionAppService.GetAll().Where(e => e.SectionId == o.SectionId).Select(e => e.SectionName).FirstOrDefault(),
                            ShowLeadJobDetail = o.LeadActionId == 2 ? 1 : (o.LeadActionId == 11 || o.LeadActionId == 13 || o.LeadActionId == 15 || o.LeadActionId == 16 || o.LeadActionId == 18 || o.LeadActionId == 19) ? 2 : !o.LeadDocumentPath.IsNullOrEmpty() ? 4:  0,
                            //ShowJobLink = link == true ? 1 : 2,
                            ActivitysDate = s7.ReminderDate.Value.ToString("dd-MM-yyyy hh:mm:ss"), //.ToString("dd-MM-yyyy hh:mm:ss"),
                            CreatorUserName = s3 == null || s3.FullName == null ? "" : s3.FullName.ToString(),
                            LeadCompanyName = s2 == null || s2.FirstName == null ? "" : s2.FirstName.ToString(),
                            LeadActionColorClass = s1.LeadActionColorClass,
                            LeadActionIconClass = s1.LeadActionIconClass,
                            LeadDocumentPath = o.LeadDocumentPath,
                            //LeadStatusColorClass = s1.LeadStatusColorClass;
                        };
            return new List<GetLeadsActivityLogForViewDto>(
               await leads.ToListAsync()
           );
        }


        public async Task<GetLeadsActivityLogForViewDto> GetLeadsActivityLogForView(int id)
        {
            var leadactivity = await _leadActivityLogRepository.GetAsync(id);

            var output = new GetLeadsActivityLogForViewDto { LeadActivityLogs = ObjectMapper.Map<LeadActivityLogDto>(leadactivity) };

            return output;
        }


        public async Task AddActivityLog(ActivityLogInput input)
        {
            try
            {
                Attachment attachment1;
                SMS_ServiceProviderOrganization SmsConfiguration = new SMS_ServiceProviderOrganization();
                var activity = ObjectMapper.Map<LeadActivityLogs>(input);
                if (input.LeadId != 0 && input.OrganizationUnit != 0)
                {
                    SmsConfiguration = _smsProviderUnitRepository.GetAll().Where(e => e.SMSProviderOrganizationId == input.OrganizationUnit).FirstOrDefault();
                    var CurrentUser = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
                    var lead = await _leadRepository.FirstOrDefaultAsync(input.LeadId);
                    var assignedToUser = _userRepository.GetAll().Where(u => u.Id == lead.ChanelPartnerID).FirstOrDefault();
                    var leadActivity = _leadactivityRepository.GetAll().Where(u => u.Id == input.ActivityId).FirstOrDefault();
                    string BaseURL = _configurationAccessor.Configuration["App:ClientSignatureRootAddress"];

                    activity.LeadActionNote = input.ActionNote;
                    //activity.Subject = input.Subject;            
                    activity.LeadId = input.LeadId;
                    activity.LeadActionId = input.ActionId;
                    activity.LeadAcitivityID = input.ActivityId;
                    activity.SectionId = input.SectionId;
                    if (AbpSession.TenantId != null)
                    {
                        activity.TenantId = (int)AbpSession.TenantId;
                    }

                    var SectionName = _SectionAppService.GetAll().Where(e => e.SectionId == input.SectionId).Select(e => e.SectionName).FirstOrDefault();

                    //activity.Body = Regex.Replace(input.Body, "<.*?>", String.Empty);
                    // activity.CreatorUserId = AbpSession.UserId;
                    //activity.CreationTime = DateTime.UtcNow;
                    //activity.LastModificationTime = DateTime.UtcNow;
                    await _leadActivityLogRepository.InsertAndGetIdAsync(activity);

                    //Comment Activity
                    if (input.ActionId == 24)
                    {
                        var CommentLeadActivityLogs = ObjectMapper.Map<CommentLeadActivityLog>(input);
                        CommentLeadActivityLogs.LeadActivityLogId = activity.Id; //input.ActivityId;
                        CommentLeadActivityLogs.CommentActivityNote = input.ActivityNote;
                        await _commentleadRepository.InsertAndGetIdAsync(CommentLeadActivityLogs);

                    }
                    //Notify Activity
                    if (input.ActionId == 9 && !string.IsNullOrEmpty(lead.FirstName) && !string.IsNullOrEmpty(CurrentUser.Name))
                    {
                        string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.FirstName + " By " + CurrentUser.Name + " From " + SectionName;
                        await _appNotifier.LeadComment(assignedToUser, msg, NotificationSeverity.Warn);

                        var NotifyLeadActivityLogs = ObjectMapper.Map<NotifyLeadActivityLog>(input);
                        NotifyLeadActivityLogs.LeadActivityLogId = activity.Id; //input.ActivityId;
                        NotifyLeadActivityLogs.NotifyActivityNote = input.ActivityNote;
                        await _notifyleadRepository.InsertAndGetIdAsync(NotifyLeadActivityLogs);

                    }
                    //Todo Activity
                    if (input.ActionId == 25 && input.UserId > 0)
                    {
                        var assignedToUserForTodo = _userRepository.GetAll().Where(u => u.Id == input.UserId).FirstOrDefault();
                        string msg = input.ActionNote + " " + input.ActivityNote + " For " + lead.FirstName + " By " + CurrentUser.Name + " From " + SectionName;
                        await _appNotifier.LeadComment(assignedToUserForTodo, msg, NotificationSeverity.Warn);

                        var TodoLeadActivityLogs = ObjectMapper.Map<ToDoLeadActivityLog>(input);
                        TodoLeadActivityLogs.LeadActivityLogId = activity.Id;
                        TodoLeadActivityLogs.CreatorUserId = AbpSession.UserId;
                        TodoLeadActivityLogs.TodopriorityId = input.TodopriorityId;
                        TodoLeadActivityLogs.TodopriorityName = "";
                        TodoLeadActivityLogs.IsTodoComplete = true;
                        TodoLeadActivityLogs.TodoActivityNote = input.ActivityNote;
                        TodoLeadActivityLogs.TodoTag = "";
                        await _todoleadRepository.InsertAndGetIdAsync(TodoLeadActivityLogs);
                    }
                    //Reminder Activity
                    if (input.ActionId == 8 && !string.IsNullOrEmpty(lead.FirstName) && !string.IsNullOrEmpty(CurrentUser.Name))
                    {
                        var CurrentTime = (_timeZoneConverter.Convert(DateTime.UtcNow, (int)AbpSession.TenantId));
                        if (Convert.ToDateTime(input.Body) < CurrentTime)
                        {
                            throw new UserFriendlyException(L("PleaseSelectGreaterDatethanCurrentDate"));
                        }
                        //activity.CreationTime = Convert.ToDateTime(input.Body);
                        input.Body = input.ActivityNote;
                        string msg = string.Format("Reminder " + input.ActionNote + " " + input.ActivityNote + " For " + lead.FirstName + " By " + CurrentUser.Name + " From " + SectionName);
                        await _appNotifier.LeadAssiged(CurrentUser, msg, NotificationSeverity.Info);

                        var ReminderActivityLogs = ObjectMapper.Map<ReminderLeadActivityLog>(input);
                        ReminderActivityLogs.LeadActivityLogId = activity.Id;
                        ReminderActivityLogs.ReminderDate = input.ActivityDate; //Convert.ToDateTime(input.Body);//
                        ReminderActivityLogs.ReminderActivityNote = input.ActivityNote;
                        await _reminderleadRepository.InsertAndGetIdAsync(ReminderActivityLogs);
                    }
                    //Email Activity
                    if (input.ActionId == 7 && !string.IsNullOrEmpty(input.EmailId))
                    {
                        //Quote template
                        if (input.ActionNote.Contains("quote"))
                        {
                            var job = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).FirstOrDefault();
                            var quotation = _quotationRepository.GetAll().Where(e => e.QuoteJobId == job.Id).OrderByDescending(e => e.Id).FirstOrDefault();
                            //var lead = _leadRepository.GetAll().Where(e => e.Id == job.LeadId).FirstOrDefault();
                            var QuoteUrl = job.TenantId + "," + job.Id + "," + quotation.Id + "," + input.OrganizationUnit;
                            //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
                            var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(QuoteUrl, AppConsts.DefaultPassPhrase));
                            var token1 = SimpleStringCipher.Instance.Encrypt(QuoteUrl, AppConsts.DefaultPassPhrase);

                            //string BaseURL = _configurationAccessor.Configuration["App:ClientSignatureRootAddress"];
                            //System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];
                            input.JobNumber = job.JobNumber;
                            QuoteUrl = BaseURL + token;
                            //string BaseURL = System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];

                            //QuoteUrl = "http://localhost:4200/signature/customer-signature?STR=" + token;
                            //smsBody = "www.thesolarproduct.com/account/customer-signature?STR=" + token;


                            QuotationLinkHistory quotationLinkHistory = new QuotationLinkHistory();
                            quotationLinkHistory.Expired = false;
                            quotationLinkHistory.QuotationId = quotation.Id;
                            quotationLinkHistory.Token = token1;
                            await _quotationLinkHistoryRepository.InsertAsync(quotationLinkHistory);
                            var htmlbody = Convert.ToString(input.Body).Replace("#quotelinkurl", QuoteUrl);
                            input.Body = htmlbody;
                        }
                        if (!string.IsNullOrEmpty(input.EmailId))
                        {                           
                            MailMessage mail = new MailMessage
                            {
                                From = new MailAddress(input.EmailFrom),
                                To = { input.EmailId },
                                Subject = input.Subject,
                                Body = input.Body,                                
                                IsBodyHtml = true,                                
                            };
                            if (!string.IsNullOrEmpty(input.FileAttachUrl))
                            {
                                System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(input.FileAttachUrl.ToString());
                                using (System.Net.HttpWebResponse HttpWResp = (System.Net.HttpWebResponse)req.GetResponse())
                                using (Stream responseStream = HttpWResp.GetResponseStream())
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    responseStream.CopyTo(ms);
                                    ms.Seek(0, SeekOrigin.Begin);
                                    Attachment attachment = new Attachment(ms, "Quote_" + input.JobNumber + ".pdf");
                                    mail.Attachments.Add(attachment);                               
                                }
                            }                            
                            await _emailSender.SendAsync(mail);
                            if (input.EmailTemplateId == 0)
                            {
                                input.EmailTemplateId = null;
                            }
                            var EmailActivityLogs = ObjectMapper.Map<EmailLeadActivityLog>(input);
                            EmailActivityLogs.LeadActivityLogId = activity.Id;
                            EmailActivityLogs.EmailTemplateId = input.EmailTemplateId;
                            EmailActivityLogs.Subject = input.Subject;
                            EmailActivityLogs.EmailActivityBody = Regex.Replace(input.Body, "<.*?>", String.Empty);
                            await _emailleadRepository.InsertAndGetIdAsync(EmailActivityLogs);
                        }
                        else
                        {
                            throw new UserFriendlyException(L("NotFoundEmailId"));
                        }
                    }
                    //SMS Activity
                    if (input.ActionId == 6 && input.Mobile != "")
                    {
                        if (SmsConfiguration != null)
                        {
                        var baseUrl = SmsConfiguration.msgtype;// solarcrmConsts.SMSBaseUrl; //"https://insprl.com";
                        var client = new RestClient(baseUrl);
                        var request = new RestRequest("/api/sms/send-msg", Method.Post);
                        var requesturl = new RestRequest("/api/url/get-short-url", Method.Post);
                        //request.RequestFormat = DataFormat.Json;
                        //request.AddParameter("Application/Json", myObject, ParameterType.RequestBody);
                        //request.AddUrlSegment("name", "Gaurav");
                        //request.AddUrlSegment("fathername", "Lt. Sh. Ramkrishan");
                        if (baseUrl != "" && input.ActionNote.Contains("quote"))
                        {
                            var job = _jobRepository.GetAll().Where(e => e.LeadId == input.LeadId).FirstOrDefault();
                            var quotation = _quotationRepository.GetAll().Where(e => e.QuoteJobId == job.Id).OrderByDescending(e => e.Id).FirstOrDefault();

                            var smsBody = job.TenantId + "," + job.Id + "," + quotation.Id;
                            //Token With Tenant Id & Promotion User Primary Id for subscribe & UnSubscribe
                            var token = HttpUtility.UrlEncode(SimpleStringCipher.Instance.Encrypt(smsBody, AppConsts.DefaultPassPhrase));
                            var token1 = SimpleStringCipher.Instance.Encrypt(smsBody, AppConsts.DefaultPassPhrase);

                            //string BaseURL = System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];

                            //smsBody = "http://localhost:4200/app/main/signature/customer-signature?STR=" + token;
                            //smsBody = "www.thesolarproduct.com/account/customer-signature?STR=" + token;

                            
                            //System.Configuration.ConfigurationManager.AppSettings["ClientRootAddress"];

                            smsBody = BaseURL + token;

                            requesturl.AddHeader("content-type", "application/x-www-form-urlencoded");
                            requesturl.AddHeader("Authorization", SmsConfiguration.SMS_AuthorizationKey); //"SyMTYj4X9&8CA*P6s1iDl^75"
                            requesturl.AddParameter("client_id", SmsConfiguration.Client_Id); //"Csprl_DP2AEBO9FQN4UKT"
                            requesturl.AddParameter("long_url", smsBody); //"APSOLR"
                            requesturl.AddParameter("tag_type", "random");
                            requesturl.AddParameter("tag_keyword", "");//"Dear [Pravin test], Greetings from Australian Premium Solar (India) Pvt. Ltd. Your application for solar rooftop system is successfully registered with us. Thank you for choosing APS. Feel free to call on [07966006600] for any query. Regards, Team APS");
                            requesturl.AddParameter("domain", "");  //"txn"
                            requesturl.AddParameter("expiry_date", "");
                            var responseadmin = await client.ExecuteAsync(requesturl);
                                
                                if (Convert.ToString(responseadmin.StatusCode).ToLower() != "ok")
                                {
                                    throw new UserFriendlyException(L(responseadmin.Content));
                                }

                            QuotationLinkHistory quotationLinkHistory = new QuotationLinkHistory();
                            quotationLinkHistory.Expired = false;
                            quotationLinkHistory.QuotationId = quotation.Id;
                            quotationLinkHistory.Token = token1;
                            await _quotationLinkHistoryRepository.InsertAsync(quotationLinkHistory);

                            var htmlbody = Convert.ToString(input.Body).Replace("#linkurl", smsBody);
                            input.Body = htmlbody;
                            //input.Body.ToString().Replace("#linkurl", smsBody);
                        }
                        
                            request.AddHeader("content-type", "application/x-www-form-urlencoded");
                            request.AddHeader("Authorization", SmsConfiguration.SMS_AuthorizationKey); //"SyMTYj4X9&8CA*P6s1iDl^75"
                            request.AddParameter("client_id", SmsConfiguration.Client_Id); //"Csprl_DP2AEBO9FQN4UKT"
                            request.AddParameter("sender_id", SmsConfiguration.Sender_Id); //"APSOLR"
                            request.AddParameter("mobile", input.Mobile);
                            request.AddParameter("message", input.Body);//"Dear [Pravin test], Greetings from Australian Premium Solar (India) Pvt. Ltd. Your application for solar rooftop system is successfully registered with us. Thank you for choosing APS. Feel free to call on [07966006600] for any query. Regards, Team APS");
                            request.AddParameter("msgtype", "txn");  //"txn"
                            request.AddParameter("temp_id", input.SmsConfig_TempId);
                            //request.AddParameter("unicode", "0");
                            //request.AddParameter("flash", "0");
                            //request.AddParameter("campaign_name", "My campaign 1001");
                            var responsecustomer = await client.ExecuteAsync(request);
                            if (Convert.ToString(responsecustomer.StatusCode).ToLower() != "ok")
                            {
                                throw new UserFriendlyException(L(responsecustomer.Content));
                            }
                        }
                        else
                        {
                            throw new UserFriendlyException(L("PleaseEnterSMSProviderConfiguration"));
                        }

                        if (input.SMSTemplateId == 0)
                        {
                            input.SMSTemplateId = null;
                        }
                        var SmsActivityLogs = ObjectMapper.Map<SmsLeadActivityLog>(input);
                        SmsActivityLogs.LeadActivityLogId = activity.Id;
                        SmsActivityLogs.SmsTemplateId = input.SMSTemplateId;
                        SmsActivityLogs.SmsActivityBody = input.Body;
                        await _smsleadRepository.InsertAndGetIdAsync(SmsActivityLogs);
                    }

                }
                else
                {
                    throw new UserFriendlyException(L("PleaseSelectLeadorOrganization"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<LeadtrackerHistoryDto>> GetLeadActivityLogHistory(GetActivityLogInput input)
        {
            try
            {
                var Result = (from item in _LeadtrackerHistoryRepository.GetAll()
                              join ur in _userRepository.GetAll() on item.CreatorUserId equals ur.Id into urjoined
                              from ur in urjoined.DefaultIfEmpty()
                              where (item.LeadActionId == input.LeadId)
                              select new LeadtrackerHistoryDto()
                              {
                                  Id = item.Id,
                                  FieldName = item.FieldName,
                                  PrevValue = item.PrevValue,
                                  CurValue = item.CurValue,
                                  Lastmodifiedbyuser = ur.Name + " " + ur.Surname
                              })
                    .OrderByDescending(e => e.Id).ToList();
                return Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task CreateOrEdit(CreateOrEditLeadsActivityLogDto input)
        {

            //var client = new RestClient("https://insprl.com/api/sms/send-msg");
            //var request = new RestRequest(Method.Post);
            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            //request.AddHeader("Authorization", "{CLIENT_KEY}");
            //request.AddParameter("clientid", "{CLIENT_ID}");
            //request.AddParameter("sender_id", "{6_CHAR_SENDER_ID}");
            //request.AddParameter("mobile", "9999999999");
            ////request.AddParameter(message", "Hi Customer, This is an alert Message");
            ////request.AddParameter(schedule_date", "03 - 02 - 2020");
            ////request.AddParameter(schedule_time", "13:20");
            ////request.AddParameter(unicode", "0");
            ////request.AddParameter(flash", "0");
            //request.AddParameter("campaign_name", "My campaign 2020 Jan");
            //IRestResponse response = client.Execute(request);

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(CreateOrEditLeadsActivityLogDto input)
        {
            var LeadActivityLogs = ObjectMapper.Map<LeadActivityLogs>(input);

            if (AbpSession.TenantId != null)
            {
                LeadActivityLogs.TenantId = (int?)AbpSession.TenantId;
            }

            var activityId = await _leadActivityLogRepository.InsertAndGetIdAsync(LeadActivityLogs);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 16; // City Pages Name
            dataVaulteActivityLog.ActionId = 1; // Created
            dataVaulteActivityLog.ActionNote = "Lead Activity Created";
            dataVaulteActivityLog.SectionValueId = activityId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        // [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_City_Edit)]
        protected virtual async Task Update(CreateOrEditLeadsActivityLogDto input)
        {
            var leadActivitylogs = await _leadActivityLogRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 16; // City Pages Name
            dataVaulteActivityLog.ActionId = 2; // Updated
            dataVaulteActivityLog.ActionNote = "Lead Activity Logs Updated";
            dataVaulteActivityLog.SectionValueId = input.Id;
            var LeadActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);


            //await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            //await _dbContextProvider.GetDbContext().SaveChangesAsync();


            ObjectMapper.Map(input, leadActivitylogs);
        }


        public async Task Delete(EntityDto input)
        {
            await _leadActivityLogRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 16; // city Pages Name
            dataVaulteActivityLog.ActionId = 3; // Deleted
            dataVaulteActivityLog.ActionNote = "Lead Activity Log Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
