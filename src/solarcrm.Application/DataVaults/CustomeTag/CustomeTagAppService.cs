﻿
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.CustomeTag.Exporting;
using solarcrm.DataVaults.CustomeTag.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.CustomeTag
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CustomeTag)]
    public class CustomeTagAppService : solarcrmAppServiceBase, ICustomeTagAppService
    {
        private readonly IRepository<CustomeTag> _customeTagRepository;
        private readonly ICustomeTagExcelExporter _customeTagExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public CustomeTagAppService(IRepository<CustomeTag> CustomeTagRepository, ICustomeTagExcelExporter customeTagExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _customeTagRepository = CustomeTagRepository;
            _customeTagExcelExporter = customeTagExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
                    
        public async Task<PagedResultDto<GetCustomeTagForViewDto>> GetAll(GetAllCustomeTagInput input)
        {
            var filteredCustomeTag = _customeTagRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.TagTableName.Contains(input.Filter));

            var pagedAndFilteredCustomeTag = filteredCustomeTag.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var customeTag = from o in pagedAndFilteredCustomeTag
                             select new GetCustomeTagForViewDto()
                                {
                                    CustomTags = new CustomeTagDto
                                    {
                                        TagTableName = o.TagTableName,
                                        TagTableFieldName = o.TagTableFieldName,
                                        IsActive = o.IsActive,
                                        Id = o.Id,
                                        CreatedDate = o.CreationTime,
                                    }
                                };

            var totalCount = await filteredCustomeTag.CountAsync();

            return new PagedResultDto<GetCustomeTagForViewDto>(totalCount, await customeTag.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_Edit)]
        public async Task<GetCustomeTagForEditOutput> GetCustomeTagForEdit(EntityDto input)
        {
            var custometagresult = await _customeTagRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCustomeTagForEditOutput { CustomeTag = ObjectMapper.Map<CustomeTagDto>(custometagresult) };

            return output;
        }
        public async Task<GetCustomeTagForViewDto> GetCustomeTagForView(int id)
        {
            var custometagresult = await _customeTagRepository.GetAsync(id);

            var output = new GetCustomeTagForViewDto { CustomTags = ObjectMapper.Map<CustomeTagDto>(custometagresult) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_ExportToExcel)]
        public async Task<FileDto> GetCustomeTagToExcel(GetAllCustomeTagForExcelInput input)
        {
            var filteredcustometag = _customeTagRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TagTableName.Contains(input.Filter));

            var query = (from o in filteredcustometag
                         select new GetCustomeTagForViewDto()
                         {
                             CustomTags = new CustomeTagDto
                             {
                                 TagTableName = o.TagTableName,
                                 TagTableFieldName = o.TagTableFieldName,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var custometagListDtos = await query.ToListAsync();

            return _customeTagExcelExporter.ExportToFile(custometagListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditCustomeTagDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_Create)]
        protected virtual async Task Create(CreateOrEditCustomeTagDto input)
        {
            try
            {
                var customeTagresult = ObjectMapper.Map<CustomeTag>(input);

                if (AbpSession.TenantId != null)
                {
                    customeTagresult.TenantId = (int?)AbpSession.TenantId;
                }

                var customeTagId = await _customeTagRepository.InsertAndGetIdAsync(customeTagresult);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 37; // Custome tag Pages Name
                dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
                dataVaulteActivityLog.ActionNote = "CustomeTag Created Name :- " + customeTagresult.TagTableName;
                dataVaulteActivityLog.SectionValueId = customeTagId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_Edit)]
        protected virtual async Task Update(CreateOrEditCustomeTagDto input)
        {
            var customeTagResult = await _customeTagRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 37; // CancelReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "CustomeTag Updated Name :- " + customeTagResult.TagTableName;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var customeTagActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (customeTagResult.TagTableName != input.TagTableName)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "TableName";
                dataVaultsHistory.PrevValue = customeTagResult.TagTableName;
                dataVaultsHistory.CurValue = input.TagTableName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = customeTagActivityLogId;
                dataVaultsHistory.SectionId = 37; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (customeTagResult.TagTableFieldName != input.TagTableFieldName)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "TableFieldName";
                dataVaultsHistory.PrevValue = customeTagResult.TagTableFieldName;
                dataVaultsHistory.CurValue = input.TagTableFieldName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = customeTagActivityLogId;
                dataVaultsHistory.SectionId = 37; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (customeTagResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = customeTagResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = customeTagActivityLogId;
                dataVaultsHistory.SectionId = 37;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, customeTagResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_Delete)]
        public async Task Delete(EntityDto input)
        {
            var customeTagResult = await _customeTagRepository.FirstOrDefaultAsync(input.Id);
            await _customeTagRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 37; // customeTag Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "CustomeTag Deleted Name :- " + customeTagResult.TagTableName;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
