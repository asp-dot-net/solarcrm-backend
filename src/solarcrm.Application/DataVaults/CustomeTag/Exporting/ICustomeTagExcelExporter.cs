﻿using solarcrm.DataVaults.CustomeTag.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.CustomeTag.Exporting
{
    public interface ICustomeTagExcelExporter
    {
        FileDto ExportToFile(List<GetCustomeTagForViewDto> customeTag);
    }
}
