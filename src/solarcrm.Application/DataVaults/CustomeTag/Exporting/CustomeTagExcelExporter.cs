﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.CustomeTag.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.CustomeTag.Exporting
{
    public class CustomeTagExcelExporter : NpoiExcelExporterBase, ICustomeTagExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CustomeTagExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCustomeTagForViewDto> leadType)
        {
            return CreateExcelPackage(
                "CustomeTag.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CustomeTag"));

                    AddHeader(
                        sheet,
                        L("TableName"),
                        L("Field")
                        );

                    AddObjects(
                        sheet, leadType,
                        _ => _.CustomTags.TagTableName,
                        _ => _.CustomTags.TagTableFieldName
                        );
                });
        }
    }
}
