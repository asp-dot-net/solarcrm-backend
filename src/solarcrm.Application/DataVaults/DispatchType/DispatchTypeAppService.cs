﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using solarcrm.DataVaults.DispatchType.Exporting;
using Abp.Authorization;
using solarcrm.Authorization;
using Abp.Application.Services.Dto;
using solarcrm.Common;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Dto;
using Abp.Collections.Extensions;
using solarcrm.DataVaults.DispatchType.Dto;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
namespace solarcrm.DataVaults.DispatchType
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DispatchType)]
    public class DispatchTypeAppService : solarcrmAppServiceBase, IDispatchTypeAppService
    {
        private readonly IRepository<DispatchType> _dispatchTypeRepository;
        private readonly IDispatchTypeExcelExporter _dispatchTypeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public DispatchTypeAppService(IRepository<DispatchType> dispatchTypeRepository, IDispatchTypeExcelExporter dispatchTypeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _dispatchTypeRepository = dispatchTypeRepository;
            _dispatchTypeExcelExporter = dispatchTypeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
        public async Task<PagedResultDto<GetDispatchTypeForViewDto>> GetAll(GetAllDispatchTypeInput input)
        {
            var filteredDispatchType = _dispatchTypeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredDiscom = filteredDispatchType.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var DispatchType = from o in pagedAndFilteredDiscom
                         select new GetDispatchTypeForViewDto()
                         {
                             DispatchType = new DispatchTypeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime
                             }
                         };

            var totalCount = await filteredDispatchType.CountAsync();

            return new PagedResultDto<GetDispatchTypeForViewDto>(totalCount, await DispatchType.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DispatchType_Edit)]
        public async Task<GetDispatchTypeForEditOutput> GetDispatchTypeForEdit(EntityDto input)
        {
            var DispatchType = await _dispatchTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDispatchTypeForEditOutput { DispatchType = ObjectMapper.Map<CreateOrEditDispatchTypeDto>(DispatchType) };

            return output;
        }

        //public async Task<GetDiscomForViewDto> GetDiscomForView(int id)
        //{
        //    var discom = await _discomRepository.GetAsync(id);

        //    var output = new GetDiscomForViewDto { Discom = ObjectMapper.Map<DiscomDto>(discom) };

        //    return output;
        //}

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DispatchType_ExportToExcel)]
        public async Task<FileDto> GetDispatchTypeToExcel(GetAllDispatchTypeForExcelInput input)
        {
            var filteredDispatch = _dispatchTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredDispatch
                         select new GetDispatchTypeForViewDto()
                         {
                             DispatchType = new DispatchTypeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var DispatchTypeListDtos = await query.ToListAsync();

            return _dispatchTypeExcelExporter.ExportToFile(DispatchTypeListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditDispatchTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DispatchType_Create)]
        protected virtual async Task Create(CreateOrEditDispatchTypeDto input)
        {
            var DispatchType = ObjectMapper.Map<DispatchType>(input);

            if (AbpSession.TenantId != null)
            {
                DispatchType.TenantId = (int?)AbpSession.TenantId;
            }

            var dispatchTypeId = await _dispatchTypeRepository.InsertAndGetIdAsync(DispatchType);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 57; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "DispatchType Created Name :- " + input.Name;
            dataVaulteActivityLog.SectionValueId = dispatchTypeId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DispatchType_Edit)]
        protected virtual async Task Update(CreateOrEditDispatchTypeDto input)
        {
            var dispatchTypeResult = await _dispatchTypeRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            int SectionId = 57; // Discom Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "DispatchType Updated Name :-" + dispatchTypeResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var DocumentListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (dispatchTypeResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = dispatchTypeResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Updated
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (dispatchTypeResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = dispatchTypeResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Updated
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }


            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, dispatchTypeResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DispatchType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var dispatchTypeResult = await _dispatchTypeRepository.FirstOrDefaultAsync(input.Id);
            await _dispatchTypeRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 57; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Created
            dataVaulteActivityLog.ActionNote = "DispatchType Deleted Name :- " + dispatchTypeResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<List<GetAllDispatchTypeForDropdown>> GetAllDispatchTypeForDropdown()
        {
            var allDispatchType = _dispatchTypeRepository.GetAll().Where(x => x.IsActive == true);

            var DispatchType = from o in allDispatchType
                               select new GetAllDispatchTypeForDropdown()
                         {
                             Name = o.Name,
                             Id = o.Id
                         };

            return new List<GetAllDispatchTypeForDropdown>(await DispatchType.ToListAsync());
        }
    }
}
