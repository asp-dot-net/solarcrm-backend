﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Discom.Dto;
using solarcrm.DataVaults.Discom.Exporting;
using solarcrm.DataVaults.DispatchType.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DispatchType.Exporting
{
    public class DispatchTypeExcelExporter : NpoiExcelExporterBase, IDispatchTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DispatchTypeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDispatchTypeForViewDto> dispatchType)
        {
            return CreateExcelPackage(
                "DispatchType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("DispatchType"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, dispatchType,
                        _ => _.DispatchType.Name
                        );
                });
        }
    }
}
