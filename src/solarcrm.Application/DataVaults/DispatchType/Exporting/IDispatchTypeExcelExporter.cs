﻿using solarcrm.DataVaults.Discom.Dto;
using solarcrm.DataVaults.DispatchType.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DispatchType.Exporting
{
    public interface IDispatchTypeExcelExporter
    {
        FileDto ExportToFile(List<GetDispatchTypeForViewDto> dispatchType);
    }
}
