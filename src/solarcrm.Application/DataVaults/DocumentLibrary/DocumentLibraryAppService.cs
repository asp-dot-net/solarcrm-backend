﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using AutoMapper;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.DocumentLibrary.Dto;
using solarcrm.EntityFrameworkCore;
using solarcrm.MultiTenancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solarcrm.Dto;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Common;
using solarcrm.DataVaults.DocumentLibrary.Exporting;
using solarcrm.DataVaults.InvoiceType.Exporting;
using solarcrm.DataVaults.InvoiceType.Dto;

namespace solarcrm.DataVaults.DocumentLibrary
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary)]
    public class DocumentLibraryAppService : solarcrmAppServiceBase, IDocumentLibraryAppService
    {
        private readonly IRepository<DocumentLibrary> _documentlibraryRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IDocumentLibraryExcelExporter _documentLibraryExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        public DocumentLibraryAppService(IRepository<DocumentLibrary> documenttypeRepository, IDocumentLibraryExcelExporter DocumentLibraryExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<Tenant> tenantRepository)
        {
            _documentlibraryRepository = documenttypeRepository;
            _documentLibraryExcelExporter = DocumentLibraryExcelExporter;
            _tenantRepository = tenantRepository;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
       
       
        public async Task<PagedResultDto<GetDocumentLibraryForViewDto>> GetAll(GetAllDocumentLibraryInput input)
        {

            var filteredDepartments = _documentlibraryRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Title.Contains(input.Filter));

            var pagedAndFilteredDepartments = filteredDepartments
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var departments = from o in pagedAndFilteredDepartments
                              select new GetDocumentLibraryForViewDto()
                              {
                                  documentLibrary = new DocumentLibraryDto
                                  {
                                      Title = o.Title,
                                      Id = o.Id,
                                      FilePath = o.FilePath,
                                      FileName = o.FileName
                                  }
                              };

            var totalCount = await filteredDepartments.CountAsync();

            return new PagedResultDto<GetDocumentLibraryForViewDto>(
                totalCount,
                await departments.ToListAsync()
            );
        }

        public async Task<GetDocumentLibraryForViewDto> GetDocumentLibraryForView(int id)
        {
            var document = await _documentlibraryRepository.GetAsync(id);

            var output = new GetDocumentLibraryForViewDto { documentLibrary = ObjectMapper.Map<DocumentLibraryDto>(document) };

            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_ExportToExcel)]
        public async Task<FileDto> GetDocumentLibraryToExcel(GetAllDocumentLibraryForExcelInput input)
        {
            var filtereddocumentLibrary = _documentlibraryRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.FileName.Contains(input.Filter));

            var query = (from o in filtereddocumentLibrary
                         select new GetDocumentLibraryForViewDto()
                         {
                             documentLibrary = new DocumentLibraryDto
                             {
                                 Title = o.Title,
                                 FileName = o.FileName,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var DocumentLibraryListDtos = await query.ToListAsync();

            return _documentLibraryExcelExporter.ExportToFile(DocumentLibraryListDtos);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_Edit)]
        public async Task<GetDocumentLibraryForEditOutput> GetDocumentLibraryForEdit(EntityDto input)
        {
            try
            {
                var DocumentlibraryDto = await _documentlibraryRepository.FirstOrDefaultAsync(input.Id);

                var output = new GetDocumentLibraryForEditOutput { documentLibrary = ObjectMapper.Map<CreateOrEditDocumentLibraryDto>(DocumentlibraryDto) };

                return output;
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }

        public async Task CreateOrEdit(CreateOrEditDocumentLibraryDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_Create)]
        protected virtual async Task Create(CreateOrEditDocumentLibraryDto input)
        {

            var documenttype = ObjectMapper.Map<DocumentLibrary>(input);

            if (AbpSession.TenantId != null)
            {
                documenttype.TenantId = (int)AbpSession.TenantId;
            }
            var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
            documenttype.FilePath = "\\Documents" + "\\" + TenantName + "\\" + "Documents" + "\\" + "DocumentLibrary" + "\\" + input.FileName;
            //await _documentlibraryRepository.InsertAsync(documenttype);
            var cancelReasonsId = await _documentlibraryRepository.InsertAndGetIdAsync(documenttype);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 22; // CancelReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Document library Created Name :-" + input.FileName;
            dataVaulteActivityLog.SectionValueId = cancelReasonsId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_Edit)]
        protected virtual async Task Update(CreateOrEditDocumentLibraryDto input)
        {
            var documenttype = await _documentlibraryRepository.FirstOrDefaultAsync((int)input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 22; // CancelReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Document library Updated Name :-" + input.FileName;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var cancelReasonsActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (documenttype.Title != input.Title)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = documenttype.Title;
                dataVaultsHistory.CurValue = input.Title;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 22; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (documenttype.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = documenttype.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 22;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();
            #endregion
            ObjectMapper.Map(input, documenttype);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_Delete)]
        public async Task Delete(EntityDto input)
        {
            var documentResult = await _documentlibraryRepository.FirstOrDefaultAsync(input.Id);
            await _documentlibraryRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 22; // Document library Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Document library Deleted Name :-" + documentResult.FileName;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}