﻿using solarcrm.DataVaults.Division.Dto;
using solarcrm.DataVaults.DocumentLibrary.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DocumentLibrary.Exporting
{
    public interface IDocumentLibraryExcelExporter
    {
        FileDto ExportToFile(List<GetDocumentLibraryForViewDto> documentLibrary);
    }
}
