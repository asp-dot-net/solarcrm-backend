﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Division.Dto;
using solarcrm.DataVaults.Division.Exporting;
using solarcrm.DataVaults.DocumentLibrary.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DocumentLibrary.Exporting
{
    public class DocumentLibraryExcelExporter : NpoiExcelExporterBase, IDocumentLibraryExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DocumentLibraryExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetDocumentLibraryForViewDto> documentLibrary)
        {
            return CreateExcelPackage(
                "DocumentLibrary.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("DocumentLibrary"));

                    AddHeader(
                        sheet,
                        L("Title"),
                        L("FileName")
                        );

                    AddObjects(
                        sheet, documentLibrary,
                        _ => _.documentLibrary.Title,
                        _ => _.documentLibrary.FileName
                        );
                });
        }
    }
}
