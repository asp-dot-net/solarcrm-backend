﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.District.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.District.Exporting
{
    public class DistrictExcelExporter : NpoiExcelExporterBase, IDistrictExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DistrictExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDistrictForViewDto> district)
        {
            return CreateExcelPackage(
                "District.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("District"));

                    AddHeader(
                        sheet,
                        L("StateName"),
                        L("Name")
                        );
                   

                    AddObjects(
                        sheet, district,
                        _ => _.Districts.StateName,
                        _ => _.Districts.Name
                        );
                });
        }
    }
}
