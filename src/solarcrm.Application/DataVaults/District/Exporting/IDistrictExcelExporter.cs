﻿using solarcrm.DataVaults.District.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.District.Exporting
{
    public interface IDistrictExcelExporter
    {
        FileDto ExportToFile(List<GetDistrictForViewDto> district);
    }
}
