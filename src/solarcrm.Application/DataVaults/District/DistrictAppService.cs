﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.District.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.District.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Common;
using solarcrm.MobileService.MCommon.Dto;

namespace solarcrm.DataVaults.District
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_District)]
    public class DistrictAppService : solarcrmAppServiceBase, IDistrictAppService
    {
        private readonly IRepository<District> _districtRepository;
        private readonly IDistrictExcelExporter _districtExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<State.State> _stateRepository;

        public DistrictAppService(IRepository<District> DistrictRepository, IDistrictExcelExporter DistrictExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<State.State> stateRepository)
        {
            _districtRepository = DistrictRepository;
            _districtExcelExporter = DistrictExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _stateRepository = stateRepository;
        }


        public async Task<PagedResultDto<GetDistrictForViewDto>> GetAll(GetAllDistrictInput input)
        {
            var filteredDistrict = _districtRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredDistrict = filteredDistrict.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var districts = from o in pagedAndFilteredDistrict
                            select new GetDistrictForViewDto()
                            {
                                Districts = new DistrictDto
                                {
                                    Name = o.Name,
                                    StateName = o.StateFk.Name,
                                    IsActive = o.IsActive,
                                    Id = o.Id,
                                    CreatedDate = o.CreationTime
                                }
                            };

            var totalCount = await filteredDistrict.CountAsync();

            return new PagedResultDto<GetDistrictForViewDto>(totalCount, await districts.ToListAsync());
        }

        public async Task CreateOrEdit(CreateOrEditDistrictDto input)
        {

            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
       

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_District_Edit)]
        public async Task<GetDistrictForEditOutput> GetDistrictForEdit(EntityDto input)
        {
            var districts = await _districtRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDistrictForEditOutput { districts = ObjectMapper.Map<CreateOrEditDistrictDto>(districts) };

            return output;
        }

        public async Task<GetDistrictForViewDto> GetDistrictForView(int id)
        {
            var districts = await _districtRepository.GetAsync(id);

            var output = new GetDistrictForViewDto { Districts = ObjectMapper.Map<DistrictDto>(districts) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_District_ExportToExcel)]
        public async Task<FileDto> GetDistrictToExcel(GetAllDistrictForExcelInput input)
        {
            var filteredDistrict = _districtRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredDistrict
                         select new GetDistrictForViewDto()
                         {
                             Districts = new DistrictDto
                             {
                                 Name = o.Name,
                                 StateName = o.StateFk.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var districtListDtos = await query.ToListAsync();

            return _districtExcelExporter.ExportToFile(districtListDtos);
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_District_Create)]
        protected virtual async Task Create(CreateOrEditDistrictDto input)
        {
            var district = ObjectMapper.Map<District>(input);

            if (AbpSession.TenantId != null)
            {
                district.TenantId = (int?)AbpSession.TenantId;
            }

            var districtResult = await _districtRepository.InsertAndGetIdAsync(district);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 14; // District Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "District Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = districtResult;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_District_Edit)]
        protected virtual async Task Update(CreateOrEditDistrictDto input)
        {
            var districtResult = await _districtRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 14; // District Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "District Updated Name :-" + districtResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var districtActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (districtResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = districtResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = districtActivityLogId;
                dataVaultsHistory.SectionId = 14; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (districtResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = districtResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = districtActivityLogId;
                dataVaultsHistory.SectionId = 14;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (districtResult.StateId != input.StateId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "State";
                dataVaultsHistory.PrevValue = _stateRepository.GetAll().Where(e => e.Id == districtResult.StateId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _stateRepository.GetAll().Where(e => e.Id == input.StateId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = districtActivityLogId;
                dataVaultsHistory.SectionId = 14;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, districtResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_District_Delete)]
        public async Task Delete(EntityDto input)
        {
            var districtResult = await _districtRepository.FirstOrDefaultAsync(input.Id);
            await _districtRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 14; // state Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Created
            dataVaulteActivityLog.ActionNote = "Disctrict Deleted Name :-" + districtResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
        public async Task<List<DistrictLookupTableDto>> GetAllDistrictForDropdown()
        {
            var allDistrict = _districtRepository.GetAll().Where(x => x.IsActive == true);

            var district = from o in allDistrict
                           select new DistrictLookupTableDto()
                           {
                               DisplayName = o.Name,
                               Id = o.Id
                           };

            return new List<DistrictLookupTableDto>(await district.ToListAsync());
        }
    }
}
