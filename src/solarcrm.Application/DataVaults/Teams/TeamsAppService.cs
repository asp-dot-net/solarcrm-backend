﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.Teams;
using solarcrm.DataVaults.Teams.Dto;
using solarcrm.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Dto;
using solarcrm.DataVaults.Teams.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.Teams
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Teams)]
    public class TeamsAppService : solarcrmAppServiceBase, ITeamsAppService
    {
        private readonly IRepository<Teams> _teamsRepository;
        private readonly ITeamsExcelExporter _teamsExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public TeamsAppService(IRepository<Teams> teamsListRepository, ITeamsExcelExporter teamsExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _teamsRepository = teamsListRepository;
            _teamsExcelExporter = teamsExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
               
        public async Task<PagedResultDto<GetTeamsForViewDto>> GetAll(GetAllTeamsInput input)
        {
            var filteredTeams = _teamsRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredTeams = filteredTeams.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var Teams = from o in pagedAndFilteredTeams
                        select new GetTeamsForViewDto()
                        {
                            Teams = new TeamsDto
                            {
                                Name = o.Name,
                                IsActive = o.IsActive,
                                Id = o.Id,
                                CreatedDate = o.CreationTime
                            }
                        };

            var totalCount = await filteredTeams.CountAsync();

            return new PagedResultDto<GetTeamsForViewDto>(totalCount, await Teams.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Teams_Edit)]
        public async Task<GetTeamsForEditOutput> GetTeamsForEdit(EntityDto input)
        {
            var Teams = await _teamsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTeamsForEditOutput { Teams = ObjectMapper.Map<CreateOrEditTeamsDto>(Teams) };

            return output;
        }

        public async Task<GetTeamsForViewDto> GetTeamsForView(int id)
        {
            var Teams = await _teamsRepository.GetAsync(id);

            var output = new GetTeamsForViewDto { Teams = ObjectMapper.Map<TeamsDto>(Teams) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Teams_ExportToExcel)]
        public async Task<FileDto> GetTeamsToExcel(GetAllTeamsForExcelInput input)
        {
            var filteredTeams = _teamsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredTeams
                         select new GetTeamsForViewDto()
                         {
                             Teams = new TeamsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var TeamssListDtos = await query.ToListAsync();

            return _teamsExcelExporter.ExportToFile(TeamssListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditTeamsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Teams_Create)]
        protected virtual async Task Create(CreateOrEditTeamsDto input)
        {
            var Teams = ObjectMapper.Map<Teams>(input);

            if (AbpSession.TenantId != null)
            {
                Teams.TenantId = (int?)AbpSession.TenantId;
            }

            var TeamsId = await _teamsRepository.InsertAndGetIdAsync(Teams);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 4; // Teams Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Team Created Name :-" +input.Name ;
            dataVaulteActivityLog.SectionValueId = TeamsId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Teams_Edit)]
        protected virtual async Task Update(CreateOrEditTeamsDto input)
        {
            var Teams = await _teamsRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 4; // Teams Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Team Updated Name :-" +Teams.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var TeamsActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (Teams.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = Teams.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = TeamsActivityLogId;
                dataVaultsHistory.SectionId = 4; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (Teams.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = Teams.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = TeamsActivityLogId;
                dataVaultsHistory.SectionId = 4;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, Teams);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Teams_Delete)]
        public async Task Delete(EntityDto input)
        {
            var teamsResult = await _teamsRepository.FirstOrDefaultAsync(input.Id);
            await _teamsRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 4; // Teams Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Teams Deleted Name :-" + teamsResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
