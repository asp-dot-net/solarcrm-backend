﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Teams.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Teams.Exporting
{
    public class TeamsExcelExporter : NpoiExcelExporterBase, ITeamsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TeamsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTeamsForViewDto> teams)
        {
            return CreateExcelPackage(
                "Teams.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Teams"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, leadSource,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, teams,
                        _ => _.Teams.Name
                        );
                });
        }
    }
}
