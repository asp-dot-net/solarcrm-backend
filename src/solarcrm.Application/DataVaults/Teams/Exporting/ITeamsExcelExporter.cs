﻿using solarcrm.DataVaults.Teams.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Teams.Exporting
{
    public interface ITeamsExcelExporter
    {
        FileDto ExportToFile(List<GetTeamsForViewDto> teams);
    }
}
