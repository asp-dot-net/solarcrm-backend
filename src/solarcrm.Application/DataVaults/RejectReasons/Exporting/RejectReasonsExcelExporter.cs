﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.HoldReasons.Exporting;
using solarcrm.DataVaults.RejectReasons.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.RejectReasons.Exporting
{
    public class RejectReasonsExcelExporter : NpoiExcelExporterBase, IRejectReasonsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RejectReasonsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRejectReasonsForViewDto> leadType)
        {
            return CreateExcelPackage(
                "RejectReasons.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("RejectReasons"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, leadType,
                        _ => _.rejectreasons.Name
                        );
                });
        }
    }
}
