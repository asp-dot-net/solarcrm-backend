﻿using solarcrm.DataVaults.HoldReasons.Dto;
using solarcrm.DataVaults.RejectReasons.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.RejectReasons.Exporting
{
    public interface IRejectReasonsExcelExporter
    {
        FileDto ExportToFile(List<GetRejectReasonsForViewDto> holdReasons);
    }
}
