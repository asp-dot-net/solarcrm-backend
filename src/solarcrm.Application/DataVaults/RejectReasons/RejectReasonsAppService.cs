﻿
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.RejectReasons.Exporting;
using solarcrm.DataVaults.RejectReasons.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.RejectReasons
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RejectReasons)]
    public class RejectReasonsAppService : solarcrmAppServiceBase, IRejectReasonsAppService
    {
        private readonly IRepository<RejectReasons> _rejectReasonsRepository;
        private readonly IRejectReasonsExcelExporter _rejectReasonsExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public RejectReasonsAppService(IRepository<RejectReasons> RejectReasonsRepository, IRejectReasonsExcelExporter RejectReasonsExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _rejectReasonsRepository = RejectReasonsRepository;
            _rejectReasonsExcelExporter = RejectReasonsExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

          
        public async Task<PagedResultDto<GetRejectReasonsForViewDto>> GetAll(GetAllRejectReasonsInput input)
        {
            var filteredRejectReasons = _rejectReasonsRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredRejectReasons = filteredRejectReasons.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var rejectReasons = from o in pagedAndFilteredRejectReasons
                                select new GetRejectReasonsForViewDto()
                         {
                             rejectreasons = new RejectReasonsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime,

                             }
                         };

            var totalCount = await filteredRejectReasons.CountAsync();

            return new PagedResultDto<GetRejectReasonsForViewDto>(totalCount, await rejectReasons.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_Edit)]
        public async Task<GetRejectReasonsForEditOutput> GetRejectReasonsForEdit(EntityDto input)
        {
            var rejectReasons = await _rejectReasonsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetRejectReasonsForEditOutput { rejectreasons = ObjectMapper.Map<CreateOrEditRejectReasonsDto>(rejectReasons) };

            return output;
        }
        public async Task<GetRejectReasonsForViewDto> GetRejectReasonsForView(int id)
        {
            var rejectReasons = await _rejectReasonsRepository.GetAsync(id);

            var output = new GetRejectReasonsForViewDto { rejectreasons = ObjectMapper.Map<RejectReasonsDto>(rejectReasons) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_ExportToExcel)]
        public async Task<FileDto> GetRejectReasonsToExcel(GetAllRejectReasonsForExcelInput input)
        {
            var filteredRejectReasons = _rejectReasonsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredRejectReasons
                         select new GetRejectReasonsForViewDto()
                         {
                             rejectreasons = new RejectReasonsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var rejectReasonsListDtos = await query.ToListAsync();

            return _rejectReasonsExcelExporter.ExportToFile(rejectReasonsListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditRejectReasonsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_Create)]
        protected virtual async Task Create(CreateOrEditRejectReasonsDto input)
        {
            var rejectReasons = ObjectMapper.Map<RejectReasons>(input);

            if (AbpSession.TenantId != null)
            {
                rejectReasons.TenantId = (int?)AbpSession.TenantId;
            }

            var rejectReasonsId = await _rejectReasonsRepository.InsertAndGetIdAsync(rejectReasons);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 60; // RejectReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Reject Reason Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = rejectReasonsId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_Edit)]
        protected virtual async Task Update(CreateOrEditRejectReasonsDto input)
        {
            var rejectReasons = await _rejectReasonsRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 60; // RejectReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Reject Reason Updated Name :-" + rejectReasons.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var rejectReasonsActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (rejectReasons.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = rejectReasons.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = rejectReasonsActivityLogId;
                dataVaultsHistory.SectionId = 60; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (rejectReasons.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = rejectReasons.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = rejectReasonsActivityLogId;
                dataVaultsHistory.SectionId = 60;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, rejectReasons);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_Delete)]
        public async Task Delete(EntityDto input)
        {
            var rejectReasonResult = await _rejectReasonsRepository.FirstOrDefaultAsync(input.Id);
            await _rejectReasonsRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 60; // RejectReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Reject Reasons  Deleted Name :-" + rejectReasonResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
