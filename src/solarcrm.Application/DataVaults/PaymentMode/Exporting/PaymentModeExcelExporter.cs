﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;

using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PaymentMode.Exporting
{
    public class PaymentModeExcelExporter : NpoiExcelExporterBase, IPaymentModeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentModeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetPaymentModeForViewDto> paymode)
        {
            return CreateExcelPackage(
                "PaymentMode.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("PaymentMode");

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, paymode,
                        _ => _.paymentMode.Name
                        );
                });
        }
    }
}
