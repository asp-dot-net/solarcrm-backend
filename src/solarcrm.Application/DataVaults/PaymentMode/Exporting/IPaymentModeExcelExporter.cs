﻿
using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.DataVaults.PaymentType.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PaymentMode.Exporting
{
    public interface IPaymentModeExcelExporter
    {
        FileDto ExportToFile(List<GetPaymentModeForViewDto> paymentMode);
    }
}
