﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.Dto;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.PaymentMode.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.PaymentMode
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentMode)]
    public class PaymentModeAppService : solarcrmAppServiceBase, IPaymentModeAppService
    {
        private readonly IRepository<PaymentMode> _paymentModeRepository;
        private readonly IPaymentModeExcelExporter _paymentModeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public PaymentModeAppService(IRepository<PaymentMode> paymentModeRepository, IPaymentModeExcelExporter paymentModeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _paymentModeRepository = paymentModeRepository;
            _paymentModeExcelExporter = paymentModeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetPaymentModeForViewDto>> GetAll(GetAllPaymentModeInput input)
        {
            var filteredpayBy = _paymentModeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredPaymentMode = filteredpayBy.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var paymentMode = from o in pagedAndFilteredPaymentMode
                              select new GetPaymentModeForViewDto()
                           {
                               paymentMode = new PaymentModeDto
                               {
                                   Name = o.Name,
                                   IsActive = o.IsActive,
                                   Id = o.Id,
                                   CreatedDate = o.CreationTime
                               }
                           };

            var totalCount = await filteredpayBy.CountAsync();

            return new PagedResultDto<GetPaymentModeForViewDto>(totalCount, await paymentMode.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_Edit)]
        public async Task<GetPaymentModeForEditOutput> GetPaymentModeForEdit(EntityDto input)
        {
            var paymentMode = await _paymentModeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPaymentModeForEditOutput { PayBy = ObjectMapper.Map<CreateOrEditPaymentModeDto>(paymentMode) };

            return output;
        }

        public async Task<GetPaymentModeForViewDto> GetPaymentModeForView(int id)
        {
            var paymentMode = await _paymentModeRepository.GetAsync(id);

            var output = new GetPaymentModeForViewDto { paymentMode = ObjectMapper.Map<PaymentModeDto>(paymentMode) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_Create)]
        public async Task<FileDto> GetPaymentModeToExcel(GetAllPaymentModeForExcelInput input)
        {
            var filteredpaymentMode = _paymentModeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredpaymentMode
                         select new GetPaymentModeForViewDto()
                         {
                             paymentMode = new PaymentModeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var paymentModesListDtos = await query.ToListAsync();

            return _paymentModeExcelExporter.ExportToFile(paymentModesListDtos);
        }


        public async Task CreateOrEdit(CreateOrEditPaymentModeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_Create)]
        protected virtual async Task Create(CreateOrEditPaymentModeDto input)
        {
            var paymentMode = ObjectMapper.Map<PaymentMode>(input);

            if (AbpSession.TenantId != null)
            {
                paymentMode.TenantId = (int?)AbpSession.TenantId;
            }

            var paymentModeId = await _paymentModeRepository.InsertAndGetIdAsync(paymentMode);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 48; // LeadType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "payment Mode Created Name :- " +input.Name;
            dataVaulteActivityLog.SectionValueId = paymentModeId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadType_Edit)]
        protected virtual async Task Update(CreateOrEditPaymentModeDto input)
        {
            var paymentMode = await _paymentModeRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 48; // LeadType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "payment Mode Updated Name :- " + input.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var leadTypeActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (paymentMode.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = paymentMode.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = leadTypeActivityLogId;
                dataVaultsHistory.SectionId = 48; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (paymentMode.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = paymentMode.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = leadTypeActivityLogId;
                dataVaultsHistory.SectionId = 48;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, paymentMode);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_Delete)]
        public async Task Delete(EntityDto input)
        {
            var paymentModeResult = await _paymentModeRepository.FirstOrDefaultAsync(input.Id);
            await _paymentModeRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 48; // Payment Mode Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Deleted
            dataVaulteActivityLog.ActionNote = "Payment Mode Deleted Name :- " + paymentModeResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
