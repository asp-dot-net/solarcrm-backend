﻿using solarcrm.DataVaults.Locations.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.Locations.Exporting
{
    public interface ILocationsExcelExporter
    {
        FileDto ExportToFile(List<GetLocationsForViewDto> locations);
    }   
}
