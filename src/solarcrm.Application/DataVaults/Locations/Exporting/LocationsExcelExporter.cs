﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Locations.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace solarcrm.DataVaults.Locations.Exporting
{
    public class LocationsExcelExporter : NpoiExcelExporterBase, ILocationsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LocationsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

       
        public FileDto ExportToFile(List<GetLocationsForViewDto> location)
        {
            return CreateExcelPackage(
                "Locations.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Locations"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                   

                    AddObjects(
                        sheet, location,
                        _ => _.Location.Name
                        );
                });
        }
    }
}
