﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.Locations.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.Locations.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using System.Collections.Generic;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Common;

namespace solarcrm.DataVaults.Locations
{
    public class LocationsAppService : solarcrmAppServiceBase, ILocationsAppService
    {
        private readonly IRepository<Locations> _locationRepository;
        private readonly ILocationsExcelExporter _locationExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        public LocationsAppService(IRepository<Locations> LocationRepository, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, ILocationsExcelExporter locationsExcelExporter, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _locationRepository = LocationRepository;
            _locationExcelExporter = locationsExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task CreateOrEdit(CreateOrEditLocationsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }

        }

        public async Task<PagedResultDto<GetLocationsForViewDto>> GetAll(GetAllLocationsInput input)
        {
            var filteredLocation = _locationRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredLeadSource = filteredLocation.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var location = from o in pagedAndFilteredLeadSource
                           select new GetLocationsForViewDto()
                           {
                               Location = new LocationsDto
                               {
                                   Name = o.Name,
                                   IsActive = o.IsActive,
                                   Id = o.Id
                               }
                           };

            var totalCount = await filteredLocation.CountAsync();

            return new PagedResultDto<GetLocationsForViewDto>(totalCount, await location.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Locations_Edit)]
        public async Task<GetLocationsForEditOutput> GetLocationForEdit(EntityDto input)
        {
            var location = await _locationRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLocationsForEditOutput { Location = ObjectMapper.Map<CreateOrEditLocationsDto>(location) };

            return output;
        }

        public async Task<GetLocationsForViewDto> GetLocationForView(int id)
        {
            var location = await _locationRepository.GetAsync(id);

            var output = new GetLocationsForViewDto { Location = ObjectMapper.Map<LocationsDto>(location) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Locations_ExportToExcel)]
        public async Task<FileDto> GetLocationToExcel(GetAllLocationsForExcelInput input)
        {
            var filteredLeadSources = _locationRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredLeadSources
                         select new GetLocationsForViewDto()
                         {
                             Location = new LocationsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var locationListDtos = await query.ToListAsync();

            return _locationExcelExporter.ExportToFile(locationListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Locations_Create)]
        protected virtual async Task Create(CreateOrEditLocationsDto input)
        {
            var location = ObjectMapper.Map<Locations>(input);

            if (AbpSession.TenantId != null)
            {
                location.TenantId = (int?)AbpSession.TenantId;
            }

            var locationId = await _locationRepository.InsertAndGetIdAsync(location);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 5; // Location Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created;  // Created
            dataVaulteActivityLog.ActionNote = "Location Created Name:- " + input.Name;
            dataVaulteActivityLog.SectionValueId = locationId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Locations_Edit)]
        protected virtual async Task Update(CreateOrEditLocationsDto input)
        {
            var location = await _locationRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 5; // Location Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated;
            dataVaulteActivityLog.ActionNote = "Location Updated Name :- " + location.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var locationActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (location.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = location.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Updated
                dataVaultsHistory.ActivityLogId = locationActivityLogId;
                dataVaultsHistory.SectionId = 5; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (location.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = location.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Updated
                dataVaultsHistory.ActivityLogId = locationActivityLogId;
                dataVaultsHistory.SectionId = 5;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion
            ObjectMapper.Map(input, location);
        }



        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Locations_Delete)]
        public async Task Delete(EntityDto input)
        {
            var locationresult = await _locationRepository.FirstOrDefaultAsync(input.Id);
             await _locationRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 5; // Location Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Deleted
            dataVaulteActivityLog.ActionNote =  "Location Deleted Name :- " + locationresult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
