﻿using solarcrm.DataVaults.Leads.Importing.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Leads.Importing
{
    public interface IInvalidLeadExporter
    {
        FileDto ExportToFile(List<ImportLeadDto> leadListDtos);
    }
}
