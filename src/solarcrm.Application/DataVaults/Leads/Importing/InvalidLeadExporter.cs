﻿using Abp.Dependency;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Leads.Importing.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Leads.Importing
{
    public class InvalidLeadExporter : NpoiExcelExporterBase, IInvalidLeadExporter, ITransientDependency
    {
        public InvalidLeadExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportLeadDto> leadItemListDtos)
        {
            return CreateExcelPackage(
           "InvalidLeads.xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.CreateSheet("InvalidLeads");

                AddHeader(
                        sheet,
                        L("FirstName"),
                        L("MiddleName"),
                        L("LastName"),                        
                        L("Mobile"),
                        L("Email"),
                        L("Address1"),
                        L("Address2"),
                        L("State"),
                        L("District"),
                        L("Taluka"),
                        L("City"),
                        L("PostCode"),
                        L("RequiredCapacity"),                       
                        L("Averagemonthlyunit"),
                        L("AvgMonthlyBill"),
                        L("Source"),
                        L("Notes"),
                        L("Exception")
                    );

                AddObjects(
                    sheet, leadItemListDtos,
                    _ => _.FirstName,
                    _ => _.MiddleName,
                    _ => _.LastName,                   
                    _ => _.Mobile,
                    _ => _.Email,
                    _ => _.Address_Line1,
                    _ => _.Address_Line2,
                    _ => _.StateName,
                    _ => _.DistrictName,
                    _ => _.TalukaName,
                    _ => _.CityName,
                    _ => _.PostCode,
                    _ => _.RequiredCapacity,                    
                    _ => _.Avg_monthly_unit,
                    _ => _.Avg_monthly_bill,
                    _ => _.LeadSource,
                    _ => _.Notes,
                    _ => _.Exception
                );

            });
        }
    }
}
