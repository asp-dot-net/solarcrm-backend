﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Abp.Threading;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.Leads.Importing.Dto;
using solarcrm.Notifications;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Organizations;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Linq.Extensions;


using Abp.Notifications;
using Abp.Extensions;
using solarcrm.MultiTenancy;
using solarcrm.DataVaults.ActivityLog;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using Newtonsoft.Json;
using Abp.UI;

namespace solarcrm.DataVaults.Leads.Importing
{
    public class ImportLeadsToExcelJob : BackgroundJob<ImportLeadsFromExcelJobArgs>, ITransientDependency
	{

		private readonly IRepository<Leads> _leadRepository;
		
		private readonly IRepository<LeadSource.LeadSource> _leadSourceRepository;
		private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
		//private readonly IRepository<PostCode> _postCodeRepository;
		//private readonly IRepository<State> _stateRepository;
		private readonly ILeadListExcelDataReader _leadListExcelDataReader;
		private readonly IBinaryObjectManager _binaryObjectManager;
		private readonly IObjectMapper _objectMapper;
		private readonly IAppNotifier _appNotifier;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IInvalidLeadExporter _invalidLeadExporter;
		private readonly IAbpSession _abpSession;
		private readonly ILeadsAppService _leadServiceRepository;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<ExternalLeads> _externalLeadRepository;
		private int _sectionId = 29;

		public ImportLeadsToExcelJob(
			IRepository<Leads> leadRepository,
			IRepository<ExternalLeads> externalleadRepository,
			IRepository<LeadSource.LeadSource> leadSourceRepository,
			IRepository<LeadActivityLogs> leadactivityRepository,
			//IRepository<PostCode> postCodeRepository,
			//IRepository<State> stateRepository,
			ILeadListExcelDataReader leadListExcelDataReader,
			IBinaryObjectManager binaryObjectManager,
			IObjectMapper objectMapper,
			IAppNotifier appNotifier,
			IUnitOfWorkManager unitOfWorkManager,
			IInvalidLeadExporter invalidLeadExporter,
			IAbpSession abpSession,
			ILeadsAppService leadServiceRepository
			, IRepository<User, long> userRepository
			)
		{
			_leadRepository = leadRepository;
			_leadSourceRepository = leadSourceRepository;
			_leadactivityRepository = leadactivityRepository;
			_externalLeadRepository = externalleadRepository;
			//_postCodeRepository = postCodeRepository;
			//_stateRepository = stateRepository;
			_leadListExcelDataReader = leadListExcelDataReader;
			_binaryObjectManager = binaryObjectManager;
			_objectMapper = objectMapper;
			_appNotifier = appNotifier;
			_unitOfWorkManager = unitOfWorkManager;
			_invalidLeadExporter = invalidLeadExporter;
			_abpSession = abpSession;
			_leadServiceRepository = leadServiceRepository;
			_userRepository = userRepository;
		}
		[UnitOfWork]
		public override void Execute(ImportLeadsFromExcelJobArgs args)
		{
			var leads = GetLeadListFromExcelOrNull(args);
			if (leads == null || !leads.Any())
			{
				SendInvalidExcelNotification(args);
				return;
			}
			else
			{
				var invalidLeads = new List<ImportLeadDto>();

				foreach (var Lead in leads)
				{
					if (Lead.CanBeImported())
					{
						try
						{
							AsyncHelper.RunSync(() => CreateLeadAsync(Lead, args));
						}
						catch (UserFriendlyException exception)
						{
							//Lead.Exception = exception.Message;
							invalidLeads.Add(Lead);
						}
						catch (Exception e)
						{
							//Lead.Exception = e.ToString();
							invalidLeads.Add(Lead);
						}
					}
					else
					{
						invalidLeads.Add(Lead);
					}
				}

				using (var uow = _unitOfWorkManager.Begin())
				{
					using (CurrentUnitOfWork.SetTenantId(args.TenantId))
					{
						AsyncHelper.RunSync(() => ProcessImportLeadsResultAsync(args, invalidLeads));
					}

					uow.Complete();
				}
			}
		}

		private List<ImportLeadDto> GetLeadListFromExcelOrNull(ImportLeadsFromExcelJobArgs args)
		{
			using (var uow = _unitOfWorkManager.Begin())
			{
				using (CurrentUnitOfWork.SetTenantId(args.TenantId))
				{
					try
					{
						var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        string filename = Path.GetExtension(file.Description);
                        return _leadListExcelDataReader.GetLeadFromExcel(file.Bytes, filename);
					}
					catch (Exception)
					{
						return null;
					}
					finally
					{
						uow.Complete();
					}
				}
			}
		}

		private async Task CreateLeadAsync(ImportLeadDto input, ImportLeadsFromExcelJobArgs args)
		{
			//Leads lead = new Leads();
			var lead = _objectMapper.Map<Leads>(input);
			//var Suburb = 0;
			//var State = 0;
			var leadSourceId = 0;
			using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
			{
                //	Suburb = await _postCodeRepository.GetAll().Where(e => e.PostalCode == input.PostCode).Select(e => e.Id).FirstOrDefaultAsync();
                //	State = await _stateRepository.GetAll().Where(e => e.Name == input.State).Select(e => e.Id).FirstOrDefaultAsync();

                if (input.LeadSource != null)
                {
                    leadSourceId = await _leadSourceRepository.GetAll().Where(e => e.Name.ToUpper() == input.LeadSource.ToUpper()).Select(e => e.Id).FirstOrDefaultAsync();
                }

                //}

                if (leadSourceId == 0)
				{
					input.Exception = "Lead Source Not Found";
					throw new Exception("Lead Source Not Found");
				}
				lead.TenantId = (int)args.TenantId;
				lead.CustomerName = input.FirstName + " " + input.MiddleName + " " + input.LastName;
				lead.EmailId = input.Email;

				lead.AvgmonthlyBill = input.Avg_monthly_bill;
				lead.AvgmonthlyUnit = input.Avg_monthly_unit;
				//if (input.Phone.Length < 10 && input.Phone.Length != 0)
				//{
				//	lead.Alt_Phone = input.Phone.ToString().PadLeft(10, '0');
				//}
				//else
				//{
				//	lead.Alt_Phone = input.Phone;
				//}
				if (input.Mobile.Length < 10 && input.Mobile.Length != 0)
				{
					lead.MobileNumber = input.Mobile.ToString().PadLeft(10, '0');
				}
				else
				{
					lead.MobileNumber = input.Mobile;
				}
			
				if (leadSourceId != 0)
				{
					lead.LeadSourceId = leadSourceId;
				}
				lead.LeadStatusId = 1;
				lead.LeadTypeId = 1;
			
				lead.SolarTypeId = 2;
				lead.CommonMeter = false;
				//1. Create
				//2. Excel Import
				//3. External Link
				//4. Copy Lead
				
				lead.CreatorUserId = (int)args.User.UserId;
				lead.OrganizationUnitId = args.OrganizationId;

                var dbDup = _leadRepository.GetAll().Where(e => e.OrganizationUnitId == args.OrganizationId && e.ChanelPartnerID != null && ((e.EmailId == input.Email && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == input.Mobile && !string.IsNullOrEmpty(e.MobileNumber)))).FirstOrDefault();
                if (dbDup != null)
                {
                    lead.IsDuplicate = true;
                    lead.DublicateLeadId = dbDup.Id;
                }
                else
                {
                    lead.IsDuplicate = false;
                }

                //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                var webDup = _leadRepository.GetAll().Where(e => e.OrganizationUnitId == args.OrganizationId && e.ChanelPartnerID == null && e.HideDublicate != true && ((e.EmailId == input.Email && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == input.Mobile && !string.IsNullOrEmpty(e.MobileNumber)))).Any();
                if (webDup)
                {
                    lead.IsWebDuplicate = true;
                }
                else
                {
                    lead.IsWebDuplicate = false;
                }
                //lead.ConsumerNumber = 0;
                var LeadId = await _leadRepository.InsertAndGetIdAsync(lead);
                if (LeadId != null)
                {
                    ExternalLeads ext = new ExternalLeads();
                    ext.LeadId = LeadId;
                    ext.StateName = input.StateName;
                    ext.DistrictName = input.DistrictName;
                    ext.TalukaName = input.TalukaName;
                    ext.CityName = input.CityName;
                    ext.SourceName = input.LeadSource;
                    ext.IsExternalLead = 2;
                    //ext.ExcelAddress = "2";
                    //ext.IsGoogle = "";
                    ext.IsPromotion = true;
                    ext.CreatorUserId = (int)args.User.UserId;
                    var extLeadId = await _externalLeadRepository.InsertAsync(ext);
                }

                var UserName = _userRepository.GetAll().Where(e => e.Id == (int)args.User.UserId).Select(e => e.FullName).FirstOrDefault();
				LeadActivityLogs leadactivity = new LeadActivityLogs();
				leadactivity.LeadActionId = 1;
				leadactivity.SectionId = _sectionId;
				leadactivity.LeadActionNote = "Manage Lead Import From Excel";
				leadactivity.LeadId = lead.Id;
				leadactivity.TenantId = (int)args.TenantId;
				leadactivity.CreatorUserId = (int)args.User.UserId;
				await _leadactivityRepository.InsertAsync(leadactivity);
			}
		}

		private void SendInvalidExcelNotification(ImportLeadsFromExcelJobArgs args)
		{
			using (var uow = _unitOfWorkManager.Begin())
			{
				using (CurrentUnitOfWork.SetTenantId(args.TenantId))
				{
					AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
						args.User,
						new LocalizableString("FileCantBeConvertedToLead", solarcrmConsts.LocalizationSourceName),
						null,
						Abp.Notifications.NotificationSeverity.Warn));
				}
				uow.Complete();
			}
		}

		private async Task ProcessImportLeadsResultAsync(ImportLeadsFromExcelJobArgs args,
			List<ImportLeadDto> invalidLeads)
		{
			if (invalidLeads.Any())
			{
				var file = _invalidLeadExporter.ExportToFile(invalidLeads);
				await _appNotifier.SomeLeadsCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
			}
			else
			{
				await _appNotifier.SendMessageAsync(
					args.User,
					new LocalizableString("AllLeadsSuccessfullyImportedFromExcel", solarcrmConsts.LocalizationSourceName),
					null,
					Abp.Notifications.NotificationSeverity.Success);
			}
		}
	}
}
