﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Leads.Importing.Dto
{
    public class ImportLeadDto : EntityDto<int?>
	{
		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string LastName { get; set; }

		//public string Category { get; set; }

		public string Email { get; set; }

		//public string Phone { get; set; }

		public string Mobile { get; set; }

		public virtual string Address_Line1 { get; set; }

		public virtual string Address_Line2 { get; set; }

		public string StateName { get; set; }

		public string DistrictName { get; set; }

		public string TalukaName { get; set; }

		public string CityName { get; set; }

		public string PostCode { get; set; }
		
		public int? RequiredCapacity { get; set; }

        //public decimal? Latitude { get; set; }

        //public decimal? Longitude { get; set; }

        public int? Avg_monthly_unit { get; set; }

        public int? Avg_monthly_bill { get; set; }

        public string LeadSource { get; set; }

        public string Notes { get; set; }

        public string Exception { get; set; }

		public bool CanBeImported()
		{
			return string.IsNullOrEmpty(Exception);
		}

		
		//public string UnitNo { get; set; }

		//public string UnitType { get; set; }

		//public string StreetNo { get; set; }

		//public string StreetName { get; set; }

		//public string StreetType { get; set; }

		//public string Requirements { get; set; }

		//public virtual string SystemType { get; set; }

		//public virtual string RoofType { get; set; }

		//public virtual string AngleType { get; set; }

		//public virtual string StoryType { get; set; }

		//public virtual string HouseAgeType { get; set; }

		public int IsExternalLead { get; set; }

		public int OrganizationId { get; set; }
	}
}
