﻿using Abp.Dependency;
using solarcrm.DataVaults.Leads.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Leads.Importing
{
    public interface ILeadListExcelDataReader : ITransientDependency
    {
        List<ImportLeadDto> GetLeadFromExcel(byte[] fileBytes, string filename);
    }
}
