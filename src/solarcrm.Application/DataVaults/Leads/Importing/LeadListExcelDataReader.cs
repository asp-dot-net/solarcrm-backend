﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Leads.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Leads.Importing
{
    public class LeadListExcelDataReader : NpoiExcelImporterBase<ImportLeadDto>, ILeadListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public LeadListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(solarcrmConsts.LocalizationSourceName);
        }

        public List<ImportLeadDto> GetLeadFromExcel(byte[] fileBytes, string filename)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow, filename);
        }

		private ImportLeadDto ProcessExcelRow(ISheet worksheet, int row)
		{
			if (IsRowEmpty(worksheet, row))
			{
				return null;
			}

			var exceptionMessage = new StringBuilder();
			var leadsItem = new ImportLeadDto();

			IRow roww = worksheet.GetRow(row);
			List<ICell> cells = roww.Cells;
			List<string> rowData = new List<string>();

			for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
			{
				ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
				rowData.Add(Convert.ToString(cell));
			}

			try
			{
				leadsItem.FirstName = rowData[0].Trim();
				leadsItem.MiddleName = rowData[1];
				leadsItem.LastName = rowData[2];
				//stockItem.Category = rowData[3];
				leadsItem.Mobile = rowData[3];
				leadsItem.Email = rowData[4];
				leadsItem.Address_Line1 = rowData[5];
				leadsItem.Address_Line2 = rowData[6];

				leadsItem.StateName = rowData[7];
				leadsItem.DistrictName = rowData[8];
				leadsItem.TalukaName = rowData[9];
				leadsItem.CityName = rowData[10];
				leadsItem.PostCode = rowData[11];

				leadsItem.RequiredCapacity = !string.IsNullOrEmpty(rowData[12]) ? Convert.ToInt32(rowData[12]) : 0;

				leadsItem.Avg_monthly_unit = !string.IsNullOrEmpty(rowData[13]) ? Convert.ToInt32(rowData[13]) : null;

				leadsItem.Avg_monthly_bill = !string.IsNullOrEmpty(rowData[14]) ? Convert.ToInt32(rowData[14]) : null;
				leadsItem.LeadSource = rowData[15];
				leadsItem.Notes = rowData[16];

			
            }
			catch (System.Exception exception)
			{
				leadsItem.Exception = exception.Message;
			}

			return leadsItem;
		}
		private string GetLocalizedExceptionMessagePart(string parameter)
		{
			return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
		}

		private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		{
			var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
			if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
			{
				return cellValue;
			}

			exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
			return null;
		}

		private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		{
			var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

			return cellValue;

		}

		private bool IsRowEmpty(ISheet worksheet, int row)
		{
			var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
			return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
		}
	}
}
