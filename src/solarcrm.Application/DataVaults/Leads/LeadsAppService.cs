﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.Customers.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.Authorization.Users;
using solarcrm.Notifications;
using Abp.Timing.Timezone;
using Abp.Organizations;
using System;
using Abp.Authorization.Users;
using solarcrm.Authorization.Roles;
using Abp.Domain.Uow;
using Abp.Notifications;
using Abp.Extensions;
using solarcrm.MultiTenancy;
using solarcrm.DataVaults.ActivityLog;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using Newtonsoft.Json;
using solarcrm.Job;
using solarcrm.Common;
using NPOI.SS.Formula.Functions;

namespace solarcrm.DataVaults.Leads
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer, AppPermissions.Pages_Tenant_Leads_ManageLead)]
    public class LeadsAppService : solarcrmAppServiceBase, ILeadsAppService
    {
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Leads> _leaddataRepository;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLog.LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<LeadtrackerHistory> _leadtrackerRepository;
        private readonly IRepository<LeadAction.LeadAction, int> _lookup_leadActionRepository;
        private readonly IRepository<LeadType.LeadType, int> _lookup_leadTypeRepository;
        private readonly IRepository<City.City, int> _lookup_cityRepository;
        private readonly IRepository<Taluka.Taluka, int> _lookup_talukaRepository;
        private readonly IRepository<District.District, int> _lookup_districtRepository;
        private readonly IRepository<Discom.Discom, int> _lookup_discomRepository;
        private readonly IRepository<Division.Division, int> _lookup_divisionRepository;
        private readonly IRepository<SubDivision.SubDivision, int> _lookup_subDivisionRepository;
        private readonly IRepository<Circle.Circle, int> _lookup_circleRepository;
        private readonly IRepository<HeightStructure.HeightStructure, int> _lookup_hightofStructureRepository;
        private readonly IRepository<State.State, int> _lookup_stateRepository;
        private readonly IRepository<LeadSource.LeadSource, int> _lookup_leadSourceRepository;
        private readonly IRepository<LeadStatus.LeadStatus, int> _lookup_leadStatusRepository;
        private readonly IRepository<SolarType.SolarType, int> _lookup_solarTypeRepository;
        private readonly IRepository<RejectReasons.RejectReasons, int> _lookup_rejectReasonRepository;
        private readonly IRepository<CancelReasons.CancelReasons, int> _lookup_cancelReasonRepository;
        private readonly IRepository<CancelRejectLeadReason, int> _cancelRejectLeadReasonRepository;
        private readonly ILeadsExcelExporter _leadsExcelExporter;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IRepository<EmailTemplates.EmailTemplates> _emailTemplateRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<SmsTemplates.SmsTemplates> _smsTemplateRepository;
        private readonly NotificationAppService _NotificationAppService;
        private readonly IRepository<Sections.Section> _SectionAppService;
        private readonly IRepository<LeadDocuments.LeadDocuments> _leaddocumentRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<UserDetail> _userDetailsRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<ExternalLeads> _externalLeadRepository;

        public LeadsAppService(IRepository<Leads> LeadsRepository,
            IRepository<Leads> leaddataRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<LeadActivityLog.LeadActivityLogs> leadactivityRepository,
            IRepository<LeadType.LeadType, int> lookup_leadTypeRepository,
            IRepository<SolarType.SolarType, int> lookup_solarTypeRepository,
            IRepository<LeadAction.LeadAction> lookup_leadActionRepository,
            IRepository<Discom.Discom, int> lookup_discomRepository,
            IRepository<City.City> lookup_cityRepository,
            IRepository<Division.Division, int> lookup_divisionRepository,
            IRepository<SubDivision.SubDivision, int> lookup_subDivisionRepository,
            IRepository<HeightStructure.HeightStructure, int> lookup_hightofStructureRepository,
            IRepository<Circle.Circle, int> lookup_circleRepository,
            IRepository<Taluka.Taluka> lookup_talukaRepository,
            IRepository<District.District> lookup_districtRepository,
            IRepository<ApplicationStatus> applicationstatusRepository,
            IRepository<State.State> lookup_stateRepository,
            IRepository<LeadSource.LeadSource> lookup_leadSourceRepository,
            IRepository<LeadStatus.LeadStatus> lookup_leadStatusRepository,
            IRepository<RejectReasons.RejectReasons> lookup_rejectReasonRepository,
            IRepository<CancelReasons.CancelReasons> lookup_cancelReasonRepository,
            IRepository<CancelRejectLeadReason> cancelRejectLeadReasonRepository,
            IRepository<EmailTemplates.EmailTemplates> emailTemplateRepository,
            IRepository<SmsTemplates.SmsTemplates> smsTemplateRepository,
            IRepository<Sections.Section> sectionRepository,
            IRepository<LeadDocuments.LeadDocuments> leadDocumentRepository,
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<Tenant> tenantRepository,
            IRepository<LeadtrackerHistory> leadtrackerRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IUserEmailer userEmailer,
            UserManager userManager,
            NotificationAppService NotificationAppService,
            ITimeZoneConverter timeZoneConverter,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository,
            ILeadsExcelExporter leadsExcelExporter,
            IRepository<User, long> userRepository,
            IRepository<UserRole, long> userroleRepository,
            IRepository<Role> roleRepository,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IWebHostEnvironment env,
            IRepository<UserDetail> userDataisRepository,
            IRepository<ExternalLeads> externalleadRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _env = env;
            _leadRepository = LeadsRepository;
            _leaddataRepository = leaddataRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _leadactivityRepository = leadactivityRepository;
            _lookup_leadTypeRepository = lookup_leadTypeRepository;
            _lookup_solarTypeRepository = lookup_solarTypeRepository;
            _lookup_divisionRepository = lookup_divisionRepository;
            _lookup_cityRepository = lookup_cityRepository;
            _lookup_cityRepository = lookup_cityRepository;
            _lookup_circleRepository = lookup_circleRepository;
            _lookup_hightofStructureRepository = lookup_hightofStructureRepository;
            _lookup_talukaRepository = lookup_talukaRepository;
            _lookup_districtRepository = lookup_districtRepository;
            _lookup_subDivisionRepository = lookup_subDivisionRepository;
            _lookup_stateRepository = lookup_stateRepository;
            _lookup_leadSourceRepository = lookup_leadSourceRepository;
            _lookup_leadStatusRepository = lookup_leadStatusRepository;
            _lookup_rejectReasonRepository = lookup_rejectReasonRepository;
            _lookup_cancelReasonRepository = lookup_cancelReasonRepository;
            _lookup_discomRepository = lookup_discomRepository;
            _cancelRejectLeadReasonRepository = cancelRejectLeadReasonRepository;
            _emailTemplateRepository = emailTemplateRepository;
            _leadtrackerRepository = leadtrackerRepository;
            _smsTemplateRepository = smsTemplateRepository;
            _SectionAppService = sectionRepository;
            _leaddocumentRepository = leadDocumentRepository;
            _dbContextProvider = dbContextProvider;
            _timeZoneConverter = timeZoneConverter;
            _leadsExcelExporter = leadsExcelExporter;
            _userRepository = userRepository;
            _userroleRepository = userroleRepository;
            _jobRepository = jobRepository;
            _applicationstatusRepository = applicationstatusRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _roleRepository = roleRepository;
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _userEmailer = userEmailer;
            _userManager = userManager;
            _tenantRepository = tenantRepository;
            _NotificationAppService = NotificationAppService;
            _userDetailsRepository = userDataisRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _externalLeadRepository = externalleadRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
        }

        /// <summary>
        /// Manage Leads API(Only UnAssigned Leads)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadsForViewDto>> GetManageAllLeads(GetAllLeadsInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            //  var jobnumberlist = new List<int?>();
            var job_list = _jobRepository.GetAll(); //.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

            var jobnumberlist = new List<int>();
            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            var le = _leadRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID == null && e.HideDublicate != true);
            var filteredLeads = _leadRepository.GetAll()
                        .Include(e => e.LeadStatusIdFk)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CustomerName.Contains(input.Filter) || e.EmailId.Contains(input.Filter) || e.Alt_Phone.Contains(input.Filter) ||
                        e.MobileNumber.Contains(input.Filter) || e.AddressLine1.Contains(input.Filter) || e.AddressLine2.Contains(input.Filter) || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.Id)))// || e.Requirements.Contains(input.Filter) || jobnumberlist.Contains(e.Id))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CustomerName), e => e.CustomerName == input.CustomerName)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.EmailId == input.EmailFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Alt_Phone == input.PhoneFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.MobileNumber == input.MobileFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.AddressLine1 == input.AddressFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.Pincode == Convert.ToInt32(input.PostCodeFilter))
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateIdFk.Name == input.StateNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
                        .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusIdFk != null && e.LeadStatusIdFk.Status == input.LeadStatusName)
                        .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)

                        .WhereIf(!string.IsNullOrWhiteSpace(input.LeadTypeNameFilter), e => e.LeadTypeIdFk.Name == input.LeadTypeNameFilter)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                        .WhereIf(input.OrganizationUnit != null, e => e.OrganizationUnitId == input.OrganizationUnit)
                        //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                        //.WhereIf(input.DuplicateFilter == "web", o => (_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.ChanelPartnerID == null && e.OrganizationUnitId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0))
                        //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                        //.WhereIf(input.DuplicateFilter == "db", o => (_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID != null).Count() > 0))
                        //Not Duplicate Leads
                        //.WhereIf(input.DuplicateFilter == "no", o => !(_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.ChanelPartnerID == null && e.OrganizationUnitId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0) && !(_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID != null).Count() > 0))
                        // .Where(e => e.ChanelPartnerID == null)
                        // .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true && e.HideDublicate !=true && e.OrganizationUnitId == input.OrganizationUnit)
                        .WhereIf(input.DuplicateFilter == "web", o => le.Where(e => e.Id != o.Id && ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).Any())

                        //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "db", o => o.IsDuplicate == true)

                        //Not Duplicate Leads
                        .WhereIf(input.DuplicateFilter == "no", o => o.IsDuplicate != true && le.Where(e => e.Id != o.Id && ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).Any() != true)

                        .Where(e => e.ChanelPartnerID == null && e.HideDublicate != true);

            var pagedAndFilteredLeads = filteredLeads
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input);

            var leads = (from o in pagedAndFilteredLeads
                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                         join o2 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()
                         select new GetLeadsForViewDto()
                         {
                             Leads = new LeadsDto
                             {
                                 CustomerName = o.CustomerName,
                                 EmailId = o.EmailId,
                                 Alt_Phone = o.Alt_Phone,
                                 MobileNumber = o.MobileNumber,
                                 Address = (o.AddressLine1 == null ? "" : o.AddressLine1 + ",") + (o.AddressLine2 == null ? "" : o.AddressLine2 + ","),
                                 CityName = o.CityId == null ? _externalLeadRepository.GetAll().Where(x => x.LeadId == o.Id).Select(e => e.CityName).FirstOrDefault() : o.CityIdFk.Name,
                                 Id = o.Id,
                                 Pincode = o.Pincode,
                                 LeadStatusID = o.LeadStatusId,
                                 LeadSourceName = o.LeadSourceIdFk.Name,
                                 ChanelPartnerID = o.ChanelPartnerID,
                                 IsExternalLead = Convert.ToInt32(_externalLeadRepository.GetAll().Where(x => x.LeadId == o.Id).Select(e => e.IsExternalLead).FirstOrDefault()),  //o.IsExternalLead,
                                 OrganizationId = o.OrganizationUnitId,
                                 OrganizationName = s2.DisplayName,
                                 CreatedDate = (DateTime)o.CreationTime,
                                 IsSelected = false,
                             },
                             Id = o.Id,
                             // DublicateLeadId = _leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.OrganizationUnitId == input.OrganizationUnit && e.Id != o.Id && e.ChanelPartnerID != null).OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault(),
                             LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                             //Duplicate = (o.IsDuplicate != true) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID != null).Any(),
                             //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                             //WebDuplicate = (o.IsWebDuplicate != true) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.ChanelPartnerID == null && e.OrganizationUnitId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Any(),
                             Duplicate = o.IsDuplicate != true ? false : true,
                             WebDuplicate = o.IsWebDuplicate != true ? false : true,
                             DublicateLeadId = o.DublicateLeadId
                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadsForViewDto>(
                totalCount,
                await leads.ToListAsync()
            );
        }



        /// <summary>
        /// My Leads API(Only Assigned Leads - CUrrent User)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<GetLeadsForViewDto>> GetAllForMyLeadCustomer(GetAllLeadsInput input)
        {
            List<int> jobnumberlist = new List<int>();
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> A = new List<int>();
            List<int?> joblist = new List<int?>();
            List<string> RoleName = new List<string>();
            var sectionList = await _SectionAppService.GetAll().ToListAsync();
            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            var User_List = _userRepository.GetAll();
            var mylead = _leadRepository.GetAll().Where(x => x.OrganizationUnitId == input.OrganizationUnit); // _leadRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnit);
            var job_list = _jobRepository.GetAll().Where(x => x.OrganizationUnitId == input.OrganizationUnit);

            var leadactivity_list = await _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit).ToListAsync();

            //var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

            if (User_List != null || User_List.Count() > 0)
            {
                RoleName = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && user.Id == UserId)
                            select (us.DisplayName)).ToList();
            }

            List<UserDetail> managerlist = await _userDetailsRepository.GetAll().Where(x => x.ManageBy == UserId).ToListAsync();
            //List<int?[]> managerlist = await _userDetailsRepository.GetAll().Where(x => x.ManageBy == UserId).Select(x => new[] { x.ManageBy, x.UserId }).ToListAsync();

            if (RoleName != null)
            {
                if (RoleName.Contains("Manager") && managerlist.Count > 0)
                {
                    var man = Convert.ToInt32(managerlist.Select(x => x.ManageBy).FirstOrDefault());  //managerlist[0][0]
                    var emp = Convert.ToInt32(managerlist.Select(x => x.UserId).FirstOrDefault());   //managerlist[0][1]

                    mylead = _leadRepository.GetAll().Where(e => e.CreatorUserId == UserId || e.ChanelPartnerID == man || e.ChanelPartnerID == emp);
                }
                else //if (RoleName.Contains("Channel Partners"))
                {
                    mylead = _leadRepository.GetAll().Where(e => e.ChanelPartnerID == UserId || e.CreatorUserId == UserId);
                }
            }
            if (!string.IsNullOrEmpty(input.Filter) && job_list != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }

            if (input.StartDate != null && input.EndDate != null && input.DateFilterType.ToLower() == "followup" && leadactivity_list != null)
            {
                FollowupList = leadactivity_list.Where(e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.LeadActionId == Convert.ToInt32(LeadActionConsts.ReminderSent)).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            }
            if (input.StartDate != null && input.EndDate != null && input.DateFilterType.ToLower() == "nextfollowup" && leadactivity_list != null)
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == Convert.ToInt32(LeadActionConsts.ReminderSent)).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            if (input.StartDate != null && input.EndDate != null && input.DateFilterType.ToLower() == "assign" && leadactivity_list != null)
            {
                Assign = leadactivity_list.Where(e => e.CreationTime.AddHours(10).Date >= SDate.Value.Date && e.CreationTime.AddHours(10).Date <= EDate.Value.Date && e.LeadActionId == Convert.ToInt32(LeadActionConsts.LeadAssign)).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            // IQueryable<Leads> filteredLeads = Enumerable.Empty<Leads>().AsQueryable();// Enumerable.Empty<Leads>().AsQueryable();// new Leads[] { }.AsQueryable();
            var filteredLeads = mylead;
            var totalCount = 0;
            if (mylead != null)
            {
                filteredLeads = mylead
                             .Include(e => e.LeadStatusIdFk).Include(e => e.LeadSourceIdFk)
                             .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CustomerName.Contains(input.Filter)
                             || e.EmailId.Contains(input.Filter) || e.Alt_Phone.Contains(input.Filter)
                             || e.MobileNumber.Contains(input.Filter) || e.AddressLine1.Contains(input.Filter) || e.AddressLine2.Contains(input.Filter)
                             //|| e.ConsumerNumber.Contains((long?)Convert.ToInt64(input.Filter))
                             || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.Id)))// || 
                                                                                           //.WhereIf(jobnumberlist.Count != 0, e => jobnumberlist.Contains(e.Id))
                                                                                           //.WhereIf(!string.IsNullOrWhiteSpace(input.CustomerName), e => e.CustomerName == input.CustomerName)
                                                                                           //.WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.EmailId == input.EmailFilter)
                                                                                           //.WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Alt_Phone == input.PhoneFilter)
                                                                                           //.WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.MobileNumber == input.MobileFilter)
                                                                                           //.WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.AddressLine1.Contains(input.AddressFilter))
                                                                                           //.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateIdFk.Name == input.StateNameFilter)
                                                                                           //.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.Pincode == Convert.ToInt32(input.PostCodeFilter))
                             .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceIdFk.Name == input.LeadSourceNameFilter)
                              .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                              .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                              .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.SolarTypeId))
                              .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.TenantId))
                              .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.DiscomId))
                              .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.DivisionId))
                              .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.SubDivisionId))
                              //.WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)

                              .WhereIf(input.DateFilterType.ToLower() == "creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                              .WhereIf(input.DateFilterType.ToLower() == "assign" && input.StartDate != null && input.EndDate != null, e => Assign.Contains(e.Id))
                             .WhereIf(input.DateFilterType.ToLower() == "reassign" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadStatusId == 11)
                             .WhereIf(input.DateFilterType.ToLower() == "followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                             .WhereIf(input.DateFilterType.ToLower() == "nextfollowup" && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id))


                             .WhereIf(!string.IsNullOrWhiteSpace(input.LeadTypeNameFilter), e => e.SolarTypeIdFk.Name == input.LeadTypeNameFilter)

                             //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                             .WhereIf(input.SalesManagerId == null && input.SalesRepId == null && RoleName.Contains("Admin"), e => e.CreatorUserId != null && e.ChanelPartnerID != null)
                             //.WhereIf(input.SalesManagerId == null && input.SalesRepId == null && RoleName.Contains("Admin"), e => UserList.Contains(e.ChanelPartnerID))
                             .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.ChanelPartnerID == input.SalesRepId)
                             .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.ChanelPartnerID == input.SalesManagerId)
                             .WhereIf(input.OrganizationUnit != null, e => e.OrganizationUnitId == input.OrganizationUnit);


                totalCount = await filteredLeads.CountAsync();
            }

            var pagedAndFilteredLeads = filteredLeads
                 .OrderBy(input.Sorting ?? "id desc")
                 .PageBy(input);

            //var LeadidList = Job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();
            var leads = from o in pagedAndFilteredLeads

                        join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                        join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                        from s3 in j3.DefaultIfEmpty()

                        join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                        from s4 in j4.DefaultIfEmpty()

                        join o6 in _jobRepository.GetAll() on o.Id equals o6.LeadId into j6
                        from s6 in j6.DefaultIfEmpty()

                        select new GetLeadsForViewDto()
                        {
                            Leads = new LeadsDto
                            {
                                FirstName = o.CustomerName,
                                EmailId = o.EmailId,
                                Alt_Phone = o.Alt_Phone,
                                MobileNumber = o.MobileNumber,
                                Address = (o.AddressLine1 == null ? "" : o.AddressLine1 + ",") + (o.AddressLine2 == null ? "" : o.AddressLine2 + ",") + (o.CityIdFk.Name == "" ? "" : o.CityIdFk.Name + "") + (o.TalukaIdFk.Name == null ? "" : o.TalukaIdFk.Name + ",") + (o.DistrictIdFk.Name == null ? "" : o.DistrictIdFk.Name + ",") + (o.StateIdFk.Name == null ? "" : o.StateIdFk.Name + ",") + (o.Pincode == 0 ? null : Convert.ToString(o.Pincode)),
                                //string.Join(", ", new[] { o.AddressLine1, o.AddressLine2 }.Where(x => o.AddressLine1 != null || o.AddressLine2 != null)),
                                //o.AddressLine1 == "" ? "" : o.AddressLine1 +"," + o.AddressLine2 == "" ? "" : o.AddressLine2 +"," + o.CityIdFk.Name == "" ? "" : o.CityIdFk.Name + "," + o.TalukaIdFk.Name == null ? "" : o.TalukaIdFk.Name+ "," + o.DistrictIdFk.Name == null ? "" : o.DistrictIdFk.Name +"," + o.StateIdFk.Name == null ? "" : o.StateIdFk.Name + "," + o.Pincode == null ? null : Convert.ToString(o.Pincode),
                                Id = o.Id,
                                LeadStatusName = o.LeadStatusIdFk.Status,
                                LeadSourceName = o.LeadSourceIdFk.Name,
                                OrganizationId = o.OrganizationUnitId,
                                OrganizationName = s4.DisplayName,
                                Longitude = o.Longitude,
                                Latitude = o.Latitude,
                                CommonMeter = o.CommonMeter,
                                SolarTypeName = o.SolarTypeIdFk.Name,
                                //FollowupDate = s5.CreationTime,
                                Notes = o.Notes,
                                LastModificationTime = o.LastModificationTime,
                                CreationTime = (DateTime)o.CreationTime,
                                ChanelPartnerName = User_List.Where(e => e.Id == o.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                //_userRepository.GetAll().Where(e => e.Id == o.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                LeadCreatedName = User_List.Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                LeadStatusColorClass = o.LeadStatusIdFk.LeadStatusColorClass,
                                LeadStatusIconClass = o.LeadStatusIdFk.LeadStatusIconClass,
                                IsVerified = o.IsVerified,
                                LeadSourceColorClass = o.LeadSourceIdFk.LeadSourceColorClass
                            },
                            //Lead Activity
                            ApplicationStatusdata = _applicationstatusRepository.GetAll().Where(x => x.Id == s6.ApplicationStatusId && x.IsActive == true).FirstOrDefault(),
                            ApplicationStatusName = _applicationstatusRepository.GetAll().Where(x => x.Id == s6.ApplicationStatusId).Select(x => x.ApplicationStatusName).FirstOrDefault(),
                            ApplicationStatusColor = _applicationstatusRepository.GetAll().Where(x => x.Id == s6.ApplicationStatusId).Select(x => x.ApplicationStatusLabelColor).FirstOrDefault(),
                            leadsSummaryCount = LeadSummaryCount(filteredLeads.ToList()),
                            //_applicationstatusRepository.GetAll().Where(x=>x.Id == s6.ApplicationStatusId).Select(x=> new[] { Convert.ToString(x.Id), x.ApplicationStatusName, x.ApplicationStatusLabelColor }).FirstOrDefault(),
                            //LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),

                            LeadAssignDate = (DateTime?)_leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.SectionId == 35 && e.LeadActionId == Convert.ToInt32(LeadActionConsts.LeadAssign)).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                            //(DateTime?)leadactivity_list.Where(e => e.LeadId == o.Id && e.LeadActionId == Convert.ToInt32(LeadActionConsts.LeadAssign)).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.CreationTime).AsEnumerable().FirstOrDefault(),

                            // _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                            //(DateTime?)_leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.CreationTime).FirstOrDefault(), 
                            //leadactivity_list.Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.CreationTime).FirstOrDefault(),
                            //leadactivity_list.Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),
                            ReminderTime = (DateTime?)_leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.SectionId == 35).OrderByDescending(e => e.LeadId == o.Id && e.LeadActionId == Convert.ToInt32(LeadActionConsts.ReminderSent)).FirstOrDefault().CreationTime,

                            //(DateTime?)leadactivity_list.Where(e => e.LeadId == o.Id).OrderByDescending(e => e.LeadId == o.Id && e.LeadActionId == Convert.ToInt32(LeadActionConsts.ReminderSent)).DistinctBy(e => e.LeadId).Select(e => e.CreationTime).AsEnumerable().FirstOrDefault(),
                            //(DateTime?)_reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                            //leadactivity_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.LeadActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                            ActivityDescription = _reminderLeadActivityLog.GetAll().Include(e => e.LeadActivityLogIdFk).Where(e => e.LeadActivityLogIdFk.LeadId == o.Id && e.LeadActivityLogIdFk.SectionId == 35).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                            //Comment = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                            //leadactivity_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.LeadActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.LeadActionNote).FirstOrDefault(),
                            ActivityComment = _commentLeadActivityLog.GetAll().Include(e => e.LeadActivityLogIdFk).Where(e => e.LeadActivityLogIdFk.LeadId == o.Id && e.LeadActivityLogIdFk.SectionId == 35).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                        };

            return new PagedResultDto<GetLeadsForViewDto>(
                totalCount,
                leads.ToList()
            );

        }

        public LeadsSummaryCount LeadSummaryCount(List<Leads> filteredLeads)
        {
            var output = new LeadsSummaryCount();
            output.New = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 1).Count());
            output.UnHandled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 2).Count());
            output.Hot = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 5).Count());
            output.Cold = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 3).Count());
            output.Warm = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 4).Count());
            output.Upgrade = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 6).Count());
            output.Rejected = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 7).Count());
            output.Cancelled = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 8).Count());
            output.Closed = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 9).Count());
            output.Total = Convert.ToString(filteredLeads.Count());
            output.Assigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 10).Count());
            output.ReAssigned = Convert.ToString(filteredLeads.Where(e => e.LeadStatusId == 11).Count());
            //output.Sold = Convert.ToString(filteredLeads.Where(e => LeadidList.Contains(e.Id)).Count());
            return output;
        }

        /// <summary>
        /// My Leads Export to Excel API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_ExportToExcel)]
        public async Task<FileDto> getMyLeadToExcel(LeadExcelExportDto input)
        {

            int UserId = (int)AbpSession.UserId;
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            // && e.ChanelPartnerID == UserId

            // && e.ChanelPartnerID == UserId
            var User_List = _userRepository.GetAll();
            var mylead = _leadRepository.GetAll(); // _leadRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnit);
            var job_list = _jobRepository.GetAll();
            try
            {
                var leadactivity_list = await _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit).ToListAsync();
                //      var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadFk.OrganizationId == input.OrganizationUnit && e.LeadFk.IsDuplicate != true && e.LeadFk.IsWebDuplicate != true);
                ///var user = await _userManager.FindByEmailAsync(model.Email);
                // Get the roles for the user
                //var roles = await _userManager.GetRolesAsync(user);
                //var reminderList = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == 1);
                var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();


                var RoleName = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                where (us != null && user.Id == UserId)
                                select (us.DisplayName));


                List<UserDetail> managerlist = await _userDetailsRepository.GetAll().Where(x => x.ManageBy == UserId).ToListAsync();
                //var managerlist = await _userDetailsRepository.GetAll().Where(x => x.ManageBy == UserId).Select(x => new[] { x.ManageBy, x.UserId }).ToListAsync();

                if (RoleName.Contains("Manager"))
                {
                    if (managerlist != null)
                    {
                        var man = Convert.ToInt32((managerlist.Select(x=>x.ManageBy).FirstOrDefault()));
                        var emp = Convert.ToInt32((managerlist.Select(x=>x.UserId).FirstOrDefault()));

                        mylead = _leadRepository.GetAll().Where(e => e.CreatorUserId == UserId || e.ChanelPartnerID == man || e.ChanelPartnerID == emp);
                    }
                }
                else if (RoleName.Contains("Channel Partners"))
                {
                    mylead = _leadRepository.GetAll().Where(e => e.ChanelPartnerID == UserId || e.CreatorUserId == UserId);
                }
                var jobnumberlist = new List<int>();
                if (input.Filter != null)
                {
                    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                }

                var FollowupList = new List<int>();
                var NextFollowupList = new List<int>();
                var Assign = new List<int>();

                if (input.StartDate != null && input.EndDate != null && input.DateFilterType == "Followup")
                {
                    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();

                }
                if (input.StartDate != null && input.EndDate != null && input.DateFilterType == "NextFollowup")
                {
                    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                }
                if (input.StartDate != null && input.EndDate != null && input.DateFilterType == "Assign")
                {
                    Assign = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 3).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                }
                var joblist = new List<int?>();
                if (input.JobStatusID != null && input.JobStatusID.Count() > 0 && input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0)
                {
                    if (input.LeadStatusIDS.Contains(6))
                    {
                        //joblist = job_list.Where(e => input.JobStatusID.Contains((int)e.JobStatusI)).Select(e => e.LeadId).ToList();
                    }
                }
                var filteredLeads = mylead;
                var totalCount = 0;
                if (mylead != null)
                {
                    filteredLeads = mylead
                                .Include(e => e.LeadStatusIdFk).Include(e => e.LeadSourceIdFk)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CustomerName.Contains(input.Filter) || e.EmailId.Contains(input.Filter) || e.Alt_Phone.Contains(input.Filter) ||
                                e.MobileNumber.Contains(input.Filter) || e.AddressLine1.Contains(input.Filter) || e.AddressLine2.Contains(input.Filter) || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.Id)))// || e.ConsumerNumber == Convert.ToInt32(input.Filter))
                                                                                                                                                                                                                      //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.ConsumerNumber == input.Filter)
                                                                                                                                                                                                                      //.WhereIf(jobnumberlist.Count != 0, e => jobnumberlist.Contains(e.Id))
                                .WhereIf(!string.IsNullOrWhiteSpace(input.CustomerName), e => e.CustomerName == input.CustomerName)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.EmailId == input.EmailFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Alt_Phone == input.PhoneFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.MobileNumber == input.MobileFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.AddressLine1.Contains(input.AddressFilter))
                                .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateIdFk.Name == input.StateNameFilter)
                                .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.Pincode == Convert.ToInt32(input.PostCodeFilter))
                                .WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceIdFk.Name == input.LeadSourceNameFilter)
                                 //.WhereIf(input.LeadStatusId > 0, e => e.LeadStatusId == input.LeadStatusId)
                                 .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.LeadStatusId))
                                 .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
                                 .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.SolarTypeId))
                                 .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.TenantId))
                                 .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.DiscomId))
                                 .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.DivisionId))
                                 .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.SubDivisionId))
                                  // .WhereIf(input.StartDate != null && input.EndDate != null, e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                                  .WhereIf(input.DateFilterType == "Creation" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date)
                                 .WhereIf(input.DateFilterType == "Assign" && input.StartDate != null && input.EndDate != null, e => Assign.Contains(e.Id))
                                .WhereIf(input.DateFilterType == "ReAssign" && input.StartDate != null && input.EndDate != null, e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadStatusId == 11)
                                .WhereIf(input.DateFilterType == "Followup" && input.StartDate != null && input.EndDate != null, e => FollowupList.Contains(e.Id))
                                .WhereIf(input.DateFilterType == "NextFollowup" && input.StartDate != null && input.EndDate != null, e => NextFollowupList.Contains(e.Id))
                                .WhereIf(!string.IsNullOrWhiteSpace(input.LeadTypeNameFilter), e => e.SolarTypeIdFk.Name == input.LeadTypeNameFilter)
                                //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
                                .WhereIf(input.SalesManagerId == null && input.SalesRepId == null && RoleName.Contains("Admin"), e => e.CreatorUserId != null && e.ChanelPartnerID != null)
                                //.WhereIf(input.SalesManagerId == null && input.SalesRepId == null && RoleName.Contains("Admin"), e => UserList.Contains(e.ChanelPartnerID))
                                .WhereIf(input.SalesRepId != null && input.SalesRepId != 0, e => e.ChanelPartnerID == input.SalesRepId)
                                .WhereIf(input.SalesManagerId != null && input.SalesManagerId != 0, e => e.ChanelPartnerID == input.SalesManagerId)
                                .WhereIf(input.OrganizationUnit != null, e => e.OrganizationUnitId == input.OrganizationUnit);

                    //.WhereIf(e => e.ChanelPartnerID != null && e.ChanelPartnerID != 0, e => e.CreatorUserId != null);
                    //.Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

                    totalCount = await filteredLeads.CountAsync();
                }



                //var LeadidList = Job_list.Where(e => e.DepositeRecceivedDate != null).Select(e => e.LeadId).ToList();
                var leads = from o in filteredLeads

                                // join o1 in _lookup_rejectReasonRepository.GetAll() on o.LeadStatusId equals o1.Id into j1
                                //from s1 in j1.DefaultIfEmpty()

                            join o2 in _lookup_cancelReasonRepository.GetAll() on o.LeadStatusId equals o2.Id into j2
                            from s2 in j2.DefaultIfEmpty()

                            join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                            from s3 in j3.DefaultIfEmpty()

                            join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                            from s4 in j4.DefaultIfEmpty()

                                //join o5 in _leadactivityRepository.GetAll() on o.Id equals o5.LeadId into j5
                                //from s5 in j5.DefaultIfEmpty()


                                //join o6 in _jobRepository.GetAll() on o.Id equals o6.LeadId into j6
                                //from s6 in j6.DefaultIfEmpty()

                            select new GetLeadsForViewDto()
                            {
                                Leads = new LeadsDto
                                {
                                    FirstName = o.CustomerName,
                                    EmailId = o.EmailId,
                                    Alt_Phone = o.Alt_Phone,
                                    MobileNumber = o.MobileNumber,
                                    Address = (o.AddressLine1 == null ? "" : o.AddressLine1 + ",") + (o.AddressLine2 == null ? "" : o.AddressLine2 + ",") + (o.CityIdFk.Name == "" ? "" : o.CityIdFk.Name + "") + (o.TalukaIdFk.Name == null ? "" : o.TalukaIdFk.Name + ",") + (o.DistrictIdFk.Name == null ? "" : o.DistrictIdFk.Name + ",") + (o.StateIdFk.Name == null ? "" : o.StateIdFk.Name + ",") + (o.Pincode == 0 ? null : Convert.ToString(o.Pincode)),
                                    //string.Join(", ", new[] { o.AddressLine1, o.AddressLine2 }.Where(x => o.AddressLine1 != null || o.AddressLine2 != null)),
                                    //o.AddressLine1 == "" ? "" : o.AddressLine1 +"," + o.AddressLine2 == "" ? "" : o.AddressLine2 +"," + o.CityIdFk.Name == "" ? "" : o.CityIdFk.Name + "," + o.TalukaIdFk.Name == null ? "" : o.TalukaIdFk.Name+ "," + o.DistrictIdFk.Name == null ? "" : o.DistrictIdFk.Name +"," + o.StateIdFk.Name == null ? "" : o.StateIdFk.Name + "," + o.Pincode == null ? null : Convert.ToString(o.Pincode),
                                    Id = o.Id,
                                    LeadStatusName = o.LeadStatusIdFk.Status,
                                    LeadSourceName = o.LeadSourceIdFk.Name,
                                    OrganizationId = o.OrganizationUnitId,
                                    OrganizationName = s4.DisplayName,
                                    Longitude = o.Longitude,
                                    Latitude = o.Latitude,
                                    CommonMeter = o.CommonMeter,
                                    SolarTypeName = o.SolarTypeIdFk.Name,
                                    StateName = o.StateIdFk.Name,
                                    DistrictName = o.DistrictIdFk.Name,
                                    TalukaName = o.TalukaIdFk.Name,
                                    CityName = o.CityIdFk.Name,
                                    Pincode = o.Pincode,
                                    LeadTypeName = o.LeadTypeIdFk.Name,
                                    ConsumerNumber = (int?)o.ConsumerNumber,
                                    AvgMonthlyUnit = (int?)o.AvgmonthlyUnit,
                                    AvgMonthlyBill = (int?)o.AvgmonthlyBill,
                                    DiscomName = o.DiscomIdFk.Name,
                                    DivisionName = o.DivisionIdFk.Name,
                                    SubDivisionName = o.SubDivisionIdFk.Name,
                                    CircleName = o.CircleIdFk.Name,
                                    RequiredCapacity = o.RequiredCapacity,
                                    HeightStructureName = o.HeightStructureIdFk.Name,
                                    CategoryName = o.Category,
                                    BankAccountNumber = o.BankAccountNumber,
                                    BankBranchName = o.BankBrachName,
                                    AccountHolderName = o.BankAccountHolderName,
                                    BankName = o.BankName,
                                    IFSC_Code = o.IFSC_Code,
                                    //FollowupDate = s5.CreationTime,
                                    Notes = o.Notes,
                                    LastModificationTime = o.LastModificationTime,
                                    CreationTime = (DateTime)o.CreationTime,
                                    ChanelPartnerName = _userRepository.GetAll().Where(e => e.Id == o.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                    LeadCreatedName = _userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                                    LeadStatusColorClass = o.LeadStatusIdFk.LeadStatusColorClass,
                                    LeadStatusIconClass = o.LeadStatusIdFk.LeadStatusIconClass,
                                    LeadSourceColorClass = o.LeadSourceIdFk.LeadSourceColorClass,
                                    IsVerified = o.IsVerified
                                },
                                //Lead Activity
                                LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                                LeadAssignDate = _leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                                //(DateTime?)_leadactivityRepository.GetAll().Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.CreationTime).FirstOrDefault(), 
                                //leadactivity_list.Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.CreationTime).FirstOrDefault(),
                                //leadactivity_list.Where(e => e.LeadId == o.Id && e.LeadActionId == 3).OrderByDescending(e => e.Id).Select(e => e.CreationTime).FirstOrDefault(),
                                ReminderTime = (DateTime?)_reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                                //leadactivity_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.LeadActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CreationTime.ToString("dd-MM-yyyy hh:mm:ss")).FirstOrDefault(),
                                ActivityDescription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                //Comment = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                //leadactivity_list.Where(e => (e.SectionId == 13 || e.SectionId == 14) && e.LeadActionId == 8 && e.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.LeadActionNote).FirstOrDefault(),
                                ActivityComment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),


                            };



                var myleadListDtos = await leads.ToListAsync();
                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _leadsExcelExporter.MyLeadExportToFile(myleadListDtos);
                }
                else
                {
                    return _leadsExcelExporter.MyLeadCsvExport(myleadListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //assign the lead
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Assign, AppPermissions.Pages_Tenant_Leads_ManageLead_Assign)]
        public async Task AssignOrTransferLead(GetLeadForAssignOrTransferOutput input)
        {
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            if (input.AssignToUserID != 0 && input.LeadIds != null)
            {
                var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();
                var managerId = await _userDetailsRepository.GetAll().Where(x => x.UserId == assignedToUser.Id).Select(x => x.ManageBy == null ? x.UserId : x.ManageBy).FirstOrDefaultAsync();

                var User_List = _userRepository.GetAll().ToList();
                var UserListsss = (from user in _userRepository.GetAll()
                                   join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                   from ur in urJoined.DefaultIfEmpty()
                                   join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                   from us in usJoined.DefaultIfEmpty()
                                   join uo in _userOrganizationUnitRepository.GetAll() on user.Id equals uo.UserId into uoJoined
                                   from uo in uoJoined.DefaultIfEmpty()
                                       //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                       //from ut in utJoined.DefaultIfEmpty()
                                   where (us != null && us.DisplayName.Contains("Channel") && uo != null && uo.OrganizationUnitId == input.OrganizationID)
                                   select (user.Id)).ToList();

                IList<string> role = (from user in User_List
                                      join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                      from ur in urJoined.DefaultIfEmpty()
                                      join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                      from us in usJoined.DefaultIfEmpty()
                                      where (us != null && user.Id == Convert.ToInt32(assignedToUser.Id))
                                      select (us.DisplayName)).ToList();

                var roleName = "";
                if (role != null)
                {
                    if (role.Contains("Admin"))
                    {
                        roleName = "Admin";
                    }
                    else if (role.Contains("Manager"))
                    {
                        roleName = "Manager";
                    }
                    else if (role.Contains("Channel Partners"))
                    {
                        roleName = "Channel Partners";
                    }
                }


                foreach (var leadid in input.LeadIds)
                {
                    var lead = await _leadRepository.FirstOrDefaultAsync(leadid);

                    LeadActivityLogs leadactivity = new LeadActivityLogs();


                    //Lead Assign to User
                    if (lead.ChanelPartnerID == null)
                    {
                        leadactivity.LeadActionId = 3;
                        leadactivity.LeadActionNote = "Lead Assign To " + assignedToUser.FullName;
                        leadactivity.LastModificationTime = DateTime.UtcNow;

                    }
                    //Lead transfer the User
                    else
                    {

                        var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == lead.ChanelPartnerID).FirstOrDefault();
                        leadactivity.LeadActionId = 4;
                        leadactivity.LeadActionNote = "Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        leadactivity.LastModificationTime = DateTime.UtcNow;

                        //LeadActivityLog leadactivity = new LeadActivityLog();
                        //leadactivity.ActionId = 4;
                        //leadactivity.SectionId = 0;
                        //if (lead.LeadStatusId == 7)
                        //{
                        //    leadactivity.ActionNote = "Rejected Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        //}
                        //else if (lead.LeadStatusId == 8)
                        //{
                        //    leadactivity.ActionNote = "Canceled Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        //}
                        //else if (lead.LeadStatusId == 11)
                        //{
                        //    leadactivity.ActionNote = "Lead ReAssign From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        //}
                        //else
                        //{
                        //    leadactivity.ActionNote = "Lead Transfer From " + assignedFromUser.FullName + " To " + assignedToUser.FullName;
                        //}
                        //leadactivity.LeadId = lead.Id;
                        //if (AbpSession.TenantId != null)
                        //{
                        //    leadactivity.TenantId = (int)AbpSession.TenantId;
                        //}
                        //leadactivity.CreatorUserId = AbpSession.UserId;
                        //await _leadactivityRepository.InsertAsync(leadactivity);
                    }

                    //assign lead the Transfer
                    if (input.OrganizationID != null)
                    {
                        lead.OrganizationUnitId = (int)input.OrganizationID;
                    }
                    lead.ChanelPartnerID = input.AssignToUserID;
                    lead.LastModificationTime = DateTime.UtcNow;
                    await _leadRepository.UpdateAsync(lead);


                    //Activity Log
                    leadactivity.LeadId = lead.Id;
                    leadactivity.SectionId = 35;
                    if (input.OrganizationID != null)
                    {
                        leadactivity.OrganizationUnitId = (int)input.OrganizationID;
                    }

                    leadactivity.TenantId = (int)AbpSession.TenantId;
                    leadactivity.CreatorUserId = AbpSession.UserId;
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }

                //Send Notification
                if (input.LeadIds.Count > 0)
                {

                    //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == input.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

                    var UserList = (from user in User_List
                                    join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                    from ur in urJoined.DefaultIfEmpty()

                                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                    from us in usJoined.DefaultIfEmpty()

                                        //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                        //from ut in utJoined.DefaultIfEmpty()

                                    where us != null && us.DisplayName == roleName  //&& ut != null && ut.TeamId == TeamId
                                    select user).Distinct();
                    if (UserList != null)
                    {
                        foreach (var item in UserList)
                        {
                            if (UserDetail.UserName.Contains("admin") && item.Id == assignedToUser.Id)
                            {
                                var managerData = User_List.Where(x => x.Id == managerId).FirstOrDefault();
                                string msg = string.Format("{0} Lead Assigned To {1}", input.LeadIds.Count, assignedToUser.FullName);
                                await _appNotifier.LeadAssiged(managerData, msg, NotificationSeverity.Info);
                            }
                            if (item.Id == assignedToUser.Id)
                            {
                                //string msg = string.Format("{0} Lead Assigned To You", input.LeadIds.Count);
                                //await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);

                                //Send Email
                                try
                                {
                                    var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
                                    await _userEmailer.SendEmailNewLeadAssign(item, Convert.ToString(input.LeadIds.Count));
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                            else
                            {
                                if (roleName != "Channel Partners")
                                {
                                    string msg = string.Format("{0} Lead Assigned To {1}", input.LeadIds.Count, assignedToUser.FullName);
                                    await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                                }
                                if (roleName != "Channel Partners")
                                {
                                    //Send Email
                                    try
                                    {
                                        var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
                                        await _userEmailer.SendEmailNewLeadToAssignTeamMember(item, Convert.ToString(input.LeadIds.Count), assignedToUser.FullName);
                                    }
                                    catch (Exception ex)
                                    {
                                        throw ex;
                                    }
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                //Lead Assign the Organization
                try
                {
                    var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
                    foreach (var leadid in input.LeadIds)
                    {
                        try
                        {
                            var lead = await _leadRepository.FirstOrDefaultAsync(leadid);
                            //var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == lead.ChanelPartnerID).FirstOrDefault();
                            //var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationUnitId).Select(e => e.DisplayName).FirstOrDefault();
                            lead.LastModificationTime = DateTime.UtcNow;
                            lead.OrganizationUnitId = (int)input.OrganizationID;
                            await _leadRepository.UpdateAsync(lead);

                            LeadActivityLogs leadactivity = new LeadActivityLogs();
                            leadactivity.LeadActionId = 3;
                            leadactivity.SectionId = 29;
                            leadactivity.OrganizationUnitId = (int)input.OrganizationID;
                            leadactivity.LeadActionNote = "Lead Assign To " + OrganizationName + " Organization";
                            leadactivity.LeadId = lead.Id;
                            if (AbpSession.TenantId != null)
                            {
                                leadactivity.TenantId = (int)AbpSession.TenantId;
                            }

                            leadactivity.CreatorUserId = AbpSession.UserId;
                            await _leadactivityRepository.InsertAsync(leadactivity);

                            var activity = new LeadActivityLogs();
                            activity.LeadId = lead.Id;
                            activity.LeadActionId = 1;
                            if (AbpSession.TenantId != null)
                            {
                                activity.TenantId = (int)AbpSession.TenantId;
                            }
                            activity.LeadActionNote = "Lead Assign From " + OrganizationName + " Organization";
                            await _leadactivityRepository.InsertAsync(activity);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }


                        //var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
                        //string msg = string.Format("New Lead Created {0}.", lead.CustomerName);
                        //await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }

                //    var lead = _leadRepository.GetAll().Where(e => e.Id == input.LeadIds).FirstOrDefault();
                //lead.Id = 0;
                ////1. Create
                ////2. Excel Import
                ////3. External Link
                ////4. Copy Lead
                ////lead.IsExternalLead = 4;  -- check
                //lead.OrganizationUnitId = input.OrganizationID;
                //lead.CreatorUserId = (int)AbpSession.UserId;
                //await _leadRepository.InsertAndGetIdAsync(lead);

                //ExternalLeads extLead = new ExternalLeads();
                //extLead.LeadId = lead.Id;
                //extLead.CreatorUserId = (int)AbpSession.UserId;
                //extLead.IsExternalLead = 4;
                //extLead.IsPromotion = true;
                //await _externalLeadRepository.InsertAndGetIdAsync(extLead);


            }
        }

        //transfer the lead other organization
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Assign, AppPermissions.Pages_Tenant_Leads_ManageLead_Assign)]
        public async Task TransferLeadToOtherOrganization(GetLeadForAssignOrTransferOutput input)
        {
            var User_List = _userRepository.GetAll().ToList();
            var ChangedOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationID).Select(e => e.DisplayName).FirstOrDefault();
            var UserDetail = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            var assignedToUser = _userRepository.GetAll().Where(u => u.Id == input.AssignToUserID).FirstOrDefault();
            if (!string.IsNullOrEmpty(ChangedOrganizationName) && assignedToUser != null)
            {
                foreach (var leadid in input.LeadIds)
                {
                    var lead = await _leadRepository.FirstOrDefaultAsync(leadid);
                    var assignedFromUser = _userRepository.GetAll().Where(u => u.Id == lead.ChanelPartnerID).FirstOrDefault();
                    var PreviousOrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == lead.OrganizationUnitId).Select(e => e.DisplayName).FirstOrDefault();
                    lead.CreationTime = DateTime.UtcNow;
                    lead.LeadStatusId = 10;
                    lead.OrganizationUnitId = (int)input.OrganizationID;
                    await _leadRepository.UpdateAsync(lead);

                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 3;
                    leadactivity.SectionId = 0;
                    leadactivity.LeadActionNote = "Lead Assign To " + assignedToUser.FullName + " In " + ChangedOrganizationName + "Organization From " + assignedFromUser.FullName + ", " + PreviousOrganizationName + " Organization";
                    leadactivity.LeadId = lead.Id;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }

                    leadactivity.CreatorUserId = AbpSession.UserId;
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }


                //var TeamId = _userTeamRepository.GetAll().Where(e => e.UserId == input.AssignToUserID).Select(e => e.TeamId).FirstOrDefault();

                var UserList = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()

                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()

                                    //join ut in _userTeamRepository.GetAll() on user.Id equals ut.UserId into utJoined
                                    //from ut in utJoined.DefaultIfEmpty()

                                where us != null && us.DisplayName == "Manager" //&& ut != null && ut.TeamId == TeamId
                                select user);

                foreach (var item in UserList)
                {
                    if (item.Id == assignedToUser.Id)
                    {
                        string msg = string.Format("{0} New Lead Assigned To You", input.LeadIds.Count);
                        await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                    }
                    else
                    {
                        string msg = string.Format("{0} New Lead Assigned To {0}", input.LeadIds.Count, assignedToUser.FullName);
                        await _appNotifier.LeadAssiged(item, msg, NotificationSeverity.Info);
                    }
                }
            }

        }


        /// <summary>
        /// Lead Status Change
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Edit, AppPermissions.Pages_Tenant_Leads_ManageLead_Edit)]  //AppPermissions.Pages_MyLeads
        public virtual async Task ChangeStatus(GetLeadForChangeStatusOutput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);

            //PreviousJobStatus jobStstus = new PreviousJobStatus();
            //jobStstus.LeadId = lead.Id;
            //if (AbpSession.TenantId != null)
            //{
            //    jobStstus.TenantId = (int)AbpSession.TenantId;
            //}
            //jobStstus.CurrentID = (int)input.LeadStatusID;
            //jobStstus.PreviousId = lead.LeadStatusId;
            //await _previousJobStatusRepository.InsertAsync(jobStstus);

            //change the Lead Status
            lead.LeadStatusId = (int)input.LeadStatusID;
            if (lead != null && lead.LeadStatusId != 0)
            {
                await _leadRepository.UpdateAsync(lead);

                //reseason not inserted
                CancelRejectLeadReason leadreason = new CancelRejectLeadReason();
                if (!input.RejectReason.IsNullOrEmpty())
                {
                    leadreason.Notes = input.RejectReason;
                    leadreason.LeadId = input.Id;
                    leadreason.CancelReasonsLeadId = input.CancelReasonId;
                    leadreason.RejectReasonLeadId = input.RejectReasonId;

                    await _cancelRejectLeadReasonRepository.InsertAsync(leadreason);
                }

                //add the reason activity log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 5;// change status input.LeadActionID; // 5;
                leadactivity.SectionId = 35;
                leadactivity.LeadAcitivityID = input.LeadActivityID;
                if (input.RejectReasonId != null || input.RejectReasonId != 0)
                {
                    leadactivity.LeadActionNote = "Customer Rejected, Reason : " + input.RejectReason;
                }
                else if (input.CancelReasonId != null || input.CancelReasonId != 0)
                {
                    leadactivity.LeadActionNote = "Customer Canceled, Reason : " + input.RejectReason;
                }
                else
                {
                    leadactivity.LeadActionNote = "Customer Status Changed";
                }

                leadactivity.LeadId = lead.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }

        public async Task CreateOrEdit(CreateOrEditLeadsDto input)
        {

            //var client = new RestClient("https://insprl.com/api/sms/send-msg");
            //var request = new RestRequest(Method.Post);
            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            //request.AddHeader("Authorization", "{CLIENT_KEY}");
            //request.AddParameter("clientid", "{CLIENT_ID}");
            //request.AddParameter("sender_id", "{6_CHAR_SENDER_ID}");
            //request.AddParameter("mobile", "9999999999");
            ////request.AddParameter(message", "Hi Customer, This is an alert Message");
            ////request.AddParameter(schedule_date", "03 - 02 - 2020");
            ////request.AddParameter(schedule_time", "13:20");
            ////request.AddParameter(unicode", "0");
            ////request.AddParameter(flash", "0");
            //request.AddParameter("campaign_name", "My campaign 2020 Jan");
            //IRestResponse response = client.Execute(request);

            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        /// <summary>
        /// Delete Multiple selected Customers Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Delete, AppPermissions.Pages_Tenant_Leads_ManageLead_Delete)]
        public async Task DeleteCustomer(EntityDto input)
        {
            await _leadRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 35; // Manage Lead Pages Name
            dataVaulteActivityLog.ActionId = 3; // Deleted
            dataVaulteActivityLog.ActionNote = "Customer Lead Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        /// <summary>
        /// Delete Multiple selected Manage Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Delete, AppPermissions.Pages_Tenant_Leads_ManageLead_Delete)]
        public async Task DeleteManageLead(int[] input)
        {
            foreach (var item in input)
            {
                try
                {
                    var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == item).Select(e => e.LeadId).FirstOrDefault();

                    if (JobLeadId > 0)
                    {
                        await _leadRepository.DeleteAsync(x => x.Id == item);
                        await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        //await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                        //await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                        await _jobRepository.DeleteAsync((int)JobLeadId);


                        //Add Activity Log
                        DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                        if (AbpSession.TenantId != null)
                        {
                            dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                        }
                        dataVaulteActivityLog.SectionId = 29; // Manage Lead Pages Name
                        dataVaulteActivityLog.ActionId = 3; // Deleted
                        dataVaulteActivityLog.ActionNote = "Manage Lead Deleted";
                        dataVaulteActivityLog.SectionValueId = item;
                        await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }

                await _leadRepository.DeleteAsync(item);
            }
        }

        /// <summary>
        /// Delete Duplicate Lead
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_ManageLead_Delete, AppPermissions.Pages_Tenant_Leads_ManageLead)]
        public async Task DeleteDuplicateLeads(EntityDto input)
        {
            var lead = _leadRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefault(); //await _leadRepository.GetAsync(input.Id);

            var duplicateLead = (_leadRepository.GetAll().Where(e => ((e.EmailId == lead.EmailId || e.MobileNumber == lead.MobileNumber) && (!string.IsNullOrEmpty(e.MobileNumber) && !string.IsNullOrEmpty(e.EmailId))) && e.Id != lead.Id && e.ChanelPartnerID != null && e.OrganizationUnitId == lead.OrganizationUnitId));
            //_leadRepository.GetAll().Where(e => (e.Phone == lead.Phone || e.Mobile == lead.Mobile || e.Email == lead.Email) && e.Id != lead.Id && e.AssignToUserID != null);

            foreach (var item in duplicateLead)
            {
                try
                {

                    //await _leadRepository.DeleteAsync(x => x.Id == item.Id);
                    //await _dbContextProvider.GetDbContext().leads.AddRangeAsync(item);
                    //await _dbContextProvider.GetDbContext().SingleDeleteAsync(item);
                    //Add Activity Log
                    DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                    if (AbpSession.TenantId != null)
                    {
                        dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                    }
                    dataVaulteActivityLog.SectionId = 29; // Manage Lead Pages Name
                    dataVaulteActivityLog.ActionId = 3; // Deleted
                    dataVaulteActivityLog.ActionNote = "Manage Duplicated Lead Deleted";
                    dataVaulteActivityLog.SectionValueId = item.Id;
                    await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);

                    //var JobLeadId = _jobRepository.GetAll().Where(e => e.LeadId == item.Id).Select(e => e.LeadId).FirstOrDefault();

                    //if (JobLeadId > 0)
                    //{
                    //    await _jobProductItemRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                    //    //await _jobPromotionRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);
                    //    //await _jobVariationRepository.DeleteAsync(x => x.JobId == (int)JobLeadId);

                    //    await _jobRepository.DeleteAsync((int)JobLeadId);

                    //}
                }
                catch (Exception e)
                {
                    throw e;
                }

                await _leadRepository.DeleteAsync(item.Id);
            }
        }


        public async Task<PagedResultDto<GetLeadsForViewDto>> GetAll(GetAllLeadsInput input)
        {
            var filteredLeads = _leadRepository.GetAll()
                .Include(e => e.LeadStatusIdFk)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.CustomerName.Contains(input.Filter) || e.EmailId.Contains(input.Filter) || e.MobileNumber.Contains(input.Filter) || e.Alt_Phone.Contains(input.Filter) || e.AddressLine1.Contains(input.Filter) || e.AddressLine2.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.FirstNameFilter), e => e.FirstName == input.FirstNameFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.EmailId == input.EmailFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Alt_Phone == input.PhoneFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.MobileNumber == input.MobileFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.AddressLine1 == input.AddressFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateIdFk.Name == input.StateNameFilter);

            var pagedAndFilteredLeads = filteredLeads.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var leads = (from o in pagedAndFilteredLeads
                         join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
                         from s3 in j3.DefaultIfEmpty()

                             //join o2 in _organizationUnitRepository.GetAll() on o.OrganizationId equals o2.Id into j2
                             //from s2 in j2.DefaultIfEmpty()
                         select new GetLeadsForViewDto()
                         {
                             Leads = new LeadsDto
                             {
                                 FirstName = o.FirstName,
                                 MiddleName = o.MiddleName,
                                 LastName = o.LastName,
                                 EmailId = o.EmailId,
                                 MobileNumber = o.MobileNumber,
                                 CustomerName = o.CustomerName,
                                 Address = o.AddressLine1 + "," + o.AddressLine2,

                                 StateName = o.StateIdFk.Name,
                                 DistrictName = o.DistrictIdFk.Name,
                                 TalukaName = o.TalukaIdFk.Name,
                                 CityName = o.CityIdFk.Name,
                                 LeadTypeName = o.LeadTypeIdFk.Name,
                                 LeadSourceName = o.LeadSourceIdFk.Name,
                                 SolarTypeName = o.SolarTypeIdFk.Name,
                                 HeightStructureName = o.HeightStructureIdFk.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreationTime = o.CreationTime
                             },
                             //Id = o.Id,
                             //DublicateLeadId = _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.OrganizationId == input.OrganizationUnit && e.Id != o.Id && e.AssignToUserID != null).OrderByDescending(e => e.Id).Select(e => e.Id).FirstOrDefault(),
                             //LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
                             ////DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                             //Duplicate = (o.IsDuplicate == false) ? false : (o.IsDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.OrganizationId == input.OrganizationUnit && e.AssignToUserID != null).Any(),
                             ////Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                             //WebDuplicate = (o.IsWebDuplicate == false) ? false : (o.IsWebDuplicate == true) ? true : _leadRepository.GetAll().Where(e => ((e.Email == o.Email && !string.IsNullOrEmpty(e.Email)) || (e.Mobile == o.Mobile && !string.IsNullOrEmpty(e.Mobile))) && e.Id != o.Id && e.AssignToUserID == null && e.OrganizationId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Any(),
                         });

            var totalCount = await filteredLeads.CountAsync();

            return new PagedResultDto<GetLeadsForViewDto>(totalCount, await leads.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_ManageLead_Edit, AppPermissions.Pages_Tenant_Leads_Customer_Edit)]
        public virtual async Task ChangeDuplicateStatus(GetLeadForChangeDuplicateStatusOutput input)
        {
            var lead = await _leadRepository.FirstOrDefaultAsync((int)input.Id);
            lead.IsDuplicate = input.IsDuplicate;
            lead.ChangeStatusDate = DateTime.UtcNow;
            lead.IsWebDuplicate = input.IsWebDuplicate;
            await _leadRepository.UpdateAsync(lead);

            LeadActivityLogs leadactivity = new LeadActivityLogs();
            if (input.IsDuplicate != null)
            {
                leadactivity.LeadActionId = 5;
                if (input.IsDuplicate == true)
                {
                    leadactivity.LeadActionNote = "Lead Status Changed To Database Duplicate";
                }
                else
                {
                    leadactivity.LeadActionNote = "Lead Status Changed To Web Duplicate";
                }
            }
            else
            {
                leadactivity.LeadActionId = 5;
                if (input.IsDuplicate == true)
                {
                    leadactivity.LeadActionNote = "Lead Status Changed To Not Database Duplicate";
                }
                else
                {
                    leadactivity.LeadActionNote = "Lead Status Changed To Not Web Duplicate";
                }
            }
            leadactivity.SectionId = 35;
            leadactivity.LeadId = lead.Id;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }

        /// <summary>
        /// Change Duplicate Status for multiple Leads
        /// </summary>
        /// <param name="input">Lead Id[], IsDuplicate - True(Web Dup) - False(DataBase Dup)</param>
        /// <returns></returns>

        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_ManageLead_Edit, AppPermissions.Pages_Tenant_Leads_Customer_Edit)]
        public virtual async Task ChangeDuplicateStatusForMultipleLeads(GetLeadForChangeDuplicateStatusOutput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);
                lead.IsDuplicate = input.IsDuplicate;
                lead.ChangeStatusDate = DateTime.UtcNow;
                lead.IsWebDuplicate = input.IsWebDuplicate;
                await _leadRepository.UpdateAsync(lead);

                LeadActivityLogs leadactivity = new LeadActivityLogs();
                if (input.IsDuplicate != null)
                {
                    leadactivity.LeadActionId = 5;
                    if (input.IsDuplicate == true)
                    {
                        leadactivity.LeadActionNote = "Lead Status Changed To Database Duplicate";
                    }
                    else
                    {
                        leadactivity.LeadActionNote = "Lead Status Changed To Web Duplicate";
                    }
                }
                else
                {
                    leadactivity.LeadActionId = 5;
                    if (input.IsDuplicate == true)
                    {
                        leadactivity.LeadActionNote = "Lead Status Changed To Not Database Duplicate";
                    }
                    else
                    {
                        leadactivity.LeadActionNote = "Lead Status Changed To Not Web Duplicate";
                    }
                }
                leadactivity.LeadId = lead.Id;
                leadactivity.SectionId = 0;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
        }


        /// <summary>
        /// Get Duplicate Lead Details (Email or Phone)
        /// </summary>
        /// <param name="id">Lead Id</param>
        /// <returns></returns>
        public async Task<List<GetDuplicateLeadPopupDto>> GetDuplicateLeadForView(int id, int orgId)
        {
            var lead = await _leadRepository.GetAsync(id);

            var duplicateLead = _leadRepository.GetAll().Where(e => ((e.EmailId == lead.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == lead.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.OrganizationUnitId == orgId && e.Id != lead.Id && e.ChanelPartnerID != null);

            var leads = from o in duplicateLead
                        join o1 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o1.Id into j1
                        from s1 in j1.DefaultIfEmpty()
                        join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                        from s2 in j2.DefaultIfEmpty()

                            //join o3 in _jobRepository.GetAll() on o.Id equals o3.LeadId into j3
                            //from s3 in j3.DefaultIfEmpty()

                            //join o4 in _lookup_jobStatusRepository.GetAll() on s3.JobStatusId equals o4.Id into j4
                            //from s4 in j4.DefaultIfEmpty()

                        select new GetDuplicateLeadPopupDto()
                        {
                            CreatedByName = s2.FullName, //_userRepository.GetAll().Where(e => e.Id == o.CreatorUserId).Select(e => e.FullName).FirstOrDefault(),
                            CreationTime = o.CreationTime,
                            Address = (o.AddressLine1 == null ? "" : o.AddressLine1 + ",") + (o.AddressLine2 == null ? "" : o.AddressLine2 + ","),
                            CustomerName = o.CustomerName,
                            EmailId = o.EmailId,
                            Id = o.Id,
                            Alt_PhoneNumber = o.Alt_Phone,
                            MobileNumber = o.MobileNumber,
                            LeadSource = o.LeadSourceIdFk.Name,
                            LeadStatus = s1.Status,
                            CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                            //UnitNo = o.UnitNo,
                            //UnitType = o.UnitType,
                            //StreetNo = o.StreetNo,
                            //StreetType = o.StreetType,
                            //StreetName = o.StreetName,
                            //Suburb = o.Suburb,
                            State = o.StateIdFk.Name,
                            Postcode = Convert.ToString(o.Pincode),
                            //ProjectStatus = s3.JobStatusFk.Name,
                            //ProjectOpenDate = s3.CreationTime,
                            //ProjectNo = s3.JobNumber,
                            LastFollowupDate = _leadactivityRepository.GetAll().Where(e => e.LeadActionId == 8 && e.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => (DateTime?)e.CreationTime).FirstOrDefault(),
                            Description = _leadactivityRepository.GetAll().Where(e => e.LeadActionId == 8 && e.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.LeadActionNote).FirstOrDefault(),
                            //LastQuoteDate = _quotationRepository.GetAll().Where(e => e.JobFk.LeadId == o.Id).OrderByDescending(x => x.Id).Select(e => e.CreationTime).FirstOrDefault()

                        };

            return new List<GetDuplicateLeadPopupDto>(
                await leads.ToListAsync()
            );
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_ManageLead_Edit, AppPermissions.Pages_Tenant_Leads_Customer_Edit)]
        public async Task CopyLead(int OId, int LeadId)
        {
            try
            {
                var OrganizationName = _organizationUnitRepository.GetAll().Where(e => e.Id == OId).Select(e => e.DisplayName).FirstOrDefault();
                var lead = _leadRepository.GetAll().Where(e => e.Id == LeadId).FirstOrDefault();
                lead.Id = 0;
                //1. Create
                //2. Excel Import
                //3. External Link
                //4. Copy Lead
                //lead.IsExternalLead = 4;  -- check
                lead.OrganizationUnitId = OId;
                lead.CreatorUserId = (int)AbpSession.UserId;
                await _leadRepository.InsertAndGetIdAsync(lead);

                ExternalLeads extLead = new ExternalLeads();
                extLead.LeadId = lead.Id;
                extLead.CreatorUserId = (int)AbpSession.UserId;
                extLead.IsExternalLead = 4;
                extLead.IsPromotion = true;
                await _externalLeadRepository.InsertAndGetIdAsync(extLead);

                var activity = new LeadActivityLogs();
                activity.LeadId = lead.Id;
                activity.LeadActionId = 1;
                if (AbpSession.TenantId != null)
                {
                    activity.TenantId = (int)AbpSession.TenantId;
                }
                activity.LeadActionNote = "Lead Copy From " + OrganizationName + " Organization";
                await _leadactivityRepository.InsertAsync(activity);

                var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == lead.CreatorUserId).FirstOrDefault();
                string msg = string.Format("New Lead Created {0}.", lead.CustomerName);
                await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_ManageLead_Edit, AppPermissions.Pages_Tenant_Leads_Customer_Edit)]
        public bool CheckCopyExist(int OId, int? LeadId)
        {
            var Result = false;

            var lead = _leadRepository.GetAll().Where(e => e.Id == LeadId).FirstOrDefault();
            var extLead = _externalLeadRepository.GetAll().Where(e => e.LeadId == LeadId).FirstOrDefault();
            //this external Lead Panding
            if (extLead != null)
            {
                LeadId = extLead.LeadId;
            }
            Result = _leadRepository.GetAll().Where(e => e.Id == LeadId && e.OrganizationUnitId == OId && e.Id != lead.Id && e.EmailId == lead.EmailId && e.Alt_Phone == lead.Alt_Phone && e.MobileNumber == lead.MobileNumber).Any();

            return Result;
        }
        // <summary>
        /// Main Search Data API
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //public async Task<PagedResultDto<GetLeadsForViewDto>> GetSearchData(GetAllLeadsInput input)
        //{
        //    var orglist = _UserWiseEmailOrgRepository.GetAll().Where(e => e.UserId == AbpSession.UserId).Select(e => e.OrganizationUnitId).ToList();
        //    var filtertext = "";
        //    var JobList = new List<int?>();
        //    if (input.Filter != null && input.Filter.Contains("|"))
        //    {
        //        string[] splitPipeValue = input.Filter.Split('|');

        //        if (splitPipeValue.Length > 0)
        //        {
        //            var jobnumber = splitPipeValue[0].ToString();
        //            if (jobnumber != "")
        //            {
        //                filtertext = splitPipeValue[0].ToString();
        //                JobList = _jobRepository.GetAll().Where(j => j.JobNumber.Contains(filtertext)).Select(e => e.LeadId).ToList();
        //            }
        //            else
        //            {
        //                filtertext = splitPipeValue[1].ToString();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        filtertext = input.Filter;
        //    }
        //    var filteredLeads = _leadRepository.GetAll()
        //                .WhereIf(!string.IsNullOrWhiteSpace(filtertext), e => e.Email.Contains(filtertext) || e.Phone.Contains(filtertext) || e.Mobile.Contains(filtertext) || e.CompanyName.Contains(filtertext) || e.Address.Contains(filtertext) || JobList.Contains(e.Id))
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter),e => JobList.Contains(e.Id))
        //                .Where(e => orglist.Contains(e.OrganizationId));

        //    var pagedAndFilteredLeads = filteredLeads
        //        .PageBy(input);

        //    var leads = from o in pagedAndFilteredLeads

        //                join o1 in _jobRepository.GetAll() on o.Id equals o1.LeadId into j1
        //                from s1 in j1.DefaultIfEmpty()

        //                join o2 in _lookup_jobStatusRepository.GetAll() on s1.JobStatusId equals o2.Id into j2
        //                from s2 in j2.DefaultIfEmpty()

        //                select new GetLeadForViewDto()
        //                {
        //                    Lead = new LeadDto
        //                    {
        //                        CompanyName = o.CompanyName,
        //                        Email = o.Email,
        //                        Phone = o.Phone,
        //                        Mobile = o.Mobile,
        //                        Address = o.Address,
        //                        Requirements = o.Requirements,
        //                        Id = o.Id,
        //                        PostCode = o.PostCode,
        //                        LeadStatusID = o.LeadStatusId,
        //                        Suburb = o.Suburb,
        //                        LeadSource = o.LeadSource,
        //                        State = o.State,
        //                    },
        //                    JobNumber = s1.JobNumber,
        //                    JobStatus = s2.Name,
        //                    JobCreatedDate = s1.CreationTime

        //                };
        //    var totalCount = await filteredLeads.CountAsync();

        //    return new PagedResultDto<GetLeadForViewDto>(
        //        totalCount,
        //        leads.DistinctBy(e => e.Lead.Id).ToList()
        //    );
        //}

        ///// <summary>
        ///// Lead Excel Export
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //public async Task<FileDto> GetLeadsToExcel(GetAllLeadsForExcelInput input)
        //{

        //    //var filteredLeads = _leadRepository.GetAll()
        //    //            //.Include(e => e.SuburbFk)
        //    //            //.Include(e => e.StateFk)
        //    //            .Include(e => e.LeadStatusFk)
        //    //            //.Include(e => e.LeadSourceFk)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CompanyName.Contains(input.Filter) || e.Email.Contains(input.Filter) || e.Phone.Contains(input.Filter) || e.Mobile.Contains(input.Filter) || e.Address.Contains(input.Filter) || e.Requirements.Contains(input.Filter))
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.CopanyNameFilter), e => e.CompanyName == input.CopanyNameFilter)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.Email == input.EmailFilter)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone == input.PhoneFilter)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.Mobile == input.MobileFilter)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address == input.AddressFilter)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
        //    //            //.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.SuburbFk != null && e.SuburbFk.Suburb == input.PostCodeSuburbFilter)
        //    //            //.WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateFk != null && e.StateFk.Name == input.StateNameFilter)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.PostCode == input.PostCodeFilter)
        //    //            .WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter);
        //    ////.WhereIf(!string.IsNullOrWhiteSpace(input.LeadSourceNameFilter), e => e.LeadSourceFk != null && e.LeadSourceFk.Name == input.LeadSourceNameFilter);

        //    var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
        //    var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

        //    //var jobnumberlist = new List<int?>();
        //    //jobnumberlist = _jobRepository.GetAll().Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();

        //    var filteredLeads = _leadRepository.GetAll()
        //                .Include(e => e.LeadStatusIdFk)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CustomerName.Contains(input.Filter) || e.EmailId.Contains(input.Filter) || e.Alt_Phone.Contains(input.Filter) || e.MobileNumber.Contains(input.Filter) || e.AddressLine1.Contains(input.Filter)) // || jobnumberlist.Contains(e.Id))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.CustomerNameFilter), e => e.CustomerName == input.CustomerNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.EmailFilter), e => e.EmailId == input.EmailFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Alt_Phone == input.PhoneFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.MobileFilter), e => e.MobileNumber == input.MobileFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.AddressLine1 == input.AddressFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.RequirementsFilter), e => e.Requirements == input.RequirementsFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeSuburbFilter), e => e.Suburb == input.PostCodeSuburbFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.StreetNameFilter), e => e.StreetName == input.StreetNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.StateNameFilter), e => e.StateIdFk.Name == input.StateNameFilter)
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.PostCodeFilter), e => e.Pincode == Convert.ToInt32(input.PostCodeFilter))
        //                .WhereIf(input.LeadSourceIdFilter != null && input.LeadSourceIdFilter.Count() > 0, e => input.LeadSourceIdFilter.Contains((int)e.LeadSourceId))
        //                .WhereIf(!string.IsNullOrWhiteSpace(input.LeadStatusName), e => e.LeadStatusIdFk != null && e.LeadStatusIdFk.Status == input.LeadStatusName)
        //                .WhereIf(input.LeadStatusId != null, e => e.LeadStatusId == input.LeadStatusId)
        //                .WhereIf(input.StartDate != null, e => e.CreationTime.Date >= SDate.Value.Date)
        //                .WhereIf(input.EndDate != null, e => e.CreationTime.Date <= EDate.Value.Date)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.TypeNameFilter), e => e.Type == input.TypeNameFilter)
        //                //.WhereIf(!string.IsNullOrWhiteSpace(input.AreaNameFilter), e => e.Area == input.AreaNameFilter)
        //                .WhereIf(input.OrganizationUnit != null, e => e.OrganizationUnitId == input.OrganizationUnit)
        //                //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
        //                .WhereIf(input.DuplicateFilter == "web", o => (_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.ChanelPartnerID == null && e.OrganizationUnitId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0))
        //                //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
        //                .WhereIf(input.DuplicateFilter == "db", o => (_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID != null).Count() > 0))
        //                //Not Duplicate Leads
        //                .WhereIf(input.DuplicateFilter == "no", o => !(_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.ChanelPartnerID == null && e.OrganizationUnitId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0) && !(_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID != null).Count() > 0))
        //                .Where(e => e.ChanelPartnerID == null)
        //                .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);

        //    var query = (from o in filteredLeads
        //                     //join o1 in _lookup_postCodeRepository.GetAll() on o.Suburb equals o1.Id into j1
        //                     //from s1 in j1.DefaultIfEmpty()

        //                     //join o2 in _lookup_stateRepository.GetAll() on o.State equals o2.Id into j2
        //                     //from s2 in j2.DefaultIfEmpty()

        //                 join o3 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o3.Id into j3
        //                 from s3 in j3.DefaultIfEmpty()

        //                     //join o4 in _lookup_leadSourceRepository.GetAll() on o.LeadSource equals o4.Id into j4
        //                     //from s4 in j4.DefaultIfEmpty()

        //                 select new GetLeadsForViewDto()
        //                 {
        //                     Leads = new LeadsDto
        //                     {
        //                         CustomerName = o.CustomerName,
        //                         EmailId = o.EmailId,
        //                         Alt_PhoneNumber = o.Alt_Phone,
        //                         MobileNumber = o.MobileNumber,
        //                         Address = o.AddressLine1,
        //                        // Requirements = o.Requirements,
        //                         Id = o.Id,
        //                         Pincode = o.Pincode
        //                     },
        //                     //PostCodeSuburb = s1 == null || s1.Suburb == null ? "" : s1.Suburb.ToString(),
        //                     //StateName = s2 == null || s2.Name == null ? "" : s2.Name.ToString(),
        //                     LeadStatusName = s3 == null || s3.Status == null ? "" : s3.Status.ToString(),
        //                     //LeadSourceName = s4 == null || s4.Name == null ? "" : s4.Name.ToString(),
        //                 });


        //    var leadListDtos = await query.ToListAsync();
        //    if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
        //    {
        //        return _leadsExcelExporter.ExportToFile(leadListDtos);
        //    }
        //    else
        //    {
        //        return _leadsExcelExporter.ExportCsvToFile(leadListDtos);
        //    }
        //}


        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Edit, AppPermissions.Pages_Tenant_Leads_ManageLead_Edit)]
        public async Task<GetLeadsForEditOutput> GetLeadsForEdit(EntityDto input)
        {
            var leads = await _leadRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadsForEditOutput { leads = ObjectMapper.Map<CreateOrEditLeadsDto>(leads) };

            return output;
        }


        public async Task<GetLeadsForViewDto> GetLeadsForView(int id)
        {
            var leads = await _leadRepository.GetAsync(id); //await _leadRepository.GetAll().Where(r => r.Id == id).FirstOrDefaultAsync(); //


            var output = new GetLeadsForViewDto { Leads = ObjectMapper.Map<LeadsDto>(leads) };


            var result = _leadRepository.GetAll().Where(x => x.Id == id).Select(e => new
            {
                SolarTypeName = e.SolarTypeIdFk.Name,
                cityName = e.CityIdFk.Name,
                talukaName = e.TalukaIdFk.Name,
                districtName = e.DistrictIdFk.Name,
                stareName = e.StateIdFk.Name
            }).FirstOrDefault();

            // var ttt =  result.LeadSourceName;

            output.Leads.Address = (leads.AddressLine1 == null ? "" : leads.AddressLine1 + ",")
                + (leads.AddressLine2 == null ? "" : leads.AddressLine2 + ",")
                + (result.cityName == null ? "" : result.cityName + "")
                + (result.talukaName == null ? "" : result.talukaName + ",")
                + (result.districtName == null ? "" : result.districtName + ",")
                + (result.stareName == null ? "" : result.stareName + ",")
                + (leads.Pincode == 0 ? null : Convert.ToString(leads.Pincode));
            //leads.AddressLine1 + "," + leads.AddressLine2 + ",Aslali,Nikol,Ahmedabad,Gujrat, 382360"; //+ leads.CityIdFk.Name + "," + leads.TalukaIdFk.Name + "," + leads.DistrictIdFk.Name + "," + leads.Pincode;
            output.Leads.SolarTypeName = result.SolarTypeName; // leads.SolarTypeIdFk.Name;
            var user = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            if (output.Leads.LeadStatusID != 0)
            {
                var _lookupLeadStatus = await _lookup_leadStatusRepository.FirstOrDefaultAsync((int)output.Leads.LeadStatusID);
                output.LeadStatusName = _lookupLeadStatus?.Status?.ToString();
                output.LeadStatusColorClass = _lookupLeadStatus?.LeadStatusColorClass?.ToString();
                output.LeadStatusIconClass = _lookupLeadStatus?.LeadStatusIconClass?.ToString();
                //output.LeadSourcesColorClass
            }

            if (leads.CreatorUserId != null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)leads.CreatorUserId);
                output.CreatedByName = _lookupUserStatus?.FullName?.ToString();
            }

            if (leads.ChanelPartnerID != null)
            {
                var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)leads.ChanelPartnerID);
                output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                output.CurrentAssignUserEmail = _lookupUserStatus?.EmailAddress?.ToString();
                output.CurrentAssignUserMobile = _lookupUserStatus?.PhoneNumber?.ToString();
            }
            else
            {
                var DupLead = _leadRepository.GetAll().Where(e => (e.EmailId == leads.EmailId || e.MobileNumber == leads.MobileNumber) && e.Id != leads.Id && e.OrganizationUnitId == leads.OrganizationUnitId && e.ChanelPartnerID != null).FirstOrDefault();
                if (DupLead != null)
                {
                    var _lookupUserStatus = await _userRepository.FirstOrDefaultAsync((int)DupLead.ChanelPartnerID);
                    output.CurrentAssignUserName = _lookupUserStatus?.FullName?.ToString();
                }
            }

            output.CreatedOn = leads.CreationTime;
            output.latitude = leads.Latitude;
            output.longitude = leads.Longitude;
            output.UserName = user.Name;
            output.UserEmail = user.EmailAddress;
            output.UserPhone = user.PhoneNumber;
            //output.OrgName = org.Where(e => e.Id == leads.OrganizationUnitId).Select(e => e.DisplayName).FirstOrDefault();
            //output.OrgMobile = org.Where(e => e.Id == leads.OrganizationUnitId).Select(e => e.Mobile).FirstOrDefault();
            //output.OrgEmail = org.Where(e => e.Id == leads.OrganizationUnitId).Select(e => e.Email).FirstOrDefault();
            //output.OrgLogo = org.Where(e => e.Id == leads.OrganizationUnitId).Select(e => e.LogoFilePath).FirstOrDefault();
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_ExportToExcel)]
        public async Task<FileDto> GetLeadsToExcel(GetAllLeadsForExcelInput input)
        {
            try
            {
                var filteredLeads = _leadRepository.GetAll()
                      .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CustomerName.Contains(input.Filter))
                        //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "web", o => (_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.ChanelPartnerID == null && e.OrganizationUnitId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0))
                        //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                        .WhereIf(input.DuplicateFilter == "db", o => (_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID != null).Count() > 0))
                        //Not Duplicate Leads
                        .WhereIf(input.DuplicateFilter == "no", o => !(_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.ChanelPartnerID == null && e.OrganizationUnitId == input.OrganizationUnit && e.IsDuplicate != true && e.IsWebDuplicate != true).Count() > 0) && !(_leadRepository.GetAll().Where(e => ((e.EmailId == o.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == o.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber))) && e.Id != o.Id && e.OrganizationUnitId == input.OrganizationUnit && e.ChanelPartnerID != null).Count() > 0))
                        .Where(e => e.ChanelPartnerID == null)
                        .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true && e.OrganizationUnitId == input.OrganizationUnit);

                var query = (from o in filteredLeads
                             select new GetLeadsForViewDto()
                             {
                                 Leads = new LeadsDto
                                 {
                                     FirstName = o.FirstName,
                                     MiddleName = o.MiddleName,
                                     LastName = o.LastName,
                                     EmailId = o.EmailId,
                                     MobileNumber = o.MobileNumber,
                                     CustomerName = o.CustomerName,
                                     Address = o.AddressLine1 + "," + o.AddressLine2,
                                     StateName = o.StateIdFk.Name,
                                     DistrictName = o.DistrictIdFk.Name,
                                     TalukaName = o.TalukaIdFk.Name,
                                     CityName = o.CityIdFk.Name,
                                     LeadTypeName = o.LeadTypeIdFk.Name,
                                     LeadSourceName = o.LeadSourceIdFk.Name,
                                     SolarTypeName = o.SolarTypeIdFk.Name,
                                     HeightStructureName = o.HeightStructureIdFk.Name,
                                     IsActive = o.IsActive,
                                     Id = o.Id
                                 }
                             });

                var leadListDtos = await query.ToListAsync();
                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _leadsExcelExporter.ExportCsvToFile(leadListDtos);
                }
                else
                {
                    return _leadsExcelExporter.ExportToFile(leadListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Create, AppPermissions.Pages_Tenant_Leads_ManageLead_Create)]
        protected virtual async Task Create(CreateOrEditLeadsDto input)
        {
            try
            {
                var leads = ObjectMapper.Map<Leads>(input);

                if (AbpSession.TenantId != null)
                {
                    leads.TenantId = (int?)AbpSession.TenantId;
                }
                leads.IsVerified = false;
                if (!string.IsNullOrEmpty(leads.FirstName) && !string.IsNullOrEmpty(leads.LastName) && !string.IsNullOrEmpty(leads.MobileNumber) &&
                    leads.SolarTypeId != 0 && leads.ConsumerNumber != null && !string.IsNullOrEmpty(leads.AddressLine1) && !string.IsNullOrEmpty(leads.AddressLine2) &&
                    leads.CityId != null && leads.StateId != null && leads.DistrictId != null && leads.StateId != null && leads.Pincode != null && leads.SubDivisionId != null && leads.DivisionId != null &&
                    leads.CircleId != null && leads.DiscomId != null && leads.Latitude != null && leads.Longitude != null && leads.RequiredCapacity != null && leads.StrctureAmmount != null && leads.HeightStructureId != null &&
                    leads.CommonMeter != null)
                {
                    leads.IsVerified = true;
                }

                leads.RequiredCapacity = input.RequiredCapacity == null ? 0 : input.RequiredCapacity;
                var dbDup = _leadRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnitId && e.ChanelPartnerID != null && ((e.EmailId == input.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == input.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).FirstOrDefault();
                if (dbDup != null)
                {
                    leads.IsDuplicate = true;
                    leads.DublicateLeadId = dbDup.Id;
                }
                else
                {
                    leads.IsDuplicate = false;
                }

                //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                var webDup = _leadRepository.GetAll().Where(e => e.OrganizationUnitId == input.OrganizationUnitId && e.ChanelPartnerID == null && e.HideDublicate != true && ((e.EmailId == input.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == input.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).Any();
                if (webDup)
                {
                    leads.IsWebDuplicate = true;
                }
                else
                {
                    leads.IsWebDuplicate = false;
                }
                var leadId = await _leadRepository.InsertAndGetIdAsync(leads);

                if (leadId != 0)
                {
                    await NewLeadNotify(leads);
                }
                //Add Activity Log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 1;
                //leadactivity.LeadAcitivityID = 0; // confusion
                leadactivity.SectionId = 35;
                leadactivity.LeadActionNote = "Customer Created";
                leadactivity.LeadId = leads.Id;
                leadactivity.OrganizationUnitId = leads.OrganizationUnitId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);


                //LeadtrackerHistory dataVaulteActivityLog = new LeadtrackerHistory();
                //if (AbpSession.TenantId != null)
                //{
                //    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                //}
                //dataVaulteActivityLog.SectionId = 29; // Cistomer Pages Name
                //dataVaulteActivityLog.ActionId = 1; // Created
                //dataVaulteActivityLog.ActionNote = "Lead Created";
                //dataVaulteActivityLog.SectionValueId = leadId;
                //await _leadHistoryLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_Edit), AbpAuthorize(AppPermissions.Pages_Tenant_Leads_ManageLead_Edit)]
        protected virtual async Task Update(CreateOrEditLeadsDto input)
        {

            List<Leads> objAuditOld_OnlineUserMst = new List<Leads>();
            List<CreateOrEditLeadsDto> objAuditNew_OnlineUserMst = new List<CreateOrEditLeadsDto>();
            DataTable Dt_Target = new DataTable();
            DataTable Dt_Source = new DataTable();
            var List = new List<LeadtrackerHistory>();

            var leads = await _leadRepository.GetAll()
                .Include(x => x.CityIdFk)
                .Include(x => x.TalukaIdFk)
                .Include(x => x.DistrictIdFk)
                .Include(x => x.StateIdFk)
                .Include(x => x.SubDivisionIdFk)
                .Include(x => x.DivisionIdFk)
                .Include(x => x.CircleIdFk)
                .Include(x => x.DiscomIdFk)
                .Include(x => x.DiscomIdFk)
                .Include(x => x.HeightStructureIdFk)
                .Include(x => x.LeadStatusIdFk)
                .Include(x => x.LeadSourceIdFk)
                .Where(x => x.Id == (int)input.Id).FirstOrDefaultAsync();//  FirstOrDefaultAsync((int)input.Id);

            try
            {
                leads.IsVerified = false;
                if (!string.IsNullOrEmpty(input.FirstName) && !string.IsNullOrEmpty(input.LastName) && !string.IsNullOrEmpty(input.MobileNumber) &&
                    input.SolarTypeId != null && input.ConsumerNumber != null && !string.IsNullOrEmpty(input.AddressLine1) && !string.IsNullOrEmpty(input.AddressLine2) &&
                    input.CityId != null && input.TalukaId != null && input.DistrictId != null && input.StateId != null && input.Pincode != null && input.SubDivisionId != null && input.DivisionId != null &&
                    input.CircleId != null && input.DiscomId != null && input.Latitude != null && input.Longitude != null && input.RequiredCapacity != null && input.Strctureammount != null && input.HeightStructureId != null && input.CommonMeter != null)
                {
                    leads.IsVerified = true;
                    input.IsVerified = true;
                }

                objAuditOld_OnlineUserMst = _leaddataRepository.GetAll().Where(e => e.Id == input.Id).ToList();
                objAuditNew_OnlineUserMst.Add(input);

                //Add Activity Log
                LeadtrackerHistory dataVaulteActivityLog = new LeadtrackerHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 2;
                //leadactivity.LeadAcitivityID = 0;
                leadactivity.SectionId = 29;
                leadactivity.LeadActionNote = "Customer Modified";
                leadactivity.LeadId = leads.Id;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);


                #region edithistory
                try
                {
                    object newvalue = objAuditNew_OnlineUserMst;
                    //var List1 = new List<LeadtrackerHistory>();
                    //if (citys.Name != input.Name)
                    //{
                    //    LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                    //    if (AbpSession.TenantId != null)
                    //    {
                    //        dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                    //    }
                    //    dataVaultsHistory.FieldName = "Name";
                    //    dataVaultsHistory.PrevValue = citys.Name;
                    //    dataVaultsHistory.CurValue = input.Name;
                    //    dataVaultsHistory.Action = "Edit";
                    //    dataVaultsHistory.ActionId = 2;
                    //    dataVaultsHistory.ActivityLogId = cityActivityLogId;
                    //    dataVaultsHistory.SectionId = 16; // Pages name
                    //    dataVaultsHistory.SectionValueId = input.Id;
                    //    List.Add(dataVaultsHistory);
                    //}
                    //System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();


                    //New Value
                    if (newvalue != null)
                    {
                        Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));
                    }
                    //Old Value
                    if (objAuditOld_OnlineUserMst != null)
                    {
                        //Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                    }
                    foreach (DataRow dr_S in Dt_Source.Rows)
                    {
                        foreach (DataColumn dc_S in Dt_Source.Columns)
                        {
                            try
                            {
                                if (leads.GetType().GetProperty(dc_S.ColumnName) != null && dr_S[dc_S.ColumnName] != null)
                                {
                                    if (Convert.ToString(leads.GetType().GetProperty(dc_S.ColumnName).GetValue(leads)) != Convert.ToString(dr_S[dc_S.ColumnName]))
                                    {
                                        LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                        if (dc_S.ColumnName == "CityId")
                                        {
                                            objAuditInfo.FieldName = "CityName";
                                            objAuditInfo.PrevValue = leads.CityIdFk == null ? "" : leads.CityIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_cityRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "TalukaId")
                                        {
                                            objAuditInfo.FieldName = "TalukaName";
                                            objAuditInfo.PrevValue = leads.TalukaIdFk == null ? "" : leads.TalukaIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_talukaRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "DistrictId")
                                        {
                                            objAuditInfo.FieldName = "DistrictName";
                                            objAuditInfo.PrevValue = leads.DistrictIdFk == null ? "" : leads.DistrictIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_districtRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "StateId")
                                        {
                                            objAuditInfo.FieldName = "StateName";
                                            objAuditInfo.PrevValue = leads.StateIdFk == null ? "" : leads.StateIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_stateRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "LeadTypeId")
                                        {
                                            objAuditInfo.FieldName = "LeadType";
                                            objAuditInfo.PrevValue = leads.LeadTypeIdFk == null ? "" : leads.LeadTypeIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_leadTypeRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "LeadSourceId")
                                        {
                                            objAuditInfo.FieldName = "LeadSource";
                                            objAuditInfo.PrevValue = leads.LeadSourceIdFk == null ? "" : leads.LeadSourceIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_leadSourceRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "LeadStatusId")
                                        {
                                            objAuditInfo.FieldName = "LeadStatus";
                                            objAuditInfo.PrevValue = leads.LeadStatusIdFk == null ? "" : leads.LeadStatusIdFk.Status; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_leadStatusRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Status).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "SolarTypeId")
                                        {
                                            objAuditInfo.FieldName = "SolarType";
                                            objAuditInfo.PrevValue = leads.SolarTypeIdFk == null ? "" : leads.SolarTypeIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_solarTypeRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "DiscomId")
                                        {
                                            objAuditInfo.FieldName = "Discom";
                                            objAuditInfo.PrevValue = leads.DiscomIdFk == null ? "" : leads.DiscomIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_discomRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "DivisionId")
                                        {
                                            objAuditInfo.FieldName = "Division";
                                            objAuditInfo.PrevValue = leads.DivisionIdFk == null ? "" : leads.DivisionIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_divisionRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "SubDivisionId")
                                        {
                                            objAuditInfo.FieldName = "SubDivision";
                                            objAuditInfo.PrevValue = leads.SubDivisionIdFk == null ? "" : leads.SubDivisionIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_subDivisionRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "HeightStructureId")
                                        {
                                            objAuditInfo.FieldName = "HeightStructure";
                                            objAuditInfo.PrevValue = leads.HeightStructureIdFk == null ? "" : leads.HeightStructureIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_hightofStructureRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "CircleId")
                                        {
                                            objAuditInfo.FieldName = "Circle";
                                            objAuditInfo.PrevValue = leads.CircleIdFk == null ? "" : leads.CircleIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = await _lookup_circleRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync();
                                        }
                                        else if (dc_S.ColumnName == "ChanelPartnerID")
                                        {
                                            objAuditInfo.FieldName = "ChanelPartner";
                                            objAuditInfo.PrevValue = await _userRepository.GetAll().Where(x => x.Id == Convert.ToInt32(dr_S[dc_S.ColumnName])).Select(u => u.Name).FirstOrDefaultAsync(); //  leads.CircleIdFk.Name; // Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                            objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                                        }
                                        else
                                        {
                                            objAuditInfo.FieldName = dc_S.ColumnName;
                                            objAuditInfo.PrevValue = Convert.ToString(leads.GetType().GetProperty(dc_S.ColumnName).GetValue(leads, null)); //leads.GetProperty .ToString().dc_S.ColumnName; //Dt_Target.Rows[0];
                                            objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                                        }

                                        objAuditInfo.Action = "Edit";
                                        objAuditInfo.LeadId = leads.Id;
                                        objAuditInfo.LeadActionId = leadactid;
                                        objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                        List.Add(objAuditInfo);
                                    }
                                }

                            }
                            catch { }

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                #endregion
                await _dbContextProvider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
                await _dbContextProvider.GetDbContext().SaveChangesAsync();

                var dbDup = _leadRepository.GetAll().Where(e => e.Id != input.Id && e.OrganizationUnitId == input.OrganizationUnitId && e.ChanelPartnerID != null && ((e.EmailId == input.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == input.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).FirstOrDefault();
                if (dbDup != null)
                {
                    input.IsDuplicate = true;
                    input.DublicateLeadId = dbDup.Id;
                }
                else
                {
                    input.IsDuplicate = false;
                }

                //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                var webDup = _leadRepository.GetAll().Where(e => e.Id != input.Id && e.OrganizationUnitId == input.OrganizationUnitId && e.ChanelPartnerID == null && e.HideDublicate != true && ((e.EmailId == input.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == input.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).Any();
                if (webDup)
                {
                    input.IsWebDuplicate = true;
                }
                else
                {
                    input.IsWebDuplicate = false;
                }

                ObjectMapper.Map(input, leads);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> NewLeadNotify(Leads input)
        {
            var User_List = _userRepository.GetAll();
            if (User_List.Count() > 0)
            {
                var User = User_List.Where(e => e.Id == AbpSession.UserId).FirstOrDefault();

                var managerId = await _userDetailsRepository.GetAll().Where(x => x.UserId == input.CreatorUserId).Select(x => x.ManageBy).FirstOrDefaultAsync();
                var RoleName = (from user in User_List
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                where (us != null && user.Id == input.CreatorUserId)
                                select (us.DisplayName));

                if (RoleName.Contains("Manager"))
                {
                    var NotifyToUser1 = _userRepository.GetAll().Where(u => u.Id == input.ChanelPartnerID).FirstOrDefault();
                    string msg1 = string.Format("New Lead Created {0}.", input.CustomerName);
                    await _appNotifier.NewLead(NotifyToUser1, msg1, NotificationSeverity.Info);
                }
                else if (RoleName.Contains("Channel Partners"))
                {
                    var NotifyToUser = _userRepository.GetAll().Where(u => u.Id == managerId).FirstOrDefault();
                    string msg = string.Format("New Lead Created {0}.", input.CustomerName);
                    await _appNotifier.NewLead(NotifyToUser, msg, NotificationSeverity.Info);
                }
            }
            return true;
        }
        //List Of Lead Document
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_ViewDocument)]
        public async Task<PagedResultDto<GetLeadDocumentForViewDto>> LeadDocumentList(GetAllLeadDocumentListInput input)
        {
            var filteredDocument = _leaddocumentRepository.GetAll()
            //  .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DocumentIdFk.Name.Contains(input.Filter))
            .Where(e => e.LeadDocumentId == Convert.ToInt32(input.Filter));

            var pagedAndFilteredDepartments = filteredDocument
               .OrderBy(input.Sorting ?? "id desc")
               .PageBy(input);

            var documentlist = from o in pagedAndFilteredDepartments
                               select new GetLeadDocumentForViewDto()
                               {
                                   documentList = new LeadDocumentDto
                                   {
                                       Title = o.DocumentIdFk.Name,
                                       Id = o.Id,
                                       LeadDocumentPath = o.LeadDocumentPath,
                                       DocumentNumber = o.DocumentNumber
                                   }
                               };
            var totalCount = await filteredDocument.CountAsync();

            return new PagedResultDto<GetLeadDocumentForViewDto>(
                totalCount,
                await documentlist.ToListAsync()
            );
        }
        public async Task CreateOrEditLeadDocument(CreateOrEditLeadDocumentDto input)
        {
        }

        //Lead Documents Delete
        [AbpAuthorize(AppPermissions.Pages_Tenant_Leads_Customer_DeleteDocument)]
        public async Task LeadDocumentDelete(EntityDto input)
        {
            try
            {
                var documentResult = await _leaddocumentRepository.GetAll().Include(x => x.LeadIdFk).Include(x => x.DocumentIdFk).Where(x => x.Id == input.Id).FirstOrDefaultAsync();
                await _leaddocumentRepository.DeleteAsync(input.Id);

                //Add Activity Log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 17;
                //leadactivity.LeadAcitivityID = 0; // confusion
                leadactivity.SectionId = 51;
                leadactivity.LeadActionNote = "Lead Document Deleted Name :-" + documentResult.DocumentIdFk.Name;
                leadactivity.LeadId = documentResult.LeadIdFk.Id;
                leadactivity.OrganizationUnitId = documentResult.LeadIdFk.OrganizationUnitId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task ChangeDuplicateStatusForMultipleLeadsHide(GetLeadForChangeDuplicateStatusOutput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);
                lead.HideDublicate = input.HideDublicate;
                lead.ChangeStatusDate = DateTime.UtcNow;

                await _leadRepository.UpdateAsync(lead);

                LeadActivityLogs leadactivity = new LeadActivityLogs();
                if (input.HideDublicate != null)
                {
                    leadactivity.LeadActionId = 5;
                    if (input.HideDublicate == true)
                    {
                        leadactivity.LeadActionNote = "Lead Status Changed To Duplicate";
                    }
                    else
                    {
                        leadactivity.LeadActionNote = "Lead Status Changed To not Duplicate";
                    }
                }

                leadactivity.LeadId = lead.Id;
                leadactivity.SectionId = 35;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                await _leadactivityRepository.InsertAsync(leadactivity);

            }


            //await UpdateLeads(input);
        }

        public virtual async Task UpdateWebDupLeads(GetLeadForChangeDuplicateStatusOutput input)
        {
            foreach (var item in input.LeadId)
            {
                var lead = await _leadRepository.FirstOrDefaultAsync((int)item);

                var DupLead = _leadRepository.GetAll().Where(e => e.OrganizationUnitId == lead.OrganizationUnitId && e.ChanelPartnerID == null && e.HideDublicate != true && ((e.EmailId == lead.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == lead.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).Select(e => e.Id);

                if (DupLead.Count() == 1)
                {
                    var Id = DupLead.FirstOrDefault();
                    var leadT = await _leadRepository.FirstOrDefaultAsync(Id);
                    leadT.IsWebDuplicate = null;
                    await _leadRepository.UpdateAsync(leadT);
                }
            }
        }

        /// <summary>
        /// Check Lead Existancy (Address, Email, Phone)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<List<GetDuplicateLeadPopupDto>> CheckExistLeadList(CheckExistLeadDto input)
        {

            var User = _userRepository.GetAll().Where(u => u.Id == AbpSession.UserId).FirstOrDefault();
            List<GetDuplicateLeadPopupDto> leads = new List<GetDuplicateLeadPopupDto>();
            IList<string> role = await _userManager.GetRolesAsync(User);

            //if (role.Contains("Admin"))
            //{
            //	return new List<GetDuplicateLeadPopupDto>();
            //}
            var Mobile = input.Mobile.Substring(input.Mobile.Length - 9);

            var duplicateLead = _leadRepository.GetAll().Where(e => e.MobileNumber.Contains(Mobile) || e.EmailId == input.Email ||
                                (e.AddressLine1 == input.AddressLine1 && e.AddressLine2 == input.AddressLine2 && e.StateId == input.State && e.DistrictId == input.DistrictId
                                && e.CityId == input.CityId && e.TalukaId == input.TalukaId && e.Pincode == input.Pincode))
                                .Where(e => e.OrganizationUnitId == input.OrganizationId)
                                .WhereIf(input.Id != null, e => e.Id != input.Id)
                                .Where(e => e.IsDuplicate != true && e.IsWebDuplicate != true);
            if (duplicateLead != null)
            {
                leads = await (from o in duplicateLead

                         join o1 in _lookup_leadStatusRepository.GetAll() on o.LeadStatusId equals o1.Id into j1
                         from s1 in j1.DefaultIfEmpty()

                         join o2 in _userRepository.GetAll() on o.CreatorUserId equals o2.Id into j2
                         from s2 in j2.DefaultIfEmpty()

                         select new GetDuplicateLeadPopupDto()
                         {
                             CreatedByName = s2.FullName,
                             CreationTime = o.CreationTime,
                             Address = o.AddressLine1,
                             CustomerName = o.CustomerName,
                             EmailId = o.EmailId,
                             Id = o.Id,
                             //Requirements = o.Requirements,
                             Alt_PhoneNumber = o.Alt_Phone,
                             MobileNumber = o.MobileNumber,
                             LeadSource = o.LeadSourceIdFk.Name,
                             LeadStatus = s1.Status,
                             CurrentAssignUserName = _userRepository.GetAll().Where(e => e.Id == o.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                             State = o.StateIdFk.Name,
                             Postcode = o.Pincode.ToString(),
                             CurrentLeadOwaner = s2.FullName,
                             AddressExist = (o.AddressLine1 == input.AddressLine1 && o.AddressLine2 == input.AddressLine2 && o.StateId == input.State && o.DistrictId == input.DistrictId
                                 && o.CityId == input.CityId && o.TalukaId == input.TalukaId && o.Pincode == input.Pincode),
                             EmailExist = o.EmailId == input.Email,
                             MobileExist = o.MobileNumber == input.Mobile,
                             RoleName = role.FirstOrDefault()
                         }).ToListAsync();
            }
            return new List<GetDuplicateLeadPopupDto>(leads);
            //return new List<GetDuplicateLeadPopupDto>(
            //    await leads.ToListAsync()
            //);
        }


        public async Task<bool> CheckValidation(CreateOrEditLeadsDto input)
        {
            var output = true;

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            IList<string> role = await _userManager.GetRolesAsync(User);

            if (!role.Contains("Admin"))
            {
                output = (!string.IsNullOrEmpty(input.CustomerName) && !string.IsNullOrEmpty(input.AddressLine1) && !string.IsNullOrEmpty(input.AddressLine2) && input.StateId != null
                    && input.DistrictId != null && input.TalukaId != null && input.CityId != null
                    && input.Pincode != null
                    && input.LeadSourceId != null && (!string.IsNullOrEmpty(input.EmailId) || !string.IsNullOrEmpty(input.MobileNumber)));
            }
            else
            {
                output = (!string.IsNullOrEmpty(input.CustomerName) && input.LeadSourceId != null && (!string.IsNullOrEmpty(input.EmailId) || !string.IsNullOrEmpty(input.MobileNumber)));
            }

            return output;
        }

        public async Task UpdateFacebookLead()
        {
            var fbLeadSource = new List<int?> { 4, 18, 24, 26 };

            var fbLeads = _leadRepository.GetAll().Where(e => e.LeadStatusId == 1 && e.ChanelPartnerID == null && e.HideDublicate == null && e.IsWebDuplicate == null && e.IsDuplicate == null && fbLeadSource.Contains(e.LeadSourceId)).ToList();

            if (fbLeads != null)
            {
                var IdList = fbLeads.Select(e => e.Id).ToList();
                foreach (var item in fbLeads)
                {
                    //Start Check Duplicate
                    //DataBase Duplicate Check Email OR Mobile Duplication Only for Assigned Leads(Not For Null Fields)
                    var dbDup = _leadRepository.GetAll().Where(e => !IdList.Contains(e.Id) && e.OrganizationUnitId == item.OrganizationUnitId && e.ChanelPartnerID != null && ((e.EmailId == item.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == item.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).FirstOrDefault();
                    if (dbDup != null)
                    {
                        item.IsDuplicate = true;
                        item.DublicateLeadId = dbDup.Id;
                    }
                    else
                    {
                        item.IsDuplicate = false;
                    }

                    //Web Duplicate Check Email OR Mobile Duplication Only for not Assigned Leads(Not For Null Fields)
                    var webDup = _leadRepository.GetAll().Where(e => !IdList.Contains(e.Id) && e.OrganizationUnitId == item.OrganizationUnitId && e.ChanelPartnerID == null && e.HideDublicate != true && ((e.EmailId == item.EmailId && !string.IsNullOrEmpty(e.EmailId)) || (e.MobileNumber == item.MobileNumber && !string.IsNullOrEmpty(e.MobileNumber)))).Any();
                    if (webDup)
                    {
                        item.IsWebDuplicate = true;
                    }
                    else
                    {
                        item.IsWebDuplicate = false;
                    }
                    //End Check Duplicate

                    await _leadRepository.UpdateAsync(item);
                    IdList.Remove(item.Id);
                }
            }
        }

    }
}
public class RootObject<T>
{
    public int Code { get; set; }
    public T Result { get; set; }
}