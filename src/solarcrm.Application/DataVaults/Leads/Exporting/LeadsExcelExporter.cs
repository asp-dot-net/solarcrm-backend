﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Customers.Exporting
{
    public class LeadsExcelExporter : NpoiExcelExporterBase, ILeadsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LeadsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetLeadsForViewDto> customers)
        {
            return CreateExcelPackage(
                "Leads.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Leads"));

                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Email"),
                        L("Mobile"),
                        L("Phone"),
                        L("Address"),
                        L("StateName"),
                        L("DistrictName"),
                        L("TalukaName"),
                        L("CityName")
                        );

                    AddObjects(
                        sheet, customers,
                        _ => _.Leads.CustomerName,
                         _ => _.Leads.EmailId,
                         _ => _.Leads.MobileNumber,
                         _ => _.Leads.Alt_Phone,
                         _ => _.Leads.Address,
                         _ => _.Leads.StateName,
                         _ => _.Leads.DistrictName,
                         _ => _.Leads.TalukaName,
                         _ => _.Leads.CityName
                        );
                });
        }

        public FileDto ExportCsvToFile(List<GetLeadsForViewDto> leads)
        {
            return CreateExcelPackage(
                "Leads.csv",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Leads"));

                    AddHeader(
                        sheet,
                        L("Customer"),
                        L("Email"),
                        L("Mobile"),
                        L("Phone"),
                        L("Address"),
                        L("StateName"),
                        L("DistrictName"),
                        L("TalukaName"),
                        L("CityName")
                        );

                    AddObjects(
                        sheet, leads,
                        _ => _.Leads.CustomerName,
                         _ => _.Leads.EmailId,
                         _ => _.Leads.MobileNumber,
                         _ => _.Leads.Alt_Phone,
                         _ => _.Leads.Address,
                         _ => _.Leads.StateName,
                         _ => _.Leads.DistrictName,
                         _ => _.Leads.TalukaName,
                         _ => _.Leads.CityName
                        );
                });
        }


        public FileDto MyLeadExportToFile(List<GetLeadsForViewDto> myleadListDtos)
        {
            return CreateExcelPackage(
                "Leads.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Leads"));
                    AddHeader(
                        sheet,
                        L("CustomerName"),
                        L("MobileNumber"),
                        L("EmailId"),
                        L("Alt_Phone"),
                        L("Address"),
                        L("StateName"),
                        L("DistrictName"),
                        L("TalukaName"),
                        L("CityName"),
                        L("Pincode"),
                        L("Latitude"),
                        L("Longitude"),
                        L("LeadType"),
                        L("LeadSource"),
                        L("LeadStatus"),
                        L("SolarType"),
                        L("CommonMeter"),
                        L("ConsumerNumber"),
                        L("AvgmonthlyUnit"),
                        L("AvgmonthlyBill"),
                        L("Discom"),
                        L("Division"),
                        L("SubDivision"),
                        L("Circle"),
                        L("RequiredCapacity"),
                        L("StrctureAmmount"),
                        L("HeightStructure"),
                        L("Notes"),
                        L("Category"),
                        L("ChanelPartner"),
                        L("BankAccountNumber"),
                        L("BankAccountHolderName"),
                        L("BankName"),
                        L("IFSC_Code"),
                        L("BankBrachName"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, myleadListDtos,
                        _ => _.Leads.FirstName,
                        _ => _.Leads.MobileNumber,
                        _ => _.Leads.EmailId,
                        _ => _.Leads.Alt_Phone,
                        _ => _.Leads.Address,
                        _ => _.Leads.StateName,
                        _ => _.Leads.DistrictName,
                        _ => _.Leads.TalukaName,
                         _ => _.Leads.CityName,
                          _ => _.Leads.Pincode,
                           _ => _.Leads.Latitude,
                            _ => _.Leads.Longitude,
                             _ => _.Leads.LeadTypeName,
                              _ => _.Leads.LeadSourceName,
                               _ => _.Leads.LeadStatusName,
                                _ => _.Leads.SolarTypeName,
                                 _ => _.Leads.CommonMeter,
                                  _ => _.Leads.ConsumerNumber,
                                   _ => _.Leads.AvgMonthlyUnit,
                                    _ => _.Leads.AvgMonthlyBill,
                                     _ => _.Leads.DiscomName,
                                      _ => _.Leads.DivisionName,
                                       _ => _.Leads.SubDivisionName,
                                        _ => _.Leads.CircleName,
                                         _ => _.Leads.RequiredCapacity,
                                          _ => _.Leads.StrctureAmmount,
                                           _ => _.Leads.HeightStructureName,
                                            _ => _.Leads.Notes,
                                             _ => _.Leads.CategoryName,
                                              _ => _.Leads.ChanelPartnerName,
                                               _ => _.Leads.BankAccountNumber,
                                                _ => _.Leads.AccountHolderName,
                                                 _ => _.Leads.BankName,
                                                  _ => _.Leads.IFSC_Code,
                                                   _ => _.Leads.BankBranchName,
                                                   _ => _.Leads.CreationTime



                        );

                    //for (var i = 1; i <= myleadListDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[6], "dd-MM-yyyy");
                    //}
                    //sheet.AutoSizeColumn(6);
                    //for (var i = 1; i <= myleadListDtos.Count; i++)
                    //{
                    //    SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd-MM-yyyy");
                    //}
                    //sheet.AutoSizeColumn(9);
                });
        }

        public FileDto MyLeadCsvExport(List<GetLeadsForViewDto> myleadListDtos)
        {
            return CreateExcelPackage(
                "Leads.csv",
                 excelPackage =>
                 {
                     var sheet = excelPackage.CreateSheet(L("Leads"));
                     AddHeader(
                         sheet,
                         L("CustomerName"),
                         L("MobileNumber"),
                         L("EmailId"),
                          L("Alt_Phone"),
                         L("Address"),
                         L("StateName"),
                         L("DistrictName"),
                         L("TalukaName"),
                         L("CityName"),
                         L("Pincode"),
                         L("Latitude"),
                         L("Longitude"),
                         L("LeadType"),
                         L("LeadSource"),
                         L("LeadStatus"),
                         L("SolarType"),
                         L("CommonMeter"),
                         L("ConsumerNumber"),
                         L("AvgmonthlyUnit"),
                         L("AvgmonthlyBill"),
                         L("Discom"),
                         L("Division"),
                         L("SubDivision"),
                         L("Circle"),
                         L("RequiredCapacity"),
                         L("StrctureAmmount"),
                         L("HeightStructure"),
                         L("Notes"),
                         L("Category"),
                         L("ChanelPartner"),
                         L("BankAccountNumber"),
                         L("BankAccountHolderName"),
                         L("BankName"),
                         L("IFSC_Code"),
                         L("BankBrachName"),
                         L("CreationTime")
                         );

                     AddObjects(
                         sheet, myleadListDtos,
                         _ => _.Leads.FirstName,
                         _ => _.Leads.MobileNumber,
                         _ => _.Leads.EmailId,
                         _ => _.Leads.Alt_Phone,
                         _ => _.Leads.Address,
                         _ => _.Leads.StateName,
                         _ => _.Leads.DistrictName,
                         _ => _.Leads.TalukaName,
                          _ => _.Leads.CityName,
                           _ => _.Leads.Pincode,
                            _ => _.Leads.Latitude,
                             _ => _.Leads.Longitude,
                              _ => _.Leads.LeadTypeName,
                               _ => _.Leads.LeadSourceName,
                                _ => _.Leads.LeadStatusName,
                                 _ => _.Leads.SolarTypeName,
                                  _ => _.Leads.CommonMeter,
                                   _ => _.Leads.ConsumerNumber,
                                    _ => _.Leads.AvgMonthlyUnit,
                                     _ => _.Leads.AvgMonthlyBill,
                                      _ => _.Leads.DiscomName,
                                       _ => _.Leads.DivisionName,
                                        _ => _.Leads.SubDivisionName,
                                         _ => _.Leads.CircleName,
                                          _ => _.Leads.RequiredCapacity,
                                           _ => _.Leads.StrctureAmmount,
                                            _ => _.Leads.HeightStructureName,
                                             _ => _.Leads.Notes,
                                              _ => _.Leads.CategoryName,
                                               _ => _.Leads.ChanelPartnerName,
                                                _ => _.Leads.BankAccountNumber,
                                                 _ => _.Leads.AccountHolderName,
                                                  _ => _.Leads.BankName,
                                                   _ => _.Leads.IFSC_Code,
                                                    _ => _.Leads.BankBranchName,
                                                    _ => _.Leads.CreationTime



                         );

                     //for (var i = 1; i <= myleadListDtos.Count; i++)
                     //{
                     //    SetCellDataFormat(sheet.GetRow(i).Cells[6], "dd-MM-yyyy");
                     //}
                     //sheet.AutoSizeColumn(6);
                     //for (var i = 1; i <= myleadListDtos.Count; i++)
                     //{
                     //    SetCellDataFormat(sheet.GetRow(i).Cells[9], "dd-MM-yyyy");
                     //}
                     //sheet.AutoSizeColumn(9);
                 });
        }


    }
}
