﻿using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Customers.Exporting
{
    public interface ILeadsExcelExporter
    {
        FileDto ExportToFile(List<GetLeadsForViewDto> customers);

        FileDto ExportCsvToFile(List<GetLeadsForViewDto> leads);
        FileDto MyLeadExportToFile(List<GetLeadsForViewDto> myleadListDtos);
        FileDto MyLeadCsvExport(List<GetLeadsForViewDto> myleadListDtos);
    }
}
