﻿using solarcrm.DataVaults.City.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.City.Exporting
{
    public interface ICityExcelExporter
    {
        FileDto ExportToFile(List<GetCityForViewDto> city);
    }
}
