﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.City.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.City.Exporting
{
    public class CityExcelExporter : NpoiExcelExporterBase, ICityExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CityExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCityForViewDto> citys)
        {
            return CreateExcelPackage(
                "City.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("City"));

                    AddHeader(
                        sheet,
                        L("TalukaName"),
                        L("CityName")
                        );

                    AddObjects(
                        sheet, citys,
                        _ => _.Citys.TalukaName,
                         _ => _.Citys.Name
                        );
                });
        }
    }
}
