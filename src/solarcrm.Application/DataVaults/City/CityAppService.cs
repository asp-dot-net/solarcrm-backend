﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.City.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.City.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Common;

namespace solarcrm.DataVaults.City
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_City)]
    public class CityAppService : solarcrmAppServiceBase, ICityAppService
    {
        private readonly IRepository<City> _cityRepository;
        private readonly ICityExcelExporter _cityExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Taluka.Taluka> _talukaRepository;
        public CityAppService(IRepository<City> CityRepository, ICityExcelExporter CityExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<Taluka.Taluka> talukaRepository)
        {
            _cityRepository = CityRepository;
            _cityExcelExporter = CityExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _talukaRepository = talukaRepository;
        }

        public async Task<PagedResultDto<GetCityForViewDto>> GetAll(GetAllCityInput input)
        {
            var filteredCity = _cityRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredCity = filteredCity.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var citys = from o in pagedAndFilteredCity
                            select new GetCityForViewDto()
                            {
                                Citys = new CityDto
                                {
                                    Name = o.Name,
                                    TalukaName = o.TalukaFk.Name,
                                    IsActive = o.IsActive,
                                    Id = o.Id,
                                    CreatedDate = o.CreationTime
                                }
                            };

            var totalCount = await filteredCity.CountAsync();

            return new PagedResultDto<GetCityForViewDto>(totalCount, await citys.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_City_Edit)]
        public async Task<GetCityForEditOutput> GetCityForEdit(EntityDto input)
        {
            var citys = await _cityRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCityForEditOutput { citys = ObjectMapper.Map<CreateOrEditCityDto>(citys) };

            return output;
        }

        public async Task<GetCityForViewDto> GetCityForView(int id)
        {
            var citys = await _cityRepository.GetAsync(id);

            var output = new GetCityForViewDto { Citys = ObjectMapper.Map<CityDto>(citys) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_City_ExportToExcel)]
        public async Task<FileDto> GetCityToExcel(GetAllCityForExcelInput input)
        {
            var filteredCity = _cityRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCity
                         select new GetCityForViewDto()
                         {
                             Citys = new CityDto
                             {
                                 Name = o.Name,
                                 TalukaName = o.TalukaFk.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var cityListDtos = await query.ToListAsync();

            return _cityExcelExporter.ExportToFile(cityListDtos);
        }


        public async Task CreateOrEdit(CreateOrEditCityDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_City_Create)]
        protected virtual async Task Create(CreateOrEditCityDto input)
        {
            var city = ObjectMapper.Map<City>(input);

            if (AbpSession.TenantId != null)
            {
                city.TenantId = (int?)AbpSession.TenantId;
            }

            var cityId = await _cityRepository.InsertAndGetIdAsync(city);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 16; // City Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "City Created Name ;-" + input.Name;
            dataVaulteActivityLog.SectionValueId = cityId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_City_Edit)]
        protected virtual async Task Update(CreateOrEditCityDto input)
        {
            var citys = await _cityRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 16; // City Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated;  // Updated
            dataVaulteActivityLog.ActionNote = "City Updated Name ;-" + citys.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var cityActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (citys.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = citys.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; 
                dataVaultsHistory.ActivityLogId = cityActivityLogId;
                dataVaultsHistory.SectionId = 16; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (citys.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = citys.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = cityActivityLogId;
                dataVaultsHistory.SectionId = 16;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if (citys.TalukaId != input.TalukaId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Taluka";
                dataVaultsHistory.PrevValue = _talukaRepository.GetAll().Where(e => e.Id == citys.TalukaId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _talukaRepository.GetAll().Where(e => e.Id == input.TalukaId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = cityActivityLogId;
                dataVaultsHistory.SectionId = 16;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, citys);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_City_Delete)]
        public async Task Delete(EntityDto input)
        {
            var cityResult = await _cityRepository.FirstOrDefaultAsync(input.Id);
            await _cityRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 16; // city Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted ; // Deleted
            dataVaulteActivityLog.ActionNote = "City Deleted Name :-" + cityResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
