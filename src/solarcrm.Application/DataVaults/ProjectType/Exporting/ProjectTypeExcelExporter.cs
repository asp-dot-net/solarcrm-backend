﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.ProjectType.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ProjectType.Exporting
{
    public class ProjectTypeExcelExporter : NpoiExcelExporterBase, IProjectTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProjectTypeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetProjectTypeForViewDto> ProjectType)
        {
            return CreateExcelPackage(
                "ProjectType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("ProjectType"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, ProjectType,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, ProjectType,
                        _ => _.ProjectType.Name
                        );
                });
        }
    }
}
