﻿using solarcrm.DataVaults.ProjectType.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.ProjectType.Exporting
{
    public interface IProjectTypeExcelExporter
    {
        FileDto ExportToFile(List<GetProjectTypeForViewDto> projecttype);
    }
}
