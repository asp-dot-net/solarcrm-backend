﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.ProjectType.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.ProjectType.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.ProjectType
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectType)]
    public class ProjectTypeAppService : solarcrmAppServiceBase, IProjectTypeAppService
    {
        private readonly IRepository<ProjectType> _ProjectTypeRepository;
        private readonly IProjectTypeExcelExporter _ProjectTypeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public ProjectTypeAppService(IRepository<ProjectType> ProjectTypeRepository, IProjectTypeExcelExporter ProjectTypeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _ProjectTypeRepository = ProjectTypeRepository;
            _ProjectTypeExcelExporter = ProjectTypeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        
        public async Task<PagedResultDto<GetProjectTypeForViewDto>> GetAll(GetAllProjectTypeInput input)
        {
            var filteredprojectType = _ProjectTypeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredProjectType = filteredprojectType.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var projectType = from o in pagedAndFilteredProjectType
                              select new GetProjectTypeForViewDto()
                             {
                                 ProjectType = new ProjectTypeDto
                                 {
                                     Name = o.Name,
                                     IsActive = o.IsActive,
                                     Id = o.Id,
                                     CreatedDate = o.CreationTime
                                 }
                             };

            var totalCount = await filteredprojectType.CountAsync();

            return new PagedResultDto<GetProjectTypeForViewDto>(totalCount, await projectType.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectType_Edit)]
        public async Task<GetProjectTypeForEditOutput> GetProjectTypeForEdit(EntityDto input)
        {
            var ProjectType = await _ProjectTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetProjectTypeForEditOutput { ProjectType = ObjectMapper.Map<CreateOrEditProjectTypeDto>(ProjectType) };

            return output;
        }

        public async Task<GetProjectTypeForViewDto> GetProjectTypeForView(int id)
        {
            var ProjectType = await _ProjectTypeRepository.GetAsync(id);

            var output = new GetProjectTypeForViewDto { ProjectType = ObjectMapper.Map<ProjectTypeDto>(ProjectType) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectType_ExportToExcel)]
        public async Task<FileDto> GetProjectTypeToExcel(GetAllProjectTypeForExcelInput input)
        {
            var filteredprojectType = _ProjectTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredprojectType
                         select new GetProjectTypeForViewDto()
                         {
                             ProjectType = new ProjectTypeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var ProjectTypeListDtos = await query.ToListAsync();

            return _ProjectTypeExcelExporter.ExportToFile(ProjectTypeListDtos);
        }


        public async Task CreateOrEdit(CreateOrEditProjectTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectType_Create)]
        protected virtual async Task Create(CreateOrEditProjectTypeDto input)
        {
            var ProjectType = ObjectMapper.Map<ProjectType>(input);

            if (AbpSession.TenantId != null)
            {
                ProjectType.TenantId = (int?)AbpSession.TenantId;
            }

            var ProjectTypeId = await _ProjectTypeRepository.InsertAndGetIdAsync(ProjectType);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 6; // ProjectType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Project Type Created Name :- "+ input.Name;
            dataVaulteActivityLog.SectionValueId = ProjectTypeId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectType_Edit)]
        protected virtual async Task Update(CreateOrEditProjectTypeDto input)
        {
            var ProjectType = await _ProjectTypeRepository.FirstOrDefaultAsync((int)input.Id);
            
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 6; // ProjectType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Project Type Updated Name:- " +input.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var projecttypeActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if(ProjectType.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = ProjectType.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = projecttypeActivityLogId;
                dataVaultsHistory.SectionId = 6;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if(ProjectType.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = ProjectType.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = projecttypeActivityLogId;
                dataVaultsHistory.SectionId = 6;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, ProjectType);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_ProjectType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var projectTypeResult = await _ProjectTypeRepository.FirstOrDefaultAsync(input.Id);
            await _ProjectTypeRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 6; // Projecttype Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Project Type Deleted Name :- " + projectTypeResult.Name
                ;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<List<ProjectTypeLookupTableDto>> GetAllProjectTypeForDropdown()
        {
            var allprojecttype = _ProjectTypeRepository.GetAll().Where(x => x.IsActive == true);

            var projecttype = from o in allprojecttype
                              select new ProjectTypeLookupTableDto()
                            {
                                DisplayName = o.Name,
                                Id = o.Id
                            };

            return new List<ProjectTypeLookupTableDto>(await projecttype.ToListAsync());
        }
    }
}
