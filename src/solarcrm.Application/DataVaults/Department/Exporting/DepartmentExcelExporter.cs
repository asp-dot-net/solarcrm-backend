﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Department.Dto;
using solarcrm.DataVaults.Department.Exporting;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Department.Exporting
{
    public class DepartmentExcelExporter : NpoiExcelExporterBase, IDepartmentExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DepartmentExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }


        public FileDto ExportToFile(List<GetDepartmentForViewDto> deparments)
        {
            return CreateExcelPackage(
                "Departments.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Departments"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, deparments,
                        _ => _.Department.Name
                        );
                });
        }
    }
}
