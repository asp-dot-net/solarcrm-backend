﻿using solarcrm.DataVaults.Department.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.Department.Exporting
{
    public interface IDepartmentExcelExporter
    {
        FileDto ExportToFile(List<GetDepartmentForViewDto> departments);
    }
}
