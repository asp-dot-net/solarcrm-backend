﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Department.Dto;
using solarcrm.DataVaults.Department.Exporting;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using solarcrm.Dto;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Common;

namespace solarcrm.DataVaults.Department
{
    public class DepartmentAppService : solarcrmAppServiceBase, IDepartmentAppService
    {
        private readonly IRepository<Department> _departmentRepository;
        private readonly IDepartmentExcelExporter _departmentExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public DepartmentAppService(IRepository<Department> departmentRepository, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDepartmentExcelExporter departmentExcelExporter, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _departmentRepository = departmentRepository;
            _departmentExcelExporter = departmentExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        
        public async Task<PagedResultDto<GetDepartmentForViewDto>> GetAll(GetAllDepartmentInput input)
        {
            var filteredDepartment = _departmentRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredDepartment = filteredDepartment.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var department = from o in pagedAndFilteredDepartment
                           select new GetDepartmentForViewDto()
                           {
                               Department = new DepartmentDto
                               {
                                   Name = o.Name,
                                   IsActive = o.IsActive,
                                   Id = o.Id
                               }
                           };

            var totalCount = await filteredDepartment.CountAsync();

            return new PagedResultDto<GetDepartmentForViewDto>(totalCount, await department.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Department_Edit)]
        public async Task<GetDepartmentForEditOutput> GetDepartmentForEdit(EntityDto input)
        {
            var department = await _departmentRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDepartmentForEditOutput { Department = ObjectMapper.Map<CreateOrEditDepartmentDto>(department) };

            return output;
        }

        public async Task<GetDepartmentForViewDto> GetDepartmentForView(int id)
        {
            var department = await _departmentRepository.GetAsync(id);

            var output = new GetDepartmentForViewDto { Department = ObjectMapper.Map<DepartmentDto>(department) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Department_ExportToExcel)]
        public async Task<FileDto> GetDepartmentToExcel(GetAllDepartmentForExcelInput input)
        {
            var filteredDepartment = _departmentRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredDepartment
                         select new GetDepartmentForViewDto()
                         {
                             Department = new DepartmentDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var departmentListDtos = await query.ToListAsync();

            return _departmentExcelExporter.ExportToFile(departmentListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditDepartmentDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Department_Create)]
        protected virtual async Task Create(CreateOrEditDepartmentDto input)
        {
            var department = ObjectMapper.Map<Department>(input);

            if (AbpSession.TenantId != null)
            {
                department.TenantId = (int?)AbpSession.TenantId;
            }

            var departmentId = await _departmentRepository.InsertAndGetIdAsync(department);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 34; // Department Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Department Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = departmentId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Department_Edit)]
        protected virtual async Task Update(CreateOrEditDepartmentDto input)
        {
            var department = await _departmentRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 34; // Department Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Department Updated Name :-" + department.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var departmentActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (department.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = department.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = departmentActivityLogId;
                dataVaultsHistory.SectionId = 34; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (department.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = department.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = departmentActivityLogId;
                dataVaultsHistory.SectionId = 34;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion
            ObjectMapper.Map(input, department);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Department_Delete)]
        public async Task Delete(EntityDto input)
        {
            var departmentResult = await _departmentRepository.FirstOrDefaultAsync(input.Id);
            await _departmentRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 34; // Department Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Department Deleted Name :-" + departmentResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
