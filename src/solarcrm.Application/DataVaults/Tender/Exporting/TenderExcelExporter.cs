﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Tender.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Tender.Exporting
{
    public class TenderExcelExporter : NpoiExcelExporterBase, ITenderExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TenderExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTenderForViewDto> tender)
        {
            return CreateExcelPackage(
                "Tender.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Tender"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("SolarType"),
                        L("StartDate"),
                        L("EndDate")
                        );

                    AddObjects(
                        sheet, tender,
                        _ => _.tender.Name,
                        _ => _.tender.SolarType,
                        _ => _.tender.StartDate,
                        _ => _.tender.EndDate
                        );
                });
        }
    }
}
