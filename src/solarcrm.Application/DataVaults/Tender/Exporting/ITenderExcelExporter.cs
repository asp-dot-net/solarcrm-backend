﻿using solarcrm.DataVaults.Tender.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Tender.Exporting
{
    public interface ITenderExcelExporter
    {
        FileDto ExportToFile(List<GetTenderForViewDto> tender);
    }
}
