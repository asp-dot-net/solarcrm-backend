﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Tender.Dto;
using solarcrm.DataVaults.Tender.Exporting;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Common;

namespace solarcrm.DataVaults.Tender
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Tender)]
    public class TenderAppService : solarcrmAppServiceBase, ITenderAppService
    {
        private readonly IRepository<Tender> _tenderRepository;
        private readonly ITenderExcelExporter _tenderExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<SolarType.SolarType> _solarTypeRepository;

        public TenderAppService(IRepository<Tender> tenderRepository, ITenderExcelExporter tenderExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<SolarType.SolarType> solarTypeRepository)
        {
            _tenderRepository = tenderRepository;
            _tenderExcelExporter = tenderExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _solarTypeRepository = solarTypeRepository;
        }
               
        public async Task<PagedResultDto<GetTenderForViewDto>> GetAll(GetAllTenderInput input)
        {
            var filteredDivision = _tenderRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredDivision = filteredDivision.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var tenderResult = from o in pagedAndFilteredDivision
                           select new GetTenderForViewDto()
                           {
                               tender = new TenderDto
                               {
                                   Name = o.Name,
                                   SolarType = o.SolarTypeIdFk.Name,
                                   StartDate = o.StartDate,
                                   EndDate = o.EndDate,
                                   IsActive = o.IsActive,
                                   Id = o.Id,
                                   CreatedDate = o.CreationTime
                               }
                           };

            var totalCount = await filteredDivision.CountAsync();

            return new PagedResultDto<GetTenderForViewDto>(totalCount, await tenderResult.ToListAsync());
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Tender_Edit)]
        public async Task<GetTenderForEditOutput> GetTenderForEdit(EntityDto input)
        {
            var tender = await _tenderRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTenderForEditOutput { tender = ObjectMapper.Map<CreateOrEditTenderDto>(tender) };

            return output;
        }
        public async Task<GetTenderForViewDto> GetTenderForView(int id)
        {
            var tender = await _tenderRepository.GetAsync(id);

            var output = new GetTenderForViewDto { tender = ObjectMapper.Map<TenderDto>(tender) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Tender_ExportToExcel)]
        public async Task<FileDto> GetTenderToExcel(GetAllTenderForExcelInput input)
        {
            var filteredTender = _tenderRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredTender
                         select new GetTenderForViewDto()
                         {
                             tender = new TenderDto
                             {
                                 Name = o.Name,
                                 SolarType = o.SolarTypeIdFk.Name,
                                 StartDate = o.StartDate,
                                 EndDate = o.EndDate,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                             }
                         });

            var TenderListDtos = await query.ToListAsync();

            return _tenderExcelExporter.ExportToFile(TenderListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditTenderDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Tender_Create)]
        protected virtual async Task Create(CreateOrEditTenderDto input)
        {

            var tender = ObjectMapper.Map<Tender>(input);

            if (AbpSession.TenantId != null)
            {
                tender.TenantId = (int?)AbpSession.TenantId;
            }
            var tenderId = await _tenderRepository.InsertAndGetIdAsync(tender);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 31; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Tender Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = tenderId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Tender_Edit)]
        protected virtual async Task Update(CreateOrEditTenderDto input)
        {
            var tenderResult = await _tenderRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            int SectionId = 31; // Discom Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Tender Updated Name ;-" + tenderResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var divisionListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (tenderResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = tenderResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (tenderResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = tenderResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if (tenderResult.StartDate != input.StartDate)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "StartDate";
                dataVaultsHistory.PrevValue = tenderResult.StartDate.ToString();
                dataVaultsHistory.CurValue = input.StartDate.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if (tenderResult.EndDate != input.EndDate)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "EndDate";
                dataVaultsHistory.PrevValue = tenderResult.EndDate.ToString();
                dataVaultsHistory.CurValue = input.EndDate.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if (tenderResult.SolarTypeId != input.SolarTypeId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "SolarType";
                dataVaultsHistory.PrevValue = _solarTypeRepository.GetAll().Where(e=>e.Id==tenderResult.SolarTypeId).Select(e=>e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _solarTypeRepository.GetAll().Where(e => e.Id == input.SolarTypeId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, tenderResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Tender_Delete)]
        public async Task Delete(EntityDto input)
        {
            var tenderResult = await _tenderRepository.FirstOrDefaultAsync((int)input.Id);
            await _tenderRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }

            dataVaulteActivityLog.SectionId = 31; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Tender Deleted Name :-" + tenderResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
