﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.JobCancellationReason.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.JobCancellationReason.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.JobCancellationReason
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason)]
    public class JobCancellationReasonAppService : solarcrmAppServiceBase, IJobCancellationReasonAppService
    {
        private readonly IRepository<JobCancellationReason> _jobCancellationReasonRepository;
        private readonly IJobCancellationReasonExcelExporter _jobCancellationReasonExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public JobCancellationReasonAppService(IRepository<JobCancellationReason> JobCancellationReasonRepository, IJobCancellationReasonExcelExporter JobCancellationReasonExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _jobCancellationReasonRepository = JobCancellationReasonRepository;
            _jobCancellationReasonExcelExporter = JobCancellationReasonExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
     
        public async Task<PagedResultDto<GetJobCancellationReasonForViewDto>> GetAll(GetAllJobCancellationReasonInput input)
        {
            var filteredJobCancellationReason = _jobCancellationReasonRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredJobCancellationReason = filteredJobCancellationReason.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var jobCancellationReason = from o in pagedAndFilteredJobCancellationReason
                                        select new GetJobCancellationReasonForViewDto()
                         {
                             jobcancellationreason = new JobCancellationReasonDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime,

                             }
                         };

            var totalCount = await filteredJobCancellationReason.CountAsync();

            return new PagedResultDto<GetJobCancellationReasonForViewDto>(totalCount, await jobCancellationReason.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_Edit)]
        public async Task<GetJobCancellationReasonForEditOutput> GetJobCancellationReasonForEdit(EntityDto input)
        {
            var jobCancellationReason = await _jobCancellationReasonRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetJobCancellationReasonForEditOutput { jobcancellationreason = ObjectMapper.Map<CreateOrEditJobCancellationReasonDto>(jobCancellationReason) };

            return output;
        }

        public async Task<GetJobCancellationReasonForViewDto> GetJobCancellationReasonForView(int id)
        {
            var jobCancellationReason = await _jobCancellationReasonRepository.GetAsync(id);

            var output = new GetJobCancellationReasonForViewDto { jobcancellationreason = ObjectMapper.Map<JobCancellationReasonDto>(jobCancellationReason) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_ExportToExcel)]
        public async Task<FileDto> GetJobCancellationReasonToExcel(GetAllJobCancellationReasonForExcelInput input)
        {
            var filteredJobCancellationReason = _jobCancellationReasonRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredJobCancellationReason
                         select new GetJobCancellationReasonForViewDto()
                         {
                             jobcancellationreason = new JobCancellationReasonDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var JobCancellationReasonListDtos = await query.ToListAsync();

            return _jobCancellationReasonExcelExporter.ExportToFile(JobCancellationReasonListDtos);
        }


        public async Task CreateOrEdit(CreateOrEditJobCancellationReasonDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_Create)]
        protected virtual async Task Create(CreateOrEditJobCancellationReasonDto input)
        {
            var jobCancellationReason = ObjectMapper.Map<JobCancellationReason>(input);

            if (AbpSession.TenantId != null)
            {
                jobCancellationReason.TenantId = (int?)AbpSession.TenantId;
            }

            var JobCancellationReasonId = await _jobCancellationReasonRepository.InsertAndGetIdAsync(jobCancellationReason);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 19; // JobCancellationReason Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Job Cancellation Reason Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = JobCancellationReasonId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_Edit)]
        protected virtual async Task Update(CreateOrEditJobCancellationReasonDto input)
        {
            var jobCancellationReason = await _jobCancellationReasonRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 19; // JobCancellationReason Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Job Cancellation Reason Updated Name :-" + jobCancellationReason.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var jobCancellationReasonActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (jobCancellationReason.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = jobCancellationReason.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = jobCancellationReasonActivityLogId;
                dataVaultsHistory.SectionId = 19; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (jobCancellationReason.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = jobCancellationReason.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = jobCancellationReasonActivityLogId;
                dataVaultsHistory.SectionId = 19;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, jobCancellationReason);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_Delete)]
        public async Task Delete(EntityDto input)
        {
            var jobcancelResult = await _jobCancellationReasonRepository.FirstOrDefaultAsync(input.Id);
            await _jobCancellationReasonRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 19; // JobCancellationReason Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Job Cancellation Reason Deleted Name :-" + jobcancelResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
