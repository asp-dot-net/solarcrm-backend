﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.JobCancellationReason.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.JobCancellationReason.Exporting
{
    public class JobCancellationReasonExcelExporter : NpoiExcelExporterBase, IJobCancellationReasonExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public JobCancellationReasonExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetJobCancellationReasonForViewDto> leadType)
        {
            return CreateExcelPackage(
                "JobCancellationReason.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("State"));

                    AddHeader(
                        sheet,
                        L("JobCancellationReason")
                        );
                    

                    AddObjects(
                        sheet, leadType,
                        _ => _.jobcancellationreason.Name
                        );
                });
        }
    }
}
