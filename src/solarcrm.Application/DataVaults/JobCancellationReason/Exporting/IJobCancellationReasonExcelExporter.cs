﻿using solarcrm.DataVaults.JobCancellationReason.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.JobCancellationReason.Exporting
{
    public interface IJobCancellationReasonExcelExporter
    {
        FileDto ExportToFile(List<GetJobCancellationReasonForViewDto> jobCancellationReason);
    }
}
