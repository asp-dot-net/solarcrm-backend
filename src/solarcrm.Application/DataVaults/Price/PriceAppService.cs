﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Division.Exporting;
using solarcrm.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using Abp.Authorization;
using solarcrm.Authorization;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Price.Exporting;
using solarcrm.DataVaults.Price.Dto;
using solarcrm.DataVaults.Prices;
using solarcrm.Common;
using solarcrm.DataVaults.StockItem;

namespace solarcrm.DataVaults.Price
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Prices)]
    public class PriceAppService : solarcrmAppServiceBase, IPriceAppService
    {
        private readonly IRepository<Prices> _priceRepository;
        private readonly IRepository<PriceMultiAreaType> _priceMultiAreaTypeRepository;
        private readonly IRepository<Tender.Tender> _tenderRepository;
        private readonly IRepository<PriceMultiAreaType> _priceMultiAreaRepository;
        private readonly IPriceExcelExporter _priceExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public PriceAppService(IRepository<Prices> priceRepository, IPriceExcelExporter priceExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IRepository<PriceMultiAreaType> priceMultiAreaTypeRepository,
        IRepository<Tender.Tender> tenderRepository,
        IRepository<PriceMultiAreaType> priceMultiAreaRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _priceRepository = priceRepository;
            _priceExcelExporter = priceExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _priceMultiAreaRepository = priceMultiAreaRepository;
            _priceMultiAreaTypeRepository = priceMultiAreaTypeRepository;
            _tenderRepository = tenderRepository;
            _dbContextProvider = dbContextProvider;
        }


        public async Task<PagedResultDto<GetPriceForViewDto>> GetAll(GetAllPriceInput input)
        {
            var filteredPrice = _priceRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.TenderIdFk.Name.Contains(input.Filter));

            var pagedAndFilteredPrice = filteredPrice.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var price = from o in pagedAndFilteredPrice
                        select new GetPriceForViewDto()
                        {
                            price = new PriceDto
                            {
                                TenderName = o.TenderIdFk.Name,
                                CentralRate = o.CentralRate,
                                StateRate = o.StateRate,
                                SystemSize = o.SystemSize,
                                ActualPrice = o.ActualPrice,
                                Subcidy40 = o.Subcidy40,
                                Subcidy20 = o.Subcidy20,
                                MeterType = o.MeterType,
                                IsActive = o.IsActive,
                                Id = o.Id,
                                CreatedDate = o.CreationTime,
                                AreaTypeName = _priceMultiAreaRepository.GetAll().Where(x => x.PriceID == o.Id).Select(x => x.PriceAreaTypeMulti).ToList()
                            }
                        };

            var totalCount = await filteredPrice.CountAsync();

            return new PagedResultDto<GetPriceForViewDto>(totalCount, await price.ToListAsync());
        }

        public async Task<GetPriceForViewDto> GetPriceForView(int id)
        {
            var price = await _priceRepository.GetAsync(id);

            var output = new GetPriceForViewDto { price = ObjectMapper.Map<PriceDto>(price) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Prices_Edit)]
        public async Task<GetPriceForEditOutput> GetPriceForEdit(EntityDto input)
        {
            var price = await _priceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPriceForEditOutput { price = ObjectMapper.Map<CreateOrEditPriceDto>(price) };
            output.price.AreaTypeList = _priceMultiAreaRepository.GetAll().Where(x => x.PriceID == input.Id).Select(x => x.PriceAreaTypeMulti).ToList();
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Prices_ExportToExcel)]
        public async Task<FileDto> GetPriceToExcel(GetAllPriceForExcelInput input)
        {
            var filteredDiscom = _priceRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.TenderIdFk.Name.Contains(input.Filter));

            var query = (from o in filteredDiscom
                         select new GetPriceForViewDto()
                         {
                             price = new PriceDto
                             {
                                 TenderName = o.TenderIdFk.Name,
                                 CentralRate = o.CentralRate,
                                 StateRate = o.StateRate,
                                 SystemSize = o.SystemSize,
                                 ActualPrice = o.ActualPrice,
                                 Subcidy40 = o.Subcidy40,
                                 Subcidy20 = o.Subcidy20,
                                 MeterType = o.MeterType,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var PriceListDtos = await query.ToListAsync();

            return _priceExcelExporter.ExportToFile(PriceListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditPriceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Create)]
        protected virtual async Task Create(CreateOrEditPriceDto input)
        {
            var division = ObjectMapper.Map<Prices>(input);

            if (AbpSession.TenantId != null)
            {
                division.TenantId = (int?)AbpSession.TenantId;
            }

            var priceId = await _priceRepository.InsertAndGetIdAsync(division);
            foreach (var item in input.AreaTypeList)
            {
                try
                {
                    PriceMultiAreaType areaType = new PriceMultiAreaType();
                    areaType.PriceID = priceId;
                    areaType.PriceAreaTypeMulti = item;
                    await _priceMultiAreaRepository.InsertOrUpdateAsync(areaType);
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 32; // Price Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Price Created Name :-" + input.SystemSize;
            dataVaulteActivityLog.SectionValueId = priceId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Division_Edit)]
        protected virtual async Task Update(CreateOrEditPriceDto input)
        {
            var price = await _priceRepository.FirstOrDefaultAsync((int)input.Id);
            var AreaTypeList = _priceMultiAreaRepository.GetAll().Where(x => x.PriceID == price.Id).Select(e => e.PriceAreaTypeMulti).ToList();
            string inputAreaType = "";
            string priceAreaType = "";
            foreach (var areaType in input.AreaTypeList)
            {
                if (inputAreaType == "")
                {
                    inputAreaType = areaType;
                }
                else
                {
                    inputAreaType = inputAreaType + ", " + areaType;
                }
            }
            foreach (var areaType in AreaTypeList)
            {
                if (priceAreaType == "")
                {
                    priceAreaType = areaType;
                }
                else
                {
                    priceAreaType = priceAreaType + ", " + areaType;
                }
            }
            //Add Activity Log
            int SectionId = 32; // Prices Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }

            foreach (var item in input.AreaTypeList)
            {
                try
                {
                    PriceMultiAreaType areaType = new PriceMultiAreaType();

                    var checkAreType = _priceMultiAreaRepository.GetAll().Where(x => x.PriceID == price.Id && x.PriceAreaTypeMulti == item).FirstOrDefault();
                    if (checkAreType != null)
                    {
                        checkAreType.PriceID = price.Id;
                        checkAreType.PriceAreaTypeMulti = item;
                        await _priceMultiAreaRepository.InsertOrUpdateAsync(checkAreType);
                    }
                    else
                    {
                        areaType.PriceID = price.Id;
                        areaType.PriceAreaTypeMulti = item;
                        await _priceMultiAreaRepository.InsertOrUpdateAsync(areaType);
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
            var checkprice1 = _priceMultiAreaRepository.GetAll().Where(x => x.PriceID == price.Id && !input.AreaTypeList.Contains(x.PriceAreaTypeMulti)).ToList();
            if (checkprice1.Count() > 0)
            {
                foreach (var item in checkprice1)
                {
                    await _priceMultiAreaRepository.DeleteAsync(item);
                }
            }

            dataVaulteActivityLog.SectionId = SectionId; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Price Updated";
            dataVaulteActivityLog.SectionValueId = input.Id;
            var divisionListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (price.TenderId != input.TenderId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Tender";
                dataVaultsHistory.PrevValue = _tenderRepository.GetAll().Where(e => e.Id == price.TenderId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _tenderRepository.GetAll().Where(e => e.Id == input.TenderId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (price.SystemSize != input.SystemSize)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "SystemSize";
                dataVaultsHistory.PrevValue = price.SystemSize.ToString();
                dataVaultsHistory.CurValue = input.SystemSize.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (price.CentralRate != input.CentralRate)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "CentralRate";
                dataVaultsHistory.PrevValue = price.CentralRate.ToString();
                dataVaultsHistory.CurValue = input.CentralRate.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (price.StateRate != input.StateRate)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "StateRate";
                dataVaultsHistory.PrevValue = price.StateRate.ToString();
                dataVaultsHistory.CurValue = input.StateRate.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (price.ActualPrice != input.ActualPrice)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "ActualPrice";
                dataVaultsHistory.PrevValue = price.ActualPrice.ToString();
                dataVaultsHistory.CurValue = input.ActualPrice.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (price.Subcidy40 != input.Subcidy40)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Subcidy40";
                dataVaultsHistory.PrevValue = price.Subcidy40.ToString();
                dataVaultsHistory.CurValue = input.Subcidy40.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (price.Subcidy20 != input.Subcidy20)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Subcidy20";
                dataVaultsHistory.PrevValue = price.Subcidy20.ToString();
                dataVaultsHistory.CurValue = input.Subcidy20.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (priceAreaType != inputAreaType)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "AreaType";
                dataVaultsHistory.PrevValue = priceAreaType;
                dataVaultsHistory.CurValue = inputAreaType;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (price.MeterType != input.MeterType)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "MeterType";
                dataVaultsHistory.PrevValue = price.MeterType == 1 ? "Yes" : "No";
                dataVaultsHistory.CurValue = input.MeterType == 1 ? "Yes" : "No";
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (price.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = price.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = divisionListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }



            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, price);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Prices_Delete)]
        public async Task Delete(EntityDto input)
        {
            var priceResult = await _priceRepository.FirstOrDefaultAsync(input.Id);
            await _priceRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 32; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Created
            dataVaulteActivityLog.ActionNote = "Price Deleted Name :-" + priceResult.SystemSize;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<GetPriceForViewDto> GetPriceBySystemSize(GetPriceInput input)
        {
            GetPriceForViewDto output = new GetPriceForViewDto();
            GetPriceForViewDto priseresult = new GetPriceForViewDto();        
            List<Prices> pricefilter = await _priceRepository.GetAll().Where(e => e.SystemSize == input.SystemSize).ToListAsync();
            if (pricefilter != null && pricefilter.Count() > 0 && !string.IsNullOrEmpty(input.AreaType))
            {
                priseresult = (from o in pricefilter

                               join o3 in _priceMultiAreaRepository.GetAll() on o.Id equals o3.PriceID into j3
                               from s3 in j3.DefaultIfEmpty()

                               join o2 in _tenderRepository.GetAll() on o.TenderId equals o2.Id into j2
                               from s2 in j2.DefaultIfEmpty()

                               where (o.SystemSize == input.SystemSize && s3.PriceAreaTypeMulti.ToLower().Contains(input.AreaType.ToLower()) && (s2.StartDate.Date <= DateTime.UtcNow.Date && s2.EndDate.Date >= DateTime.UtcNow.Date)
                               && o.IsActive)

                               select new GetPriceForViewDto()
                               {
                                   productPriceDto = new ProductPriceDto
                                   {
                                       CentralRate = o.CentralRate,
                                       StateRate = o.StateRate,
                                       SystemSize = o.SystemSize,
                                       ActualPrice = o.ActualPrice,
                                       Subcidy20 = o.Subcidy20,
                                       Subcidy40 = o.Subcidy40,
                                       MeterType = o.MeterType,
                                   }
                               }
                      ).FirstOrDefault(); //Where( e => e.price.SystemSize == input.SystemSize && e.price.StartDate <= DateTime.UtcNow && e.TenderIdFk.EndDate >= DateTime.UtcNow).FirstOrDefault();
                output = new GetPriceForViewDto { productPriceDto = ObjectMapper.Map<ProductPriceDto>(priseresult == null ? priseresult : priseresult.productPriceDto) };
            }
            return output;
        }
    }
}
