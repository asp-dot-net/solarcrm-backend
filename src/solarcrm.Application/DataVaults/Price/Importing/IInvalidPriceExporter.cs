﻿using solarcrm.DataVaults.Price.Importing.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Importing
{
    public interface IInvalidPriceExporter
    {
        FileDto ExportToFile(List<ImportPriceDto> priceListDtos);
    }
}
