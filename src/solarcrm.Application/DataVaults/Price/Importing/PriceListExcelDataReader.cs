﻿using Abp.Localization;
using Abp.Localization.Sources;
using NPOI.SS.UserModel;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Price.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Importing
{
    public class PriceListExcelDataReader : NpoiExcelImporterBase<ImportPriceDto>, IPriceListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public PriceListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(solarcrmConsts.LocalizationSourceName);
        }

        public List<ImportPriceDto> GetPriceFromExcel(byte[] fileBytes , string filename)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow, filename);
        }

		private ImportPriceDto ProcessExcelRow(ISheet worksheet, int row)
		{
			if (IsRowEmpty(worksheet, row))
			{
				return null;
			}

			var exceptionMessage = new StringBuilder();
			var price = new ImportPriceDto();

			IRow roww = worksheet.GetRow(row);
			List<ICell> cells = roww.Cells;
			List<string> rowData = new List<string>();

			for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
			{
				ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
				rowData.Add(cell.ToString());
			}

			try
			{
				price.TenderName = rowData[0].Trim();
				price.SystemSize = rowData[1].Trim();
				price.CentralRate = rowData[2].Trim();
                price.StateRate = rowData[3].Trim();
                price.ActualPrice = rowData[4].Trim();
				price.Subcidy40 = rowData[5].Trim();
				price.Subcidy20 = rowData[6].Trim();
				price.MeterType = rowData[7].Trim();
			}
			catch (System.Exception exception)
			{
				price.Exception = exception.Message;
			}

			return price;
		}

		private string GetLocalizedExceptionMessagePart(string parameter)
		{
			return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
		}

		//private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		//{
		//	var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
		//	if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
		//	{
		//		return cellValue;
		//	}

		//	exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
		//	return null;
		//}

		//private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
		//{
		//	var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

		//	return cellValue;

		//}

		private bool IsRowEmpty(ISheet worksheet, int row)
		{
			var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
			return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
		}
	}
}
