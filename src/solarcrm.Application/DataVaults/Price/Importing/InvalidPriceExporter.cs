﻿using Abp.Dependency;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Price.Importing.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Importing
{
    public class InvalidPriceExporter : NpoiExcelExporterBase, IInvalidPriceExporter, ITransientDependency
    {
        public InvalidPriceExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportPriceDto> priceListDtos)
        {
            return CreateExcelPackage(
                "InvalidPriceImportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InvalidPriceImports"));

                    AddHeader(
                        sheet,
                        L("TenderName"),
                        L("SystemSize"),
                        L("CentralRate"),
                        L("StateRate"),
                        L("ActualPrice"),
                        L("Subcidy40"),
                        L("Subcidy20"),
                        L("MeterType"),
                        L("Refuse Reason")
                    );

                    AddObjects(
                        sheet, priceListDtos,
                        _ => _.TenderName,
                        _ => _.SystemSize,
                        _ => _.CentralRate,
                         _ => _.StateRate,
                        _ => _.ActualPrice,
                        _ => _.Subcidy40,
                        _ => _.Subcidy20,
                        _ => _.MeterType,
                        _ => _.Exception
                    );

                    for (var i = 0; i < 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
