﻿using Abp.Dependency;
using solarcrm.DataVaults.Price.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Importing
{
    public interface IPriceListExcelDataReader : ITransientDependency
    {
        List<ImportPriceDto> GetPriceFromExcel(byte[] fileBytes, string filename);
    }
}
