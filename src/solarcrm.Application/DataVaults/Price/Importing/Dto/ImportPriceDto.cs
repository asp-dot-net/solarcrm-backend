﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Importing.Dto
{
    public class ImportPriceDto
    {
        public string TenderName { get; set; }

        public string SystemSize { get; set; }

        public string CentralRate { get; set; }
        public string StateRate { get; set; }

        public string ActualPrice { get; set; }

        public string Subcidy40 { get; set; }

        public string Subcidy20 { get; set; }

        public string MeterType { get; set; }

        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
