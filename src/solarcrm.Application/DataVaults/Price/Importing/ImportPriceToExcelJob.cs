﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Runtime.Session;
using Abp.Threading;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.Price.Dto;
using solarcrm.DataVaults.Price.Importing.Dto;
using solarcrm.DataVaults.StockItem.Importing.Dto;
using solarcrm.DataVaults.StockItemLocation.Dto;
using solarcrm.DataVaults.Tender;
using solarcrm.EntityFrameworkCore;
using solarcrm.Notifications;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Importing
{
    public class ImportPriceToExcelJob : BackgroundJob<ImportPriceFromExcelJobArgs>, ITransientDependency
    {
        private readonly IRepository<Prices> _priceRepository;
        private readonly IPriceListExcelDataReader _priceListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidPriceExporter _invalidPriceExporter;
        private readonly IAbpSession _abpSession;
        //private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Tender.Tender> _tenderRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private int _sectionId = 31; // Pages Section Name

        public ImportPriceToExcelJob(
            IRepository<Prices> priceRepository,
            IPriceListExcelDataReader priceListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidPriceExporter invalidPriceExporter,
            IAbpSession abpSession,
            //IRepository<User, long> userRepository,
            IRepository<Tender.Tender> tenderRepository,
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider
            )
        {
            _priceRepository = priceRepository;
            _priceListExcelDataReader = priceListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _unitOfWorkManager = unitOfWorkManager;
            _invalidPriceExporter = invalidPriceExporter;
            _abpSession = abpSession;
            //_userRepository = userRepository;
            _tenderRepository = tenderRepository;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        [UnitOfWork]
        public override void Execute(ImportPriceFromExcelJobArgs args)
        {
            var Prices = GetPriceListFromExcelOrNull(args);
            if (Prices == null || !Prices.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var invalidPrice = new List<ImportPriceDto>();

                foreach (var price in Prices)
                {
                    if (price.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateOrUpdatePriceAsync(price, args));
                        }
                        //catch (UserFriendlyException exception)
                        //{
                        //    invalidPrice.Add(price);
                        //}
                        //catch (Exception e)
                        catch
                        {
                            invalidPrice.Add(price);
                        }
                    }
                    else
                    {
                        invalidPrice.Add(price);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportPriceResultAsync(args, invalidPrice));
                    }

                    uow.Complete();
                }
            }
        }

        private List<ImportPriceDto> GetPriceListFromExcelOrNull(ImportPriceFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        string filename = Path.GetExtension(file.Description);
                        return _priceListExcelDataReader.GetPriceFromExcel(file.Bytes, filename);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateOrUpdatePriceAsync(ImportPriceDto input, ImportPriceFromExcelJobArgs args)
        {
            Prices prices = new Prices();

            var TenderId = 0;
            using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
            {
                if (input.TenderName != null)
                {
                    TenderId = await _tenderRepository.GetAll().Where(e => e.Name.ToUpper() == input.TenderName.ToUpper()).Select(e => e.Id).FirstOrDefaultAsync();
                }

                if (TenderId == 0)
                {
                    input.Exception = "Tender Name Not Found";
                    throw new Exception("Tender Name Not Found");
                }

                decimal SystemSize = 0;
                if (!string.IsNullOrEmpty(input.SystemSize.Trim()))
                {
                    try
                    {
                        SystemSize = Convert.ToDecimal(input.SystemSize.Trim());
                    }
                    catch
                    {
                        input.Exception = "Invalid System Size";
                        throw new Exception("Invalid System Size");
                    }
                }

                decimal CentralRate = 0;
                if (!string.IsNullOrEmpty(input.CentralRate.Trim()))
                {
                    try
                    {
                        CentralRate = Convert.ToDecimal(input.CentralRate.Trim());
                    }
                    catch
                    {
                        input.Exception = "Invalid CentralRate";
                        throw new Exception("Invalid CentralRate");
                    }
                }
                decimal StateRate = 0;
                if (!string.IsNullOrEmpty(input.StateRate.Trim()))
                {
                    try
                    {
                        StateRate = Convert.ToDecimal(input.StateRate.Trim());
                    }
                    catch
                    {
                        input.Exception = "Invalid StateRate";
                        throw new Exception("Invalid StateRate");
                    }
                }
                decimal ActualPrice = 0;
                if (!string.IsNullOrEmpty(input.ActualPrice.Trim()))
                {
                    try
                    {
                        ActualPrice = Convert.ToDecimal(input.ActualPrice.Trim());
                    }
                    catch
                    {
                        input.Exception = "Invalid Actual Price";
                        throw new Exception("Invalid Actual Price");
                    }
                }

                decimal Subcidy40 = 0;
                if (!string.IsNullOrEmpty(input.Subcidy40.Trim()))
                {
                    try
                    {
                        Subcidy40 = Convert.ToDecimal(input.Subcidy40.Trim());
                    }
                    catch
                    {
                        input.Exception = "Invalid Subcidy40";
                        throw new Exception("Invalid Subcidy40");
                    }
                }

                decimal Subcidy20 = 0;
                if (!string.IsNullOrEmpty(input.Subcidy20.Trim()))
                {
                    try
                    {
                        Subcidy20 = Convert.ToDecimal(input.Subcidy20.Trim());
                    }
                    catch
                    {
                        input.Exception = "Invalid Subcidy20";
                        throw new Exception("Invalid Subcidy20");
                    }
                }

                int? MeterType = null;
                if (!string.IsNullOrEmpty(input.MeterType.Trim()))
                {
                    try
                    {
                        MeterType = input.Subcidy20.Trim().ToLower() == "yes" ? 1 : input.Subcidy20.Trim().ToLower() == "no" ? 0 : null;
                    }
                    catch
                    {
                        input.Exception = "Invalid Meter Type";
                        throw new Exception("Invalid Meter Type");
                    }
                }

                prices = await _priceRepository.GetAll().Where(e => e.SystemSize == SystemSize).FirstOrDefaultAsync();
                if (prices == null)
                {
                    var PriceNew = _objectMapper.Map<Prices>(input);
                    PriceNew.TenantId = (int)args.TenantId;
                    PriceNew.TenderId = TenderId;
                    PriceNew.SystemSize = SystemSize;
                    PriceNew.CentralRate = CentralRate;
                    PriceNew.StateRate = StateRate;
                    PriceNew.ActualPrice = ActualPrice;
                    PriceNew.Subcidy40 = Subcidy40;
                    PriceNew.Subcidy20 = Subcidy20;
                    PriceNew.MeterType = MeterType;
                    PriceNew.IsActive = true;
                    PriceNew.CreatorUserId = (int)args.User.UserId;

                    var PriceId = await _priceRepository.InsertAndGetIdAsync(PriceNew);
                    //Add Activity Log
                    DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                    dataVaulteActivityLog.TenantId = (int)args.TenantId;
                    dataVaulteActivityLog.SectionId = _sectionId;
                    dataVaulteActivityLog.ActionId = 1; //1-Created, 2-Updated, 3-Deleted
                    dataVaulteActivityLog.ActionNote = "Price Created From Excel";
                    dataVaulteActivityLog.SectionValueId = PriceId;
                    dataVaulteActivityLog.CreatorUserId = (int)args.User.UserId;
                    await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);

                }
                else
                {
                    //Add Activity Log
                    DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                    dataVaulteActivityLog.TenantId = (int)args.TenantId;
                    dataVaulteActivityLog.SectionId = _sectionId; // stock category Pages Name
                    dataVaulteActivityLog.ActionId = 2; // Updated
                    dataVaulteActivityLog.ActionNote = "Price Updated";
                    dataVaulteActivityLog.SectionValueId = prices.Id;
                    dataVaulteActivityLog.CreatorUserId = (int)args.User.UserId;
                    var PriceActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

                    #region Added Log History
                    var List = new List<DataVaultsHistory>();
                    if (prices.TenderId != TenderId)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "TenderId";
                        dataVaultsHistory.PrevValue = prices.TenderId.ToString();
                        dataVaultsHistory.CurValue = TenderId.ToString();
                        dataVaultsHistory.Action = "Edited from Excel";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = PriceActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = prices.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (prices.CentralRate != CentralRate)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "CentralRate";
                        dataVaultsHistory.PrevValue = prices.CentralRate.ToString();
                        dataVaultsHistory.CurValue = CentralRate.ToString();
                        dataVaultsHistory.Action = "Edited from Excel";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = PriceActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = prices.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (prices.StateRate != StateRate)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "StateRate";
                        dataVaultsHistory.PrevValue = prices.StateRate.ToString();
                        dataVaultsHistory.CurValue = StateRate.ToString();
                        dataVaultsHistory.Action = "Edited from Excel";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = PriceActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = prices.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (prices.ActualPrice != ActualPrice)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "ActualPrice";
                        dataVaultsHistory.PrevValue = prices.ActualPrice.ToString();
                        dataVaultsHistory.CurValue = ActualPrice.ToString();
                        dataVaultsHistory.Action = "Edited from Excel";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = PriceActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = prices.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (prices.Subcidy40 != Subcidy40)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "Subcidy40";
                        dataVaultsHistory.PrevValue = prices.Subcidy40.ToString();
                        dataVaultsHistory.CurValue = Subcidy40.ToString();
                        dataVaultsHistory.Action = "Edited from Excel";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = PriceActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = prices.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (prices.Subcidy20 != Subcidy20)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "Subcidy20";
                        dataVaultsHistory.PrevValue = prices.Subcidy20.ToString();
                        dataVaultsHistory.CurValue = Subcidy20.ToString();
                        dataVaultsHistory.Action = "Edited from Excel";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = PriceActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = prices.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }
                    if (prices.MeterType != MeterType)
                    {
                        DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                        dataVaultsHistory.TenantId = (int)args.TenantId;
                        dataVaultsHistory.FieldName = "MeterType";
                        dataVaultsHistory.PrevValue = prices.MeterType.ToString();
                        dataVaultsHistory.CurValue = MeterType.ToString();
                        dataVaultsHistory.Action = "Edited from Excel";
                        dataVaultsHistory.ActionId = 2;
                        dataVaultsHistory.ActivityLogId = PriceActivityLogId;
                        dataVaultsHistory.SectionId = _sectionId;
                        dataVaultsHistory.SectionValueId = prices.Id;
                        dataVaultsHistory.CreatorUserId = (int)args.User.UserId;
                        List.Add(dataVaultsHistory);
                    }

                    await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
                    await _dbContextProvider.GetDbContext().SaveChangesAsync();

                    #endregion

                    prices.TenderId = TenderId;
                    prices.CentralRate = CentralRate;
                    prices.StateRate = StateRate;
                    prices.ActualPrice = ActualPrice;
                    prices.Subcidy40 = Subcidy40;
                    prices.Subcidy20 = Subcidy20;
                    prices.MeterType = MeterType;
                    prices.IsActive = true;
                    prices.LastModifierUserId = (int)args.User.UserId;

                    await _priceRepository.UpdateAsync(prices);
                }
            }
        }

        private void SendInvalidExcelNotification(ImportPriceFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToPrice", solarcrmConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }

        private async Task ProcessImportPriceResultAsync(ImportPriceFromExcelJobArgs args, List<ImportPriceDto> invalidPrice)
        {
            if (invalidPrice.Any())
            {
                var file = _invalidPriceExporter.ExportToFile(invalidPrice);
                await _appNotifier.SomePriceCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllPriceSuccessfullyImportedFromExcel", solarcrmConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }

    }
}
