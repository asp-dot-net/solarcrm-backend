﻿using solarcrm.DataVaults.Price.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Exporting
{
    public interface IPriceExcelExporter
    {
        FileDto ExportToFile(List<GetPriceForViewDto> price);
    }
}
