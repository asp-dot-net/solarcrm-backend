﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Price.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price.Exporting
{
    public class PriceExcelExporter : NpoiExcelExporterBase, IPriceExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PriceExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetPriceForViewDto> price)
        {
            return CreateExcelPackage(
                "Price.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Price"));

                    AddHeader(
                        sheet,
                        L("TenderName"),
                        L("SystemSize"),
                        L("CentralRate"),
                        L("StateRate"),
                        L("ActualPrice"),
                        L("Subcidy40"),
                        L("Subcidy20"),
                        L("MeterType")
                        );

                    AddObjects(
                        sheet, price,
                        _ => _.price.TenderName,
                        _ => _.price.SystemSize,
                        _ => _.price.CentralRate,
                        _ => _.price.StateRate,
                        _ => _.price.ActualPrice,
                        _ => _.price.Subcidy40,
                        _ => _.price.Subcidy20,
                        _ => _.price.MeterType
                        );
                });
        }
    }
}
