﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.InvoiceType.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.InvoiceType.Exporting
{
    public class InvoiceTypeExcelExporter : NpoiExcelExporterBase, IInvoiceTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public InvoiceTypeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetInvoiceTypeForViewDto> ProjectType)
        {
            return CreateExcelPackage(
                "InvoiceType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InvoiceType"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, ProjectType,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, ProjectType,
                        _ => _.InvoiceType.Name
                        );
                });
        }
    }
}
