﻿using solarcrm.DataVaults.InvoiceType.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.InvoiceType.Exporting
{
    public interface IInvoiceTypeExcelExporter
    {
        FileDto ExportToFile(List<GetInvoiceTypeForViewDto> invoicetype);
    }
}
