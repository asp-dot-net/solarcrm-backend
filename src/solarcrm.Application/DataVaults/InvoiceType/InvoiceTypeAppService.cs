﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.InvoiceType.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.InvoiceType.Exporting;
using solarcrm.Common;

namespace solarcrm.DataVaults.InvoiceType
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InvoiceType)]
    public class InvoiceTypeAppService : solarcrmAppServiceBase, IInvoiceTypeAppService
    {
        private readonly IRepository<InvoiceType> _InvoiceTypeRepository;
        private readonly IInvoiceTypeExcelExporter _InvoiceTypeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public InvoiceTypeAppService(IRepository<InvoiceType> InvoiceTypeRepository, IInvoiceTypeExcelExporter InvoiceTypeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _InvoiceTypeRepository = InvoiceTypeRepository;
            _InvoiceTypeExcelExporter = InvoiceTypeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
                       
        public async Task<PagedResultDto<GetInvoiceTypeForViewDto>> GetAll(GetAllInvoiceTypeInput input)
        {
            var filteredinvoicetype = _InvoiceTypeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredinvoiceType = filteredinvoicetype.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var invoicetype = from o in pagedAndFilteredinvoiceType
                              select new GetInvoiceTypeForViewDto()
                             {
                                  InvoiceType = new InvoiceTypeDto
                                 {
                                     Name = o.Name,
                                     IsActive = o.IsActive,
                                     Id = o.Id,
                                     CreatedDate = o.CreationTime
                                 }
                             };

            var totalCount = await filteredinvoicetype.CountAsync();

            return new PagedResultDto<GetInvoiceTypeForViewDto>(totalCount, await invoicetype.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_Edit)]
        public async Task<GetInvoiceTypeForEditOutput> GetInvoiceTypeForEdit(EntityDto input)
        {
            var InvoiceType = await _InvoiceTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetInvoiceTypeForEditOutput { InvoiceType = ObjectMapper.Map<CreateOrEditInvoiceTypeDto>(InvoiceType) };

            return output;
        }

        public async Task<GetInvoiceTypeForViewDto> GetInvoiceTypeForView(int id)
        {
            var InvoiceType = await _InvoiceTypeRepository.GetAsync(id);

            var output = new GetInvoiceTypeForViewDto { InvoiceType = ObjectMapper.Map<InvoiceTypeDto>(InvoiceType) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_ExportToExcel)]
        public async Task<FileDto> GetInvoiceTypeToExcel(GetAllInvoiceTypeForExcelInput input)
        {
            var filteredinvoiceType = _InvoiceTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredinvoiceType
                         select new GetInvoiceTypeForViewDto()
                         {
                             InvoiceType = new InvoiceTypeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var InvoiceTypeListDtos = await query.ToListAsync();

            return _InvoiceTypeExcelExporter.ExportToFile(InvoiceTypeListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditInvoiceTypeDto input)
        {

            // var response = client.Execute(request);
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_Create)]
        protected virtual async Task Create(CreateOrEditInvoiceTypeDto input)
        {
            var InvoiceType = ObjectMapper.Map<InvoiceType>(input);

            if (AbpSession.TenantId != null)
            {
                InvoiceType.TenantId = (int?)AbpSession.TenantId;
            }

            var InvoiceTypeId = await _InvoiceTypeRepository.InsertAndGetIdAsync(InvoiceType);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 9; // InvoiceType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Invoice Type Created Name :-" +input.Name;
            dataVaulteActivityLog.SectionValueId = InvoiceTypeId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_Edit)]
        protected virtual async Task Update(CreateOrEditInvoiceTypeDto input)
        {
            var InvoiceType = await _InvoiceTypeRepository.FirstOrDefaultAsync((int)input.Id);
            
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 9; // InvoiceType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Invoice Type Updated Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var invoicetypeActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if(InvoiceType.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = InvoiceType.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = invoicetypeActivityLogId;
                dataVaultsHistory.SectionId = 9;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if(InvoiceType.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = InvoiceType.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = invoicetypeActivityLogId;
                dataVaultsHistory.SectionId = 9;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, InvoiceType);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var invoiceTypeResult = await _InvoiceTypeRepository.FirstOrDefaultAsync(input.Id);
            await _InvoiceTypeRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 9; // InvoiceType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Invoice Type Deleted Name :- " + invoiceTypeResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
