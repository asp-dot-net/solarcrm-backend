﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.SmsTemplates.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.SmsTemplates.Exporting
{
    public class SmsTemplatesExcelExporter : NpoiExcelExporterBase, ISmsTemplatesExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SmsTemplatesExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetSmsTemplateForViewDto> smstemp)
        {
            return CreateExcelPackage(
                "SmsTemplates.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("SmsTemplates"));

                    AddHeader(
                       sheet,
                       L("Name"),
                       L("Text")
                       );

                    AddObjects(
                        sheet, smstemp,
                        _ => _.SmsTemplate.Name,
                         _ => _.SmsTemplate.Text
                        );
                });
        }
    }
}
