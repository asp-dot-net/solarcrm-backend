﻿using solarcrm.DataVaults.SmsTemplates.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.SmsTemplates.Exporting
{
    public interface ISmsTemplatesExcelExporter
    {
        FileDto ExportToFile(List<GetSmsTemplateForViewDto> cancelreasons);

    }
}
