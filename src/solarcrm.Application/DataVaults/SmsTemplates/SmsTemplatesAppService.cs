﻿
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.SmsTemplates.Exporting;
using Abp.Application.Services.Dto;
using TheSolarProduct.SmsTemplates;
using solarcrm.DataVaults.SmsTemplates.Dto;
using solarcrm.Common;
using Abp.Organizations;

namespace solarcrm.DataVaults.SmsTemplates
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates)]
    public class SmsTemplatesAppService : solarcrmAppServiceBase, ISmsTemplatesAppService
    {
        private readonly IRepository<SmsTemplates> _smsTemplatesRepository;
        private readonly ISmsTemplatesExcelExporter _smsTemplatesExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;

        public SmsTemplatesAppService(IRepository<SmsTemplates> SmsTemplatesRepository, ISmsTemplatesExcelExporter SmsTemplatesExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<OrganizationUnit, long> organizationUnitRepository)
        {
            _smsTemplatesRepository = SmsTemplatesRepository;
            _smsTemplatesExcelExporter = SmsTemplatesExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _organizationUnitRepository = organizationUnitRepository;
        }

        public async Task<PagedResultDto<GetSmsTemplateForViewDto>> GetAll(GetAllSmsTemplateInput input)
        {
            var filteredSmsTemplates = _smsTemplatesRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredSmsTemplates = filteredSmsTemplates.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var smsTemplates = from o in pagedAndFilteredSmsTemplates
                                select new GetSmsTemplateForViewDto()
                                {
                                    SmsTemplate = new SmsTemplateDto
                                    {
                                        Name = o.Name,
                                        Text = o.Text,
                                        IsActive = o.IsActive,
                                        Id = o.Id,
                                        CreatedDate = o.CreationTime,

                                    }
                                };

            var totalCount = await filteredSmsTemplates.CountAsync();

            return new PagedResultDto<GetSmsTemplateForViewDto>(totalCount, await smsTemplates.ToListAsync());
        }

        public async Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEdit(EntityDto input)
        {
            var smsTemplate = await _smsTemplatesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSmsTemplateForEditOutput { SmsTemplate = ObjectMapper.Map<CreateOrEditSmsTemplateDto>(smsTemplate) };

            return output;
        }

        public async Task<GetSmsTemplateForViewDto> GetSmsTemplateForView(int id)
        {
            var smsTemplate = await _smsTemplatesRepository.GetAsync(id);

            var output = new GetSmsTemplateForViewDto { SmsTemplate = ObjectMapper.Map<SmsTemplateDto>(smsTemplate) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Edit)]
        public async Task<GetSmsTemplateForEditOutput> GetSmsTemplatesForEdit(EntityDto input)
        {
            var smsTemplates = await _smsTemplatesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSmsTemplateForEditOutput { SmsTemplate = ObjectMapper.Map<CreateOrEditSmsTemplateDto>(smsTemplates) };

            return output;
        }
        public async Task<GetSmsTemplateForViewDto> GetSmsTemplatesForView(int id)
        {
            var smsTemplates = await _smsTemplatesRepository.GetAsync(id);

            var output = new GetSmsTemplateForViewDto { SmsTemplate = ObjectMapper.Map<SmsTemplateDto>(smsTemplates) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Create)]
        public async Task<FileDto> GetSmsTemplatesToExcel(GetAllSmsTemplatesForExcelInput input)
        {
            var filteredSmsTemplates = _smsTemplatesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredSmsTemplates
                         select new GetSmsTemplateForViewDto()
                         {
                             SmsTemplate = new SmsTemplateDto
                             {
                                 Name = o.Name,
                                 Text = o.Text,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var smsTemplatesListDtos = await query.ToListAsync();

            return _smsTemplatesExcelExporter.ExportToFile(smsTemplatesListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditSmsTemplateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Create)]
        protected virtual async Task Create(CreateOrEditSmsTemplateDto input)
        {
            try
            {
                var smsTemplates = ObjectMapper.Map<SmsTemplates>(input);

                if (AbpSession.TenantId != null)
                {
                    smsTemplates.TenantId = (int?)AbpSession.TenantId;
                }

                var smsTemplatesId = await _smsTemplatesRepository.InsertAndGetIdAsync(smsTemplates);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 23; // SmsTemplates Pages Name
                dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
                dataVaulteActivityLog.ActionNote = "SMS Template Created Name :-" +input.Name;
                dataVaulteActivityLog.SectionValueId = smsTemplatesId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Edit)]
        protected virtual async Task Update(CreateOrEditSmsTemplateDto input)
        {
            var smsTemplates = await _smsTemplatesRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 23; // SmsTemplates Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "SMS Template Updated Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var smsTemplatesActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (smsTemplates.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = smsTemplates.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = smsTemplatesActivityLogId;
                dataVaultsHistory.SectionId = 23; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (smsTemplates.OrganizationUnitId != input.OrganizationUnitId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "OrganizationUnit";
                dataVaultsHistory.PrevValue = _organizationUnitRepository.GetAll().Where(e => e.Id == smsTemplates.OrganizationUnitId).FirstOrDefault().DisplayName; 
                dataVaultsHistory.CurValue = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationUnitId).FirstOrDefault().DisplayName; 
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = smsTemplatesActivityLogId;
                dataVaultsHistory.SectionId = 23; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (smsTemplates.ProviderTemp_Id != input.ProviderTemp_Id)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "SMS TempId";
                dataVaultsHistory.PrevValue = smsTemplates.ProviderTemp_Id.ToString(); 
                dataVaultsHistory.CurValue = input.ProviderTemp_Id.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = smsTemplatesActivityLogId;
                dataVaultsHistory.SectionId = 23; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if (smsTemplates.Text != input.Text)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Text";
                dataVaultsHistory.PrevValue = smsTemplates.Text; 
                dataVaultsHistory.CurValue = input.Text;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = smsTemplatesActivityLogId;
                dataVaultsHistory.SectionId = 23; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (smsTemplates.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = smsTemplates.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = smsTemplatesActivityLogId;
                dataVaultsHistory.SectionId = 17;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, smsTemplates);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Delete)]
        public async Task Delete(EntityDto input)
        {
            var smsTemplateResult = await _smsTemplatesRepository.FirstOrDefaultAsync(input.Id);
            await _smsTemplatesRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 23; // Sms Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Sms Template  Deleted Name :-" + smsTemplateResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
