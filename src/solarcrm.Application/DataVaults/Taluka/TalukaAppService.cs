﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.Taluka.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.Taluka.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Common;

namespace solarcrm.DataVaults.Taluka
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Taluka)]
    public class TalukaAppService : solarcrmAppServiceBase, ITalukaAppService
    {
        private readonly IRepository<Taluka> _talukaRepository;
        private readonly ITalukaExcelExporter _talukaExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<District.District> _districtRepository;

        public TalukaAppService(IRepository<Taluka> TalukaRepository, ITalukaExcelExporter TalukaExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<District.District> districtRepository)
        {
            _talukaRepository = TalukaRepository;
            _talukaExcelExporter = TalukaExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _districtRepository = districtRepository;
        }

        public async Task<PagedResultDto<GetTalukaForViewDto>> GetAll(GetAllTalukaInput input)
        {
            var filteredTaluka = _talukaRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredTaluka = filteredTaluka.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var talukas = from o in pagedAndFilteredTaluka
                          select new GetTalukaForViewDto()
                          {
                              Talukas = new TalukaDto
                              {
                                  Name = o.Name,
                                  DistrictName = o.DistrictFk.Name,
                                  IsActive = o.IsActive,
                                  Id = o.Id,
                                  CreatedDate = o.CreationTime
                              }
                          };

            var totalCount = await filteredTaluka.CountAsync();

            return new PagedResultDto<GetTalukaForViewDto>(totalCount, await talukas.ToListAsync());
        }

        public async Task CreateOrEdit(CreateOrEditTalukaDto input)
        {
            try
            {
                if (input.Id == null)
                {
                    await Create(input);
                }
                else
                {
                    await Update(input);
                }
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }
       
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Taluka_Edit)]
        public async Task<GetTalukaForEditOutput> GetTalukaForEdit(EntityDto input)
        {
            var talukas = await _talukaRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetTalukaForEditOutput { talukas = ObjectMapper.Map<CreateOrEditTalukaDto>(talukas) };

            return output;
        }

        public async Task<GetTalukaForViewDto> GetTalukaForView(int id)
        {
            var talukas = await _talukaRepository.GetAsync(id);

            var output = new GetTalukaForViewDto { Talukas = ObjectMapper.Map<TalukaDto>(talukas) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Taluka_Create)]
        protected virtual async Task Create(CreateOrEditTalukaDto input)
        {
            try
            {
                var taluka = ObjectMapper.Map<Taluka>(input);

                if (AbpSession.TenantId != null)
                {
                    taluka.TenantId = (int?)AbpSession.TenantId;
                }

                var talukaId = await _talukaRepository.InsertAndGetIdAsync(taluka);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 15; // talukas Pages Name
                dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
                dataVaulteActivityLog.ActionNote = "Taluka Created Name :-" + input.Name;
                dataVaulteActivityLog.SectionValueId = talukaId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Taluka_Edit)]
        protected virtual async Task Update(CreateOrEditTalukaDto input)
        {
            var talukaResult = await _talukaRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 15; // talukas Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Taluka Updated Name :-" + talukaResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var talukasActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (talukaResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = talukaResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = talukasActivityLogId;
                dataVaultsHistory.SectionId = 15; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (talukaResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = talukaResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = talukasActivityLogId;
                dataVaultsHistory.SectionId = 15;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (talukaResult.DistrictId != input.DistrictId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "District";
                dataVaultsHistory.PrevValue = _districtRepository.GetAll().Where(e => e.Id == talukaResult.DistrictId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _districtRepository.GetAll().Where(e => e.Id == input.DistrictId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = talukasActivityLogId;
                dataVaultsHistory.SectionId = 15;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, talukaResult);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Taluka_Delete)]
        public async Task Delete(EntityDto input)
        {
            var talukaResult = await _talukaRepository.FirstOrDefaultAsync(input.Id);
            await _talukaRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 15; // Taluka Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Taluka Deleted Name :-" + talukaResult.Name ;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Taluka_ExportToExcel)]
        public async Task<FileDto> GetTalukaToExcel(GetAllTalukaForExcelInput input)
        {
            var filteredTaluka = _talukaRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredTaluka
                         select new GetTalukaForViewDto()
                         {
                             Talukas = new TalukaDto
                             {
                                 Name = o.Name,
                                 DistrictName = o.DistrictFk.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var talukaListDtos = await query.ToListAsync();

            return _talukaExcelExporter.ExportToFile(talukaListDtos);
        }

        public async Task<List<TalukaLookupTableDto>> GetAllTalukaForDropdown()
        {
            var allTaluka = _talukaRepository.GetAll().Where(x => x.IsActive == true);

            var taluka = from o in allTaluka
                         select new TalukaLookupTableDto()
                         {
                             DisplayName = o.Name,
                             Id = o.Id
                         };

            return new List<TalukaLookupTableDto>(await taluka.ToListAsync());
        }
    }
}
