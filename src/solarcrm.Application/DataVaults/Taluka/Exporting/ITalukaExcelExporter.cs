﻿using solarcrm.DataVaults.Taluka.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Taluka.Exporting
{
    public interface ITalukaExcelExporter
    {
        FileDto ExportToFile(List<GetTalukaForViewDto> district);
    }
}
