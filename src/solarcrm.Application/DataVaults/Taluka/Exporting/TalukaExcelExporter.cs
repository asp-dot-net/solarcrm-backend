﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Taluka.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Taluka.Exporting
{
    public class TalukaExcelExporter : NpoiExcelExporterBase, ITalukaExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public TalukaExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetTalukaForViewDto> leadType)
        {
            return CreateExcelPackage(
                "Taluka.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Taluka"));

                    AddHeader(
                        sheet,
                        L("DistrictName"),
                        L("Name")
                        );
                   
                    AddObjects(
                        sheet, leadType,
                        _ => _.Talukas.DistrictName,
                        _ => _.Talukas.Name
                        );
                });
        }
    }
}
