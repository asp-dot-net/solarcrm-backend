﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.PdfTemplates.Dto;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.Common;
using Abp.Authorization;
using solarcrm.Authorization;
using System;

namespace solarcrm.DataVaults.PdfTemplates
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PdfTemplates)]
    public class PdfTemplateAppService : solarcrmAppServiceBase, IPdfTemplateAppService
    {
        private readonly IRepository<PdfTemplates> _pdfTemplatesRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public PdfTemplateAppService(IRepository<PdfTemplates> PdfTemplatesRepository, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _pdfTemplatesRepository = PdfTemplatesRepository;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_PdfTemplates)]
        public async Task<PagedResultDto<GetPdfTemplateForViewDto>> GetAll(GetAllPdfTemplateInput input)
        {
            var filteredEmailTemplate = _pdfTemplatesRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.PdfTemplateName.Contains(input.Filter));

            var pagedAndFilteredEmailTemplate = filteredEmailTemplate.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var pdfTemplateResult = from o in pagedAndFilteredEmailTemplate
                                select new GetPdfTemplateForViewDto()
                                {
                                    PdfTemplates = new PdfTemplateDto
                                    {
                                        PdfTemplateName = o.PdfTemplateName,                                       
                                        ViewHtml = o.ViewHtml,
                                        IsActive = o.IsActive,
                                        Id = o.Id,
                                        CreatedDate = o.CreationTime,
                                    }
                                };

            var totalCount = await filteredEmailTemplate.CountAsync();

            return new PagedResultDto<GetPdfTemplateForViewDto>(totalCount, await pdfTemplateResult.ToListAsync());
        }
      
        //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit)]
        public async Task<GetPdfTemplateForEditOutput> GetPdfTemplateForEdit(EntityDto input)
        {
            var pdfTemplateResult = await _pdfTemplatesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetPdfTemplateForEditOutput { PdfTemplates = ObjectMapper.Map<CreateOrEditPdfTemplateDto>(pdfTemplateResult) };

            return output;
        }
        public async Task<GetPdfTemplateForEditOutput> GetPdfTemplateForView(int id,int organizationId)
        {
            var pdfTemplateResult = await _pdfTemplatesRepository.GetAll().Where(x => x.ApiHtml == Convert.ToString(id) && x.OrganizationUnitId == organizationId).FirstOrDefaultAsync();//FirstOrDefaultAsync(input.Id);

            var output = new GetPdfTemplateForEditOutput { PdfTemplates = ObjectMapper.Map<CreateOrEditPdfTemplateDto>(pdfTemplateResult) };

            return output;
        }
        public async Task CreateOrEdit(CreateOrEditPdfTemplateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Create)]
        protected virtual async Task Create(CreateOrEditPdfTemplateDto input)
        {
            try
            {
                var pdfTemplateResult = ObjectMapper.Map<PdfTemplates>(input);

                if (AbpSession.TenantId != null)
                {
                    pdfTemplateResult.TenantId = (int?)AbpSession.TenantId;
                }

                var pdfTemplateId = await _pdfTemplatesRepository.InsertAndGetIdAsync(pdfTemplateResult);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 46; // pdfTemplate Pages Name
                dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
                dataVaulteActivityLog.ActionNote = "Pdf Template Created Name :- " + input.PdfTemplateName;
                dataVaulteActivityLog.SectionValueId = pdfTemplateId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

       // [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit)]
        protected virtual async Task Update(CreateOrEditPdfTemplateDto input)
        {
            var pdfTemplateResult = await _pdfTemplatesRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 46; // pdfTemplate Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Pdf Template Updated Name :- " + pdfTemplateResult.PdfTemplateName;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var pdfTemplateActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (pdfTemplateResult.PdfTemplateName != input.PdfTemplateName)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = pdfTemplateResult.PdfTemplateName;
                dataVaultsHistory.CurValue = input.PdfTemplateName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Updated
                dataVaultsHistory.ActivityLogId = pdfTemplateActivityLogId;
                dataVaultsHistory.SectionId = 46; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (pdfTemplateResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = pdfTemplateResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Updated Pages name
                dataVaultsHistory.ActivityLogId = pdfTemplateActivityLogId;
                dataVaultsHistory.SectionId = 46;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, pdfTemplateResult);
        }

        // [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Delete)]
        public async Task Delete(EntityDto input)
        {
            var pdfTemplateResult = await _pdfTemplatesRepository.GetAsync(input.Id);
            await _pdfTemplatesRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 46; // Pdf Template Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Deleted
            dataVaulteActivityLog.ActionNote = "pdf Template Deleted Name :- " + pdfTemplateResult.PdfTemplateName;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
