﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Circle.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Circle.Exporting
{
    public class CircleExcelExporter : NpoiExcelExporterBase, ICircleExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CircleExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetCircleForViewDto> circle)
        {
            return CreateExcelPackage(
                "Circle.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Circle"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DiscomName"),
                        L("DivisionName"),
                        L("SubDivisionName")
                        );

                    AddObjects(
                        sheet, circle,
                        _ => _.circle.Name,
                        _ => _.circle.DiscomName,
                        _ => _.circle.DivisionName,
                        _ => _.circle.SubDivisionName
                        );
                });
        }
    }
}
