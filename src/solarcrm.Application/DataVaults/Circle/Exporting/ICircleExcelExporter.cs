﻿using solarcrm.DataVaults.Circle.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Circle.Exporting
{
    public interface ICircleExcelExporter
    {
        FileDto ExportToFile(List<GetCircleForViewDto> circle);
    }
}
