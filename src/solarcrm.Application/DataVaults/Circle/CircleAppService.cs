﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using solarcrm.Dto;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;

using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Circle.Exporting;
using solarcrm.DataVaults.Circle.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.Circle
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Circle)]
    public class CircleAppService : solarcrmAppServiceBase, ICircleAppService
    {
        private readonly IRepository<Circle> _circleRepository;
        private readonly ICircleExcelExporter _circleExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Discom.Discom> _discomRepository;
        private readonly IRepository<Division.Division> _divisionRepository;
        private readonly IRepository<SubDivision.SubDivision> _subDivisionRepository;

        public CircleAppService(IRepository<Circle> CircleRepository
            , ICircleExcelExporter CircleExcelExporter
            , IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository
            , IDbContextProvider<solarcrmDbContext> dbContextProvider
            , IRepository<SubDivision.SubDivision> subDivisionRepository
            , IRepository<Division.Division> divisionRepository
            , IRepository<Discom.Discom> discomRepository)
        {
            _circleRepository = CircleRepository;
            _circleExcelExporter = CircleExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _subDivisionRepository = subDivisionRepository;
            _divisionRepository = divisionRepository;
            _discomRepository = discomRepository;
        }

        public async Task CreateOrEdit(CreateOrEditCircleDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
       
        
        public async Task<PagedResultDto<GetCircleForViewDto>> GetAll(GetAllCircleInput input)
        {
            var filteredCircle = _circleRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredCircle = filteredCircle.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var circle = from o in pagedAndFilteredCircle
                                select new GetCircleForViewDto()
                                {
                                    circle = new CircleDto
                                    {
                                        Name = o.Name,
                                        DiscomName = o.DiscomIdFk.Name,
                                        DivisionName = o.DivisionIdFk.Name,
                                        SubDivisionName = o.SubDivisionIdFk.Name,
                                        IsActive = o.IsActive,
                                        Id = o.Id,
                                        CreatedDate = o.CreationTime,

                                    }
                                };

            var totalCount = await filteredCircle.CountAsync();

            return new PagedResultDto<GetCircleForViewDto>(totalCount, await circle.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Circle_Edit)]
        public async Task<GetCircleForEditOutput> GetCircleForEdit(EntityDto input)
        {
            var circle = await _circleRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCircleForEditOutput { Circle = ObjectMapper.Map<CreateOrEditCircleDto>(circle) };

            return output;
        }

        public async Task<GetCircleForViewDto> GetCircleForView(int id)
        {
            var circle = await _circleRepository.GetAsync(id);

            var output = new GetCircleForViewDto { circle = ObjectMapper.Map<CircleDto>(circle) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Circle_Create)]
        public async Task<FileDto> GetCircleToExcel(GetAllCircleForExcelInput input)
        {
            var filteredCircle = _circleRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCircle
                         select new GetCircleForViewDto()
                         {
                             circle = new CircleDto
                             {
                                 Name = o.Name,
                                 DiscomName = o.DiscomIdFk.Name,
                                 DivisionName = o.DivisionIdFk.Name,
                                 SubDivisionName = o.SubDivisionIdFk.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var circleListDtos = await query.ToListAsync();

            return _circleExcelExporter.ExportToFile(circleListDtos);
        }

       [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Circle_Create)]
        protected virtual async Task Create(CreateOrEditCircleDto input)
        {
            try
            {
                var circle = ObjectMapper.Map<Circle>(input);

                if (AbpSession.TenantId != null)
                {
                    circle.TenantId = (int?)AbpSession.TenantId;
                }

                var circleId = await _circleRepository.InsertAndGetIdAsync(circle);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 33; // CancelReasons Pages Name
                dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
                dataVaulteActivityLog.ActionNote = "Circle Created Name :-" + input.Name;
                dataVaulteActivityLog.SectionValueId = circleId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Circle_Edit)]
        protected virtual async Task Update(CreateOrEditCircleDto input)
        {
            var circleResult = await _circleRepository.FirstOrDefaultAsync((int)input.Id);


            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 33; // CancelReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Circle Updated Name :-" + circleResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var cancelReasonsActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (circleResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = circleResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 17; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (circleResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = circleResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 17;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (circleResult.DivisionId != input.DivisionId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Division";
                dataVaultsHistory.PrevValue = _divisionRepository.GetAll().Where(e => e.Id == circleResult.DivisionId).Select(e=>e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _divisionRepository.GetAll().Where(e => e.Id == input.DivisionId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 17;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (circleResult.DiscomId != input.DiscomId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Discom";
                dataVaultsHistory.PrevValue = _discomRepository.GetAll().Where(e => e.Id == circleResult.DiscomId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _discomRepository.GetAll().Where(e => e.Id == input.DiscomId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 17;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (circleResult.SubDivisionId != input.SubDivisionId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "SubDivision";
                dataVaultsHistory.PrevValue = _subDivisionRepository.GetAll().Where(e => e.Id == circleResult.SubDivisionId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _subDivisionRepository.GetAll().Where(e => e.Id == input.SubDivisionId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 17;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, circleResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Circle_Delete)]
        public async Task Delete(EntityDto input)
        {

            var circleResult = await _circleRepository.FirstOrDefaultAsync(input.Id);
            await _circleRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 33; // Cancel Reason Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Created
            dataVaulteActivityLog.ActionNote = "Circle Deleted Name :- " + circleResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
