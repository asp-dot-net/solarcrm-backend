﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.LeadStatus.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.LeadStatus.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;

namespace solarcrm.DataVaults.LeadStatus
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadStatus)]
    public class LeadStatusAppService : solarcrmAppServiceBase, ILeadStatusAppService
    {
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly ILeadStatusExcelExporter _leadStatusExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public LeadStatusAppService(IRepository<LeadStatus> LeadStatusRepository, ILeadStatusExcelExporter LeadStatusExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _leadStatusRepository = LeadStatusRepository;
            _leadStatusExcelExporter = LeadStatusExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
        public async Task CreateOrEdit(CreateOrEditLeadStatusDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _leadStatusRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 30; // Leads status Pages Name
            dataVaulteActivityLog.ActionId = 3; // Deleted
            dataVaulteActivityLog.ActionNote = "Leads Status Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }


        public async Task<PagedResultDto<GetLeadStatusForViewDto>> GetAll(GetAllLeadStatusInput input)
        {
            var filteredState = _leadStatusRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Status.Contains(input.Filter));

            var pagedAndFilteredState = filteredState.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var States = from o in pagedAndFilteredState
                         select new GetLeadStatusForViewDto()
                         {
                             leadStatus = new LeadStatusDto
                             {
                                 Name = o.Status,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime,

                             }
                         };

            var totalCount = await filteredState.CountAsync();

            return new PagedResultDto<GetLeadStatusForViewDto>(totalCount, await States.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Edit)]
        public async Task<GetLeadStatusForEditOutput> GetLeadStatusForEdit(EntityDto input)
        {
            var states = await _leadStatusRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadStatusForEditOutput { leadstatus = ObjectMapper.Map<CreateOrEditLeadStatusDto>(states) };

            return output;
        }

        public async Task<GetLeadStatusForViewDto> GetLeadStatusForView(int id)
        {
            var states = await _leadStatusRepository.GetAsync(id);

            var output = new GetLeadStatusForViewDto { leadStatus = ObjectMapper.Map<LeadStatusDto>(states) };

            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Create)]
        public async Task<FileDto> GetLeadStatusToExcel(GetAllLeadStatusForExcelInput input)
        {
            var filteredState = _leadStatusRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Status.Contains(input.Filter));

            var query = (from o in filteredState
                         select new GetLeadStatusForViewDto()
                         {
                             leadStatus = new LeadStatusDto
                             {
                                 Name = o.Status,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var stateListDtos = await query.ToListAsync();

            return _leadStatusExcelExporter.ExportToFile(stateListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Create)]
        protected virtual async Task Create(CreateOrEditLeadStatusDto input)
        {
            var state = ObjectMapper.Map<LeadStatus>(input);

            if (AbpSession.TenantId != null)
            {
                state.TenantId = (int?)AbpSession.TenantId;
            }

            var stateId = await _leadStatusRepository.InsertAndGetIdAsync(state);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 30; // State Pages Name
            dataVaulteActivityLog.ActionId = 1; // Created
            dataVaulteActivityLog.ActionNote = "Lead Status Created";
            dataVaulteActivityLog.SectionValueId = stateId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Edit)]
        protected virtual async Task Update(CreateOrEditLeadStatusDto input)
        {
            var states = await _leadStatusRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 30; // state Pages Name
            dataVaulteActivityLog.ActionId = 2; // Updated
            dataVaulteActivityLog.ActionNote = "Lead Status Updated";
            dataVaulteActivityLog.SectionValueId = input.Id;
            var stateActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (states.Status != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = states.Status;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = stateActivityLogId;
                dataVaultsHistory.SectionId = 30; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (states.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = states.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2; // Pages name
                dataVaultsHistory.ActivityLogId = stateActivityLogId;
                dataVaultsHistory.SectionId = 30;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, states);
        }
    }
}
