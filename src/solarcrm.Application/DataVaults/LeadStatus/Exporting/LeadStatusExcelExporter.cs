﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.LeadStatus.Dto;
using solarcrm.DataVaults.State.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;

namespace solarcrm.DataVaults.LeadStatus.Exporting
{
    public class LeadStatusExcelExporter : NpoiExcelExporterBase, ILeadStatusExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LeadStatusExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetLeadStatusForViewDto> leadType)
        {
            return CreateExcelPackage(
                "LeadStatus.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LeadStatus"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, leadType,
                        _ => _.leadStatus.Name
                        );
                });
        }
    }
}
