﻿using solarcrm.DataVaults.LeadStatus.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadStatus.Exporting
{
    public interface ILeadStatusExcelExporter
    {
        FileDto ExportToFile(List<GetLeadStatusForViewDto> leadStatus);
    }
}
