﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.HeightStructure.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.HeightStructure
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HeightStructure)]
    public class HeightStructureAppService : solarcrmAppServiceBase, IHeightStructureAppService
    {
        private readonly IRepository<HeightStructure> _heightStructureRepository;
        private readonly IHeightStructureExcelExporter _heightStructureExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public HeightStructureAppService(IRepository<HeightStructure> HeightStructureRepository, IHeightStructureExcelExporter HeightStructureExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _heightStructureRepository = HeightStructureRepository;
            _heightStructureExcelExporter = HeightStructureExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }
       
        public async Task<PagedResultDto<GetHeightStructureForViewDto>> GetAll(GetAllHeightStructureInput input)
        {
            var filteredHeightStructure = _heightStructureRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredHeightStructure = filteredHeightStructure.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var heightStructure = from o in pagedAndFilteredHeightStructure
                                  select new GetHeightStructureForViewDto()
                            {
                                HeightStructures = new HeightStructureDto
                                {
                                    Name = o.Name,
                                    IsActive = o.IsActive,
                                    Id = o.Id,
                                    CreatedDate = o.CreationTime
                                }
                            };

            var totalCount = await filteredHeightStructure.CountAsync();

            return new PagedResultDto<GetHeightStructureForViewDto>(totalCount, await heightStructure.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_Edit)]
        public async Task<GetHeightStructureForEditOutput> GetHeightStructureForEdit(EntityDto input)
        {
            var heightStructure = await _heightStructureRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetHeightStructureForEditOutput { HeightStructures = ObjectMapper.Map<CreateOrEditHeightStructureDto>(heightStructure) };

            return output;
        }
        public async Task<GetHeightStructureForViewDto> GetHeightStructureForView(int id)
        {
            var heightStructure = await _heightStructureRepository.GetAsync(id);

            var output = new GetHeightStructureForViewDto { HeightStructures = ObjectMapper.Map<HeightStructureDto>(heightStructure) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_ExportToExcel)]
        public async Task<FileDto> GetHeightStructureToExcel(GetAllHeightStructureForExcelInput input)
        {
            var filteredHeightStructure = _heightStructureRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredHeightStructure
                         select new GetHeightStructureForViewDto()
                         {
                             HeightStructures = new HeightStructureDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var solarListDtos = await query.ToListAsync();

            return _heightStructureExcelExporter.ExportToFile(solarListDtos);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_Create)]

        public async Task CreateOrEdit(CreateOrEditHeightStructureDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Create(CreateOrEditHeightStructureDto input)
        {
            var heightStructure = ObjectMapper.Map<HeightStructure>(input);

            if (AbpSession.TenantId != null)
            {
                heightStructure.TenantId = (int?)AbpSession.TenantId;
            }

            var heightStructureID = await _heightStructureRepository.InsertAndGetIdAsync(heightStructure);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 27; // Height Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Height Structure Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = heightStructureID;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_Edit)]
        protected virtual async Task Update(CreateOrEditHeightStructureDto input)
        {
            var solerTypes = await _heightStructureRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 27; // City Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Height Structure Updated Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var solerTypesActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (solerTypes.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = solerTypes.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = solerTypesActivityLogId;
                dataVaultsHistory.SectionId = 16; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (solerTypes.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = solerTypes.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = solerTypesActivityLogId;
                dataVaultsHistory.SectionId = 27;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, solerTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_Delete)]
        public async Task Delete(EntityDto input)
        {
            var heightResult = await _heightStructureRepository.FirstOrDefaultAsync(input.Id);
            await _heightStructureRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 27; // HeightStructure Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Height Structure Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
        public async Task<List<HeightStructureLookupTable>> GetAllHeightStructureForDropdown()
        {
            var allHeightStructure = _heightStructureRepository.GetAll().Where(x => x.IsActive == true);

            var heightStructure = from o in allHeightStructure
                                  select new HeightStructureLookupTable()
                            {
                                Name = o.Name,
                                Id = o.Id
                            };

            return new List<HeightStructureLookupTable>(await heightStructure.ToListAsync());
        }
    }
}
