﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.HeightStructure.Exporting
{
    public class HeightStructureExcelExporter : NpoiExcelExporterBase, IHeightStructureExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public HeightStructureExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetHeightStructureForViewDto> heightstructure)
        {
            return CreateExcelPackage(
                "HeightStructure.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("HeightStructure"));
                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    AddObjects(
                        sheet, heightstructure,
                        _ => _.HeightStructures.Name
                        );
                });
        }
    }
}
