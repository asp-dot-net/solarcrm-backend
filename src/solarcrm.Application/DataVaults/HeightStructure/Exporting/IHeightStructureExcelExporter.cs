﻿using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.HeightStructure.Exporting
{
    public interface IHeightStructureExcelExporter
    {
        FileDto ExportToFile(List<GetHeightStructureForViewDto> HeightStructures);
    }
}
