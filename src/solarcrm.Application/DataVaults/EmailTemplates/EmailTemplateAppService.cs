﻿
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.EmailTemplates.Dto;
using solarcrm.Common;
using Abp.Organizations;

namespace solarcrm.DataVaults.EmailTemplates
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates)]
    public class EmailTemplateAppService : solarcrmAppServiceBase, IEmailTemplateAppService
    {
        private readonly IRepository<EmailTemplates> _emailTemplatesRepository;
      //  private readonly IEmailTemplateExcelExporter _EmailTemplateExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;

        public EmailTemplateAppService(IRepository<EmailTemplates> EmailTemplatesRepository,  IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<OrganizationUnit,long> organizationUnitRepository)
        {
            _emailTemplatesRepository = EmailTemplatesRepository;
            //_EmailTemplateExcelExporter = EmailTemplateExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _organizationUnitRepository = organizationUnitRepository;
        }

        public async Task<PagedResultDto<GetEmailTemplateForViewDto>> GetAll(GetAllEmailTemplateInput input)
        {
            var filteredEmailTemplate = _emailTemplatesRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Subject.Contains(input.Filter));

            var pagedAndFilteredEmailTemplate = filteredEmailTemplate.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var EmailTemplate = from o in pagedAndFilteredEmailTemplate
                                select new GetEmailTemplateForViewDto()
                                {
                                    EmailTemplates = new EmailTemplateDto
                                    {
                                        TemplateName = o.TemplateName,
                                        Subject = o.Subject,
                                        Body = o.Body,
                                        IsActive = o.IsActive,
                                        Id = o.Id,
                                        CreatedDate = o.CreationTime,

                                    }
                                };

            var totalCount = await filteredEmailTemplate.CountAsync();

            return new PagedResultDto<GetEmailTemplateForViewDto>(totalCount, await EmailTemplate.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit)]
        public async Task<GetEmailTemplateForEditOutput> GetEmailTemplateForEdit(EntityDto input)
        {
            var EmailTemplate = await _emailTemplatesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetEmailTemplateForEditOutput { EmailTemplates = ObjectMapper.Map<CreateOrEditEmailTemplateDto>(EmailTemplate) };

            return output;
        }
        public async Task<GetEmailTemplateForViewDto> GetEmailTemplateForView(int id)
        {
            var EmailTemplate = await _emailTemplatesRepository.GetAsync(id);

            var output = new GetEmailTemplateForViewDto { EmailTemplates = ObjectMapper.Map<EmailTemplateDto>(EmailTemplate) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEmailTemplateDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Create)]
        protected virtual async Task Create(CreateOrEditEmailTemplateDto input)
        {
            try
            {
                var emailTemplate = ObjectMapper.Map<EmailTemplates>(input);

                if (AbpSession.TenantId != null)
                {
                    emailTemplate.TenantId = (int?)AbpSession.TenantId;
                }

                var emailTemplateId = await _emailTemplatesRepository.InsertAndGetIdAsync(emailTemplate);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 24; // EmailTemplate Pages Name
                dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
                dataVaulteActivityLog.ActionNote = "Email Template Created Name :-" +input.TemplateName;
                dataVaulteActivityLog.SectionValueId = emailTemplateId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit)]
        protected virtual async Task Update(CreateOrEditEmailTemplateDto input)
        {
            var emailTemplateResult = await _emailTemplatesRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 24; // EmailTemplate Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Email Template Updated Name :-" + input.TemplateName ;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var emailTemplateActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (emailTemplateResult.TemplateName != input.TemplateName)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = emailTemplateResult.TemplateName;
                dataVaultsHistory.CurValue = input.TemplateName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = emailTemplateActivityLogId;
                dataVaultsHistory.SectionId = 24; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (emailTemplateResult.Subject != input.Subject)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Subject";
                dataVaultsHistory.PrevValue = emailTemplateResult.Subject;
                dataVaultsHistory.CurValue = input.Subject;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = emailTemplateActivityLogId;
                dataVaultsHistory.SectionId = 24; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (emailTemplateResult.TemplateName != input.TemplateName)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "TemplateName";
                dataVaultsHistory.PrevValue = emailTemplateResult.TemplateName;
                dataVaultsHistory.CurValue = input.TemplateName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = emailTemplateActivityLogId;
                dataVaultsHistory.SectionId = 24; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (emailTemplateResult.OrganizationUnitId != input.OrganizationUnitId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "OrganizationUnit";
                dataVaultsHistory.PrevValue = _organizationUnitRepository.GetAll().Where(e=>e.Id==emailTemplateResult.OrganizationUnitId).FirstOrDefault().DisplayName;
                dataVaultsHistory.CurValue = _organizationUnitRepository.GetAll().Where(e => e.Id == input.OrganizationUnitId).FirstOrDefault().DisplayName;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = emailTemplateActivityLogId;
                dataVaultsHistory.SectionId = 24; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (emailTemplateResult.Body != input.Body)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Body";
                dataVaultsHistory.PrevValue = emailTemplateResult.Body;
                dataVaultsHistory.CurValue = input.Body;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = emailTemplateActivityLogId;
                dataVaultsHistory.SectionId = 24; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (emailTemplateResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = emailTemplateResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = emailTemplateActivityLogId;
                dataVaultsHistory.SectionId = 24;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, emailTemplateResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Delete)]
        public async Task Delete(EntityDto input)
        {
            var emailTemplateResult = await _emailTemplatesRepository.FirstOrDefaultAsync(input.Id);
            await _emailTemplatesRepository.DeleteAsync(input.Id);
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 24; // Email Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Email Template Deleted :-" + emailTemplateResult.TemplateName;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
