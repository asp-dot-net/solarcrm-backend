﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.RefundReasons.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.RefundReasons.Exporting
{
    public class RefundReasonsExcelExporter : NpoiExcelExporterBase, IRefundReasonsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public RefundReasonsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetRefundReasonsForViewDto> refundReasons)
        {
            return CreateExcelPackage(
                "RefundReasons.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(("RefundReason"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, ProjectType,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, refundReasons,
                        _ => _.RefundReason.Name
                        );
                });
        }
    }
}
