﻿using solarcrm.DataVaults.RefundReasons.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.RefundReasons.Exporting
{
    public interface IRefundReasonsExcelExporter
    {
        FileDto ExportToFile(List<GetRefundReasonsForViewDto> refundReason);
    }
}
