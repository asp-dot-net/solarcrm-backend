﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.RefundReasons.Dto;
using solarcrm.DataVaults.RefundReasons.Exporting;
using solarcrm.Dto;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Common;

namespace solarcrm.DataVaults.RefundReasons
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RefundReasons)]
    public class RefundReasonsAppService : solarcrmAppServiceBase, IRefundReasonsAppService
    {
        private readonly IRepository<RefundReasons> _RefundReasonRepository;
        private readonly IRefundReasonsExcelExporter _RefundReasonExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public RefundReasonsAppService(IRepository<RefundReasons> RefundReasonRepository, IRefundReasonsExcelExporter RefundReasonExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _RefundReasonRepository = RefundReasonRepository;
            _RefundReasonExcelExporter = RefundReasonExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetRefundReasonsForViewDto>> GetAll(GetAllRefundReasonsInput input)
        {
            var filteredprojectType = _RefundReasonRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredProjectType = filteredprojectType.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var projectType = from o in pagedAndFilteredProjectType
                              select new GetRefundReasonsForViewDto()
                              {
                                  RefundReason = new RefundReasonsDto
                                  {
                                      Name = o.Name,
                                      IsActive = o.IsActive,
                                      Id = o.Id,
                                      CreatedDate = o.CreationTime
                                  }
                              };

            var totalCount = await filteredprojectType.CountAsync();

            return new PagedResultDto<GetRefundReasonsForViewDto>(totalCount, await projectType.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_Edit)]
        public async Task<GetRefundReasonsForEditOutput> GetRefundReasonsForEdit(EntityDto input)
        {
            var RefundReason = await _RefundReasonRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetRefundReasonsForEditOutput { RefundReason = ObjectMapper.Map<CreateOrEditRefundReasonsDto>(RefundReason) };

            return output;
        }

        public async Task<GetRefundReasonsForViewDto> GetRefundReasonsForView(int id)
        {
            var RefundReasons = await _RefundReasonRepository.GetAsync(id);

            var output = new GetRefundReasonsForViewDto { RefundReason = ObjectMapper.Map<RefundReasonsDto>(RefundReasons) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_ExportToExcel)]
        public async Task<FileDto> GetRefundReasonsToExcel(GetAllRefundReasonsForExcelInput input)
        {
            var filteredprojectType = _RefundReasonRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredprojectType
                         select new GetRefundReasonsForViewDto()
                         {
                             RefundReason = new RefundReasonsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var ProjectTypeListDtos = await query.ToListAsync();

            return _RefundReasonExcelExporter.ExportToFile(ProjectTypeListDtos);
        }

        public async Task CreateOrEdit(CreateOrEditRefundReasonsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_Create)]
        protected virtual async Task Create(CreateOrEditRefundReasonsDto input)
        {
            var refundReason = ObjectMapper.Map<RefundReasons>(input);

            if (AbpSession.TenantId != null)
            {
                refundReason.TenantId = (int?)AbpSession.TenantId;
            }

            var refundReasonId = await _RefundReasonRepository.InsertAndGetIdAsync(refundReason);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 55; // ProjectType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Refund Reason Created Name :-" +input.Name;
            dataVaulteActivityLog.SectionValueId = refundReasonId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_Edit)]
        protected virtual async Task Update(CreateOrEditRefundReasonsDto input)
        {
            var ProjectType = await _RefundReasonRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 55; // ProjectType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Refund Reason Updated Name :-" +ProjectType.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var projecttypeActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (ProjectType.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = ProjectType.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = projecttypeActivityLogId;
                dataVaultsHistory.SectionId = 55;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (ProjectType.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = ProjectType.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = projecttypeActivityLogId;
                dataVaultsHistory.SectionId = 55;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, ProjectType);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_Delete)]
        public async Task Delete(EntityDto input)
        {
            var refundReasonResult = await _RefundReasonRepository.FirstOrDefaultAsync(input.Id);
            await _RefundReasonRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 55; // Projecttype Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Refund Reason Deleted Name :-" + refundReasonResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
