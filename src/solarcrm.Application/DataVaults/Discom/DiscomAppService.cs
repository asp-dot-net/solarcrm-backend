﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.Discom.Dto;
using solarcrm.DataVaults.Discom.Exporting;
using solarcrm.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using solarcrm.Common;
using solarcrm.DataVaults.Locations;

namespace solarcrm.DataVaults.Discom
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Discom)]
    public class DiscomAppService : solarcrmAppServiceBase, IDiscomAppService
    {
        private readonly IRepository<Discom> _discomRepository;
        private readonly IDiscomExcelExporter _discomExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<solarcrm.DataVaults.Locations.Locations> _locationRepository; 
        public DiscomAppService(IRepository<Discom> discomRepository, IDiscomExcelExporter discomExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<solarcrm.DataVaults.Locations.Locations> locationRepository)
        {
            _discomRepository = discomRepository;
            _discomExcelExporter = discomExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _locationRepository = locationRepository;
        }

        public async Task CreateOrEdit(CreateOrEditDiscomDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
                
        public async Task<PagedResultDto<GetDiscomForViewDto>> GetAll(GetAllDiscomInput input)
        {
            var filteredDiscom = _discomRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredDiscom = filteredDiscom.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var discom = from o in pagedAndFilteredDiscom
                         select new GetDiscomForViewDto()
                         {
                             Discom = new DiscomDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime,
                                 ABB = o.ABB,
                                 Location = o.LocationsFk.Name,
                                 PaymentLink = o.PaymentLink
                             }
                         };

            var totalCount = await filteredDiscom.CountAsync();

            return new PagedResultDto<GetDiscomForViewDto>(totalCount, await discom.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Discom_Edit)]
        public async Task<GetDiscomForEditOutput> GetDiscomForEdit(EntityDto input)
        {
            var discom = await _discomRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetDiscomForEditOutput { Discom = ObjectMapper.Map<CreateOrEditDiscomDto>(discom) };

            return output;
        }

        //public async Task<GetDiscomForViewDto> GetDiscomForView(int id)
        //{
        //    var discom = await _discomRepository.GetAsync(id);

        //    var output = new GetDiscomForViewDto { Discom = ObjectMapper.Map<DiscomDto>(discom) };

        //    return output;
        //}

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Discom_ExportToExcel)]
        public async Task<FileDto> GetDiscomToExcel(GetAllDiscomForExcelInput input)
        {
            var filteredDiscom = _discomRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredDiscom
                         select new GetDiscomForViewDto()
                         {
                             Discom = new DiscomDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 ABB = o.ABB,
                                 Location = o.LocationsFk.Name,
                                 PaymentLink = o.PaymentLink
                             }
                         });

            var DiscomsListDtos = await query.ToListAsync();

            return _discomExcelExporter.ExportToFile(DiscomsListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Discom_Create)]
        protected virtual async Task Create(CreateOrEditDiscomDto input)
        {
            var Discom = ObjectMapper.Map<Discom>(input);

            if (AbpSession.TenantId != null)
            {
                Discom.TenantId = (int?)AbpSession.TenantId;
            }

            var discomId = await _discomRepository.InsertAndGetIdAsync(Discom);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 10; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Discom Created Name :- " + input.Name;
            dataVaulteActivityLog.SectionValueId = discomId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Discom_Edit)]
        protected virtual async Task Update(CreateOrEditDiscomDto input)
        {
            var discomResult = await _discomRepository.FirstOrDefaultAsync((int)input.Id);
            var location = _locationRepository.GetAll();

            //Add Activity Log
            int SectionId = 10; // Discom Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Discom Updated Name :-" +discomResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var DocumentListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (discomResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = discomResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (discomResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = discomResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (discomResult.ABB != input.ABB)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "ABB";
                dataVaultsHistory.PrevValue = discomResult.ABB.ToString();
                dataVaultsHistory.CurValue = input.ABB.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }          

            if (discomResult.LocationId != input.LocationId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Location";
                dataVaultsHistory.PrevValue = location.Where(e => e.Id == discomResult.LocationId).Select(e=>e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = location.Where(e => e.Id == input.LocationId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            if (discomResult.PaymentLink != input.PaymentLink)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "PaymentLink";
                dataVaultsHistory.PrevValue = discomResult.PaymentLink.ToString();
                dataVaultsHistory.CurValue = input.PaymentLink.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = 2;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, discomResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_Discom_Delete)]
        public async Task Delete(EntityDto input)
        {
            var discomResult = await _discomRepository.FirstOrDefaultAsync(input.Id);
            await _discomRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 10; // Discom Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Created
            dataVaulteActivityLog.ActionNote = "Discom Deleted Name :- " + discomResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        public async Task<List<GetAllDiscomForDropdown>> GetAllDiscomForDropdown()
        {
            var allDiscom = _discomRepository.GetAll().Where(x => x.IsActive == true);

            var discom = from o in allDiscom
                         select new GetAllDiscomForDropdown()
                            {
                                Name = o.Name,
                                Id = o.Id
                            };

            return new List<GetAllDiscomForDropdown>(await discom.ToListAsync());
        }
    }
}
