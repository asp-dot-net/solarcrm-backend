﻿using solarcrm.DataVaults.Discom.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.Discom.Exporting
{
    public interface IDiscomExcelExporter
    {
        FileDto ExportToFile(List<GetDiscomForViewDto> discom);
    }
}
