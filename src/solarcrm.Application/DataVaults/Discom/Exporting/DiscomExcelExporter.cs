﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.Discom.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Discom.Exporting
{
    public class DiscomExcelExporter : NpoiExcelExporterBase, IDiscomExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public DiscomExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetDiscomForViewDto> discom)
        {
            return CreateExcelPackage(
                "Discom.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("Discom"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("DiscomABB"),                        
                        L("Locations"),
                        L("PaymentLink")
                        );

                    AddObjects(
                        sheet, discom,
                        _ => _.Discom.Name,
                        _ => _.Discom.ABB,                       
                        _ => _.Discom.Location,
                         _ => _.Discom.PaymentLink
                        );
                });
        }
    }
}
