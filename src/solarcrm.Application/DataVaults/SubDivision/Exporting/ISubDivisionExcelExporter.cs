﻿using solarcrm.DataVaults.SubDivision.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.SubDivision.Exporting
{
    public interface ISubDivisionExcelExporter
    {
        FileDto ExportToFile(List<GetSubDivisionForViewDto> subDivision);
    }
}
