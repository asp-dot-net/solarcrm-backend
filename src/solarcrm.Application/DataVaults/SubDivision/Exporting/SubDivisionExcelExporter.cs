﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.SubDivision.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System.Collections.Generic;

namespace solarcrm.DataVaults.SubDivision.Exporting
{
    public class SubDivisionExcelExporter : NpoiExcelExporterBase, ISubDivisionExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public SubDivisionExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetSubDivisionForViewDto> subDivision)
        {
            return CreateExcelPackage(
                "SubDivision.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("SubDivision"));

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Division")
                        );

                    AddObjects(
                        sheet, subDivision,
                        _ => _.SubDivision.Name,
                        _ => _.SubDivision.Division
                        );
                });
        }
    }
}
