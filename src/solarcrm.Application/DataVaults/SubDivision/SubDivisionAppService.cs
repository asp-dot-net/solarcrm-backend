﻿using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Dto;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using Abp.Authorization;
using solarcrm.Authorization;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.SubDivision.Dto;
using System.Linq.Dynamic.Core;
using solarcrm.DataVaults.SubDivision.Exporting;
using solarcrm.DataVaults.SubDivision;
using solarcrm.Common;

namespace solarcrm.DataVaults.SubSubDivision
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SubDivision)]
    public class SubSubDivisionAppService : solarcrmAppServiceBase, ISubDivisionAppService
    {
        private readonly IRepository<SubDivision.SubDivision> _subDivisionRepository;
        private readonly ISubDivisionExcelExporter _subDivisionExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Division.Division> _divisionRepository;

        public SubSubDivisionAppService(IRepository<SubDivision.SubDivision> subDivisionRepository, ISubDivisionExcelExporter subDivisionExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider, IRepository<Division.Division> divisionRepository)
        {
            _subDivisionRepository = subDivisionRepository;
            _subDivisionExcelExporter = subDivisionExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
            _divisionRepository = divisionRepository;
        }

        public async Task CreateOrEdit(CreateOrEditSubDivisionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

       
        public async Task<PagedResultDto<GetSubDivisionForViewDto>> GetAll(GetAllSubDivisionInput input)
        {
            var filteredSubDivision = _subDivisionRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredSubDivision = filteredSubDivision.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var subDivision = from o in pagedAndFilteredSubDivision
                         select new GetSubDivisionForViewDto()
                         {
                             SubDivision = new SubDivisionDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime,
                                 Division = o.DivisionFk.Name
                             }
                         };

            var totalCount = await filteredSubDivision.CountAsync();

            return new PagedResultDto<GetSubDivisionForViewDto>(totalCount, await subDivision.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SubDivision_Edit)]
        public async Task<GetSubDivisionForEditOutput> GetSubDivisionForEdit(EntityDto input)
        {
            var subDivision = await _subDivisionRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetSubDivisionForEditOutput { SubDivision = ObjectMapper.Map<CreateOrEditSubDivisionDto>(subDivision) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SubDivision_ExportToExcel)]
        public async Task<FileDto> GetSubDivisionToExcel(GetAllSubDivisionForExcelInput input)
        {
            var filteredSubDivision = _subDivisionRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredSubDivision
                         select new GetSubDivisionForViewDto()
                         {
                             SubDivision = new SubDivisionDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 Division = o.DivisionFk.Name
                             }
                         });

            var DiscomsListDtos = await query.ToListAsync();

            return _subDivisionExcelExporter.ExportToFile(DiscomsListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SubDivision_Create)]
        protected virtual async Task Create(CreateOrEditSubDivisionDto input)
        {
            var subDivision = ObjectMapper.Map<SubDivision.SubDivision>(input);

            if (AbpSession.TenantId != null)
            {
                subDivision.TenantId = (int?)AbpSession.TenantId;
            }
            
            var subDivisionId = await _subDivisionRepository.InsertAndGetIdAsync(subDivision);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 12; // Sub Division Pages Name
            dataVaulteActivityLog.ActionId =(int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Sub Division Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = subDivisionId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SubDivision_Edit)]
        protected virtual async Task Update(CreateOrEditSubDivisionDto input)
        {
            var subDivisionResult = await _subDivisionRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            int SectionId = 12; // Division Page Name
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = SectionId; // Division Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
            dataVaulteActivityLog.ActionNote = "Sub Division Updated Name :- " + subDivisionResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var DocumentListActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (subDivisionResult.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = subDivisionResult.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (subDivisionResult.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = subDivisionResult.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (subDivisionResult.DivisionId != input.DivisionId)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Division";
                dataVaultsHistory.PrevValue = _divisionRepository.GetAll().Where(e => e.Id == subDivisionResult.DivisionId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.CurValue = _divisionRepository.GetAll().Where(e => e.Id == input.DivisionId).Select(e => e.Name).FirstOrDefault();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = DocumentListActivityLogId;
                dataVaultsHistory.SectionId = SectionId;// Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, subDivisionResult);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_SubDivision_Delete)]
        public async Task Delete(EntityDto input)
        {
            var subDivisionResult = await _subDivisionRepository.FirstOrDefaultAsync(input.Id);
            await _subDivisionRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 12; // Sub Division Pages Name
            dataVaulteActivityLog.ActionId =  (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Sub Division Deleted Name :-" + subDivisionResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

    }
}
