﻿using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.CancelReasons.Exporting
{
    public interface ICancelReasonsExcelExporter
    {
        FileDto ExportToFile(List<GetCancelReasonsForViewDto> cancelreasons);
    }
}
