﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.CancelReasons.Exporting
{
    public class CancelReasonsExcelExporter : NpoiExcelExporterBase, ICancelReasonsExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CancelReasonsExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetCancelReasonsForViewDto> leadType)
        {
            return CreateExcelPackage(
                "CancelReasons.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("CancelReasons"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );                   

                    AddObjects(
                        sheet, leadType,
                        _ => _.CancelReasons.Name
                        );
                });
        }
    }
}
