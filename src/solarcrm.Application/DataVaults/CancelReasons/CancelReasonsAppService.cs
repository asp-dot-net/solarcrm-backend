﻿
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.CancelReasons.Exporting;
using Abp.Application.Services.Dto;
using solarcrm.Common;

namespace solarcrm.DataVaults.CancelReasons
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CancelReasons)]
    public class CancelReasonsAppService : solarcrmAppServiceBase, ICancelReasonsAppService
    {
        private readonly IRepository<CancelReasons> _cancelReasonsRepository;
        private readonly ICancelReasonsExcelExporter _cancelReasonsExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public CancelReasonsAppService(IRepository<CancelReasons> CancelReasonsRepository, ICancelReasonsExcelExporter CancelReasonsExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _cancelReasonsRepository = CancelReasonsRepository;
            _cancelReasonsExcelExporter = CancelReasonsExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetCancelReasonsForViewDto>> GetAll(GetAllCancelReasonsInput input)
        {
            var filteredCancelReasons = _cancelReasonsRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredCancelReasons = filteredCancelReasons.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var cancelReasons = from o in pagedAndFilteredCancelReasons
                         select new GetCancelReasonsForViewDto()
                         {
                             CancelReasons = new CancelReasonsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id,
                                 CreatedDate = o.CreationTime,

                             }
                         };

            var totalCount = await filteredCancelReasons.CountAsync();

            return new PagedResultDto<GetCancelReasonsForViewDto>(totalCount, await cancelReasons.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_Edit)]
        public async Task<GetCancelReasonsForEditOutput> GetCancelReasonsForEdit(EntityDto input)
        {
            var cancelReasons = await _cancelReasonsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetCancelReasonsForEditOutput { CancelReasons = ObjectMapper.Map<CreateOrEditCancelReasonsDto>(cancelReasons) };

            return output;
        }
        public async Task<GetCancelReasonsForViewDto> GetCancelReasonsForView(int id)
        {
            var cancelReasons = await _cancelReasonsRepository.GetAsync(id);

            var output = new GetCancelReasonsForViewDto { CancelReasons = ObjectMapper.Map<CancelReasonsDto>(cancelReasons) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_ExportToExcel)]
        public async Task<FileDto> GetCancelReasonsToExcel(GetAllCancelReasonsForExcelInput input)
        {
            var filteredCancelReasons = _cancelReasonsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredCancelReasons
                         select new GetCancelReasonsForViewDto()
                         {
                             CancelReasons = new CancelReasonsDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var cancelReasonsListDtos = await query.ToListAsync();

            return _cancelReasonsExcelExporter.ExportToFile(cancelReasonsListDtos);
        }


        public async Task CreateOrEdit(CreateOrEditCancelReasonsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_Create)]
        protected virtual async Task Create(CreateOrEditCancelReasonsDto input)
        {
            try
            {
                var cancelReasons = ObjectMapper.Map<CancelReasons>(input);

                if (AbpSession.TenantId != null)
                {
                    cancelReasons.TenantId = (int?)AbpSession.TenantId;
                }

                var cancelReasonsId = await _cancelReasonsRepository.InsertAndGetIdAsync(cancelReasons);

                //Add Activity Log
                DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                if (AbpSession.TenantId != null)
                {
                    dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                }
                dataVaulteActivityLog.SectionId = 58; // CancelReasons Pages Name
                dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created;  // Created
                dataVaulteActivityLog.ActionNote = "Cancel Reason Created Name :-" + input.Name;
                dataVaulteActivityLog.SectionValueId = cancelReasonsId;
                await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_Edit)]
        protected virtual async Task Update(CreateOrEditCancelReasonsDto input)
        {
            var cancelReasons = await _cancelReasonsRepository.FirstOrDefaultAsync((int)input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 58; // CancelReasons Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated;  // Updated
            dataVaulteActivityLog.ActionNote = "Cancel Reason Updated Name :-" + cancelReasons.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var cancelReasonsActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if (cancelReasons.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = cancelReasons.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 58; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            if (cancelReasons.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = cancelReasons.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated; // Pages name
                dataVaultsHistory.ActivityLogId = cancelReasonsActivityLogId;
                dataVaultsHistory.SectionId = 58;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, cancelReasons);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_Delete)]
        public async Task Delete(EntityDto input)
        {
            var cancelreasonResult = await _cancelReasonsRepository.FirstOrDefaultAsync(input.Id);
            await _cancelReasonsRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 58; // Cancel Reason Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted;  // Updated
            dataVaulteActivityLog.ActionNote = "Cancel Reason Deleted Name :-" + cancelreasonResult.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
