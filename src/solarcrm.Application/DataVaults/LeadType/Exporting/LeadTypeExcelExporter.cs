﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.DataVaults.LeadType.Dto;
using solarcrm.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadType.Exporting
{
    public class LeadTypeExcelExporter : NpoiExcelExporterBase, ILeadTypeExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public LeadTypeExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetLeadTypeForViewDto> leadType)
        {
            return CreateExcelPackage(
                "LeadType.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("LeadType"));

                    AddHeader(
                        sheet,
                        L("Name")
                        );

                    //AddObjects(
                    //    sheet, 2, leadSource,
                    //    _ => _.Location.Name
                    //    );

                    AddObjects(
                        sheet, leadType,
                        _ => _.LeadType.Name
                        );
                });
        }
    }
}
