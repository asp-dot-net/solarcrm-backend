﻿using solarcrm.DataVaults.LeadType.Dto;
using solarcrm.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.LeadType.Exporting
{
    public interface ILeadTypeExcelExporter
    {
        FileDto ExportToFile(List<GetLeadTypeForViewDto> leadSource);
    }
}
