﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using solarcrm.DataVaults.LeadType.Dto;
using solarcrm.Dto;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.LeadType.Exporting;
using solarcrm.Authorization;
using Abp.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using System.Collections.Generic;
using Abp.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Common;

namespace solarcrm.DataVaults.LeadType
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadType)]
    public class LeadTypeAppService : solarcrmAppServiceBase, ILeadTypeAppService
    {
        private readonly IRepository<LeadType> _leadTypeRepository;
        private readonly ILeadTypeExcelExporter _leadTypeExcelExporter;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public LeadTypeAppService(IRepository<LeadType> leadTypeRepository, ILeadTypeExcelExporter leadTypeExcelExporter, IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository, IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _leadTypeRepository = leadTypeRepository;
            _leadTypeExcelExporter = leadTypeExcelExporter;
            _dataVaultsActivityLogRepository = dataVaulteActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<PagedResultDto<GetLeadTypeForViewDto>> GetAll(GetAllLeadTypeInput input)
        {
            var filteredLeadType = _leadTypeRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.Name.Contains(input.Filter));

            var pagedAndFilteredLeadType = filteredLeadType.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var leadType = from o in pagedAndFilteredLeadType
                             select new GetLeadTypeForViewDto()
                             {
                                 LeadType = new LeadTypeDto
                                 {
                                     Name = o.Name,
                                     IsActive = o.IsActive,
                                     Id = o.Id,
                                     CreatedDate = o.CreationTime
                                 }
                             };

            var totalCount = await filteredLeadType.CountAsync();

            return new PagedResultDto<GetLeadTypeForViewDto>(totalCount, await leadType.ToListAsync());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadType_Edit)]
        public async Task<GetLeadTypeForEditOutput> GetLeadTypeForEdit(EntityDto input)
        {
            var leadType = await _leadTypeRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetLeadTypeForEditOutput { LeadType = ObjectMapper.Map<CreateOrEditLeadTypeDto>(leadType) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditLeadTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        public async Task<GetLeadTypeForViewDto> GetLeadTypeForView(int id)
        {
            var leadType = await _leadTypeRepository.GetAsync(id);

            var output = new GetLeadTypeForViewDto { LeadType = ObjectMapper.Map<LeadTypeDto>(leadType) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadType_ExportToExcel)]
        public async Task<FileDto> GetLeadTypeToExcel(GetAllLeadTypeForExcelInput input)
        {
            var filteredLeadTypes = _leadTypeRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter));

            var query = (from o in filteredLeadTypes
                         select new GetLeadTypeForViewDto()
                         {
                             LeadType = new LeadTypeDto
                             {
                                 Name = o.Name,
                                 IsActive = o.IsActive,
                                 Id = o.Id
                             }
                         });

            var leadTypesListDtos = await query.ToListAsync();

            return _leadTypeExcelExporter.ExportToFile(leadTypesListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadType_Create)]
        protected virtual async Task Create(CreateOrEditLeadTypeDto input)
        {
            var leadType = ObjectMapper.Map<LeadType>(input);

            if (AbpSession.TenantId != null)
            {
                leadType.TenantId = (int?)AbpSession.TenantId;
            }

            var leadTypeId = await _leadTypeRepository.InsertAndGetIdAsync(leadType);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 2; // LeadType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Created; // Created
            dataVaulteActivityLog.ActionNote = "Lead Type Created Name :-" + input.Name;
            dataVaulteActivityLog.SectionValueId = leadTypeId;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadType_Edit)]
        protected virtual async Task Update(CreateOrEditLeadTypeDto input)
        {
            var leadType = await _leadTypeRepository.FirstOrDefaultAsync((int)input.Id);
            
            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 2; // LeadType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated;  // Updated
            dataVaulteActivityLog.ActionNote = "Lead Type Updated Name :-" + leadType.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            var leadTypeActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

            #region Added Log History
            var List = new List<DataVaultsHistory>();
            if(leadType.Name != input.Name)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "Name";
                dataVaultsHistory.PrevValue = leadType.Name;
                dataVaultsHistory.CurValue = input.Name;
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                dataVaultsHistory.ActivityLogId = leadTypeActivityLogId;
                dataVaultsHistory.SectionId = 2; // Pages name
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }
            
            if(leadType.IsActive != input.IsActive)
            {
                DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                if (AbpSession.TenantId != null)
                {
                    dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                }
                dataVaultsHistory.FieldName = "IsActive";
                dataVaultsHistory.PrevValue = leadType.IsActive.ToString();
                dataVaultsHistory.CurValue = input.IsActive.ToString();
                dataVaultsHistory.Action = "Edit";
                dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;  // Pages name
                dataVaultsHistory.ActivityLogId = leadTypeActivityLogId;
                dataVaultsHistory.SectionId = 2;
                dataVaultsHistory.SectionValueId = input.Id;
                List.Add(dataVaultsHistory);
            }

            await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
            await _dbContextProvider.GetDbContext().SaveChangesAsync();

            #endregion

            ObjectMapper.Map(input, leadType);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_DataVaults_LeadType_Delete)]
        public async Task Delete(EntityDto input)
        {
            var leadType = await _leadTypeRepository.FirstOrDefaultAsync(input.Id);
            await _leadTypeRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 2; // LeadType Pages Name
            dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Deleted; // Deleted
            dataVaulteActivityLog.ActionNote = "Lead Type Deleted Name :-" + leadType.Name;
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }
    }
}
