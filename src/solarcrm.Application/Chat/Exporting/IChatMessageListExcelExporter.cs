﻿using System.Collections.Generic;
using Abp;
using solarcrm.Chat.Dto;
using solarcrm.Dto;

namespace solarcrm.Chat.Exporting
{
    public interface IChatMessageListExcelExporter
    {
        FileDto ExportToFile(UserIdentifier user, List<ChatMessageExportDto> messages);
    }
}
