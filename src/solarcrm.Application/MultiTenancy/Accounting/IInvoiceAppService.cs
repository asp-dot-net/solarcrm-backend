﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using solarcrm.MultiTenancy.Accounting.Dto;

namespace solarcrm.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
