﻿using System.Threading.Tasks;
using Abp.Application.Services;
using solarcrm.Install.Dto;

namespace solarcrm.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}