﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentPaid.Importing.Dto
{
    public class ImportPaymentPaidReportDto : EntityDto<int?>
    {       
        public string ProjectNo { get; set; }
        public string PaymentType { get; set; }
        public DateTime? PayDate { get; set; }

        public string PaymentNotes { get; set; }
        public virtual int OrganizationUnitId { get; set; }
        public virtual int TenantId { get; set; }
        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
