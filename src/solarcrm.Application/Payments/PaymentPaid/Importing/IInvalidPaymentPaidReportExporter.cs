﻿using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentPaid.Importing.Dto
{
    public interface IInvalidPaymentPaidReportExporter
    {
        FileDto ExportToFile(List<ImportPaymentPaidReportDto> PaymentPaidReportListDtos);
    }
}
