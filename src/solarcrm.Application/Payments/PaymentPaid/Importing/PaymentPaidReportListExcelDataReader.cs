﻿using Abp.Localization;
using Abp.Localization.Sources;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Payments.PaymentPaid.Importing.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.SS.UserModel;

namespace solarcrm.Payments.PaymentPaid.Importing
{
    public class PaymentPaidReportListExcelDataReader : NpoiExcelImporterBase<ImportPaymentPaidReportDto>, IPaymentPaidReportListExcelDataReader
    {
        private readonly ILocalizationSource _localizationSource;

        public PaymentPaidReportListExcelDataReader(ILocalizationManager localizationManager)
        {
            _localizationSource = localizationManager.GetSource(solarcrmConsts.LocalizationSourceName);
        }

        public List<ImportPaymentPaidReportDto> GetPaymentPaidReportFromExcel(byte[] fileBytes, string filename)
        {
            return ProcessExcelFile(fileBytes, ProcessExcelRow, filename);
        }

        private ImportPaymentPaidReportDto ProcessExcelRow(ISheet worksheet, int row)
        {
            if (IsRowEmpty(worksheet, row))
            {
                return null;
            }

            var exceptionMessage = new StringBuilder();
            var paymentItem = new ImportPaymentPaidReportDto();

            IRow roww = worksheet.GetRow(row);
            List<ICell> cells = roww.Cells;
            List<string> rowData = new List<string>();

            for (int colNumber = 0; colNumber < roww.LastCellNum; colNumber++)
            {
                ICell cell = roww.GetCell(colNumber, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                rowData.Add(Convert.ToString(cell));
            }

            try
            {
                if (rowData.Count() > 0)
                {
                    paymentItem.ProjectNo = Convert.ToString(rowData[0]).Trim();
                    paymentItem.PaymentType = Convert.ToString(rowData[1]).Trim();
                    paymentItem.PayDate = (rowData[2] == "" ? (DateTime?)null : (DateTime?)Convert.ToDateTime(rowData[2])); // Convert.ToString(rowData[2]).Trim();
                    paymentItem.PaymentNotes = Convert.ToString(rowData[3].Trim());
                }
            }
            catch (System.Exception exception)
            {
                paymentItem.Exception = exception.Message;
            }

            return paymentItem;
        }

        private string GetLocalizedExceptionMessagePart(string parameter)
        {
            return _localizationSource.GetString("{0}IsInvalid", _localizationSource.GetString(parameter)) + "; ";
        }
        private string GetRequiredValueFromRowOrNull(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].StringCellValue;
            if (cellValue != null && !string.IsNullOrWhiteSpace(cellValue))
            {
                return cellValue;
            }

            exceptionMessage.Append(GetLocalizedExceptionMessagePart(columnName));
            return null;
        }

        private double GetValue(ISheet worksheet, int row, int column, string columnName, StringBuilder exceptionMessage)
        {
            var cellValue = worksheet.GetRow(row).Cells[column].NumericCellValue;

            return cellValue;
        }

        private bool IsRowEmpty(ISheet worksheet, int row)
        {
            var cell = worksheet.GetRow(row)?.Cells.FirstOrDefault();
            return cell == null || string.IsNullOrWhiteSpace(cell.StringCellValue);
        }
    }
}
