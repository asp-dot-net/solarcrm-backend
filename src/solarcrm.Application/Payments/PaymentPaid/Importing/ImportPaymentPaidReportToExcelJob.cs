﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Localization;
using Abp.ObjectMapping;
using Abp.Threading;
using Abp.UI;
using solarcrm.Common;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.PaymentType;
using solarcrm.EntityFrameworkCore;
using solarcrm.Notifications;
using solarcrm.Payments.PaymentPaid.Dto;
using solarcrm.Payments.PaymentPaid.Importing.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentPaid.Importing
{
    public class ImportPaymentPaidReportToExcelJob : BackgroundJob<ImportPaymentPaidReportFromExcelJobArgs>, ITransientDependency
    {
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IPaymentPaidReportListExcelDataReader _paymentpaidReportListExcelDataReader;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IObjectMapper _objectMapper;
        private readonly IAppNotifier _appNotifier;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IInvalidPaymentPaidReportExporter _invalidPaymentPaidReportExporter;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailsReportrepository;
        private readonly IRepository<PaymentReceiptUploadJob> _paymentReportrepository;
        private readonly IRepository<PaymentType> _paymentTypeReportrepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly ICommonLookupAppService _CommonDocumentSaveRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private int _sectionId = 45;

        public ImportPaymentPaidReportToExcelJob(
            IPaymentPaidReportListExcelDataReader paymentpaidReportListExcelDataReader,
            IBinaryObjectManager binaryObjectManager,
            IRepository<PaymentReceiptUploadJob> paymentReportrepository,
            IRepository<PaymentDetailsJob> paymentDetailsReportrepository,
            IObjectMapper objectMapper,
            IAppNotifier appNotifier,
            IUnitOfWorkManager unitOfWorkManager,
            IInvalidPaymentPaidReportExporter invalidPaymentReportExporter,
            IRepository<Job.Jobs> jobRepository,
            IRepository<PaymentType> paymentTypeReportrepository,
        IRepository<LeadActivityLogs> leadactivityRepository,
            ICommonLookupAppService CommonDocumentSaveRepository,
            IRepository<DataVaulteActivityLog> dataVaultsActivityLogRepository,
        IDbContextProvider<solarcrmDbContext> dbContextProvider
            )
        {
            _paymentpaidReportListExcelDataReader = paymentpaidReportListExcelDataReader;
            _binaryObjectManager = binaryObjectManager;
            _objectMapper = objectMapper;
            _appNotifier = appNotifier;
            _paymentReportrepository = paymentReportrepository;
            _paymentDetailsReportrepository = paymentDetailsReportrepository;
            _unitOfWorkManager = unitOfWorkManager;
            _invalidPaymentPaidReportExporter = invalidPaymentReportExporter;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _paymentTypeReportrepository = paymentTypeReportrepository;
            _dataVaultsActivityLogRepository = dataVaultsActivityLogRepository;            
            _CommonDocumentSaveRepository = CommonDocumentSaveRepository;
            _dbContextProvider = dbContextProvider;
        }


        [UnitOfWork]

        public override void Execute(ImportPaymentPaidReportFromExcelJobArgs args)
        {
            var stockItems = GetPaymentPaidReportListFromExcelOrNull(args);
            if (stockItems == null || !stockItems.Any())
            {
                SendInvalidExcelNotification(args);
                return;
            }
            else
            {
                var dd = _jobRepository.GetAll();
                var invalidPaymentPaid = new List<ImportPaymentPaidReportDto>();

                foreach (var paidItem in stockItems)
                {
                    if (paidItem.CanBeImported())
                    {
                        try
                        {
                            AsyncHelper.RunSync(() => CreateOrUpdatePaymentPaidReportAsync(paidItem, args));
                        }
                        catch (UserFriendlyException exception)
                        {                            
                            invalidPaymentPaid.Add(paidItem);
                        }                        
                        catch
                        {                         
                            invalidPaymentPaid.Add(paidItem);
                        }
                    }
                    else
                    {
                        invalidPaymentPaid.Add(paidItem);
                    }
                }

                using (var uow = _unitOfWorkManager.Begin())
                {
                    using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                    {
                        AsyncHelper.RunSync(() => ProcessImportPaymentPaidReportResultAsync(args, invalidPaymentPaid));
                    }

                    uow.Complete();
                }
            }
        }


        private List<ImportPaymentPaidReportDto> GetPaymentPaidReportListFromExcelOrNull(ImportPaymentPaidReportFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    try
                    {
                        var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));
                        string filename = Path.GetExtension(file.Description);
                        var files = _paymentpaidReportListExcelDataReader.GetPaymentPaidReportFromExcel(file.Bytes,filename);

                        return files;
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                    finally
                    {
                        uow.Complete();
                    }
                }
            }
        }

        private async Task CreateOrUpdatePaymentPaidReportAsync(ImportPaymentPaidReportDto input, ImportPaymentPaidReportFromExcelJobArgs args)
        {
            PaymentReceiptUploadJob PaymentItem = new PaymentReceiptUploadJob();
            try
            {
                var PaymentTypeId = 0;
                //var JobStatusId = 0;
                using (_unitOfWorkManager.Current.SetTenantId((int)args.TenantId))
                {

                    if (input.PaymentType != null)
                    {
                        PaymentTypeId = _paymentTypeReportrepository.GetAll().Where(e => e.Name.ToLower() == input.PaymentType.ToLower()).Select(e => e.Id).FirstOrDefault();//.FirstOrDefaultAsync();
                    }
                    if (PaymentTypeId == 0)
                    {
                        input.Exception = "PaymentType Found";
                        throw new Exception("PaymentType Not Found");
                    }
                    PaymentItem = (from o in _jobRepository.GetAll()
                                   join o3 in _paymentDetailsReportrepository.GetAll() on o.Id equals o3.PaymentJobId into j3
                                   from s3 in j3.DefaultIfEmpty()

                                   join o2 in _paymentReportrepository.GetAll() on s3.Id equals o2.PaymentId into j2
                                   from s2 in j2.DefaultIfEmpty()
                                   where (o.JobNumber == input.ProjectNo && s2.PaymentRecPaidDate == input.PayDate)
                                   select new PaymentReceiptUploadJob()
                                   { }
                                       ).FirstOrDefault();
                    //PaymentItem = await _paymentReportrepository.GetAll().Where(e => e.job == input.ProjectName.Trim()).FirstOrDefaultAsync();
                    if (PaymentItem == null)
                    {
                        var PaymentItemNew = _objectMapper.Map<PaymentReceiptUploadJob>(input);
                        PaymentItemNew.PaymentRecNotes = input.PaymentNotes;
                        PaymentItemNew.IsPaymentRecVerified = true; //true for done And False for Pending                       
                        //PaymentItemNew.LastModifierUserId = (int)args.User.UserId;

                        var PaidReportUpdate = await _paymentReportrepository.UpdateAsync(PaymentItemNew);

                        //Add Activity Log
                        DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                        dataVaulteActivityLog.TenantId = (int)args.TenantId;
                        dataVaulteActivityLog.SectionId = _sectionId;
                        dataVaulteActivityLog.ActionId = 2; //1-Created, 2-Updated, 3-Deleted
                        dataVaulteActivityLog.ActionNote = "Payment Paid Item Updated From Excel";
                        dataVaulteActivityLog.SectionValueId = (int?)PaidReportUpdate.Id;
                        dataVaulteActivityLog.CreatorUserId = (int)args.User.UserId;
                        await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendInvalidExcelNotification(ImportPaymentPaidReportFromExcelJobArgs args)
        {
            using (var uow = _unitOfWorkManager.Begin())
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                        args.User,
                        new LocalizableString("FileCantBeConvertedToPaymentPaidReport", solarcrmConsts.LocalizationSourceName),
                        null,
                        Abp.Notifications.NotificationSeverity.Warn));
                }
                uow.Complete();
            }
        }
        private async Task ProcessImportPaymentPaidReportResultAsync(ImportPaymentPaidReportFromExcelJobArgs args, List<ImportPaymentPaidReportDto> invalidPaidItem)
        {
            if (invalidPaidItem.Any())
            {
                var file = _invalidPaymentPaidReportExporter.ExportToFile(invalidPaidItem);
                await _appNotifier.SomeStockItemCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
            }
            else
            {
                await _appNotifier.SendMessageAsync(
                    args.User,
                    new LocalizableString("AllPaymentPaidItemSuccessfullyImportedFromExcel", solarcrmConsts.LocalizationSourceName),
                    null,
                    Abp.Notifications.NotificationSeverity.Success);
            }
        }    
    }
}
