﻿using Abp.Dependency;
using solarcrm.Payments.PaymentPaid.Importing.Dto;
using System;
using System.Collections.Generic;

namespace solarcrm.Payments.PaymentPaid.Importing
{
    public interface IPaymentPaidReportListExcelDataReader : ITransientDependency
    {
        List<ImportPaymentPaidReportDto> GetPaymentPaidReportFromExcel(byte[] fileBytes, string filename);
    }
}
