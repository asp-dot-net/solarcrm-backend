﻿using Abp.Dependency;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Payments.PaymentPaid.Importing.Dto;
using solarcrm.Storage;
using System.Collections.Generic;

namespace solarcrm.Payments.PaymentPaid.Importing
{
    internal class InvalidPaymentPaidReportExporter : NpoiExcelExporterBase, IInvalidPaymentPaidReportExporter, ITransientDependency
    {
        public InvalidPaymentPaidReportExporter(ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
        }

        public FileDto ExportToFile(List<ImportPaymentPaidReportDto> paymentPaidReportListDtos)
        {
            return CreateExcelPackage(
                "InvalidPaymentPaidReportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet(L("InvalidPaymentPaidReportList"));

                    AddHeader(
                        sheet,
                        L("ProjectNo"),
                        L("PaymentType"),
                        L("PayDate"),
                        L("PayDate"),
                        L("Refuse Reason")
                    );
                    AddObjects(
                        sheet, paymentPaidReportListDtos,
                        _ => _.ProjectNo,
                        _ => _.PaymentType,
                        _ => _.PayDate,
                        _ => _.PaymentNotes,
                        _ => _.Exception
                    );

                    for (var i = 0; i < 8; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }
    }
}
