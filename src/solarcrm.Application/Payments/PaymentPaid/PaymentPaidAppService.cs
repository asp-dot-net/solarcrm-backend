﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.StockItem;
using solarcrm.EntityFrameworkCore;
using solarcrm.Organizations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Job;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.Variation;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.LeadStatus;
using Abp.Organizations;
using solarcrm.DataVaults;
using Abp.Timing.Timezone;
using solarcrm.DataVaults.Price;
using solarcrm.Payments.Dto;
using solarcrm.Payments.PaymentPaid.Dto;
using solarcrm.Dto;
using solarcrm.Payments.PaymentPaid.Exporting;
using Abp.Authorization;
using solarcrm.Authorization;


namespace solarcrm.Payments.PaymentPaid
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentPaid)]
    public class PaymentPaidAppService : solarcrmAppServiceBase, IPaymentPaidAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<OrganizationUnit, long> _OrganizationRepository;
        private readonly IRepository<BankDetailsOrganization> _bankOrganizationDetailsRepository;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailjobRepository;
        private readonly IRepository<PaymentReceiptUploadJob> _paymentReceiptUploadjobRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<StockCategory> _stockCategoryRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<Job.JobVariation.JobVariation> _jobVariationRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly IRepository<Prices> _priceRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<SSPaymentDetails> _ssPaymentDetailsRepository;
        private readonly IRepository<STCPaymentDetails> _stcPaymentDetailsRepository;
        private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IPaymentPaidExcelExporter _PaymentPaidExcelExporter;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public PaymentPaidAppService(
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<PaymentDetailsJob> paymentDetailjobRepository,
            IRepository<PaymentReceiptUploadJob> paymentReceiptUploadjobRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<StockItem> stockItemRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<StockCategory> stockCategoryRepository,
            IRepository<Variation> variationRepository,
            IRepository<Job.JobVariation.JobVariation> jobVariationRepository,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus> leadStatusRepository,
            IRepository<Prices> priceRepository,
            IRepository<OrganizationUnit, long> OrganizationRepository,
            IRepository<BankDetailsOrganization> bankOrganizationDetailsRepository,
            IRepository<SSPaymentDetails> ssPaymentDetailsRepository,
            IRepository<STCPaymentDetails> stcPaymentDetailsRepository,
            IRepository<ApplicationStatus> applicationstatusRepository,
            UserManager userManager,
            IPaymentPaidExcelExporter PaymentPaidExcelExporter,
            IRepository<DataVaulteActivityLog> dataVaultsActivityLogRepository,
            ITimeZoneConverter timeZoneConverter
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _paymentDetailjobRepository = paymentDetailjobRepository;
            _paymentReceiptUploadjobRepository = paymentReceiptUploadjobRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _stockItemRepository = stockItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _stockCategoryRepository = stockCategoryRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _userRepository = userRepository;
            _leadStatusRepository = leadStatusRepository;
            _priceRepository = priceRepository;
            _userManager = userManager;
            _OrganizationRepository = OrganizationRepository;
            _bankOrganizationDetailsRepository = bankOrganizationDetailsRepository;
            _ssPaymentDetailsRepository = ssPaymentDetailsRepository;
            _stcPaymentDetailsRepository = stcPaymentDetailsRepository;
            _applicationstatusRepository = applicationstatusRepository;
            _dataVaultsActivityLogRepository = dataVaultsActivityLogRepository;
            _PaymentPaidExcelExporter = PaymentPaidExcelExporter;
            _timeZoneConverter = timeZoneConverter;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentPaid)]
        public async Task<PagedResultDto<GetPaymentPaidForViewDto>> GetAllPaymentPaid(GetAllPaymentPaidInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);

            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            IList<int> billnumberlist = await _paymentDetailjobRepository.GetAll().Select(e => e.Id).ToListAsync();
            IList<int> stcbillnumberlist = null;
            IList<int> ssbillnumberlist = null;
            if (input.BillFilter != null && input.BillFilter == "stc")
            {
                stcbillnumberlist = await _stcPaymentDetailsRepository.GetAll().Where(t => billnumberlist.Contains(t.STCPaymentId)).Select(x => x.STCPaymentId).ToListAsync();
            }
            if (input.BillFilter != null && input.BillFilter == "ss")
            {
                ssbillnumberlist = await _ssPaymentDetailsRepository.GetAll().Where(t => billnumberlist.Contains(t.SSPaymentId)).Select(x => x.SSPaymentId).ToListAsync();
            }

            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            //{
            //    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            //}

            var filteredpaymentPaid = _paymentReceiptUploadjobRepository.GetAll().Include(e => e.PaymentIdFK.JobsIdFK)
                .Include(e => e.PaymentIdFK.JobsIdFK.ApplicationStatusIdFK).Include(e => e.PaymentIdFK.JobsIdFK.LeadFK)
                .Include(e => e.PaymentIdFK.JobsIdFK.LeadFK.SolarTypeIdFk)
                .Include(e => e.PaymentModeIdFK).Include(e => e.PaymentTypeIdFK)
                                //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.PaymentIdFK.JobsIdFK.JobNumber.Contains(input.Filter))
                                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.PaymentIdFK.JobsIdFK.JobNumber.Contains(input.Filter) || e.PaymentIdFK.JobsIdFK.LeadFK.CustomerName.Contains(input.Filter)
                                || e.PaymentIdFK.JobsIdFK.LeadFK.EmailId.Contains(input.Filter) || e.PaymentIdFK.JobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) 
                                || e.PaymentIdFK.JobsIdFK.LeadFK.MobileNumber.Contains(input.Filter) 
                                //|| e.PaymentIdFK.JobsIdFK.LeadFK.AddressLine1.Contains(input.Filter) || e.PaymentIdFK.JobsIdFK.LeadFK.AddressLine2.Contains(input.Filter)
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.PaymentIdFK.JobsIdFK.LeadId)))
                                .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusId))
                                .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.PaymentIdFK.JobsIdFK.ApplicationStatusId))
                                .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.DiscomId))
                                .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.CircleId))
                                .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.DivisionId))
                                .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.SubDivisionId))
                                .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.SolarTypeId))
                                .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.TenantId))
                                .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.ChanelPartnerID))
                                .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "stc", e => stcbillnumberlist.Contains(e.PaymentId))
                                .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "ss", e => ssbillnumberlist.Contains(e.PaymentId))
                                .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "both", e => stcbillnumberlist.Contains(e.PaymentId) && stcbillnumberlist.Contains(e.PaymentId))
                                .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "none", e => !stcbillnumberlist.Contains(e.PaymentId) && !stcbillnumberlist.Contains(e.PaymentId)) // Pending Logic
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "billingdate" && input.StartDate != null, e => e.PaymentRecEnterDate >= SDate.Value.Date && e.PaymentRecEnterDate <= EDate.Value.Date)
                                .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "verificationdate" && input.StartDate != null, e => e.PaymentRecPaidDate >= SDate.Value.Date && e.PaymentRecEnterDate <= EDate.Value.Date)
                                .WhereIf(input.PaymentStatusFilter != null && input.PaymentStatusFilter == 1, e => e.IsPaymentRecVerified == true)
                                .WhereIf(input.PaymentStatusFilter != null && input.PaymentStatusFilter == 0, e => e.IsPaymentRecVerified == false)
                                .Where(e => e.PaymentIdFK.JobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);

            var pagedAndFilteredJob = filteredpaymentPaid.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var paymentPaid = from o in pagedAndFilteredJob
                              select new GetPaymentPaidForViewDto()
                              {
                                  paymentPaid = new PaymentPaidDto
                                  {
                                      Id = o.Id,
                                      leaddata = ObjectMapper.Map<LeadsDto>(o.PaymentIdFK.JobsIdFK.LeadFK),
                                      JobId = o.PaymentIdFK.JobsIdFK.Id,
                                      ProjectNumber = o.PaymentIdFK.JobsIdFK.JobNumber,
                                      ////JobStatus = o.JobStatusFk.Name,
                                      CustomerName = o.PaymentIdFK.JobsIdFK.LeadFK.CustomerName,
                                      Address = (o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine1 == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine1 + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine2 == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine2 + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.PaymentIdFK.JobsIdFK.LeadFK.Pincode == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.Pincode),
                                      Mobile = o.PaymentIdFK.JobsIdFK.LeadFK.MobileNumber,
                                      PaymentRecEnterDate = o.PaymentRecEnterDate,
                                      PaymentRecPaidDate = o.PaymentRecPaidDate,
                                      PaymentTotal = o.PaymentIdFK.NetAmount,
                                      PaymentRecTotalPaid = o.PaymentRecTotalPaid,
                                      TotalProjectCost = o.PaymentIdFK.JobsIdFK.TotalJobCost,
                                      PaymentRefferenceNo = o.PaymentRecRefNo,
                                      PaymentMode = o.PaymentModeIdFK.Name,
                                      PaymentType = o.PaymentTypeIdFK.Name,
                                      PaymentChequeStatus = o.PaymentRecChequeStatus,
                                      IsPaymentRecVerified = o.IsPaymentRecVerified,
                                      PaymentRecVerifiedBy = _userRepository.GetAll().Where(e => e.Id == o.PaymentIdFK.JobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                      PaymentAccountInvoiceNotes = o.PaymentIdFK.PaymentAccountInvoiceNotes,
                                      PaymentRecNotes = o.PaymentRecNotes,
                                      LeadStatusName = o.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                      FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.PaymentIdFK.JobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                      FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.PaymentIdFK.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                      Notes = o.PaymentIdFK.JobsIdFK.LeadFK.Notes,
                                      LeadStatusColorClass = o.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                      LeadStatusIconClass = o.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                      Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.PaymentIdFK.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                      BalanceOwing = o.PaymentIdFK.BalanceOwing,
                                      PaymentDeliveryOption = o.PaymentIdFK.PaymentDeliveryOption,
                                      ApplicationStatusdata = o.PaymentIdFK.JobsIdFK.ApplicationStatusIdFK,
                                      PaymentRecModeId = o.PaymentModeIdFK.Id,
                                      PaymentRecTypeId = o.PaymentTypeIdFK.Id
                                  },
                                  paymentPaidSummaryCount = PaymentPaidSummaryCount(filteredpaymentPaid.ToList())
                              };

            var totalCount = await filteredpaymentPaid.CountAsync();
            return new PagedResultDto<GetPaymentPaidForViewDto>(totalCount, await paymentPaid.ToListAsync());
        }

        public PaymentPaidSummaryCount PaymentPaidSummaryCount(List<PaymentReceiptUploadJob> filteredLeads)
        {
            var output = new PaymentPaidSummaryCount();
            if (filteredLeads.Count > 0)
            {
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);
               var result = filteredLeads.GroupBy(p => p.PaymentId).Select(grp => grp.First()).ToList();
                output.Total = Convert.ToString(result.Sum(x => x.PaymentIdFK.NetAmount));
                output.TotalOwingAmount = Convert.ToString(result.Sum(x => x.PaymentIdFK.BalanceOwing));
                output.TotalPanel = Convert.ToString(panelCount);
                output.TotalKilowatt = Convert.ToString(result.Sum(x => x.PaymentIdFK.JobsIdFK.PvCapacityKw));
                output.ReceivedAmount = Convert.ToString(filteredLeads.Where(x=>x.IsPaymentRecVerified).Sum(x => x.PaymentRecTotalPaid));
                output.EastimatePaid = Convert.ToString(result.Where(x => x.PaymentIdFK.JobsIdFK.EastimatePaidStatus == "Paid").Count());
                output.FeasiblityApproved = Convert.ToString(result.Where(x => x.PaymentIdFK.JobsIdFK.ApplicationStatusId == 4).Count());
            }
            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_QuickView)]
        public async Task<PaymentPaidDto> GetPaymentPaidQuickView(EntityDto input)
        {
            try
            {
                //var leads = _leadRepository.FirstOrDefault(input.Id);
                var job = await _jobRepository.GetAll().Include(e => e.LeadFK).
                    Include(e => e.LeadFK.DivisionIdFk).
                    Include(e => e.LeadFK.SubDivisionIdFk).
                    Include(e => e.LeadFK.DiscomIdFk).
                    Include(e => e.LeadFK.CityIdFk).
                    Include(e => e.LeadFK.TalukaIdFk).
                    Include(e => e.LeadFK.DistrictIdFk).
                    Include(e => e.LeadFK.StateIdFk).
                    Where(e => e.Id == input.Id).FirstOrDefaultAsync();

                var output = new PaymentPaidDto
                {
                    leaddata = ObjectMapper.Map<LeadsDto>(job.LeadFK),
                    EastimatePaidDate = (DateTime?)job.EastimatePayDate,
                    EastimatePaidStatus = job.EastimatePaidStatus,
                    //Job = ObjectMapper.Map<CreateOrEditJobDto>(job),                   

                };
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<CreateOrEditPaymentReceiptItemsDto> GetPaymentRecVerifiedForView(EntityDto input)
        {
            var paymentReceiptData = await _paymentReceiptUploadjobRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync(); //await _leadRepository.FirstOrDefaultAsync(input.Id);
            if (paymentReceiptData == null)
            {
                paymentReceiptData = new PaymentReceiptUploadJob();
            }
            var output = ObjectMapper.Map<CreateOrEditPaymentReceiptItemsDto>(paymentReceiptData);//new CreateOrEditPaymentReceiptItemsDto           
            return output;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_PaymentVerified)]
        public async Task PaymentVerifiedForAddOrEdit(CreateOrEditPaymentReceiptItemsDto input, int SectionId)
        {
            var paymentvarifiedData = await _paymentReceiptUploadjobRepository.GetAll().Include(e => e.PaymentIdFK).Where(e => e.Id == input.Id).FirstOrDefaultAsync();
            var PaymentoldData = paymentvarifiedData;

            //setString = paymentvarifiedData;
            var List = new List<LeadtrackerHistory>();
            //if (SSpaymentData != null)
            {
                if (paymentvarifiedData == null)
                {
                    paymentvarifiedData = new PaymentReceiptUploadJob();
                }
                paymentvarifiedData.PaymentRecPaidDate = input.PaymentRecPaidDate;
                paymentvarifiedData.PaymentRecChequeStatus = input.PaymentRecChequeStatus;
                paymentvarifiedData.PaymentRecNotes = input.PaymentRecNotes;
                paymentvarifiedData.IsPaymentRecVerified = input.IsPaymentRecVerified;
                if (input.PaymentRecModeId != null)
                {
                    paymentvarifiedData.PaymentRecModeId = input.PaymentRecModeId;
                }
                paymentvarifiedData.PaymentRecVerifiedById = (int?)AbpSession.UserId; // input.PaymentRecVerifiedById;
                var PaymentVerId = await _paymentReceiptUploadjobRepository.InsertOrUpdateAndGetIdAsync(paymentvarifiedData);
                if (input.IsPaymentRecVerified && PaymentVerId != 0 && paymentvarifiedData.PaymentId != 0)
                {
                    var paymentDetail = await _paymentDetailjobRepository.GetAll().Where(x => x.Id == paymentvarifiedData.PaymentId).FirstOrDefaultAsync();
                    if (paymentDetail != null)
                    {
                        paymentDetail.BalanceOwing = paymentDetail.BalanceOwing - input.PaymentRecTotalPaid;
                        paymentDetail.LastModificationTime = DateTime.UtcNow;
                        var paymentDetailId = await _paymentDetailjobRepository.InsertOrUpdateAndGetIdAsync(paymentDetail);
                    }
                }
                if (PaymentVerId != 0)
                {
                    #region Activity Log
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 11;
                    leadactivity.SectionId = SectionId;
                    leadactivity.LeadActionNote = paymentvarifiedData.Id == 0 ? "Payment Verified Details Created" : "Payment Verified Details Modified";
                    leadactivity.LeadId = input.LeadId;
                    leadactivity.OrganizationUnitId = input.OrganizationUnitId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    var leadactid = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    if (input.Id != null)
                    {

                        if (PaymentoldData.PaymentRecPaidDate != input.PaymentRecPaidDate)
                        {
                            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            }
                            objAuditInfo.FieldName = "PaymentRecPaidDate";
                            objAuditInfo.PrevValue = Convert.ToString(PaymentoldData.PaymentRecPaidDate);
                            objAuditInfo.CurValue = Convert.ToString(input.PaymentRecPaidDate);
                            objAuditInfo.Action = "Edit";
                            objAuditInfo.LeadId = input.LeadId;
                            objAuditInfo.LeadActionId = leadactid;
                            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            List.Add(objAuditInfo);
                        }
                        if (PaymentoldData.PaymentRecChequeStatus != input.PaymentRecChequeStatus)
                        {
                            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            }
                            objAuditInfo.FieldName = "PaymentRecChequeStatus";
                            objAuditInfo.PrevValue = Convert.ToString(PaymentoldData.PaymentRecChequeStatus);
                            objAuditInfo.CurValue = Convert.ToString(input.PaymentRecChequeStatus);
                            objAuditInfo.Action = "Edit";
                            objAuditInfo.LeadId = input.LeadId;
                            objAuditInfo.LeadActionId = leadactid;
                            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            List.Add(objAuditInfo);
                        }
                        if (PaymentoldData.PaymentRecNotes != input.PaymentRecNotes)
                        {
                            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            }
                            objAuditInfo.FieldName = "PaymentRecNotes";
                            objAuditInfo.PrevValue = Convert.ToString(PaymentoldData.PaymentRecNotes);
                            objAuditInfo.CurValue = Convert.ToString(input.PaymentRecNotes);
                            objAuditInfo.Action = "Edit";
                            objAuditInfo.LeadId = input.LeadId;
                            objAuditInfo.LeadActionId = leadactid;
                            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            List.Add(objAuditInfo);
                        }
                        if (PaymentoldData.IsPaymentRecVerified != input.IsPaymentRecVerified)
                        {
                            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            }
                            objAuditInfo.FieldName = "IsPaymentRecVerified";
                            objAuditInfo.PrevValue = Convert.ToString(PaymentoldData.IsPaymentRecVerified);
                            objAuditInfo.CurValue = Convert.ToString(input.IsPaymentRecVerified);
                            objAuditInfo.Action = "Edit";
                            objAuditInfo.LeadId = input.LeadId;
                            objAuditInfo.LeadActionId = leadactid;
                            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            List.Add(objAuditInfo);
                        }
                        if (PaymentoldData.PaymentRecTotalPaid != input.PaymentRecTotalPaid)
                        {
                            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            }
                            objAuditInfo.FieldName = "PaymentRecTotalPaid";
                            objAuditInfo.PrevValue = Convert.ToString(PaymentoldData.PaymentRecTotalPaid);
                            objAuditInfo.CurValue = Convert.ToString(input.PaymentRecTotalPaid);
                            objAuditInfo.Action = "Edit";
                            objAuditInfo.LeadId = input.LeadId;
                            objAuditInfo.LeadActionId = leadactid;
                            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            List.Add(objAuditInfo);
                        }
                        await _dbContextProvider.GetDbContext().LeadtrackerHistorys.AddRangeAsync(List);
                        await _dbContextProvider.GetDbContext().SaveChangesAsync();
                    }

                    #endregion
                }

            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_ExportToExcel)]
        public async Task<FileDto> GetPaymentPaidToExcel(GetAllPaymentPaidInput input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));

                List<int> FollowupList = new List<int>();
                List<int> NextFollowupList = new List<int>();
                List<int> Assign = new List<int>();
                List<int> jobnumberlist = new List<int>();

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                var User_List = _userRepository.GetAll();
                var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
                IList<string> role = await _userManager.GetRolesAsync(User);

                var job_list = _jobRepository.GetAll();
                //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

                if (input.Filter != null)
                {
                    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                }
                IList<int> billnumberlist = await _paymentDetailjobRepository.GetAll().Select(e => e.Id).ToListAsync();
                IList<int> stcbillnumberlist = null;
                IList<int> ssbillnumberlist = null;
                if (input.BillFilter != null && input.BillFilter == "stc")
                {
                    stcbillnumberlist = await _stcPaymentDetailsRepository.GetAll().Where(t => billnumberlist.Contains(t.STCPaymentId)).Select(x => x.STCPaymentId).ToListAsync();
                }
                if (input.BillFilter != null && input.BillFilter == "ss")
                {
                    ssbillnumberlist = await _ssPaymentDetailsRepository.GetAll().Where(t => billnumberlist.Contains(t.SSPaymentId)).Select(x => x.SSPaymentId).ToListAsync();
                }

                //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
                //{
                //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
                //}
                //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
                //{
                //    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                //}

                var filteredpaymentPaid = _paymentReceiptUploadjobRepository.GetAll().Include(e => e.PaymentIdFK.JobsIdFK)
                    .Include(e => e.PaymentIdFK.JobsIdFK.ApplicationStatusIdFK).Include(e => e.PaymentIdFK.JobsIdFK.LeadFK)
                    .Include(e => e.PaymentIdFK.JobsIdFK.LeadFK.SolarTypeIdFk)
                    .Include(e => e.PaymentModeIdFK).Include(e => e.PaymentTypeIdFK)
                                    //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.PaymentIdFK.JobsIdFK.JobNumber.Contains(input.Filter))
                                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.PaymentIdFK.JobsIdFK.JobNumber.Contains(input.Filter) || e.PaymentIdFK.JobsIdFK.LeadFK.CustomerName.Contains(input.Filter)
                                    || e.PaymentIdFK.JobsIdFK.LeadFK.EmailId.Contains(input.Filter) || e.PaymentIdFK.JobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter)
                                    || e.PaymentIdFK.JobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                    //|| e.PaymentIdFK.JobsIdFK.LeadFK.AddressLine1.Contains(input.Filter) || e.PaymentIdFK.JobsIdFK.LeadFK.AddressLine2.Contains(input.Filter)
                                    || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.PaymentIdFK.JobsIdFK.LeadId)))
                                    .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusId))
                                    .WhereIf(input.leadApplicationStatusFilter != null && input.leadApplicationStatusFilter.Count() > 0, e => input.leadApplicationStatusFilter.Contains((int)e.PaymentIdFK.JobsIdFK.ApplicationStatusId))
                                    .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.DiscomId))
                                    .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.CircleId))
                                    .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.DivisionId))
                                    .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.SubDivisionId))
                                    .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.SolarTypeId))
                                    .WhereIf(input.TenderIdFilter != null && input.TenderIdFilter.Count() > 0, e => input.TenderIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.TenantId))
                                    .WhereIf(input.employeeIdFilter != null, e => input.employeeIdFilter.Contains((int)e.PaymentIdFK.JobsIdFK.LeadFK.ChanelPartnerID))
                                    .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "stc", e => stcbillnumberlist.Contains(e.PaymentId))
                                    .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "ss", e => ssbillnumberlist.Contains(e.PaymentId))
                                    .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "both", e => stcbillnumberlist.Contains(e.PaymentId) && stcbillnumberlist.Contains(e.PaymentId))
                                    .WhereIf(input.BillFilter != null && input.BillFilter.ToLower() == "none", e => !stcbillnumberlist.Contains(e.PaymentId) && !stcbillnumberlist.Contains(e.PaymentId)) // Pending Logic
                                    .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "billingdate" && input.StartDate != null, e => e.PaymentRecEnterDate >= SDate.Value.Date && e.PaymentRecEnterDate <= EDate.Value.Date)
                                    .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "verificationdate" && input.StartDate != null, e => e.PaymentRecPaidDate >= SDate.Value.Date && e.PaymentRecEnterDate <= EDate.Value.Date)
                                  .Where(e => e.PaymentIdFK.JobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);

                var pagedAndFilteredJob = filteredpaymentPaid.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                var paymentPaid = from o in pagedAndFilteredJob
                                  select new GetPaymentPaidForViewDto()
                                  {
                                      paymentPaid = new PaymentPaidDto
                                      {
                                          Id = o.Id,
                                          leaddata = ObjectMapper.Map<LeadsDto>(o.PaymentIdFK.JobsIdFK.LeadFK),
                                          JobId = o.PaymentIdFK.JobsIdFK.Id,
                                          ProjectNumber = o.PaymentIdFK.JobsIdFK.JobNumber,
                                          ////JobStatus = o.JobStatusFk.Name,
                                          CustomerName = o.PaymentIdFK.JobsIdFK.LeadFK.CustomerName,
                                          Address = (o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine1 == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine1 + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine2 == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.AddressLine2 + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.PaymentIdFK.JobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.PaymentIdFK.JobsIdFK.LeadFK.Pincode == null ? "" : o.PaymentIdFK.JobsIdFK.LeadFK.Pincode),
                                          Mobile = o.PaymentIdFK.JobsIdFK.LeadFK.MobileNumber,
                                          PaymentRecEnterDate = o.PaymentRecEnterDate,
                                          PaymentRecPaidDate = o.PaymentRecPaidDate,
                                          PaymentTotal = o.PaymentIdFK.NetAmount,
                                          PaymentRecTotalPaid = o.PaymentRecTotalPaid,
                                          TotalProjectCost = o.PaymentIdFK.JobsIdFK.TotalJobCost,
                                          PaymentRefferenceNo = o.PaymentRecRefNo,
                                          PaymentMode = o.PaymentModeIdFK.Name,
                                          PaymentType = o.PaymentTypeIdFK.Name,
                                          PaymentChequeStatus = o.PaymentRecChequeStatus,
                                          IsPaymentRecVerified = o.IsPaymentRecVerified,
                                          PaymentRecVerifiedBy = _userRepository.GetAll().Where(e => e.Id == o.PaymentIdFK.JobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                          PaymentAccountInvoiceNotes = o.PaymentIdFK.PaymentAccountInvoiceNotes,
                                          PaymentRecNotes = o.PaymentRecNotes,
                                          LeadStatusName = o.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                          FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.PaymentIdFK.JobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                          FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.PaymentIdFK.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                          Notes = o.PaymentIdFK.JobsIdFK.LeadFK.Notes,
                                          LeadStatusColorClass = o.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                          LeadStatusIconClass = o.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                          Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.PaymentIdFK.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                          BalanceOwing = o.PaymentIdFK.BalanceOwing,
                                          PaymentDeliveryOption = o.PaymentIdFK.PaymentDeliveryOption,
                                          ApplicationStatusdata = o.PaymentIdFK.JobsIdFK.ApplicationStatusIdFK,
                                          PaymentRecModeId = o.PaymentModeIdFK.Id,
                                          PaymentRecTypeId = o.PaymentTypeIdFK.Id
                                      },
                                  };
                var applicationtrackerListDtos = await paymentPaid.ToListAsync();
                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _PaymentPaidExcelExporter.ExportToFile(applicationtrackerListDtos);
                }
                else
                {
                    return _PaymentPaidExcelExporter.ExportToFile(applicationtrackerListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
