﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Payments.PaymentIssue.Dto;
using solarcrm.Payments.PaymentIssue.Exporting;
using solarcrm.Payments.PaymentPaid.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentPaid.Exporting
{
    public class PaymentPaidExcelExporter : NpoiExcelExporterBase, IPaymentPaidExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentPaidExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetPaymentPaidForViewDto> paymentpaid)
        {
            return CreateExcelPackage(
                "PaymentPaid.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("PaymentPaid");

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),                       
                        L("CustomerName"),
                         L("PaidDate"),
                        L("PaymentTotal"),
                        L("Totalprojectcost"),
                        L("ModeofPayement"),
                        L("PaymentReferencenumber"),
                        L("Paymentnotes"),
                        L("InvoiceNotes")
                        );
                    AddObjects(
                        sheet, paymentpaid,
                        _ => _.paymentPaid.ProjectNumber,
                        _ => _.paymentPaid.CustomerName,
                        _ => _.paymentPaid.PaidTillDate,
                        _ => _.paymentPaid.PaymentTotal,
                        _ => _.paymentPaid.TotalProjectCost,
                        _ => _.paymentPaid.PaymentMode,
                        _ => _.paymentPaid.PaymentRefferenceNo,
                        _ => _.paymentPaid.Notes,
                        _ => _.paymentPaid.PaymentAccountInvoiceNotes
                        );
                });
        }
    }
}
