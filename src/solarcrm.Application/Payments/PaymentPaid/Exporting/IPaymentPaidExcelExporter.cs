﻿using solarcrm.Dto;
using solarcrm.Payments.PaymentIssue.Dto;
using solarcrm.Payments.PaymentPaid.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentPaid.Exporting
{
    public interface IPaymentPaidExcelExporter
    {
        FileDto ExportToFile(List<GetPaymentPaidForViewDto> paymentPaid);
    }
}
