﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Timing.Timezone;
using MailKit.Net.Pop3;
using MailKit.Search;
using MailKit.Net.Imap;
using MailKit.Security;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using solarcrm.Authorization;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.DataVaults.Price;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.StockItem;
using solarcrm.DataVaults.Variation;
using solarcrm.EmailSetup.Dto;
using solarcrm.EntityFrameworkCore;
using solarcrm.Job;
using solarcrm.Payments.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using MailKit;
using solarcrm.DataVaults.PaymentType;
using solarcrm.DataVaults.PaymentMode;

namespace solarcrm.Payments
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Payment)]
    public class PaymentsAppService : solarcrmAppServiceBase, IPaymentAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;

        private readonly IRepository<PaymentDetailsJob> _paymentDetailjobRepository;
        private readonly IRepository<PaymentReceiptUploadJob> _paymentReceiptUploadjobRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<StockCategory> _stockCategoryRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<Job.JobVariation.JobVariation> _jobVariationRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<PaymentType> _paymenttypeRepository;
        private readonly IRepository<PaymentMode> _paymentmodeRepository;
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly IRepository<Prices> _priceRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public PaymentsAppService(
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<PaymentType> paymenttypeRepository,
            IRepository<PaymentMode> paymentmodeRepository,
        IRepository<Job.Jobs> jobRepository,
            IRepository<PaymentDetailsJob> paymentDetailjobRepository,
            IRepository<PaymentReceiptUploadJob> paymentReceiptUploadjobRepository,
        IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<StockItem> stockItemRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<StockCategory> stockCategoryRepository,
            IRepository<Variation> variationRepository,
            IRepository<Job.JobVariation.JobVariation> jobVariationRepository,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus> leadStatusRepository,
            IRepository<Prices> priceRepository,
            IRepository<DataVaulteActivityLog> dataVaultsActivityLogRepository,
        ITimeZoneConverter timeZoneConverter
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _paymenttypeRepository = paymenttypeRepository;
            _paymentmodeRepository = paymentmodeRepository;
            _paymentDetailjobRepository = paymentDetailjobRepository;
            _paymentReceiptUploadjobRepository = paymentReceiptUploadjobRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _stockItemRepository = stockItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _stockCategoryRepository = stockCategoryRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _userRepository = userRepository;
            _leadStatusRepository = leadStatusRepository;
            _priceRepository = priceRepository;
            _dataVaultsActivityLogRepository = dataVaultsActivityLogRepository;
            _timeZoneConverter = timeZoneConverter;
        }
       // [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Payment_Edit)]
        public async Task<GetPaymentForEditOutput> GetPaymentForEdit(EntityDto input)
        {
            //var leads = _leadRepository.GetAll().Include(x => x.SolarTypeIdFk).Where(x => x.Id == input.Id).FirstOrDefault(); //await _leadRepository.FirstOrDefaultAsync(input.Id);
            var job = await _jobRepository.FirstOrDefaultAsync(e => e.Id == input.Id);
            var payment = await _paymentDetailjobRepository.GetAll().Include(x => x.JobsIdFK).Where(x => x.PaymentJobId == input.Id).FirstOrDefaultAsync();
            decimal? lesssubsidy = (job.NetSystemCost - job.TotalJobCost) - job.DepositeAmount;
            decimal? netamounts = job.NetSystemCost - lesssubsidy;
            decimal? paidtilldate = 0;
            List<CreateOrEditPaymentReceiptItemsDto> PaymentReceiptList = new List<CreateOrEditPaymentReceiptItemsDto>();

            if (payment != null)
            {
                var paymentverified = await _paymentReceiptUploadjobRepository.GetAll().Where(x => x.PaymentId == payment.Id && x.IsPaymentRecVerified).ToListAsync();
                lesssubsidy = (payment.JobsIdFK.NetSystemCost - payment.JobsIdFK.TotalJobCost) - payment.JobsIdFK.DepositeAmount;
                netamounts = payment.JobsIdFK.NetSystemCost - lesssubsidy;
                paidtilldate = paymentverified.Sum(x => Convert.ToDecimal(x.PaymentRecTotalPaid));
                PaymentReceiptList = ObjectMapper.Map<List<CreateOrEditPaymentReceiptItemsDto>>(await _paymentReceiptUploadjobRepository.GetAll()
                    .Where(x => x.PaymentId == payment.Id).ToListAsync());
            }
            if (PaymentReceiptList.Count != 0)
            {
                //PaymentReceiptList.Add(new CreateOrEditPaymentReceiptItemsDto());
                foreach (var item in PaymentReceiptList)
                {   
                    item.PaymentRecType = await _paymenttypeRepository.GetAll().Where(x => x.Id == item.PaymentRecTypeId).Select(x => x.Name).FirstOrDefaultAsync();
                    item.PaymentRecMode = await _paymentmodeRepository.GetAll().Where(x => x.Id == item.PaymentRecModeId).Select(x => x.Name).FirstOrDefaultAsync();
                    item.PaymentRecAddedByName = await _userRepository.GetAll().Where(x=>x.Id == item.PaymentRecAddedById).Select(x=>x.UserName).FirstOrDefaultAsync();
                    item.PaymentVerifiedByName = await _userRepository.GetAll().Where(x => x.Id == item.PaymentRecVerifiedById).Select(x => x.UserName).FirstOrDefaultAsync();
                }
            }

            var output = new GetPaymentForEditOutput
            {
                //payment = ObjectMapper.Map<CreateOrEditPaymentDto>(payment),
                Id = payment == null ? 0 : payment.Id,
                JobNumber = job.JobNumber,
                TotalSystemCost = job.NetSystemCost,
                LessSubsidy = lesssubsidy, //(job.NetSystemCost - job.TotalJobCost) - job.DepositeAmount,                
                NetAmount = netamounts, //job.NetSystemCost - lesssubsidy,
                PaidTillDate = paidtilldate, //paymentverified.Sum(x => Convert.ToDecimal(x.PaymentTotalPaid)),
                BalanceOwing = netamounts - paidtilldate,
                PaymentAccountInvoiceNotes = payment == null ? "" : payment.PaymentAccountInvoiceNotes,
                PaymentDeliveryOption = payment == null ? null : payment.PaymentDeliveryOption,
                PaymentReceiptItems = PaymentReceiptList,
                
            };
            return output;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Payment)]
        public async Task CreateOrEdit(CreateOrEditPaymentDto input, int SectionId)
        {
            if (input.Id == null)
            {
                await Create(input, SectionId);
            }
            else
            {
                // await Update(input, SectionId);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Payment_Create, AppPermissions.Pages_Tenant_Jobs_Payment_Edit)]
        protected virtual async Task Create(CreateOrEditPaymentDto input, int SectionId)
        {
            var job = await _jobRepository.FirstOrDefaultAsync(input.PaymentJobId);
            var payment = ObjectMapper.Map<PaymentDetailsJob>(input);



            var PaymentId = await _paymentDetailjobRepository.InsertAndGetIdAsync(payment);
            foreach (var item in input.PaymentReceiptItems)
            {
                try
                {
                    PaymentReceiptUploadJob paymentReceipt = new PaymentReceiptUploadJob();
                    paymentReceipt.PaymentId = PaymentId;
                    paymentReceipt.PaymentRecEnterDate = item.PaymentRecEnterDate;
                    paymentReceipt.PaymentRecTotalPaid = item.PaymentRecTotalPaid;
                    paymentReceipt.PaymentRecTypeId = item.PaymentRecTypeId;
                    paymentReceipt.PaymentRecModeId = item.PaymentRecModeId;
                    paymentReceipt.PaymentReceiptName = item.PaymentReceiptName;
                    paymentReceipt.PaymentReceiptPath = item.PaymentReceiptPath;
                    paymentReceipt.PaymentRecAddedById = item.PaymentRecAddedById;
                    paymentReceipt.IsPaymentRecVerified = false;
                   
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }
            //Add Activity Log
            LeadActivityLogs leadactivity = new LeadActivityLogs();
            leadactivity.LeadActionId = 10;
            //leadactivity.LeadAcitivityID = 0; // confusion
            leadactivity.SectionId = SectionId;
            leadactivity.LeadActionNote = "Payment Created";
            leadactivity.LeadId = job.LeadId;
            leadactivity.OrganizationUnitId = job.OrganizationUnitId;
            if (AbpSession.TenantId != null)
            {
                leadactivity.TenantId = (int)AbpSession.TenantId;
            }
            await _leadactivityRepository.InsertAsync(leadactivity);
        }
        //[AbpAuthorize(AppPermissions.Pages_Tenant_jobs_PaymentReceipt_Delete)]
        public async Task DeletePaymentReceipt(EntityDto input)
        {
            await _paymentReceiptUploadjobRepository.DeleteAsync(input.Id);

            //Add Activity Log
            DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
            if (AbpSession.TenantId != null)
            {
                dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
            }
            dataVaulteActivityLog.SectionId = 50; // DocumentList Pages Name
            dataVaulteActivityLog.ActionId = 3; // Deleted
            dataVaulteActivityLog.ActionNote = "Payment Receipt Deleted";
            dataVaulteActivityLog.SectionValueId = input.Id;
            await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
        }

      
    }
}
