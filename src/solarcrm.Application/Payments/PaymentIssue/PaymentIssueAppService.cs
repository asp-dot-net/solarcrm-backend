﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Newtonsoft.Json;
using solarcrm.Authorization;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.StockItem;
using solarcrm.EntityFrameworkCore;
using solarcrm.Organizations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using solarcrm.Job;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.Variation;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.DataVaults.LeadStatus;
using Abp.Organizations;
using solarcrm.DataVaults;
using Abp.Timing.Timezone;
using solarcrm.Payments.PaymentIssue.Dto;
using solarcrm.DataVaults.Price;
using solarcrm.Payments.Dto;
using solarcrm.Dto;
using solarcrm.Payments.PaymentPaid.Dto;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using solarcrm.Payments.PaymentIssue.Exporting;

namespace solarcrm.Payments.PaymentIssue
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued)]
    public class PaymentIssueAppService : solarcrmAppServiceBase, IPaymentIssueAppService
    {
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;
        private readonly IRepository<Leads> _leadRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<OrganizationUnit, long> _OrganizationRepository;
        private readonly IRepository<BankDetailsOrganization> _bankOrganizationDetailsRepository;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailjobRepository;
        private readonly IRepository<PaymentReceiptUploadJob> _paymentReceiptUploadjobRepository;
        private readonly IRepository<ReminderLeadActivityLog> _reminderLeadActivityLog;
        private readonly IRepository<LeadtrackerHistory> _leadHistoryLogRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<CommentLeadActivityLog> _commentLeadActivityLog;
        private readonly IRepository<StockItem> _stockItemRepository;
        private readonly IRepository<JobProductItem> _jobProductItemRepository;
        private readonly IRepository<StockCategory> _stockCategoryRepository;
        private readonly IRepository<Variation> _variationRepository;
        private readonly IRepository<Job.JobVariation.JobVariation> _jobVariationRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<LeadStatus> _leadStatusRepository;
        private readonly IRepository<Prices> _priceRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<SSPaymentDetails> _ssPaymentDetailsRepository;
        private readonly IRepository<STCPaymentDetails> _stcPaymentDetailsRepository;
        private readonly IRepository<ApplicationStatus> _applicationstatusRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IPaymentIssueExcelExporter _PaymentIssueExcelExporter;
        private readonly ITimeZoneConverter _timeZoneConverter;
        public PaymentIssueAppService(
            IRepository<DataVaulteActivityLog> dataVaulteActivityLogRepository,
            IDbContextProvider<solarcrmDbContext> dbContextProvider,
            IRepository<Leads> leadRepository,
            IRepository<Job.Jobs> jobRepository,
            IRepository<PaymentDetailsJob> paymentDetailjobRepository,
            IRepository<PaymentReceiptUploadJob> paymentReceiptUploadjobRepository,
            IRepository<ReminderLeadActivityLog> reminderLeadActivityLog,
            IRepository<LeadActivityLogs> leadactivityRepository,
            IRepository<LeadtrackerHistory> leadHistoryLogRepository,
            IRepository<CommentLeadActivityLog> commentLeadActivityLog,
            IRepository<StockItem> stockItemRepository,
            IRepository<JobProductItem> jobProductItemRepository,
            IRepository<StockCategory> stockCategoryRepository,
            IRepository<Variation> variationRepository,
            IRepository<Job.JobVariation.JobVariation> jobVariationRepository,
            IRepository<User, long> userRepository,
            IRepository<LeadStatus> leadStatusRepository,
            IRepository<Prices> priceRepository,
            IRepository<OrganizationUnit, long> OrganizationRepository,
            IRepository<BankDetailsOrganization> bankOrganizationDetailsRepository,
            IRepository<SSPaymentDetails> ssPaymentDetailsRepository,
            IRepository<STCPaymentDetails> stcPaymentDetailsRepository,
            IRepository<ApplicationStatus> applicationstatusRepository,
            UserManager userManager,
            IRepository<DataVaulteActivityLog> dataVaultsActivityLogRepository,
            IPaymentIssueExcelExporter PaymentIssueExcelExporter,
            ITimeZoneConverter timeZoneConverter
        )
        {
            _dbContextProvider = dbContextProvider;
            _leadRepository = leadRepository;
            _jobRepository = jobRepository;
            _paymentDetailjobRepository = paymentDetailjobRepository;
            _paymentReceiptUploadjobRepository = paymentReceiptUploadjobRepository;
            _reminderLeadActivityLog = reminderLeadActivityLog;
            _leadactivityRepository = leadactivityRepository;
            _leadHistoryLogRepository = leadHistoryLogRepository;
            _commentLeadActivityLog = commentLeadActivityLog;
            _stockItemRepository = stockItemRepository;
            _jobProductItemRepository = jobProductItemRepository;
            _stockCategoryRepository = stockCategoryRepository;
            _variationRepository = variationRepository;
            _jobVariationRepository = jobVariationRepository;
            _userRepository = userRepository;
            _leadStatusRepository = leadStatusRepository;
            _priceRepository = priceRepository;
            _userManager = userManager;
            _OrganizationRepository = OrganizationRepository;
            _bankOrganizationDetailsRepository = bankOrganizationDetailsRepository;
            _ssPaymentDetailsRepository = ssPaymentDetailsRepository;
            _stcPaymentDetailsRepository = stcPaymentDetailsRepository;
            _applicationstatusRepository = applicationstatusRepository;
            _dataVaultsActivityLogRepository = dataVaultsActivityLogRepository;
            _PaymentIssueExcelExporter = PaymentIssueExcelExporter;
            _timeZoneConverter = timeZoneConverter;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued)]
        public async Task<PagedResultDto<GetPaymentIssueForViewDto>> GetAllPaymentIssue(GetAllPaymentIssueInput input)
        {
            var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
            var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
            List<int> FollowupList = new List<int>();
            List<int> NextFollowupList = new List<int>();
            List<int> Assign = new List<int>();
            List<int> jobnumberlist = new List<int>();
            List<int> sspaymentlist = new List<int>();
            List<int> stcpaymentlist = new List<int>();
            List<int> paymenttypelist = new List<int>();
            List<int> paymentModelist = new List<int>();

            var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
            var User_List = _userRepository.GetAll();
            var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
            IList<string> role = await _userManager.GetRolesAsync(User);

            var job_list = _jobRepository.GetAll();
            //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

            if (input.Filter != null)
            {
                jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
            }
            if (input.BillTypeFilter == "ss")
            {
                sspaymentlist = _ssPaymentDetailsRepository.GetAll().Select(x => x.SSPaymentId).ToList();
            }
            if (input.BillTypeFilter == "stc")
            {
                stcpaymentlist = _stcPaymentDetailsRepository.GetAll().Select(x => x.STCPaymentId).ToList();
            }
            if (input.PaymentTypeIdFilter != null)
            {
                paymenttypelist = _paymentReceiptUploadjobRepository.GetAll().Where(x => x.PaymentRecTypeId != 0).Select(x => x.PaymentId).ToList();
            }
            if (input.PaymentModeIdFilter != null)
            {
                paymentModelist = _paymentReceiptUploadjobRepository.GetAll().Where(x => x.PaymentRecModeId != 0).Select(x => x.PaymentId).ToList();
            }
            //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
            //{
            //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
            //}
            if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
            {
                NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
            }
            var filteredpaymentIssue = _paymentDetailjobRepository.GetAll().Include(e => e.JobsIdFK).Include(e => e.JobsIdFK.ApplicationStatusIdFK)
                                        .Include(e => e.JobsIdFK.LeadFK).Include(e => e.JobsIdFK.LeadFK.SolarTypeIdFk).Include(e=>e.JobsIdFK)
                        //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobsIdFK.JobNumber.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobsIdFK.JobNumber.Contains(input.Filter) || e.JobsIdFK.LeadFK.CustomerName.Contains(input.Filter) 
                            || e.JobsIdFK.LeadFK.EmailId.Contains(input.Filter) || e.JobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter) 
                            || e.JobsIdFK.LeadFK.MobileNumber.Contains(input.Filter) 
                            //|| e.JobsIdFK.LeadFK.AddressLine1.Contains(input.Filter) || e.JobsIdFK.LeadFK.AddressLine2.Contains(input.Filter) 
                            || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.JobsIdFK.LeadId)))
                        .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.JobsIdFK.LeadFK.LeadStatusId))
                        .WhereIf(input.LeadApplicationStatusFilter != null && input.LeadApplicationStatusFilter.Count() > 0, e => input.LeadApplicationStatusFilter.Contains((int)e.JobsIdFK.ApplicationStatusId))
                        .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.JobsIdFK.LeadFK.DiscomId))
                        .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.JobsIdFK.LeadFK.CircleId))
                        .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.JobsIdFK.LeadFK.DivisionId))
                        .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.JobsIdFK.LeadFK.SubDivisionId))
                        .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.JobsIdFK.LeadFK.SolarTypeId))
                        .WhereIf(input.EmployeeIdFilter != null && input.EmployeeIdFilter.Count() > 0, e => input.EmployeeIdFilter.Contains((int)e.JobsIdFK.LeadFK.ChanelPartnerID))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "stc", e => stcpaymentlist.Contains(e.Id))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "ss", e => sspaymentlist.Contains(e.Id))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "both", e => stcpaymentlist.Contains(e.Id) && sspaymentlist.Contains(e.Id))
                        .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "none", e => !stcpaymentlist.Contains(e.Id) && !sspaymentlist.Contains(e.Id))
                        .WhereIf(input.PaymentModeIdFilter != null && input.PaymentModeIdFilter.Count() > 0 , e=> paymentModelist.Contains(e.Id)) //paymentMode
                        .WhereIf(input.PaymentTypeIdFilter != null && input.PaymentTypeIdFilter.Count() > 0, e => paymenttypelist.Contains(e.Id)) //paymentType                                                                                                                                                                                                
                        .WhereIf(input.OwingAmountFromFilter != 0 && input.OwingAmountToFilter != 0, e => e.BalanceOwing == input.OwingAmountFromFilter || e.BalanceOwing == input.OwingAmountToFilter) //Awaiting                                                                                                                                                              
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "depositereceived", e => e.JobsIdFK.DepositRecivedDate != null)
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "billingdate", e => NextFollowupList.Contains(e.Id))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "installationcompleted", e => NextFollowupList.Contains(e.Id))
                        .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "lastpayment", e => NextFollowupList.Contains(e.Id))
                        .Where(e => e.JobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);

            var pagedAndFilteredJob = filteredpaymentIssue.OrderBy(input.Sorting ?? "id desc").PageBy(input);

            var paymentIssues = from o in pagedAndFilteredJob

                                    //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                                    //from s4 in j4.DefaultIfEmpty()

                                select new GetPaymentIssueForViewDto()
                                {
                                    paymentIssue = new PaymentIssueDto
                                    {
                                        Id = o.Id,
                                        leaddata = ObjectMapper.Map<LeadsDto>(o.JobsIdFK.LeadFK),
                                        JobId = o.JobsIdFK.Id,
                                        ProjectNumber = o.JobsIdFK.JobNumber,
                                        ////JobStatus = o.JobStatusFk.Name,
                                        CustomerName = o.JobsIdFK.LeadFK.CustomerName,
                                        Address = (o.JobsIdFK.LeadFK.AddressLine1 == null ? "" : o.JobsIdFK.LeadFK.AddressLine1 + ", ") + (o.JobsIdFK.LeadFK.AddressLine2 == null ? "" : o.JobsIdFK.LeadFK.AddressLine2 + ", ") + (o.JobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.JobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.JobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.JobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.JobsIdFK.LeadFK.Pincode == null ? "" : o.JobsIdFK.LeadFK.Pincode),
                                        Mobile = o.JobsIdFK.LeadFK.MobileNumber,
                                        Email = o.JobsIdFK.LeadFK.EmailId,
                                        SolarType = o.JobsIdFK.LeadFK.SolarTypeIdFk.Name,
                                        CpName = _userRepository.GetAll().Where(e => e.Id == o.JobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                        CpMobile = _userRepository.GetAll().Where(e => e.Id == o.JobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.PhoneNumber).FirstOrDefault(),
                                        //SubDivision = o.LeadFK.SubDivisionIdFk.Name,
                                        //JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                        LeadStatusName = o.JobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                        FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.JobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                        FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                        Notes = o.JobsIdFK.LeadFK.Notes,
                                        LeadStatusColorClass = o.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                        LeadStatusIconClass = o.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                        Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                        TotalProjectCost = o.JobsIdFK.TotalJobCost,
                                        BalanceOwing = o.BalanceOwing,
                                        LastPaymentDate = _paymentReceiptUploadjobRepository.GetAll().Where(x => x.PaymentId == o.Id).OrderByDescending(e => e.PaymentId == o.Id).FirstOrDefault().CreationTime,
                                        AdvancePayment = 0,
                                        Is_STC_payment = _stcPaymentDetailsRepository.GetAll().Where(x => x.STCPaymentId == o.Id).FirstOrDefault() == null ? false : true,
                                        Is_SS_payment = _ssPaymentDetailsRepository.GetAll().Where(x => x.SSPaymentId == o.Id).FirstOrDefault() == null ? false : true,
                                        PaymentDeliveryOption = o.PaymentDeliveryOption,
                                        ApplicationStatusdata = o.JobsIdFK.ApplicationStatusIdFK, //_applicationstatusRepository.GetAll().Where(x => x.Id == o.JobsIdFK.ApplicationStatusId).FirstOrDefault(),
                                        EastimatePaidDate = (DateTime?)o.JobsIdFK.EastimatePayDate,
                                        EastimatePaidStatus = o.JobsIdFK.EastimatePaidStatus,
                                        PaymentRefferenceNo = o.Id
                                    },

                                    paymentIssueSummaryCount = PaymentPaidSummaryCount(filteredpaymentIssue.ToList())
                                    //ReminderTime = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.LeadId).OrderByDescending(e => e.LeadId == o.Id && e.LeadActionId == 8).FirstOrDefault().CreationTime,
                                    ////(DateTime?)_reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).FirstOrDefault().CreationTime,
                                    //ActivityDescription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                    //ActivityComment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.Id).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                    //ApplicationStatusName = _applicationstatusRepository.GetAll().Where(x => x.Id == s6.ApplicationStatusId).FirstOrDefault(), // the add the goverment status


                                };

            var totalCount = await filteredpaymentIssue.CountAsync();

            return new PagedResultDto<GetPaymentIssueForViewDto>(totalCount, await paymentIssues.ToListAsync());
        }
        public PaymentIssueSummaryCount PaymentPaidSummaryCount(List<PaymentDetailsJob> filteredLeads)
        {
            var output = new PaymentIssueSummaryCount();
            if (filteredLeads.Count > 0)
            {
                var paymentid = filteredLeads.Select(x=>x.Id);
                var panelCount = _jobProductItemRepository.GetAll().Include(x => x.ProductItemFk.StockCategoryFk).Where(x => x.ProductItemFk.StockCategoryFk.Id == 1).Sum(x => x.Quantity);
                var receivetotal = _paymentReceiptUploadjobRepository.GetAll().Where(x => x.IsPaymentRecVerified && paymentid.Contains(x.PaymentId)).Sum(x=>x.PaymentRecTotalPaid);
                output.Total = Convert.ToString(filteredLeads.Sum(x => x.NetAmount));
                output.TotalOwingAmount = Convert.ToString(filteredLeads.Sum(x => x.BalanceOwing));
                output.TotalPanel = Convert.ToString(panelCount);
                output.TotalKilowatt = Convert.ToString(filteredLeads.Sum(x => x.JobsIdFK.PvCapacityKw));
                output.ReceivedAmount = Convert.ToString(receivetotal);
                output.EastimatePaid = Convert.ToString(filteredLeads.Where(x => x.JobsIdFK.EastimatePaidStatus == "Paid").Count());
                output.FeasiblityApproved = Convert.ToString(filteredLeads.Where(x => x.JobsIdFK.ApplicationStatusId == 4).Count());
            }
            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_QuickView)]
        public async Task<PaymentIssueDto> GetPaymentIssueQuickView(EntityDto input)
        {
            try
            {
                //var leads = _leadRepository.FirstOrDefault(input.Id);
                var job = await _jobRepository.GetAll().Include(e => e.LeadFK).
                    Include(e => e.LeadFK.DivisionIdFk).
                    Include(e => e.LeadFK.SubDivisionIdFk).
                    Include(e => e.LeadFK.DiscomIdFk).
                    Include(e => e.LeadFK.CityIdFk).
                    Include(e => e.LeadFK.TalukaIdFk).
                    Include(e => e.LeadFK.DistrictIdFk).
                    Include(e => e.LeadFK.StateIdFk).
                    Where(e => e.Id == input.Id).FirstOrDefaultAsync();

                var output = new PaymentIssueDto
                {
                    leaddata = ObjectMapper.Map<LeadsDto>(job.LeadFK),
                    EastimatePaidDate = (DateTime?)job.EastimatePayDate,
                    EastimatePaidStatus = job.EastimatePaidStatus,
                    //Job = ObjectMapper.Map<CreateOrEditJobDto>(job),
                    //paymentIssue.le
                    //ProjectNumber = job.JobNumber,
                    //Latitude = (decimal?)job.LeadFK.Latitude,
                    //Longitude = (decimal?)job.LeadFK.Longitude,
                    //AvgmonthlyBill = (int?)job.LeadFK.AvgmonthlyBill,
                    //AvgmonthlyUnit = (int?)job.LeadFK.AvgmonthlyUnit,
                    //CustomerName = job.LeadFK.CustomerName,
                    //ConsumerNumber = job.LeadFK.ConsumerNumber,
                    //SubDivisionName = job.LeadFK.SubDivisionIdFk.Name, //leads.SubDivisionId != null ? _subDivisionRepository.FirstOrDefaultAsync((int)leads.SubDivisionId).Result.Name : "",
                    //DivisionName = job.LeadFK.DivisionIdFk.Name, //leads.DivisionId != null ? _divisionRepository.FirstOrDefaultAsync((int)leads.DivisionId).Result.Name : "",
                    //DiscomName = job.LeadFK.DiscomIdFk.Name, //leads.DiscomId != null ? _discomRepository.FirstOrDefaultAsync((int)leads.DiscomId).Result.Name : "",

                };
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_SSBillPaymentView)]
        public async Task<CreateOrEditSSPaymentDto> GetSSPaymentForView(EntityDto input)
        {
            var SSData = await _ssPaymentDetailsRepository.GetAll().Where(x => x.SSPaymentId == input.Id).FirstOrDefaultAsync(); //await _leadRepository.FirstOrDefaultAsync(input.Id);
            if (SSData == null)
            {
                SSData = new SSPaymentDetails();
            }
            var output = new CreateOrEditSSPaymentDto
            {
                SSPaymentId = SSData.SSPaymentId,
                SSDate = SSData.SSDate,
                SSGSTNo = SSData.SSGSTNo,
                SSPaymentApprovedById = SSData.SSPaymentApprovedById,
                SSNotes = SSData.SSNotes
            };
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_SSBillAddorEdit)]
        public async Task SSPaymentForAddOrEdit(CreateOrEditSSPaymentDto input, int SectionId)
        {
            var SSpaymentData = await _ssPaymentDetailsRepository.GetAll().Include(e => e.PaymentIdFK).Include(e => e.PaymentIdFK.JobsIdFK.LeadFK).Where(e => e.SSPaymentId == input.SSPaymentId).FirstOrDefaultAsync();
            //if (SSpaymentData != null)
            {
                if (SSpaymentData == null)
                {
                    SSpaymentData = new SSPaymentDetails();
                }
                SSpaymentData.SSPaymentId = input.SSPaymentId;
                SSpaymentData.SSGSTNo = input.SSGSTNo;
                SSpaymentData.SSPaymentApprovedById = input.SSPaymentApprovedById;
                SSpaymentData.SSNotes = input.SSNotes;
                SSpaymentData.SSDate = input.SSDate;
                SSpaymentData.SSNotes = Convert.ToString(input.SSNotes);
                var ssPaymentId = await _ssPaymentDetailsRepository.InsertOrUpdateAndGetIdAsync(SSpaymentData);

                if (ssPaymentId != 0)
                {
                    var ssResult = await _ssPaymentDetailsRepository.GetAll().Include(e => e.PaymentIdFK).Include(e => e.PaymentIdFK.JobsIdFK.LeadFK)
                        .Include(e => e.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk).Where(e => e.SSPaymentId == input.SSPaymentId).FirstOrDefaultAsync();

                    //Lead status Update
                    var LeadData = ssResult.PaymentIdFK.JobsIdFK.LeadFK;
                    var leadstatusoldvalue = ssResult.PaymentIdFK.JobsIdFK.LeadFK.LeadStatusIdFk.Status;
                    LeadData.LeadStatusId = 6; // Ready to Dispatch
                    await _leadRepository.UpdateAsync(LeadData);

                    //add the leadtrackerhistory
                    if (LeadData != null)
                    {
                        LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                        objAuditInfo.FieldName = "LeadStatus";
                        objAuditInfo.PrevValue = Convert.ToString(leadstatusoldvalue);
                        objAuditInfo.CurValue = "Ready to Dispatch";//Convert.ToString(LeadData.LeadStatusId);
                        //objAuditInfo.Action = "Edit";
                        objAuditInfo.Action = SSpaymentData.Id == 0 ? "SS Bill Payment Details Created" : "SS Bill Payment Details Modified";
                        objAuditInfo.LeadId = SSpaymentData.PaymentIdFK.JobsIdFK.LeadFK.Id;  //SSpaymentData.PaymentIdFK.JobsIdFK.LeadId;
                        objAuditInfo.LeadActionId = 36;
                        objAuditInfo.TenantId = (int)AbpSession.TenantId;
                        await _leadHistoryLogRepository.InsertOrUpdateAsync(objAuditInfo);
                    }
                    #region Activity Log
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 11;
                    leadactivity.SectionId = SectionId;
                    leadactivity.LeadActionNote = SSpaymentData.Id == 0 ? "SS Bill Payment Details Created" : "SS Bill Payment Details Modified";
                    leadactivity.LeadId = input.LeadId; //SSpaymentData.PaymentIdFK.JobsIdFK.LeadId;// job.LeadId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    var leadactid = await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    if (input.Id != null)
                    {
                        var List = new List<LeadtrackerHistory>();
                        try
                        {
                            List<SSPaymentDetails> objAuditOld_OnlineUserMst = new List<SSPaymentDetails>();
                            List<CreateOrEditSSPaymentDto> objAuditNew_OnlineUserMst = new List<CreateOrEditSSPaymentDto>();

                            DataTable Dt_Target = new DataTable();
                            DataTable Dt_Source = new DataTable();

                            objAuditOld_OnlineUserMst.Add(SSpaymentData);
                            objAuditNew_OnlineUserMst.Add(input);

                            //Add Activity Log
                            LeadtrackerHistory leadActivityLog = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                leadActivityLog.TenantId = (int)AbpSession.TenantId;
                            }

                            object newvalue = objAuditNew_OnlineUserMst;
                            Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                            if (objAuditOld_OnlineUserMst != null)
                            {
                                Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                            }
                            foreach (DataRow dr_S in Dt_Source.Rows)
                            {
                                foreach (DataColumn dc_S in Dt_Source.Columns)
                                {

                                    if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                                    {
                                        LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                                        objAuditInfo.FieldName = dc_S.ColumnName;
                                        objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                                        objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();
                                        //objAuditInfo.Action = "Edit";
                                        objAuditInfo.Action = Dt_Target.Rows[0][dc_S.ColumnName].ToString() == "" ? "Add" : "Edit";
                                        objAuditInfo.LeadId = input.LeadId;  //SSpaymentData.PaymentIdFK.JobsIdFK.LeadId;
                                        objAuditInfo.LeadActionId = leadactid;
                                        objAuditInfo.TenantId = (int)AbpSession.TenantId;
                                        List.Add(objAuditInfo);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                    }
                    #endregion
                }

            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_STCBillPaymentView)]
        public async Task<CreateOrEditSTCPaymentDto> GetSTCPaymentForView(EntityDto input)
        {
            var STCData = await _stcPaymentDetailsRepository.GetAll().Where(x => x.STCPaymentId == input.Id).FirstOrDefaultAsync(); //await _leadRepository.FirstOrDefaultAsync(input.Id);
            if (STCData == null)
            {
                STCData = new STCPaymentDetails();
            }
            //var output = CreateOrEditSTCPaymentDto // ObjectMapper.Map<CreateOrEditSTCPaymentDto>(STCData);
            var output = new CreateOrEditSTCPaymentDto
            {
                STCPaymentId = STCData.STCPaymentId,
                STCDate = STCData.STCDate,

                STCPaymentApprovedById = STCData.STCPaymentApprovedById,
                STCNotes = STCData.STCNotes
            };
            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_STCBillAddorEdit)]
        public async Task STCPaymentForAddOrEdit(CreateOrEditSTCPaymentDto input, int SectionId)
        {
            var STCpaymentData = await _stcPaymentDetailsRepository.GetAll().Include(e => e.PaymentIdFK).Where(e => e.STCPaymentId == input.STCPaymentId).FirstOrDefaultAsync();
            //if (SSpaymentData != null)
            {
                if (STCpaymentData == null)
                {
                    STCpaymentData = new STCPaymentDetails();
                }
                STCpaymentData.STCPaymentId = input.STCPaymentId;
                STCpaymentData.STCPaymentApprovedById = input.STCPaymentApprovedById;
                STCpaymentData.STCNotes = input.STCNotes;
                STCpaymentData.STCDate = input.STCDate;
                STCpaymentData.STCNotes = input.STCNotes.ToString();

                var STCpaymentId = await _stcPaymentDetailsRepository.InsertOrUpdateAndGetIdAsync(STCpaymentData);

                //Add Activity Log
                LeadActivityLogs leadactivity = new LeadActivityLogs();
                leadactivity.LeadActionId = 11;
                leadactivity.SectionId = SectionId;
                leadactivity.LeadActionNote = STCpaymentData.Id == 0 ? "STC Payment Details Created" : "STC Payment Details Modified";
                leadactivity.LeadId = input.LeadId;
                if (AbpSession.TenantId != null)
                {
                    leadactivity.TenantId = (int)AbpSession.TenantId;
                }
                var leadactid = await _leadactivityRepository.InsertOrUpdateAndGetIdAsync(leadactivity);


            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_DeliverPaymentAddorEdit)]
        public async Task PaymentDeliveryForAddOrEdit(CreateOrEditPaymentDto input, int SectionId)
        {
            var paymentDetails = await _paymentDetailjobRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync(); //ObjectMapper.Map<PaymentDetailsJob>(input);
            if (paymentDetails != null)
            {
                paymentDetails.PaymentDeliveryOption = input.PaymentDeliveryOption;
                var PaymentId = await _paymentDetailjobRepository.InsertOrUpdateAndGetIdAsync(paymentDetails);
                if (PaymentId != 0)
                {
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 10;
                    //leadactivity.LeadAcitivityID = 0; // confusion
                    leadactivity.SectionId = SectionId;
                    leadactivity.LeadActionNote = "PaymentDeliveryOption Modified";
                    leadactivity.LeadId = input.leadId;
                    leadactivity.OrganizationUnitId = input.OrganizationUnitId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);

                    if (input.Id != null)
                    {
                        var List = new List<LeadtrackerHistory>();
                        try
                        {
                            List<PaymentDetailsJob> objAuditOld_OnlineUserMst = new List<PaymentDetailsJob>();
                            List<CreateOrEditPaymentDto> objAuditNew_OnlineUserMst = new List<CreateOrEditPaymentDto>();

                            DataTable Dt_Target = new DataTable();
                            DataTable Dt_Source = new DataTable();



                            objAuditOld_OnlineUserMst.Add(paymentDetails);
                            objAuditNew_OnlineUserMst.Add(input);

                            //Add Activity Log
                            LeadtrackerHistory leadActivityLog = new LeadtrackerHistory();
                            if (AbpSession.TenantId != null)
                            {
                                leadActivityLog.TenantId = (int)AbpSession.TenantId;
                            }

                            object newvalue = objAuditNew_OnlineUserMst;
                            Dt_Source = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(newvalue), (typeof(DataTable)));

                            if (objAuditOld_OnlineUserMst != null)
                            {
                                Dt_Target = (DataTable)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(objAuditOld_OnlineUserMst), (typeof(DataTable)));
                            }
                            //foreach (DataRow dr_S in Dt_Source.Rows)
                            //{
                            //    foreach (DataColumn dc_S in Dt_Source.Columns)
                            //    {
                            //        if (Dt_Target.Rows[0][dc_S.ColumnName].ToString() != dr_S[dc_S.ColumnName].ToString())
                            //        {
                            //            LeadtrackerHistory objAuditInfo = new LeadtrackerHistory();
                            //            objAuditInfo.FieldName = dc_S.ColumnName;
                            //            objAuditInfo.PrevValue = Dt_Target.Rows[0][dc_S.ColumnName].ToString();
                            //            objAuditInfo.CurValue = dr_S[dc_S.ColumnName].ToString();                                        
                            //            objAuditInfo.Action = Dt_Target.Rows[0][dc_S.ColumnName].ToString() == "" ? "Add" : "Edit";
                            //            objAuditInfo.LeadId = input.leadId;
                            //            objAuditInfo.LeadActionId = 2;
                            //            objAuditInfo.TenantId = (int)AbpSession.TenantId;
                            //            List.Add(objAuditInfo);
                            //        }
                            //    }
                            //}
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_RemoveBillSSorSTC)]
        public async Task STCorSSPaymentDelete(CreateOrEditPaymentDto input, int SectionId)
        {
            var sspaymentDetails = await _ssPaymentDetailsRepository.GetAll().Include(x => x.PaymentIdFK.JobsIdFK.LeadFK).Where(x => x.SSPaymentId == input.Id).FirstOrDefaultAsync(); //ObjectMapper.Map<PaymentDetailsJob>(input);
            var stcpaymentDetails = await _stcPaymentDetailsRepository.GetAll().Where(x => x.STCPaymentId == input.Id).FirstOrDefaultAsync();
            if (sspaymentDetails != null)
            {

                sspaymentDetails.IsDeleted = input.IsSSPaymentDetails;
                sspaymentDetails.DeleterUserId = (int)AbpSession.UserId;
                var PaymentId = await _ssPaymentDetailsRepository.InsertOrUpdateAndGetIdAsync(sspaymentDetails);

                var leadData = sspaymentDetails.PaymentIdFK.JobsIdFK.LeadFK;
                var Leadstatuschage = await _leadHistoryLogRepository.GetAll().Where(x => x.LeadId == leadData.Id && x.LeadActionId == 36).OrderByDescending(x => x.CreationTime).FirstOrDefaultAsync();
                if (Leadstatuschage != null)
                {

                    leadData.LeadStatusId = Convert.ToInt32(Leadstatuschage.PrevValue);
                    await _leadRepository.UpdateAsync(leadData);

                }

                if (PaymentId != 0 && input.IsSSPaymentDetails)
                {
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 10;
                    //leadactivity.LeadAcitivityID = 0; // confusion
                    leadactivity.SectionId = SectionId;
                    leadactivity.LeadActionNote = "SS payment Deleted";
                    leadactivity.LeadId = input.leadId;
                    leadactivity.OrganizationUnitId = input.OrganizationUnitId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);
                }
            }

            if (stcpaymentDetails != null && input.IsSTCPaymentDetails)
            {
                stcpaymentDetails.IsDeleted = input.IsSTCPaymentDetails;
                stcpaymentDetails.DeleterUserId = (int)AbpSession.UserId;
                var PaymentId = await _stcPaymentDetailsRepository.InsertOrUpdateAndGetIdAsync(stcpaymentDetails);
                if (PaymentId != 0)
                {
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 10;
                    //leadactivity.LeadAcitivityID = 0; // confusion
                    leadactivity.SectionId = SectionId;
                    leadactivity.LeadActionNote = "STC payment Deleted";
                    leadactivity.LeadId = input.leadId;
                    leadactivity.OrganizationUnitId = input.OrganizationUnitId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                }
            }
        }

        public async Task<BillPaymentDto> BillPaymentDetails(EntityDto input, int organizationId)
        {
            var job = await _jobRepository.GetAll().Include(x => x.LeadFK).Where(x => x.LeadId == input.Id).FirstOrDefaultAsync();
            var output = new BillPaymentDto();
            if (job != null)
            {
                var paymentDetails = await _paymentDetailjobRepository.GetAll().Where(x => x.PaymentJobId == job.Id).FirstOrDefaultAsync();
                decimal systemamount = Convert.ToDecimal(job.ActualSystemCost - paymentDetails.LessSubsidy);
                decimal SolarGridAmount = Convert.ToDecimal(((systemamount * 70 / 100) * 100) / 112);
                decimal SolarServiceAmount = Convert.ToDecimal(((systemamount * 30 / 100) * 100) / 118);
                output = new BillPaymentDto
                {
                    SolarGridAmount = SolarGridAmount,
                    SystemSize = job.PvCapacityKw,
                    TotalSystemPrice = job.ActualSystemCost,
                    Subsidy20 = systemamount,
                    Subsidy40 = systemamount,
                    TotalSubSidy = paymentDetails.LessSubsidy,
                    SystemSuplyCGST = (SolarGridAmount * 6) / 100,
                    SystemSuplySGST = (SolarGridAmount * 6) / 100,
                    SolarServiceAmount = SolarServiceAmount,
                    SolarServiceCGST = (SolarServiceAmount * 9) / 100,
                    SolarServiceSGST = (SolarServiceAmount * 9) / 100,
                    TotalAmount = systemamount,
                    leaddata = ObjectMapper.Map<LeadsDto>(job.LeadFK),
                    jobdata = ObjectMapper.Map<object>(job),
                    OrganizationDetails = ObjectMapper.Map<object>(_OrganizationRepository.GetAll().Where(x => x.Id == organizationId).ToList()),
                    OrganizationbankDetails = ObjectMapper.Map<List<object>>(_bankOrganizationDetailsRepository.GetAll().Where(x => x.BankDetailOrganizationId == 5).ToList())
                };
            }
            return output;
        }

        public async Task<FileDto> GetPaymentIssueToExcel(GetAllPaymentIssueInput input)
        {
            try
            {
                var SDate = (_timeZoneConverter.Convert(input.StartDate, (int)AbpSession.TenantId));
                var EDate = (_timeZoneConverter.Convert(input.EndDate, (int)AbpSession.TenantId));
                List<int> FollowupList = new List<int>();
                List<int> NextFollowupList = new List<int>();
                List<int> Assign = new List<int>();
                List<int> jobnumberlist = new List<int>();
                List<int> sspaymentlist = new List<int>();
                List<int> stcpaymentlist = new List<int>();
                List<int> paymenttypelist = new List<int>();
                List<int> paymentModelist = new List<int>();

                var User = _userRepository.GetAll().Where(e => e.Id == AbpSession.UserId).FirstOrDefault();
                var User_List = _userRepository.GetAll();
                var leadactivity_list = _leadactivityRepository.GetAll().Where(e => e.LeadIdFk.OrganizationUnitId == input.OrganizationUnit);
                IList<string> role = await _userManager.GetRolesAsync(User);

                var job_list = _jobRepository.GetAll();
                //var leads = await _leadRepository.GetAll().Where(x=>job_list.);

                if (input.Filter != null)
                {
                    jobnumberlist = job_list.Where(e => e.JobNumber.Contains(input.Filter)).Select(e => e.LeadId).ToList();
                }
                if (input.BillTypeFilter == "ss")
                {
                    sspaymentlist = _ssPaymentDetailsRepository.GetAll().Select(x => x.SSPaymentId).ToList();
                }
                if (input.BillTypeFilter == "stc")
                {
                    sspaymentlist = _stcPaymentDetailsRepository.GetAll().Select(x => x.STCPaymentId).ToList();
                }
                if (input.PaymentTypeIdFilter != null)
                {
                    paymenttypelist = _paymentReceiptUploadjobRepository.GetAll().Where(x => x.PaymentRecTypeId != 0).Select(x => x.PaymentId).ToList();
                }
                if (input.PaymentModeIdFilter != null)
                {
                    paymentModelist = _paymentReceiptUploadjobRepository.GetAll().Where(x => x.PaymentRecModeId != 0).Select(x => x.PaymentId).ToList();
                }
                //if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "Followup")
                //{
                //    FollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).Select(e => e.LeadId).ToList();
                //}
                if (input.StartDate != null && input.EndDate != null && input.FilterbyDate == "NextFollowUpdate")
                {
                    NextFollowupList = leadactivity_list.Where(e => e.CreationTime.Date >= SDate.Value.Date && e.CreationTime.Date <= EDate.Value.Date && e.LeadActionId == 8).OrderByDescending(e => e.Id).DistinctBy(e => e.LeadId).Select(e => e.LeadId).ToList();
                }
                var filteredpaymentIssue = _paymentDetailjobRepository.GetAll().Include(e => e.JobsIdFK).Include(e => e.JobsIdFK.ApplicationStatusIdFK)
                                            .Include(e => e.JobsIdFK.LeadFK).Include(e => e.JobsIdFK.LeadFK.SolarTypeIdFk).Include(e => e.JobsIdFK)
                            //.WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => e.JobsIdFK.JobNumber.Contains(input.Filter))
                            .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.JobsIdFK.JobNumber.Contains(input.Filter) || e.JobsIdFK.LeadFK.CustomerName.Contains(input.Filter)
                                || e.JobsIdFK.LeadFK.EmailId.Contains(input.Filter) || e.JobsIdFK.LeadFK.Alt_Phone.Contains(input.Filter)
                                || e.JobsIdFK.LeadFK.MobileNumber.Contains(input.Filter)
                                //|| e.JobsIdFK.LeadFK.AddressLine1.Contains(input.Filter) || e.JobsIdFK.LeadFK.AddressLine2.Contains(input.Filter) 
                                || (jobnumberlist.Count != 0 && jobnumberlist.Contains(e.JobsIdFK.LeadId)))
                            .WhereIf(input.LeadStatusIDS != null && input.LeadStatusIDS.Count() > 0, e => input.LeadStatusIDS.Contains(e.JobsIdFK.LeadFK.LeadStatusId))
                            .WhereIf(input.LeadApplicationStatusFilter != null && input.LeadApplicationStatusFilter.Count() > 0, e => input.LeadApplicationStatusFilter.Contains((int)e.JobsIdFK.ApplicationStatusId))
                            .WhereIf(input.DiscomIdFilter != null && input.DiscomIdFilter.Count() > 0, e => input.DiscomIdFilter.Contains((int)e.JobsIdFK.LeadFK.DiscomId))
                            .WhereIf(input.CircleIdFilter != null && input.CircleIdFilter.Count() > 0, e => input.CircleIdFilter.Contains((int)e.JobsIdFK.LeadFK.CircleId))
                            .WhereIf(input.DivisionIdFilter != null && input.DivisionIdFilter.Count() > 0, e => input.DivisionIdFilter.Contains((int)e.JobsIdFK.LeadFK.DivisionId))
                            .WhereIf(input.SubDivisionIdFilter != null && input.SubDivisionIdFilter.Count() > 0, e => input.SubDivisionIdFilter.Contains((int)e.JobsIdFK.LeadFK.SubDivisionId))
                            .WhereIf(input.SolarTypeIdFilter != null && input.SolarTypeIdFilter.Count() > 0, e => input.SolarTypeIdFilter.Contains((int)e.JobsIdFK.LeadFK.SolarTypeId))
                            .WhereIf(input.EmployeeIdFilter != null && input.EmployeeIdFilter.Count() > 0, e => input.EmployeeIdFilter.Contains((int)e.JobsIdFK.LeadFK.ChanelPartnerID))
                            .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "stc", e => stcpaymentlist.Contains(e.PaymentJobId))
                            .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "ss", e => sspaymentlist.Contains(e.PaymentJobId))
                            .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "both", e => stcpaymentlist.Contains(e.PaymentJobId) && sspaymentlist.Contains(e.PaymentJobId))
                            .WhereIf(input.BillTypeFilter != null && input.BillTypeFilter.ToLower() == "none", e => !stcpaymentlist.Contains(e.PaymentJobId) && !sspaymentlist.Contains(e.PaymentJobId))
                            .WhereIf(input.PaymentModeIdFilter != null && input.PaymentModeIdFilter.Count() > 0, e => paymentModelist.Contains(e.PaymentJobId)) //paymentMode
                            .WhereIf(input.PaymentTypeIdFilter != null && input.PaymentTypeIdFilter.Count() > 0, e => paymenttypelist.Contains(e.PaymentJobId)) //paymentType                                                                                                                                                                                                
                            .WhereIf(input.OwingAmountFromFilter != 0 && input.OwingAmountToFilter != 0, e => e.BalanceOwing == input.OwingAmountFromFilter || e.BalanceOwing == input.OwingAmountToFilter) //Awaiting                                                                                                                                                              
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "depositereceived", e => e.JobsIdFK.DepositRecivedDate != null)
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "billingdate", e => NextFollowupList.Contains(e.Id))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "installationcompleted", e => NextFollowupList.Contains(e.Id))
                            .WhereIf(input.FilterbyDate != null && input.FilterbyDate.ToLower() == "lastpayment", e => NextFollowupList.Contains(e.Id))
                            .Where(e => e.JobsIdFK.LeadFK.OrganizationUnitId == input.OrganizationUnit);

                var pagedAndFilteredJob = filteredpaymentIssue.OrderBy(input.Sorting ?? "id desc").PageBy(input);

                var paymentIssues = from o in pagedAndFilteredJob

                                        //join o4 in _organizationUnitRepository.GetAll() on o.OrganizationUnitId equals o4.Id into j4
                                        //from s4 in j4.DefaultIfEmpty()

                                    select new GetPaymentIssueForViewDto()
                                    {
                                        paymentIssue = new PaymentIssueDto
                                        {
                                            Id = o.Id,
                                            leaddata = ObjectMapper.Map<LeadsDto>(o.JobsIdFK.LeadFK),
                                            JobId = o.JobsIdFK.Id,
                                            ProjectNumber = o.JobsIdFK.JobNumber,
                                            ////JobStatus = o.JobStatusFk.Name,
                                            CustomerName = o.JobsIdFK.LeadFK.CustomerName,
                                            Address = (o.JobsIdFK.LeadFK.AddressLine1 == null ? "" : o.JobsIdFK.LeadFK.AddressLine1 + ", ") + (o.JobsIdFK.LeadFK.AddressLine2 == null ? "" : o.JobsIdFK.LeadFK.AddressLine2 + ", ") + (o.JobsIdFK.LeadFK.CityIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.CityIdFk.Name + ", ") + (o.JobsIdFK.LeadFK.TalukaIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.TalukaIdFk.Name + ", ") + (o.JobsIdFK.LeadFK.DiscomIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.DiscomIdFk.Name + ", ") + (o.JobsIdFK.LeadFK.StateIdFk.Name == null ? "" : o.JobsIdFK.LeadFK.StateIdFk.Name + "-") + (o.JobsIdFK.LeadFK.Pincode == null ? "" : o.JobsIdFK.LeadFK.Pincode),
                                            Mobile = o.JobsIdFK.LeadFK.MobileNumber,
                                            Email = o.JobsIdFK.LeadFK.EmailId,
                                            SolarType = o.JobsIdFK.LeadFK.SolarTypeIdFk.Name,
                                            CpName = _userRepository.GetAll().Where(e => e.Id == o.JobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                            CpMobile = _userRepository.GetAll().Where(e => e.Id == o.JobsIdFK.LeadFK.ChanelPartnerID).Select(e => e.PhoneNumber).FirstOrDefault(),
                                            //SubDivision = o.LeadFK.SubDivisionIdFk.Name,
                                            //JobOwnedBy = _userRepository.GetAll().Where(e => e.Id == o.LeadFK.ChanelPartnerID).Select(e => e.FullName).FirstOrDefault(),
                                            LeadStatusName = o.JobsIdFK.LeadFK.LeadStatusIdFk.Status,
                                            FollowDate = (DateTime?)leadactivity_list.Where(e => e.LeadId == o.JobsIdFK.LeadId && e.LeadActionId == 8).OrderByDescending(e => e.LeadId == o.Id).FirstOrDefault().CreationTime,
                                            FollowDiscription = _reminderLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.ReminderActivityNote).FirstOrDefault(),
                                            Notes = o.JobsIdFK.LeadFK.Notes,
                                            LeadStatusColorClass = o.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusColorClass,
                                            LeadStatusIconClass = o.JobsIdFK.LeadFK.LeadStatusIdFk.LeadStatusIconClass,
                                            Comment = _commentLeadActivityLog.GetAll().Where(e => e.LeadActivityLogIdFk.LeadId == o.JobsIdFK.LeadId).OrderByDescending(e => e.Id).Select(e => e.CommentActivityNote).FirstOrDefault(),
                                            TotalProjectCost = o.JobsIdFK.TotalJobCost,
                                            BalanceOwing = o.BalanceOwing,
                                            LastPaymentDate = _paymentReceiptUploadjobRepository.GetAll().Where(x => x.PaymentId == o.Id).OrderByDescending(e => e.PaymentId == o.Id).FirstOrDefault().CreationTime,
                                            AdvancePayment = 0,
                                            Is_STC_payment = _stcPaymentDetailsRepository.GetAll().Where(x => x.STCPaymentId == o.Id).FirstOrDefault() == null ? false : true,
                                            Is_SS_payment = _ssPaymentDetailsRepository.GetAll().Where(x => x.SSPaymentId == o.Id).FirstOrDefault() == null ? false : true,
                                            PaymentDeliveryOption = o.PaymentDeliveryOption,
                                            ApplicationStatusdata = o.JobsIdFK.ApplicationStatusIdFK, //_applicationstatusRepository.GetAll().Where(x => x.Id == o.JobsIdFK.ApplicationStatusId).FirstOrDefault(),
                                            EastimatePaidDate = (DateTime?)o.JobsIdFK.EastimatePayDate,
                                            EastimatePaidStatus = o.JobsIdFK.EastimatePaidStatus,
                                            PaymentRefferenceNo = o.Id
                                        },                                      
                                    };
                var applicationtrackerListDtos = await paymentIssues.ToListAsync();
                if (input.excelorcsv == 1) // 1 use for excel , 2 use for csv
                {
                    return _PaymentIssueExcelExporter.ExportToFile(applicationtrackerListDtos);
                }
                else
                {
                    return _PaymentIssueExcelExporter.ExportToFile(applicationtrackerListDtos);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
