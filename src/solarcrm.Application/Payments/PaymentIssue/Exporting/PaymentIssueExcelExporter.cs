﻿using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.DataExporting.Excel.NPOI;
using solarcrm.Dto;
using solarcrm.Payments.PaymentIssue.Dto;
using solarcrm.Storage;
using solarcrm.Tracker.ApplicationTracker.Exporting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentIssue.Exporting
{
    public class PaymentIssueExcelExporter : NpoiExcelExporterBase, IPaymentIssueExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public PaymentIssueExcelExporter(ITimeZoneConverter timeZoneConverter, IAbpSession abpSession, ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }
        public FileDto ExportToFile(List<GetPaymentIssueForViewDto> paymentissue)
        {
            return CreateExcelPackage(
                "PaymentIssue.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("PaymentIssue");

                    AddHeader(
                        sheet,
                        L("ProjectNumber"),
                        L("LeadStatus"),
                        L("ApplicationStatus"),
                        L("CustomerName"),
                        L("Mobile"),
                        L("Totalprojectcost"),
                        L("Owingammount "),
                        L("Lastpaymentdate"),
                        L("Advance payment")                        
                        );
                    AddObjects(
                        sheet, paymentissue,
                        _ => _.paymentIssue.ProjectNumber,
                        _ => _.paymentIssue.LeadStatusName,
                        _ => _.paymentIssue.ApplicationStatusdata,
                        _ => _.paymentIssue.CustomerName,                        
                        _ => _.paymentIssue.Mobile,
                        _ => _.paymentIssue.TotalProjectCost,
                        _ => _.paymentIssue.BalanceOwing,
                        _ => _.paymentIssue.LastPaymentDate,
                        _ => _.paymentIssue.AdvancePayment
                        );
                });
        }
    }
}
