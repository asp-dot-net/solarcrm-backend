﻿using solarcrm.Dto;
using solarcrm.Payments.PaymentIssue.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentIssue.Exporting
{
    public interface IPaymentIssueExcelExporter
    {
        FileDto ExportToFile(List<GetPaymentIssueForViewDto> paymentIssueTracker);
    }
}
