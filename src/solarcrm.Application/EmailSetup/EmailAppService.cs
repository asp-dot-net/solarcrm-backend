﻿using MailKit;
using MailKit.Net.Imap;
using MailKit.Net.Pop3;
using MailKit.Net.Smtp;
using MailKit.Search;
using MailKit.Security;
using MimeKit;
using MimeKit.Cryptography;
using MimeKit.Text;
using Newtonsoft.Json;
using solarcrm.EmailSetup.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.EmailSetup
{
    public class EmailAppService : solarcrmAppServiceBase, IEmailAppService
    {
        //private readonly IEmailConfiguration _emailConfiguration;

        public EmailAppService(
            //IEmailConfiguration emailConfiguration
            )
        {
            //_emailConfiguration = emailConfiguration;
        }

        public async Task<List<object>> ReceiveEmail(int maxCount = 10)
        {
            //const string GMailAccount = "username@gmail.com";

            //var clientSecrets = new ClientSecrets
            //{
            //    ClientId = "XXX.apps.googleusercontent.com",
            //    ClientSecret = "XXX"
            //};

            //var codeFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
            //{
            //    // Cache tokens in ~/.local/share/google-filedatastore/CredentialCacheFolder on Linux/Mac
            //    DataStore = new FileDataStore("CredentialCacheFolder", false),
            //    Scopes = new[] { "https://mail.google.com/" },
            //    ClientSecrets = clientSecrets
            //});

            //var codeReceiver = new LocalServerCodeReceiver();
            //var authCode = new AuthorizationCodeInstalledApp(codeFlow, codeReceiver);
            //var credential = await authCode.AuthorizeAsync(GMailAccount, CancellationToken.None);

            //if (authCode.ShouldRequestAuthorizationCode(credential.Token))
            //    await credential.RefreshTokenAsync(CancellationToken.None);

            //var oauth2 = new SaslMechanismOAuth2(credential.UserId, credential.Token.AccessToken);

            //using (var emailClient = new Pop3Client())
            //{
            //    emailClient.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13;

            //    emailClient.CheckCertificateRevocation = false; 
            //    emailClient.Connect("pop.gmail.com", 995, SecureSocketOptions.SslOnConnect);

            //    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

            //    emailClient.Authenticate("pravin1.arisesolar@gmail.com", "wocoxzjjsulqmujb");

            //    List<EmailMessage> emails = new List<EmailMessage>();
            //    for (int i = 0; i < emailClient.Count && i < maxCount; i++)
            //    {
            //        var message = await emailClient.GetMessageAsync(i);
            //        var emailMessage = new EmailMessage
            //        {
            //            Content = !string.IsNullOrEmpty(message.HtmlBody) ? message.HtmlBody : message.TextBody,
            //            Subject = message.Subject
            //        };
            //        emailMessage.ToAddresses.AddRange(message.To.Select(x => (MailboxAddress)x).Select(x => new EmailAddress { Address = x.Address, Name = x.Name }));
            //        emailMessage.FromAddresses.AddRange(message.From.Select(x => (MailboxAddress)x).Select(x => new EmailAddress { Address = x.Address, Name = x.Name }));
            //        emails.Add(emailMessage);

            //    }
            //    emailClient.Disconnect(true);
            //    return emails;
            //}
            //using (var client = new SmtpClient())
            //{
            //    await client.ConnectAsync("smtp.office365.com", 587, SecureSocketOptions.StartTls);
            //    await client.AuthenticateAsync("{from email address}", "{from password}");
            //    //for (int i = 0; i < client.Count && i < maxCount; i++)
            //    //{
            //    //    var message = await client.GetMessageAsync(i);
            //    //}
            //    await client.DisconnectAsync(true);
            //}
            try
            {

                using (var emailClient = new ImapClient())
                {
                    emailClient.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13;

                    emailClient.ServerCertificateValidationCallback = (s, c, ch, e) => true;
                    await emailClient.ConnectAsync("outlook.office365.com", 993, SecureSocketOptions.SslOnConnect);
                    // await emailClient.ConnectAsync("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);

                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                    emailClient.Authenticate("pravin1.arisesolar@outlook.com", "Arise@123");
                    //await emailClient.AuthenticateAsync("pravin1.arisesolar@gmail.com", "wocoxzjjsulqmujb");
                    // The Inbox folder is always available on all IMAP servers...
                    var inbox = emailClient.Inbox;
                    await inbox.OpenAsync(FolderAccess.ReadOnly);
                    List<object> emails = new List<object>();
                    //for (int i = 0; i < inbox.Count && i < maxCount; i++)
                    //{
                    //    var message = await inbox.GetMessageAsync(i);
                    //    var emailMessage = new EmailMessage
                    //    {
                    //        Content = !string.IsNullOrEmpty(message.HtmlBody) ? message.HtmlBody : message.TextBody,
                    //        Subject = message.Subject
                    //    };
                    //    emailMessage.ToAddresses.AddRange(message.To.Select(x => (MailboxAddress)x).Select(x => new EmailAddress { Address = x.Address, Name = x.Name }));
                    //    emailMessage.FromAddresses.AddRange(message.From.Select(x => (MailboxAddress)x).Select(x => new EmailAddress { Address = x.Address, Name = x.Name }));
                    //    emails.Add(emailMessage);
                    //}
                    var summaries = await inbox.FetchAsync(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate | MessageSummaryItems.Envelope | MessageSummaryItems.References);
                    var orderBy = new OrderBy[] { OrderBy.ReverseDate };
                    var threads = MessageThreader.Thread(summaries, ThreadingAlgorithm.References, orderBy);
                    //emails.Add(threads);
                    //foreach (var summary in await inbox.FetchAsync(0, -1, MessageSummaryItems.Full))
                    foreach (var summary in threads)
                    {
                        if (summary.Message != null)
                        {
                            EmailClass additem = new EmailClass();
                            additem.Emailsubject = summary.Message.Envelope;
                            additem.uniqueid = (UniqueId)summary.UniqueId;
                            emails.Add(additem);
                        }
                        // summarysub.Add(summary);
                    }
                    //foreach (var summary in inbox.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.BodyStructure))
                    //{
                    //    if (summary.TextBody != null)
                    //    {
                    //        // this will download *just* the text/plain part
                    //        var text = inbox.GetBodyPart(summary.UniqueId, summary.TextBody);
                    //    }

                    //    if (summary.HtmlBody != null)
                    //    {
                    //        // this will download *just* the text/html part
                    //        var html = inbox.GetBodyPart(summary.UniqueId, summary.HtmlBody);
                    //    }

                    //    // if you'd rather grab, say, an image attachment... it might look something like this:
                    //    if (summary.Body is BodyPartMultipart)
                    //    {
                    //        var multipart = (BodyPartMultipart)summary.Body;

                    //        var attachment = multipart.BodyParts.OfType<BodyPartBasic>().FirstOrDefault(x => x.FileName == "logo.jpg");
                    //        if (attachment != null)
                    //        {
                    //            // this will download *just* the attachment
                    //            var part = inbox.GetBodyPart(summary.UniqueId, attachment);
                    //        }
                    //    }
                    //}


                    await emailClient.DisconnectAsync(true);
                    return emails;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public async Task<string> ReceiveEmailBody(uint index)
        {
            try
            {
                UniqueId duniqueid = new UniqueId(index);
                //dd(false, 1);
                using (var emailClient = new ImapClient())
                {
                    emailClient.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13;

                    emailClient.CheckCertificateRevocation = false;
                    //await emailClient.ConnectAsync("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);
                    await emailClient.ConnectAsync("outlook.office365.com", 993, SecureSocketOptions.SslOnConnect);
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    await emailClient.AuthenticateAsync("pravin1.arisesolar@gmail.com", "wocoxzjjsulqmujb");
                    // The Inbox folder is always available on all IMAP servers...
                    var inbox = emailClient.Inbox;
                    await inbox.OpenAsync(FolderAccess.ReadOnly);

                    var messageBody = await inbox.GetMessageAsync(duniqueid);
                    if (messageBody.Attachments.Count() > 0)
                    {
                        foreach (MimePart attachment in messageBody.Attachments)
                        {
                            Console.WriteLine("<p>Attachment: {0} {1}</p>", attachment.FileName, attachment.ContentType.MimeType);
                        }
                    }
                    var body = !string.IsNullOrEmpty(messageBody.HtmlBody) ? messageBody.HtmlBody : messageBody.TextBody;
                    return body;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<string> SendEmailCommonFun(MimeMessage message)
        {
            using (var emailClient = new SmtpClient())
            {
                await emailClient.ConnectAsync("smtp.office365.com", 587, SecureSocketOptions.Auto);
                // await emailClient.ConnectAsync("imap.gmail.com", 993, SecureSocketOptions.SslOnConnect);

                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                emailClient.Authenticate("pravin1.arisesolar@outlook.com", "Arise@123");
                //The last parameter here is to use SSL (Which you should!)
                //emailClient.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, true);

                //Remove any OAuth functionality as we won't be using it. 
                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

               // emailClient.Authenticate(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                emailClient.Send(message);

                emailClient.Disconnect(true);
            }
            return "";
        }
        public async Task<string> SendComposeEmail(EmailMessage emailMessage)
        {
            try
            {
                //var attachment = CreateAttachment();
                //var plain = CreateTextPlainPart();
                //var html = CreateTextHtmlPart();

                //// Note: it is important that the text/html part is added second, because it is the
                //// most expressive version and (probably) the most faithful to the sender's WYSIWYG 
                //// editor.
                //var alternative = new Multipart("alternative");
                //alternative.Add(plain);
                //alternative.Add(html);

                var message = new MimeMessage();
                message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
                message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

                message.Subject = emailMessage.Subject;
                //We will say we are sending HTML. But there are options for plaintext etc. 
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = emailMessage.Content
                };

                //var body = CreateMessageBody();
                //// now to encrypt our message body using our custom S/MIME cryptography context
                //using (var ctx = new MySecureMimeContext())
                //{
                //    // Note: this assumes that "Alice" has an S/MIME certificate with an X.509
                //    // Subject Email identifier that matches her email address. If she doesn't,
                //    // try using a SecureMailboxAddress which allows you to specify the
                //    // fingerprint of her certificate to use for lookups.
                //    message.Body = ApplicationPkcs7Mime.Encrypt(ctx, message.To.Mailboxes, body);
                //}
               

                // now set the multipart/mixed as the message body
                //message.Body = multipart;
                SendEmailCommonFun(message);
                // create our message text, just like before (except don't set it as the message.Body)
                //var body = new TextPart("plain")
                //{
                //    Text = @"Hey Alice,
                //    What are you up to this weekend? Monica is throwing one of her parties on
                //    Saturday and I was hoping you could make it.
                //    Will you be my +1?
                //    -- Joey
                //    "
                //};
                //var path = "";
                //// create an image attachment for the file located at path
                //var attachment1 = new MimePart("image", "gif")
                //{
                //    Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
                //    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                //    ContentTransferEncoding = ContentEncoding.Base64,
                //    FileName = Path.GetFileName(path)
                //};
                ////Save Msg and Attachment
                //// clone the default formatting options
                //var format = FormatOptions.Default.Clone();
               

                //// override the line-endings to be DOS no matter what platform we are on
                //format.NewLineFormat = NewLineFormat.Dos;
                //message.WriteTo(format, "message.eml");
                //foreach (var attachment in message.Attachments)
                //{
                //    if (attachment is MessagePart)
                //    {
                //        var fileName = attachment.ContentDisposition?.FileName;
                //        var rfc822 = (MessagePart)attachment;

                //        if (string.IsNullOrEmpty(fileName))
                //            fileName = "attached-message.eml";

                //        using (var stream = File.Create(fileName))
                //            rfc822.Message.WriteTo(stream);
                //    }
                //    else
                //    {
                //        var part = (MimePart)attachment;
                //        var fileName = part.FileName;

                //        using (var stream = File.Create(fileName))
                //            part.Content.DecodeTo(stream);
                //    }
                //    format.ParameterEncodingMethod = ParameterEncodingMethod.Rfc2047;

                //    //message.WriteTo(format, part);
                //}
                //// now create the multipart/mixed container to hold the message text and the
                //// image attachment
                //var multipart = new Multipart("mixed");
                //multipart.Add(body);
                //multipart.Add(attachment1);

                //// now set the multipart/mixed as the message body
                //message.Body = multipart;
                return message.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    //public Task<string> CreateMessageBody()
    //{
    //    // create an image attachment for the file located at path
    //    var attachment = new MimePart("image", "gif")
    //    {
    //        Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
    //        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
    //        ContentTransferEncoding = ContentEncoding.Base64,
    //        FileName = Path.GetFileName(path)
    //    };

    //    // now create the multipart/mixed container to hold the message text and the
    //    // image attachment
    //    var multipart = new Multipart("mixed");
    //    multipart.Add(body);
    //    multipart.Add(attachment);
    //    return "";
    //}
    //static Stream Decrypt(MimeMessage message)
    //{
    //    var text = message.TextBody;
    //    try
    //    {
    //        using (var encrypted = new MemoryStream(Encoding.ASCII.GetBytes(text), false))
    //        {
    //            using (var ctx = new GnuPGContext())
    //            {
    //                var decrypted = new MemoryStream();

    //                ctx.DecryptTo(encrypted, decrypted);
    //                decrypted.Position = 0;

    //                return decrypted;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }

    //}
    public class EmailClass
    {
        public object Emailsubject { get; set; }


        public UniqueId uniqueid { get; set; }
    }
}