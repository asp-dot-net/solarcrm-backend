﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using solarcrm.EntityFrameworkCore;

namespace solarcrm.HealthChecks
{
    public class solarcrmDbContextHealthCheck : IHealthCheck
    {
        private readonly DatabaseCheckHelper _checkHelper;

        public solarcrmDbContextHealthCheck(DatabaseCheckHelper checkHelper)
        {
            _checkHelper = checkHelper;
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            if (_checkHelper.Exist("db"))
            {
                return Task.FromResult(HealthCheckResult.Healthy("solarcrmDbContext connected to database."));
            }

            return Task.FromResult(HealthCheckResult.Unhealthy("solarcrmDbContext could not connect to database"));
        }
    }
}
