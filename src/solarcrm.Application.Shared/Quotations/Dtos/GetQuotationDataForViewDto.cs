﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class GetQuotationDataForViewDto
    {
        public QuotationDataDto Quotation { get; set; }
    }
}
