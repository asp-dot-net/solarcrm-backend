﻿using Abp.Web.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class UploadDocumentOutput : ErrorInfo
    {
        public string FileName { get; set; }

        public string FileType { get; set; }

        public string FileToken { get; set; }

        public int Size { get; set; }

        public UploadDocumentOutput()
        {

        }

        public UploadDocumentOutput(ErrorInfo error)
        {
            Code = error.Code;
            Details = error.Details;
            Message = error.Message;
            ValidationErrors = error.ValidationErrors;
        }
    }
}
