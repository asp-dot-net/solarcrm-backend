﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class QuotationDataDetailsDto
    {
        public QuotationDataDto QuotationData { get; set; }
        public int JobId { get; set; }

        public int QuotationId { get; set; }

        public string QuoteCustomerName { get; set; }
        public string QuoteNumber { get; set; }

        public DateTime? QuoteGenerateDate { get; set; }
        public virtual bool IsQuotateSigned { get; set; }
        public virtual bool IsQuotateExpiry { get; set; }        

        public virtual DateTime? QuotateSignedDate { get; set; }

        public virtual string QuotateSignedImagePath { get; set; }
        public virtual string CustomerName { get; set; }

        public virtual string Address1 { get; set; }

        public virtual string Address2 { get; set; }
        public virtual string Mobile { get; set; }

        public virtual string Email { get; set; }
        public virtual string SolarAdvisorName { get; set; }      //Lead Assign User Name

        public virtual string SolarAdvisorMobile { get; set; }   //Lead Assign User Mobile Number

        public virtual string SolarAdvisorEmail { get; set; }    //Lead Assign User Email

        public virtual string SystemCapacity { get; set; }    //Syatem Size 
        public virtual decimal? PvCapacityKw { get; set; }
        
        public virtual decimal? ActualSystemCost { get; set; }    // Actual Price

        public virtual decimal? Subcidy40 { get; set; }

        public virtual decimal? Subcidy20 { get; set; }

        public virtual decimal? SubcidyTotal { get; set; }

        public virtual decimal? BillToPay { get; set; }
        public virtual decimal? DepositeRequired { get; set; }
        public virtual string DiscomName { get; set; }
        public virtual string HeightOfStructure { get; set; }

        public virtual string DivisionName { get; set; }

        public virtual string QuoteNotes { get; set; }
        public List<object> JobProductItemList { get; set; }

        public List<object> JobVariationItemList { get; set; }

        public object OrganizationDetails { get; set; }

        public List<object> OrganizationbankDetails { get; set; }
        //public List<QuotationDataProduct> qunityAndModelLists { get; set; }
        //public List<QunityAndModelList> qunityAndModelLists { get; set; }
    }
}
