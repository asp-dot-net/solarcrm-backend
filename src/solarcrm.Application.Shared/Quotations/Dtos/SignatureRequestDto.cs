﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class SignatureRequestDto
    {
        public bool Expiry { get; set; }
        public bool Signed { get; set; }
        public string QuoteNo { get; set; }
        public string CustName { get; set; }
    }
}
