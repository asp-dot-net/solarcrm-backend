﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class SaveCustomerSignature
    {
        public string QuoteSignIP { get; set; }

        public decimal QuoteSignLongitude { get; set; }

        public decimal? QuoteSignLatitude { get; set; }

        public string EncString { get; set; }

        public string ImageData { get; set; }
    }
}
