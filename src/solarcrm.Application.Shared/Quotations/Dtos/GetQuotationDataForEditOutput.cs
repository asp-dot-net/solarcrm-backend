﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class GetQuotationDataForEditOutput
    {
        public CreateOrEditQuotationDataDto DepositOption { get; set; }
    }
}
