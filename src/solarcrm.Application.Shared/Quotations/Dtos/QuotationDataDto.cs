﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class QuotationDataDto : EntityDto
    {
        public string QuoteNumber { get; set; }
        public DateTime? QuoteDate { get; set; }
        public bool IsSigned { get; set; }
        public int? QuoteMode { get; set; }
        public int? JobId { get; set; }
        public string CreatedBy { get; set; }
        //public Guid? DocumentId { get; set; }
        //public string QuoteFileName { get; set; }
        //public string SignedQuoteFileName { get; set; }
        //public string QuoteFilePath { get; set; }


    }
}
