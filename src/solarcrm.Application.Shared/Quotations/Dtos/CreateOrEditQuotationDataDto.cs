﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Quotations.Dtos
{
    public class CreateOrEditQuotationDataDto : EntityDto<int?>
    {
        public virtual DateTime QuoteDate { get; set; }
        public virtual bool IsSigned { get; set; }
        public virtual Guid? DocumentId { get; set; }
        public virtual int QuoteMode { get; set; }
        public virtual int? JobId { get; set; }

        public List<object> JobProductItemList { get; set; }

        public List<object> JobVariationItemList { get; set; }
        public virtual decimal? Subcidy40 { get; set; }

        public virtual decimal? Subcidy20 { get; set; }

        // public virtual decimal? SubcidyTotal { get; set; }

        public virtual decimal? BillToPay { get; set; }
        public string EmailBody { get; set; }

        public int OrganizationUnitId { get; set; }
        public string EmailTo { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string Subject { get; set; }    
    }
}
