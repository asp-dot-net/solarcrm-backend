﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.Quotations.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Quotations
{
    public interface IQuotationsAppService : IApplicationService
    {
        Task<PagedResultDto<GetQuotationDataForViewDto>> GetAll(GetAllQuotationDataInput input);
        Task<QuotationDataDetailsDto> GetQuoteForView(int quoteId, int organizationId);
        Task<GetQuotationDataForEditOutput> GetQuotationForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditQuotationDataDto input);
        //Task SendQuotation(CreateOrEditQuotationDataDto input);
        Task Delete(EntityDto input);
        //Task<string> GetQuotationStringByQuoateIdJobId(int JobId, int QuoteId);
        Task<string> GeneratePdf(string JobNumber, object QuoteString);
        //Task<string> DownloadQuote(int JobId, int QuoteId);

        Task SaveSignature(SaveCustomerSignature input);
    }
}
