﻿using System.Threading.Tasks;
using Abp.Webhooks;

namespace solarcrm.WebHooks
{
    public interface IWebhookEventAppService
    {
        Task<WebhookEvent> Get(string id);
    }
}
