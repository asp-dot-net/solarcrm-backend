﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using System.Text;

namespace solarcrm.MobileService.MCommonRespose
{
    public class BaseResponse
    {
        [DataMember]
        public int StatusCode { get; set; }

        public bool IsSuccess { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Message { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public object Result { get; set; }

        public BaseResponse(HttpStatusCode statusCode, bool Success, object result = null, string MessageRes = null)
        {
            StatusCode = (int)statusCode;
            IsSuccess = Success;
            Result = result;
            Message = MessageRes;
        }
    }
}
