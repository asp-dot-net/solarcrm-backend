﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MobileService.MCommon.Dto
{
    public class NamingString
    {
        public string Name { get; set; }
    }

    public class CityResult
    {
        public string City { get; set; }

        public string Taluka { get; set; }

        public string District { get; set; }

        public string State { get; set; }
    }

    public class SubDivisionResult
    {
        public string SubDivision { get; set; }

        public string Division { get; set; }

        public string Circle { get; set; }

        public string Discom { get; set; }
    }

    public class Root
    {
        public List<NamingString> result { get; set; }
        public object targetUrl { get; set; }
        public bool success { get; set; }
        public object error { get; set; }
        public bool unAuthorizedRequest { get; set; }
        public bool __abp { get; set; }
    }
}
