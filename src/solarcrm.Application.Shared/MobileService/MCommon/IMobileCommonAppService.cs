﻿using Abp.Application.Services;
using solarcrm.MobileService.MCommon.Dto;
using solarcrm.MobileService.MCommonRespose;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MobileService.MCommon
{
    public interface IMobileCommonAppService : IApplicationService
    {
        Task<List<NamingString>> GetAllSolarTypeForDropdown();

        Task<List<NamingString>> SearchCity(StringFilter input);

        Task<CityResult> CityResult(StringFilter input);

        Task<List<NamingString>> SearchSubDivision(StringFilter input);

        Task<SubDivisionResult> SubDivisionResult(StringFilter input);

        Task<List<NamingString>> GetAllDivisionForDropdown();

        Task<List<NamingString>> GetAllCircleForDropdown();

        Task<List<NamingString>> GetAllDiscomForDropdown();

        Task<List<NamingString>> GetAllHeightOfStructureForDropdown();

        Task<List<NamingString>> GetAllLeadSourceForDropdown();

        Task<List<NamingString>> GetAllLeadTypeForDropdown();

        Task<List<NamingString>> GetAllLeadStatusForDropdown();

        Task<List<NamingString>> GetAllCategoryForDropdown();

        Task<List<NamingString>> SearchChannelPartner(StringFilter input);

        Task<BaseResponse> CheckDuplicateConsumerNoMobile(DuplicateConsumerFilter filter);
    }
}
