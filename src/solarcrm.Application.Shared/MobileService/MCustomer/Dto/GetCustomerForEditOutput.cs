﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MobileService.MCustomer.Dto
{
    public class GetCustomerForEditOutput
    {
        public CreateOrEditCustomerDto Customer { get; set; }
    }
}
