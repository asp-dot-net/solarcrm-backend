﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.MobileService.MCustomer.Dto
{
    public class CreateOrEditCustomerDto : EntityDto<int?>
    {
        [Required]
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public string LastName { get; set; }

        public string CustomerName { get; set; }

        [Required]
        public string MobileNumber { get; set; }

        public string EmailId { get; set; }

        public string Alt_Phone { get; set; }

        public string AddressLine1 { get; set; }
        
        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string Taluka { get; set; }

        public string District { get; set; }

        public string State { get; set; }

        public int? Pincode { get; set; }

        public string LeadType { get; set; }

        public string LeadSource { get; set; }

        public string LeadStatus { get; set; }
        
        public string SolarType { get; set; }
        
        public bool CommonMeter { get; set; }

        public int? ConsumerNumber { get; set; }
        
        public int? AvgmonthlyUnit { get; set; }
        
        public int? AvgmonthlyBill { get; set; }

        public string Discom { get; set; }
        
        public string Division { get; set; }

        public string SubDivision { get; set; }

        public string Circle { get; set; }
        
        public int? RequiredCapacity { get; set; }

        public int? StrctureAmmount { get; set; }
        
        public string HeightStructure { get; set; }

        public string Category { get; set; }

        public string ChanelPartner { get; set; }

        public int? BankAccountNumber { get; set; }

        public string BankAccountHolderName { get; set; }

        public string BankName { get; set; }
        
        public string BankBrachName { get; set; }
        
        public string IFSC_Code { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public string Notes { get; set; }
        
        public bool IsActive { get; set; }
        
        public virtual bool IsVerified { get; set; }
        
        public string AreaType { get; set; }
    }
}
