﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MobileService.MCustomer.Dto
{
    public class GetCustomerForViewDto
    {
        public string JobNumber { get; set; }

        //public string Latitude { get; set; }

        //public string Longitude { get; set; }

        //public string SolarType { get; set; }

        //public string HightOfStructure { get; set; }

        //public string StructureAmount { get; set; }

        //public string CommonMeter { get; set; }

        //public string RequireCapacity { get; set; }

        //public string ConsumerNumber { get; set; }

        public List<TechnicalDetailsDto> TechnicalDetails { get; set; }
    }
}
