﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MobileService.MCustomer.Dto
{
    public class CustomerDto : EntityDto
    {
        public string CustomerName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string Mobile { get; set; }

        public string CustomerBadge { get; set; }

        public bool IsLeadStatusVisible { get; set; }

        public string LeadStatus { get; set; }

        public string LeadStatusColorClass { get; set; }

        public bool IsApplicationStatusVisible { get; set; }

        public string ApplicationStatus { get; set; }

        public string ApplicationColorClass { get; set; }

        public bool IsVerified { get; set; }

        public bool IsJob { get; set; }
    }
}
