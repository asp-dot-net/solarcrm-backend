﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MobileService.MCustomer.Dto
{
    public class TechnicalDetailsDto
    {
        public string Title { get; set; }

        public string Value { get; set; }
    }
}
