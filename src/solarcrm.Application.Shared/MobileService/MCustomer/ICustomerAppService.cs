﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.MobileService.MCustomer.Dto;
using System.Threading.Tasks;
using solarcrm.MobileService.MCommonRespose;

namespace solarcrm.MobileService.MCustomer
{
    public interface ICustomerAppService: IApplicationService
    {
        Task<PagedResultDto<CustomerDto>> GetAll(GetAllCustomerInput input);

        Task<GetCustomerForViewDto> GetCustomerForView(NullableIdDto<int> input);

        Task<GetCustomerForEditOutput> GetCustomerForEdit(NullableIdDto<int> input);

        Task<BaseResponse> CreateOrEdit(CreateOrEditCustomerDto input);

    }
}
