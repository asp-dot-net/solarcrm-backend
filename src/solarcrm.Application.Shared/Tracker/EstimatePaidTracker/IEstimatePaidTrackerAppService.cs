﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Jobs.Dto;
using solarcrm.Tracker.EstimatePaidTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.EstimatePaidTracker
{
    public interface IEstimatePaidTrackerAppService
    {
        Task<PagedResultDto<GetEstimatedTrackerForViewDto>> GetAllEstimatedTracker(GetAllEstimatedTrackerInput input);

        Task<EstimatedTrackerQuickViewDto> GetEstimatedQuickView(EntityDto input);

        Task EstimatedDetailsEdit(CreateOrEditJobDto input);

        Task AddToEstimatePayResponse(GetProjectTransferForEstimatePayResponseOutput input);

        Task<FileDto> GetEstimatedTrackerToExcel(GetAllEstimatedTrackerInput input);
    }
}
