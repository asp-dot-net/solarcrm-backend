﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.EstimatePaidTracker.Dto
{
    public class EstimatedTrackerQuickViewDto : EntityDto
    {
        public int LeadId { get; set; }
        public string ProjectNumber { get; set; }
        public string CustomerName { get; set; }
        public string MobileNumber { get; set; }
        public long? ConsumerNumber { get; set; }
        public string DiscomeName { get; set; }
        public string CpName { get; set; }
        public string CpMobileNumber { get; set; }
        public string EstimatePayementRefNumber { get; set; }
        public string EastimatePaidStatus { get; set; } 
        public int? EastimateQuoteNo { get; set; }
        public decimal? EastimateQuoteAmount { get; set; }
        public DateTime? EastimatePayDate { get; set; }
        public DateTime? EastimateDueDate { get; set; }
        public string EastimatePayReceiptPath { get; set; }
        public string EastimatePayResponse { get; set; }
    }
}
