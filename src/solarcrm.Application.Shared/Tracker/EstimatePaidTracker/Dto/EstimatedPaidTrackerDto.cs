﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.EstimatePaidTracker.Dto
{
    public class EstimatedPaidTrackerDto : EntityDto
    {
        public LeadsDto leaddata { get; set; }

        public string ProjectNumber { get; set; }

        public string CustomerName { get;set; }

        public string Mobile { get; set; }

       public string Discom { get; set; }

        public long? ConsumerNumber { get; set; }

        public int? EastimateQuoteNo { get; set; }

        public decimal? EastimateQuoteAmount { get; set; }

        public DateTime? EastimateDueDate { get; set; }

        public string EastimatePayResponse { get; set; }

        public string EastimatePaidStatus { get; set; }
        public string EstimatePaymentReceiptPath { get; set; }

        public int? PanaltyDays { get;set; }
        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }
        public string Comment { get; set; }
        public object ApplicationStatusdata { get; set; }

        public string PaymentLink { get; set; }

    }
}
