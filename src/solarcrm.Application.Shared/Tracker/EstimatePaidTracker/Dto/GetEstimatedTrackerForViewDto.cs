﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.EstimatePaidTracker.Dto
{
    public class GetEstimatedTrackerForViewDto
    {
        public EstimatedPaidTrackerDto EstimateTracker { get; set; }
        public EstimatePaidTrackerSummaryCount estimatePadiTrackerSummaryCount { get; set; }
    }

    public class EstimatePaidTrackerSummaryCount {
        public string Total { get; set; } = "0";
        public string FeasiblityApproved { get; set; } = "0";
        public string EastimatePaid { get; set; } = "0";
        public string DueDate { get; set; } = "0";
        public string TotalKillowatt { get; set; } = "0";
        public string TotalPanel { get; set; } = "0";        
    }
}
