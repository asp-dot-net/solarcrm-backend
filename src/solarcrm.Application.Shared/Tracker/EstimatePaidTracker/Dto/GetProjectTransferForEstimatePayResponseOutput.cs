﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.EstimatePaidTracker.Dto
{
    public class GetProjectTransferForEstimatePayResponseOutput : EntityDto<int?>
	{
		
		public int? OrganizationID { get; set; }

		public List<int> JobIds { get; set; }

		public int? AssignToUserID { get; set; }

		public string PayResponse { get; set; }

	}
}
