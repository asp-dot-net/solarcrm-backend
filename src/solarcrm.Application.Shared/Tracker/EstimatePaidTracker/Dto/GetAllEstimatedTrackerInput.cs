﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.EstimatePaidTracker.Dto
{
    public class GetAllEstimatedTrackerInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        //public string ProjectNumberFilter { get; set; }
        //public string CustomerName { get; set; }

        //public string FirstNameFilter { get; set; }

        //public string EmailFilter { get; set; }

        //public string PhoneFilter { get; set; }

        //public string MobileFilter { get; set; }

        //public string AddressFilter { get; set; }

        //public string StateNameFilter { get; set; }

        //public string PostCodeFilter { get; set; }
        public int? OrganizationUnit { get; set; }    //organizationFilter            

        //public int? SalesManagerId { get; set; }

        //public int? SalesRepId { get; set; }

        public List<int> LeadStatusIDS { get; set; }
      
        public List<int> applicationstatusTrackerFilter { get; set; }
        public string priorityFilter { get; set; }
        public string estimatedPaidFilter { get; set; }

        public string estimatedPayResonseFilter { get; set; }

        public List<int> DiscomIdFilter { get; set; }

        public List<int> CircleIdFilter { get; set; }

        public List<int> DivisionIdFilter { get; set; }

        public List<int> SubDivisionIdFilter { get; set; }

        public List<int> solarTypeIdFilter { get; set; }
        public List<int> TenderIdFilter { get; set; }

        public List<int> employeeCpList { get; set; }

        public string FilterbyDate { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? excelorcsv { get; set; }

        public string ManualPaymentFilter { get; set; }
    }
}
