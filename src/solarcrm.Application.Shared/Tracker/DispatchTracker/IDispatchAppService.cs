﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.DispatchTracker.Dto;
using solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial;
using solarcrm.Tracker.DispatchTracker.Dto.DispatchStockTransfer;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.DispatchTracker
{
    public interface IDispatchAppService : IApplicationService
    {
        Task<PagedResultDto<GetDispatchItemForViewDto>> GetAllDispatch(GetAllDispatchItemsInput input);
        Task<DispatchTrackerQuickViewDto> GetDispatchQuickView(EntityDto input);

        Task<GetDispatchForEditOutput> GetDispatchForEdit(EntityDto input);
        Task Verifypicklist(CreateOrEditDispatchDto input);

        Task CreateOrEdit(CreateOrEditDispatchDto input);
        Task<GetDispatchExtraMaterialForEditOutput> GetDispatchExtraMaterialForEdit(EntityDto input);
        Task CreateOrEditExtraMaterial(List<DispatchExtraMaterialDto> input);
        Task DeleteDispatch(EntityDto input, int leadId, int OrgId);
        Task<GetDispatchMaterialTransferForEditOutput> GetDispatchMaterialTransferForEdit(EntityDto input);
        Task CreateOrEditTransferMaterial(DispatchMaterialTransferDto input);
        Task<FileDto> GetDispatchTrackerToExcel(GetAllDispatchItemsInput input);
    }
}
