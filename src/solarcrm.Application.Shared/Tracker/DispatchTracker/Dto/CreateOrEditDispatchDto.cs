﻿using Abp.Application.Services.Dto;
using solarcrm.Jobs.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto
{
    public class CreateOrEditDispatchDto : EntityDto<int?>
    {
        public virtual int DispatchJobId { get; set; }

        public virtual int? DispatchStockItemId { get; set; }

        public virtual int? DispatchHeightStructureId { get; set; }

        public virtual int? DispatchTypeId { get; set; }

        public virtual DateTime DispatchDate { get; set; }

        public virtual string DispatchNotes { get; set; }

        public virtual DateTime? DispatchReturnDate { get; set; }
        public virtual string DispatchReturnNotes { get; set; }
        public virtual string ExtraMaterialNotes { get; set; }
        public virtual bool IsExtraMaterialApproved { get; set; }
        public virtual string ExtraMaterialApprovedNotes { get; set; }
        public virtual int leadid { get; set; }
        public virtual int OrgID { get; set; }
        public virtual bool IsActive { get; set; }

        public virtual List<CreateOrEditDispatchProductItemDto> dispathproductlist { get; set; }
    }
}
