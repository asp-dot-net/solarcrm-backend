﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto
{
    public class GetDispatchForEditOutput
    {
        public CreateOrEditDispatchDto Dispatch { get; set; }
    }
}
