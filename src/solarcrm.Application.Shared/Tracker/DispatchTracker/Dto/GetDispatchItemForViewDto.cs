﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto
{
    public class GetDispatchItemForViewDto
    {
        public DispatchDto dispatchItem { get; set; }
        public DispatchTrackerSummaryCount dispatchTrackerSummaryCount { get; set; }
    }
    public class DispatchTrackerSummaryCount
    {
        public string Total { get; set; } = "0";
        public string TotalPanels { get; set; } = "0";
        public string TotalOwingAmount { get; set; } = "0";
        public string TotalKilowatt { get; set; } = "0";

    }
}
