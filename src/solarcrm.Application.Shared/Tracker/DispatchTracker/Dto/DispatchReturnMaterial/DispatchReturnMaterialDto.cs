﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchReturnMaterial
{
    public class DispatchReturnMaterialDto : EntityDto
    {
        public virtual int DispatchId { get; set; }

        public virtual int? DispatchStockItemId { get; set; }

        public decimal ActualQuantity { get; set; }
    }
}
