﻿using solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchReturnMaterial
{
    public class GetDispatchReturnMaterialForEditOutput
    {
        public CreateOrEditDispatchReturnMaterialDto ReturnMaterial { get; set; }
    }
}
