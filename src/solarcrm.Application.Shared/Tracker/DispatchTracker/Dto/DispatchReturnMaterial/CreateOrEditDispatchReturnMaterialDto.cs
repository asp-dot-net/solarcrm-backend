﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchReturnMaterial
{
    public class CreateOrEditDispatchReturnMaterialDto : EntityDto<int?>
    {
        public virtual int DispatchId { get; set; }
       
        public virtual int? DispatchStockItemId { get; set; }
       
        public decimal ActualQuantity { get; set; }
    }
}
