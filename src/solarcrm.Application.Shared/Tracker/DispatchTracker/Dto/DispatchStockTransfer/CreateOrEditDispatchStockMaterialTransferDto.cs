﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchStockTransfer
{
    public class CreateOrEditDispatchStockMaterialTransferDto
    {
        public virtual int Id { get; set; }
        public virtual int DispatchId { get; set; }
        public virtual string JobNumber { get; set; }
        public virtual string Reason { get; set; }
    }
}
