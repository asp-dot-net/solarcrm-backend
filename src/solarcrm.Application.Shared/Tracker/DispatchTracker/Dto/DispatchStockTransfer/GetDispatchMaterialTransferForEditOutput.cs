﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchStockTransfer
{
    public class GetDispatchMaterialTransferForEditOutput
    {
        public CreateOrEditDispatchStockMaterialTransferDto materialTransferDto { get; set; }
    }
}
