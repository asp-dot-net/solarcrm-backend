﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchStockTransfer
{
    public class DispatchMaterialTransferDto : EntityDto
    {
        public virtual int DispatchId { get; set; }
        public virtual string JobNumber { get; set; }
        public virtual string Reason { get; set; }
        public virtual int leadid { get; set; }
        public virtual int OrgID { get; set; }
    }
}
