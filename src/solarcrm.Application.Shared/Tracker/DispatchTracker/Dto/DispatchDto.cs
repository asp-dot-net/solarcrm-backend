﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto
{
    public class DispatchDto : EntityDto
    {
        public int DispatchJobID { get; set; }
        public int? PanaltyDays { get; set; }
        public DateTime? DispatchDate { get; set; }
        public string DispatchType { get; set; }
        public bool VerifyPicklist { get; set; }
        public LeadsDto leaddata { get; set; }
        public object jobdata { get; set; }

        public int JobId { get; set; }
        public string ProjectNumber { get; set; }
        public int dispatchJobId { get; set; }
        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }
        public string Email { get; set; }
        public string SolarType { get; set; }
        public string CpName { get; set; }
        public string CpMobile { get; set; }
        public string JobOwnedBy { get; set; }
        public string LeadStatusName { get; set; }

        public int ApplicationStatus { get; set; }

        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }

        public string Comment { get; set; }
        public string Notes { get; set; }
        public bool IsActive { get; set; }
        public bool IsApprovedMaterial { get; set; }
        public DateTime? ApplicationDate { get; set; }

        public string LeadStatusColorClass { get; set; }
        public string LeadStatusIconClass { get; set; }

        public object ApplicationStatusdata { get; set; }

        public DateTime? ReminderTime { get; set; }
        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }
        public bool isExtraMaterialApproved { get; set; }

    }
}
