﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto
{
    public class GetAllDispatchTrackerInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? OrganizationUnit { get; set; }    //organizationFilter            

        //public int? SalesManagerId { get; set; }

        //public int? SalesRepId { get; set; }

        public List<int> LeadStatusIDS { get; set; }
        public List<int> employeeIdFilter { get; set; }
        public List<int> leadApplicationStatusFilter { get; set; } //application Filter
        public int? refundPaymentTrackerFilter { get; set; }
        public int? refundVerifyFilter { get; set; }
        public int? refundPenddingFilter { get; set; }
        public string jobStausFilter { get; set; }
        public int refundReasonFilter { get; set; }
        public string refundDateFilter { get; set; }
        public string FilterbyDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? excelorcsv { get; set; }

    }
}
