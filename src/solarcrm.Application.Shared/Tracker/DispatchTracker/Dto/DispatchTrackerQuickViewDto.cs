﻿using Abp.Application.Services.Dto;
using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto
{
    public class DispatchTrackerQuickViewDto : EntityDto
    {
        //CustomerDetails
        public string ProjectNumber { get; set; }
        public string CustomerName { get; set; }
        public string MobileNumber { get; set; }
        public long? ConsumerNumber { get; set; }
        public string DiscomName { get; set; }
        public string ChanelPartnerName { get; set; }
        public string ChanelPartnerMobileNo { get; set; }

        //Project Details
        public decimal? SystemSize { get; set; }
        public int? NoOfPanel { get; set; }
        public string PanelBrandName { get; set; }
        public int? NoOfInverter { get; set; }
        public string InverterBrand { get; set; }
        public string StructureHeight { get; set; }
        public decimal? StructureAmount { get; set; }

        //Eastimate Payment Details
        public string EastimatePayementRefferanceNo { get; set; }
        public string EastimatePaymentStatus { get; set; }
        public int? EastimateQuoteNo { get; set; }
        public decimal? EastimateQuoteAmount { get; set; }
        public DateTime? EastimateDueDate { get; set; }
        public string EastimatePayResponse { get; set; }

        //Sent Material Details
        public string HeightofStructure { get; set; }
        public DateTime? DispatchDate { get; set; }
        public string DispatchNote { get; set; }
        public DateTime? DispatchReturnDate { get; set; }
        public string DispatchReturnNote { get; set; }
        public string SentMaterial { get; set; }
        public string VehicalName { get; set; }
        public string VerifyBy { get; set; }

        //Extra Material Details
        
        public string ApprovadBy { get; set; }
        public DateTime? ApprovadDate { get; set; }
        public string ExtraMaterialNotes { get; set; }
        public bool IsExtraMaterialApproved { get; set; }
        public string ExtraMaterialApprovedNotes { get; set; }
        public List<DispatchExtraMaterialDto> ExtraMaterialList { get; set; }
        //Document Download 
        public List<LeadDocumentDto> LeadDocumentList { get; set; }
    }
}
