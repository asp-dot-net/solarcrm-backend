﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto
{
    public class CreateOrEditDispatchProductItemDto : EntityDto<int?>
    {
        public virtual int? DispatchId { get; set; }
      
        public virtual int? ProductItemId { get; set; }
      
        public virtual int? Quantity { get; set; }
        public int? ProductTypeId { get; set; }
    }
}
