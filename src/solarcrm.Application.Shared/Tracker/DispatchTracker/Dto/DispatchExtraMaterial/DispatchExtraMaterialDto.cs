﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial
{
    public class DispatchExtraMaterialDto : EntityDto
    {
        public virtual int DispatchId { get; set; }
        public virtual int? DispatchStockItemId { get; set; }
        public virtual string StockItemName { get; set; }
        public virtual decimal? Quantity { get; set; }
        public virtual long? UserManagerId { get; set; }
        public virtual string ManagerName { get; set; }
        public virtual string ExtraMaterialNotes { get; set; }
        public virtual bool IsExtraMaterialApproved { get; set; }
        public virtual string ExtraMaterialApprovedNotes { get; set; }        
        public virtual long? MaterilApproveById { get; set; }
       
        public virtual int leadid { get; set; }
        public virtual int OrgID { get; set; }
    }
}
