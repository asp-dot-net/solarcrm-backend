﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial
{
    public class CreateOrEditDispatchExtraMaterialDto : EntityDto<int?>
    {
        public virtual int DispatchId { get; set; }
        public virtual int? DispatchStockItemId { get; set; }
        public virtual string StockItemName { get; set; }
        public virtual int? StockItemQty { get; set; }
        public virtual long? ManagerId { get; set; }
        public virtual string ManagerName { get; set; }
        public virtual string ExtraMaterialNotes { get; set; }
        public virtual bool IsExtraMaterialApproved { get; set; }
        public virtual string ExtraMaterialApprovedNotes { get; set; }
        public virtual long? MaterilApproveById { get; set; }
       

    }
}
