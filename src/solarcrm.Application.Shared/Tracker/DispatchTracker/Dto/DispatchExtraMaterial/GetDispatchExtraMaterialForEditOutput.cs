﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DispatchTracker.Dto.DispatchExtraMaterial
{
    public class GetDispatchExtraMaterialForEditOutput
    {
        public CreateOrEditDispatchExtraMaterialDto ExtraMaterial { get; set; }
    }
}
