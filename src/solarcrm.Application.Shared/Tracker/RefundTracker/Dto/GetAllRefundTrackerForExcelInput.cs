﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.RefundTracker.Dto
{
    public class GetAllRefundTrackerForExcelInput
    {
        public string Filter { get; set; }
    }
}
