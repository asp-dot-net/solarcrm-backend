﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.RefundTracker.Dto
{
    public class GetRefundTrackerForViewDto
    {
        public RefundTrackerDto refundTracker { get; set; }
        public RefundTrackerSummaryCount refundTrackerSummaryCount { get; set; }
    }
    public class RefundTrackerSummaryCount
    {
        public string Total { get; set; } = "0";
        public string Pending { get; set; } = "0";
        public string Refunded { get; set; } = "0";
        
    }
}
