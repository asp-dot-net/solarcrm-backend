﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.RefundTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.RefundTracker
{
    public interface IRefundTrackerAppService
    {
        Task<PagedResultDto<GetRefundTrackerForViewDto>> GetAllRefundTracker(GetAllRefundTrackerInput input);

        Task<RefundTrackerQuickViewDto> GetRefundQuickView(EntityDto input);

        //Task RefundDetailsEdit(CreateOrEditJobDto input, int SectionId);

        Task<FileDto> GetRefundTrackerToExcel(GetAllRefundTrackerInput input);
    }
}
