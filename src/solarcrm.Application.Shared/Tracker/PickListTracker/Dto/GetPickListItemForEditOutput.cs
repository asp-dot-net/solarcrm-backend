﻿using solarcrm.Tracker.DispatchTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.PickListTracker.Dto
{
    public class GetPickListItemForEditOutput
    {
        public virtual int DispatchId { get; set; }

        public List<CreateOrEditDispatchProductItemDto> picklistStockItem { get; set; }
        public List<CreateOrEditPicklistItemDto> picklistItem { get; set; }
    }
}
