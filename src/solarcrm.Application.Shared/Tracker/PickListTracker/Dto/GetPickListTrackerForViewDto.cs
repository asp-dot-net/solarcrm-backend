﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.PickListTracker.Dto
{
    public class GetPickListTrackerForViewDto
    {
        public PickListTrackerDto picklistTracker { get; set; }
        public PickListTrackerSummaryCount pickListTrackerSummaryCount { get; set; }
    }
    public class PickListTrackerSummaryCount
    {
        public string Total { get; set; } = "0";       
        public string TotalKillowatt { get; set; } = "0";
        public string TotalPanel { get; set; } = "0";
    }
}
