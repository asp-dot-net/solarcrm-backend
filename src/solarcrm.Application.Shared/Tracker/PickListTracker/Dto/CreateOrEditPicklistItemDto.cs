﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.PickListTracker.Dto
{
    public class CreateOrEditPicklistItemDto : EntityDto<int?>
    {
        public virtual int PickListDisptchId { get; set; }       
        public virtual int BillOfMaterialId { get; set; }        
        public virtual string MaterialName { get; set; }
        public virtual int? PickListQty { get; set; }
    }
}
