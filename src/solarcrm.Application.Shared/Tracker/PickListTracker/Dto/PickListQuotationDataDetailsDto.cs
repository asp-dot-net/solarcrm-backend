﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.PickListTracker.Dto
{
    public class PickListQuotationDataDetailsDto
    {
        //Customer Details
        public int JobId { get; set; }

        public virtual string CustomerName { get; set; }

        public virtual string Address { get; set; }

        //public virtual string Address2 { get; set; }
        public virtual string Mobile { get; set; }
        public virtual string AltPhone { get; set; }
        public virtual string Email { get; set; }
        public virtual string ProjectNo { get; set; }      //Lead Assign User Name

        public virtual string ApplicationNo { get; set; }   //Lead Assign User Mobile Number

        public virtual decimal? SystemSize { get; set; }    //Lead Assign User Email

        public virtual string StructureHeight { get; set; }    //Syatem Size 
        public virtual DateTime? InstallationDate { get; set; }

        //Installaer Details
        public virtual decimal? TotalSystemCost { get; set; }    // Actual Price

        public virtual decimal? PaidToDate { get; set; }

        public virtual decimal? BalanceOwing { get; set; }

        public virtual string InstallerName { get; set; }
        public virtual string InstallerMobile { get; set; }
        public virtual string InstallerNote { get; set; }
        public virtual string PickedBy { get; set; }
        public virtual DateTime? PickedDate { get; set; }        

        public List<object> JobProductItemList { get; set; }

        public List<object> JobPickList { get; set; }

        public object OrganizationDetails { get; set; }

        public List<object> OrganizationbankDetails { get; set; }
    }
}
