﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.PickListTracker.Dto
{
    public class GetAllPickListTrackerInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? OrganizationUnit { get; set; }    //organizationFilter            

      
        public List<int> LeadStatusIDS { get; set; }
        public List<int> employeeIdFilter { get; set; }
        public List<int> leadApplicationStatusFilter { get; set; } //application Filter
       
        public string otpPendingFilter { get; set; }

        public string queryRaisedFilter { get; set; }
        public string FilterbyDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? excelorcsv { get; set; }

    }
}
