﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.PickListTracker.Dto
{
    public class PickListTrackerQuickViewDto : EntityDto
    {
        public string ProjectNumber { get; set; }
        public string CustomerName { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }

        public int? AvgmonthlyBill { get; set; }
        public int? AvgmonthlyUnit { get; set; }

        public string DiscomName { get; set; }

        public long? ConsumerNumber { get; set; }

        public string DivisionName { get; set; }

        public string SubDivisionName { get; set; }

        public decimal? SystemSize { get; set; }

        public string MeterPhase { get; set; }

        public string Category { get; set; }

        public string EmailId { get; set; }

        public string MobileNumber { get; set; }   

    }
}
