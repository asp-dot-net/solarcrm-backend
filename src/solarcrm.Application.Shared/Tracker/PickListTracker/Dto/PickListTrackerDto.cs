﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.PickListTracker.Dto
{
    public class PickListTrackerDto : FullAuditedEntity
    {
        public LeadsDto leaddata { get; set; }
        public string ProjectNumber { get; set; }
        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }

        public string SubDivision { get; set; }

        public string JobOwnedBy { get; set; }

        public string LeadStatusName { get; set; }

        public int ApplicationStatus { get; set; }

        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }
        public string LeadStatusColorClass { get; set; }
        public string LeadStatusIconClass { get; set; }
        public string Comment { get; set; }
        public string Notes { get; set; }
        public object ApplicationStatusdata { get; set; }

        public virtual int DispatchJobId { get; set; }
       
        public virtual int? DispatchStockItemId { get; set; }
       
        public virtual int? DispatchHeightStructureId { get; set; }
       
        public virtual int? DispatchTypeId { get; set; }
       
        public virtual DateTime? DispatchDate { get; set; }

        public virtual string DispatchNotes { get; set; }

        public virtual bool IsActive { get; set; }
        public virtual string HeightStructureName { get; set; }
        public virtual string PickListType { get; set; }
        public virtual DateTime? InstallationDate { get; set; }
        public virtual string InstallerName { get; set; }
        public virtual string CreatedBy { get; set; }

    }
}
