﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.PickListTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.PickListTracker
{
    public interface IPickListTrackerAppService
    {
        Task<PagedResultDto<GetPickListTrackerForViewDto>> GetAllPickListTracker(GetAllPickListTrackerInput input);
        Task<PickListQuotationDataDetailsDto> PickListQuotationData(int JobId, int dispatchId, int organizationId);
        Task<GetPickListItemForEditOutput> GetPickLitsForView(EntityDto input);
        Task<GetPickListItemForEditOutput> UpdatePickList(List<CreateOrEditPicklistItemDto> input);
        Task<List<GetPickListTrackerForViewDto>> GetAll(int input);
        //Task<PickListTrackerQuickViewDto> GetPickListQuickView(EntityDto input);

        //Task RefundDetailsEdit(CreateOrEditJobDto input, int SectionId);

        //Task<FileDto> GetPickListTrackerToExcel(GetAllPickListTrackerInput input);
    }
}
