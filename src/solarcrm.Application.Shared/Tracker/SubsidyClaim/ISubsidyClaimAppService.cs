﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.SubsidyClaim.Dto;
using solarcrm.Tracker.SubsidyTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyClaim
{
    public interface ISubsidyClaimAppService
    {
        Task<PagedResultDto<GetSubsidyClaimForViewDto>> GetAllSubsidyClaim(GetAllSubsidyClaimInput input);
        Task<FileDto> GetSubsidyClaimToExcel(GetAllSubsidyTrackerInput input);
    }
}
