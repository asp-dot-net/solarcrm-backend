﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.SubsidyClaim.Dto
{
    public class SubsidyClaimQuickViewDto
    {
        public virtual int SubsidyClaimId { get; set; }
        public virtual string SubsidyClaimNumber { get; set; }
        public virtual string Scheme { get; set; }
        public virtual string DiscomName { get; set; }
        public virtual string CircleName { get; set; }
        public virtual string DivisionName { get; set; }
        public virtual int? ClaimTotalApplications { get; set; }
        public virtual decimal? ClaimRegisterCapacityKW { get; set; }
        public virtual decimal? ClaimInstalledPVModuleCapacityKW { get; set; }
        public virtual decimal? ClaimInstalledInverterCapacityKW { get; set; }
        public virtual decimal? ClaimTotalProjectCost { get; set; }
        public virtual decimal? ClaimTotalSubsidyAmount { get; set; }
        public virtual decimal? ClaimPBGDeduction { get; set; }
        public virtual decimal? ClaimPenaltyDeduction { get; set; }
        public virtual decimal? ClaimAnyotherduesdeduction { get; set; }
        public virtual decimal? ClaimNetSubsidyPayable { get; set; }
        public virtual string ClaimStatus { get; set; }
        public virtual string ClaimVerifiedORnot { get; set; }
        public virtual string ClaimHandOverPerson { get; set; }
        public virtual string Date { get; set; }
        public virtual DateTime? FileSubmitDate { get; set; }
    }
}
