﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.SubsidyClaim.Dto
{
    public class GetAllSubsidyClaimForExcelInput
    {
        public string Filter { get; set; }
    }
}
