﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.SubsidyClaim.Dto
{
    public class GetAllSubsidyClaimInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public int? OrganizationUnit { get; set; }
        public List<int> LeadStatusIDS { get; set; }
        public List<int> employeeIdFilter { get; set; }
        public List<int> leadApplicationStatusFilter { get; set; } //application Filter
        public int? subsidyTrackerFilter { get; set; }
        public int? subsidyOcCopyFilter { get; set; }
        public int? subsidyReceivedFilter { get; set; }
        public string subsidyFileVerifiedFilter { get; set; }
        public List<int> DiscomIdFilter { get; set; }

        public List<int> DivisionIdFilter { get; set; }
        public List<int> solarTypeIdFilter { get; set; }
        public List<int> TenderIdFilter { get; set; }
        public string FilterbyDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? excelorcsv { get; set; }
    }
}
