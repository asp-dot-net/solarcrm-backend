﻿using solarcrm.Tracker.SubsidyTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.SubsidyClaim.Dto
{
    public class GetSubsidyClaimForViewDto
    {
        public SubsidyClaimDto subsidyClaim { get; set; }
        public SubsidyClaimSummaryCount subsidyClaimSummaryCount { get; set; }
    }
    public class SubsidyClaimSummaryCount
    {
        public string TotalSubsidyReceived { get; set; } = "0";
        public string TotalSubsidyInProcess { get; set; } = "0";
        public string TotalProjectsClaimed { get; set; } = "0";

        public string TotalKilowatt { get; set; } = "0";

    }
}
