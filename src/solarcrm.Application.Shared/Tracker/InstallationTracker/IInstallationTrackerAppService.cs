﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.InstallationTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.InstallationTracker
{

    public interface IInstallationTrackerAppService
    {
        Task<PagedResultDto<GetInstallationTrackerForViewDto>> GetAllInstallationTracker(GetAllInstallationTrackerInput input);

        Task<InstallationTrackerQuickViewDto> GetInstallationQuickView(EntityDto input);

        //Task ApplicationDetailsEdit(CreateOrEditJobDto input, int SectionId);

       //Task<FileDto> GetApplicationTrackerToExcel(GetAllInstallationTrackerInput input);
    }
}
