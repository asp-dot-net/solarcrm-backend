﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.InstallationTracker.Dto
{
    public class InstallationTrackerQuickViewDto : EntityDto
    {
        public string ProjectNumber { get; set; }
        public string CustomerName { get; set; }

    }
}
