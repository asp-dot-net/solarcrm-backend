﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.InstallationTracker.Dto
{
    public class GetAllInstallationTrackerForExcelInput
    {
        public string Filter { get; set; }
    }
}
