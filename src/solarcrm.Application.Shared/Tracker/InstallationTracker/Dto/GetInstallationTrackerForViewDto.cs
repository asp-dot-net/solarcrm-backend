﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.InstallationTracker.Dto
{
    public class GetInstallationTrackerForViewDto
    {
        public InstallationTrackerDto installTracker { get; set; }

        //public List<DocumentListDto> documentListName { get; set; }
        //public List<LeadDocumentDto> leaddocumentList { get; set; }
        public string LeadStatusName { get; set; }

    }
}
