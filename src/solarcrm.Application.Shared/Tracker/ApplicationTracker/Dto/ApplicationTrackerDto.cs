﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.ApplicationTracker.Dto
{
    public class ApplicationTrackerDto : FullAuditedEntity
    {
        
        public int JobId { get; set; }
        public string ProjectNumber { get; set; }

        public LeadsDto leaddata { get; set; }
        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }

        public string SubDivision { get; set; }

        public string JobOwnedBy { get; set; }

        public string LeadStatusName { get; set; }

        public int ApplicationStatus { get; set; }

        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }
      
        public string Comment { get; set; }
        public string Notes { get; set; }

        public string OtpVerifed { get; set; }

        public string ApplicationNumber { get; set; }

        public string GUVNLRegisterationNo { get; set; }

        public DateTime? ApplicationDate { get; set; }

        public string SignApplicationPath { get; set; }

        public string LeadStatusColorClass { get;set; }
        public string LeadStatusIconClass { get; set; }

        public string QuesryDescription { get; set; }
        public object ApplicationStatusdata { get; set; }
        
        public DateTime? ReminderTime { get; set; }
        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }
    }
}
