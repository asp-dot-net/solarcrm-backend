﻿using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.ApplicationTracker.Dto
{
    public class GetApplicationTrackerForViewDto
    {
        public ApplicationTrackerDto appTracker { get; set; }

        public List<DocumentListDto> documentListName { get; set; }
        public List<LeadDocumentDto> leaddocumentList { get; set; }
        public string LeadStatusName { get; set; }
        public ApplicationTrackerSummaryCount applicationTrackerSummaryCount { get; set; }
    }

    public class ApplicationTrackerSummaryCount
    {
        public string Total { get; set; } = "0";
        public string ApplicationReady { get; set; }= "0";
        public string OTP_Pending { get; set; } = "0";
        public string ApplicationPending { get; set; } = "0";
        public string ApplicationSubmited { get; set; } = "0";
        public string FeasiblityApproved { get; set; } = "0";
        public string MeterInstalled { get; set; } = "0";
        public string SubcidyClaimed { get; set; } = "0";
    }   
}
