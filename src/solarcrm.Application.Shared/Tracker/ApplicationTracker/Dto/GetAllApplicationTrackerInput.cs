﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.ApplicationTracker.Dto
{
    public class GetAllApplicationTrackerInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
       
		public int? OrganizationUnit { get; set; }    //organizationFilter            

        //public int? SalesManagerId { get; set; }

        //public int? SalesRepId { get; set; }

        public List<int> LeadStatusIDS { get; set; }
        public List<int> employeeIdFilter { get; set; }
        public List<int> leadApplicationStatusFilter { get; set; } //application Filter
        public int? applicationstatusTrackerFilter { get; set; }

        public string otpPendingFilter { get; set; }

        public string queryRaisedFilter   { get; set; }

        public List<int> DiscomIdFilter { get; set; }

        public List<int> CircleIdFilter { get; set; }

        public List<int> DivisionIdFilter { get; set; }

        public List<int> SubDivisionIdFilter { get; set; }

        public List<int> solarTypeIdFilter { get; set; }
        public List<int> TenderIdFilter { get; set; }
       
        public string FilterbyDate { get; set; }

        //public List<int> JobStatusID { get; set; }

        //public List<int> LeadSourceIdFilter { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int? excelorcsv { get; set; }
    }
}
