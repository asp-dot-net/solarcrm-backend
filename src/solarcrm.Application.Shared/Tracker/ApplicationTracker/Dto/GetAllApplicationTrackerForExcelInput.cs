﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.ApplicationTracker.Dto
{
    public class GetAllApplicationTrackerForExcelInput
    {
        public string Filter { get; set; }
    }
}
