﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using solarcrm.Jobs.Dto;
using solarcrm.Tracker.ApplicationTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.ApplicationTracker
{
    public interface IApplicationTrackerAppService
    {
       
        Task<PagedResultDto<GetApplicationTrackerForViewDto>> GetAllApplicationTracker(GetAllApplicationTrackerInput input);

        Task<ApplicationTrackerQuickViewDto> GetApplicationQuickView(EntityDto input);

        Task ApplicationDetailsEdit(CreateOrEditJobDto input, int SectionId);

        Task<FileDto> GetApplicationTrackerToExcel(GetAllApplicationTrackerInput input);
    }
}
