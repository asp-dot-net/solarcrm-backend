﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using solarcrm.Jobs.Dto;
using solarcrm.Tracker.ApplicationTracker.Dto;
using solarcrm.Tracker.DepositeTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
namespace solarcrm.Tracker.DepositeTracker
{
    public interface IDepositTrackerAppService
    {
        Task<PagedResultDto<GetDepositTrackerForViewDto>> GetAllDepositeTracker(GetAllDepositTrackerInput input);
        Task<FileDto> GetDepositeTrackerToExcel(GetAllDepositTrackerInput input);
        //Task<DepositTrackerQuickViewDto> GetDepositeQuickView(EntityDto input);

        //Task ApplicationDetailsEdit(CreateOrEditDepositeDto input, int SectionId);

        //Task<FileDto> GetDepositeTrackerToExcel(GetAllDepositTrackerInput input);
    }
}
