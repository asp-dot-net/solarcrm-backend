﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DepositeTracker.Dto
{
    public class GetAllDepositTrackerForExcelInput
    {
        public string Filter { get; set; }
    }
}
