﻿using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.DepositeTracker.Dto
{
    public class GetDepositTrackerForViewDto
    {
        public DepositTrackerDto depositTracker { get; set; }

        public List<DocumentListDto> documentListName { get; set; }
        public List<LeadDocumentDto> leaddocumentList { get; set; }
        public string LeadStatusName { get; set; }

    }
}
