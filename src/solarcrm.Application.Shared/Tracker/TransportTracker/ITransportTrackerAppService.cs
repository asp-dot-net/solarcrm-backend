﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.TransportTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.TransportTracker
{
    public interface ITransportTrackerAppService
    {
        Task<PagedResultDto<GetTransportTrackerForViewDto>> GetAllTransportTracker(GetAllTransportTrackerInput input);

        //Task<TransportTrackerQuickViewDto> GetTransportQuickView(EntityDto input);

        Task CreateOrEdit(CreateOrEditTransportDto input);
        // Task TransportDetailsEdit(CreateOrEditTransportDto input, int SectionId);

        Task<FileDto> GetTransportTrackerToExcel(GetAllTransportTrackerInput input);
    }
}
