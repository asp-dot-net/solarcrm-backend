﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.TransportTracker.Dto
{
    public class CreateOrEditTransportDto : EntityDto<int?>
    {
        public virtual int TransportJobId { get; set; }

        public virtual int? VehicalId { get; set; }

        public virtual string DriverName { get; set; }
       
        public virtual DateTime TransportDate { get; set; }
        public virtual DateTime DispatchDate { get; set; }

        public virtual string TransportNote { get; set; }

        //public virtual int? PickListId { get; set; }
        public virtual int leadid { get; set; }
        public virtual int OrgID { get; set; }
        public virtual bool IsActive { get; set; }
        public List<int> DispatchJobList { get; set; }
    }
}
