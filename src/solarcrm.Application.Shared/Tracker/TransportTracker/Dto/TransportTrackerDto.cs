﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.TransportTracker.Dto
{
    public class TransportTrackerDto : FullAuditedEntity
    {

        public int JobId { get; set; }
        public string ProjectNumber { get; set; }

        public LeadsDto leaddata { get; set; }
        public string CustomerName { get; set; }

        public string Address { get; set; }
        public string City { get; set; }

        public string Mobile { get; set; }
        public string LeadStatusName { get; set; }

        public int ApplicationStatus { get; set; }

        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }

        public string Comment { get; set; }
        public string Notes { get; set; }

        public string ApplicationNumber { get; set; }

        public string GUVNLRegisterationNo { get; set; }

        public DateTime? DispatchDate { get; set; }
        public DateTime? TransportDate { get; set; }

        public string VehicalType { get; set; }
        public string Regonumber { get; set; }
        public string DriverName { get; set; }

        public DateTime? ReminderTime { get; set; }
        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }
    }
}
