﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.TransportTracker.Dto
{
    public class GetTransportTrackerForViewDto
    {
        public TransportTrackerDto transportTracker { get; set; }

       // public List<DocumentListDto> documentListName { get; set; }
       // public List<LeadDocumentDto> leaddocumentList { get; set; }
        public string LeadStatusName { get; set; }
        public TransportTrackerSummaryCount transportTrackerSummaryCount { get; set; }
    }
    public class TransportTrackerSummaryCount
    {
        public string TotalPanel { get; set; } = "0";
        public string InstallationComplet { get; set; } = "0";
        public string TotalKilowatt { get; set; } = "0";

    }
}
