﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.TransportTracker.Dto
{
    public class GetAllTransportTrackerForExcelInput
    {
        public string Filter { get; set; }
    }
}
