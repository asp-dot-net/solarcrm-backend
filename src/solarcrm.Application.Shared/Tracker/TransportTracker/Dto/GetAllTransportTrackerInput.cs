﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.TransportTracker.Dto
{
    public class GetAllTransportTrackerInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? OrganizationUnit { get; set; }    //organizationFilter            

        public string BillTypeFilter { get; set; }

        public List<int> VehicalType { get; set; }

        public List<int> DiscomIdFilter { get; set; }

        public int RevertDispatch { get; set; }

        public List<int> solarTypeIdFilter { get; set; }
        public List<int> TenderIdFilter { get; set; }

        public string FilterbyDate { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public int? excelorcsv { get; set; }

    }
}
