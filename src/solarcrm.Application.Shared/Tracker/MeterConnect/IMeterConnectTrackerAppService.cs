﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.MeterConnect.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.MeterConnect
{
    public interface IMeterConnectTrackerAppService
    {
        Task<PagedResultDto<GetMeterConnectTrackerForViewDto>> GetAllMeterConnectTracker(GetAllMeterConnectTrackerInput input);
        Task<GetMeterConnectDetailsForEditOutput> GetMeterConnectForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditMeterConnectDto input);
        Task<SelfCertificateDetailDto> SelfCertficateDetails(EntityDto input, int organizationId);
        //Task<MeterConnectTrackerQuickViewDto> GetMeterConnectQuickView(EntityDto input);

        //Task ApplicationDetailsEdit(CreateOrEditMeterConnectDto input, int SectionId);

        //Task<FileDto> GetMeterConnectTrackerToExcel(GetAllMeterConnectTrackerInput input);
    }
}
