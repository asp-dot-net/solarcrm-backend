﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class MeterConnectTrackerDto : FullAuditedEntity
    {
        public int JobId { get; set; }
        public string ProjectNumber { get; set; }
        public LeadsDto leaddata { get; set; }
        public string CustomerName { get; set; }
        public string LeadStatusName { get; set; }
        public int ApplicationStatus { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public decimal? KiloWatt { get; set; }
        public int? InstallationId { get; set; }
        public DateTime? InstallationDate { get; set; }
        public string SubDivision { get; set; }
        public string Discom { get; set; }
        public string ConsumerNo { get; set; }
        public DateTime? FollowDate { get; set; }
        public string FollowDiscription { get; set; }
        public int? PenaltyDays { get; set; }
        public string Comment { get; set; }        
        public string LeadStatusColorClass { get; set; }
        public string LeadStatusIconClass { get; set; }
        public object ApplicationStatusdata { get; set; }
    }
    
}
