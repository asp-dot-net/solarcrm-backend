﻿using solarcrm.Tracker.TransportTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class GetMeterConnectTrackerForViewDto
    {
        public MeterConnectTrackerDto meterconnectTracker { get; set; }
        public MeterConnectTrackerSummaryCount meterConnectTrackerSummaryCount { get; set; }
       
    }
    public class MeterConnectTrackerSummaryCount
    {
        public string Total { get; set; } = "0";
        public string FeasiblityApproved { get; set; } = "0";
        public string InstalltionCompleted { get; set; } = "0";
        public string MeterfileSent { get; set; }
        public string MeterfileSubmitted { get; set; }
    }
}
