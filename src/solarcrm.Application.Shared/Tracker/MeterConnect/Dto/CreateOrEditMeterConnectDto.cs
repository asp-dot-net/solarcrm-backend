﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class CreateOrEditMeterConnectDto : EntityDto<int>
    {
        public virtual int InstallationId { get; set; }
        public virtual string MeterAgreementGivenTo { get; set; }
        public virtual string MeterAgreementNo { get; set; }
        public virtual DateTime? MeterAgreementSentDate { get; set; }
        public virtual int? MeterApplicationReadyStatus { get; set; }
        public virtual DateTime? MeterSubmissionDate { get; set; }
        public virtual int leadid { get; set; }
        public virtual int? OrgID { get; set; }
    }
}
