﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class MeterConnectTrackerQuickViewDto : EntityDto
    {
        public string ProjectNumber { get; set; }
        public string CustomerName { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }

        public int? AvgmonthlyBill { get; set; }
        public int? AvgmonthlyUnit { get; set; }

        public string DiscomName { get; set; }

        public long? ConsumerNumber { get; set; }

        public string DivisionName { get; set; }

        public string SubDivisionName { get; set; }

        public decimal? SystemSize { get; set; }

        public string MeterPhase { get; set; }

        public string Category { get; set; }

        public string EmailId { get; set; }

        public string MobileNumber { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string TalukaName { get; set; }

        public string DistrictName { get; set; }

        public string CityName { get; set; }

        public string StateName { get; set; }

        public int? Pincode { get; set; }

        public bool CommonMeter { get; set; }
    }
}
