﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class SelfCertificateDetailDto
    {
        public int Id { get; set; }
        public long? ConsumerNumber { get; set; }
        public string DiscomName { get; set; }
        public DateTime? CertificateDate { get; set; }
        public string AgencyName { get; set; }
        public string GUVNLnumber { get; set; }
        public string Customername { get; set; }
        public string CircleName { get; set; }
        public string DivisionName { get; set; }
        public string SubDivisionName { get; set; }
        public string Customeraddress1 { get; set; }
        public string Customeraddress2 { get; set; }
        public decimal? ContractedLoadKW { get; set;}
        public string ContractPhase { get; set;}
        public decimal? GUVNLPortalCapacity { get; set; }
        public string GUVNLPortalPhase { get; set; }

        //PV Module Specification:
        public string SolarPVModuleMake { get; set; }
        public string PVModelNo { get; set; }
        public string TypeOfPVModule { get; set; }
        public decimal? RatedCapacityofSolarModuleinwattmorethan250wp { get; set; }
        public decimal? PVNoofModules { get; set; }
        public decimal? TotalPVCapacityinstalledinKwp { get; set; }
        //
        public string ModuleofIndiaMake { get; set; }
        //Inverter Details
        public string MakeOfInverter { get; set; }
        public string IVModelNo { get; set; }
        public string TypeOfInverter { get; set; }
        public decimal? RatedACoutputofInverterKiloWatt { get; set; }
        public string SerialNoofInverter { get; set; }
        public decimal? RemoteMoniteringSystem { get; set; }
        public string SimMobileNumber { get; set; }
    }
}
