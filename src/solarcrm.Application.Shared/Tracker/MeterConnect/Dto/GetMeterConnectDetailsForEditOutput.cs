﻿using solarcrm.Tracker.DispatchTracker.Dto.DispatchStockTransfer;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class GetMeterConnectDetailsForEditOutput
    {
        public CreateOrEditMeterConnectDto meterConnectDto { get; set; }
    }
}
