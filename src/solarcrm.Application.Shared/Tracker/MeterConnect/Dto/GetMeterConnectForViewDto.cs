﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class GetMeterConnectForViewDto
    {
        public MeterConnectDto meterConnect { get; set; }
    }
}
