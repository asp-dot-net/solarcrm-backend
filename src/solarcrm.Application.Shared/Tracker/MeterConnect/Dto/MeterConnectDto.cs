﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class MeterConnectDto : EntityDto
    {
        public virtual int InstallationId { get; set; }
        public virtual string MeterAgreementGivenTo { get; set; }
        public virtual string MeterAgreementNo { get; set; }
        public virtual DateTime? MeterAgreementSentDate { get; set; }
        public virtual int? MeterApplicationReadyStatus { get; set; }
        public virtual DateTime? MeterSubmissionDate { get; set; }
    }
}
