﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.MeterConnect.Dto
{
    public class GetAllMeterConnectTrackerForExcelInput
    {
        public string Filter { get; set; }
    }
}
