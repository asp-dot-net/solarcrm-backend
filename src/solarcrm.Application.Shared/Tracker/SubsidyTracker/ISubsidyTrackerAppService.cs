﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Tracker.SubsidyTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Tracker.SubsidyTracker
{
    public interface ISubsidyTrackerAppService
    {
        Task<PagedResultDto<GetSubsidyTrackerForViewDto>> GetAllSubsidyTracker(GetAllSubsidyTrackerInput input);
        Task<FileDto> GetSubsidyTrackerToExcel(GetAllSubsidyTrackerInput input);
    }
}
