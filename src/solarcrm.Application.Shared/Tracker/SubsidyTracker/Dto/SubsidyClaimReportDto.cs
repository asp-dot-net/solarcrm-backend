﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.Tracker.SubsidyTracker.Dto
{
    public class SubsidyClaimReportDto : FullAuditedEntity
    {        
        public virtual int SubsidyDivisionId { get; set; }
        public virtual string SubsidyClaimNo { get; set; }
        public virtual string SubsidyNote { get; set; }
        public virtual DateTime? SubcidyClaimDate { get; set; }
        public virtual bool IsSubcidyFileVerify { get; set; }
        public virtual bool IsSubcidyReceived { get; set; }       
        public virtual string OCCopyPath { get; set; }
        public virtual DateTime? ClaimFileSubmissionDate { get; set; }       
        public virtual string SubcidyReceiptsPath { get; set; }
    }
}
