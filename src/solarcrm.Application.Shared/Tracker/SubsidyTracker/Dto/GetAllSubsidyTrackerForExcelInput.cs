﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.SubsidyTracker.Dto
{
    public class GetAllSubsidyTrackerForExcelInput
    {
        public string Filter { get; set; }
    }
}
