﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.SubsidyTracker.Dto
{
    public class SubsidyTrackerDto : FullAuditedEntity
    {   
        public object leaddata { get; set; }
        public string ClaimNumber { get; set; }
        public string Division { get; set;}
        public decimal? TotalProjectCost { get; set;}
        public decimal? SubsidyAmount { get; set;}
        public decimal? NetPayableAmount { get;set;}
        public decimal? BankableAmount { get; set; }       
        public decimal? ActualAmountRec { get; set;}
        public bool SubcidyFileVerify { get; set;}
        public string OCCopy { get; set; }
        public bool SubcidyReceived { get; set; }
        public string SubcidyReceipts { get; set; }
    }
}
