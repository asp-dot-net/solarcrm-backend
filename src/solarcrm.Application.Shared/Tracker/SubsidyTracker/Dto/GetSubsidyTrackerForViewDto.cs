﻿using solarcrm.Tracker.RefundTracker.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Tracker.SubsidyTracker.Dto
{
    public class GetSubsidyTrackerForViewDto
    {
        public SubsidyTrackerDto subsidyTracker { get; set; }
        public SubsidyTrackerSummaryCount subsidyTrackerSummaryCount { get; set; }
    }
    public class SubsidyTrackerSummaryCount
    {
        public string TotalSubsidyReceived { get; set; } = "0";
        public string TotalSubsidyInProcess { get; set; } = "0";
        public string TotalProjectsClaimed { get; set; } = "0";

        public string TotalKilowatt { get; set; } = "0";

    }
}
