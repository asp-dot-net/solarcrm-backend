﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace solarcrm.Tracker.SubsidyTracker.Dto
{
    public class SubsidyClaimProjectDto : FullAuditedEntity
    {
        public virtual long SubsidyClaimId { get; set; }       
        public virtual long SubsidyJobId { get; set; }       
    }
}
