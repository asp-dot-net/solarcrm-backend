﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Common.Dto
{
    public class DepartmentForDropdownDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}
