﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Common.Dto
{
    public class GetLocationNameOutput
    {
        public string Name { get; set; }
    }
}
