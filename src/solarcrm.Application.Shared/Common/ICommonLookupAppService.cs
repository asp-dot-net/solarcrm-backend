﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.Authorization.Users.Dto;
using solarcrm.Common.Dto;
using solarcrm.Dto;
using solarcrm.Editions.Dto;
using solarcrm.Organizations.Dto;

namespace solarcrm.Common
{
    public interface ICommonLookupAppService : IApplicationService
    {
        Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false);

        Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input);

        GetDefaultEditionNameOutput GetDefaultEditionName();

        //Task<List<OrganizationUnitDto>> GetOrganizationUnit();
        Task<List<CommonLookupDto>> GetAllStateForDropdown(int id);

        Task<List<CommonLookupDto>> GetAllDistrictForDropdown(int id);

        Task<List<CommonLookupDto>> GetAllTalukaForDropdown(int[] id);

        //Task<List<CommonLookupDto>> GetAllCityForDropdown(int id);

        Task<List<CommonLookupDto>> GetAllSolarTypeForDropdown();

        Task<List<CommonLookupDto>> GetAllHeightStructureForDropdown();

        Task<List<CommonLookupDto>> GetAllSubdivisionWiseData(string input);
       

        Task<List<CommonLookupDto>> GetAllDivisionFilterDropdown(int id);

        Task<List<CommonLookupDto>> GetAllDiscomFilterDropdown(int id);

        Task<List<CommonLookupDto>> GetAllUserTypes();

        Task<List<CommonLookupDto>> GetDistrictByState(int id);

        Task<List<CommonLookupDto>> GetTalukaByDistrict(int id);
        
        Task<List<CommonLookupDto>> GetCityByTaluka(int id);

        Task<List<CommonLookupDto>> GetEmployees(int id);

        Task<List<CommonLookupDto>> GetDiscomForDropdown();

        Task<List<CommonLookupDto>> getDepartmentForDropdown();

        Task<List<CommonLookupDto>> GetEmployeesByRole(string RoleName);

        Task<List<CommonLookupDto>> GetCircleForDropdown();
        DocumentReturnValuesDto SaveCommonDocument(byte[] ByteArray, string filename, string SectionName, int? JobId, int? DocumentTypeId, int? TenantId);

    }
}