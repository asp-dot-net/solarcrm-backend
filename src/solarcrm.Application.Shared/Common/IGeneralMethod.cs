﻿using solarcrm.DataVaults.LeadsActivityLog.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Common
{
    public interface IGeneralMethod
    {
        Task SendEmail(ActivityLogInput input);
        Task Sendsms(ActivityLogInput input);
    }
}
