﻿using solarcrm.EmailSetup.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.EmailSetup
{
    public interface IEmailAppService
    {
       // void Send(EmailMessage emailMessage);
        Task<List<object>> ReceiveEmail(int maxCount = 10);

        Task<string> ReceiveEmailBody(uint index);

        Task<string> SendComposeEmail(EmailMessage emailMessage);
    }
}
