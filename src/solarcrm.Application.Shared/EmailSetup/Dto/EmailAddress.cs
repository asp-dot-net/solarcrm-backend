﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.EmailSetup.Dto
{
    public class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
