﻿using Abp.Application.Services;
using solarcrm.DocumentRequests.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DocumentRequests
{
    public interface IDocumentRequestAppService : IApplicationService
    {
        Task SendDocumentRequestForm(SendDocumentRequestInputDto input);

        Task<DocumentRequestDto> DocumentRequestData(string STR);
    }
}
