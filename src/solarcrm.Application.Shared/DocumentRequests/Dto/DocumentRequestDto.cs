﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DocumentRequests.Dto
{
    public class DocumentRequestDto
    {
        public bool Expiry { get; set; }

        public bool IsSubmitted { get; set; }

        public string DocumnetType { get; set; }

        public string RequestedBy { get; set; }
    }
}
