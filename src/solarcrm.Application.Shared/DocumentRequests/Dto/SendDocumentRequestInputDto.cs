﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DocumentRequests.Dto
{
    public class SendDocumentRequestInputDto
    {
        public int LeadId { get; set; }

        public int SectionId { get; set; }

        public int DocTypeId { get; set; }

        public string SendMode { get; set; }
    }
}
