﻿namespace solarcrm.Entity
{
    public class DynamicEntityPropertyGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}
