﻿namespace solarcrm.Entity.Dto
{
    public class GetAllEntitiesHasDynamicPropertyOutput
    {
        public string EntityFullName { get; set; }
    }
}