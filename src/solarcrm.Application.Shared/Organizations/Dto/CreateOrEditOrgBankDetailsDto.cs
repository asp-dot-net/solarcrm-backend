﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Organizations.Dto
{
    public class CreateOrEditOrgBankDetailsDto:EntityDto<int?>
    {
        public virtual long BankDetailOrganizationId { get; set; }

        public virtual string OrgBankName { get; set; }

        public virtual string OrgBankBrachName { get; set; }

        public virtual int? OrgBankAccountNumber { get; set; }

        public virtual string OrgBankAccountHolderName { get; set; }


        public virtual string OrgBankIFSC_Code { get; set; }
        public virtual string OrgBankQrCode_path { get; set; }

        public virtual int? TenantId { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
