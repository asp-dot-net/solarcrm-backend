﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Organizations;

namespace solarcrm.Organizations.Dto
{
    public class CreateOrganizationUnitInput
    {
        //[Range(1, long.MaxValue)]
        //public long Id { get; set; }

        public long? ParentId { get; set; }

        [Required]
        [StringLength(OrganizationUnit.MaxDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        public string OrganizationCode { get; set; }

        public string ProjectId { get; set; }
      
        public string Organization_Address { get; set; }

        public string Organization_ContactNo { get; set; }
      
        public string Organization_defaultFromAddress { get; set; }
        public string Organization_defaultFromDisplayName { get; set; }
        public string Organization_Website { get; set; }    
        public string Organization_Mobile { get; set; }
        public string Organization_Email { get; set; }
        public string FileTokenOrg { get; set; }
        public string FileTokenQrCode { get; set; }
        public string Organization_LogoFileName { get; set; }
        public string Organization_LogoFilePath { get; set; }

        public string Organization_QrCodeFileName { get; set; }
        public string Organization_QrCodeFilePath { get; set; }

        public string paymentMethod { get; set; }
        public string cardNumber { get; set; }
        public string expiryDateMonth { get; set; }
        public string expiryDateYear { get; set; }
        public string cvn { get; set; }
        public string cardholderName { get; set; }
        public string MerchantId { get; set; }
        public string PublishKey { get; set; }
        public string SecrateKey { get; set; }
       
        public string OrganizationGSTNumber { get; set; }

        public string OrganizationPANNumber { get; set; }

        public string OrganizationCINNumber { get; set; }
        public string SMS_authorizationKey { get; set; }
        public string sms_clientId { get; set; }

        public string sms_senderId { get; set; }

        public string sms_msgType { get; set; }

        public List<CreateOrEditOrgBankDetailsDto> OrgBankDetails { get; set; }
    }
}