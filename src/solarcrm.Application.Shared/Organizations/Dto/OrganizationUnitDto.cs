using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace solarcrm.Organizations.Dto
{
    public class OrganizationUnitDto : AuditedEntityDto<long>
    {
        public long? ParentId { get; set; }

        public string Code { get; set; }

        public string DisplayName { get; set; }

        public int MemberCount { get; set; }
        
        public int RoleCount { get; set; }

        public string OrganizationCode { get; set; }

        public string ProjectId { get; set; }

        public string GSTNumber { get; set; }

        public string PANNumber { get; set; }

        public string CINNumber { get; set; }
        public string Logo { get; set; }       

        public string Address { get; set; }

        public string ContactNo { get; set; }
     
        public string defaultFromAddress { get; set; }
        public string defaultFromDisplayName { get; set; }
    
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string LogoFileName { get; set; }
        public string LogoFilePath { get; set; }


        public string SMS_authorizationKey { get; set; }
        public string sms_clientId { get; set; }

        public string sms_senderId { get; set; }

        public string sms_msgType { get; set; }
        public string paymentMethod { get; set; }
        public string cardNumber { get; set; }
        public string expiryDateMonth { get; set; }
        public string expiryDateYear { get; set; }
        public string cvn { get; set; }
        public string cardholderName { get; set; }

        public string MerchantId { get; set; }
        public string PublishKey { get; set; }
        public string SecrateKey { get; set; }
        public string website { get; set; }


        public List<CreateOrEditOrgBankDetailsDto> OrgBankList { get; set; }

        //CreateOrEditOrgBankDetailsDto


    }
}