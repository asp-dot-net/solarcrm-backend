﻿using Abp.Application.Services.Dto;
using solarcrm.JobRefund.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.JobRefund
{
    public interface IJobRefundAppService
    {
        Task<List<GetJobRefundForViewDto>> GetAll(GetAllJobRefundInput input);
        Task<GetJobRefundForViewDto> GetJobRefundForView(int id);
        Task<GetJobRefundForEditOutput> GetJobRefundForEdit(EntityDto input);       
        Task CreateOrEdit(CreateOrEditJobRefundDto input);
    }
}
