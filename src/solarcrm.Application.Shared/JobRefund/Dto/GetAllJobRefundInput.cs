﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobRefund.Dto
{
    public class GetAllJobRefundInput 
    {
        //public string Filter { get; set; }
        public int JobId { get; set; }
    }
}
