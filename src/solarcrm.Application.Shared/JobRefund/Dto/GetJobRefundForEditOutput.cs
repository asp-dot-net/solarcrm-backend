﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobRefund.Dto
{
    public class GetJobRefundForEditOutput
    {
        public CreateOrEditJobRefundDto JobRefunds { get; set; }
        //public virtual int RefundJobId { get; set; }
        //public virtual int RefundReasonId { get; set; }
        //public virtual int PaymentTypeId { get; set; }
        //public virtual decimal RefundAmount { get; set; }
        //public virtual DateTime? RefundEnterDate { get; set; }
        //public virtual string RefundEnterNotes { get; set; }
        //public virtual bool IsRefundVerify { get; set; }
        //public virtual DateTime? RefundVerifyDate { get; set; }
        //public virtual string RefundVerifyNotes { get; set; }
        //public virtual int? RefundVerifyById { get; set; }
        //public virtual long? BankAccountNumber { get; set; }
        //public virtual string BankAccountHolderName { get; set; }
        //public virtual string BankBrachName { get; set; }
        //public virtual string BankName { get; set; }
        //public virtual string IFSC_Code { get; set; }
        //public virtual string RefundRecPath { get; set; }
    }
}
