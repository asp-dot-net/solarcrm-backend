﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobRefund.Dto
{
    public class CreateOrEditJobRefundDto : EntityDto<int>
    {
        public virtual int RefundJobId { get; set; }
        
        public virtual int RefundReasonId { get; set; }
        
        public virtual int PaymentTypeId { get; set; }
       
        public virtual decimal RefundPaidAmount { get; set; }
        public virtual DateTime? RefundPaidDate { get; set; }
        
        public virtual string RefundPaidNotes { get; set; }
        public virtual bool IsRefundVerify { get; set; }
        public virtual DateTime? RefundVerifyDate { get; set; }
        public virtual string RefundVerifyNotes { get; set; }

        public virtual int? RefundVerifyById { get; set; }
        public virtual long? BankAccountNumber { get; set; }
        public virtual string BankAccountHolderName { get; set; }
        public virtual string BankBrachName { get; set; }
        public virtual string BankName { get; set; }

        public virtual string IFSC_Code { get; set; }
        public virtual string RefundRecPath { get; set; }
        public virtual string ReceiptNo { get; set; }
        public virtual bool? JobRefundEmailSend { get; set; }
        public virtual DateTime? JobRefundEmailSendDate { get; set; }
        public virtual bool JobRefundSmsSend { get; set; }
        public virtual DateTime? JobRefundSmsSendDate { get; set; }
        public virtual int leadid { get; set; }
        public virtual int OrgID { get; set; }

    }
}
