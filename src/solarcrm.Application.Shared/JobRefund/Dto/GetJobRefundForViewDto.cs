﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobRefund.Dto
{
    public class GetJobRefundForViewDto
    {
        public JobRefundDto jobRefund { get; set; }
    }
}
