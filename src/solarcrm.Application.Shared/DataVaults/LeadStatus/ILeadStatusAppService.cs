﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.LeadStatus.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadStatus
{
    public interface ILeadStatusAppService
    {
        Task<PagedResultDto<GetLeadStatusForViewDto>> GetAll(GetAllLeadStatusInput input);

        Task<GetLeadStatusForViewDto> GetLeadStatusForView(int id);

        Task<GetLeadStatusForEditOutput> GetLeadStatusForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLeadStatusDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetLeadStatusToExcel(GetAllLeadStatusForExcelInput input);
    }
}
