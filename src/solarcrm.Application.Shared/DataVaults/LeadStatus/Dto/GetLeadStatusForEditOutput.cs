﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadStatus.Dto
{
    public class GetLeadStatusForEditOutput
    {
        public CreateOrEditLeadStatusDto leadstatus { get; set; }
    }
}
