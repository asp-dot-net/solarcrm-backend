﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadStatus.Dto
{
    public class GetAllLeadStatusForExcelInput
    {
        public string Filter { get; set; }
    }
}
