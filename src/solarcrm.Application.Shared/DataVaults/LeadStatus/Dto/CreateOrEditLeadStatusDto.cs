﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.LeadStatus.Dto
{
    public class CreateOrEditLeadStatusDto : EntityDto<int?>
    {
        [Required]
        [StringLength(StateConsts.MaxNameLength, MinimumLength = StateConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
