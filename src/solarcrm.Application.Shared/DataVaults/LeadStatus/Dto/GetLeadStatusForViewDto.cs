﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadStatus.Dto
{
    public class GetLeadStatusForViewDto
    {
        public LeadStatusDto leadStatus { get; set; }
    }
}
