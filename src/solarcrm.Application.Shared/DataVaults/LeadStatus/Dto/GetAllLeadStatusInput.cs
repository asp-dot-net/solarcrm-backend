﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadStatus.Dto
{
    public class GetAllLeadStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
