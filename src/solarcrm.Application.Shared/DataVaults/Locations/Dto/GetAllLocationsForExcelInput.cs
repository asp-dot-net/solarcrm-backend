﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.Locations.Dto
{
    public class GetAllLocationsForExcelInput
    {
        public string Filter { get; set; }
    }
}
