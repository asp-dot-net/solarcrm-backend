﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.Locations.Dto
{
    public class GetAllLocationsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
