﻿using Abp.Domain.Entities;

namespace solarcrm.DataVaults.Locations.Dto
{
    public class GetAllLocationForDropdown : Abp.Domain.Entities.Entity
    {
        public string Name { get; set; }
    }
}
