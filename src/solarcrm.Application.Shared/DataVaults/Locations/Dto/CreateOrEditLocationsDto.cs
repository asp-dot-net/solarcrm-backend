﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.Locations.Dto
{
    public class CreateOrEditLocationsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(LocationConsts.MaxNameLength, MinimumLength = LocationConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
