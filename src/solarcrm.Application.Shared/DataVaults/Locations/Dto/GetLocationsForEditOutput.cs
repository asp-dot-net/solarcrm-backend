﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Locations.Dto
{
    public class GetLocationsForEditOutput
    {
        public CreateOrEditLocationsDto Location { get; set; }
    }
}
