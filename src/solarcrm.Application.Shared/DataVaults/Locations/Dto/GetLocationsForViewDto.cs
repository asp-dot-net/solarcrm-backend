﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Locations.Dto
{
    public class GetLocationsForViewDto
    {
        public LocationsDto Location { get; set; }
    }
}
