﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.Locations.Dto
{
    public class LocationsDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
