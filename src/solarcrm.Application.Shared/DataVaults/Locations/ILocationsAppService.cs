﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Locations.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Locations
{
    public interface ILocationsAppService
    {
        Task<PagedResultDto<GetLocationsForViewDto>> GetAll(GetAllLocationsInput input);

        Task<GetLocationsForViewDto> GetLocationForView(int id);

        Task<GetLocationsForEditOutput> GetLocationForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLocationsDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetLocationToExcel(GetAllLocationsForExcelInput input);

        
    }
}
