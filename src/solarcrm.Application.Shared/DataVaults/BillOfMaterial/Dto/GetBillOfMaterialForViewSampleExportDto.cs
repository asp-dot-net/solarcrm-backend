﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class GetBillOfMaterialForViewSampleExportDto : EntityDto<int?>
    {
        public BillOfMaterialSampleDto BillOfMaterialsample { get; set; }
    }
}
