﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class BillOfMaterialDto : EntityDto<int?>
    {
        public virtual string MaterialName { get; set; }        
        public virtual string HeightStructure { get; set; }
        public virtual int? Panel_S { get; set; }
        public virtual int? Panel_E { get; set; }
        public virtual int? Quantity { get; set; }
        public virtual bool IsActive { get; set; }
        public int? TenantId { get; set; }
    }
}
