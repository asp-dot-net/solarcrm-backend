﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class GetBillOfMaterialForViewDto
    {
        public BillOfMaterialDto BillOfMaterialItem { get; set; }
    }
}
