﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class GetAllBillOfMaterialInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
