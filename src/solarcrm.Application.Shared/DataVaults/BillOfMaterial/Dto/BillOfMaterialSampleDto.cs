﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class BillOfMaterialSampleDto
    {
        public virtual string StockItem { get; set; }
        public virtual string Panel_S { get; set; }
        public virtual string Panel_E { get; set; }
        public virtual List<string> HeightOfStructure { get; set; }
    }
}
