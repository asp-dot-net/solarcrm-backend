﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class GetAllBillOfMaterialForExcelInput
    {
        public string Filter { get; set; }
    }
}
