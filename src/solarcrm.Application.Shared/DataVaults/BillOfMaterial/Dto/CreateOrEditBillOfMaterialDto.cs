﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class CreateOrEditBillOfMaterialDto : EntityDto<int?>
    {
        public virtual int StockItemId { get; set; }
        public virtual int HeightStructureId { get; set; }
        public virtual int? Panel_S { get; set; }
        public virtual int? Panel_E { get; set; }
        public virtual int? Quantity { get; set; }
        public virtual bool Active { get; set; }
        public int? TenantId { get; set; }
    }
}
