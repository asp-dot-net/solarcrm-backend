﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.BillOfMaterial.Dto
{
    public class GetBillOfMaterialForEditOutput
    {
        public CreateOrEditBillOfMaterialDto BillOfMaterialItem { get; set; }
    }
}
