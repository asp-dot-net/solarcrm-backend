﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial
{
    public interface IBillOfMaterialAppService
    {
        Task<PagedResultDto<GetBillOfMaterialForViewDto>> GetAll(GetAllBillOfMaterialInput input);

        //Task<GetStockCategoryForViewDto> GetStockCategoryForView(int id);

        Task<GetBillOfMaterialForEditOutput> GetBillOfMaterialForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditBillOfMaterialDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetBillOfMaterialToExcel(GetAllBillOfMaterialForExcelInput input); //ExportToSampleFile
        //Task<FileDto> GetBillOfMaterialToExcelSample(GetAllBillOfMaterialForExcelInput input);
    }
}
