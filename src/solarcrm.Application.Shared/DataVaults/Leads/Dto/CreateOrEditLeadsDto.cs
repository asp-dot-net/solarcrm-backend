﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class CreateOrEditLeadsDto : EntityDto<int?>
    {
       

        [Required]
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public string FirstName { get; set; }

       
        public string MiddleName { get; set; }

        //[Required]
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public string LastName { get; set; }

        //[Required]      
        public string CustomerName { get; set; }

        [Required]
        public string MobileNumber { get; set; }

       
        public string EmailId { get; set; }

        
        public string Alt_Phone { get; set; }

       
        public string AddressLine1 { get; set; }

        
        public string AddressLine2 { get; set; }

       
        public int? StateId { get; set; }

       
        public int? DistrictId { get; set; }
       
        public int? TalukaId { get; set; }

        public int? CityId { get; set; }

       
        public int? Pincode { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public int? LeadTypeId { get; set; }

        public int LeadSourceId { get; set; }

        public int? LeadStatusId { get; set; }
        public int? SolarTypeId { get; set; }
        public bool CommonMeter { get; set; }

        public long? ConsumerNumber { get; set; }

        public int? AvgmonthlyUnit { get; set; }
        public int? AvgmonthlyBill { get; set; }

        public int? DiscomId { get; set; }
        public int? DivisionId { get; set; }

        public int? SubDivisionId { get; set; }

        public int? CircleId { get; set; }
        public int? RequiredCapacity { get; set; }

        public int? Strctureammount { get; set; }
        public int? HeightStructureId { get; set; }

        public string Category { get; set; }

        public int? ChanelPartnerId { get; set; }

        public long? BankAccountNumber { get; set; }

        public string BankAccountHolderName { get; set; }

        public string BankName { get; set; }
        public string BankBrachName { get; set; }
        public string IFSC_Code { get; set; }
        public virtual bool? IsDuplicate { get; set; }

        public virtual bool? IsWebDuplicate { get; set; }
        public string Notes { get; set; }
        public bool IsActive { get; set; }
        public virtual bool IsVerified { get; set; }
        public virtual int? TenantId { get; set; }
        public virtual int OrganizationUnitId { get; set; }

        public string AreaType { get; set; }
        public int? DublicateLeadId { get; set; }
    }
}
