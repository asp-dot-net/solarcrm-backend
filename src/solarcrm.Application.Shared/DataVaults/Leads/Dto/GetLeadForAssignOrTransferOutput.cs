﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetLeadForAssignOrTransferOutput : EntityDto<int?>
	{
		public string FirstName { get; set; }

		public int? LeadStatusID { get; set; }

		public int? OrganizationID { get; set; }

		public List<int> LeadIds { get; set; }

		public int? AssignToUserID { get; set; }

		public string UserName { get; set; }
	}
}
