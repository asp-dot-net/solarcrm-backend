﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class ImportLeadsFromExcelJobArgs
    {
        public int? TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }

        public int OrganizationId { get; set; }
    }
}
