﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class CheckExistLeadDto : EntityDto<int?>
    {
        public string Email { get; set; }

        public string Mobile { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public int? State { get; set; }

        public int? DistrictId { get; set; }

        public int? CityId { get; set; }

        public int? TalukaId { get; set; }

        public int? Pincode { get; set; }

        public int OrganizationId { get; set; }
    }
}
