﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class DuplicateLeadDto : FullAuditedEntity
	{
		public string CustomerName { get; set; }

		public string Email { get; set; }

		public string Phone { get; set; }

		public string Mobile { get; set; }

		public string Address { get; set; }

		public string Requirements { get; set; }


		public string Suburb { get; set; }

		public string State { get; set; }

		public string PostCode { get; set; }

		public string LeadSource { get; set; }


		public string UnitNo { get; set; }

		public string UnitType { get; set; }

		public string StreetNo { get; set; }

		public string StreetName { get; set; }

		public string StreetType { get; set; }

		public int? LeadStatusID { get; set; }

		public int? AssignToUserID { get; set; }

		public bool? IsDuplicate { get; set; }


		public string AltPhone { get; set; }

		public string Type { get; set; }

		public string SolarType { get; set; }


		public string Area { get; set; }

		public string Country { get; set; }

		public string ABN { get; set; }

		public string Fax { get; set; }

		public string SystemType { get; set; }

		public string RoofType { get; set; }

		public string AngleType { get; set; }

		public string StoryType { get; set; }

		public string HouseAgeType { get; set; }

		public virtual int? LeadSourceId { get; set; }

		public virtual int? SuburbId { get; set; }

		public virtual int StateId { get; set; }

		public virtual int? PostalStateId { get; set; }

		public virtual int? PostalSuburbId { get; set; }

		public virtual int IsExternalLead { get; set; }

		public virtual int OrganizationId { get; set; }

		public string OrganizationName { get; set; }
		public string CurrentLeadOwner { get; set; }
	}
}
