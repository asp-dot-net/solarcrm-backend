﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetAllLeadDocumentListInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
