﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetLeadDocumentForViewDto
    {
        public LeadDocumentDto documentList { get; set; }
    }
}
