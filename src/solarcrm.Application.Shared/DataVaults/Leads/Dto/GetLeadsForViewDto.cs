﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetLeadsForViewDto : EntityDto
	{
        public LeadsDto Leads { get; set; }

        public string PostCode { get; set; }
		public string StateName { get; set; }


		public string LeadSourceName { get; set; }

		public string LeadStatusName { get; set; }

		public string LeadStatusColorClass { get; set; }

		public string LeadStatusIconClass { get; set; }
		public string LeadSourcesColorClass { get; set; }
		public bool WebDuplicate { get; set; }

		public bool Duplicate { get; set; }

		public string CurrentAssignUserName { get; set; }

		public string CurrentAssignUserEmail { get; set; }

		public string CurrentAssignUserMobile { get; set; }

		public string CreatedByName { get; set; }

		public DateTime? CreatedOn { get; set; }
		public DateTime? DispatchedDate { get; set; }
		public string TrackingNo { get; set; }
		public int? WebDuplicateCount { get; set; }

		public int? DuplicateCount { get; set; }

		public string RejectReasonName { get; set; }

		public string CancelReasonName { get; set; }

		public string UnHandled { get; set; }

		public string New { get; set; }

		
		
		public string Total { get; set; }

		public string Facebook { get; set; }

		public string Online { get; set; }
		public string Tv { get; set; }
		public string Referral { get; set; }
		public string Others { get; set; }

		public string Assigned { get; set; }

		public string ReAssigned { get; set; }
		public string Sold { get; set; }

		public string LastComment { get; set; }

		public DateTime? LastCommentDate { get; set; }

		public DateTime? LastQuoteDate { get; set; }

		public object ApplicationStatusdata { get; set; }
		public string ApplicationStatusName { get; set; }
		public string ApplicationStatusColor { get; set; }
		public string ApplicationRefNo { get; set; }

		public int IsExternalLead { get; set; }

		public int OrganizationId { get; set; }

		public string OrganizationName { get; set; }

		public decimal? latitude { get; set; }

		public decimal? longitude { get; set; }

		public string LastModifiedUser { get; set; }

		public string JobNumber { get; set; }
		public decimal? SystemCapacity { get; set; }
		public decimal? TotalQuotaion { get; set; }
		public DateTime? InstallationDate { get; set; }

		public decimal? Owning { get; set; }
		public string JobStatusName { get; set; }
		public string InstallerName { get; set; }
		public string Reviewlink { get; set; }
		public int? JobStatusId { get; set; }

		public DateTime? JobCreatedDate { get; set; }

		public string JobStatus { get; set; }

		public bool LeadOwaner { get; set; }

		public string QunityAndModelList { get; set; }
		public DateTime? ActivityReminderTime { get; set; }
		public DateTime? ReminderTime { get; set; }
		public string ActivityDescription { get; set; }
		
		public string ActivityComment { get; set; }


		public string SignedQuotedDoc { get; set; }
		public string SignedQuotedDocPath { get; set; }
		public string UserManualDoc { get; set; }
		public string UserManualDocPath { get; set; }
		public string PanelBrocherDoc { get; set; }
		public string PanelBrocherFileName { get; set; }
		public string PanelBrocherDocPath { get; set; }
		public string InverterBrocherFileName { get; set; }
		public string InverterBrocherDoc { get; set; }
		public string InverterBrocherDocPath { get; set; }
		public string InverterWarrantyDoc { get; set; }
		public string InverterWarrantyDocPath { get; set; }
		public string PanelWarrantyDoc { get; set; }
		public string PanelWarrantyFileName { get; set; }
		public string InverterWarrantyFileName { get; set; }
		public string PanelWarrantyDocPath { get; set; }
		public string TransportCompanyName { get; set; }
		public string TransportLink { get; set; }
		public string FreebiesPromoType { get; set; }
		public string UserName { get; set; }
		public string UserEmail { get; set; }
		public string UserPhone { get; set; }

		public string OrgName { get; set; }
		public string OrgMobile { get; set; }
		public string OrgEmail { get; set; }
		public string OrgLogo { get; set; }
		public bool alldoc { get; set; }

		public int? leadid { get; set; }

		public int? jobid { get; set; }
		//public List<SendEmailAttachmentDto> filelist { get; set; }
		public int? DublicateLeadId { get; set; }
		public DateTime? LeadAssignDate { get; set; }
		public DateTime? CreationTime { get; set; }
		public LeadsSummaryCount leadsSummaryCount { get; set; }
    }
	public class LeadsSummaryCount
	{
		public string New { get; set; }

		public string Hot { get; set; }

		public string Cold { get; set; }

		public string Warm { get; set; }

		public string Upgrade { get; set; }

		public string Rejected { get; set; }

		public string Cancelled { get; set; }

		public string Closed { get; set; }

		public string Total { get; set; }
		public string UnHandled { get; set; }
		public string Assigned { get; set; }
		public string ReAssigned { get; set; }
		public string Sold { get; set; }
	}

	public class CancleLeadsSummaryCount
	{
		public string Facebook { get; set; }

		public string Online { get; set; }
		public string Tv { get; set; }
		public string Referral { get; set; }
		public string Others { get; set; }
		public string Total { get; set; }
	}
}
