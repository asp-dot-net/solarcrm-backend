﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class LeadsDto : FullAuditedEntity
    {

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string CustomerName { get; set; }

        public string MobileNumber { get; set; }

        public string EmailId { get; set; }

        public string Alt_Phone { get; set; }

        public string Address { get; set; }
        //public string AddressLine2 { get; set; }

        public string StateName { get; set; }
        public string DistrictName { get; set; }

        public string TalukaName { get; set; }

        public string CityName { get; set; }

        public int? Pincode { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public string LeadTypeName { get; set; }

        public int LeadStatusID { get; set; }
        public string LeadStatusName { get; set; }
        public string LeadSourceName { get; set; }

        public int? ChanelPartnerID { get; set; }
        public string SolarTypeName { get; set; }
        public DateTime? FollowupDate { get; set; }
        public bool CommonMeter { get; set; }

        public int? ConsumerNumber { get; set; }

        public int? AvgMonthlyUnit { get; set; }

        public int? AvgMonthlyBill { get; set; }
        public string DiscomName { get; set; }

        public string DivisionName { get; set; }

        public string SubDivisionName { get; set; }

        public string CircleName { get; set; }
        public decimal? RequiredCapacity { get; set; }

        public int? StrctureAmmount { get; set; }

        public string HeightStructureName { get; set; }

        public string CategoryName { get; set; }

        public string ChanelPartnerName { get; set; }
        public string LeadCreatedName { get; set; }
        public long? BankAccountNumber { get; set; }

        public string AccountHolderName { get; set; }

        public string BankName { get; set; }

        public string BankBranchName { get; set; }
        public string IFSC_Code { get; set; }

        public string Notes { get; set; }

        public virtual bool IsDuplicate { get; set; }

        public virtual bool IsWebDuplicate { get; set; }
        public bool IsActive { get; set; }
        public int IsExternalLead { get; set; }
        public DateTime CreatedDate { get; set; }

        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string LeadStatusColorClass { get; set; }
        public string LeadStatusIconClass { get;set; }

        public string LeadSourceColorClass { get; set; }
        public bool IsVerified { get; set; }
        public bool IsSelected { get; set; }
    }
}
