﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetLeadsForEditOutput
    {
        public CreateOrEditLeadsDto leads { get; set; }
    }
}
