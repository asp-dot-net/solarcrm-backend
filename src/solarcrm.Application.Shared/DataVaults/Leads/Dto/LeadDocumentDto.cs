﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class LeadDocumentDto : EntityDto
    {
        public string Title { get; set; }
        public string LeadDocumentId { get; set; }
        public string DocumentId { get; set; }
        public string DocumentNumber { get; set; }
        public string LeadDocumentPath { get; set; }
        public string FileType { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
