﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetLeadForChangeDuplicateStatusOutput : EntityDto<int?>
    {
        public int?[] LeadId { get; set; }

        public bool? IsDuplicate { get; set; }

        public bool? IsWebDuplicate { get; set; }

        public bool? HideDublicate { get; set; }

    }
}
