﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class CreateOrEditLeadDocumentDto : EntityDto<int?>
    {
        public int LeadId { get; set; }
        public string DocumentId { get; set; }
        public string DocumentNumber { get; set; }
        public string FileToken { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }

        public string FileSize { get; set; }
        public string docfile { get; set; }
        public bool IsActive { get; set; }
        public int? TenantId { get; set; }
    }
}
