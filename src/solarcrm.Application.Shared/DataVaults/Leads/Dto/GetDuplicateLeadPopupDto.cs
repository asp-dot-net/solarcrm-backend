﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetDuplicateLeadPopupDto : FullAuditedEntity
	{
		public string CustomerName { get; set; }

		public string CurrentLeadOwaner { get; set; }

		public string EmailId { get; set; }

		public bool EmailExist { get; set; }

		public string Alt_PhoneNumber { get; set; }

		public bool MobileExist { get; set; }

		public string MobileNumber { get; set; }

		public bool AddressExist { get; set; }

		//public string UnitNo { get; set; }

		//public string UnitType { get; set; }

		//public string StreetNo { get; set; }

		//public string StreetName { get; set; }

		//public string StreetType { get; set; }

		//public string Suburb { get; set; }

		public string State { get; set; }

		public string Postcode { get; set; }

		public string Address { get; set; }

		public string Requirements { get; set; }

		public string LeadSource { get; set; }

		public string LeadStatus { get; set; }

		public string CurrentAssignUserName { get; set; }

		public string CreatedByName { get; set; }

		public string ProjectNo { get; set; }

		public string ProjectStatus { get; set; }

		public DateTime? ProjectOpenDate { get; set; }

		public DateTime? LastFollowupDate { get; set; }

		public DateTime? LastQuoteDate { get; set; }

		public string Description { get; set; }

		public string RoleName { get; set; }
	}
}
