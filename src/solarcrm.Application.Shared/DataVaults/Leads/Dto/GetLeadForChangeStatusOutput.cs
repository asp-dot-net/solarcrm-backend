﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Leads.Dto
{
    public class GetLeadForChangeStatusOutput : EntityDto
    {
        public int? LeadActionID { get; set; }

        public int? LeadActivityID { get; set; }
        public int? LeadStatusID { get; set; }

        public string StatusName { get; set; }        

        public int? RejectReasonId { get; set; }

        public int? CancelReasonId { get; set; }
        public string RejectReason { get; set; }
    }
}
