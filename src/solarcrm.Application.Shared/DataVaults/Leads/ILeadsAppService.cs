﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Leads
{
    public interface ILeadsAppService
    {
        Task<PagedResultDto<GetLeadsForViewDto>> GetAll(GetAllLeadsInput input);

        Task<GetLeadsForViewDto> GetLeadsForView(int id);

        Task<GetLeadsForEditOutput> GetLeadsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLeadsDto input);

        Task DeleteCustomer(EntityDto input);

        Task<FileDto> GetLeadsToExcel(GetAllLeadsForExcelInput input);

        Task DeleteManageLead(int[] input);

        Task CreateOrEditLeadDocument(CreateOrEditLeadDocumentDto input);
        Task LeadDocumentDelete(EntityDto input);
        Task DeleteDuplicateLeads(EntityDto input);

        Task ChangeDuplicateStatusForMultipleLeadsHide(GetLeadForChangeDuplicateStatusOutput input);

        Task UpdateWebDupLeads(GetLeadForChangeDuplicateStatusOutput input);

        Task<List<GetDuplicateLeadPopupDto>> CheckExistLeadList(CheckExistLeadDto input);

        Task<bool> CheckValidation(CreateOrEditLeadsDto input);

        Task UpdateFacebookLead();
    }
}
