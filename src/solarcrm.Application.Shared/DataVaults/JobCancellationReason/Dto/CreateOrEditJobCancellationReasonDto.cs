﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.JobCancellationReason.Dto
{
    public class CreateOrEditJobCancellationReasonDto : EntityDto<int?>
    {
        [Required]
        [StringLength(JobCancellationReasonConsts.MaxNameLength, MinimumLength = JobCancellationReasonConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
