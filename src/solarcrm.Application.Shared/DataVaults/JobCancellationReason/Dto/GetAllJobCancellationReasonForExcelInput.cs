﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.JobCancellationReason.Dto
{
    public class GetAllJobCancellationReasonForExcelInput
    {
        public string Filter { get; set; }
    }
}
