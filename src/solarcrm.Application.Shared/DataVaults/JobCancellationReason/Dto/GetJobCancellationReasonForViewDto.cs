﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.JobCancellationReason.Dto
{
    public class GetJobCancellationReasonForViewDto
    {
        public JobCancellationReasonDto jobcancellationreason { get; set; }
    }
}
