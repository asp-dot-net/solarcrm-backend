﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.JobCancellationReason.Dto
{
    public class GetAllJobCancellationReasonInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
