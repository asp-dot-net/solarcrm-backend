﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.JobCancellationReason.Dto
{
    public class GetJobCancellationReasonForEditOutput
    {
        public CreateOrEditJobCancellationReasonDto jobcancellationreason { get; set; }
    }
}
