﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.JobCancellationReason.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.JobCancellationReason
{
    public interface IJobCancellationReasonAppService
    {
        Task<PagedResultDto<GetJobCancellationReasonForViewDto>> GetAll(GetAllJobCancellationReasonInput input);

        Task<GetJobCancellationReasonForViewDto> GetJobCancellationReasonForView(int id);

        Task<GetJobCancellationReasonForEditOutput> GetJobCancellationReasonForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditJobCancellationReasonDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetJobCancellationReasonToExcel(GetAllJobCancellationReasonForExcelInput input);
    }
}
