﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.LeadsActivityLog.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadsActivityLog
{
    public interface ILeadsActivityLogAppService
    {
        Task<List<GetLeadsActivityLogForViewDto>> GetAllActivityLog(GetActivityLogInput input);

        Task<GetLeadsActivityLogForViewDto> GetLeadsActivityLogForView(int id);

        Task AddActivityLog(ActivityLogInput input);
        Task CreateOrEdit(CreateOrEditLeadsActivityLogDto input);

        Task Delete(EntityDto input);
    }
}
