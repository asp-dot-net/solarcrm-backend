﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadsActivityLog.Dto
{
    public class CreateOrEditLeadsActivityLogDto : EntityDto<int?>
    {
        public int LeadId { get; set; }

        public int LeadActionId { get; set; }

        public int LeadAcitivityID { get; set; }

        public int SectionId { get; set; }

        public string LeadActionNote { get; set; }

        public virtual int? TenantId { get; set; }
        public virtual int OrganizationUnitId { get; set; }
    }
}
