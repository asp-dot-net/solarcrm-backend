﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadsActivityLog.Dto
{
    public class LeadActivityLogDto : EntityDto
    {
        public string LeadActionName { get; set; }

        public string LeadActivityName { get; set; }

        public string LeadActionNote { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
