﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadsActivityLog.Dto
{
    public class GetActivityLogInput
    {
		public int LeadId { get; set; }

		public int? ActionId { get; set; }

		public int? ActivityId { get; set; }
		public int? SectionId { get; set; }
		public bool? CurrentPage { get; set; }
		public bool? OnlyMy { get; set; }
		public int MyInstallerId { get; set; }
	}
}
