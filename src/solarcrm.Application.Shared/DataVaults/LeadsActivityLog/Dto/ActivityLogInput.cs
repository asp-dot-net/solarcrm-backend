﻿using Abp.Application.Services.Dto;
using solarcrm.DemoUiComponents.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadsActivityLog.Dto
{
    public class ActivityLogInput : PagedAndSortedResultRequestDto
	{
		public int TenantId { get; set; }

		public int LeadId { get; set; }

		public int ActionId { get; set; }

		public int ActivityId { get; set; }
		public int? EmailTemplateId { get; set; }

		public int? SMSTemplateId { get; set; }

		public string SmsConfig_TempId { get; set; }
		public int? CustomeTagsId { get; set; }

		public string EmailId { get; set; }

		public string Mobile { get; set; }
		public string ActionNote { get; set; }

		public DateTime? ActivityDate { get; set; }

		public string Body { get; set; }

		public string ActivityNote { get; set; }

		public int? TrackerId { get; set; }
		public string Subject { get; set; }
		public int? UserId { get; set; }
		public List<UploadFileOutput> AttachmentList { get; set; }
		public string FileAttachUrl { get; set; }
		public string JobNumber { get; set; }
        public int? ReferanceId { get; set; }
		public int? SectionId { get; set; }
		public int? TodopriorityId { get; set; }
		public string Todopriority { get; set; }
		public string EmailFrom { get; set; }
		public int MyInstallerId { get; set; }
		public string CreatedUser { get; set; }

		public int? OrganizationUnit { get; set; }
	}
}
