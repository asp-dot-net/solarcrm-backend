﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadsActivityLog.Dto
{
    public class LeadtrackerHistoryDto : FullAuditedEntity
    { 
        public virtual string FieldName { get; set; }
        public virtual string PrevValue { get; set; }
        public virtual string CurValue { get; set; }
        public virtual string Action { get; set; }
        // public virtual string DisplayField { get; set; }
        public virtual DateTime LastmodifiedDateTime { get; set; }
        public int LeadId { get; set; }
        public int LeadActionId { get; set; }
        public virtual string Lastmodifiedbyuser { get; set; }
    }
}
