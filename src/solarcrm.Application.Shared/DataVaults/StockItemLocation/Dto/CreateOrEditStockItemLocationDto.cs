﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockItemLocation.Dto
{
    public class CreateOrEditStockItemLocationDto : EntityDto<int?>
    {
        public int StockItemId { get; set; }

        public int LocationId { get; set; }

        public string LocationName { get; set; }

        public int? MinQty { get; set; }

        public bool IsActive { get; set; }
    }
}
