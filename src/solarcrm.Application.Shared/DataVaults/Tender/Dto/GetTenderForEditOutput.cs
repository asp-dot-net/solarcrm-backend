﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Tender.Dto
{
    public class GetTenderForEditOutput
    {
        public CreateOrEditTenderDto tender { get; set; }
    }
}
