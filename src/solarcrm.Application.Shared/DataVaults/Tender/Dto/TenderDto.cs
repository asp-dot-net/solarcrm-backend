﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Tender.Dto
{
    public class TenderDto : EntityDto
    {
        public string Name { get; set; }

        public string SolarType { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
