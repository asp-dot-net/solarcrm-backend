﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.Tender.Dto
{
    public class CreateOrEditTenderDto : EntityDto<int?>
    {
        [Required]
        [StringLength(TalukaConsts.MaxNameLength, MinimumLength = TalukaConsts.MinNameLength)]
        public string Name { get; set; }

        [Required]
        public int SolarTypeId { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }

    }
}
