﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Tender.Dto
{
    public class GetAllTenderInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
