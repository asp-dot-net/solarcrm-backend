﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Tender.Dto
{
    public class GetAllTenderForExcelInput
    {
        public string Filter { get; set; }
    }
}
