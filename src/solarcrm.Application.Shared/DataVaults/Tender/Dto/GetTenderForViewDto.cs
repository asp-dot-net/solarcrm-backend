﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Tender.Dto
{
    public class GetTenderForViewDto
    {
        public TenderDto tender { get; set; }
    }
}
