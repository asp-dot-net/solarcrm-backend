﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Tender.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Tender
{
    public interface ITenderAppService
    {
        Task<PagedResultDto<GetTenderForViewDto>> GetAll(GetAllTenderInput input);

        Task<GetTenderForViewDto> GetTenderForView(int id);

        Task<GetTenderForEditOutput> GetTenderForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTenderDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTenderToExcel(GetAllTenderForExcelInput input);
    }
}
