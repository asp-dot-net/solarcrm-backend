﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.LeadSource.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadSource
{
    public interface ILeadSourceAppService
    {
        Task<PagedResultDto<GetLeadSourceForViewDto>> GetAll(GetAllLeadSourceInput input);

        Task<GetLeadSourceForViewDto> GetLeadSourceForView(int id);

        Task<GetLeadSourceForEditOutput> GetLeadSourceForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLeadSourceDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetLeadSourceToExcel(GetAllLeadSourceForExcelInput input);

        Task<List<LeadSourceLookupTableDto>> GetAllLeadSourceForDropdown();
    }
}
