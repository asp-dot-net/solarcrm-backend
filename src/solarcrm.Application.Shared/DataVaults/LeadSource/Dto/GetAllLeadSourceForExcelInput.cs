﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.LeadSource.Dto
{
    public class GetAllLeadSourceForExcelInput
    {
        public string Filter { get; set; }
    }
}
