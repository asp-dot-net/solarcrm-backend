﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadSource.Dto
{
    public class GetLeadSourceForViewDto
    {
        public LeadSourceDto LeadSource { get; set; }
    }
}
