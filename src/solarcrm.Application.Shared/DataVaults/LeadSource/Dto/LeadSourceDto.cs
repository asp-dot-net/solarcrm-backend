﻿using Abp.Application.Services.Dto;
using Abp.Organizations;
using System;
using System.Collections.Generic;

namespace solarcrm.DataVaults.LeadSource.Dto
{
    public class LeadSourceDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public List<string> OrganizationUnitsName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
