﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.LeadSource.Dto
{
    public class CreateOrEditLeadSourceDto : EntityDto<int?>
    {
        [Required]
        [StringLength(LeadSourceConsts.MaxNameLength, MinimumLength = LeadSourceConsts.MinNameLength)]
        public string Name { get; set; }

        public List<long> OrganizationUnits { get; set; }
        public bool IsActive { get; set; }
    }
}
