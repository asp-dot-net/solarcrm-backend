﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadSource.Dto
{
    public class GetLeadSourceForEditOutput
    {
        public CreateOrEditLeadSourceDto LeadSource { get; set; }
    }
}
