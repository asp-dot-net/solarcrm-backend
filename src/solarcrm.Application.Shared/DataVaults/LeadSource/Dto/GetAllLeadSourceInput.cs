﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.LeadSource.Dto
{
    public class GetAllLeadSourceInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
