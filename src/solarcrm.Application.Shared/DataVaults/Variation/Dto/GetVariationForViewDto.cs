﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Variation.Dto
{
    public class GetVariationForViewDto
    {
        public VariationDto Variation { get; set; }
    }
}
