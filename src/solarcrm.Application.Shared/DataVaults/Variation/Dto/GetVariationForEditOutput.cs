﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Variation.Dto
{
    public class GetVariationForEditOutput
    {
        public CreateOrEditVariationDto Variation { get; set; }
    }
}
