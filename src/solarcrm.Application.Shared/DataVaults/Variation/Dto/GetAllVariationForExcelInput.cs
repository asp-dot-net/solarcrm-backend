﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Variation.Dto
{
    public class GetAllVariationForExcelInput
    {
        public string Filter { get; set; }
    }
}
