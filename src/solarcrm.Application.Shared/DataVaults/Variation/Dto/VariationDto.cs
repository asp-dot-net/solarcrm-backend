﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.Variation.Dto
{
    public class VariationDto : EntityDto
    {
        public string Name { get; set; }

        public string Action { get; set; }

        public bool IsActive { get; set; }
    }
}
