﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Variation.Dto
{
    public class GetAllVariationInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
