﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.Variation.Dto
{
    public class CreateOrEditVariationDto : EntityDto<int?>
    {
        public string Name { get; set; }

        [Required]
        [StringLength(VariationConsts.MaxActionLength, MinimumLength = VariationConsts.MinActionLength)]
        public string Action { get; set; }

        public bool IsActive { get; set; }
    }
}
