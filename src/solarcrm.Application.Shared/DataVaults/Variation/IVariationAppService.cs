﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Variation.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Variation
{
    public interface IVariationAppService
    {
        Task<PagedResultDto<GetVariationForViewDto>> GetAll(GetAllVariationInput input);

        Task<GetVariationForViewDto> GetVariationForView(int id);

        Task<GetVariationForEditOutput> GetVariationForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditVariationDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetVariationToExcel(GetAllVariationForExcelInput input);

        //Task<List<GetAllLocationForDropdown>> GetAllLocationForDropdown();
    }
}
