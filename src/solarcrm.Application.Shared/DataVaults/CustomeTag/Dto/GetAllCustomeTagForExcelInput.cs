﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.CustomeTag.Dto
{
    public class GetAllCustomeTagForExcelInput
    {
        public string Filter { get; set; }
    }
}
