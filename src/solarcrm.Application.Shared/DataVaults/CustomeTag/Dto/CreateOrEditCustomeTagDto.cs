﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.CustomeTag.Dto
{
    public class CreateOrEditCustomeTagDto : EntityDto<int?>
    {
        [Required]
        public string TagTableName { get; set; }

        [Required]
        //[StringLength(CityConsts.MaxNameLength, MinimumLength = CityConsts.MinNameLength)]
        public string TagTableFieldName { get; set; }


        public bool IsActive { get; set; }
    }
}
