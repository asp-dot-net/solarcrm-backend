﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.CustomeTag.Dto
{
    public class CustomeTagDto : EntityDto
    {

        public string TagTableName { get; set; }

        public string TagTableFieldName { get; set; }

     
        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}

