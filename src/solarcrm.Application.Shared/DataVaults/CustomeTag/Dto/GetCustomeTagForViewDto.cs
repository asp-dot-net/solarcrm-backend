﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.CustomeTag.Dto
{
    public class GetCustomeTagForViewDto
    {
        public CustomeTagDto CustomTags { get; set; }
    }
}
