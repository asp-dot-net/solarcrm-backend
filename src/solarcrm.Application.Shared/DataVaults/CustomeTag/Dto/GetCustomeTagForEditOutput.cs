﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.CustomeTag.Dto
{
    public class GetCustomeTagForEditOutput
    {
        public CustomeTagDto CustomeTag { get; set; }
    }
}
