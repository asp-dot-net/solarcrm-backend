﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.CustomeTag.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.CustomeTag
{
    public interface ICustomeTagAppService
    {
        Task<PagedResultDto<GetCustomeTagForViewDto>> GetAll(GetAllCustomeTagInput input);

        Task<GetCustomeTagForViewDto> GetCustomeTagForView(int id);

        Task<GetCustomeTagForEditOutput> GetCustomeTagForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCustomeTagDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetCustomeTagToExcel(GetAllCustomeTagForExcelInput input);
    }
}
