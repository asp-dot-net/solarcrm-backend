﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PdfTemplates.Dto
{
    public class PdfTemplateDto : EntityDto
    {
        public string PdfTemplateName { get; set; }

        public string ViewHtml { get; set; }

        public string ApiHtml { get; set; }

        public int OrganizationUnitId { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
