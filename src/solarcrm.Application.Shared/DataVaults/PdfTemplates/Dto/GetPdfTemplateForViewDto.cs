﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PdfTemplates.Dto
{
    public class GetPdfTemplateForViewDto
    {
        public PdfTemplateDto PdfTemplates { get; set; }
    }
}
