﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PdfTemplates.Dto
{
    public class GetAllPdfTemplateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
