﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.PdfTemplates.Dto
{
    public class CreateOrEditPdfTemplateDto : EntityDto<int?>
    {
        [Required]
        [StringLength(PdfTemplateConsts.MaxNameLength, MinimumLength = PdfTemplateConsts.MinNameLength)]
        public virtual string PdfTemplateName { get; set; }

        [Required]
        public virtual string ViewHtml { get; set; }

        public virtual string ApiHtml { get; set; }

        public virtual int? OrganizationUnitId { get; set; }

        public virtual int? TenantId { get; set; }

        public bool IsActive { get; set; }
    }
}
