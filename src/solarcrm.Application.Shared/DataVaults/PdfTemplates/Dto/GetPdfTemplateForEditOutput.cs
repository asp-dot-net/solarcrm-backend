﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PdfTemplates.Dto
{
    public class GetPdfTemplateForEditOutput
    {
        public CreateOrEditPdfTemplateDto PdfTemplates { get; set; }
    }
}
