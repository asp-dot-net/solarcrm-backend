﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.PdfTemplates.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PdfTemplates
{
    public interface IPdfTemplateAppService
    {
        Task<PagedResultDto<GetPdfTemplateForViewDto>> GetAll(GetAllPdfTemplateInput input);

        Task<GetPdfTemplateForEditOutput> GetPdfTemplateForEdit(EntityDto input);
        Task<GetPdfTemplateForEditOutput> GetPdfTemplateForView(int id, int organizationId);
        Task CreateOrEdit(CreateOrEditPdfTemplateDto input);

        Task Delete(EntityDto input);
    }
}
