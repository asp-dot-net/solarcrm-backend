﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLists.Dto
{
    public class GetAllDocumentListInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }


    }
}
