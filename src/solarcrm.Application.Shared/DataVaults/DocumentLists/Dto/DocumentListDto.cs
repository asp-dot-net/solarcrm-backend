﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.DocumentLists.Dto
{
    public class DocumentListDto : EntityDto
    {
        public string Name { get; set; }
        public string DocumentShortName { get; set; }
        public bool IsATFlag { get; set; }
        public bool? IsUploadFlag { get; set; }
        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
        public string DocumenStorageUnit { get; set; }
        public virtual string DocumenSize { get; set; }
        public virtual string DocumenFormate { get; set; }

    }
}
