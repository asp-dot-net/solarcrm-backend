﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.DocumentLists.Dto
{
    public class CreateOrEditDocumentListDto : EntityDto<int?>
    {
        [Required]
        [StringLength(DocumentListConsts.MaxNameLength, MinimumLength = DocumentListConsts.MinNameLength)]
        public string Name { get; set; }

        public string DocumentShortName { get; set; }
        public string DocumenSize { get; set; }
     
        public string DocumenFormate { get; set; }

        public bool IsATFlag { get; set; }
        public bool IsActive { get; set; }
        public virtual string DocumenStorageUnit { get; set; }

    }
}
