﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLists.Dto
{
    public class GetDocumentListForViewDto
    {
        public DocumentListDto DocumentList { get; set; }
    }
}
