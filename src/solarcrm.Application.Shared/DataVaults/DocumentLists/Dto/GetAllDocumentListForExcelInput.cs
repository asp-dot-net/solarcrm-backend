﻿using System;
using System.Collections.Generic;

namespace solarcrm.DataVaults.DocumentLists.Dto
{
    public class GetAllDocumentListForExcelInput
    {
        public string Filter { get; set; }
    }
}
