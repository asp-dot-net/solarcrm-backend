﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLists.Dto
{
    public class GetDocumentListForEditOutput
    {
        public CreateOrEditDocumentListDto DocumentList { get; set; }
    }
}
