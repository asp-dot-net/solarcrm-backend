﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.DocumentLists.Dto;
using solarcrm.Dto;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DocumentLists
{
    public interface IDocumentListAppService
    {
        Task<PagedResultDto<GetDocumentListForViewDto>> GetAll(GetAllDocumentListInput input);

        Task<GetDocumentListForViewDto> GetDocumentListForView(int id);

        Task<GetDocumentListForEditOutput> GetDocumentListForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDocumentListDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDocumentListToExcel(GetAllDocumentListForExcelInput input);
    }
}
