﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.SolarType.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.SolarType
{
    public interface ISolarTypeAppService
    {
        Task<PagedResultDto<GetSolarTypeForViewDto>> GetAll(GetAllSolarTypeInput input);

        Task<GetSolarTypeForViewDto> GetSolarTypeForView(int id);

        Task<GetSolarTypeForEditOutput> GetSolarTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSolarTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetSolarTypeToExcel(GetAllSolarTypeForExcelInput input);

        Task<List<SolarTypeLookupTable>> GetAllSolarTypeForDropdown();
    }
}
