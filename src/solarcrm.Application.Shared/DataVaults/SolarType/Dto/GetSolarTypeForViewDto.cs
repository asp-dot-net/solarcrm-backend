﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SolarType.Dto
{
    public class GetSolarTypeForViewDto
    {
        public SolarTypeDto SolarTypes { get; set; }
    }
}
