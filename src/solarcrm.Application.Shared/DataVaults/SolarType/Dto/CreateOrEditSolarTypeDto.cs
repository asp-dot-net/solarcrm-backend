﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.SolarType.Dto
{
    public class CreateOrEditSolarTypeDto : EntityDto<int?>
    {
        [Required]
        [StringLength(SolarTypeConsts.MaxNameLength, MinimumLength = SolarTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public bool IsManualPricing { get; set; }
    }
}
