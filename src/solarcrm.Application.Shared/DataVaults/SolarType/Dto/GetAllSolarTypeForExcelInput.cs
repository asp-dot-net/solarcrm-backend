﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SolarType.Dto
{
    public class GetAllSolarTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
