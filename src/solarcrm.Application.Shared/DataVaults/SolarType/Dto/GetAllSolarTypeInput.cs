﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SolarType.Dto
{
    public class GetAllSolarTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
