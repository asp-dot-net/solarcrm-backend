﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SolarType.Dto
{
    public class GetSolarTypeForEditOutput
    {
        public CreateOrEditSolarTypeDto SolarTypes { get; set; }
    }
}
