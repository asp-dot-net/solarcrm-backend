﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.City.Dto
{
    public class CityDto : EntityDto
    {
       
        public string TalukaName { get; set; }

        public string DistrictName { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
