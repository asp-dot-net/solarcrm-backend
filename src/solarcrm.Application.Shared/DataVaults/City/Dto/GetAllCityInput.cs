﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.City.Dto
{
    public class GetAllCityInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
