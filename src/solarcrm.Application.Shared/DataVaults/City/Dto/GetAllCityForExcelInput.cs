﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.City.Dto
{
    public class GetAllCityForExcelInput
    {
        public string Filter { get; set; }
    }
}
