﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.City.Dto
{
    public class CreateOrEditCityDto : EntityDto<int?>
    {       
        [Required]
        public int TalukaId { get; set; }

        [Required]
        [StringLength(CityConsts.MaxNameLength, MinimumLength = CityConsts.MinNameLength)]
        public string Name { get; set; }


        public bool IsActive { get; set; }
    }
}
