﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.City.Dto
{
    public class GetCityForEditOutput
    {
        public CreateOrEditCityDto citys { get; set; }
    }
}
