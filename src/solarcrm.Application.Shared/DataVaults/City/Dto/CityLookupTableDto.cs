﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.City.Dto
{
    public class CityLookupTableDto : Abp.Domain.Entities.Entity
    {     

        public string DisplayName { get; set; }
    }
}
