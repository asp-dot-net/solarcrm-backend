﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.City.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.City
{
    public interface ICityAppService
    {
        Task<PagedResultDto<GetCityForViewDto>> GetAll(GetAllCityInput input);

        Task<GetCityForViewDto> GetCityForView(int id);

        Task<GetCityForEditOutput> GetCityForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCityDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetCityToExcel(GetAllCityForExcelInput input);

      
    }
}
