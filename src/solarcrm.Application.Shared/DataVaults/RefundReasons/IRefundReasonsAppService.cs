﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.RefundReasons.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.RefundReasons
{
    public interface IRefundReasonsAppService
    {
        Task<PagedResultDto<GetRefundReasonsForViewDto>> GetAll(GetAllRefundReasonsInput input);

        Task<GetRefundReasonsForViewDto> GetRefundReasonsForView(int id);

        Task<GetRefundReasonsForEditOutput> GetRefundReasonsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditRefundReasonsDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetRefundReasonsToExcel(GetAllRefundReasonsForExcelInput input);
    }
}
