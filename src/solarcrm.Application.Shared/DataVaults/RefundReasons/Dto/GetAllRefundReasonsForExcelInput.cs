﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.RefundReasons.Dto
{
    public class GetAllRefundReasonsForExcelInput
    {
        public string Filter { get; set; }
    }
}
