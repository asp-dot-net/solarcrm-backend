﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.RefundReasons.Dto
{
    public class GetAllRefundReasonsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
