﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.RefundReasons.Dto
{
    public class GetRefundReasonsForViewDto
    {
        public RefundReasonsDto RefundReason { get; set; }
    }
}
