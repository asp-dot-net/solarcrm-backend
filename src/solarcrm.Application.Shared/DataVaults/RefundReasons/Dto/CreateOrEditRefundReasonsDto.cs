﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.RefundReasons.Dto
{
    public class CreateOrEditRefundReasonsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(ProjectTypeConsts.MaxNameLength, MinimumLength = ProjectTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
