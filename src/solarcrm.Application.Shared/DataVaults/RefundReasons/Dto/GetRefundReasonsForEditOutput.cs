﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.RefundReasons.Dto
{
    public class GetRefundReasonsForEditOutput
    {
        public CreateOrEditRefundReasonsDto RefundReason { get; set; }
    }
}
