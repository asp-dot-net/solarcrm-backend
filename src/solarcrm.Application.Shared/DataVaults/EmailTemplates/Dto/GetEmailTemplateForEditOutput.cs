﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.EmailTemplates.Dto
{
    public class GetEmailTemplateForEditOutput
    {
        public CreateOrEditEmailTemplateDto EmailTemplates { get; set; }
    }
}
