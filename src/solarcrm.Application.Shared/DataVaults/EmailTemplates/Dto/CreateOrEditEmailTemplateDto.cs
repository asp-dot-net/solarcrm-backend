﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.EmailTemplates.Dto
{
    public class CreateOrEditEmailTemplateDto : EntityDto<int?>
	{
		[Required]
		[StringLength(EmailTemplateConsts.MaxNameLength, MinimumLength = EmailTemplateConsts.MinNameLength)]
		public virtual string TemplateName { get; set; }

		[Required]
		public virtual string Body { get; set; }

		public virtual string Subject { get; set; }

		public virtual int? OrganizationUnitId { get; set; }

		public bool IsActive { get; set; }
	}
}
