﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.EmailTemplates.Dto
{
    public class GetAllEmailTemplateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
