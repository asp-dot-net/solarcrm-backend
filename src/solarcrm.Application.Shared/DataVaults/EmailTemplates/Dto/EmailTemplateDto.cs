﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.EmailTemplates.Dto
{
    public class EmailTemplateDto : EntityDto
    {
        public string TemplateName { get; set; }

        public string Body { get; set; }

        public string Subject { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
