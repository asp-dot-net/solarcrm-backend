﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.EmailTemplates.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.EmailTemplates
{
    public interface IEmailTemplateAppService
    {
        Task<PagedResultDto<GetEmailTemplateForViewDto>> GetAll(GetAllEmailTemplateInput input);

        Task<GetEmailTemplateForEditOutput> GetEmailTemplateForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmailTemplateDto input);

        Task Delete(EntityDto input);
    }
}
