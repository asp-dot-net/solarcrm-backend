﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.ProjectStatus.Dto
{
    public class GetAllProjectStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
