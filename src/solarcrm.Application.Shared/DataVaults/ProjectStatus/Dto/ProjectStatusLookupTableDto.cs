﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ProjectStatus.Dto
{
    public class ProjectStatusLookupTableDto : Abp.Domain.Entities.Entity
    {
        public string DisplayName { get; set; }
    }
}
