﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ProjectStatus.Dto
{
    public class GetProjectStatusForViewDto
    {
        public ProjectStatusDto ProjectStatus { get; set; }
    }
}
