﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ProjectStatus.Dto
{
    public class GetProjectStatusForEditOutput
    {
        public CreateOrEditProjectStatusDto ProjectStatus { get; set; }
    }
}
