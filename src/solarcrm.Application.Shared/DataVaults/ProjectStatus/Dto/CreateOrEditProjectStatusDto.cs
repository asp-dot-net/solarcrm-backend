﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.ProjectStatus.Dto
{
    public class CreateOrEditProjectStatusDto : EntityDto<int?>
    {
        [Required]
        [StringLength(ProjectStatusConsts.MaxNameLength, MinimumLength = ProjectStatusConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
