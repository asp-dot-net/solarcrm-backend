﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.ProjectStatus.Dto
{
    public class GetAllProjectStatusForExcelInput
    {
        public string Filter { get; set; }
    }
}
