﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.ProjectStatus.Dto
{
    public class ProjectStatusDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
