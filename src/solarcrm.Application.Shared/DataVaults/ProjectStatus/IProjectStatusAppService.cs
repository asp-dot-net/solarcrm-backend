﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.ProjectStatus.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ProjectStatus
{
    public interface IProjectStatusAppService
    {
        Task<PagedResultDto<GetProjectStatusForViewDto>> GetAll(GetAllProjectStatusInput input);

        Task<GetProjectStatusForViewDto> GetProjectStatusForView(int id);

        Task<GetProjectStatusForEditOutput> GetProjectStatusForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditProjectStatusDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetProjectStatusToExcel(GetAllProjectStatusForExcelInput input);
        Task<List<ProjectStatusLookupTableDto>> GetAllProjectStatusForDropdown();
    }
}
