﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SmsTemplates.Dto
{
    public class GetSmsTemplateForViewDto
    {
        public SmsTemplateDto SmsTemplate { get; set; }
        //public string Organization { get; set; }
    }
}
