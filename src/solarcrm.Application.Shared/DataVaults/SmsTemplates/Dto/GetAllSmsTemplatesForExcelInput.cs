﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SmsTemplates.Dto
{
    public class GetAllSmsTemplatesForExcelInput
    {
        public string Filter { get; set; }
    }
}
