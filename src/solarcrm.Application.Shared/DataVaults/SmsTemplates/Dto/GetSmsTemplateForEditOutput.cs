﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SmsTemplates.Dto
{
    public class GetSmsTemplateForEditOutput
    {
        public CreateOrEditSmsTemplateDto SmsTemplate { get; set; }
    }
}
