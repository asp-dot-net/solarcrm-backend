﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.SmsTemplates.Dto
{
    public class CreateOrEditSmsTemplateDto : EntityDto<int?>
    {

        [Required]
        [StringLength(SmsTemplateConsts.MaxNameLength, MinimumLength = SmsTemplateConsts.MinNameLength)]
        public string Name { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public string ProviderTemp_Id { get; set; }
        public virtual int? OrganizationUnitId { get; set; }

        public bool IsActive { get; set; }
    }
}
