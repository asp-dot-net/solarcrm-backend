﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SmsTemplates.Dto
{
    public class SmsTemplateDto : EntityDto
    {
        public string Name { get; set; }

        public string Text { get; set; }

        public string ProviderTemp_Id { get; set; }
        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
