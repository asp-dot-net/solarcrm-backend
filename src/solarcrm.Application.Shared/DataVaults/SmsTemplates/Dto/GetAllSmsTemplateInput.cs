﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SmsTemplates.Dto
{
    public class GetAllSmsTemplateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        //public int OrganizationUnitId { get; set; }
    }
}
