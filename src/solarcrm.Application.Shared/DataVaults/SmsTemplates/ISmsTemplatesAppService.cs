﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.SmsTemplates.Dto;
using solarcrm.Dto;

namespace TheSolarProduct.SmsTemplates
{
    public interface ISmsTemplatesAppService 
    {
        Task<PagedResultDto<GetSmsTemplateForViewDto>> GetAll(GetAllSmsTemplateInput input);

        Task<GetSmsTemplateForViewDto> GetSmsTemplateForView(int id);

        Task<GetSmsTemplateForEditOutput> GetSmsTemplateForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSmsTemplateDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetSmsTemplatesToExcel(GetAllSmsTemplatesForExcelInput input);       

    }
}