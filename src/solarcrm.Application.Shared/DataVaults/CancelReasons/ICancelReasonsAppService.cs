﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.CancelReasons
{
    public interface ICancelReasonsAppService
    {
        Task<PagedResultDto<GetCancelReasonsForViewDto>> GetAll(GetAllCancelReasonsInput input);

        Task<GetCancelReasonsForViewDto> GetCancelReasonsForView(int id);

        Task<GetCancelReasonsForEditOutput> GetCancelReasonsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCancelReasonsDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetCancelReasonsToExcel(GetAllCancelReasonsForExcelInput input);

       
    }
}
