﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.CancelReasons
{
    public class CreateOrEditCancelReasonsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(CancelReasonsConsts.MaxNameLength, MinimumLength = CancelReasonsConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
