﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.CancelReasons
{
    public class GetAllCancelReasonsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
