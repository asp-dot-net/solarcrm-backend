﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.CancelReasons
{
    public class GetCancelReasonsForViewDto
    {
        public CancelReasonsDto CancelReasons { get; set; }
    }
}
