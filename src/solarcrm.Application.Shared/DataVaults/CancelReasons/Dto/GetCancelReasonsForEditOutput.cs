﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.CancelReasons
{
    public class GetCancelReasonsForEditOutput
    {
        public CreateOrEditCancelReasonsDto CancelReasons { get; set; }
    }
}
