﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using System.Threading.Tasks;
using solarcrm.DataVaults.Discom.Dto;
using System.Collections.Generic;

namespace solarcrm.DataVaults.Discom
{
    public interface IDiscomAppService
    {
        Task<PagedResultDto<GetDiscomForViewDto>> GetAll(GetAllDiscomInput input);

        //Task<GetDiscomForViewDto> GetDiscomForView(int id);

        Task<GetDiscomForEditOutput> GetDiscomForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDiscomDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDiscomToExcel(GetAllDiscomForExcelInput input);

        Task<List<GetAllDiscomForDropdown>> GetAllDiscomForDropdown();
    }
}
