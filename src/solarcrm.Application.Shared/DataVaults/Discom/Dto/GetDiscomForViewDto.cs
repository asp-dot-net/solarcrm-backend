﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Discom.Dto
{
    public class GetDiscomForViewDto
    {
        public DiscomDto Discom { get; set; }
    }
}
