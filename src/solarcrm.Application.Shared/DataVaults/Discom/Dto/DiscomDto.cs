﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.Discom.Dto
{
    public class DiscomDto : EntityDto
    {
        public string Name { get; set; }

        public string ABB { get; set; }

        public string PaymentLink { get; set; }
        public string Location { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
