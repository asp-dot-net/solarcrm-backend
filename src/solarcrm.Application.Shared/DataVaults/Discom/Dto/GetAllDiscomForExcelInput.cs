﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Discom.Dto
{
    public class GetAllDiscomForExcelInput
    {
        public string Filter { get; set; }
    }
}
