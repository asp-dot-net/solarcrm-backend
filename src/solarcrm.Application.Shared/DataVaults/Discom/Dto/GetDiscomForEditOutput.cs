﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Discom.Dto
{
    public class GetDiscomForEditOutput
    {
        public CreateOrEditDiscomDto Discom { get; set; }
    }
}
