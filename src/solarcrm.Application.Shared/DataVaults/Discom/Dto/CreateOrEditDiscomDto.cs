﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.Discom.Dto
{
    public class CreateOrEditDiscomDto : EntityDto<int?>
    {
        [Required]
        [StringLength(DiscomConsts.MaxNameLength, MinimumLength = DiscomConsts.MinNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(DiscomConsts.MaxABBLength, MinimumLength = DiscomConsts.MinABBLength)]
        public string ABB { get; set; }       

        public int LocationId { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(DiscomConsts.MaxPaymentLinkLength, MinimumLength = DiscomConsts.MinPaymentLinkLength)]
        public string PaymentLink { get; set; }
    }
}
