﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Discom.Dto
{
    public class GetAllDiscomForDropdown : Abp.Domain.Entities.Entity
    {
        public string Name { get; set; }
    }
}
