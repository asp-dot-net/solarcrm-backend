﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Discom.Dto
{
    public class GetAllDiscomInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
