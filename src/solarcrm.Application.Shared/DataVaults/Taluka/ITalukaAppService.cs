﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Taluka.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Taluka
{
    public interface ITalukaAppService
    {
        Task<PagedResultDto<GetTalukaForViewDto>> GetAll(GetAllTalukaInput input);

        Task<GetTalukaForViewDto> GetTalukaForView(int id);

        Task<GetTalukaForEditOutput> GetTalukaForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTalukaDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTalukaToExcel(GetAllTalukaForExcelInput input);

        Task<List<TalukaLookupTableDto>> GetAllTalukaForDropdown();
    }
}
