﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.Taluka.Dto
{
    public class CreateOrEditTalukaDto : EntityDto<int?>
    {       
        [Required]
        public int DistrictId { get; set; }

        [Required]
        [StringLength(TalukaConsts.MaxNameLength, MinimumLength = TalukaConsts.MinNameLength)]
        public string Name { get; set; }


        public bool IsActive { get; set; }
    }
}
