﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Taluka.Dto
{
    public class GetTalukaForEditOutput
    {
        public CreateOrEditTalukaDto talukas { get; set; }
    }
}
