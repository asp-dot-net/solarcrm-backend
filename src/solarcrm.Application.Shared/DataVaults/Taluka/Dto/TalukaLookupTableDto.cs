﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Taluka.Dto
{
    public class TalukaLookupTableDto : Abp.Domain.Entities.Entity
    {
        public string DisplayName { get; set; }
    }
}
