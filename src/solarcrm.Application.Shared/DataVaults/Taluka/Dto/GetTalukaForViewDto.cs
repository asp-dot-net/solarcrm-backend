﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Taluka.Dto
{
    public class GetTalukaForViewDto
    {
        public TalukaDto Talukas { get; set; }
    }
}
