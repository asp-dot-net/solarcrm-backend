﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.Taluka.Dto
{
    public class GetAllTalukaForExcelInput
    {
        public string Filter { get; set; }
    }
}
