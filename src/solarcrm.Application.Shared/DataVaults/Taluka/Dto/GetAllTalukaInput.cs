﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.Taluka.Dto
{
    public class GetAllTalukaInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
