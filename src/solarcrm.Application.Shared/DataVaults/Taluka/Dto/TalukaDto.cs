﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.Taluka.Dto
{
    public class TalukaDto : EntityDto
    {
       
        public string DistrictName { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
