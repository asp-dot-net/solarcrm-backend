﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentType.Dto
{
    public class GetPaymentTypeForEditOutput
    {
        public CreateOrEditPaymentTypeDto paymentType { get; set; }
    }
}
