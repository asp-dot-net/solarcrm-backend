﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentType.Dto
{
    public class GetAllPaymentTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
