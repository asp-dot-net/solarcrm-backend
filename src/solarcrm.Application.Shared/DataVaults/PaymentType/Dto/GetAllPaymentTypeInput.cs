﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentType.Dto
{
    public class GetAllPaymentTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
