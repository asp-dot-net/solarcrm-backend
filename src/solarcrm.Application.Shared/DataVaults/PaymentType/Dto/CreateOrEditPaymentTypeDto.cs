﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.PaymentType.Dto
{
    public class CreateOrEditPaymentTypeDto : EntityDto<int?>
    {
        [Required]
        [StringLength(LeadTypeConsts.MaxNameLength, MinimumLength = LeadTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
