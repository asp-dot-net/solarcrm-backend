﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentType.Dto
{
    public class GetPaymentTypeForViewDto
    {
        public PaymentTypeDto paymentType { get; set; }
    }
}
