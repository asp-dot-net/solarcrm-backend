﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.DataVaults.PaymentType.Dto;
using solarcrm.Dto;
using System;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PaymentType
{
    public interface IPaymentTypeAppService
    {
        Task<PagedResultDto<GetPaymentTypeForViewDto>> GetAll(GetAllPaymentTypeInput input);

        Task<GetPaymentTypeForViewDto> GetPaymentTypeForView(int id);

        Task<GetPaymentTypeForEditOutput> GetPaymentTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPaymentTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetPaymentTypeToExcel(GetAllPaymentTypeForExcelInput input);
    }
}
