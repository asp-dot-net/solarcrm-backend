﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.InvoiceType.Dto;
using solarcrm.Dto;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.InvoiceType
{
    public interface IInvoiceTypeAppService
    {
        Task<PagedResultDto<GetInvoiceTypeForViewDto>> GetAll(GetAllInvoiceTypeInput input);

        Task<GetInvoiceTypeForViewDto> GetInvoiceTypeForView(int id);

        Task<GetInvoiceTypeForEditOutput> GetInvoiceTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditInvoiceTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetInvoiceTypeToExcel(GetAllInvoiceTypeForExcelInput input);
    }
}
