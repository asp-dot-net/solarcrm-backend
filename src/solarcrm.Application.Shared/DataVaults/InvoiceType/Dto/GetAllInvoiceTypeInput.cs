﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.InvoiceType.Dto
{
    public class GetAllInvoiceTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
