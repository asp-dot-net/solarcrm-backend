﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.InvoiceType.Dto
{
    public class InvoiceTypeDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
