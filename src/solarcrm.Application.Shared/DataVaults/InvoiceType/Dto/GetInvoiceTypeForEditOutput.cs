﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.InvoiceType.Dto
{
    public class GetInvoiceTypeForEditOutput
    {
        public CreateOrEditInvoiceTypeDto InvoiceType { get; set; }
    }
}
