﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.InvoiceType.Dto
{
    public class GetInvoiceTypeForViewDto
    {
        public InvoiceTypeDto InvoiceType { get; set; }
    }
}
