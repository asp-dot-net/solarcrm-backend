﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.InvoiceType.Dto
{
    public class CreateOrEditInvoiceTypeDto : EntityDto<int?>
    {
        [Required]
        [StringLength(InvoiceTypeConsts.MaxNameLength, MinimumLength = InvoiceTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
