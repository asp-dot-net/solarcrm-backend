﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.InvoiceType.Dto
{
    public class GetAllInvoiceTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
