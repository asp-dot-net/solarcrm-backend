﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLibrary.Dto
{
    public class GetAllDocumentLibraryForExcelInput
    {
        public string Filter { get; set; }
    }
}
