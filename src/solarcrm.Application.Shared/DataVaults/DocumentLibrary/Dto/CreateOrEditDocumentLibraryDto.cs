﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLibrary.Dto
{
    public class CreateOrEditDocumentLibraryDto : EntityDto<int?>
    {

       
        public string Title { get; set; }
        public string FileToken { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileType { get; set; }

        public bool IsActive { get; set; }
        public int? TenantId { get; set; }

    }
}
