﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLibrary.Dto
{
    public class GetAllDocumentLibraryInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

    }
}
