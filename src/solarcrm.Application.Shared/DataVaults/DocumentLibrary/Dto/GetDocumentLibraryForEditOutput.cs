﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLibrary.Dto
{
    public class GetDocumentLibraryForEditOutput
    {
        public CreateOrEditDocumentLibraryDto documentLibrary { get; set; }
    }
}
