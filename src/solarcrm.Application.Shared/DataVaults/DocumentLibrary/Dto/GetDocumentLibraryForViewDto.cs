﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DocumentLibrary.Dto
{
    public class GetDocumentLibraryForViewDto
    {
        public DocumentLibraryDto documentLibrary { get; set; }
    }
}
