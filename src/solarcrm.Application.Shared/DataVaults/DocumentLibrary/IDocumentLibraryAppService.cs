﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.DocumentLibrary.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DocumentLibrary
{
    public interface IDocumentLibraryAppService
    {
        Task<PagedResultDto<GetDocumentLibraryForViewDto>> GetAll(GetAllDocumentLibraryInput input);

        Task<GetDocumentLibraryForViewDto> GetDocumentLibraryForView(int id);

        Task<FileDto> GetDocumentLibraryToExcel(GetAllDocumentLibraryForExcelInput input);
        Task<GetDocumentLibraryForEditOutput> GetDocumentLibraryForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDocumentLibraryDto input);

        Task Delete(EntityDto input);
    }

}
