﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.LeadType.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadType
{
    public interface ILeadTypeAppService
    {
        Task<PagedResultDto<GetLeadTypeForViewDto>> GetAll(GetAllLeadTypeInput input);

        Task<GetLeadTypeForViewDto> GetLeadTypeForView(int id);

        Task<GetLeadTypeForEditOutput> GetLeadTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditLeadTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetLeadTypeToExcel(GetAllLeadTypeForExcelInput input);

        //Task<List<LeadTypeLookupTableDto>> GetAllLeadTypeForDropdown();
    }
}
