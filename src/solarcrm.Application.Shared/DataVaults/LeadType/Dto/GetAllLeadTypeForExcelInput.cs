﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.LeadType.Dto
{
    public class GetAllLeadTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
