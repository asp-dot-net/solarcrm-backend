﻿using solarcrm.DataVaults.LeadType.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadType.Dto
{
    public class GetLeadTypeForViewDto
    {
        public LeadTypeDto LeadType { get; set; }
    }
}
