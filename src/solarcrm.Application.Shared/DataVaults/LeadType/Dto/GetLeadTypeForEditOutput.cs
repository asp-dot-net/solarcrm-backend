﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadType.Dto
{
    public class GetLeadTypeForEditOutput
    {
        public CreateOrEditLeadTypeDto LeadType { get; set; }
    }
}
