﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.LeadType.Dto
{
    public class LeadTypeLookupTableDto : Abp.Domain.Entities.Entity
    {
        public string DisplayName { get; set; }
    }
}
