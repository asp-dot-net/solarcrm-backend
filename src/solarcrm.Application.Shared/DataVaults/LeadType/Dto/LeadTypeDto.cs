﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.LeadType.Dto
{
    public class LeadTypeDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
