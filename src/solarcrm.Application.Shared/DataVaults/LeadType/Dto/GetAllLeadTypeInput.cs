﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.LeadType.Dto
{
    public class GetAllLeadTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
