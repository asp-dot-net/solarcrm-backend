﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.LeadType.Dto
{
    public class CreateOrEditLeadTypeDto : EntityDto<int?>
    {
        [Required]
        [StringLength(LeadTypeConsts.MaxNameLength, MinimumLength = LeadTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
