﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.District.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.District
{
    public interface IDistrictAppService
    {
        Task<PagedResultDto<GetDistrictForViewDto>> GetAll(GetAllDistrictInput input);

        Task<GetDistrictForViewDto> GetDistrictForView(int id);

        Task<GetDistrictForEditOutput> GetDistrictForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDistrictDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDistrictToExcel(GetAllDistrictForExcelInput input);

        Task<List<DistrictLookupTableDto>> GetAllDistrictForDropdown();
    }
}
