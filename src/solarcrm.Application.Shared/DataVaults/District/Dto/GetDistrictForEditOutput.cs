﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.District.Dto
{
    public class GetDistrictForEditOutput
    {
        public CreateOrEditDistrictDto districts { get; set; }
    }
}
