﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.District.Dto
{
    public class GetAllDistrictForExcelInput
    {
        public string Filter { get; set; }
    }
}
