﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.District.Dto
{
    public class GetDistrictForViewDto
    {
        public DistrictDto Districts { get; set; }
    }
}
