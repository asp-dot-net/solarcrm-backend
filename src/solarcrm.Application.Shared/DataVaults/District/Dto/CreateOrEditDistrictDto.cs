﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.District.Dto
{
    public class CreateOrEditDistrictDto : EntityDto<int?>
    {       
        [Required]
        public int StateId { get; set; }

        [Required]
        [StringLength(DistrictConsts.MaxNameLength, MinimumLength = DistrictConsts.MinNameLength)]
        public string Name { get; set; }


        public bool IsActive { get; set; }
    }
}
