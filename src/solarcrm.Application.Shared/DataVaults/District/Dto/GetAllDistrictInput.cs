﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.District.Dto
{
    public class GetAllDistrictInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
