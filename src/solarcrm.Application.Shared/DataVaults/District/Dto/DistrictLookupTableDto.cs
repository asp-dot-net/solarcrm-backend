﻿namespace solarcrm.DataVaults.District.Dto
{
    public class DistrictLookupTableDto : Abp.Domain.Entities.Entity
    {
        public string DisplayName { get; set; }
    }
}
