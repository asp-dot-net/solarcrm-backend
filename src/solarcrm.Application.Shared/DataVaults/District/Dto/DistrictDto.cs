﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.District.Dto
{
    public class DistrictDto : EntityDto
    {
       
        public string StateName { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
