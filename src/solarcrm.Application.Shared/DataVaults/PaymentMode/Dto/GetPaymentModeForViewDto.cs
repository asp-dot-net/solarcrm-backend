﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentMode.Dto
{
    public class GetPaymentModeForViewDto
    {
        public PaymentModeDto paymentMode { get; set; }
    }
}
