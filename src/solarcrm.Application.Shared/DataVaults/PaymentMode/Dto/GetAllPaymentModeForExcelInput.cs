﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentMode.Dto
{
    public class GetAllPaymentModeForExcelInput
    {
        public string Filter { get; set; }
    }
}
