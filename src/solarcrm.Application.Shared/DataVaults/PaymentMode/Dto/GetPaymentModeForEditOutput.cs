﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentMode.Dto
{
    public class GetPaymentModeForEditOutput
    {
        public CreateOrEditPaymentModeDto PayBy { get; set; }
    }
}
