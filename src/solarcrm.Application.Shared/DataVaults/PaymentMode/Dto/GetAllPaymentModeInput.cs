﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.PaymentMode.Dto
{
    public class GetAllPaymentModeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
