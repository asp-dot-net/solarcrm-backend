﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.PaymentMode.Dto;
using solarcrm.Dto;
using System;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PaymentMode
{
    public interface IPaymentModeAppService
    {
        Task<PagedResultDto<GetPaymentModeForViewDto>> GetAll(GetAllPaymentModeInput input);

        Task<GetPaymentModeForViewDto> GetPaymentModeForView(int id);

        Task<GetPaymentModeForEditOutput> GetPaymentModeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPaymentModeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetPaymentModeToExcel(GetAllPaymentModeForExcelInput input);
    }
}
