﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Vehical.Dto
{
    public class GetAllVehicalInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
