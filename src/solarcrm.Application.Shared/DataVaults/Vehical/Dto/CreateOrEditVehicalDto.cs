﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.Vehical.Dto
{
    public class CreateOrEditVehicalDto : EntityDto<int?>
    {
        [Required]
        [StringLength(VehicalConsts.MaxNameLength, MinimumLength = VehicalConsts.MinNameLength)]
        public string VehicalType { get; set; }


        [StringLength(VehicalConsts.MaxNameLength, MinimumLength = VehicalConsts.MinNameLength)]
        public string VehicalRegNo { get; set; }

        public bool IsActive { get; set; }

    }
}
