﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Vehical.Dto
{
    public class VehicalDto : EntityDto
    {
        public virtual string VehicalType { get; set; }
        public virtual string VehicalRegNo { get; set; }
        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
