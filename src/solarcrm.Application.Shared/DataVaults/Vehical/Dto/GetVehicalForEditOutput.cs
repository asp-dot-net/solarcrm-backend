﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Vehical.Dto
{
    public class GetVehicalForEditOutput
    {
        public CreateOrEditVehicalDto Vehical { get; set; }
    }
}
