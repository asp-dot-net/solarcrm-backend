﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Vehical.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Vehical
{
    public interface IVehicalAppService
    {
        Task<PagedResultDto<GetVehicalForViewDto>> GetAll(GetAllVehicalInput input);

        Task<GetVehicalForViewDto> GetVehicalForView(int id);

        Task<GetVehicalForEditOutput> GetVehicalForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditVehicalDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetVehicalToExcel(GetAllVehicalForExcelInput input);
    }
}
