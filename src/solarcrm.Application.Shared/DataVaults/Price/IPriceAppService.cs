﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Price.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price
{
    public interface IPriceAppService
    {
        Task<PagedResultDto<GetPriceForViewDto>> GetAll(GetAllPriceInput input);

        Task<GetPriceForViewDto> GetPriceForView(int id);

        Task<GetPriceForEditOutput> GetPriceForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPriceDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetPriceToExcel(GetAllPriceForExcelInput input);

        Task<GetPriceForViewDto> GetPriceBySystemSize(GetPriceInput input);
    }
}
