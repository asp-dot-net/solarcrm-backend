﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class CreateOrEditPriceDto : EntityDto<int?>
    {
        public int TenderId { get; set; }

        public decimal CentralRate { get; set; }
        public decimal StateRate { get; set; }

        public decimal SystemSize { get; set; }

        public decimal ActualPrice { get; set; }

        public decimal Subcidy40 { get; set; }

        public decimal Subcidy20 { get; set; }

        public List<string> AreaTypeList { get; set; }
        public int MeterType { get; set; }

        public bool IsActive { get; set; }
    }
}
