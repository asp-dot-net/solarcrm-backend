﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class GetPriceForEditOutput
    {
        public CreateOrEditPriceDto price { get; set; }

    }
}
