﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class GetAllPriceForExcelInput
    {
        public string Filter { get; set; }
    }
}
