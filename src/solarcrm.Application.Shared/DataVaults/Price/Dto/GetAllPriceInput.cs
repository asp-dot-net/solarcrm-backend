﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class GetAllPriceInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
