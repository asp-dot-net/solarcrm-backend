﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class ImportPriceFromExcelJobArgs
    {
        public int? TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }
    }
}
