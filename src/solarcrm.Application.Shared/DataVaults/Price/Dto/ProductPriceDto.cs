﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class ProductPriceDto
    {
        public decimal? StateRate { get; set; }
        public decimal? CentralRate { get; set; }
        public decimal? SystemSize { get; set; }

        public decimal? ActualPrice { get; set; }

        public decimal? Subcidy40 { get; set; }

        public decimal? Subcidy20 { get; set; }

        
        public int? MeterType { get; set; }  //0= stands for Yes ,1= Stands for No 


    }
}
