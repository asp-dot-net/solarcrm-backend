﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class GetPriceInput
    {
        public decimal? SystemSize { get; set; }

        public string AreaType { get; set; }
    }
}
