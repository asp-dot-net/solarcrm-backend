﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class GetPriceForViewDto
    {
        public PriceDto price { get; set; }

        public ProductPriceDto productPriceDto { get; set; }
    }
}
