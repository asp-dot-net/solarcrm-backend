﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Price.Dto
{
    public class PriceDto : EntityDto
    {
        public string TenderName { get; set; }
        
        public decimal? CentralRate { get; set; }
        public decimal? StateRate { get; set; }
        public decimal? SystemSize { get; set; }

        public decimal? ActualPrice { get; set; }

        public decimal? Subcidy40 { get; set; }

        public decimal? Subcidy20 { get; set; }
        
        public int? MeterType { get; set; }  //0= stands for Yes ,1= Stands for No 

        public bool IsActive { get; set; }
        public List<string> AreaTypeName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
