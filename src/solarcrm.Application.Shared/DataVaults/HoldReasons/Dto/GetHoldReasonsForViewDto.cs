﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HoldReasons.Dto
{
    public class GetHoldReasonsForViewDto
    {
        public HoldReasonsDto holdreasons { get; set; }
    }
}
