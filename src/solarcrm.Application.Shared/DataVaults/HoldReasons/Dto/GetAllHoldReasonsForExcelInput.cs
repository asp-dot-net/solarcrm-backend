﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HoldReasons.Dto
{
    public class GetAllHoldReasonsForExcelInput
    {
        public string Filter { get; set; }
    }
}
