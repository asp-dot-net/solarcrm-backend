﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HoldReasons.Dto
{
    public class GetHoldReasonsForEditOutput
    {
        public CreateOrEditHoldReasonsDto holdreasons { get; set; }
    }
}
