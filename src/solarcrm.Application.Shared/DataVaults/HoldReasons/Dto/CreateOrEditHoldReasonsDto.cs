﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.HoldReasons.Dto
{
    public class CreateOrEditHoldReasonsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(HoldReasonsConsts.MaxNameLength, MinimumLength = HoldReasonsConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
