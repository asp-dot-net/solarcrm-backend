﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HoldReasons.Dto
{
    public class GetAllHoldReasonsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
