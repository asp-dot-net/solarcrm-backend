﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.HoldReasons.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.HoldReasons
{
    public interface IHoldReasonsAppService
    {
        Task<PagedResultDto<GetHoldReasonsForViewDto>> GetAll(GetAllHoldReasonsInput input);

        Task<GetHoldReasonsForViewDto> GetHoldReasonsForView(int id);

        Task<GetHoldReasonsForEditOutput> GetHoldReasonsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditHoldReasonsDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetHoldReasonsToExcel(GetAllHoldReasonsForExcelInput input);
    }
}
