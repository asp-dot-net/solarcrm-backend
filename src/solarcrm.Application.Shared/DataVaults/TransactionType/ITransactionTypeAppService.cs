﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.TransactionType.Dto;
using solarcrm.Dto;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.TransactionType
{
    public interface ITransactionTypeAppService
    {
        Task<PagedResultDto<GetTransactionTypeForViewDto>> GetAll(GetAllTransactionTypeInput input);

        Task<GetTransactionTypeForEditOutput> GetTransactionTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTransactionTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTransactionTypeToExcel(GetAllTransactionTypeForExcelInput input);
    }
}
