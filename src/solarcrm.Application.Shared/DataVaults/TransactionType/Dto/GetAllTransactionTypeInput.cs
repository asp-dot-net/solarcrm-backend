﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.TransactionType.Dto
{
    public class GetAllTransactionTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
