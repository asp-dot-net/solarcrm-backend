﻿using Abp.Domain.Entities;

namespace solarcrm.DataVaults.TransactionType.Dto
{
    public class TransactionTypeDto : Abp.Domain.Entities.Entity
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
