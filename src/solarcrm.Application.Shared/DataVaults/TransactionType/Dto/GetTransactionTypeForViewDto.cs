﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.TransactionType.Dto
{
    public class GetTransactionTypeForViewDto
    {
        public TransactionTypeDto TransactionType { get; set; }
    }
}
