﻿using Abp.Domain.Entities;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.TransactionType.Dto
{
    public class CreateOrEditTransactionTypeDto : Entity<int?>
    {
        [Required]
        [StringLength(TransactionTypeConsts.MaxNameLength, MinimumLength = TransactionTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
