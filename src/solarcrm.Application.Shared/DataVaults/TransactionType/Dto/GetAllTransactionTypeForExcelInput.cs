﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.TransactionType.Dto
{
    public class GetAllTransactionTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
