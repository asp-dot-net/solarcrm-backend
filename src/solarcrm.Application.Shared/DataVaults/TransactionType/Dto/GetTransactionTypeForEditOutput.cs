﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.TransactionType.Dto
{
    public class GetTransactionTypeForEditOutput
    {
        public CreateOrEditTransactionTypeDto TransactionType { get; set; }
    }
}
