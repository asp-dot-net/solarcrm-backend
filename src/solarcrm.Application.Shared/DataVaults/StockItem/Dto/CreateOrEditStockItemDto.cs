﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using solarcrm.DataVaults.StockItemLocation.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.StockItem.Dto
{
    public class CreateOrEditStockItemDto : EntityDto<int?>
    {
        [Required]
        [StringLength(StockItemConsts.MaxNameLength, MinimumLength = StockItemConsts.MinNameLength)]
        public string Name { get; set; }

        [StringLength(StockItemConsts.MaxManufacturerLength, MinimumLength = StockItemConsts.MinManufacturerLength)]
        public string Manufacturer { get; set; }

        [StringLength(StockItemConsts.MaxModelLength, MinimumLength = StockItemConsts.MinModelLength)]
        public string Model { get; set; }

        [StringLength(StockItemConsts.MaxSeriesLength, MinimumLength = StockItemConsts.MinSeriesLength)]
        public string Series { get; set; }

        public decimal? Size { get; set; }

        public int? Phase { get; set; }
        public int? Seq { get; set; }
        public string UOM { get; set; }
        public int CategoryId { get; set; }
        public List<long> OrganizationUnits { get; set; }
        public bool Active { get; set; }

        public List<CreateOrEditStockItemLocationDto> StockItemLocations { get; set; }
    }
}
