﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockItem.Dto
{
    public class GetAllStockItemForExcelInput
    {
        public string Filter { get; set; }
    }
}
