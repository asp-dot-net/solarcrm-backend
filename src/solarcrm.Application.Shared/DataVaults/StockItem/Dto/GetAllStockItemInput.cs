﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockItem.Dto
{
    public class GetAllStockItemInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
