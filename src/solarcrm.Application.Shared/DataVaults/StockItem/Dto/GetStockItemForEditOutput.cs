﻿using solarcrm.DataVaults.StockItemLocation.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockItem.Dto
{
    public class GetStockItemForEditOutput
    {
        public CreateOrEditStockItemDto StockItem { get; set; }
    }
}
