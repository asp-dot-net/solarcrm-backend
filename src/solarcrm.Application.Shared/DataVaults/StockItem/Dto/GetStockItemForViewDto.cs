﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockItem.Dto
{
    public class GetStockItemForViewDto
    {
        public StockItemDto StockItem { get; set; }
    }
}
