﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockItem.Dto
{
    public class StockItemDto : EntityDto
    {
        public string Name { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }
        
        public string Series { get; set; }

        public decimal? Size { get; set; }

        public int? Phase { get; set; }

        public string Category { get; set; }

        public int? Sequence { get; set; }

        public string UOM { get; set; }

        public List<string> OrganizationUnitsName { get; set; }

        public bool IsActive { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }
    }
}
