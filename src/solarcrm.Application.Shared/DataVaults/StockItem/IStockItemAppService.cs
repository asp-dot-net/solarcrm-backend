﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.StockItem.Dto;
using solarcrm.DataVaults.StockItemLocation.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockItem
{
    public interface IStockItemAppService
    {
        Task<PagedResultDto<GetStockItemForViewDto>> GetAll(GetAllStockItemInput input);

        //Task<GetStockCategoryForViewDto> GetStockCategoryForView(int id);

        Task<GetStockItemForEditOutput> GetStockItemForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStockItemDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetStockItemToExcel(GetAllStockItemForExcelInput input);

        Task SaveStockIteamDocument(string FileToken, string FileNames, int id);
    }
}
