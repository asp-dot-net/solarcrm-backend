﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Teams.Dto;
using solarcrm.Dto;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Teams
{
    public interface ITeamsAppService
    {
        Task<PagedResultDto<GetTeamsForViewDto>> GetAll(GetAllTeamsInput input);

        Task<GetTeamsForViewDto> GetTeamsForView(int id);

        Task<GetTeamsForEditOutput> GetTeamsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditTeamsDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetTeamsToExcel(GetAllTeamsForExcelInput input);
    }
}
