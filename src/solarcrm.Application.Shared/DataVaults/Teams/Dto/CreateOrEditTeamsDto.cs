﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.Teams.Dto
{
    public class CreateOrEditTeamsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(TeamsConsts.MaxNameLength, MinimumLength = TeamsConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
