﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.Teams.Dto
{
    public class TeamsDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
