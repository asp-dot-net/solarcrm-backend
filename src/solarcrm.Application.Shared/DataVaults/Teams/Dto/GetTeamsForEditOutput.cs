﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Teams.Dto
{
    public class GetTeamsForEditOutput
    {
        public CreateOrEditTeamsDto Teams { get; set; }
    }
}
