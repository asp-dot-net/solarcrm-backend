﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Teams.Dto
{
    public class GetAllTeamsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
