﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Teams.Dto
{
    public class GetTeamsForViewDto
    {
        public TeamsDto Teams { get; set; }
    }
}
