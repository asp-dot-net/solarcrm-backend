﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Teams.Dto
{
    public class GetAllTeamsForExcelInput
    {
        public string Filter { get; set; }
    }
}
