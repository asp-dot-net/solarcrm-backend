﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HeightStructure.Dto
{
    public class GetHeightStructureForEditOutput
    {
        public CreateOrEditHeightStructureDto HeightStructures { get; set; }
    }
}
