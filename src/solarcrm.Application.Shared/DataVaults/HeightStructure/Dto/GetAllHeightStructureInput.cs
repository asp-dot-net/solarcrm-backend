﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HeightStructure.Dto
{
    public class GetAllHeightStructureInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
