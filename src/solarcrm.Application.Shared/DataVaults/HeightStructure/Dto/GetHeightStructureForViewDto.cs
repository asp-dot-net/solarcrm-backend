﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HeightStructure.Dto
{
    public class GetHeightStructureForViewDto
    {
        public HeightStructureDto HeightStructures { get; set; }
    }
}
