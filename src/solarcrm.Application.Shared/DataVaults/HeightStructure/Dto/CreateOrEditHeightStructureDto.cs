﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.HeightStructure.Dto
{
    public class CreateOrEditHeightStructureDto : EntityDto<int?>
    {
        [Required]
        [StringLength(HeightStructureConsts.MaxNameLength, MinimumLength = HeightStructureConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
