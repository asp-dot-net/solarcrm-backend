﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.HeightStructure.Dto
{
    public class GetAllHeightStructureForExcelInput
    {
        public string Filter { get; set; }
    }
}
