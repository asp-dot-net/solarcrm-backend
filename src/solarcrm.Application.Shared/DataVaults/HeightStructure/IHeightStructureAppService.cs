﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.HeightStructure.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.HeightStructure
{
    public interface IHeightStructureAppService
    {
        Task<PagedResultDto<GetHeightStructureForViewDto>> GetAll(GetAllHeightStructureInput input);

        Task<GetHeightStructureForViewDto> GetHeightStructureForView(int id);

        Task<GetHeightStructureForEditOutput> GetHeightStructureForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditHeightStructureDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetHeightStructureToExcel(GetAllHeightStructureForExcelInput input);

        Task<List<HeightStructureLookupTable>> GetAllHeightStructureForDropdown();
    }
}
