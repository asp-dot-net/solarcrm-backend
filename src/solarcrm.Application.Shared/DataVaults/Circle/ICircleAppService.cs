﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Circle.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Circle
{
    public interface ICircleAppService
    {
        Task<PagedResultDto<GetCircleForViewDto>> GetAll(GetAllCircleInput input);

        Task<GetCircleForViewDto> GetCircleForView(int id);

        Task<GetCircleForEditOutput> GetCircleForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCircleDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetCircleToExcel(GetAllCircleForExcelInput input);
    }
}
