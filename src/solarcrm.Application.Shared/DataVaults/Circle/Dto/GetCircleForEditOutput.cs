﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Circle.Dto
{
    public class GetCircleForEditOutput
    {
        public CreateOrEditCircleDto Circle { get; set; }
    }
}
