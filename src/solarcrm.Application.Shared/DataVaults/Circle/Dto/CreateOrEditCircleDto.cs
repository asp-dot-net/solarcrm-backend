﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.Circle.Dto
{
    public class CreateOrEditCircleDto : EntityDto<int?>
    {
        [Required]
        [StringLength(CircleConsts.MaxNameLength, MinimumLength = CircleConsts.MinNameLength)]
        public string Name { get; set; }

        public  int? DiscomId { get; set; }

        public  int? DivisionId { get; set; }

        public  int? SubDivisionId { get; set; }

        public bool IsActive { get; set; }
    }
}
