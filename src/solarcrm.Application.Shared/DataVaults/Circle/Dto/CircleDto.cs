﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Circle.Dto
{
    public class CircleDto : EntityDto
    {
        public string Name { get; set; }

         public string DiscomName { get; set; }

        public string DivisionName { get; set; }

        public string SubDivisionName { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
