﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Circle.Dto
{
    public class GetAllCircleInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
