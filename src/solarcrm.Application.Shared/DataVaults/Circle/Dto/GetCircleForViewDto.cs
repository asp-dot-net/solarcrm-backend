﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Circle.Dto
{
    public class GetCircleForViewDto
    {
        public CircleDto circle { get; set; }
    }
}
