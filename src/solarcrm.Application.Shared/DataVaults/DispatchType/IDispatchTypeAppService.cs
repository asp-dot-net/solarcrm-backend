﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.DispatchType.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DispatchType
{
    public interface IDispatchTypeAppService
    {
        Task<PagedResultDto<GetDispatchTypeForViewDto>> GetAll(GetAllDispatchTypeInput input);

        Task<GetDispatchTypeForEditOutput> GetDispatchTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDispatchTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDispatchTypeToExcel(GetAllDispatchTypeForExcelInput input);

        Task<List<GetAllDispatchTypeForDropdown>> GetAllDispatchTypeForDropdown();
    }
}
