﻿using solarcrm.DataVaults.Division.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DispatchType.Dto
{
    public class GetDispatchTypeForEditOutput
    {
        public CreateOrEditDispatchTypeDto DispatchType { get; set; }
    }
}
