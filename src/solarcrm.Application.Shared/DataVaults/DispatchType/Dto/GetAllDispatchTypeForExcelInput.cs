﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DispatchType.Dto
{
    public class GetAllDispatchTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
