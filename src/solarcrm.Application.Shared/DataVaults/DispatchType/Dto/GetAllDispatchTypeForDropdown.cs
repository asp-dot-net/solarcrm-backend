﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DispatchType.Dto
{
    public class GetAllDispatchTypeForDropdown : Abp.Domain.Entities.Entity
    {
        public string Name { get; set; }
    }
}
