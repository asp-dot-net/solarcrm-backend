﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DispatchType.Dto
{
    public class GetAllDispatchTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
