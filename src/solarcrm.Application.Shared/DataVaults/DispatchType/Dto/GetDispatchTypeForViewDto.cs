﻿using solarcrm.DataVaults.Division.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.DispatchType.Dto
{
    public class GetDispatchTypeForViewDto
    {
        public DispatchTypeDto DispatchType { get; set; }
    }
}
