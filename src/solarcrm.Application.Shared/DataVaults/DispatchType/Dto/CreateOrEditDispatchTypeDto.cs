﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.DispatchType.Dto
{
    public class CreateOrEditDispatchTypeDto : EntityDto<int?>
    {
        [Required]
        [StringLength(DivisionConsts.MaxNameLength, MinimumLength = DivisionConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
