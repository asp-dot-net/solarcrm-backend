﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SubDivision.Dto
{
    public class GetAllSubDivisionInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
