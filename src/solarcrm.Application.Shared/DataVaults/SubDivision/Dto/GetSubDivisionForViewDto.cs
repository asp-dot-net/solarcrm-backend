﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SubDivision.Dto
{
    public class GetSubDivisionForViewDto
    {
        public SubDivisionDto SubDivision { get; set; }
    }
}
