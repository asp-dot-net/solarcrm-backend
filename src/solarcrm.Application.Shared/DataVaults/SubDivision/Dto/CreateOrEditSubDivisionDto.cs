﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.SubDivision.Dto
{
    public class CreateOrEditSubDivisionDto : EntityDto<int?>
    {
        [Required]
        [StringLength(SubDivisionConsts.MaxNameLength, MinimumLength = SubDivisionConsts.MinNameLength)]
        public string Name { get; set; }

        public int DivisionId { get; set; }

        public bool IsActive { get; set; }
    }
}
