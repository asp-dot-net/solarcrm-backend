﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SubDivision.Dto
{
    public class GetAllSubDivisionForExcelInput
    {
        public string Filter { get; set; }
    }
}
