﻿using Abp.Domain.Entities;
using System;

namespace solarcrm.DataVaults.SubDivision.Dto
{
    public class SubDivisionDto : Abp.Domain.Entities.Entity
    {
        public string Name { get; set; }

        public string Division { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
