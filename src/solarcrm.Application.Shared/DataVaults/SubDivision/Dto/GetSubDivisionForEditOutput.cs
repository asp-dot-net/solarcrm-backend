﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.SubDivision.Dto
{
    public class GetSubDivisionForEditOutput
    {
        public CreateOrEditSubDivisionDto SubDivision { get; set; }
    }
}
