﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using solarcrm.DataVaults.SubDivision.Dto;
using solarcrm.Dto;

namespace solarcrm.DataVaults.SubDivision
{
    public interface ISubDivisionAppService
    {
        Task<PagedResultDto<GetSubDivisionForViewDto>> GetAll(GetAllSubDivisionInput input);

        //Task<GetSubDivisionForViewDto> GetSubDivisionForView(int id);

        Task<GetSubDivisionForEditOutput> GetSubDivisionForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditSubDivisionDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetSubDivisionToExcel(GetAllSubDivisionForExcelInput input);
    }
}
