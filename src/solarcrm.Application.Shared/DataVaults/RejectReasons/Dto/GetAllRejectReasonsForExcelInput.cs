﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.RejectReasons.Dto
{
    public class GetAllRejectReasonsForExcelInput
    {
        public string Filter { get; set; }
    }
}
