﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.RejectReasons.Dto
{
    public class GetAllRejectReasonsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
