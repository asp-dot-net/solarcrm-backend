﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.RejectReasons.Dto
{
    public class GetRejectReasonsForViewDto
    {
        public RejectReasonsDto rejectreasons { get; set; }
    }
}
