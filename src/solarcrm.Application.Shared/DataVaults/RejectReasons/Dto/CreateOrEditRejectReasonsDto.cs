﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.DataVaults.RejectReasons.Dto
{
    public class CreateOrEditRejectReasonsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(RejectReasonsConsts.MaxNameLength, MinimumLength = RejectReasonsConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
