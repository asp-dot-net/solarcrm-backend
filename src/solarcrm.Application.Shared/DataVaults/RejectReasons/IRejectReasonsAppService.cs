﻿
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.RejectReasons.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.RejectReasons
{
    public interface IRejectReasonsAppService
    {
        Task<PagedResultDto<GetRejectReasonsForViewDto>> GetAll(GetAllRejectReasonsInput input);

        Task<GetRejectReasonsForViewDto> GetRejectReasonsForView(int id);

        Task<GetRejectReasonsForEditOutput> GetRejectReasonsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditRejectReasonsDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetRejectReasonsToExcel(GetAllRejectReasonsForExcelInput input);
    }
}
