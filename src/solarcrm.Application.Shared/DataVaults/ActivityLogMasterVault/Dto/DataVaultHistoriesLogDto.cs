﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Dto
{
    public class DataVaultHistoriesLogDto : FullAuditedEntity
    {
        public string FieldName { get; set; }

        public string prevValue { get; set; }

        public string curValue { get; set; }
        public string ActionName { get; set; }

        public bool IsActive { get; set; }

        public string Lastmodifiedbyuser { get; set; }
    }
}
