﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Dto
{
    public class DataVaultLogDto : EntityDto
    {
        public string SectionName { get; set; }

        public string UserName { get; set; }

        public string IpAddress { get; set; }

        public DateTime? LastModificationTime { get; set; }
        
        public string ActionName { get; set; }

        public string ActionNote { get; set; }

        public int SectionIsd { get; set; }

    }
}
