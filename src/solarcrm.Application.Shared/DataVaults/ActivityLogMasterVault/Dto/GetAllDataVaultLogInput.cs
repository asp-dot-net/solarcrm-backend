﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Dto
{
    public class GetAllDataVaultLogInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
