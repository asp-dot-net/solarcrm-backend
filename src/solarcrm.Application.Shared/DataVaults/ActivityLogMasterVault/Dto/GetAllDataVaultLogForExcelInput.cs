﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Dto
{
    public class GetAllDataVaultLogForExcelInput
    {
        public string Filter { get; set; }
    }
}
