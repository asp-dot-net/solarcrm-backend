﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Dto
{
    public class GetDataVaultsHistoriesLogForViewDto
    {
        public DataVaultHistoriesLogDto DataVaultHistories { get; set; }
    }
}
