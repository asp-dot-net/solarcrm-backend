﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ActivityLogMasterVault.Dto
{
    public class GetDataVaultLogForViewDto
    {
        public DataVaultLogDto DataVault { get; set; }
    }
}
