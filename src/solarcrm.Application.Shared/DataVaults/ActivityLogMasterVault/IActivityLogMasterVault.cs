﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.ActivityLogMasterVault.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ActivityLogMasterVault
{
    public interface IActivityLogMasterVault
    {
        Task<PagedResultDto<GetDataVaultLogForViewDto>> GetAllDataVaultLog(GetAllDataVaultLogInput input);

        Task<List<DataVaultLogDto>> GetDataVaultLogForView(int sectionId);

        Task<List<DataVaultHistoriesLogDto>> GetDataVaultHistoriesLogForView(int id);

        //Task<FileDto> GetDataVaultToExcel(GetAllDataVaultLogForExcelInput input);
    }
}
