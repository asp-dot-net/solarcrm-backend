﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Department.Dto;
using solarcrm.Dto;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Department
{
    public interface IDepartmentAppService
    {
        Task<PagedResultDto<GetDepartmentForViewDto>> GetAll(GetAllDepartmentInput input);

        Task<GetDepartmentForViewDto> GetDepartmentForView(int id);

        Task<GetDepartmentForEditOutput> GetDepartmentForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDepartmentDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDepartmentToExcel(GetAllDepartmentForExcelInput input);
       
    }
}
