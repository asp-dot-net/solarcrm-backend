﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Department.Dto
{
    public class GetDepartmentForEditOutput
    {
        public CreateOrEditDepartmentDto Department { get; set; }
    }
}
