﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Department.Dto
{
    public class GetAllDepartmentForExcelInput
    {
        public string Filter { get; set; }
    }
}
