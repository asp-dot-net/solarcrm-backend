﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.Department.Dto
{
    public class DepartmentDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
