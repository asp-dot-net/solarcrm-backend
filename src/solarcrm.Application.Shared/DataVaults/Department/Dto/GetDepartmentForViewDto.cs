﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Department.Dto
{
    public class GetDepartmentForViewDto
    {
        public DepartmentDto Department { get; set; }
    }
}
