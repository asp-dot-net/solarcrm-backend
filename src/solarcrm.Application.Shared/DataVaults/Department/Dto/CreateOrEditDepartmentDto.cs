﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.Department.Dto
{
    public class CreateOrEditDepartmentDto : EntityDto<int?>
    {
        [Required]
        [StringLength(DepartmentConsts.MaxNameLength, MinimumLength = DepartmentConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
