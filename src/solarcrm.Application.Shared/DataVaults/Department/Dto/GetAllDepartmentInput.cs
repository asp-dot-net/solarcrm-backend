﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.Department.Dto
{
    public class GetAllDepartmentInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
