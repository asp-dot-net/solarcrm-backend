﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.State.Dto
{
    public class GetStateForEditOutput
    {
        public CreateOrEditStateDto states { get; set; }
    }
}
