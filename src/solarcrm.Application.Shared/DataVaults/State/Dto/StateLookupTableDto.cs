﻿namespace solarcrm.DataVaults.State.Dto
{
    public class StateLookupTableDto : Abp.Domain.Entities.Entity
    {
        public string DisplayName { get; set; }
    }
}
