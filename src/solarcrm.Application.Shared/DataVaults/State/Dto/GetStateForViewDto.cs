﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.State.Dto
{
    public class GetStateForViewDto
    {
        public StateDto states { get; set; }
    }
}
