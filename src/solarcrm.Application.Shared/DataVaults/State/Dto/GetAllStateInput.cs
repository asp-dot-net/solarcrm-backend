﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.State.Dto
{
    public class GetAllStateInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
