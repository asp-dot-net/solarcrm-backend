﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.State.Dto
{
    public class CreateOrEditStateDto : EntityDto<int?>
    {
        [Required]
        [StringLength(StateConsts.MaxNameLength, MinimumLength = StateConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
