﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.State.Dto
{
    public class StateDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
