﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.State.Dto
{
    public class GetAllStateForExcelInput
    {
        public string Filter { get; set; }
    }
}
