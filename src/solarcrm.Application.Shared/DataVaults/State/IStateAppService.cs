﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.State.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.State
{
    public interface IStateAppService
    {
        Task<PagedResultDto<GetStateForViewDto>> GetAll(GetAllStateInput input);

        Task<GetStateForViewDto> GetStateForView(int id);

        Task<GetStateForEditOutput> GetStateForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStateDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetStateToExcel(GetAllStateForExcelInput input);

        Task<List<StateLookupTableDto>> GetAllStateForDropdown();
    }
}
