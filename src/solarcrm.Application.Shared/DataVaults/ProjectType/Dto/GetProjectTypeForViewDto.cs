﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ProjectType.Dto
{
    public class GetProjectTypeForViewDto
    {
        public ProjectTypeDto ProjectType { get; set; }
    }
}
