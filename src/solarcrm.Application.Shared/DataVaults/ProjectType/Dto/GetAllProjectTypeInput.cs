﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.ProjectType.Dto
{
    public class GetAllProjectTypeInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
