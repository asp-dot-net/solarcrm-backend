﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.ProjectType.Dto
{
    public class CreateOrEditProjectTypeDto : EntityDto<int?>
    {
        [Required]
        [StringLength(ProjectTypeConsts.MaxNameLength, MinimumLength = ProjectTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
