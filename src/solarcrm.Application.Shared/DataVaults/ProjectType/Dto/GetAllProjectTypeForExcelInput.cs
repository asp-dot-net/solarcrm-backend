﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.ProjectType.Dto
{
    public class GetAllProjectTypeForExcelInput
    {
        public string Filter { get; set; }
    }
}
