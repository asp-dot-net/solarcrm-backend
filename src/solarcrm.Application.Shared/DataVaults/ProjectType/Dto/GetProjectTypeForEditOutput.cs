﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ProjectType.Dto
{
    public class GetProjectTypeForEditOutput
    {
        public CreateOrEditProjectTypeDto ProjectType { get; set; }
    }
}
