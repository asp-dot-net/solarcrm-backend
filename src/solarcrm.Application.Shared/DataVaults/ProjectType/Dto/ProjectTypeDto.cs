﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.ProjectType.Dto
{
    public class ProjectTypeDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
