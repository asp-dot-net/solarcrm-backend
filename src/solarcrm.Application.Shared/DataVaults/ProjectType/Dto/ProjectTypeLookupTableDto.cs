﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.ProjectType.Dto
{
    public class ProjectTypeLookupTableDto : Abp.Domain.Entities.Entity
    {
        public string DisplayName { get; set; }
    }
}
