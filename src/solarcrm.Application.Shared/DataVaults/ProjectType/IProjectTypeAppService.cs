﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.ProjectType.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ProjectType
{
    public interface IProjectTypeAppService
    {
        Task<PagedResultDto<GetProjectTypeForViewDto>> GetAll(GetAllProjectTypeInput input);

        Task<GetProjectTypeForViewDto> GetProjectTypeForView(int id);

        Task<GetProjectTypeForEditOutput> GetProjectTypeForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditProjectTypeDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetProjectTypeToExcel(GetAllProjectTypeForExcelInput input);

        Task<List<ProjectTypeLookupTableDto>> GetAllProjectTypeForDropdown();
    }
}
