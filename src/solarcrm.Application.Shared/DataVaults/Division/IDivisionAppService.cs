﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Division.Dto;
using solarcrm.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Division
{
    public interface IDivisionAppService
    {
        Task<PagedResultDto<GetDivisionForViewDto>> GetAll(GetAllDivisionInput input);

        //Task<GetDivisionForViewDto> GetDivisionForView(int id);

        Task<GetDivisionForEditOutput> GetDivisionForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditDivisionDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetDivisionToExcel(GetAllDivisionForExcelInput input);

        Task<List<GetAllDivisionForDropdown>> GetAllDivisionForDropdown();
    }
}
