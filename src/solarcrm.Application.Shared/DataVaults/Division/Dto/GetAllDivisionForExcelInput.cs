﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Division.Dto
{
    public class GetAllDivisionForExcelInput
    {
        public string Filter { get; set; }
    }
}
