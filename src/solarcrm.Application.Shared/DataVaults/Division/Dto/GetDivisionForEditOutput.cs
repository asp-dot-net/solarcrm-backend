﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Division.Dto
{
    public class GetDivisionForEditOutput
    {
        public CreateOrEditDivisionDto Division { get; set; }
    }
}
