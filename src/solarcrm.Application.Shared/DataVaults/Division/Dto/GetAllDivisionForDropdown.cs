﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Division.Dto
{
    public class GetAllDivisionForDropdown : Abp.Domain.Entities.Entity
    {
        public string Name { get; set; }
    }
}
