﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.Division.Dto
{
    public class GetAllDivisionInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
