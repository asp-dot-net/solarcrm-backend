﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Division.Dto
{
    public class GetDivisionForViewDto
    {
        public DivisionDto Division { get; set; }
    }
}
