﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.Division.Dto
{
    public class DivisionDto : EntityDto
    {
        public string Name { get; set; }

        public string Circle { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
