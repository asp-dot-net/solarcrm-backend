﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.StockCategory.Dto;
using solarcrm.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockCategory
{
    public interface IStockCategoryAppService
    {
        Task<PagedResultDto<GetStockCategoryForViewDto>> GetAll(GetAllStockCategoryInput input);

        Task<GetStockCategoryForViewDto> GetStockCategoryForView(int id);

        Task<GetStockCategoryForEditOutput> GetStockCategoryForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditStockCategoryDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetStockCategoryToExcel(GetAllStockCategoryForExcelInput input);

        Task<List<StockCategoryLookupTableDto>> GetAllStockCategoryForDropdown();
    }
}
