﻿using System;
using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.StockCategory.Dto
{
    public class GetAllStockCategoryForExcelInput
    {
        public string Filter { get; set; }
    }
}
