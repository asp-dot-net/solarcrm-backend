﻿using Abp.Application.Services.Dto;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.DataVaults.StockCategory.Dto
{
    public class CreateOrEditStockCategoryDto : EntityDto<int?>
    {
        [Required]
        [StringLength(StockCategoryConsts.MaxNameLength, MinimumLength = StockCategoryConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
