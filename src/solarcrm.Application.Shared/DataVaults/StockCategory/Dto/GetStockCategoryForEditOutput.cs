﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockCategory.Dto
{
    public class GetStockCategoryForEditOutput
    {
        public CreateOrEditStockCategoryDto StockCategory { get; set; }
    }
}
