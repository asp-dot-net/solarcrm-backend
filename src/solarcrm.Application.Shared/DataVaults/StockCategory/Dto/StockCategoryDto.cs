﻿using Abp.Application.Services.Dto;
using System;

namespace solarcrm.DataVaults.StockCategory.Dto
{
    public class StockCategoryDto : EntityDto
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
