﻿using Abp.Application.Services.Dto;

namespace solarcrm.DataVaults.StockCategory.Dto
{
    public class GetAllStockCategoryInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
