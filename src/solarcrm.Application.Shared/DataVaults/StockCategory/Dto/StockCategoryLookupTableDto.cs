﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockCategory.Dto
{
    public class StockCategoryLookupTableDto : Abp.Domain.Entities.Entity
    {
        public string DisplayName { get; set; }
    }
}
