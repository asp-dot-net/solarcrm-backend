﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.DataVaults.StockCategory.Dto
{
    public class GetStockCategoryForViewDto
    {
        public StockCategoryDto StockCategory { get; set; }
    }
}
