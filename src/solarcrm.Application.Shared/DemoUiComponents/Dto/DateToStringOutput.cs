﻿namespace solarcrm.DemoUiComponents.Dto
{
    public class DateToStringOutput
    {
        public string DateString { get; set; }
    }
}
