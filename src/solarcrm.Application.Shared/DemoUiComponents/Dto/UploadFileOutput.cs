﻿using System;

namespace solarcrm.DemoUiComponents.Dto
{
    public class UploadFileOutput
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
    }
}
