﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace solarcrm.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
