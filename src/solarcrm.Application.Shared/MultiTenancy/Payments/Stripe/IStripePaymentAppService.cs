﻿using System.Threading.Tasks;
using Abp.Application.Services;
using solarcrm.MultiTenancy.Payments.Dto;
using solarcrm.MultiTenancy.Payments.Stripe.Dto;

namespace solarcrm.MultiTenancy.Payments.Stripe
{
    public interface IStripePaymentAppService : IApplicationService
    {
        Task ConfirmPayment(StripeConfirmPaymentInput input);

        StripeConfigurationDto GetConfiguration();

        Task<SubscriptionPaymentDto> GetPaymentAsync(StripeGetPaymentInput input);

        Task<string> CreatePaymentSession(StripeCreatePaymentSessionInput input);
    }
}