﻿using System.Threading.Tasks;
using Abp.Application.Services;
using solarcrm.MultiTenancy.Payments.PayPal.Dto;

namespace solarcrm.MultiTenancy.Payments.PayPal
{
    public interface IPayPalPaymentAppService : IApplicationService
    {
        Task ConfirmPayment(long paymentId, string paypalOrderId);

        PayPalConfigurationDto GetConfiguration();
    }
}
