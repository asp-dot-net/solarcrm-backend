using System.Threading.Tasks;
using Abp.Application.Services;
using solarcrm.Editions.Dto;
using solarcrm.MultiTenancy.Dto;

namespace solarcrm.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}