﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentIssue.Dto
{
    public class CreateOrEditSSPaymentDto : EntityDto<int?>
    {
        public int LeadId { get; set; }
        public int SSPaymentId { get; set; }

        public DateTime? SSDate { get; set; }

        public string SSGSTNo { get; set; }

        public int SSPaymentApprovedById { get; set; }

        public string SSNotes { get; set; }


    }
}
