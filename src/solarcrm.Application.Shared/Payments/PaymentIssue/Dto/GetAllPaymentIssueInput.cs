﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;


namespace solarcrm.Payments.PaymentIssue.Dto
{
    public class GetAllPaymentIssueInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? OrganizationUnit { get; set; }
       
        public List<int> LeadStatusIDS { get; set; }

        public List<int> LeadApplicationStatusFilter { get; set; } //application Filter
        public List<int> EmployeeIdFilter { get; set; }
     
        public List<int> DiscomIdFilter { get; set; }

        public List<int> CircleIdFilter { get; set; }

        public List<int> DivisionIdFilter { get; set; }

        public List<int> SubDivisionIdFilter { get; set; }

        public List<int> SolarTypeIdFilter { get; set; }
        public string BillTypeFilter { get; set; }

        public List<int> PaymentModeIdFilter { get; set; }
        public List<int> PaymentTypeIdFilter { get; set; }

        public decimal? OwingAmountFromFilter { get; set; }
        public decimal? OwingAmountToFilter { get; set; }

        public string FilterbyDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? excelorcsv { get; set; }
    }
}
