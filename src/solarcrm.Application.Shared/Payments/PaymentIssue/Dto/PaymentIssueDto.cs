﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentIssue.Dto
{
    public class PaymentIssueDto : EntityDto
    {
        public virtual int PaymentJobId { get; set; }

        //public int LeadId { get; set; }

        public LeadsDto leaddata { get; set; }

        public int JobId { get; set; }
        public string ProjectNumber { get; set; }


        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }
        public string Email { get; set; }
        public string SolarType { get; set; }
        public string CpName { get; set; }
        public string CpMobile { get; set; }
        public string JobOwnedBy { get; set; }
        public string LeadStatusName { get; set; }

        public int ApplicationStatus { get; set; }

        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }

        public string Comment { get; set; }
        public string Notes { get; set; }

        public DateTime? ApplicationDate { get; set; }
      
        public string LeadStatusColorClass { get; set; }
        public string LeadStatusIconClass { get; set; }

        public object ApplicationStatusdata { get; set; }

        public DateTime? ReminderTime { get; set; }
        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }
        public virtual decimal? TotalProjectCost { get; set; }       

        public virtual decimal? BalanceOwing { get; set; }

        public virtual decimal? AdvancePayment { get; set; }
        public virtual DateTime? LastPaymentDate { get; set; }
        public virtual bool? Is_STC_payment { get; set; }
        public virtual bool? Is_SS_payment { get; set; }
        public virtual bool? PaymentDeliveryOption { get; set; }

        public virtual string PaymentAccountInvoiceNotes { get; set; }

        public virtual DateTime? QuataionSignDate { get; set; }
        public virtual string EastimatePaidStatus { get; set; }
        public virtual DateTime? EastimatePaidDate { get; set; }
        public virtual int PaymentRefferenceNo { get; set; }
    }
}
