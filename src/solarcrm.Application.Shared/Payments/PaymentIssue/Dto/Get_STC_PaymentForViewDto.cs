﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentIssue.Dto
{
    public class Get_STC_PaymentForViewDto : EntityDto<int?>
    {
        public Int32 STCPaymentId { get; set; }

        public DateTime? STCDate { get; set; }

        public Int32? STCPaymentApprovedById { get; set; }

        public string STCNotes { get; set; }
    }
}
