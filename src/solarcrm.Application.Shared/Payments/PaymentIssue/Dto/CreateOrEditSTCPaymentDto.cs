﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentIssue.Dto
{
    public class CreateOrEditSTCPaymentDto : EntityDto<int?>
    {
        public int LeadId { get; set; }
        public int STCPaymentId { get; set; }

        public DateTime? STCDate { get; set; }    

        public int? STCPaymentApprovedById { get; set; }

        public string STCNotes { get; set; }

        
    }
}
