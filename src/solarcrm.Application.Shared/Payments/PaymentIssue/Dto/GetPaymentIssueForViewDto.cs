﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentIssue.Dto
{
    public class GetPaymentIssueForViewDto
    {
        public PaymentIssueDto paymentIssue { get; set; }
        public PaymentIssueSummaryCount paymentIssueSummaryCount { get; set; }
    }
    public class PaymentIssueSummaryCount
    {
        public string Total { get; set; } = "0";
        public string ReceivedAmount { get; set; } = "0";
        public string TotalOwingAmount { get; set; } = "0";
        public string FeasiblityApproved { get; set; } = "0";
        public string EastimatePaid { get; set; } = "0";
        public string TotalKilowatt { get; set; } = "0";
        public string TotalPanel { get; set; } = "0";
    }
}
