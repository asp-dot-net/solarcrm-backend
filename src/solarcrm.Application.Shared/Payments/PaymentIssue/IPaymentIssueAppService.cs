﻿using Abp.Application.Services.Dto;
using solarcrm.ApplicationTracker.Dto;
using solarcrm.Dto;
using solarcrm.Payments.PaymentIssue.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentIssue
{
    public interface IPaymentIssueAppService
    {
        Task<PagedResultDto<GetPaymentIssueForViewDto>> GetAllPaymentIssue(GetAllPaymentIssueInput input);
        Task<PaymentIssueDto> GetPaymentIssueQuickView(EntityDto input);

        Task<CreateOrEditSSPaymentDto> GetSSPaymentForView(EntityDto input);
        Task SSPaymentForAddOrEdit(CreateOrEditSSPaymentDto input, int SectionId);
      
        Task<CreateOrEditSTCPaymentDto> GetSTCPaymentForView(EntityDto input);
        Task STCPaymentForAddOrEdit(CreateOrEditSTCPaymentDto input, int SectionId);
        Task<FileDto> GetPaymentIssueToExcel(GetAllPaymentIssueInput input);
    }
}
