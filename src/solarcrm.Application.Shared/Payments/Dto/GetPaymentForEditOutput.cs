﻿using Abp.Application.Services.Dto;
using solarcrm.Jobs.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class GetPaymentForEditOutput : EntityDto<int?>
    {
        public virtual int PaymentJobId { get; set; }

        public virtual decimal? LessSubsidy { get; set; }

        public virtual decimal? PaidTillDate { get; set; }

        public virtual decimal? NetAmount { get; set; }

        public virtual decimal? BalanceOwing { get; set; }
        public virtual bool? PaymentDeliveryOption { get; set; }

        public virtual string PaymentAccountInvoiceNotes { get; set; }

        public CreateOrEditPaymentDto payment { get; set; }

        public virtual string JobNumber { get; set; }

        public virtual string PaymentAddedByName { get; set; }
        public virtual string VerifiedByName { get; set; }
        public virtual decimal? TotalSystemCost { get; set; }
        public List<CreateOrEditJobProductItemDto> JobProductItems { get; set; }

        public List<CreateOrEditPaymentReceiptItemsDto> PaymentReceiptItems { get; set; }
    }
}
