﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class GetAllPaymentReceiptItemsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

       // public string ProductItemNameFilter { get; set; }
    }
}
