﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class GetPaymentForViewDto
    {
        public PaymentDto payment { get; set; }
    }
}
