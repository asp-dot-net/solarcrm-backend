﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class GetAllPaymentInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? OrganizationUnit { get; set; }
        public string FilterbyDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
