﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class GetPaymentReceiptItemForEditOutput
    {
        public virtual int PaymentId { get; set; }

        public virtual DateTime? PaymentPayDate { get; set; }

        public virtual decimal? PaymentTotalPaid { get; set; }

        public virtual int? PaymentTypeId { get; set; }


        public virtual int? PaymentModeId { get; set; }

        public virtual string PaymentReceiptName { get; set; }

        public virtual string PaymentReceiptPath { get; set; }

        public virtual int? PaymentAddedById { get; set; }

        public virtual bool IsPaymentVerified { get; set; }
    }
}
