﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class CreateOrEditPaymentReceiptItemsDto : EntityDto<int?>
    {
        public virtual int PaymentId { get; set; }

        public virtual DateTime? PaymentRecEnterDate { get; set; }
        public virtual DateTime? PaymentRecPaidDate { get; set; }
        public virtual decimal? PaymentRecTotalPaid { get; set; }
        public virtual string PaymentRecRefNo { get; set; }
        public virtual int? PaymentRecTypeId { get; set; }

       
        public virtual int? PaymentRecModeId { get; set; }
        public virtual string PaymentRecChequeStatus { get; set; }
        public virtual string PaymentReceiptName { get; set; }

        public virtual string PaymentReceiptPath { get; set; }

        public virtual int? PaymentRecAddedById { get; set; }

        public virtual bool IsPaymentRecVerified { get; set; }

        public virtual int? PaymentRecVerifiedById { get; set; }

        public virtual string PaymentRecNotes { get; set; }

        public virtual int LeadId { get; set; }
        public int? OrganizationUnitId { get; set; }

        public string PaymentRecType { get; set; }
        public string PaymentRecMode { get; set; }
        public virtual string PaymentRecAddedByName { get; set; }

        public virtual string PaymentVerifiedByName { get; set; }
    }
}
