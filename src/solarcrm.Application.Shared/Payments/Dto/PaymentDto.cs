﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class PaymentDto : EntityDto
    {
        public virtual int PaymentJobId { get; set; }       

        public virtual decimal? LessSubsidy { get; set; }

        public virtual decimal? PaidTillDate { get; set; }

        public virtual decimal? NetAmount { get; set; }

        public virtual decimal? BalanceOwing { get; set; }
        public virtual bool? PaymentDeliveryOption { get; set; }

        public virtual string PaymentAccountInvoiceNotes { get; set; }
    }
}
