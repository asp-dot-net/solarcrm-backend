﻿using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.Dto
{
    public class BillPaymentDto
    {
        public int BillJobID { get; set; }
        public decimal? SolarGridAmount { get; set; }

        public decimal? SystemSize { get; set; }

        public decimal? TotalSystemPrice { get; set; }

        public decimal? Subsidy40 { get; set; }

        public decimal? Subsidy20 { get; set; }
        public decimal? TotalSubSidy { get; set; }
        public decimal? SystemSuplyCGST { get; set; }
        public decimal? SystemSuplySGST { get; set; }

        public decimal? SolarServiceAmount { get; set; }

        public decimal? SolarServiceCGST { get; set; }
        public decimal? SolarServiceSGST { get; set; }

        public decimal? TotalAmount { get; set; }

        public object OrganizationDetails { get; set; }

        public List<object> OrganizationbankDetails { get; set; }

        public LeadsDto leaddata { get; set; }

        public object jobdata { get; set; }
    }
}
