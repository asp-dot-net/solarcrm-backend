﻿using Abp.Application.Services.Dto;
using solarcrm.EmailSetup.Dto;
using solarcrm.Payments.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments
{
    public interface IPaymentAppService
    {
        Task<GetPaymentForEditOutput> GetPaymentForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditPaymentDto input, int SectionId);

        Task DeletePaymentReceipt(EntityDto input);
        //Task<List<string>> GetProductItemList(int productTypeId, string productItem);

        //Task<PagedResultDto<GetPaymentForViewDto>> GetAll(GetAllPaymentInput input);

       
    }
}
