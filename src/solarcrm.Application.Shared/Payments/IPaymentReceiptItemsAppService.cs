﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.Payments.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments
{
    public interface IPaymentReceiptItemsAppService : IApplicationService
	{
		Task<PagedResultDto<GetPaymentReceiptItemForViewDto>> GetAll(GetAllPaymentReceiptItemsInput input);

		Task<GetPaymentReceiptItemForEditOutput> GetPaymentReceiptItemForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditPaymentReceiptItemsDto input);

		Task Delete(EntityDto input);

		//Task<List<JobProductItemJobLookupTableDto>> GetAllJobForTableDropdown();

		//Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdown();

		//Task<List<JobProductItemProductItemLookupTableDto>> GetAllProductItemForTableDropdownForEdit();

		Task<List<GetPaymentReceiptItemForEditOutput>> GetPaymentReceiptItemByJobId(int jobid);
	}
}
