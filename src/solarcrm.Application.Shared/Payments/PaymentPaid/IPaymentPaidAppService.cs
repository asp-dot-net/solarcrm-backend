﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Payments.PaymentIssue.Dto;
using solarcrm.Payments.PaymentPaid.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments.PaymentPaid
{
    public interface IPaymentPaidAppService
    {
        Task<PagedResultDto<GetPaymentPaidForViewDto>> GetAllPaymentPaid(GetAllPaymentPaidInput input);
        Task<PaymentPaidDto> GetPaymentPaidQuickView(EntityDto input);
        Task<FileDto> GetPaymentPaidToExcel(GetAllPaymentPaidInput input);
    }
}
