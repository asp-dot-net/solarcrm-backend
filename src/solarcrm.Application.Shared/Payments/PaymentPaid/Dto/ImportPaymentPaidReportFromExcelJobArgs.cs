﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentPaid.Dto
{
    public class ImportPaymentPaidReportFromExcelJobArgs
    {
        public int? TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }

        public int OrganizationId { get; set; }
        public byte[] FileBytes { get; set; }
    }
}
