﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentPaid.Dto
{
    public class PaymentPaidDto : EntityDto
    {
        public virtual int PaymentJobId { get; set; }

        public virtual decimal? LessSubsidy { get; set; }

        public virtual decimal? PaidTillDate { get; set; }

        public virtual decimal? NetAmount { get; set; }

        public virtual decimal? BalanceOwing { get; set; }
        public virtual bool? PaymentDeliveryOption { get; set; }
        public LeadsDto leaddata { get; set; }

        public int JobId { get; set; }
        public string ProjectNumber { get; set; }


        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }

        public decimal? PaymentRecTotalPaid { get; set; }
        public string Email { get; set; }
        public virtual DateTime? PaymentRecEnterDate { get; set; }
        public virtual DateTime? PaymentRecPaidDate { get; set; }
        public virtual decimal? PaymentTotal { get; set; }
        public virtual decimal? TotalProjectCost { get; set; }

        public virtual string PaymentRefferenceNo { get; set; }
        public virtual string PaymentType { get; set; }
        public virtual string PaymentMode { get; set; }

        public virtual string PaymentChequeStatus { get; set; }

        public virtual bool? IsPaymentRecVerified { get; set; }

        public string PaymentRecVerifiedBy { get; set; }

        public virtual string PaymentAccountInvoiceNotes { get; set; }

        public virtual string PaymentRecNotes { get; set; }
        public string LeadStatusName { get; set; }

        public int ApplicationStatus { get; set; }

        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }

        public string Comment { get; set; }
        public string Notes { get; set; }
       
        public string LeadStatusColorClass { get; set; }
        public string LeadStatusIconClass { get; set; }

        public object ApplicationStatusdata { get; set; }

        public DateTime? ReminderTime { get; set; }
        public string ActivityDescription { get; set; }

        public string ActivityComment { get; set; }


        public virtual DateTime? QuataionSignDate { get; set; }
        public virtual string EastimatePaidStatus { get; set; }
        public virtual DateTime? EastimatePaidDate { get; set; }

        public int PaymentRecTypeId { get; set; }

        public int PaymentRecModeId { get; set; }

    }
}
