﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentPaid.Dto
{
    public class GetPaymentPaidForViewDto
    {
        public PaymentPaidDto paymentPaid { get; set; }
        public PaymentPaidSummaryCount paymentPaidSummaryCount { get; set; }
    }
    public class PaymentPaidSummaryCount
    {
        public string Total { get; set; } = "0";
        public string ReceivedAmount { get; set; } = "0";
        public string TotalOwingAmount { get; set; } = "0";       
        public string FeasiblityApproved { get; set; } = "0";
        public string EastimatePaid { get; set; } = "0";
        public string TotalKilowatt { get; set; } = "0";
        public string TotalPanel { get; set; } = "0";
    }
}
