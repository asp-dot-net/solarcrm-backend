﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Payments.PaymentPaid.Dto
{
    public class GetAllPaymentPaidInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? OrganizationUnit { get; set; }
        public string FilterbyDate { get; set; }
        public List<int> LeadStatusIDS { get; set; }
        public List<int> employeeIdFilter { get; set; }
        public List<int> leadApplicationStatusFilter { get; set; } //application Filter
        public int? applicationstatusTrackerFilter { get; set; }
       
        public List<int> DiscomIdFilter { get; set; }

        public List<int> CircleIdFilter { get; set; }

        public List<int> DivisionIdFilter { get; set; }

        public List<int> SubDivisionIdFilter { get; set; }

        public List<int> SolarTypeIdFilter { get; set; }
        public List<int> TenderIdFilter { get; set; }

        public string BillFilter { get; set; }
        public List<int> PaymentModeFilter { get; set; }
        public int? PaymentStatusFilter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? excelorcsv { get; set; }

    }
}
