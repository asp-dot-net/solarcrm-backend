﻿using Abp.Auditing;
using solarcrm.Configuration.Dto;

namespace solarcrm.Configuration.Tenants.Dto
{
    public class TenantEmailSettingsEditDto : EmailSettingsEditDto
    {
        public bool UseHostDefaultEmailSettings { get; set; }
    }
}