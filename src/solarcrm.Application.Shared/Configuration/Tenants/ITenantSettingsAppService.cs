﻿using System.Threading.Tasks;
using Abp.Application.Services;
using solarcrm.Configuration.Tenants.Dto;

namespace solarcrm.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
