﻿using System.Threading.Tasks;
using Abp.Application.Services;
using solarcrm.Configuration.Host.Dto;

namespace solarcrm.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
