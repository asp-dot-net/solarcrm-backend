﻿namespace solarcrm.Configuration
{
    public interface IExternalLoginOptionsCacheManager
    {
        void ClearCache();
    }
}
