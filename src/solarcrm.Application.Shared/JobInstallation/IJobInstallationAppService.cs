﻿using solarcrm.JobInstallation.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.JobInstallation
{
    public interface IJobInstallationAppService
    {
        Task<GetJobInstallForEditOutput> GetJobInstallById(int jobid);
        Task CreateOrEdit(CreateOrEditJobInstallDto input);
    }
}
