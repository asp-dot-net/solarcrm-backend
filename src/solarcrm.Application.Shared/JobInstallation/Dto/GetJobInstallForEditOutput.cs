﻿using Abp.Application.Services.Dto;
using solarcrm.Jobs.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobInstallation.Dto
{
    public class GetJobInstallForEditOutput : EntityDto<int>
    {
       
        public int JobId { get; set; }
        public DateTime? InstallStartDate { get; set; }
        public int StoreLocationId { get; set; }
        public int JobInstallerID { get; set; }
        public DateTime? InstallCompleteDate { get; set; }
        public string InstallerNotes { get; set; }

       
    }
}
