﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobInstallation.Dto
{
    public class JobInstallationDto : EntityDto
    {
        public int JobId { get; set; }

        public DateTime? InstallStartDate { get; set; }

        public int StoreLocationId { get; set; }

        public int InstallerID { get; set; }

        public DateTime? InstallCompleteDate { get; set; }
    }
}
