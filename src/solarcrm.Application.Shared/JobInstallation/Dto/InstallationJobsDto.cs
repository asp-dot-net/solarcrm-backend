﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobInstallation.Dto
{
    public class InstallationJobsDto
    {
        public string InstallerName { get; set; }
		public string CustomerName { get; set; }
		public DateTime? InstallationTime { get; set; }
		public int JobId { get; set; }
		public int? LeadId { get; set; }
		public string JobNumber { get; set; }
		
		public string State { get; set; }
		public string PostalCode { get; set; }
		public string Address { get; set; }

	}
}
