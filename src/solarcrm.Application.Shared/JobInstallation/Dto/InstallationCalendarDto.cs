﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobInstallation.Dto
{
    public class InstallationCalendarDto
    {
        public DateTime InstallationDate { get; set; }
        public int InstallationCount { get; set; }
        public List<InstallationJobsDto> Jobs { get; set; }
    }
}
