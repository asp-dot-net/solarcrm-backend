﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobInstallation.Dto
{
   public class GetAllInstallationInput : PagedAndSortedResultRequestDto
    {
        public int OrganizationUnit { get; set; }
        public int status { get; set; }
        public string Filter { get; set; }
        public string StateFilter { get; set; }
        public string SteetAddressFilter { get; set; }
        public string JobTypeNameFilter { get; set; }
        public string JobStatusNameFilter { get; set; }
        public int? HousetypeId { get; set; }
        public int? Rooftypeid { get; set; }
        public int? Jobstatusid { get; set; }
        public string Panelmodel { get; set; }
        public string Invertmodel { get; set; }
        public int? Paymenttypeid { get; set; }
        public string AreaNameFilter { get; set; }
        public string DateFilterType { get; set; }
        public string PostalCodeFrom { get; set; }
        public string PostalCodeTo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? jobTypeId { get; set; }
        public int? JobAssignUser { get; set; }
        public int? AssignJob { get; set; }
    }
    
}
