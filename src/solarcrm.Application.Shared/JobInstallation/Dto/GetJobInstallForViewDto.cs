﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobInstallation.Dto
{
    public class GetJobInstallForViewDto
    {
        public JobInstallationDto JobInstall { get; set; }
        public string JobApplivationNumber { get; set; }
        public string CustomerName { get; set; }

        public string Address { get; set; }
        public string InstallerName { get; set; }


    }
}
