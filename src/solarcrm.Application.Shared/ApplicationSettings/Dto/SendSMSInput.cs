﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.ApplicationSettings.Dto
{
    public class SendSMSInput
    {
        [Required]
        public string PhoneNumber { get; set; }

        public string Text { get; set; }

        public int ActivityId { get; set; }

        public int promoresponseID { get; set; }

        public int SectionID { get; set; }

        public int? oId { get; set; }
    }
}
