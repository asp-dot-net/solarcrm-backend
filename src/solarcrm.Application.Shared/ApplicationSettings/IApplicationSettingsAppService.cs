﻿using Abp.Application.Services;
using solarcrm.ApplicationSettings.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.ApplicationSettings
{
    public interface IApplicationSettingsAppService : IApplicationService
    {
        Task SendSMS(SendSMSInput input);
    }
}
