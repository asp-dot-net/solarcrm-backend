﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.MISReport.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MISReport
{
    public interface IMISReportAppService
    {
        Task<PagedResultDto<GetMISReportForViewDto>> GetAll(GetAllMISReportInput input);

        //Task<GetDivisionForViewDto> GetDivisionForView(int id);

        Task<GetMISReportForEditOutput> GetMISReportForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditMISReportDto input);

        Task Delete(EntityDto input);

        Task<FileDto> GetMISReportToExcel(GetAllMISReportForExcelInput input);

       
    }
}
