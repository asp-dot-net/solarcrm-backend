﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class MISReportDto : EntityDto
    {
       
        public string ProjectName { get; set; }   
        public string ApplicationNo { get; set; }
        public string GUVNLRegistrationNo { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string PVCapacity { get; set; }       
        public string DiscomName { get; set; }
        public string ConsumerNo { get; set; }
        public string TariffCategory { get; set; }
        public string Circle { get; set; }
        public string DivisionZone { get; set; }
        public string Subdivision { get; set; }
        public string SanctionedContractLoad { get; set; }
        public string PhaseofproposedSolarInverter { get; set; }
        public string Category { get; set; }       
        public string ConsumerEmail { get; set; }
        public string ConsumerMobile { get; set; }      
        public string NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string LandlineNo { get; set; }
        public string StreetHouseNo { get; set; }
        public string Taluka { get; set; }
        public string District { get; set; }
        public string CityVillage { get; set; }
        public string State { get; set; }
        public string Pin { get; set; }      
        public string CommonMeter { get; set; }
        public string AadhaarNoEntered { get; set; }
        public DateTime? OTPVerifiedon { get; set; }
        public DateTime? SignedDocumentUploadedDate { get; set; }
        public string LastComment { get; set; }
        public DateTime? LastCommentDate { get; set; }
        public DateTime? LastCommentRepliedDate { get; set; }
        public DateTime? DocumentVerifiedDate { get; set; }    
        public string EstimateQuotationNo { get; set; }
        public string DisComEstimateAmount { get; set; }
        public DateTime? DueDate { get; set; }
        public string PaymentReceived { get; set; }
        public DateTime? PaymentMadeon { get; set; }
        public DateTime? SelfCertification { get; set; }
        public string CEIDrawingApplicationID { get; set; }
        public DateTime? CEIDrawingApplicationApprovalDate { get; set; }
        public string BidirectionalMeterMake { get; set; }
        public string BidirectionalMeterNo { get; set; }
        public string SolarMeterMake { get; set; }
        public string SolarMeterNo { get; set; }
        public DateTime? DateofInstallationofSolarMeter { get; set; }
        public DateTime? AgreementSigningDate { get; set; }
        public string ApplicationStatus { get; set; }
        public DateTime? IntimationReceivedStatus { get; set; }
        public DateTime? IntimationApprovedDate { get; set; }
        public string IntimationStatus { get; set; }
        public int? PenaltyDaysPending { get; set; }
        public string PCRCode { get; set; }
        public DateTime? PCRSubmittedDate { get; set; }

    }
}
