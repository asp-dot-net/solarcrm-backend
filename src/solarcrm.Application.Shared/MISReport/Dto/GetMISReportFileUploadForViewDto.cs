﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class GetMISReportFileUploadForViewDto
    {
        public MISFileUploadListDto MISReportFileUpload { get; set; }
    }
}
