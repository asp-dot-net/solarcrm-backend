﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class CreateOrEditMISReportDto : EntityDto<int?>
    {
        //[Required]
        //[StringLength(StockItemConsts.MaxNameLength, MinimumLength = StockItemConsts.MinNameLength)]
        public DateTime? TimeStamp { get; set; }

        public string Scheme { get; set; }
     
        public string ProjectName { get; set; }

        public decimal? ProjectArea { get; set; }
        public int? ProjectAreaType { get; set; }

        public int? AvgMonthlyBill { get; set; }

        public int? EnergyConsumption { get; set; }

        public string ApplicationNo { get; set; }

        public string GUVNLRegistrationNo { get; set; }
        public decimal? Lat { get; set; }

        public decimal? Long { get; set; }

        public decimal? PVCapacity { get; set; }

        public string ExistingPVCapacity { get; set; }

        public string InstallerName { get; set; }

        public string InstallerCategory { get; set; }
        public string EmpanelmentNo { get; set; }
        public string DiscomName { get; set; }
        public int? ConsumerNo { get; set; }
        public string TariffCategory { get; set; }
        public string Circle { get; set; }
        public string DivisionZone { get; set; }
        public string Subdivision { get; set; }
        public decimal? SanctionedContractLoad { get; set; }
        public string PhaseofproposedSolarInverter { get; set; }
        public string Category { get; set; }
        public string WhowillprovidetheNetMeter { get; set; }
        public string ConsumerEmail { get; set; }
        public string ConsumerMobile { get; set; }
        public string InstallerEmail { get; set; }
        public string InstallerMobile { get; set; }
        public string NamePrefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string LandlineNo { get; set; }
        public string StreetHouseNo { get; set; }
        public string Taluka { get; set; }
        public string District { get; set; }
        public string CityVillage { get; set; }
        public string State { get; set; }
        public int? Pin { get; set; }
        public string CommunicationAddress { get; set; }
        public string WhetherthePremisesisownedorRented { get; set; }
        public string CommonMeter { get; set; }
        public string AadhaarNoEntered { get; set; }
        public DateTime? OTPVerifiedon { get; set; }
        public DateTime? SignedDocumentUploadedDate { get; set; }
        public string LastComment { get; set; }
        public DateTime? LastCommentDate { get; set; }
        public DateTime? LastCommentRepliedDate { get; set; }
        public DateTime? DocumentVerifiedDate { get; set; }
        public string DepositQuotationNo { get; set; }
        public string DepositQuotationAmount { get; set; }
        public DateTime? DepositDueDate { get; set; }
        public DateTime? DepositPaymentPaidon { get; set; }
        public int? EstimateQuotationNo { get; set; }
        public decimal? DisComEstimateAmount { get; set; }
        public DateTime? DueDate { get; set; }
        public string PaymentReceived { get; set; }
        public DateTime? PaymentMadeon { get; set; }
        public DateTime? SelfCertification { get; set; }
        public string CEIDrawingApplicationID { get; set; }
        public DateTime? CEIDrawingApplicationApprovalDate { get; set; }
        public string BidirectionalMeterMake { get; set; }
        public string BidirectionalMeterNo { get; set; }
        public string SolarMeterMake { get; set; }
        public string SolarMeterNo { get; set; }
        public DateTime? DateofInstallationofSolarMeter { get; set; }
        public DateTime? AgreementSigningDate { get; set; }
        public string ApplicationStatus { get; set; }
        public DateTime? IntimationReceivedStatus { get; set; }
        public DateTime? IntimationApprovedDate { get; set; }
        public string IntimationStatus { get; set; }
        public string PenaltyDaysPending { get; set; }
        public string PCRCode { get; set; }
        public DateTime? PCRSubmittedDate { get; set; }




    }
}
