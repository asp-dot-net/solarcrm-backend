﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class MISFileUploadListDto : FullAuditedEntityDto
    {
        public string FileName { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string FileStatus { get; set; }
        public virtual string userName { get; set; }
        public int TenantId { get; set; }
        public virtual int? OrganizationUnitId { get; set; }
    }
}
