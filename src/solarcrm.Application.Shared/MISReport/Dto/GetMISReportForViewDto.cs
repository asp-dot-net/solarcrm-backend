﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class GetMISReportForViewDto
    {
        public MISReportDto MISReport { get; set; }
    }
}
