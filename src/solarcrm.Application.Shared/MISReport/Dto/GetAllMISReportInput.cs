﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class GetAllMISReportInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}
