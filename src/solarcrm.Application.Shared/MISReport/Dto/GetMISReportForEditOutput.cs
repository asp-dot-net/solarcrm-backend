﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class GetMISReportForEditOutput
    {
        public CreateOrEditMISReportDto MISReport { get; set; }
    }
}
