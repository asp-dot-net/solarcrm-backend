﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class GetAllMISReportForExcelInput
    {
        public string Filter { get; set; }
    }
}
