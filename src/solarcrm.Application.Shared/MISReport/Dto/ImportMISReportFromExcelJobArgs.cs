﻿using Abp;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.MISReport.Dto
{
    public class ImportMISReportFromExcelJobArgs
    {
        public int? TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }
        public string GovermentAgency { get; set; }
        public int OrganizationId { get; set; }
        public byte[] FileBytes { get; set; }
        public string FileName { get; set; }    
    }
}
