﻿namespace solarcrm.Tenants.Dashboard.Dto
{
    public enum SalesSummaryDatePeriod
    {
        Daily = 1,
        Weekly = 2,
        Monthly = 3
    }
}