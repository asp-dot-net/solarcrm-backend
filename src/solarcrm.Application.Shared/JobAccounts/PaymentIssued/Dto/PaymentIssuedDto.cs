﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.JobAccounts.PaymentIssued.Dto
{
    public class PaymentIssuedDto : FullAuditedEntity
    {
        public int LeadId { get; set; }
        public int JobId { get; set; }
        public string ProjectNumber { get; set; }


        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }

        public string SubDivision { get; set; }

        public string JobOwnedBy { get; set; }

        public string LeadStatusName { get; set; }

        public int ApplicationStatus { get; set; }

        public DateTime? FollowDate { get; set; }

        public string FollowDiscription { get; set; }

        public string Comment { get; set; }
        public string Notes { get; set; }

       
        public object ApplicationStatusdata { get; set; }
    }
}
