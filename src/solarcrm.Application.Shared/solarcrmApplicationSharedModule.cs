﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace solarcrm
{
    [DependsOn(typeof(solarcrmCoreSharedModule))]
    public class solarcrmApplicationSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(solarcrmApplicationSharedModule).GetAssembly());
        }
    }
}