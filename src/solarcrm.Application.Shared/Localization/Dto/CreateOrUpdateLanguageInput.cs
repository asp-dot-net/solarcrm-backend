﻿using System.ComponentModel.DataAnnotations;

namespace solarcrm.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}