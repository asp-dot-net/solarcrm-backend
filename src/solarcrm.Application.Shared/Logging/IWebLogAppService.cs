﻿using Abp.Application.Services;
using solarcrm.Dto;
using solarcrm.Logging.Dto;

namespace solarcrm.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
