﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.Authorization.Permissions.Dto;

namespace solarcrm.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
