﻿using System.Collections.Generic;

namespace solarcrm.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public List<string> Permissions { get; set; }
    }
}
