﻿using System.ComponentModel.DataAnnotations;

namespace solarcrm.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
