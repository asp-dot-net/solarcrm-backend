﻿using System.Collections.Generic;
using solarcrm.Authorization.Permissions.Dto;

namespace solarcrm.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}