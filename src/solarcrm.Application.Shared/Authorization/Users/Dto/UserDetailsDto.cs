﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Authorization.Users.Dto
{
    public class UserDetailsDto : EntityDto<int?>
    {
        public int? UserType { get; set; }

        public string MiddleName { get; set; }

        public string CompanyMobileNo { get; set; }

        public string EmergancyContactNo { get; set; }

        public string EmergancyContactName { get; set; }

        public string BloodGroup { get; set; }

        public string Gender { get; set; }

        public string DateOfBirth { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public int? State { get; set; }

        public int? District { get; set; }

        public int? Taluko { get; set; }

        public int? City { get; set; }

        public virtual long? Pincode { get; set; }

        public string BankName { get; set; }

        public string TypeOfAccount { get; set; }

        public string AccountNo { get; set; }

        public string IFSCCode { get; set; }

        public string UANNo { get; set; }

        public string PancardNo { get; set; }

        public string Designation { get; set; }

        //public DateTime? JoiningDate { get; set; }

        //public DateTime? ShiftStartAt { get; set; }

        //public DateTime? ShiftEndAt { get; set; }

        public string JoiningDate { get; set; }

        public string ShiftStartAt { get; set; }

        public string ShiftEndAt { get; set; }

        public string TypeOfCompany { get; set; }

        public string BelongsTo { get; set; }

        public string AdharNo { get; set; }

        public string GSTNo { get; set; }

        public int? ManageBy { get; set; }

        public int? Discom { get; set; }

        public string Department { get; set; }
    }
}
