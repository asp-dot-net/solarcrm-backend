﻿using Abp.Application.Services.Dto;

namespace solarcrm.Authorization.Users.Dto
{
    public interface IGetLoginAttemptsInput: ISortedResultRequest
    {
        string Filter { get; set; }
    }
}