﻿using solarcrm.Security;

namespace solarcrm.Authorization.Users.Profile.Dto
{
    public class GetPasswordComplexitySettingOutput
    {
        public PasswordComplexitySetting Setting { get; set; }
    }
}
