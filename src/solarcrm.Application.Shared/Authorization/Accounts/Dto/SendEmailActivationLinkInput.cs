﻿using System.ComponentModel.DataAnnotations;

namespace solarcrm.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}