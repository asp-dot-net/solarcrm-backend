﻿using System.Threading.Tasks;
using Abp.Application.Services;
using solarcrm.Sessions.Dto;

namespace solarcrm.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
