﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using solarcrm.Jobs.JobVariation.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Jobs.JobVariation
{
    public interface IJobVariationAppService : IApplicationService
    {
        Task<GetJobVariationForEditOutput> GetJobVariationForEdit(EntityDto input);

        Task<List<GetJobVariationForEditOutput>> GetJobVariationByJobId(int jobid);

        Task CreateOrEdit(CreateOrEditJobVariationDto input);

        Task Delete(EntityDto input);

        Task<List<JobVariationLookupTableDto>> GetAllVariationForTableDropdown(int id);
    }
}
