﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.JobVariation.Dto
{
    public class GetJobVariationForEditOutput
    {
        public CreateOrEditJobVariationDto JobVariation { get; set; }

        public string VariationName { get; set; }
    }
}
