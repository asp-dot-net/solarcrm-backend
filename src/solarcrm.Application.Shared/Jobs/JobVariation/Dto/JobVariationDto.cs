﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.JobVariation.Dto
{
    public class JobVariationDto : EntityDto
    {
        public int? VariationId { get; set; }

        public int? JobId { get; set; }

        public decimal? Cost { get; set; }
    }
}
