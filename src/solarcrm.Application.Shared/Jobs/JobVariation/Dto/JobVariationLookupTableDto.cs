﻿namespace solarcrm.Jobs.JobVariation.Dto
{
    public class JobVariationLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }

        public string ActionName { get; set; }
    }
}
