﻿using Abp.Application.Services.Dto;
using solarcrm.Dto;
using solarcrm.Jobs.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace solarcrm.Jobs
{
    public interface IJobAppService
    {
        Task<GetJobForEditOutput> GetJobForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditJobDto input, int SectionId);

        Task<List<string>> GetProductItemList(int productTypeId, string productItem);

        Task<PagedResultDto<GetJobForViewDto>> GetAll(GetAllJobInput input);

        Task<FileDto> GetJobsToExcel(GetAllJobInput input);
    }
}
