﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class JobProductItemDto : EntityDto
    {
        public int? JobId { get; set; }

        public int? ProductItemId { get; set; }
    }
}
