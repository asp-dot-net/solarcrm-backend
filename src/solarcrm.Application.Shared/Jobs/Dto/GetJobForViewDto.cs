﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class GetJobForViewDto
    {
        public JobDto Job { get; set; }
        public jobSummaryCount jobSummaryCount { get; set; }
    }
}
