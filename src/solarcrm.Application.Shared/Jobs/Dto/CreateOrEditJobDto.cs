﻿using Abp.Application.Services.Dto;
using solarcrm.Jobs.JobVariation.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class CreateOrEditJobDto : EntityDto<int>
    {
        public int OrganizationUnitId { get; set; }

        public int LeadId { get; set; }

        public string JobNumber { get; set; }

        public string JobNotes { get; set; }

        public string InstallerNotes { get; set; }

        public string NotesForInstallationDepartment { get; set; }

        public decimal ActualSystemCost { get; set; }
        public decimal? JobSubsidy20 { get; set; }
        public decimal? JobSubsidy40 { get; set; }
        public decimal? JobTotalSubsidyAmount { get; set; }

        public decimal DepositeAmount { get; set; }

        public decimal NetSystemCost { get; set; }

        public decimal TotalJobCost { get; set; }

        public string PhaseOfInverter { get; set; }

        public decimal PvCapacityKw { get; set; }

        public string ApplicationNumber { get; set; }        

        public List<CreateOrEditJobProductItemDto> JobProductItems { get; set; }

        public List<CreateOrEditJobVariationDto> JobVariation { get; set; }

        public string GUVNLRegisterationNo { get; set; }

        public DateTime? ApplicationDate { get; set; }

        public string OtpVerifed { get; set; }

        public string SignApplicationPath { get; set; }
        public DateTime? SignApplicationDate { get; set; }

        public string LoadReduceReasonApplication { get; set; }

        public string LoadReducePathApplication { get; set; }

        public virtual string EastimatePaymentRefNumber { get; set; }     //EstimatePaymentReferenceNo

        public virtual int? EastimateQuoteNumber { get; set; }     //EstimateQuotationNo

        public virtual decimal? EastimateQuoteAmount { get; set; }   //DisComEstimateAmount

        public virtual DateTime? EastimateDueDate { get; set; }    //DueDate

        public virtual DateTime? EastimatePayDate { get; set; }     //PaymentMadeon

        public virtual string EastimatePaidStatus { get; set; }   //PaymentReceived
        public virtual string EastimatePaymentResponse { get; set; }     //EstimatePaymentResponse

        public virtual string EstimatePaymentReceiptPath { get; set; }

        public virtual DateTime? DocumentVerifiedDate{ get; set; }

        public virtual string LastComment { get; set; }

        public virtual string LastCommentDate { get; set; }

        public virtual DateTime? LastCommentRepliedDate { get; set; }

        public virtual int? PenaltyDaysPending { get; set; }

        public virtual decimal? SanctionedContractLoad { get; set; }

       public int JobLoanOption { get; set; }
    }
}
