﻿using Abp.Application.Services.Dto;

namespace solarcrm.Jobs.Dto
{
    public class CreateOrEditJobProductItemDto : EntityDto<int?>
    {
		public int? Quantity { get; set; }

		public int? JobId { get; set; }

		public string ProductItemName { get; set; }

		public int? ProductItemId { get; set; }

		public int? ProductTypeId { get; set; }

		public decimal? Size { get; set; }
	}
}
