﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class GetAllJobProductItemsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string ProductItemNameFilter { get; set; }
    }
}
