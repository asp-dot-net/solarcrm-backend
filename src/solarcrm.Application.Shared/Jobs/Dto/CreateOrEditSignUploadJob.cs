﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class CreateOrEditSignUploadJob
    {
        public int Id { get; set; }
        public string SignApplicationPath { get; set; }
        public DateTime? SignApplicationDate { get; set; }
    }
}
