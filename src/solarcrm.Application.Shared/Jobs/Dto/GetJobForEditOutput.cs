﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class GetJobForEditOutput
    {
        public int JobType { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string Taluka { get; set; }

        public string District { get; set; }

        public string State { get; set; }

        public int? Pincode { get; set; }

        public CreateOrEditJobDto Job { get; set; }

        public decimal? StrctureAmmount { get; set; }

        public long? ConsumerNumber { get; set; }

        public string SubDivision { get; set; }

        public string Division { get; set; }

        public string Discom { get; set; }

        public string Circle { get; set; }

        public decimal RequiredCapacity { get; set; }

        public string OtpVerifed { get; set; }

        public string ApplicationNumber { get; set; }

        public string GUVNLRegisterationNo { get; set; }

        public DateTime? ApplicationDate { get; set; }

        public string SignApplicationPath { get; set; }

        public string LeadStatus { get; set; }

        public string LeadStatusColorClass { get; set; }

        public string LeadStatusIconClass { get; set; }

        public virtual string AreaType { get; set; }

        public bool? IsManualPricingSubcidy { get; set; }

        public int JobLoanOption { get; set; }

    }
}
