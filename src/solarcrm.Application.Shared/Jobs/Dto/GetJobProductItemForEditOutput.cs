﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class GetJobProductItemForEditOutput
    {
        public CreateOrEditJobProductItemDto JobProductItem { get; set; }

        public string ProductItemName { get; set; }
        public string ProductTypeName { get; set; }
        public decimal? Size { get; set; }
        
        public string Model { get; set; }
        
        public int? Quantity { get; set; }
        
        public int? ID { get; set; }
    }
}
