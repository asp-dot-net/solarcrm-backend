﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class GetJobProductItemForViewDto
    {
        public JobProductItemDto JobProductItem { get; set; }

        public string ProductItemName { get; set; }
    }
}
