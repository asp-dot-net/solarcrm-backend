﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class CreateOrEditJobProductDto : EntityDto<int?>
	{
		public int? Quantity { get; set; }

		public string ProductItemName { get; set; }

		public int? ProductItemId { get; set; }

		public int? JobId { get; set; }

		public string Model { get; set; }

		public decimal? Size { get; set; }
	}
}
