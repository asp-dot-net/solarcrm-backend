﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class JobProductItemJobLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
    }
}
