﻿using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class JobDto : EntityDto
    {
        public int LeadId { get; set; }

        public LeadsDto leaddata { get; set; }
        public string ProjectNumber { get; set; }

        public string JobStatus { get; set; }

        public string CustomerName { get; set; }

        public string Address { get; set; }

        public string Mobile { get; set; }

        public string SubDivision { get; set; }
        public DateTime? FollowDate { get; set; }


        public string FollowDiscription { get; set; }
        //public string JobOwnedBy { get; set; }

        public int? PanelyDays { get; set; }

        public string LeadStatus { get; set; }

        //public string ApplicationStatus { get; set; }

        public string Comment { get; set; }
        public string AssignUserName { get; set; }
        public string LeadCreatedName { get; set; }
        public string LeadStatusColorClass { get; set; }
        public string LeadStatusIconClass { get; set; }
        public object ApplicationStatusdata { get; set; }
        //public jobSummaryCount jobSummaryCount { get; set; }
    }

    public class jobSummaryCount
    {
        public int TotalJob { get; set; } = 0;
        public int Kilowatt { get; set; } = 0;
        public int TotalPanel { get; set; } = 0;
        public decimal AvgStructureamount { get; set; } = 0;
        public decimal AvgPricePerkilowatt { get; set; } = 0;
        public int DocumentReceived { get; set; } = 0;
        public int DepositeReceived { get; set; } = 0;
        public int ReadyDispatch { get; set; } = 0;
        public int InstalltionBooked { get; set; } = 0;
        public int InstalltionCompleted { get; set; } = 0;  
        public int SubcidyClaimed { get; set; } = 0;
        public int SubcidyReceived { get; set; } = 0;
    }
}
