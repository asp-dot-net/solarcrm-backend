﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Jobs.Dto
{
    public class JobProductItemProductItemLookupTableDto
    {
        public int Id { get; set; }

        public string DisplayName { get; set; }
        
        public int ProductTypeId { get; set; }
        
        public decimal? Size { get; set; }
        
        public string Model { get; set; }
    }
}
