﻿using Abp.Dependency;
using GraphQL.Types;
using GraphQL.Utilities;
using solarcrm.Queries.Container;
using System;

namespace solarcrm.Schemas
{
    public class MainSchema : Schema, ITransientDependency
    {
        public MainSchema(IServiceProvider provider) :
            base(provider)
        {
            Query = provider.GetRequiredService<QueryContainer>();
        }
    }
}