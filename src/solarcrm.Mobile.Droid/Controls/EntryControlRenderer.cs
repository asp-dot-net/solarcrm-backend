﻿using System;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using solarcrm.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryControl), typeof(EntryControlRenderer))]
namespace solarcrm.Controls
{
    [Obsolete]
    public class EntryControlRenderer : EntryRenderer
	{
        public EntryControlRenderer(Context context) : base(context)
        {
            AutoPackage = false;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Background = new ColorDrawable(Android.Graphics.Color.Transparent);
                //Control.Gravity = GravityFlags.Start | GravityFlags.CenterVertical;
                Control.TextAlignment = Android.Views.TextAlignment.Center;
            }
        }
    }
}

