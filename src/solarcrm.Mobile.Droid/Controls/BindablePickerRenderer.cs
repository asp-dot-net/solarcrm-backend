﻿using System;
using Android.Views;
using solarcrm.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BindablePicker), typeof(BindablePickerRenderer))]
namespace solarcrm.Controls
{
	public class BindablePickerRenderer : PickerRenderer
	{
        [Obsolete]
        public BindablePickerRenderer()
		{
		}

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            var view = (BindablePicker)Element;

            if (e.OldElement != null)
            {
                view.Focused -= pickerFocused;
                view.Unfocused -= pickerUnfocused;
            }

            if (e.NewElement != null)
            {
                SetPlaceholderColor(view);
                SetEnabled(view);
                SetBorder(view);
                SetTextAlignment(view);
                Control.SetTextColor(((Xamarin.Forms.Color)App.Current.Resources["BlackColor"]).ToAndroid());
                view.Focused += pickerFocused;
                view.Unfocused += pickerUnfocused;
            }
        }

        void pickerFocused(object sender, FocusEventArgs fe)
        {
            var view = (BindablePicker)sender;
            if (Control != null)
            {
                Control.SetTextColor(view.TextColor.ToAndroid());
            }
        }

        void pickerUnfocused(object sender, FocusEventArgs fe)
        {
            if (Control != null)
            {
                Control.SetTextColor(((Xamarin.Forms.Color)App.Current.Resources["BlackColor"]).ToAndroid());
            }
        }

        void SetEnabled(BindablePicker view)
        {
            if (view != null)
            {
                if (view.IsCustomFocused)
                {
                    view.Focus();
                }
                else
                {
                    view.Unfocus();
                }
            }
        }

        void SetBorder(BindablePicker view)
        {
            try
            {
                Control.SetBackgroundColor(global::Android.Graphics.Color.Argb(0, 0, 0, 0));
                Control.SetPadding(0, 0, 0, 0);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        void SetPlaceholderColor(BindablePicker view)
        {
            Control.SetHintTextColor(((Xamarin.Forms.Color)App.Current.Resources["GrayColor"]).ToAndroid());
        }

        void SetTextAlignment(BindablePicker view)
        {
            GravityFlags verticalAlignment = GravityFlags.Center;
            GravityFlags horizontalAlignment = GravityFlags.Start;

            switch (view.XAlign)
            {
                case Xamarin.Forms.TextAlignment.Center:
                    horizontalAlignment = GravityFlags.CenterHorizontal;

                    break;
                case Xamarin.Forms.TextAlignment.End:
                    horizontalAlignment = GravityFlags.End;
                    break;
                case Xamarin.Forms.TextAlignment.Start:
                    horizontalAlignment = GravityFlags.Start;
                    break;
            }

            switch (view.YAlign)
            {
                case Xamarin.Forms.TextAlignment.Center:
                    verticalAlignment = GravityFlags.CenterVertical;
                    break;
            }

            Control.Gravity = verticalAlignment | horizontalAlignment;
        }
    }
}

