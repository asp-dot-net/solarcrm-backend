﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace solarcrm
{
    [DependsOn(typeof(solarcrmXamarinSharedModule))]
    public class solarcrmXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(solarcrmXamarinAndroidModule).GetAssembly());
        }
    }
}