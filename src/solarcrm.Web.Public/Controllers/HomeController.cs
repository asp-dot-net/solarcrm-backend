using Microsoft.AspNetCore.Mvc;
using solarcrm.Web.Controllers;

namespace solarcrm.Web.Public.Controllers
{
    public class HomeController : solarcrmControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}