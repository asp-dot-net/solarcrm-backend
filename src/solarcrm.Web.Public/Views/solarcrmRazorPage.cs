﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace solarcrm.Web.Public.Views
{
    public abstract class solarcrmRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected solarcrmRazorPage()
        {
            LocalizationSourceName = solarcrmConsts.LocalizationSourceName;
        }
    }
}
