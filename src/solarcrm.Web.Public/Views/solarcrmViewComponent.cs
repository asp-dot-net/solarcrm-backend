﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace solarcrm.Web.Public.Views
{
    public abstract class solarcrmViewComponent : AbpViewComponent
    {
        protected solarcrmViewComponent()
        {
            LocalizationSourceName = solarcrmConsts.LocalizationSourceName;
        }
    }
}