﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace solarcrm
{
    public class solarcrmClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(solarcrmClientModule).GetAssembly());
        }
    }
}
