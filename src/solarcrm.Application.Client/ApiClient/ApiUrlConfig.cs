﻿using System;
using Abp.Extensions;

namespace solarcrm.ApiClient
{
    public static class ApiUrlConfig
    {
        public const string DefaultHostUrl = "http://app.solarcrm.co.in/"; //"https://localhost:44301/";//"http://172.16.124.219:8080"; //"https://localhost:44301/"; //TODO: Replace with PROD WebAPI URL.

        //public const string DefaultHostUrl = "http://App.solarcrm.co.in/";

        public static string BaseUrl { get; private set; }

        static ApiUrlConfig()
        {
            ResetBaseUrl();
        }

        public static void ChangeBaseUrl(string baseUrl)
        {
            BaseUrl = ReplaceLocalhost(NormalizeUrl(baseUrl));
        }

        public static void ResetBaseUrl()
        {
            BaseUrl = ReplaceLocalhost(DefaultHostUrl);
        }

        public static bool IsLocal => DefaultHostUrl.Contains("localhost");

        private static string NormalizeUrl(string baseUrl)
        {
            if (!Uri.TryCreate(baseUrl, UriKind.Absolute, out var uriResult) ||
                (uriResult.Scheme != "http" && uriResult.Scheme != "https"))
            {
                throw new ArgumentException("Unexpected base URL: " + baseUrl);
            }

            return uriResult.ToString().EnsureEndsWith('/');
        }

        private static string ReplaceLocalhost(string url)
        {
            return url.Replace("localhost", DebugServerIpAddresses.Current);
        }
    }
}