﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using solarcrm.MobileService.MCommon;
using solarcrm.MobileService.MCommon.Dto;
using solarcrm.MobileService.MCommonRespose;

namespace solarcrm.Common
{
	public class MobileCommonAppService : ProxyAppServiceBase, IMobileCommonAppService
	{
        public async Task<List<NamingString>> GetAllSolarTypeForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllSolarTypeForDropdown)));
        }

        public async Task<List<NamingString>> SearchCity(StringFilter city)
        {
            return await ApiClient.PostAsync<List<NamingString>>(GetEndpoint(nameof(SearchCity)), city);
        }

        public async Task<CityResult> CityResult(StringFilter cityName)
        {
            return await ApiClient.PostAsync<CityResult>(GetEndpoint(nameof(CityResult)), cityName);
        }

        public async Task<List<NamingString>> GetAllDivisionForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllDivisionForDropdown)));
        }

        public async Task<List<NamingString>> SearchSubDivision(StringFilter subDivision)
        {
            return await ApiClient.PostAsync<List<NamingString>>(GetEndpoint(nameof(SearchSubDivision)), subDivision);
        }

        public async Task<SubDivisionResult> SubDivisionResult(StringFilter subDivision)
        {
            return await ApiClient.PostAsync<SubDivisionResult>(GetEndpoint(nameof(SubDivisionResult)), subDivision);
        }

        public async Task<List<NamingString>> GetAllCircleForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllCircleForDropdown)));
        }

        public async Task<List<NamingString>> GetAllDiscomForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllDiscomForDropdown)));
        }

        public async Task<List<NamingString>> GetAllCategoryForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllCategoryForDropdown)));
        }

        public async Task<List<NamingString>> GetAllHeightOfStructureForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllHeightOfStructureForDropdown)));
        }

        public async Task<List<NamingString>> SearchChannelPartner(StringFilter input)
        {
            return await ApiClient.PostAsync<List<NamingString>>(GetEndpoint(nameof(SearchChannelPartner)), input);
        }

        public async Task<List<NamingString>> GetAllLeadSourceForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllLeadSourceForDropdown)));
        }

        public async Task<List<NamingString>> GetAllLeadStatusForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllLeadStatusForDropdown)));
        }

        public async Task<List<NamingString>> GetAllLeadTypeForDropdown()
        {
            return await ApiClient.GetAsync<List<NamingString>>(GetEndpoint(nameof(GetAllLeadTypeForDropdown)));
        }

        public async Task<BaseResponse> CheckDuplicateConsumerNoMobile(DuplicateConsumerFilter filter)
        {
           // return await ApiClient.PostAsync<List<NamingString>>(GetEndpoint(nameof(SearchSubDivision)), subDivision);
            return await ApiClient.PostAsync<BaseResponse>(GetEndpoint(nameof(CheckDuplicateConsumerNoMobile)), filter);
        }
    }
}

