﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Threading;
using solarcrm.Common.Dto;
using solarcrm.Dto;
using solarcrm.Editions.Dto;

namespace solarcrm.Common
{
    public class ProxyCommonLookupAppService : ProxyAppServiceBase, ICommonLookupAppService
    {
        public async Task<ListResultDto<SubscribableEditionComboboxItemDto>> GetEditionsForCombobox(bool onlyFreeItems = false)
        {
            return await ApiClient.GetAsync<ListResultDto<SubscribableEditionComboboxItemDto>>(GetEndpoint(nameof(GetEditionsForCombobox)));
        }

        public async Task<PagedResultDto<NameValueDto>> FindUsers(FindUsersInput input)
        {
            return await ApiClient.PostAsync<PagedResultDto<NameValueDto>>(GetEndpoint(nameof(FindUsers)), input);
        }

        public GetDefaultEditionNameOutput GetDefaultEditionName()
        {
            return AsyncHelper.RunSync(() =>
                ApiClient.GetAsync<GetDefaultEditionNameOutput>(GetEndpoint(nameof(GetDefaultEditionName))));
        }

        public Task<List<CommonLookupDto>> GetAllStateForDropdown(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllDistrictForDropdown(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllTalukaForDropdown(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllSolarTypeForDropdown()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllHeightStructureForDropdown()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllSubdivisionWiseData(string input)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllDivisionFilterDropdown(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllDiscomFilterDropdown(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllUserTypes()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetDistrictByState(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetTalukaByDistrict(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetCityByTaluka(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetEmployees(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetDiscomForDropdown()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> getDepartmentForDropdown()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetEmployeesByRole(string RoleName)
        {
            throw new System.NotImplementedException();
        }

        Task<List<CommonLookupDto>> ICommonLookupAppService.GetCircleForDropdown()
        {
            throw new System.NotImplementedException();
        }

        public DocumentReturnValuesDto SaveCommonDocument(byte[] ByteArray, string filename, string SectionName, int? JobId, int? DocumentTypeId, int? TenantId)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CommonLookupDto>> GetAllTalukaForDropdown(int[] id)
        {
            throw new System.NotImplementedException();
        }
    }
}
