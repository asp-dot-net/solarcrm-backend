﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.Dto;

namespace solarcrm.Customer
{
	public class ProxyLeadsAppService : ProxyAppServiceBase //, ILeadsAppService
	{
        public async Task CreateOrEdit(CreateOrEditLeadsDto input)
        {
            await ApiClient.PostAsync(GetEndpoint(nameof(CreateOrEdit)), input);
        }

        public async Task CreateOrEditLeadDocument(CreateOrEditLeadDocumentDto input)
        {
            await ApiClient.PostAsync(GetEndpoint(nameof(CreateOrEditLeadDocument)), input);
        }

        public async Task DeleteCustomer(EntityDto input)
        {
            await ApiClient.DeleteAsync(GetEndpoint(nameof(DeleteCustomer)), input);
        }

        public async Task DeleteDuplicateLeads(EntityDto input)
        {
            await ApiClient.DeleteAsync(GetEndpoint(nameof(DeleteDuplicateLeads)), input);
        }

        public async Task DeleteManageLead(int[] input)
        {
            await ApiClient.DeleteAsync(GetEndpoint(nameof(DeleteManageLead)), input);
        }

        public async Task<PagedResultDto<GetLeadsForViewDto>> GetAll(GetAllLeadsInput input)
        {
            return await ApiClient.GetAsync<PagedResultDto<GetLeadsForViewDto>>(GetEndpoint(nameof(GetAll)), input);
        }

        public async Task<GetLeadsForEditOutput> GetLeadsForEdit(EntityDto input)
        {
            return await ApiClient.GetAsync<GetLeadsForEditOutput>(GetEndpoint(nameof(GetLeadsForEdit)), input);
        }

        public async Task<GetLeadsForViewDto> GetLeadsForView(int id)
        {
            return await ApiClient.GetAsync<GetLeadsForViewDto>(GetEndpoint(nameof(GetLeadsForEdit)), id);
        }

        public async Task<FileDto> GetLeadsToExcel(GetAllLeadsForExcelInput input)
        {
            return await ApiClient.GetAsync<FileDto>(GetEndpoint(nameof(GetLeadsToExcel)), input);
        }

        public async Task LeadDocumentDelete(EntityDto input)
        {
            await ApiClient.DeleteAsync(GetEndpoint(nameof(LeadDocumentDelete)), input);
        }
    }
}

