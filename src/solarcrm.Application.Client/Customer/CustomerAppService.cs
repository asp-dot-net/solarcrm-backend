﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using solarcrm.DataVaults.Leads.Dto;
using solarcrm.MobileService.MCommonRespose;
using solarcrm.MobileService.MCustomer;
using solarcrm.MobileService.MCustomer.Dto;

namespace solarcrm.Customer
{
	public class CustomerAppService : ProxyAppServiceBase, ICustomerAppService
    {
        public async Task<PagedResultDto<CustomerDto>> GetAll(GetAllCustomerInput input)
        {
            return await ApiClient.GetAsync<PagedResultDto<CustomerDto>>(GetEndpoint(nameof(GetAll)), input);
        }

        public async Task<GetCustomerForViewDto> GetCustomerForView(NullableIdDto<int> input)
        {
            return await ApiClient.GetAsync<GetCustomerForViewDto>(GetEndpoint(nameof(GetCustomerForView)), input);
        }

        public async Task<BaseResponse> CreateOrEdit(CreateOrEditCustomerDto input)
        {
            return await ApiClient.PostAsync<BaseResponse>(GetEndpoint(nameof(CreateOrEdit)), input);
        }

        public async Task<GetCustomerForEditOutput> GetCustomerForEdit(NullableIdDto<int> input)
        {
            return await ApiClient.GetAsync<GetCustomerForEditOutput>(GetEndpoint(nameof(GetCustomerForEdit)), input);
        }
    }
}

