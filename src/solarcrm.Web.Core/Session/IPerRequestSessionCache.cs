﻿using System.Threading.Tasks;
using solarcrm.Sessions.Dto;

namespace solarcrm.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
