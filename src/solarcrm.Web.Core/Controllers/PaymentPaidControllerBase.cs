﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.AspNetCore.Mvc.Authorization;

using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using solarcrm.Authorization;
using solarcrm.DataVaults.StockItem.Dto;
using solarcrm.DataVaults.StockItem.Importing;
using solarcrm.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;
using solarcrm.MISReport.Importing.Dto;
using solarcrm.MISReport.Dto;
using solarcrm.MISReport.Importing;

namespace solarcrm.Web.Controllers
{
    public class PaymentPaidControllerBase : solarcrmControllerBase
    {
        private const int MaxFileBytes = 5242880; //5MB
        protected readonly IBinaryObjectManager BinaryObjectManager;
        protected readonly IBackgroundJobManager BackgroundJobManager;

        protected PaymentPaidControllerBase(
            IBinaryObjectManager binaryObjectManager,
            IBackgroundJobManager backgroundJobManager)
        {
            BinaryObjectManager = binaryObjectManager;
            BackgroundJobManager = backgroundJobManager;
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_ImportToExcel)]
        public async Task<JsonResult> ImportFromExcelMIS()
        {
            try
            {
                var file = Request.Form.Files.First();

                if (file == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (file.Length > 1048576 * 100) //100 MB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }

                byte[] fileBytes;
                using (var stream = file.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var tenantId = AbpSession.TenantId;
                var fileObject = new BinaryObject(tenantId, fileBytes, $"{DateTime.UtcNow} import from excel file.");

                await BinaryObjectManager.SaveAsync(fileObject);

                await BackgroundJobManager.EnqueueAsync<ImportMISReportToExcelJob, ImportMISReportFromExcelJobArgs>(new ImportMISReportFromExcelJobArgs
                {
                    TenantId = tenantId,
                    BinaryObjectId = fileObject.Id,
                    User = AbpSession.ToUserIdentifier()
                });

                return Json(new AjaxResponse(new { }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    
}
}
