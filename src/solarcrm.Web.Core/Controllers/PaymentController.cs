﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using solarcrm.Authorization;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.MultiTenancy;
using solarcrm.Payments;
using solarcrm.Payments.Dto;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace solarcrm.Web.Controllers
{
    // [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Payment)]
    public class PaymentController : solarcrmControllerBase
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IWebHostEnvironment _env;
        // private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<PaymentDetailsJob> _paymentDetailjobRepository;
        private readonly IRepository<PaymentReceiptUploadJob> _paymentReceiptUploadjobRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        public PaymentController(IBinaryObjectManager binaryObjectManager, IWebHostEnvironment env
            //, IRepository<ProductItem> productItemRepository
            , IRepository<Tenant> tenantRepository
            , ITempFileCacheManager tempFileCacheManager,
           IRepository<PaymentDetailsJob> paymentDetailjobRepository,
        IRepository<PaymentReceiptUploadJob> paymentReceiptUploadjobRepository,

        IRepository<Job.Jobs> jobRepository,
             IRepository<LeadActivityLogs> leadactivityRepository
       )
        {
            _binaryObjectManager = binaryObjectManager;
            _env = env;
            //_productItemRepository = productItemRepository;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _paymentDetailjobRepository = paymentDetailjobRepository;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            _paymentReceiptUploadjobRepository = paymentReceiptUploadjobRepository;
        }

        [HttpPost]
        //[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Payment_Create, AppPermissions.Pages_Tenant_Jobs_Payment_Edit)]
        public async Task<JsonResult> PaymentDetailsReceiptDocument()
        {
            try
            {
                Int32 LeadId = Convert.ToInt32(Request.Form["leadId"][0]);
                List<FileList> AuthorList = new List<FileList>();
                List<CreateOrEditPaymentReceiptItemsDto> PaymentReceiptList = new List<CreateOrEditPaymentReceiptItemsDto>();
                var files = Request.Form.Files;
                //var FileExt = fileformate.Split(',');
                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<Guid> filesOutput = new List<Guid>();
                string FinalFilePath = "";
                string uploadPath = "";
                foreach (var file in files)
                {
                    if (file.Length > MaxFileBytes) //1048576 1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }
                    FileInfo fi = new FileInfo(file.Name);



                    var Path1 = _env.WebRootPath;
                    var MainFolder = Path1;
                    MainFolder = MainFolder + "\\Documents";


                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    string FileName = DateTime.Now.Ticks + "_" + file.FileName;
                    string TenantPath = MainFolder + "\\" + TenantName + "\\";
                    string PaymentPath = TenantPath + "\\" + LeadId + "\\" + "PaymentReceiptList\\";
                    FinalFilePath = PaymentPath + "\\" + FileName;
                    uploadPath = "\\Documents" + "\\" + TenantName + "\\" + LeadId + "\\PaymentReceiptList\\" + FileName;


                    if (System.IO.Directory.Exists(MainFolder))
                    {
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(PaymentPath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(PaymentPath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(PaymentPath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(PaymentPath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(MainFolder);
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(PaymentPath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(PaymentPath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(PaymentPath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(PaymentPath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }


                    // collection of List of File  
                    AuthorList.Add(new FileList { FileName = file.Name, FilePath = uploadPath });

                }
                //add the file list in PaymentReceipt Path
                JArray jArraypaymentReceipt = JArray.Parse(Request.Form["paymentReceiptuploadList"][0]);
                bool check = false;
                // JSONArray jsonArray = new JSONArray(Request.Form["fileInfo"][0]);
                foreach (var item in jArraypaymentReceipt)
                {
                    var data = JsonConvert.DeserializeObject<CreateOrEditPaymentReceiptItemsDto>(item.ToString());
                    foreach (var item1 in AuthorList)
                    {
                        if (data.PaymentReceiptName == item1.FileName)
                        {
                            data.PaymentReceiptPath = item1.FilePath;
                            PaymentReceiptList.Add(data);
                            check = true;
                        }
                    }
                    if (check)
                    {
                        check = false;
                    }
                    else
                    {
                        PaymentReceiptList.Add(data);
                    }
                }
                string ActivityNotes = "";
                //Get the Payment Details Data
                var paymentDetailsdata = JsonConvert.DeserializeObject<CreateOrEditPaymentDto>(Request.Form["paymentDetails"][0]);
                // JArray jArraypaymentDetails = JArray.Parse(Request.Form["paymentDetails"][0]);
                if (paymentDetailsdata != null)
                {

                    var paymentDetails = ObjectMapper.Map<PaymentDetailsJob>(paymentDetailsdata);
                    var PaymentId = await _paymentDetailjobRepository.InsertOrUpdateAndGetIdAsync(paymentDetails);
                    if (paymentDetails.Id == 0)
                    {
                        ActivityNotes = "Payment Created";
                    }
                    else
                    {
                        ActivityNotes = "Payment Modified";
                    }
                    #region Activity Log
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 10;
                    //leadactivity.LeadAcitivityID = 0; // confusion
                    leadactivity.SectionId = 49; // Convert.ToInt32(Request.Form["sectionId"][0]);
                    leadactivity.LeadActionNote = ActivityNotes;
                    leadactivity.LeadId = LeadId; // Convert.ToInt32(Request.Form["leadId"][0]);
                    leadactivity.OrganizationUnitId = Convert.ToInt32(Request.Form["organizationId"][0]); //job.OrganizationUnitId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    await _leadactivityRepository.InsertAsync(leadactivity);
                    #endregion
                    //add the Payment Receipt List Data    
                    if (PaymentId != 0 && PaymentReceiptList.Count != 0)
                    {
                        foreach (var item in PaymentReceiptList)
                        {
                            try
                            {
                                if (item.PaymentRecEnterDate != null && item.PaymentRecTotalPaid != 0)
                                {
                                    PaymentReceiptUploadJob paymentReceipt = new PaymentReceiptUploadJob();
                                    paymentReceipt = await _paymentReceiptUploadjobRepository.FirstOrDefaultAsync(e => e.Id == item.Id);
                                    if (paymentReceipt == null)
                                    {
                                        paymentReceipt = new PaymentReceiptUploadJob();
                                        paymentReceipt.IsPaymentRecVerified = false;
                                    }
                                    //paymentReceipt.Id = Convert.ToInt32(item.Id);
                                    paymentReceipt.PaymentId = PaymentId;
                                    paymentReceipt.PaymentRecEnterDate = item.PaymentRecEnterDate;
                                    paymentReceipt.PaymentRecTotalPaid = item.PaymentRecTotalPaid;
                                    paymentReceipt.PaymentRecRefNo = item.PaymentRecRefNo;
                                    paymentReceipt.PaymentRecTypeId = item.PaymentRecTypeId;
                                    paymentReceipt.PaymentRecModeId = item.PaymentRecModeId;
                                    paymentReceipt.PaymentReceiptName = item.PaymentReceiptName;
                                    paymentReceipt.PaymentReceiptPath = item.PaymentReceiptPath;
                                    paymentReceipt.PaymentRecAddedById = item.PaymentRecAddedById;
                                    //
                                    await _paymentReceiptUploadjobRepository.InsertOrUpdateAsync(paymentReceipt);

                                    if (paymentReceipt.Id == 0)
                                    {
                                        ActivityNotes = "Payment Receipt Created";
                                    }
                                    else
                                    {
                                        ActivityNotes = "Payment Receipt Modified";
                                    }
                                    #region Activity Log
                                    //Add Activity Log
                                    leadactivity = new LeadActivityLogs();
                                    leadactivity.LeadActionId = 10;
                                    //leadactivity.LeadAcitivityID = 0; // confusion
                                    leadactivity.SectionId = 49; // Convert.ToInt32(Request.Form["sectionId"][0]);
                                    leadactivity.LeadActionNote = ActivityNotes;
                                    leadactivity.LeadId = Convert.ToInt32(Request.Form["leadId"][0]);
                                    leadactivity.OrganizationUnitId = Convert.ToInt32(Request.Form["organizationId"][0]); //job.OrganizationUnitId;
                                    if (AbpSession.TenantId != null)
                                    {
                                        leadactivity.TenantId = (int)AbpSession.TenantId;
                                    }
                                    await _leadactivityRepository.InsertAsync(leadactivity);
                                    #endregion
                                }

                            }
                            catch (System.Exception ex)
                            {
                                throw ex;
                            }
                        }
                    }

                }
                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}
public class FileList
{
    // Auto-Initialized properties
    public string FileName { get; set; }
    public string FilePath { get; set; }
}