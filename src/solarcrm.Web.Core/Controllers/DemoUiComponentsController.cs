using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using solarcrm.Common;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.DocumentLibrary;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DemoUiComponents.Dto;
using solarcrm.EntityFrameworkCore;
using solarcrm.MultiTenancy;
using solarcrm.Storage;
//using static solarcrm.Configuration.AppSettings.ExternalLoginProvider;

namespace solarcrm.Web.Controllers
{
    //[AbpMvcAuthorize]
    public class DemoUiComponentsController : solarcrmControllerBase
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IWebHostEnvironment _env;
        // private readonly IRepository<ProductItem> _productItemRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<DocumentLibrary> _documentlibraryRepository;
        private readonly IRepository<DataVaulteActivityLog> _dataVaultsActivityLogRepository;
        private readonly IDbContextProvider<solarcrmDbContext> _dbContextProvider;

        public DemoUiComponentsController(IBinaryObjectManager binaryObjectManager, IWebHostEnvironment env
            //, IRepository<ProductItem> productItemRepository
            , IRepository<Tenant> tenantRepository
            , ITempFileCacheManager tempFileCacheManager,
           
            IRepository<DocumentLibrary> documentlibraryRepository
            , IRepository<DataVaulteActivityLog> dataVaultsActivityLogRepository
            , IDbContextProvider<solarcrmDbContext> dbContextProvider)
        {
            _binaryObjectManager = binaryObjectManager;
            _env = env;
            //_productItemRepository = productItemRepository;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
           
            _documentlibraryRepository = documentlibraryRepository;
            _dataVaultsActivityLogRepository = dataVaultsActivityLogRepository;
            _dbContextProvider = dbContextProvider;
        }

        [HttpPost]
        public async Task<JsonResult> UploadFiles()
        {
            try
            {
                var files = Request.Form.Files;

                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<UploadFileOutput> filesOutput = new List<UploadFileOutput>();

                foreach (var file in files)
                {
                    if (file.Length > 1048576) //1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }

                    var fileObject = new BinaryObject(AbpSession.TenantId, fileBytes, $"Demo ui, uploaded file {DateTime.UtcNow}");
                    await _binaryObjectManager.SaveAsync(fileObject);

                    filesOutput.Add(new UploadFileOutput
                    {
                        Id = fileObject.Id,
                        FileName = file.FileName
                    });
                }

                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadDocumentLibrary(int? id, string title)
        {
            try
            {
                var files = Request.Form.Files;

                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<Guid> filesOutput = new List<Guid>();

                foreach (var file in files)
                {
                    if (file.Length > 1048576) //1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }



                    //var ByteArray = _tempFileCacheManager.GetFile(fileBytes);

                    //if (ByteArray == null)
                    //{
                    //	throw new UserFriendlyException("There is no such file with the token: " + input.FileToken);
                    //}

                    //if (ByteArray.Length > MaxFileBytes)
                    //{
                    //	throw new UserFriendlyException("Document size exceeded");
                    //}

                    var Path1 = solarcrm.solarcrmConsts.DocumentPath;
                    var MainFolder = Path1;
                    MainFolder = MainFolder + "\\Documents";


                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    string FileName = DateTime.Now.Ticks + "_" + file.FileName;
                    string TenantPath = MainFolder + "\\" + TenantName + "\\";
                    string SignaturePath = TenantPath + "DocumentLibrary\\";
                    var FinalFilePath = SignaturePath + "\\" + FileName;



                    if (System.IO.Directory.Exists(MainFolder))
                    {
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(MainFolder);
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }

                    if (id == 0)
                    {
                        DocumentLibrary doc = new DocumentLibrary();
                        doc.Title = title;
                        doc.FileName = FileName;
                        doc.FilePath = "\\Documents" + "\\" + TenantName + "\\DocumentLibrary\\" + FileName;
                        doc.FileType = file.ContentType;
                        if (AbpSession.TenantId != null)
                        {
                            doc.TenantId = (int)AbpSession.TenantId;
                        }
                        var documentlibraryID=await _documentlibraryRepository.InsertAndGetIdAsync(doc);
                        DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                        if (AbpSession.TenantId != null)
                        {
                            dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                        }
                        dataVaulteActivityLog.SectionId = 22; // CancelReasons Pages Name
                        dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Document; // Created
                        dataVaulteActivityLog.ActionNote = "Document library Created Name :-" + doc.FileName;
                        dataVaulteActivityLog.SectionValueId = documentlibraryID;
                        await _dataVaultsActivityLogRepository.InsertAsync(dataVaulteActivityLog);
                    }
                    if (id > 0)
                    {
                        var Doc = _documentlibraryRepository.GetAll().Where(e => e.Id == id).FirstOrDefault();
                        DataVaulteActivityLog dataVaulteActivityLog = new DataVaulteActivityLog();
                        if (AbpSession.TenantId != null)
                        {
                            dataVaulteActivityLog.TenantId = (int)AbpSession.TenantId;
                        }
                        dataVaulteActivityLog.SectionId = 22; // CancelReasons Pages Name
                        dataVaulteActivityLog.ActionId = (int?)CrudActionConsts.Updated; // Updated
                        dataVaulteActivityLog.ActionNote = "Document library Updated Name :-" + FileName;
                        dataVaulteActivityLog.SectionValueId = id;
                        var documentlibraryActivityLogId = await _dataVaultsActivityLogRepository.InsertAndGetIdAsync(dataVaulteActivityLog);

                        #region Added Log History
                        var List = new List<DataVaultsHistory>();
                        if (Doc.Title != title)
                        {
                            DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                            if (AbpSession.TenantId != null)
                            {
                                dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                            }
                            dataVaultsHistory.FieldName = "Title";
                            dataVaultsHistory.PrevValue = Doc.Title;
                            dataVaultsHistory.CurValue = title;
                            dataVaultsHistory.Action = "Edit";
                            dataVaultsHistory.ActionId = (int?)CrudActionConsts.Updated;
                            dataVaultsHistory.ActivityLogId = documentlibraryActivityLogId;
                            dataVaultsHistory.SectionId = 22; // Pages name
                            dataVaultsHistory.SectionValueId = id;
                            List.Add(dataVaultsHistory);
                        }
                         if (Doc.FileName != FileName)
                        {
                            DataVaultsHistory dataVaultsHistory = new DataVaultsHistory();
                            if (AbpSession.TenantId != null)
                            {
                                dataVaultsHistory.TenantId = (int)AbpSession.TenantId;
                            }
                            dataVaultsHistory.FieldName = "FileName";
                            dataVaultsHistory.PrevValue = Doc.FileName;
                            dataVaultsHistory.CurValue = FileName;
                            dataVaultsHistory.Action = "Edit";
                            dataVaultsHistory.ActionId = (int?)CrudActionConsts.Document;
                            dataVaultsHistory.ActivityLogId = documentlibraryActivityLogId;
                            dataVaultsHistory.SectionId = 22; // Pages name
                            dataVaultsHistory.SectionValueId = id;
                            List.Add(dataVaultsHistory);
                        }

                        await _dbContextProvider.GetDbContext().DataVaultsHistories.AddRangeAsync(List);
                        await _dbContextProvider.GetDbContext().SaveChangesAsync();
                        #endregion
                        Doc.FileName = FileName;
                        Doc.FileType = file.ContentType;
                        Doc.FilePath = "\\Documents" + "\\" + TenantName + "\\DocumentLibrary\\" + FileName;
                        Doc.Title = title;
                        if (AbpSession.TenantId != null)
                        {
                            Doc.TenantId = (int)AbpSession.TenantId;
                        }
                        await _documentlibraryRepository.UpdateAsync(Doc);

                    }

                }

                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

       
    }
}