﻿using Abp.Domain.Repositories;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.MultiTenancy;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Abp.IO.Extensions;
using solarcrm.DataVaults.Leads;


namespace solarcrm.Web.Controllers
{
    public class EstimatedPaidTrackerController : solarcrmControllerBase
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IWebHostEnvironment _env;

        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        //private readonly IRepository<DocumentLibrary> _documentlibraryRepository;
        //private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly IRepository<Job.Jobs> _jobRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        public EstimatedPaidTrackerController(IBinaryObjectManager binaryObjectManager, IWebHostEnvironment env
            //, IRepository<ProductItem> productItemRepository
            , IRepository<Tenant> tenantRepository
            , ITempFileCacheManager tempFileCacheManager,

            IRepository<Job.Jobs> jobRepository,
             IRepository<LeadActivityLogs> leadactivityRepository
        //IRepository<DocumentLibrary> documentlibraryRepository,
        //     IRepository<LeadDocuments> leadDocument,
            )
        {
            _binaryObjectManager = binaryObjectManager;
            _env = env;
            //_productItemRepository = productItemRepository;
            _tenantRepository = tenantRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _jobRepository = jobRepository;
            _leadactivityRepository = leadactivityRepository;
            //_documentlibraryRepository = documentlibraryRepository;
            //_leadDocumentRepository = leadDocument;
        }
        [HttpPost]
        public async Task<JsonResult> PaymentReceiptUploadDocument()
        {
            try
            {
                var job = await _jobRepository.FirstOrDefaultAsync(e => e.Id == Convert.ToInt32(Request.Form["Id"][0]));

                var files = Request.Form.Files;
                //var FileExt = fileformate.Split(',');
                //Check input
                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                List<Guid> filesOutput = new List<Guid>();
                string FinalFilePath = "";
                string uploadPath = "";
                foreach (var file in files)
                {
                    if (file.Length > 1048576) //1MB
                    {
                        throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    }

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }
                    FileInfo fi = new FileInfo(file.Name);



                    var Path1 = _env.WebRootPath;
                    var MainFolder = Path1;
                    MainFolder = MainFolder + "\\Documents";


                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    string FileName = DateTime.Now.Ticks + "_" + file.FileName;
                    string TenantPath = MainFolder + "\\" + TenantName + "\\";
                    string SignaturePath = TenantPath + "\\" + job.LeadId + "\\" + "LeadDocumentList\\";
                    FinalFilePath = SignaturePath + "\\" + FileName;
                    uploadPath = "\\Documents" + "\\" + TenantName + "\\" + job.LeadId + "\\LeadDocumentList\\" + FileName;


                    if (System.IO.Directory.Exists(MainFolder))
                    {
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(MainFolder);
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }


                }
                
                if (job.Id != 0)
                {
                    // applicationFlag = 1 to SignApplication added , 0 to LoadReduce added
                   // if (Request.Form["applicationFlag"][0] == "1")
                    {
                        job.EastimatePaymentRefNumber = Convert.ToString(Request.Form["estimatePayementRefNumber"][0]);
                        job.EastimatePayDate = Convert.ToDateTime(Request.Form["eastimatePayDate"][0]);
                        job.EstimatePaymentReceiptPath = uploadPath == "" ? job.EstimatePaymentReceiptPath : uploadPath; // "\\Documents" + "\\" + TenantName + "\\LeadDocumentList\\" + FileName;
                    }
                   


                    #region Activity Log
                    //Add Activity Log
                    LeadActivityLogs leadactivity = new LeadActivityLogs();
                    leadactivity.LeadActionId = 11;
                    //leadactivity.LeadAcitivityID = 0;
                    leadactivity.SectionId = Convert.ToInt32(Request.Form["SectionId"][0]);
                    //if (Request.Form["applicationFlag"][0] == "1")
                    {
                        leadactivity.LeadActionNote = "Payment Reciept Upload Job Application Details";
                    }
                   

                    leadactivity.LeadId = job.LeadId;
                    if (AbpSession.TenantId != null)
                    {
                        leadactivity.TenantId = (int)AbpSession.TenantId;
                    }
                    var leadactid = _leadactivityRepository.InsertAndGetId(leadactivity);

                    #endregion
                }
                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

    }
}
