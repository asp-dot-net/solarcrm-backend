﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.BackgroundJobs;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using solarcrm.Authorization;
using solarcrm.DataVaults.BillOfMaterial.Dto;
using solarcrm.DataVaults.BillOfMaterial.Importing;
using solarcrm.Storage;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace solarcrm.Web.Controllers
{
    public class BillOfMaterialControllerBase : solarcrmControllerBase
    {
        protected readonly IBinaryObjectManager BinaryObjectManager;
        protected readonly IBackgroundJobManager BackgroundJobManager;

        protected BillOfMaterialControllerBase(
             IBinaryObjectManager binaryObjectManager,
            IBackgroundJobManager backgroundJobManager)
        {
            BinaryObjectManager = binaryObjectManager;
            BackgroundJobManager = backgroundJobManager;
        }

        [HttpPost]
       // [AbpMvcAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_ImportToExcel)]
        public async Task<JsonResult> ImportFromExcel()
        {
            try
            {
                var file = Request.Form.Files.First();

                if (file == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (file.Length > 1048576 * 100) //100 MB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }

                byte[] fileBytes;
                using (var stream = file.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var tenantId = AbpSession.TenantId;
                var fileObject = new BinaryObject(tenantId, fileBytes, $"{DateTime.UtcNow} import from excel file. filename ={file.FileName}");

                await BinaryObjectManager.SaveAsync(fileObject);
                try
                {
                    await BackgroundJobManager.EnqueueAsync<ImportBillOfMaterialToExcelJob, ImportBillOfMaterialFromExcelJobArgs>(new ImportBillOfMaterialFromExcelJobArgs
                    {
                        TenantId = tenantId,
                        BinaryObjectId = fileObject.Id,
                        User = AbpSession.ToUserIdentifier()
                    });
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return Json(new AjaxResponse(new { }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}
