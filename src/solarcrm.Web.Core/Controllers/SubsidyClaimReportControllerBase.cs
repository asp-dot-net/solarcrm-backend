﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.BackgroundJobs;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using solarcrm.Authorization;
using solarcrm.MISReport.Dto;
using solarcrm.MISReport.Importing;
using solarcrm.Storage;
using System.Linq;
using System.Threading.Tasks;
using System;
using Abp.IO.Extensions;
using solarcrm.Tracker.SubsidyTracker.Importing;
using solarcrm.Tracker.SubsidyTracker.Dto;

namespace solarcrm.Web.Controllers
{
    public class SubsidyClaimReportControllerBase : solarcrmControllerBase
    {
        private const int MaxFileBytes = 5242880; //5MB
        protected readonly IBinaryObjectManager BinaryObjectManager;
        protected readonly IBackgroundJobManager BackgroundJobManager;

        protected SubsidyClaimReportControllerBase(
            IBinaryObjectManager binaryObjectManager,
            IBackgroundJobManager backgroundJobManager)
        {
            BinaryObjectManager = binaryObjectManager;
            BackgroundJobManager = backgroundJobManager;
        }

        [HttpPost]
        [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Leads_MISReport_ImportToExcel)]
        public async Task<JsonResult> ImportFromExcelSubsidyClaim()
        {
            try
            {
                var file = Request.Form.Files.First();

                if (file == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (file.Length > 1048576 * 100) //100 MB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }

                byte[] fileBytes;
                using (var stream = file.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                var tenantId = AbpSession.TenantId;
                var fileObject = new BinaryObject(tenantId, fileBytes, $"{DateTime.UtcNow} import from excel file. filename ={file.FileName}");

                await BinaryObjectManager.SaveAsync(fileObject);

                await BackgroundJobManager.EnqueueAsync<ImportSubsidyClaimReportToExcelJob, ImportSubsidyClaimReportFromExcelJobArgs>(new ImportSubsidyClaimReportFromExcelJobArgs
                {
                    TenantId = tenantId,
                    FileName = file.Name,
                    BinaryObjectId = fileObject.Id,
                    User = AbpSession.ToUserIdentifier()
                });

                return Json(new AjaxResponse(new { }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}
