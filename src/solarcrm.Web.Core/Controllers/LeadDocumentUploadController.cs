﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.IO.Extensions;
using Abp.Notifications;
using Abp.Runtime.Security;
using Abp.UI;
using Abp.Web.Models;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.DocumentLibrary;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.DocumentRequests;
using solarcrm.MultiTenancy;
using solarcrm.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace solarcrm.Web.Controllers
{
    public class LeadDocumentUploadController : solarcrmControllerBase
    {
        private const int MaxFileBytes = 5242880; //5MB
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IWebHostEnvironment _env;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IRepository<DocumentLibrary> _documentlibraryRepository;
        private readonly IRepository<LeadDocuments> _leadDocumentRepository;
        private readonly IRepository<LeadActivityLogs> _leadactivityRepository;
        private readonly IRepository<Job.Jobs> _jobsRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<DocumentRequest> _documentRequestRepository;
        private readonly IRepository<DocumentRequestLinkHistory> _documentRequestLinkHistoryRepository;
       
        public LeadDocumentUploadController(IBinaryObjectManager binaryObjectManager, IWebHostEnvironment env
            , IRepository<Tenant> tenantRepository
            , IRepository<LeadDocuments> leadDocument
            , IRepository<LeadActivityLogs> leadactivityRepository
            , ITempFileCacheManager tempFileCacheManager
            , IRepository<Job.Jobs> jobsRepository
            , IUnitOfWorkManager unitOfWorkManager
            , IRepository<DocumentRequest> documentRequestRepository
            , IRepository<DocumentRequestLinkHistory> documentRequestLinkHistoryRepository
            

       )
        {
            _binaryObjectManager = binaryObjectManager;
            _env = env;
            _leadDocumentRepository = leadDocument;
            _tenantRepository = tenantRepository;
            _leadactivityRepository = leadactivityRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _jobsRepository = jobsRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _documentRequestRepository = documentRequestRepository;
            _documentRequestLinkHistoryRepository = documentRequestLinkHistoryRepository;           
        }

        [HttpPost]
        public async Task<JsonResult> UploadLeadDocument()
        {
            try
            {
                var files = Request.Form.Files;

                if (files == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }
                Int32 LeadId = Convert.ToInt32(Request.Form["leadId"][0]);
                List<Guid> filesOutput = new List<Guid>();

                foreach (var file in files)
                {
                    //if (file.Length > 1048576) //1MB
                    //{
                    //    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                    //}

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }
                    FileInfo fi = new FileInfo(file.Name);

                    var jobnumber= _jobsRepository.GetAll().Where(e => e.LeadId == LeadId).FirstOrDefault().JobNumber;


                    var Path1 = solarcrm.solarcrmConsts.DocumentPath;
                    var MainFolder = Path1;
                    MainFolder = MainFolder + "\\Documents";


                    var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                    string FileName = DateTime.Now.Ticks + "_" + file.FileName;
                    string TenantPath = MainFolder + "\\" + TenantName + "\\";
                    string SignaturePath = TenantPath + "\\" + jobnumber + "\\" + "LeadDocumentList\\";
                    var FinalFilePath = SignaturePath + "\\" + FileName;

                    if (System.IO.Directory.Exists(MainFolder))
                    {
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);

                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(MainFolder);
                        if (System.IO.Directory.Exists(TenantPath))
                        {
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(TenantPath);
                            if (System.IO.Directory.Exists(SignaturePath))
                            {
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                            else
                            {
                                System.IO.Directory.CreateDirectory(SignaturePath);
                                using (MemoryStream mStream = new MemoryStream(fileBytes))
                                {
                                    System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                }
                            }
                        }
                    }

                    if (Request.Form["leadId"][0] != "0")
                    {
                        LeadDocuments doc = new LeadDocuments();
                        doc.LeadDocumentId = Convert.ToInt32(Request.Form["leadId"][0]);// leadid;
                        doc.DocumentId = Convert.ToInt32(Request.Form["documentId"][0]); // documentid;
                        doc.DocumentNumber = Convert.ToString(Request.Form["documentNumber"][0]); // documentNumber;
                        doc.LeadDocumentPath = "\\Documents" + "\\" + TenantName + "\\" + jobnumber + "\\LeadDocumentList\\" + FileName;
                        //doc.FileType = file.ContentType;
                        if (AbpSession.TenantId != null)
                        {
                            doc.TenantId = (int)AbpSession.TenantId;
                        }
                        var DocumentId = await _leadDocumentRepository.InsertAndGetIdAsync(doc);

                        //Add Activity Log
                        LeadActivityLogs leadactivity = new LeadActivityLogs();
                        leadactivity.LeadActionId = 17;
                        leadactivity.SectionId = 51;
                        leadactivity.LeadActionNote = "Lead Document Created Name :- " + FileName;
                        leadactivity.LeadId = Convert.ToInt32(Request.Form["leadId"][0]);
                        leadactivity.OrganizationUnitId = Convert.ToInt32(Request.Form["OrganizationUnitId"][0]); // job.OrganizationUnitId;
                        leadactivity.LeadDocumentPath = doc.LeadDocumentPath;
                        if (AbpSession.TenantId != null)
                        {
                            leadactivity.TenantId = (int)AbpSession.TenantId;
                        }
                        await _leadactivityRepository.InsertAsync(leadactivity);
                    }
                }

                return Json(new AjaxResponse(filesOutput));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadDocumentRequest(string STR)
        {
            try
            {
                if (!string.IsNullOrEmpty(STR))
                {
                    var Ids = HttpUtility.UrlDecode(SimpleStringCipher.Instance.Decrypt(STR, AppConsts.DefaultPassPhrase));

                    var Id = Ids.Split(",");
                    int TenantId = Convert.ToInt32(Id[0]);
                    int LeadId = Convert.ToInt32(Id[1]);
                    int DocRequestId = Convert.ToInt32(Id[2]);
                    int SectionId = Convert.ToInt32(Id[3]);
                    int DocTypeId = Convert.ToInt32(Id[3]);
                    var jobnumber = "";
                    var files = Request.Form.Files;
                    var path = "";
                    using (var uow = _unitOfWorkManager.Begin())
                    {
                        using (_unitOfWorkManager.Current.SetTenantId(TenantId))
                        {
                            var documentRequest = _documentRequestRepository.GetAll().Include(e => e.DocTypeFk).Include(e => e.LeadFk).Where(e => e.Id == DocRequestId).FirstOrDefault();

                            foreach (var file in files)
                            {
                                byte[] fileBytes;
                                using (var stream = file.OpenReadStream())
                                {
                                    fileBytes = stream.GetAllBytes();
                                }

                                var Path1 = solarcrm.solarcrmConsts.DocumentPath;
                                var MainFolder = Path1;
                                MainFolder = MainFolder + "\\Documents";


                                var TenantName = _tenantRepository.GetAll().Where(e => e.Id == AbpSession.TenantId).Select(e => e.TenancyName).FirstOrDefault();
                                FileInfo fi = new FileInfo(file.FileName);
                                jobnumber = _jobsRepository.GetAll().Where(e => e.LeadId == LeadId).FirstOrDefault().JobNumber;

                                string FileName = DateTime.Now.Ticks + "_" + file.FileName;
                                string TenantPath = MainFolder + "\\" + TenantName + "\\";
                                string SignaturePath = TenantPath + "\\" + jobnumber + "\\" + "LeadDocumentList\\";
                                var FinalFilePath = SignaturePath + "\\" + FileName;

                                if (System.IO.Directory.Exists(MainFolder))
                                {
                                    if (System.IO.Directory.Exists(TenantPath))
                                    {
                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(TenantPath);

                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    System.IO.Directory.CreateDirectory(MainFolder);
                                    if (System.IO.Directory.Exists(TenantPath))
                                    {
                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        System.IO.Directory.CreateDirectory(TenantPath);
                                        if (System.IO.Directory.Exists(SignaturePath))
                                        {
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                        else
                                        {
                                            System.IO.Directory.CreateDirectory(SignaturePath);
                                            using (MemoryStream mStream = new MemoryStream(fileBytes))
                                            {
                                                System.IO.File.WriteAllBytes(string.Format("{0}", FinalFilePath), fileBytes);
                                            }
                                        }
                                    }
                                }

                                path = "\\Documents" + "\\" + TenantName + "\\" + jobnumber + "\\LeadDocumentList\\" + FileName;
                                LeadDocuments doc = new LeadDocuments();
                                doc.LeadDocumentId = LeadId;// leadid;
                                doc.DocumentId = DocTypeId; // documentid;
                                doc.DocumentNumber = ""; // documentNumber;
                                doc.LeadDocumentPath = path;
                                //doc.FileType = file.ContentType;
                                if (AbpSession.TenantId != null)
                                {
                                    doc.TenantId = (int)AbpSession.TenantId;
                                }
                                var DocumentId = await _leadDocumentRepository.InsertAndGetIdAsync(doc);
                            }

                            documentRequest.IsSubmitted = true;
                            await _documentRequestRepository.UpdateAsync(documentRequest);
                            var Link = _documentRequestLinkHistoryRepository.GetAll().Where(e => e.DocumentRequestId == DocRequestId);
                            foreach (var item in Link)
                            {
                                item.Expired = true;
                                await _documentRequestLinkHistoryRepository.UpdateAsync(item);
                            }

                            string msg = string.Format(documentRequest.DocTypeFk.Name + " Received For {0} Job.", jobnumber);

                            uow.Complete();
                            LeadActivityLogs leadactivity = new LeadActivityLogs();

                            leadactivity.LeadActionId = 17;
                            leadactivity.LeadId = Convert.ToInt32(LeadId);
                            leadactivity.CreatorUserId = 1;
                            leadactivity.TenantId = TenantId;
                            leadactivity.SectionId = SectionId;
                            leadactivity.LeadActionNote = files.Count + " Documents Received For " + documentRequest.DocTypeFk.Name;
                            leadactivity.LeadDocumentPath = path;

                            await _leadactivityRepository.InsertAndGetIdAsync(leadactivity);



                            return Json(new AjaxResponse(new { success = true }));
                        }
                    }
                }
                else
                {
                    return Json(new AjaxResponse(new { success = false }));
                }

            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpGet]
        public async Task<FileResult> DownloadFiles(int LeadId, string OrgName)
        {
            var webRoot = _env.ContentRootPath;
            var fileName = "DocumentZip.zip";
            var tempOutput = webRoot + "//wwwroot//Documents//" + OrgName + "//TempFile//" + fileName;
            var folderPath = Path.Combine(_env.ContentRootPath + "//wwwroot//Documents//" + OrgName +"/"+ LeadId, "LeadDocumentList");

            using (ZipOutputStream IzipOutputStream = new ZipOutputStream(System.IO.File.Create(tempOutput)))
            {
                IzipOutputStream.SetLevel(9);
                byte[] buffer = new byte[4096];
                // var imageList = new List<string>();
                var imageList = Directory.GetFiles(folderPath);
                if (imageList != null)
                {
                    for (int i = 0; i < imageList.Length; i++)
                    {
                        ZipEntry entry = new ZipEntry(Path.GetFileName(imageList[i]));
                        entry.DateTime = DateTime.Now;
                        entry.IsUnicodeText = true;
                        IzipOutputStream.PutNextEntry(entry);

                        using (FileStream oFileStream = System.IO.File.OpenRead(imageList[i]))
                        {
                            int sourceBytes;
                            do
                            {
                                sourceBytes = oFileStream.Read(buffer, 0, buffer.Length);
                                IzipOutputStream.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                    }
                    IzipOutputStream.Finish();
                    IzipOutputStream.Flush();
                    IzipOutputStream.Close();
                    byte[] finalResult = System.IO.File.ReadAllBytes(tempOutput);
                    if (System.IO.File.Exists(tempOutput))
                    {
                        System.IO.File.Delete(tempOutput);
                    }
                    if (finalResult == null || !finalResult.Any())
                    {
                        throw new Exception(String.Format("Nothing found"));
                    }

                    return File(finalResult, "application/zip", fileName);
                }
                else
                {
                    return File("", "application/zip", fileName);
                }
               
            }

            
        }
    }
}
