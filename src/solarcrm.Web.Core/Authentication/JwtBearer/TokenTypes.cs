﻿namespace solarcrm.Web.Authentication.JwtBearer
{
    public enum TokenType
    {
        AccessToken,
        RefreshToken
    }
}
