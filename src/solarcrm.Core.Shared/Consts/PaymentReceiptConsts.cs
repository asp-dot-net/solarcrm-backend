﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class PaymentReceiptConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
        public const int JobNumber = 50;
        public const int PaymentRecRefNo = 100;
        public const int PaymentRecChequeStatus = 20;
        public const int PaymentReceiptName = 100;
        public const int PaymentReceiptPath = 200;
        public const int PaymentRecNotes = 700;
    }
}
