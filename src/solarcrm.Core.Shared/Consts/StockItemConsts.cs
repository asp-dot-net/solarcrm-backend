﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class StockItemConsts
    {
		public const int MinNameLength = 3;
		public const int MaxNameLength = 500;

		public const int MinManufacturerLength = 3;
		public const int MaxManufacturerLength = 200;

		public const int MinModelLength = 1;
		public const int MaxModelLength = 50;

		public const int MinSeriesLength = 2;
		public const int MaxSeriesLength = 50;

		public const int MinUMOLength = 2;
		public const int MaxUMOLength = 20;
	}
}
