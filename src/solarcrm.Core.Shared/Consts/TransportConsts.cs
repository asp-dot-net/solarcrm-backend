﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class TransportConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
        public const int Name = 150;
        public const int TransportNotes = 500;
    }
}
