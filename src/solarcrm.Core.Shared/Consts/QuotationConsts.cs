﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class QuotationConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
        public const int JobNumber = 50;
        public const int CustomerName = 250;
        public const int Address = 400;
        public const int Mobile = 20;
        public const int Email = 60;
        public const int SolarAdvisiorName = 100;
        public const int QuotationNumber = 30;
        public const int QuoteSignFileName = 150;
        public const int QuoteSignFilePath = 250;
        public const int QuoteSignSignIP = 100;
        //public const int  = 50;
    }
}
