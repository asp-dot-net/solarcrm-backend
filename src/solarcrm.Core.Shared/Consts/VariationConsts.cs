﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class VariationConsts
    {
        public const int MinActionLength = 1;
        public const int MaxActionLength = 10;
    }
}
