﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class PaymentDetailsConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
        public const int PaymentAccountInvoiceNotes = 500;

        public const int SSGSTNo = 70;
        public const int PaymentSSNotes = 800;
        public const int PaymentSTCNotes = 800;
    }
}
