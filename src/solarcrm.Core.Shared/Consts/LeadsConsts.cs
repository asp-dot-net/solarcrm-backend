﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class LeadsConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
        public const int MobileLength = 15;
        public const int EmailLength = 65;
        public const int AddressLength = 200;
        public const int NotesLength = 500;
        public const int CategoryLength = 20;
        public const int BankAccountHolderNameLength = 100;
        public const int BankNameLength = 100;
        public const int BankQrCodePathLength = 200;
        public const int StateNameLength = 100;
        public const int DistrictNameLength = 100;
        public const int TalukaNameLength = 100;
        public const int CityNameLength = 100;
        public const int SourceNameLength = 100;
        public const int ExcelAddressLength = 150;
        public const int GoogleLength = 100;
        public const int IFSCCodeLength = 15;
        public const int ActivityNotesLength = 800;
    }
}
