﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class LeadActivityConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
    }
}
