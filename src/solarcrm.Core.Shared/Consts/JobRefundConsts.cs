﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class JobRefundConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100; 
        public const int NotesLength = 500;       
        public const int BankAccountHolderNameLength = 100;
        public const int BankNameLength = 100;
        public const int RefundReceiptPathLength = 200;
        public const int IFSCCodeLength = 15;
        public const int RefundReceiptNo = 100;
    }
}
