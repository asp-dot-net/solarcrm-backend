﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class StockCategoryConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 32;
    }
}
