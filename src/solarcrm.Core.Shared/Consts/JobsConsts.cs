﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class JobsConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
        public const int JobNumber = 50;
        public const int InstallerNotes = 500;
        public const int NotesForInstallationDepartment = 500;
        public const int ApplicationNumber = 50;
        public const int GUVNLRegisterationNo = 50;
        public const int EastimatePaymentRefNumber = 50;
        public const int EastimatePaidStatus = 25;
        public const int EastimatePaymentResponse = 25;
        public const int EstimatePaymentReceiptPath = 250;
        public const int OtpVerifed = 25;
        public const int SignApplicationPath = 250;
        public const int LoadReduceReasonApplication = 500;
        public const int LoadReducePathApplication = 250;
        public const int LastComment = 500;
        public const int PhaseOfInverter = 100;
        public const int DiscomMeterNo = 100;
        public const int SolarMeterNo = 100;
        public const int DiscomBidirectionalMeterPath = 250;
        public const int DepositNotes = 500;

        public const int MeterAgreementGivenTo = 80;
        public const int MeterAgreementNo = 80;
    }
}
