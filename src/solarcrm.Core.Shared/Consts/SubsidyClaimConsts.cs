﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class SubsidyClaimConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;
        public const int SubsidyClaimNo= 100;
        public const int SubsidyClaimNotes = 500;
        public const int OCCopyPath = 250;
        public const int SubcidyReceiptsPath = 250;
        public const int FileName = 150;
        public const int FilePath = 250;
        public const int FileStatus = 40;
        public const int SubsidyProjectNumber = 50;


    }
}
