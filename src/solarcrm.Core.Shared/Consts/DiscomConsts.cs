﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Consts
{
    public class DiscomConsts
    {
        public const int MinNameLength = 1;
        public const int MaxNameLength = 100;

        public const int MinABBLength = 1;
        public const int MaxABBLength = 32;

        public const int MinPaymentLinkLength = 1;
        public const int MaxPaymentLinkLength = 500;
    }
}
