﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Common
{
    public enum CrudActionConsts
    {
        Created = 1,
        Updated = 2,
        Deleted = 3,
        Document = 4,
    }
}
