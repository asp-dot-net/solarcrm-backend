﻿using System;
using System.Collections.Generic;
using System.Text;

namespace solarcrm.Common
{
    public enum LeadActionConsts
    {
        CreatedNewLead = 1,
        LeadModified = 2,
        LeadAssign = 3,
        LeadTransfer = 4,
        LeadStatusChanged = 5,
        SMSSent = 6,
        EmailSent = 7,
        ReminderSent = 8,
        Notify = 9,
        CreatedNewJob = 10,
        JobModified = 11,
        JobQuotation = 12
    }
}
