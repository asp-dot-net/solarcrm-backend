﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace solarcrm
{
    public class solarcrmCoreSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(solarcrmCoreSharedModule).GetAssembly());
        }
    }
}