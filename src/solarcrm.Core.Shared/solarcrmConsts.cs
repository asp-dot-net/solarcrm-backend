﻿namespace solarcrm
{
    public class solarcrmConsts
    {
        public const string LocalizationSourceName = "solarcrm";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const bool AllowTenantsToChangeEmailSettings = false; //true

        public const string Currency = "USD";

        public const string CurrencySign = "$";

        public const string AbpApiClientUserAgent = "AbpApiClient";

        public const string SMSBaseUrl = "https://insprl.com";

        // Note:
        // Minimum accepted payment amount. If a payment amount is less then that minimum value payment progress will continue without charging payment
        // Even though we can use multiple payment methods, users always can go and use the highest accepted payment amount.
        //For example, you use Stripe and PayPal. Let say that stripe accepts min 5$ and PayPal accepts min 3$. If your payment amount is 4$.
        // User will prefer to use a payment method with the highest accept value which is a Stripe in this case.
        public const decimal MinimumUpgradePaymentAmount = 1M;

       // public const string DocumentPath = @"D:\APS"; // For Local
        public const string DocumentPath = @"S:\documents.solarcrm.co.in"; // For Live

        //public const string ViewDocumentPath = @"D:\APS"; // For Local
        public const string ViewDocumentPath = "https://documents.solarcrm.co.in";


    }
}
