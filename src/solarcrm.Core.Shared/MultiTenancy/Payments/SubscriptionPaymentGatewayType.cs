﻿namespace solarcrm.MultiTenancy.Payments
{
    public enum SubscriptionPaymentGatewayType
    {
        Paypal = 1,
        Stripe = 2
    }
}
