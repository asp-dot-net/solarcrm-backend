﻿namespace solarcrm.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";
        }

        public static class Tenants
        {
            public const string Admin = "Admin";

            public const string User = "User";

            public const string Installer = "Installer";

            public const string ChannelPartner = "Channel Partner";

            public const string Manager = "Manager";

            public const string InstallationManager = "Installation Manager";

            public const string JobBooking = "Job Booking";

            public const string Invoice = "Invoice";
        }
    }
}