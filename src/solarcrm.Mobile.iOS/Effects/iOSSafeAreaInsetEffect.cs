﻿using solarcrm.Effects;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("solarcrm")]
[assembly: ExportEffect(typeof(iOSSafeAreaInsetEffect), nameof(iOSSafeAreaInsetEffect))]
namespace solarcrm.Effects
{
	public class iOSSafeAreaInsetEffect_iOS : PlatformEffect
	{
        protected override void OnAttached()
        {
            if (Element is Layout element)
            {
                //returns the appropriate safe area taking into consideration iOS versions.
                var safeArea = UIApplication.SharedApplication.KeyWindow.SafeAreaInsets;
                element.Margin = new Thickness(0, 20, 0, 0);
            }
        }

        protected override void OnDetached()
        {
            if (Element is Layout element)
            {
                element.Margin = new Thickness();
            }
        }
    }
}

