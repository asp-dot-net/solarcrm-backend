﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace solarcrm
{
    [DependsOn(typeof(solarcrmXamarinSharedModule))]
    public class solarcrmXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(solarcrmXamarinIosModule).GetAssembly());
        }
    }
}