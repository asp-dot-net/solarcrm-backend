﻿using System;
using System.Diagnostics;
using CoreAnimation;
using solarcrm.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BindablePicker), typeof(BindablePickerRenderer))]
namespace solarcrm.Controls
{
	public class BindablePickerRenderer : PickerRenderer
	{
		public BindablePickerRenderer()
		{
		}

        CALayer border = new CALayer();
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            var view = (BindablePicker)Element;

            if (view != null && Control != null)
            {
                SetPlaceholderColor(view);
                SetEnabled(view);
                Control.BorderStyle = UITextBorderStyle.None;
                SetTextAlignment(view);

                Control.TextColor = ((Color)App.Current.Resources["BlackColor"]).ToUIColor();

                view.Focused += (object sender, FocusEventArgs fe) =>
                {
                    if (Control != null)
                    {
                        Control.TextColor = view.TextColor.ToUIColor();
                        //Control.TextColor = ((Color)App.Current.Resources["SecondaryColor"]).ToUIColor();
                        //SetBorder(view, true);
                    }
                };

                view.Unfocused += (object sender, FocusEventArgs fe) =>
                {
                    if (Control != null)
                    {
                        Control.TintColor = ((Color)App.Current.Resources["GrayColor"]).ToUIColor();
                        Control.TextColor = ((Color)App.Current.Resources["BlackColor"]).ToUIColor();
                        //SetBorder(view, false);
                    }
                };
            }
        }

        [Foundation.Export("textField:shouldChangeCharactersInRange:replacementString:")]
        public bool ShouldChangeCharacters(UIKit.UITextField textField, Foundation.NSRange range, string replacementString)
        {
            return false;
        }

        void SetEnabled(BindablePicker view)
        {
            if (view != null)
            {
                if (view.IsCustomFocused)
                {
                    view.Focus();
                }
                else
                {
                    view.Unfocus();
                }

            }
        }

        void SetBorder(BindablePicker view, bool IsFocused)
        {
            try
            {
                if (IsFocused)
                {
                    float borderWidth = (float)view.BorderWidth;
                    border.BorderColor = UIColor.DarkGray.CGColor;
                    border.Frame = new CoreGraphics.CGRect(0, Control.Frame.Size.Height - borderWidth, Control.Frame.Size.Width, Control.Frame.Size.Height);
                    border.BorderWidth = borderWidth;
                    Control.Layer.AddSublayer(border);
                }
                else
                {
                    border.RemoveFromSuperLayer();
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        void SetPlaceholderColor(BindablePicker view)
        {
            Control.TintColor = ((Color)App.Current.Resources["GrayColor"]).ToUIColor();
        }

        void SetTextAlignment(BindablePicker view)
        {
            switch (view.XAlign)
            {
                case TextAlignment.Center:
                    Control.TextAlignment = UITextAlignment.Center;
                    break;
                case TextAlignment.End:
                    Control.TextAlignment = UITextAlignment.Right;
                    break;
                case TextAlignment.Start:
                    Control.TextAlignment = UITextAlignment.Left;
                    break;
            }
        }
    }
}

