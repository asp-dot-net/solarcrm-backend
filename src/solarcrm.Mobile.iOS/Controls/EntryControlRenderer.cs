﻿using System;
using solarcrm.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(EntryControl), typeof(EntryControlRenderer))]
namespace solarcrm.Controls
{
	public class EntryControlRenderer : EntryRenderer
	{
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            try
            {
                var view = (Entry)Element;
                if (view != null && Control != null)
                {
                    UIView paddingView = new UIView(new CoreGraphics.CGRect(0, 0, 10, 20));
                    Control.LeftView = paddingView;
                    Control.LeftViewMode = UITextFieldViewMode.Always;
                    Control.TintColor = view.TextColor.ToUIColor();
                    Control.Layer.BorderWidth = 0;
                    Control.BorderStyle = UITextBorderStyle.None;
                   
                    if (view.IsPassword)
                    {
                        Control.TextContentType = null;
                    }
                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);
            }
        }
    }
}

