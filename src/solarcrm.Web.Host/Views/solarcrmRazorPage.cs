﻿using Abp.AspNetCore.Mvc.Views;

namespace solarcrm.Web.Views
{
    public abstract class solarcrmRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected solarcrmRazorPage()
        {
            LocalizationSourceName = solarcrmConsts.LocalizationSourceName;
        }
    }
}
