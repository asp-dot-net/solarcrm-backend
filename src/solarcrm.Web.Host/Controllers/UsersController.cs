﻿using Abp.AspNetCore.Mvc.Authorization;
using solarcrm.Authorization;
using solarcrm.Storage;
using Abp.BackgroundJobs;

namespace solarcrm.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users)]
    public class UsersController : UsersControllerBase
    {
        public UsersController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}