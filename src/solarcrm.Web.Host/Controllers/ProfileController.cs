﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;
using solarcrm.Authorization.Users.Profile;
using solarcrm.Storage;

namespace solarcrm.Web.Controllers
{
    [AbpMvcAuthorize]
    public class ProfileController : ProfileControllerBase
    {
        public ProfileController(
            ITempFileCacheManager tempFileCacheManager,
            IProfileAppService profileAppService) :
            base(tempFileCacheManager, profileAppService)
        {
        }
    }
}