﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.BackgroundJobs;
using Microsoft.AspNetCore.Mvc;
using solarcrm.Authorization;
using solarcrm.Storage;

namespace solarcrm.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Leads_MISReport)]
    public class MISReportController : MISReportControllerBase
    {
        public MISReportController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
              : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}
