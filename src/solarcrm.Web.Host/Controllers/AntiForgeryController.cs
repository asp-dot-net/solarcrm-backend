﻿using Microsoft.AspNetCore.Antiforgery;

namespace solarcrm.Web.Controllers
{
    public class AntiForgeryController : solarcrmControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
