﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.BackgroundJobs;
using solarcrm.Authorization;
using solarcrm.Storage;

namespace solarcrm.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_DataVaults_StockItem)]
    public class StockItemController : StockItemControllerBase
    {
        public StockItemController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}
