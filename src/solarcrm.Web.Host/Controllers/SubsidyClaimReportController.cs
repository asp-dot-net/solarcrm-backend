﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.BackgroundJobs;
using Microsoft.AspNetCore.Mvc;
using solarcrm.Authorization;
using solarcrm.Storage;

namespace solarcrm.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail)]
    public class SubsidyClaimReportController : SubsidyClaimReportControllerBase
    {
        public SubsidyClaimReportController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
              : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}
