﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.BackgroundJobs;
using Microsoft.AspNetCore.Mvc;
using solarcrm.Authorization;
using solarcrm.Storage;

namespace solarcrm.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial)]
    public class BillOfMaterialController : BillOfMaterialControllerBase
    {
        public BillOfMaterialController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}
