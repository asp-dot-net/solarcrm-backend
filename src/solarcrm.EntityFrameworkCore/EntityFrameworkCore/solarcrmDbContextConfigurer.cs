using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace solarcrm.EntityFrameworkCore
{
    public static class solarcrmDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<solarcrmDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<solarcrmDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}