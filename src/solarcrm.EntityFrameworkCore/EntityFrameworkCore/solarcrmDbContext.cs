﻿using Abp.IdentityServer4vNext;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using solarcrm.Authorization.Delegation;
using solarcrm.Authorization.Roles;
using solarcrm.Authorization.Users;
using solarcrm.Chat;
using solarcrm.Editions;
using solarcrm.Friendships;
using solarcrm.MultiTenancy;
using solarcrm.MultiTenancy.Accounting;
using solarcrm.MultiTenancy.Payments;
using solarcrm.Storage;
using solarcrm.DataVaults.LeadSource;
using solarcrm.Sections;
using solarcrm.DataVaults.ActivityLog.DataVaultsAction;
using solarcrm.DataVaults.ActivityLog;
using solarcrm.DataVaults.LeadType;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.Teams;
using solarcrm.DataVaults.Locations;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.Division;
using solarcrm.DataVaults.SubDivision;
using solarcrm.DataVaults.TransactionType;
using solarcrm.DataVaults.StockItem;
using solarcrm.DataVaults.ProjectStatus;
using solarcrm.DataVaults.ProjectType;
using solarcrm.DataVaults.StockCategory;
using solarcrm.DataVaults.InvoiceType;
using solarcrm.DataVaults.State;
using solarcrm.DataVaults.District;
using solarcrm.DataVaults.Taluka;
using solarcrm.DataVaults.City;
using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.HoldReasons;
using solarcrm.DataVaults.RejectReasons;
using solarcrm.DataVaults;
using solarcrm.DataVaults.JobCancellationReason;
using solarcrm.DataVaults.DocumentLibrary;
using solarcrm.DataVaults.SmsTemplates;
using solarcrm.DataVaults.EmailTemplates;
using solarcrm.DataVaults.SolarType;
using solarcrm.DataVaults.HeightStructure;
using solarcrm.DataVaults.Leads;
using solarcrm.DataVaults.LeadAction;
using solarcrm.DataVaults.LeadStatus;
using solarcrm.DataVaults.LeadActivityLog;
using solarcrm.DataVaults.LeadActivity;
using solarcrm.DataVaults.LeadHistory;
using solarcrm.DataVaults.UserType;
using solarcrm.DataVaults.Tender;
using solarcrm.DataVaults.Circle;
using solarcrm.DataVaults.Price;
using solarcrm.DataVaults.CustomeTag;
using solarcrm.DataVaults.LeadDocuments;
using solarcrm.Organizations;

using solarcrm.DataVaults.Department;
using solarcrm.Job;
using solarcrm.MISReport;
using solarcrm.DataVaults.Variation;
using solarcrm.Job.JobVariation;
using solarcrm.MISReport.MISFileUploadList;
using solarcrm.MISReport.MISActivityLog;
using solarcrm.Job.Quotation;
using solarcrm.Quotation;
using solarcrm.DataVaults.PdfTemplates;
using solarcrm.DataVaults.Prices;
using solarcrm.Quotations;
using solarcrm.DataVaults.PaymentMode;
using solarcrm.DataVaults.PaymentType;
using solarcrm.Payments;
using solarcrm.EmailIntegrated;
using solarcrm.DataVaults.BillOfMaterial;
using solarcrm.JobInstallation;
using solarcrm.DataVaults.RefundReasons;
using solarcrm.Job.JobRefunds;
using solarcrm.Dispatch;
using solarcrm.DataVaults.DispatchType;
using solarcrm.PickList;
using solarcrm.DataVaults.Vehical;
using solarcrm.Transport;
using solarcrm.DataVaults.MaterialTransferReason;
using solarcrm.DocumentRequests;
using solarcrm.MeterConnect;
using solarcrm.Subsidy;

namespace solarcrm.EntityFrameworkCore
{
    public class solarcrmDbContext : AbpZeroDbContext<Tenant, Role, User, solarcrmDbContext>, IAbpPersistedGrantDbContext
    {
        /* Define an IDbSet for each entity of the application */
        /* Add Lead Source Entity */
        public virtual DbSet<LeadSource> LeadSources { get; set; }

        /* Add Section Entity for Activity Log */
        public virtual DbSet<Section> Sections { get; set; }

        /* Add DataVaults Actions Entity for Activity Log */
        public virtual DbSet<DataVaultsAction> DataVaultsActions { get; set; }

        /* Add DataVaults Activity Log Entity for Activity Log */
        public virtual DbSet<DataVaulteActivityLog> DataVaulteActivityLogs { get; set; }

        /* Add DataVaults Activity Log History Entity for Activity Log */
        public virtual DbSet<DataVaultsHistory> DataVaultsHistories { get; set; }

        /* Add Lead Type Entity */
        public virtual DbSet<LeadType> LeadTypes { get; set; }

        /* Add Document List Entity */
        public virtual DbSet<DocumentList> DocumentLists { get; set; }

        /* Add Teams Entity */
        public virtual DbSet<Teams> Teams { get; set; }

        /* Add Location Entity */
        public virtual DbSet<Locations> Location { get; set; }

        /* Add Discom Entity */
        public virtual DbSet<Discom> Discoms { get; set; }

        /* Add Division Entity */
        public virtual DbSet<Division> Divisions { get; set; }

        /* Add Sub Division Entity */
        public virtual DbSet<SubDivision> SubDivisions { get; set; }

        /* Add Transaction Type Entity */
        public virtual DbSet<TransactionType> TransactionTypes { get; set; }

        public virtual DbSet<StockItem> StockItems { get; set; }

        /* Add ProjectStatus Entity */
        public virtual DbSet<ProjectStatus> ProjectStatus { get; set; }

        /* Add ProjectType Entity */
        public virtual DbSet<ProjectType> ProjectType { get; set; }

        /* Add StockCategory Entity */
        public virtual DbSet<StockCategory> StockCategory { get; set; }

        /* Add InvoiceType Entity */
        public virtual DbSet<InvoiceType> InvoiceType { get; set; }

        /* Add State Entity */
        public virtual DbSet<State> States { get; set; }

        /* Add District Entity */
        public virtual DbSet<District> Districts { get; set; }

        /* Add Taluka Entity */
        public virtual DbSet<Taluka> Talukas { get; set; }

        /* Add City Entity */
        public virtual DbSet<City> Citys { get; set; }

        /* Add CancelReasons Entity */
        public virtual DbSet<CancelReasons> CancelReasonss { get; set; }

        /* Add HoldReasons Entity */
        public virtual DbSet<HoldReasons> HoldReasonss { get; set; }

        /* Add RejectReasons Entity */
        public virtual DbSet<RejectReasons> RejectReasonss { get; set; }

        /* Add JobCancellationReason Entity */
        public virtual DbSet<JobCancellationReason> JobCancellationReasons { get; set; }

        /* Add DocumentLibrary Entity */
        public virtual DbSet<DocumentLibrary> DocumentLibrarys { get; set; }

        /* Add SmsTemplates Entity */
        public virtual DbSet<SmsTemplates> smsTemplates { get; set; }

        /* Add EmailTemplates Entity */
        public virtual DbSet<EmailTemplates> emailTemplates { get; set; }

        /* Add StockItem Locations Entity */
        public virtual DbSet<StockItemLocation> StockItemLocations { get; set; }

        /* Add SolarType Entity */
        public virtual DbSet<SolarType> solarTypes { get; set; }

        /* Add HeightStructure Entity */
        public virtual DbSet<HeightStructure> HeightStructures { get; set; }

        /* Add PaymentMode Entity */
        public virtual DbSet<PaymentMode> paymentMode { get; set; }

        /* Add PaymentType Entity */
        public virtual DbSet<PaymentType> paymentType { get; set; }

        /* Add Customer Entity */
        public virtual DbSet<Leads> leads { get; set; }

        public virtual DbSet<ExternalLeads> externaLeads { get; set; }
        public virtual DbSet<LeadStatus> leadStatus { get; set; }

        public virtual DbSet<LeadActivityLogs> leadActivityLogs { get; set; }

        public virtual DbSet<CommentLeadActivityLog> commentLeadActivityLogs { get; set; }

        public virtual DbSet<EmailLeadActivityLog> emailLeadActivityLogs { get; set; }

        public virtual DbSet<NotifyLeadActivityLog> notifyLeadActivityLogs { get; set; }

        public virtual DbSet<ReminderLeadActivityLog> reminderLeadActivityLogs { get; set; }

        public virtual DbSet<SmsLeadActivityLog> smsLeadActivityLogs { get; set; }

        public virtual DbSet<ToDoLeadActivityLog> toDoLeadActivityLogs { get; set; }
        public virtual DbSet<LeadAction> leadActions { get; set; }

        public virtual DbSet<LeadActivity> LeadActivitys { get; set; }

        public virtual DbSet<LeadtrackerHistory> LeadtrackerHistorys { get; set; }

        public virtual DbSet<UserType> UserTypes { get; set; }

        public virtual DbSet<Tender> tenders { get; set; }

        public virtual DbSet<Circle> circles { get; set; }

        public virtual DbSet<Prices> prices { get; set; }

        public virtual DbSet<CancelRejectLeadReason> cancelrejectLeadReasons { get; set; }

        public virtual DbSet<CustomeTag> customeTags { get; set; }

        public virtual DbSet<LeadDocuments> leadDocuments { get; set; }

        public virtual DbSet<SMS_ServiceProviderOrganization> sms_ServiceProviderOrganization { get; set; }

        //Extend the Organization Table
        public virtual DbSet<ExtendOrganizationUnit> extendOrganizationUnits { get; set; }
        public virtual DbSet<ExtendUserOrganizationUnit> extendUserOrganizationUnits { get; set; }

        /* Add User Deyails Entity */
        public virtual DbSet<UserDetail> UserDetails { get; set; }

        /* Add User Deyails Entity */
        public virtual DbSet<Department> Departments { get; set; }

        /* Add User Deyails Entity */
        public virtual DbSet<Jobs> Jobs { get; set; }

        public virtual DbSet<JobProductItem> JobProductItems { get; set; }

        public virtual DbSet<Variation> Variations { get; set; }

        public virtual DbSet<JobVariation> JobVariations { get; set; }

        //public virtual DbSet<ActivityDropdown> ActivityDropdowns { get; set;}

        //public virtual DbSet<ActivityMapping> ActivityMappings { get; set; }

        /* -----------------------------------------------Below Contant Do not Delete---------------------------------------------------- */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        //public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }

        public virtual DbSet<UserDelegation> UserDelegations { get; set; }

        public virtual DbSet<RecentPassword> RecentPasswords { get; set; }

        public virtual DbSet<ApplicationStatus> applicationStatus { get; set; }

        public virtual DbSet<MISReport.MISReport> misReport { get; set; }

        public virtual DbSet<MISFileUploadLists> misFileUploadList { get; set; }

        public virtual DbSet<MISActivityLogs> misActivityLogs { get; set; }

        public virtual DbSet<MultipleFieldOrganizationUnitSelection> multipleFieldOrganizationUnitSelections { get; set; }

        public virtual DbSet<QuotationData> quotationDatas { get; set; }
        public virtual DbSet<QuotationDataDetails> quotationDataDetails { get; set; }
        public virtual DbSet<QuotationDataDetailProduct> quotationDataProducts { get; set; }
        public virtual DbSet<QuotationDataDetailVariations> quotationDataVariations { get; set; }
        public virtual DbSet<PdfTemplates> pdfTemplates { get; set; }
        public virtual DbSet<BankDetailsOrganization> bankDetailsOrganizations { get; set; }
        public virtual DbSet<PriceMultiAreaType> priceMultiAreaTypes { get; set; }
        public virtual DbSet<QuotationLinkHistory> quotationLinkHistory { get; set; }
        public virtual DbSet<PaymentReceiptUploadJob> paymentReceiptUploadJob { get; set; }
        public virtual DbSet<PaymentDetailsJob> paymentDetailsJob { get; set; }
        public virtual DbSet<STCPaymentDetails> stcPaymentDetails { get; set; }
        public virtual DbSet<SSPaymentDetails> ssPaymentDetails { get; set; }
        public virtual DbSet<BillOfMaterial> billOfMaterial { get; set; }
        public virtual DbSet<JobInstallationDetail> jobInstallationDetails { get; set; }
        public virtual DbSet<DiscomMeterInstallationDetail> discomMeterInstallationDetails { get; set; }
        public virtual DbSet<BidirectionalCertificateDetail> bidirectionalCertificateDetail { get; set; }
        public virtual DbSet<RefundReasons> refundReasons { get; set; }
        public virtual DbSet<JobRefunds> jobRefunds { get; set; }
        public virtual DbSet<DispatchType> dispatchType { get; set; }
        public virtual DbSet<DispatchMaterialDetails> dispatchMaterialDetails { get; set; }
        public virtual DbSet<DispatchExtraMaterials> dispatchExtraMaterials { get; set; }
        public virtual DbSet<DispatchStockMaterialTransfer> dispatchStockMaterialTransfer { get; set; }
        public virtual DbSet<PickListData> pickListData { get; set; }
        public virtual DbSet<Vehical> vehical { get; set; }
        public virtual DbSet<TransportDetails> transportDetails { get; set; }
        public virtual DbSet<MaterialTransferReason> materialTransferReason { get; set; }

        public virtual DbSet<DocumentRequest> DocumentRequests { get; set; }
        public virtual DbSet<DocumentRequestLinkHistory> DocumentRequestLinkHistorys { get; set; }

        public virtual DbSet<DispatchProductItem> dispatchProductItems { get; set; }
        public virtual DbSet<DispatchReturnMaterials> dispatchReturnMaterials { get; set; }
        public virtual DbSet<MeterConnectDetails> meterConnectDetails { get; set; }
        public virtual DbSet<SubsidyClaimFileUploadLists> subsidyClaimFileUploadLists { get; set; }
        public virtual DbSet<SubsidyClaimDetails> subsidyClaimDetails { get; set; }
        public virtual DbSet<SubsidyClaimProjectDetails> subsidyClaimProjectDetails { get; set; }
        public virtual DbSet<SubsidyActivityLogs> subsidyActivityLogs { get; set; }
        //public virtual DbSet<UserEmailProtocolConfigure> userEmailProtocolConfigure { get; set; }
        public solarcrmDbContext(DbContextOptions<solarcrmDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
            {
                b.HasQueryFilter(m => !m.IsDeleted)
                    .HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
                    .IsUnique();
            });

            modelBuilder.Entity<UserDelegation>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.SourceUserId });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
