﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class added_SMSProvidertbl_keyfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_sms_ServiceProviderOrganization_AbpOrganizationUnits_SMSProviderOrganizationId",
                table: "sms_ServiceProviderOrganization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_sms_ServiceProviderOrganization",
                table: "sms_ServiceProviderOrganization");

            migrationBuilder.RenameTable(
                name: "sms_ServiceProviderOrganization",
                newName: "SMS_ServiceProviderOrganization");

            migrationBuilder.RenameIndex(
                name: "IX_sms_ServiceProviderOrganization_SMSProviderOrganizationId",
                table: "SMS_ServiceProviderOrganization",
                newName: "IX_SMS_ServiceProviderOrganization_SMSProviderOrganizationId");

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "SMS_ServiceProviderOrganization",
                type: "int",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "SMS_ServiceProviderOrganization",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "SMS_ServiceProviderOrganization",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "SMS_ServiceProviderOrganization",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SMS_AuthorizationKey",
                table: "SMS_ServiceProviderOrganization",
                type: "nvarchar(55)",
                maxLength: 55,
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SMS_ServiceProviderOrganization",
                table: "SMS_ServiceProviderOrganization",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_SMS_ServiceProviderOrganization_AbpOrganizationUnits_SMSProviderOrganizationId",
                table: "SMS_ServiceProviderOrganization",
                column: "SMSProviderOrganizationId",
                principalTable: "AbpOrganizationUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SMS_ServiceProviderOrganization_AbpOrganizationUnits_SMSProviderOrganizationId",
                table: "SMS_ServiceProviderOrganization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SMS_ServiceProviderOrganization",
                table: "SMS_ServiceProviderOrganization");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "SMS_ServiceProviderOrganization");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "SMS_ServiceProviderOrganization");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "SMS_ServiceProviderOrganization");

            migrationBuilder.DropColumn(
                name: "SMS_AuthorizationKey",
                table: "SMS_ServiceProviderOrganization");

            migrationBuilder.RenameTable(
                name: "SMS_ServiceProviderOrganization",
                newName: "sms_ServiceProviderOrganization");

            migrationBuilder.RenameIndex(
                name: "IX_SMS_ServiceProviderOrganization_SMSProviderOrganizationId",
                table: "sms_ServiceProviderOrganization",
                newName: "IX_sms_ServiceProviderOrganization_SMSProviderOrganizationId");

            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "sms_ServiceProviderOrganization",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_sms_ServiceProviderOrganization",
                table: "sms_ServiceProviderOrganization",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_sms_ServiceProviderOrganization_AbpOrganizationUnits_SMSProviderOrganizationId",
                table: "sms_ServiceProviderOrganization",
                column: "SMSProviderOrganizationId",
                principalTable: "AbpOrganizationUnits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
