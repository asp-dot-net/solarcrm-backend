﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class change_multiplefieldselectionorganization_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.AlterColumn<int>(
                name: "StockItemOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LeadSourceOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RequiredCapacity",
                table: "Leads",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_StockItems_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "StockItemId",
                principalTable: "StockItems",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_StockItems_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.AlterColumn<long>(
                name: "StockItemOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "LeadSourceOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RequiredCapacity",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "StockItemId",
                principalTable: "LeadSources",
                principalColumn: "Id");
        }
    }
}
