﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_ReturnMaterial_Dispatchproduct_changes_dispatchMaterialField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtraComment",
                table: "DispatchExtraMaterials");

            migrationBuilder.DropColumn(
                name: "ISExtraMaterialApprove",
                table: "DispatchExtraMaterials");

            migrationBuilder.DropColumn(
                name: "MaterialApproveComment",
                table: "DispatchExtraMaterials");

            migrationBuilder.AddColumn<string>(
                name: "ExtraMaterialApprovedNotes",
                table: "DispatchMaterialDetails",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExtraMaterialNotes",
                table: "DispatchMaterialDetails",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsExtraMaterialApproved",
                table: "DispatchMaterialDetails",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<long>(
                name: "OrgBankAccountNumber",
                table: "BankDetailsOrganization",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "DispatchProductItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DispatchId = table.Column<int>(type: "int", nullable: true),
                    ProductItemId = table.Column<int>(type: "int", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DispatchProductItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DispatchProductItems_DispatchMaterialDetails_DispatchId",
                        column: x => x.DispatchId,
                        principalTable: "DispatchMaterialDetails",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DispatchProductItems_StockItems_ProductItemId",
                        column: x => x.ProductItemId,
                        principalTable: "StockItems",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "DispatchReturnMaterials",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DispatchId = table.Column<int>(type: "int", nullable: false),
                    DispatchStockItemId = table.Column<int>(type: "int", nullable: true),
                    ActualQuantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DispatchReturnMaterials", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DispatchReturnMaterials_DispatchMaterialDetails_DispatchId",
                        column: x => x.DispatchId,
                        principalTable: "DispatchMaterialDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DispatchReturnMaterials_StockItems_DispatchStockItemId",
                        column: x => x.DispatchStockItemId,
                        principalTable: "StockItems",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_DispatchProductItems_DispatchId",
                table: "DispatchProductItems",
                column: "DispatchId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchProductItems_ProductItemId",
                table: "DispatchProductItems",
                column: "ProductItemId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchReturnMaterials_DispatchId",
                table: "DispatchReturnMaterials",
                column: "DispatchId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchReturnMaterials_DispatchStockItemId",
                table: "DispatchReturnMaterials",
                column: "DispatchStockItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DispatchProductItems");

            migrationBuilder.DropTable(
                name: "DispatchReturnMaterials");

            migrationBuilder.DropColumn(
                name: "ExtraMaterialApprovedNotes",
                table: "DispatchMaterialDetails");

            migrationBuilder.DropColumn(
                name: "ExtraMaterialNotes",
                table: "DispatchMaterialDetails");

            migrationBuilder.DropColumn(
                name: "IsExtraMaterialApproved",
                table: "DispatchMaterialDetails");

            migrationBuilder.AddColumn<string>(
                name: "ExtraComment",
                table: "DispatchExtraMaterials",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ISExtraMaterialApprove",
                table: "DispatchExtraMaterials",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "MaterialApproveComment",
                table: "DispatchExtraMaterials",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrgBankAccountNumber",
                table: "BankDetailsOrganization",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);
        }
    }
}
