﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class update_Leads_Added_HideDublicate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HideDublicate",
                table: "Leads",
                type: "bit",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HideDublicate",
                table: "Leads");
        }
    }
}
