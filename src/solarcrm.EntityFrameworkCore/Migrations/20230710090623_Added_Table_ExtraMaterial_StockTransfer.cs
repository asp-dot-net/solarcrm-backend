﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Table_ExtraMaterial_StockTransfer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DispatchExtraMaterials",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DispatchId = table.Column<int>(type: "int", nullable: false),
                    DispatchStockItemId = table.Column<int>(type: "int", nullable: true),
                    Quantity = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ExtraComment = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    UserManagerId = table.Column<long>(type: "bigint", nullable: true),
                    ISExtraMaterialApprove = table.Column<bool>(type: "bit", nullable: false),
                    MaterilApproveById = table.Column<long>(type: "bigint", nullable: true),
                    MaterialApproveComment = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DispatchExtraMaterials", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DispatchExtraMaterials_AbpUsers_MaterilApproveById",
                        column: x => x.MaterilApproveById,
                        principalTable: "AbpUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DispatchExtraMaterials_AbpUsers_UserManagerId",
                        column: x => x.UserManagerId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DispatchExtraMaterials_DispatchMaterialDetails_DispatchId",
                        column: x => x.DispatchId,
                        principalTable: "DispatchMaterialDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DispatchExtraMaterials_StockItems_DispatchStockItemId",
                        column: x => x.DispatchStockItemId,
                        principalTable: "StockItems",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "DispatchStockMaterialTransfer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DispatchId = table.Column<int>(type: "int", nullable: false),
                    JobNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Reason = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DispatchStockMaterialTransfer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DispatchStockMaterialTransfer_DispatchMaterialDetails_DispatchId",
                        column: x => x.DispatchId,
                        principalTable: "DispatchMaterialDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DispatchExtraMaterials_DispatchId",
                table: "DispatchExtraMaterials",
                column: "DispatchId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchExtraMaterials_DispatchStockItemId",
                table: "DispatchExtraMaterials",
                column: "DispatchStockItemId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchExtraMaterials_MaterilApproveById",
                table: "DispatchExtraMaterials",
                column: "MaterilApproveById");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchExtraMaterials_UserManagerId",
                table: "DispatchExtraMaterials",
                column: "UserManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchStockMaterialTransfer_DispatchId",
                table: "DispatchStockMaterialTransfer",
                column: "DispatchId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DispatchExtraMaterials");

            migrationBuilder.DropTable(
                name: "DispatchStockMaterialTransfer");
        }
    }
}
