﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class addedLeadsActivitytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LeadAction",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActionName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadAction", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LeadActivity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActivityName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadActivity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LeadStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Leads",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    CustomerName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    MobileNumber = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    EmailId = table.Column<string>(type: "nvarchar(65)", maxLength: 65, nullable: true),
                    Alt_Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    AddressLine1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    AddressLine2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    StateId = table.Column<int>(type: "int", nullable: true),
                    DistrictId = table.Column<int>(type: "int", nullable: true),
                    TalukaId = table.Column<int>(type: "int", nullable: true),
                    CityId = table.Column<int>(type: "int", nullable: true),
                    Pincode = table.Column<int>(type: "int", nullable: true),
                    Latitude = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Longitude = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    LeadTypeId = table.Column<int>(type: "int", nullable: false),
                    LeadSourceId = table.Column<int>(type: "int", nullable: false),
                    LeadStatusId = table.Column<int>(type: "int", nullable: true),
                    SolarTypeId = table.Column<int>(type: "int", nullable: false),
                    CommonMeter = table.Column<bool>(type: "bit", nullable: false),
                    ConsumerNumber = table.Column<int>(type: "int", nullable: false),
                    AvgmonthlyUnit = table.Column<int>(type: "int", nullable: true),
                    AvgmonthlyBill = table.Column<int>(type: "int", nullable: true),
                    DiscomId = table.Column<int>(type: "int", nullable: true),
                    DivisionId = table.Column<int>(type: "int", nullable: true),
                    SubDivisionId = table.Column<int>(type: "int", nullable: true),
                    RequiredCapacity = table.Column<int>(type: "int", nullable: false),
                    StrctureAmmount = table.Column<int>(type: "int", nullable: false),
                    HeightStructureId = table.Column<int>(type: "int", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Category = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    ChanelPartnerID = table.Column<int>(type: "int", nullable: true),
                    BankAccountNumber = table.Column<int>(type: "int", nullable: true),
                    BankAccountHolderName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    BankName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IFSC_Code = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    IsDuplicate = table.Column<bool>(type: "bit", nullable: false),
                    IsWebDuplicate = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Leads", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Leads_City_CityId",
                        column: x => x.CityId,
                        principalTable: "City",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_Discoms_DiscomId",
                        column: x => x.DiscomId,
                        principalTable: "Discoms",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_District_DistrictId",
                        column: x => x.DistrictId,
                        principalTable: "District",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_Divisions_DivisionId",
                        column: x => x.DivisionId,
                        principalTable: "Divisions",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_HeightStructure_HeightStructureId",
                        column: x => x.HeightStructureId,
                        principalTable: "HeightStructure",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_LeadSources_LeadSourceId",
                        column: x => x.LeadSourceId,
                        principalTable: "LeadSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leads_LeadStatus_LeadStatusId",
                        column: x => x.LeadStatusId,
                        principalTable: "LeadStatus",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_LeadTypes_LeadTypeId",
                        column: x => x.LeadTypeId,
                        principalTable: "LeadTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leads_SolarType_SolarTypeId",
                        column: x => x.SolarTypeId,
                        principalTable: "SolarType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Leads_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_SubDivisions_SubDivisionId",
                        column: x => x.SubDivisionId,
                        principalTable: "SubDivisions",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Leads_Taluka_TalukaId",
                        column: x => x.TalukaId,
                        principalTable: "Taluka",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "LeadActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadId = table.Column<int>(type: "int", nullable: false),
                    LeadActionId = table.Column<int>(type: "int", nullable: true),
                    LeadAcitivityID = table.Column<int>(type: "int", nullable: true),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    LeadActionNote = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadActivityLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeadActivityLog_LeadAction_LeadActionId",
                        column: x => x.LeadActionId,
                        principalTable: "LeadAction",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_LeadActivityLog_LeadActivity_LeadAcitivityID",
                        column: x => x.LeadAcitivityID,
                        principalTable: "LeadActivity",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_LeadActivityLog_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LeadActivityLog_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "CommentLeadActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActivityLogId = table.Column<int>(type: "int", nullable: true),
                    CommentActivityNote = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentLeadActivityLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommentLeadActivityLog_LeadActivityLog_LeadActivityLogId",
                        column: x => x.LeadActivityLogId,
                        principalTable: "LeadActivityLog",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "EmailLeadActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActivityLogId = table.Column<int>(type: "int", nullable: true),
                    EmailTemplateId = table.Column<int>(type: "int", nullable: false),
                    Subject = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    EmailActivityBody = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailLeadActivityLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailLeadActivityLog_EmailTemplate_EmailTemplateId",
                        column: x => x.EmailTemplateId,
                        principalTable: "EmailTemplate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmailLeadActivityLog_LeadActivityLog_LeadActivityLogId",
                        column: x => x.LeadActivityLogId,
                        principalTable: "LeadActivityLog",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "NotifyLeadActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActivityLogId = table.Column<int>(type: "int", nullable: true),
                    NotifyActivityNote = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotifyLeadActivityLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NotifyLeadActivityLog_LeadActivityLog_LeadActivityLogId",
                        column: x => x.LeadActivityLogId,
                        principalTable: "LeadActivityLog",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ReminderLeadActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActivityLogId = table.Column<int>(type: "int", nullable: true),
                    ReminderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ReminderActivityNote = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReminderLeadActivityLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReminderLeadActivityLog_LeadActivityLog_LeadActivityLogId",
                        column: x => x.LeadActivityLogId,
                        principalTable: "LeadActivityLog",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SmsLeadActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActivityLogId = table.Column<int>(type: "int", nullable: true),
                    SmsTemplateId = table.Column<int>(type: "int", nullable: false),
                    SmsActivityBody = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SmsLeadActivityLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SmsLeadActivityLog_LeadActivityLog_LeadActivityLogId",
                        column: x => x.LeadActivityLogId,
                        principalTable: "LeadActivityLog",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SmsLeadActivityLog_SmsTemplates_SmsTemplateId",
                        column: x => x.SmsTemplateId,
                        principalTable: "SmsTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ToDoLeadActivityLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadActivityLogId = table.Column<int>(type: "int", nullable: true),
                    TodopriorityId = table.Column<int>(type: "int", nullable: true),
                    TodopriorityName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsTodoComplete = table.Column<bool>(type: "bit", nullable: true),
                    TodoResponse = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TodoTag = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TodoDueDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TodoresponseTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    TodoActivityNote = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ToDoLeadActivityLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ToDoLeadActivityLog_LeadActivityLog_LeadActivityLogId",
                        column: x => x.LeadActivityLogId,
                        principalTable: "LeadActivityLog",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommentLeadActivityLog_LeadActivityLogId",
                table: "CommentLeadActivityLog",
                column: "LeadActivityLogId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailLeadActivityLog_EmailTemplateId",
                table: "EmailLeadActivityLog",
                column: "EmailTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailLeadActivityLog_LeadActivityLogId",
                table: "EmailLeadActivityLog",
                column: "LeadActivityLogId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_LeadAcitivityID",
                table: "LeadActivityLog",
                column: "LeadAcitivityID");

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_LeadActionId",
                table: "LeadActivityLog",
                column: "LeadActionId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_LeadId",
                table: "LeadActivityLog",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadActivityLog_SectionId",
                table: "LeadActivityLog",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_CityId",
                table: "Leads",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_DiscomId",
                table: "Leads",
                column: "DiscomId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_DistrictId",
                table: "Leads",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_DivisionId",
                table: "Leads",
                column: "DivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_HeightStructureId",
                table: "Leads",
                column: "HeightStructureId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadSourceId",
                table: "Leads",
                column: "LeadSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadStatusId",
                table: "Leads",
                column: "LeadStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_LeadTypeId",
                table: "Leads",
                column: "LeadTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_SolarTypeId",
                table: "Leads",
                column: "SolarTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_StateId",
                table: "Leads",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_SubDivisionId",
                table: "Leads",
                column: "SubDivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_Leads_TalukaId",
                table: "Leads",
                column: "TalukaId");

            migrationBuilder.CreateIndex(
                name: "IX_NotifyLeadActivityLog_LeadActivityLogId",
                table: "NotifyLeadActivityLog",
                column: "LeadActivityLogId");

            migrationBuilder.CreateIndex(
                name: "IX_ReminderLeadActivityLog_LeadActivityLogId",
                table: "ReminderLeadActivityLog",
                column: "LeadActivityLogId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsLeadActivityLog_LeadActivityLogId",
                table: "SmsLeadActivityLog",
                column: "LeadActivityLogId");

            migrationBuilder.CreateIndex(
                name: "IX_SmsLeadActivityLog_SmsTemplateId",
                table: "SmsLeadActivityLog",
                column: "SmsTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_ToDoLeadActivityLog_LeadActivityLogId",
                table: "ToDoLeadActivityLog",
                column: "LeadActivityLogId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentLeadActivityLog");

            migrationBuilder.DropTable(
                name: "EmailLeadActivityLog");

            migrationBuilder.DropTable(
                name: "NotifyLeadActivityLog");

            migrationBuilder.DropTable(
                name: "ReminderLeadActivityLog");

            migrationBuilder.DropTable(
                name: "SmsLeadActivityLog");

            migrationBuilder.DropTable(
                name: "ToDoLeadActivityLog");

            migrationBuilder.DropTable(
                name: "LeadActivityLog");

            migrationBuilder.DropTable(
                name: "LeadAction");

            migrationBuilder.DropTable(
                name: "LeadActivity");

            migrationBuilder.DropTable(
                name: "Leads");

            migrationBuilder.DropTable(
                name: "LeadStatus");
        }
    }
}
