﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class misreport_field_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AvgMonthlyBill",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CommunicationAddress",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DepositDueDate",
                table: "MISReport",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepositPaymentPaidon",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepositQuotationAmount_inRS",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DepositQuotationNo",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmpanelmentNo",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EnergyConsumption",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExistingPVCapacityKW",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstallerCategory",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstallerEmail",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstallerMobile",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstallerName",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "IntimationRejectedDate",
                table: "MISReport",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectArea",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectAreaType",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Scheme",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeStamp",
                table: "MISReport",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WhetherthePremisesisownedorRented",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WhowillprovidetheNetMeter",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AvgMonthlyBill",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "CommunicationAddress",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "DepositDueDate",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "DepositPaymentPaidon",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "DepositQuotationAmount_inRS",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "DepositQuotationNo",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "EmpanelmentNo",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "EnergyConsumption",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "ExistingPVCapacityKW",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "InstallerCategory",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "InstallerEmail",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "InstallerMobile",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "InstallerName",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "IntimationRejectedDate",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "ProjectArea",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "ProjectAreaType",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "Scheme",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "TimeStamp",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "WhetherthePremisesisownedorRented",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "WhowillprovidetheNetMeter",
                table: "MISReport");
        }
    }
}
