﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Update_PaymentTable_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentMode_PaymentModeId",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentType_PaymentTypeId",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentTypeId",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropColumn(
                name: "PaymentAccountNotes",
                table: "PaymentDetailsJob");

            migrationBuilder.RenameColumn(
                name: "PaymentTypeId",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentRecVerifiedById");

            migrationBuilder.RenameColumn(
                name: "PaymentTotalPaid",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentRecTotalPaid");

            migrationBuilder.RenameColumn(
                name: "PaymentPayDate",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentRecPaidDate");

            migrationBuilder.RenameColumn(
                name: "PaymentModeId",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentRecTypeId");

            migrationBuilder.RenameColumn(
                name: "PaymentAddedById",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentRecModeId");

            migrationBuilder.RenameColumn(
                name: "IsPaymentVerified",
                table: "PaymentReceiptUploadJob",
                newName: "IsPaymentRecVerified");

            migrationBuilder.RenameIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentModeId",
                table: "PaymentReceiptUploadJob",
                newName: "IX_PaymentReceiptUploadJob_PaymentRecTypeId");

            migrationBuilder.AlterColumn<string>(
                name: "STCNotes",
                table: "STCPaymentDetails",
                type: "nvarchar(800)",
                maxLength: 800,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SSPaymentApprovedById",
                table: "SSPaymentDetails",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SSNotes",
                table: "SSPaymentDetails",
                type: "nvarchar(800)",
                maxLength: 800,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SSGSTNo",
                table: "SSPaymentDetails",
                type: "nvarchar(70)",
                maxLength: 70,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PaymentReceiptPath",
                table: "PaymentReceiptUploadJob",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PaymentReceiptName",
                table: "PaymentReceiptUploadJob",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentRecAddedById",
                table: "PaymentReceiptUploadJob",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentRecChequeStatus",
                table: "PaymentReceiptUploadJob",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PaymentRecEnterDate",
                table: "PaymentReceiptUploadJob",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentRecNotes",
                table: "PaymentReceiptUploadJob",
                type: "nvarchar(700)",
                maxLength: 700,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentRecRefNo",
                table: "PaymentReceiptUploadJob",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentAccountInvoiceNotes",
                table: "PaymentDetailsJob",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentRecModeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentRecModeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentMode_PaymentRecModeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentRecModeId",
                principalTable: "PaymentMode",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentType_PaymentRecTypeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentRecTypeId",
                principalTable: "PaymentType",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentMode_PaymentRecModeId",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentType_PaymentRecTypeId",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentRecModeId",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropColumn(
                name: "PaymentRecAddedById",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropColumn(
                name: "PaymentRecChequeStatus",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropColumn(
                name: "PaymentRecEnterDate",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropColumn(
                name: "PaymentRecNotes",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropColumn(
                name: "PaymentRecRefNo",
                table: "PaymentReceiptUploadJob");

            migrationBuilder.DropColumn(
                name: "PaymentAccountInvoiceNotes",
                table: "PaymentDetailsJob");

            migrationBuilder.RenameColumn(
                name: "PaymentRecVerifiedById",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentTypeId");

            migrationBuilder.RenameColumn(
                name: "PaymentRecTypeId",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentModeId");

            migrationBuilder.RenameColumn(
                name: "PaymentRecTotalPaid",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentTotalPaid");

            migrationBuilder.RenameColumn(
                name: "PaymentRecPaidDate",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentPayDate");

            migrationBuilder.RenameColumn(
                name: "PaymentRecModeId",
                table: "PaymentReceiptUploadJob",
                newName: "PaymentAddedById");

            migrationBuilder.RenameColumn(
                name: "IsPaymentRecVerified",
                table: "PaymentReceiptUploadJob",
                newName: "IsPaymentVerified");

            migrationBuilder.RenameIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentRecTypeId",
                table: "PaymentReceiptUploadJob",
                newName: "IX_PaymentReceiptUploadJob_PaymentModeId");

            migrationBuilder.AlterColumn<string>(
                name: "STCNotes",
                table: "STCPaymentDetails",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(800)",
                oldMaxLength: 800,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SSPaymentApprovedById",
                table: "SSPaymentDetails",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "SSNotes",
                table: "SSPaymentDetails",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(800)",
                oldMaxLength: 800,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SSGSTNo",
                table: "SSPaymentDetails",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(70)",
                oldMaxLength: 70,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PaymentReceiptPath",
                table: "PaymentReceiptUploadJob",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(200)",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PaymentReceiptName",
                table: "PaymentReceiptUploadJob",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentAccountNotes",
                table: "PaymentDetailsJob",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentTypeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentMode_PaymentModeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentModeId",
                principalTable: "PaymentMode",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentReceiptUploadJob_PaymentType_PaymentTypeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentTypeId",
                principalTable: "PaymentType",
                principalColumn: "Id");
        }
    }
}
