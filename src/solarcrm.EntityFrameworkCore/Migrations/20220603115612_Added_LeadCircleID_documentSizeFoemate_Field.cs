﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_LeadCircleID_documentSizeFoemate_Field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CircleId",
                table: "Leads",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumenFormate",
                table: "LeadDocuments",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumenSize",
                table: "LeadDocuments",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Leads_CircleId",
                table: "Leads",
                column: "CircleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Circle_CircleId",
                table: "Leads",
                column: "CircleId",
                principalTable: "Circle",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Circle_CircleId",
                table: "Leads");

            migrationBuilder.DropIndex(
                name: "IX_Leads_CircleId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "CircleId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "DocumenFormate",
                table: "LeadDocuments");

            migrationBuilder.DropColumn(
                name: "DocumenSize",
                table: "LeadDocuments");
        }
    }
}
