﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Table_Added_Quotation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_LeadSourceId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_StockItems_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_LeadSourceId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropColumn(
                name: "LeadSourceId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropColumn(
                name: "StockItemId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.CreateTable(
                name: "QuotationData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuoteJobId = table.Column<int>(type: "int", nullable: false),
                    QuotationNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    QuotationGenerateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsQuotateSigned = table.Column<bool>(type: "bit", nullable: false),
                    QuotationSignedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    QuotationNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationData_Jobs_QuoteJobId",
                        column: x => x.QuoteJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuotationDataDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuoteDataId = table.Column<int>(type: "int", nullable: false),
                    CustomerName = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Address1 = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    Address2 = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: true),
                    Mobile = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    SolarAdvisorName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SolarAdvisorMobile = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    SolarAdvisorEmail = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true),
                    PvCapacityKw = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActualSystemCost = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Subcidy40 = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Subcidy20 = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    BillToPay = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    DiscomName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    HeightOfStructure = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DivisionName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationDataDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationDataDetail_QuotationData_QuoteDataId",
                        column: x => x.QuoteDataId,
                        principalTable: "QuotationData",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuotationDataDetailProduct",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuotationDetailId = table.Column<int>(type: "int", nullable: false),
                    QuoteProductType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    QuoteProductName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    QuoteProductModelNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    QuoteProductQuantity = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationDataDetailProduct", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationDataDetailProduct_QuotationDataDetail_QuotationDetailId",
                        column: x => x.QuotationDetailId,
                        principalTable: "QuotationDataDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuotationDataDetailVariations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuotationDetailId = table.Column<int>(type: "int", nullable: false),
                    QuoteVarationName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    QuoteVarationCost = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationDataDetailVariations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationDataDetailVariations_QuotationDataDetail_QuotationDetailId",
                        column: x => x.QuotationDetailId,
                        principalTable: "QuotationDataDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_LeadSourceOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "LeadSourceOrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_StockItemOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "StockItemOrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationData_QuoteJobId",
                table: "QuotationData",
                column: "QuoteJobId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationDataDetail_QuoteDataId",
                table: "QuotationDataDetail",
                column: "QuoteDataId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationDataDetailProduct_QuotationDetailId",
                table: "QuotationDataDetailProduct",
                column: "QuotationDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationDataDetailVariations_QuotationDetailId",
                table: "QuotationDataDetailVariations",
                column: "QuotationDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_LeadSourceOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "LeadSourceOrganizationId",
                principalTable: "LeadSources",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_StockItems_StockItemOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "StockItemOrganizationId",
                principalTable: "StockItems",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_LeadSourceOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_StockItems_StockItemOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropTable(
                name: "QuotationDataDetailProduct");

            migrationBuilder.DropTable(
                name: "QuotationDataDetailVariations");

            migrationBuilder.DropTable(
                name: "QuotationDataDetail");

            migrationBuilder.DropTable(
                name: "QuotationData");

            migrationBuilder.DropIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_LeadSourceOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.DropIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_StockItemOrganizationId",
                table: "MultipleFieldOrganizationUnitSelection");

            migrationBuilder.AddColumn<int>(
                name: "LeadSourceId",
                table: "MultipleFieldOrganizationUnitSelection",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StockItemId",
                table: "MultipleFieldOrganizationUnitSelection",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_LeadSourceId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "LeadSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "StockItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_LeadSourceId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "LeadSourceId",
                principalTable: "LeadSources",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MultipleFieldOrganizationUnitSelection_StockItems_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "StockItemId",
                principalTable: "StockItems",
                principalColumn: "Id");
        }
    }
}
