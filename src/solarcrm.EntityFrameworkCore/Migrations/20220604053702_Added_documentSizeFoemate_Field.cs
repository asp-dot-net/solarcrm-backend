﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_documentSizeFoemate_Field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumenFormate",
                table: "LeadDocuments");

            migrationBuilder.DropColumn(
                name: "DocumenSize",
                table: "LeadDocuments");

            migrationBuilder.AddColumn<string>(
                name: "DocumenFormate",
                table: "DocumentLists",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumenSize",
                table: "DocumentLists",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumenFormate",
                table: "DocumentLists");

            migrationBuilder.DropColumn(
                name: "DocumenSize",
                table: "DocumentLists");

            migrationBuilder.AddColumn<string>(
                name: "DocumenFormate",
                table: "LeadDocuments",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumenSize",
                table: "LeadDocuments",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }
    }
}
