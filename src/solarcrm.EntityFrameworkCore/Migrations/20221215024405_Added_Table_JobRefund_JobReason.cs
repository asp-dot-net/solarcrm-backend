﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Table_JobRefund_JobReason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RefundReasons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefundReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobRefunds",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RefundJobId = table.Column<int>(type: "int", nullable: false),
                    RefundReasonId = table.Column<int>(type: "int", nullable: false),
                    PaymentTypeId = table.Column<int>(type: "int", nullable: false),
                    RefundPaidAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    RefundPaidDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RefundPaidNotes = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    IsRefundVerify = table.Column<bool>(type: "bit", nullable: false),
                    RefundVerifyDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RefundVerifyNotes = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    RefundVerifyById = table.Column<int>(type: "int", nullable: true),
                    BankAccountNumber = table.Column<long>(type: "bigint", nullable: true),
                    BankAccountHolderName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    BankBrachName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    BankName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IFSC_Code = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    RefundRecPath = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    ReceiptNo = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    JobRefundEmailSend = table.Column<bool>(type: "bit", nullable: true),
                    JobRefundEmailSendDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    JobRefundSmsSend = table.Column<bool>(type: "bit", nullable: false),
                    JobRefundSmsSendDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobRefunds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobRefunds_Jobs_RefundJobId",
                        column: x => x.RefundJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobRefunds_PaymentType_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobRefunds_RefundReasons_RefundReasonId",
                        column: x => x.RefundReasonId,
                        principalTable: "RefundReasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobRefunds_PaymentTypeId",
                table: "JobRefunds",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_JobRefunds_RefundJobId",
                table: "JobRefunds",
                column: "RefundJobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobRefunds_RefundReasonId",
                table: "JobRefunds",
                column: "RefundReasonId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobRefunds");

            migrationBuilder.DropTable(
                name: "RefundReasons");
        }
    }
}
