﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Update_Division_Circle_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Divisions_Discoms_DiscomId",
                table: "Divisions");

            migrationBuilder.RenameColumn(
                name: "DiscomId",
                table: "Divisions",
                newName: "CircleId");

            migrationBuilder.RenameIndex(
                name: "IX_Divisions_DiscomId",
                table: "Divisions",
                newName: "IX_Divisions_CircleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Divisions_Circle_CircleId",
                table: "Divisions",
                column: "CircleId",
                principalTable: "Circle",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Divisions_Circle_CircleId",
                table: "Divisions");

            migrationBuilder.RenameColumn(
                name: "CircleId",
                table: "Divisions",
                newName: "DiscomId");

            migrationBuilder.RenameIndex(
                name: "IX_Divisions_CircleId",
                table: "Divisions",
                newName: "IX_Divisions_DiscomId");

            migrationBuilder.AddForeignKey(
                name: "FK_Divisions_Discoms_DiscomId",
                table: "Divisions",
                column: "DiscomId",
                principalTable: "Discoms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
