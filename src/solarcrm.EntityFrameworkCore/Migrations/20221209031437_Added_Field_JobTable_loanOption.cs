﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Field_JobTable_loanOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "JobLoanOption",
                table: "Jobs",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JobLoanOption",
                table: "Jobs");
        }
    }
}
