﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_SubsidyClaim_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SubsidyClaimDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubsidyDivisionId = table.Column<int>(type: "int", nullable: true),
                    SubsidyClaimNo = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SubsidyNote = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    SubcidyClaimDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsSubcidyFileVerify = table.Column<bool>(type: "bit", nullable: false),
                    IsSubcidyReceived = table.Column<bool>(type: "bit", nullable: false),
                    OCCopyPath = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    ClaimFileSubmissionDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SubcidyReceiptsPath = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidyClaimDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubsidyClaimDetails_Divisions_SubsidyDivisionId",
                        column: x => x.SubsidyDivisionId,
                        principalTable: "Divisions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SubsidyClaimFileUploadLists",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubsidyFileName = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    SubsidyFilePath = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    SubsidyFileStatus = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    SubsidyTenantId = table.Column<int>(type: "int", nullable: false),
                    SubsidyOrganizationUnitId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidyClaimFileUploadLists", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubsidyClaimProjectDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubsidyJobId = table.Column<int>(type: "int", nullable: false),
                    SubsidyClaimId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidyClaimProjectDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubsidyClaimProjectDetails_Jobs_SubsidyJobId",
                        column: x => x.SubsidyJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubsidyClaimProjectDetails_SubsidyClaimDetails_SubsidyClaimId",
                        column: x => x.SubsidyClaimId,
                        principalTable: "SubsidyClaimDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubsidyActivityLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubsidyId = table.Column<int>(type: "int", nullable: false),
                    SubsidyProjectName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    SubsidyLeadActionId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    SubsidyActionNote = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidyActivityLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubsidyActivityLogs_LeadAction_SubsidyLeadActionId",
                        column: x => x.SubsidyLeadActionId,
                        principalTable: "LeadAction",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubsidyActivityLogs_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SubsidyActivityLogs_SubsidyClaimFileUploadLists_SubsidyId",
                        column: x => x.SubsidyId,
                        principalTable: "SubsidyClaimFileUploadLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubsidyActivityLogs_SectionId",
                table: "SubsidyActivityLogs",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubsidyActivityLogs_SubsidyId",
                table: "SubsidyActivityLogs",
                column: "SubsidyId");

            migrationBuilder.CreateIndex(
                name: "IX_SubsidyActivityLogs_SubsidyLeadActionId",
                table: "SubsidyActivityLogs",
                column: "SubsidyLeadActionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubsidyClaimDetails_SubsidyDivisionId",
                table: "SubsidyClaimDetails",
                column: "SubsidyDivisionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubsidyClaimProjectDetails_SubsidyClaimId",
                table: "SubsidyClaimProjectDetails",
                column: "SubsidyClaimId");

            migrationBuilder.CreateIndex(
                name: "IX_SubsidyClaimProjectDetails_SubsidyJobId",
                table: "SubsidyClaimProjectDetails",
                column: "SubsidyJobId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubsidyActivityLogs");

            migrationBuilder.DropTable(
                name: "SubsidyClaimProjectDetails");

            migrationBuilder.DropTable(
                name: "SubsidyClaimFileUploadLists");

            migrationBuilder.DropTable(
                name: "SubsidyClaimDetails");
        }
    }
}
