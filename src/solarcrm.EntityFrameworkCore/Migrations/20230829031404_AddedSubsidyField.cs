﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class AddedSubsidyField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "JobSubsidy20",
                table: "Jobs",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "JobSubsidy40",
                table: "Jobs",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "JobTotalSubsidyAmount",
                table: "Jobs",
                type: "decimal(18,2)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JobSubsidy20",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobSubsidy40",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "JobTotalSubsidyAmount",
                table: "Jobs");
        }
    }
}
