﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Change_Field_StoreLocation_Installer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobInstallationDetail_StockItemLocations_StockItemLocationId",
                table: "JobInstallationDetail");

            migrationBuilder.RenameColumn(
                name: "StockItemLocationId",
                table: "JobInstallationDetail",
                newName: "StoreLocationId");

            migrationBuilder.RenameIndex(
                name: "IX_JobInstallationDetail_StockItemLocationId",
                table: "JobInstallationDetail",
                newName: "IX_JobInstallationDetail_StoreLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobInstallationDetail_Locations_StoreLocationId",
                table: "JobInstallationDetail",
                column: "StoreLocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobInstallationDetail_Locations_StoreLocationId",
                table: "JobInstallationDetail");

            migrationBuilder.RenameColumn(
                name: "StoreLocationId",
                table: "JobInstallationDetail",
                newName: "StockItemLocationId");

            migrationBuilder.RenameIndex(
                name: "IX_JobInstallationDetail_StoreLocationId",
                table: "JobInstallationDetail",
                newName: "IX_JobInstallationDetail_StockItemLocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_JobInstallationDetail_StockItemLocations_StockItemLocationId",
                table: "JobInstallationDetail",
                column: "StockItemLocationId",
                principalTable: "StockItemLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
