﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Update_Jobs_Table_JobStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Circle_Circle",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Discoms_Discom",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_Divisions_Division",
                table: "Jobs");

            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_SubDivisions_SubDivision",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_Circle",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_Discom",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_Division",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_SubDivision",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "Circle",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "Discom",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "Division",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SanctionedLoadKW",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SubDivision",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "SubDivsion",
                table: "Jobs",
                newName: "JobStatus");

            migrationBuilder.RenameColumn(
                name: "StrctureAmmount",
                table: "Jobs",
                newName: "NetMeterCharges");

            migrationBuilder.AlterColumn<string>(
                name: "PhaseOfInverter",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_JobStatus",
                table: "Jobs",
                column: "JobStatus");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ProjectStatus_JobStatus",
                table: "Jobs",
                column: "JobStatus",
                principalTable: "ProjectStatus",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ProjectStatus_JobStatus",
                table: "Jobs");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_JobStatus",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "NetMeterCharges",
                table: "Jobs",
                newName: "StrctureAmmount");

            migrationBuilder.RenameColumn(
                name: "JobStatus",
                table: "Jobs",
                newName: "SubDivsion");

            migrationBuilder.AlterColumn<int>(
                name: "PhaseOfInverter",
                table: "Jobs",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Circle",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Discom",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Division",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SanctionedLoadKW",
                table: "Jobs",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "SubDivision",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_Circle",
                table: "Jobs",
                column: "Circle");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_Discom",
                table: "Jobs",
                column: "Discom");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_Division",
                table: "Jobs",
                column: "Division");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_SubDivision",
                table: "Jobs",
                column: "SubDivision");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Circle_Circle",
                table: "Jobs",
                column: "Circle",
                principalTable: "Circle",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Discoms_Discom",
                table: "Jobs",
                column: "Discom",
                principalTable: "Discoms",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_Divisions_Division",
                table: "Jobs",
                column: "Division",
                principalTable: "Divisions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_SubDivisions_SubDivision",
                table: "Jobs",
                column: "SubDivision",
                principalTable: "SubDivisions",
                principalColumn: "Id");
        }
    }
}
