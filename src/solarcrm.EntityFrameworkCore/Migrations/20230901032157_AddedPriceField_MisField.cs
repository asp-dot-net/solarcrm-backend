﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class AddedPriceField_MisField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Rate",
                table: "Prices",
                newName: "StateRate");

            migrationBuilder.AddColumn<decimal>(
                name: "CentralRate",
                table: "Prices",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CEIInspectionApplicationApprovalDate",
                table: "MISReport",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CEIInspectionApplicationID",
                table: "MISReport",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "DontWantSubsidy",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ElectricityBill",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FieldReportStatusReceivedfromDisComon",
                table: "MISReport",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GovermentAgency",
                table: "MISReport",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "PVCapacityBand",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PassportSizePhoto",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SanctionID",
                table: "MISReport",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "SolarPVsystemOwnedbytheConsumer",
                table: "MISReport",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "WorkEndDate",
                table: "MISReport",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "WorkOrderDate",
                table: "MISReport",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "WorkOrderNo",
                table: "MISReport",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "WorkStartDate",
                table: "MISReport",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CentralRate",
                table: "Prices");

            migrationBuilder.DropColumn(
                name: "CEIInspectionApplicationApprovalDate",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "CEIInspectionApplicationID",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "DontWantSubsidy",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "ElectricityBill",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "FieldReportStatusReceivedfromDisComon",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "GovermentAgency",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "PVCapacityBand",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "PassportSizePhoto",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "SanctionID",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "SolarPVsystemOwnedbytheConsumer",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "WorkEndDate",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "WorkOrderDate",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "WorkOrderNo",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "WorkStartDate",
                table: "MISReport");

            migrationBuilder.RenameColumn(
                name: "StateRate",
                table: "Prices",
                newName: "Rate");
        }
    }
}
