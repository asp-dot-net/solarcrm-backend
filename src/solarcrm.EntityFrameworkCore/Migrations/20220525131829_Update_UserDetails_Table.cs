﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Update_UserDetails_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AbpUsers_UserDetails_UserDetailId",
                table: "AbpUsers");

            migrationBuilder.DropIndex(
                name: "IX_AbpUsers_UserDetailId",
                table: "AbpUsers");

            migrationBuilder.DropColumn(
                name: "UserDetailId",
                table: "AbpUsers");

            migrationBuilder.RenameColumn(
                name: "TenantId",
                table: "UserDetails",
                newName: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "UserDetails",
                newName: "TenantId");

            migrationBuilder.AddColumn<int>(
                name: "UserDetailId",
                table: "AbpUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AbpUsers_UserDetailId",
                table: "AbpUsers",
                column: "UserDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_AbpUsers_UserDetails_UserDetailId",
                table: "AbpUsers",
                column: "UserDetailId",
                principalTable: "UserDetails",
                principalColumn: "Id");
        }
    }
}
