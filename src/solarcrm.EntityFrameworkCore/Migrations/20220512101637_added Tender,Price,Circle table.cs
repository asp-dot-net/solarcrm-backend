﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class addedTenderPriceCircletable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailLeadActivityLog_EmailTemplate_EmailTemplateId",
                table: "EmailLeadActivityLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_LeadStatus_LeadStatusId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_SmsLeadActivityLog_SmsTemplates_SmsTemplateId",
                table: "SmsLeadActivityLog");

            migrationBuilder.AlterColumn<int>(
                name: "SmsTemplateId",
                table: "SmsLeadActivityLog",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ReminderDate",
                table: "ReminderLeadActivityLog",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "OrganizationUnitId",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LeadStatusId",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EmailTemplateId",
                table: "EmailLeadActivityLog",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "LeadtrackerHistory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    FieldName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PrevValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CurValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Action = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LeadId = table.Column<int>(type: "int", nullable: false),
                    LeadActionId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadtrackerHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tender",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    SolarTypeId = table.Column<int>(type: "int", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tender", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tender_SolarType_SolarTypeId",
                        column: x => x.SolarTypeId,
                        principalTable: "SolarType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Prices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenderId = table.Column<int>(type: "int", nullable: false),
                    NoofPanels = table.Column<int>(type: "int", nullable: true),
                    Rate = table.Column<long>(type: "bigint", nullable: true),
                    SystemPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ActualPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Subcidy40 = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    Subcidy20 = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    MeterType = table.Column<int>(type: "int", nullable: true),
                    SizeRange = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Prices_Tender_TenderId",
                        column: x => x.TenderId,
                        principalTable: "Tender",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Prices_TenderId",
                table: "Prices",
                column: "TenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Tender_SolarTypeId",
                table: "Tender",
                column: "SolarTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmailLeadActivityLog_EmailTemplate_EmailTemplateId",
                table: "EmailLeadActivityLog",
                column: "EmailTemplateId",
                principalTable: "EmailTemplate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_LeadStatus_LeadStatusId",
                table: "Leads",
                column: "LeadStatusId",
                principalTable: "LeadStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SmsLeadActivityLog_SmsTemplates_SmsTemplateId",
                table: "SmsLeadActivityLog",
                column: "SmsTemplateId",
                principalTable: "SmsTemplates",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmailLeadActivityLog_EmailTemplate_EmailTemplateId",
                table: "EmailLeadActivityLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_LeadStatus_LeadStatusId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_SmsLeadActivityLog_SmsTemplates_SmsTemplateId",
                table: "SmsLeadActivityLog");

            migrationBuilder.DropTable(
                name: "LeadtrackerHistory");

            migrationBuilder.DropTable(
                name: "Prices");

            migrationBuilder.DropTable(
                name: "Tender");

            migrationBuilder.AlterColumn<int>(
                name: "SmsTemplateId",
                table: "SmsLeadActivityLog",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ReminderDate",
                table: "ReminderLeadActivityLog",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrganizationUnitId",
                table: "Leads",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "LeadStatusId",
                table: "Leads",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "EmailTemplateId",
                table: "EmailLeadActivityLog",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_EmailLeadActivityLog_EmailTemplate_EmailTemplateId",
                table: "EmailLeadActivityLog",
                column: "EmailTemplateId",
                principalTable: "EmailTemplate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_LeadStatus_LeadStatusId",
                table: "Leads",
                column: "LeadStatusId",
                principalTable: "LeadStatus",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_SmsLeadActivityLog_SmsTemplates_SmsTemplateId",
                table: "SmsLeadActivityLog",
                column: "SmsTemplateId",
                principalTable: "SmsTemplates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
