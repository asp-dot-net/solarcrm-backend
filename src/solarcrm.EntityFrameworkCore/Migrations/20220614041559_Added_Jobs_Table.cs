﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Jobs_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Jobs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    LeadId = table.Column<int>(type: "int", nullable: false),
                    Lead = table.Column<int>(type: "int", nullable: true),
                    JobNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    JobNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    InstallerNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NotesForInstallationDepartment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ActualSystemCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    JobSubsidy40 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    JobSubsidy20 = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    JobTotalSubsidyAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    NetSystemCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DepositeAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    StrctureAmmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    AgreementCharges = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    GedaCharges = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalJobCost = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    SubDivsion = table.Column<int>(type: "int", nullable: true),
                    SubDivision = table.Column<int>(type: "int", nullable: true),
                    Division = table.Column<int>(type: "int", nullable: true),
                    Circle = table.Column<int>(type: "int", nullable: true),
                    Discom = table.Column<int>(type: "int", nullable: true),
                    SanctionedLoadKW = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PhaseOfInverter = table.Column<int>(type: "int", nullable: false),
                    PvCapacityKw = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ApplicationNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SignApplicationPath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    OtpVerifed = table.Column<string>(type: "nvarchar(max)", nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jobs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Jobs_Circle_Circle",
                        column: x => x.Circle,
                        principalTable: "Circle",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Jobs_Discoms_Discom",
                        column: x => x.Discom,
                        principalTable: "Discoms",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Jobs_Divisions_Division",
                        column: x => x.Division,
                        principalTable: "Divisions",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Jobs_Leads_Lead",
                        column: x => x.Lead,
                        principalTable: "Leads",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Jobs_SubDivisions_SubDivision",
                        column: x => x.SubDivision,
                        principalTable: "SubDivisions",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_Circle",
                table: "Jobs",
                column: "Circle");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_Discom",
                table: "Jobs",
                column: "Discom");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_Division",
                table: "Jobs",
                column: "Division");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_Lead",
                table: "Jobs",
                column: "Lead");

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_SubDivision",
                table: "Jobs",
                column: "SubDivision");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Jobs");
            //migrationBuilder.RenameColumn(
            //    name: "Subcidy20",
            //    table: "Jobs",
            //    newName: "JobSubsidy20");
            //migrationBuilder.RenameColumn(
            //    name: "Subcidy40",
            //    table: "Jobs",
            //    newName: "JobSubsidy40");
            //migrationBuilder.RenameColumn(
            //    name: "TotalSubcidyAmount",
            //    table: "Jobs",
            //    newName: "JobTotalSubsidyAmount");
        }
    }
}
