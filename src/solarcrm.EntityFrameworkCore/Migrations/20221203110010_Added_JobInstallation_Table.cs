﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_JobInstallation_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobInstallationDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JobId = table.Column<int>(type: "int", nullable: false),
                    InstallStartDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    StockItemLocationId = table.Column<int>(type: "int", nullable: false),
                    JobInstallerID = table.Column<long>(type: "bigint", nullable: false),
                    InstallCompleteDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobInstallationDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobInstallationDetail_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JobInstallationDetail_StockItemLocations_StockItemLocationId",
                        column: x => x.StockItemLocationId,
                        principalTable: "StockItemLocations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiscomMeterInstallationDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JobInstallationId = table.Column<int>(type: "int", nullable: false),
                    DiscomCompleteDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DiscomMeterNo = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SolarMeterNo = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    DiscomBidirectionMeterInstallImgPath = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscomMeterInstallationDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DiscomMeterInstallationDetail_JobInstallationDetail_JobInstallationId",
                        column: x => x.JobInstallationId,
                        principalTable: "JobInstallationDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BiDirectionalCertificateDetail",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DiscomMeterInstallationId = table.Column<int>(type: "int", nullable: false),
                    BidirectionalCompleteDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CertificateBidirectionMeterInstallImgPath = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BiDirectionalCertificateDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BiDirectionalCertificateDetail_DiscomMeterInstallationDetail_DiscomMeterInstallationId",
                        column: x => x.DiscomMeterInstallationId,
                        principalTable: "DiscomMeterInstallationDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BiDirectionalCertificateDetail_DiscomMeterInstallationId",
                table: "BiDirectionalCertificateDetail",
                column: "DiscomMeterInstallationId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscomMeterInstallationDetail_JobInstallationId",
                table: "DiscomMeterInstallationDetail",
                column: "JobInstallationId");

            migrationBuilder.CreateIndex(
                name: "IX_JobInstallationDetail_JobId",
                table: "JobInstallationDetail",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_JobInstallationDetail_StockItemLocationId",
                table: "JobInstallationDetail",
                column: "StockItemLocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BiDirectionalCertificateDetail");

            migrationBuilder.DropTable(
                name: "DiscomMeterInstallationDetail");

            migrationBuilder.DropTable(
                name: "JobInstallationDetail");
        }
    }
}
