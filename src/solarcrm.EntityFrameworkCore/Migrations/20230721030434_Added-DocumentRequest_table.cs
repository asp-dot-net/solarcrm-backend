﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class AddedDocumentRequest_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsMark",
                table: "LeadActivityLog",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MessageId",
                table: "LeadActivityLog",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsAccountSid",
                table: "AbpOrganizationUnits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsPhoneNumber",
                table: "AbpOrganizationUnits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsPropertySid",
                table: "AbpOrganizationUnits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FoneDynamicsToken",
                table: "AbpOrganizationUnits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DocumentRequestLinkHistorys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DocumentRequestId = table.Column<int>(type: "int", nullable: true),
                    Expired = table.Column<bool>(type: "bit", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentRequestLinkHistorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentRequests",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadId = table.Column<int>(type: "int", nullable: false),
                    DocTypeId = table.Column<int>(type: "int", nullable: false),
                    IsSubmitted = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentRequests_DocumentLists_DocTypeId",
                        column: x => x.DocTypeId,
                        principalTable: "DocumentLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentRequests_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentRequests_DocTypeId",
                table: "DocumentRequests",
                column: "DocTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentRequests_LeadId",
                table: "DocumentRequests",
                column: "LeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentRequestLinkHistorys");

            migrationBuilder.DropTable(
                name: "DocumentRequests");

            migrationBuilder.DropColumn(
                name: "IsMark",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "MessageId",
                table: "LeadActivityLog");

            migrationBuilder.DropColumn(
                name: "FoneDynamicsAccountSid",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "FoneDynamicsPhoneNumber",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "FoneDynamicsPropertySid",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "FoneDynamicsToken",
                table: "AbpOrganizationUnits");
        }
    }
}
