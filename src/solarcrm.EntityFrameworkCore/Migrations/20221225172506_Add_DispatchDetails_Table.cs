﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Add_DispatchDetails_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DepositNotes",
                table: "Jobs",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DepositRecivedDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FirstDepositRecivedDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DispatchType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DispatchType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DispatchMaterialDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DispatchJobId = table.Column<int>(type: "int", nullable: false),
                    DispatchStockItemId = table.Column<int>(type: "int", nullable: true),
                    DispatchHeightStructureId = table.Column<int>(type: "int", nullable: true),
                    DispatchTypeId = table.Column<int>(type: "int", nullable: true),
                    DispatchDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DispatchNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DispatchReturnDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DispatchReturnNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DispatchMaterialDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DispatchMaterialDetails_DispatchType_DispatchTypeId",
                        column: x => x.DispatchTypeId,
                        principalTable: "DispatchType",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DispatchMaterialDetails_HeightStructure_DispatchHeightStructureId",
                        column: x => x.DispatchHeightStructureId,
                        principalTable: "HeightStructure",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DispatchMaterialDetails_Jobs_DispatchJobId",
                        column: x => x.DispatchJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DispatchMaterialDetails_StockItems_DispatchStockItemId",
                        column: x => x.DispatchStockItemId,
                        principalTable: "StockItems",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_DispatchMaterialDetails_DispatchHeightStructureId",
                table: "DispatchMaterialDetails",
                column: "DispatchHeightStructureId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchMaterialDetails_DispatchJobId",
                table: "DispatchMaterialDetails",
                column: "DispatchJobId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchMaterialDetails_DispatchStockItemId",
                table: "DispatchMaterialDetails",
                column: "DispatchStockItemId");

            migrationBuilder.CreateIndex(
                name: "IX_DispatchMaterialDetails_DispatchTypeId",
                table: "DispatchMaterialDetails",
                column: "DispatchTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DispatchMaterialDetails");

            migrationBuilder.DropTable(
                name: "DispatchType");

            migrationBuilder.DropColumn(
                name: "DepositNotes",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DepositRecivedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "FirstDepositRecivedDate",
                table: "Jobs");
        }
    }
}
