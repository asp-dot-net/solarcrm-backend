﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_SolarType_Heightstructure_Customers_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HeightStructure",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HeightStructure", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SolarType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SolarType", x => x.Id);
                });

        //    migrationBuilder.CreateTable(
        //        name: "Customers",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(type: "int", nullable: false)
        //                .Annotation("SqlServer:Identity", "1, 1"),
        //            FirstName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
        //            MiddleName = table.Column<string>(type: "nvarchar(max)", nullable: false),
        //            LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
        //            CustomerName = table.Column<string>(type: "nvarchar(max)", nullable: false),
        //            MobileNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
        //            EmailId = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            Alt_Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            AddressLine1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            AddressLine2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            StateId = table.Column<int>(type: "int", nullable: true),
        //            DistrictId = table.Column<int>(type: "int", nullable: true),
        //            TalukaId = table.Column<int>(type: "int", nullable: true),
        //            CityId = table.Column<int>(type: "int", nullable: true),
        //            Pincode = table.Column<int>(type: "int", nullable: true),
        //            Latitude = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
        //            Longitude = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
        //            LeadTypeId = table.Column<int>(type: "int", nullable: false),
        //            LeadSourceId = table.Column<int>(type: "int", nullable: false),
        //            SolarTypeId = table.Column<int>(type: "int", nullable: false),
        //            CommonMeter = table.Column<bool>(type: "bit", nullable: false),
        //            ConsumerNumber = table.Column<int>(type: "int", nullable: false),
        //            AvgmonthlyUnit = table.Column<int>(type: "int", nullable: true),
        //            AvgmonthlyBill = table.Column<int>(type: "int", nullable: true),
        //            DiscomId = table.Column<int>(type: "int", nullable: false),
        //            DivisionId = table.Column<int>(type: "int", nullable: true),
        //            SubDivisionId = table.Column<int>(type: "int", nullable: true),
        //            KiloWattSystem = table.Column<int>(type: "int", nullable: false),
        //            HeightStructureId = table.Column<int>(type: "int", nullable: true),
        //            Category = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            ChanelPartnerID = table.Column<int>(type: "int", nullable: true),
        //            BankAccountNumber = table.Column<int>(type: "int", nullable: true),
        //            BankAccountHolderName = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            BankName = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            IFSC_Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
        //            IsActive = table.Column<bool>(type: "bit", nullable: false),
        //            TenantId = table.Column<int>(type: "int", nullable: true),
        //            CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
        //            CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
        //            LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
        //            LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
        //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
        //            DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
        //            DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Customers", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_Customers_City_CityId",
        //                column: x => x.CityId,
        //                principalTable: "City",
        //                principalColumn: "Id");
        //            table.ForeignKey(
        //                name: "FK_Customers_Discoms_DiscomId",
        //                column: x => x.DiscomId,
        //                principalTable: "Discoms",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Customers_District_DistrictId",
        //                column: x => x.DistrictId,
        //                principalTable: "District",
        //                principalColumn: "Id");
        //            table.ForeignKey(
        //                name: "FK_Customers_Divisions_DivisionId",
        //                column: x => x.DivisionId,
        //                principalTable: "Divisions",
        //                principalColumn: "Id");
        //            table.ForeignKey(
        //                name: "FK_Customers_HeightStructure_HeightStructureId",
        //                column: x => x.HeightStructureId,
        //                principalTable: "HeightStructure",
        //                principalColumn: "Id");
        //            table.ForeignKey(
        //                name: "FK_Customers_LeadSources_LeadSourceId",
        //                column: x => x.LeadSourceId,
        //                principalTable: "LeadSources",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Customers_LeadTypes_LeadTypeId",
        //                column: x => x.LeadTypeId,
        //                principalTable: "LeadTypes",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Customers_SolarType_SolarTypeId",
        //                column: x => x.SolarTypeId,
        //                principalTable: "SolarType",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Customers_State_StateId",
        //                column: x => x.StateId,
        //                principalTable: "State",
        //                principalColumn: "Id");
        //            table.ForeignKey(
        //                name: "FK_Customers_SubDivisions_SubDivisionId",
        //                column: x => x.SubDivisionId,
        //                principalTable: "SubDivisions",
        //                principalColumn: "Id");
        //            table.ForeignKey(
        //                name: "FK_Customers_Taluka_TalukaId",
        //                column: x => x.TalukaId,
        //                principalTable: "Taluka",
        //                principalColumn: "Id");
        //        });

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_CityId",
        //        table: "Customers",
        //        column: "CityId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_DiscomId",
        //        table: "Customers",
        //        column: "DiscomId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_DistrictId",
        //        table: "Customers",
        //        column: "DistrictId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_DivisionId",
        //        table: "Customers",
        //        column: "DivisionId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_HeightStructureId",
        //        table: "Customers",
        //        column: "HeightStructureId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_LeadSourceId",
        //        table: "Customers",
        //        column: "LeadSourceId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_LeadTypeId",
        //        table: "Customers",
        //        column: "LeadTypeId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_SolarTypeId",
        //        table: "Customers",
        //        column: "SolarTypeId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_StateId",
        //        table: "Customers",
        //        column: "StateId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_SubDivisionId",
        //        table: "Customers",
        //        column: "SubDivisionId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Customers_TalukaId",
        //        table: "Customers",
        //        column: "TalukaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "Customers");

            migrationBuilder.DropTable(
                name: "HeightStructure");

            migrationBuilder.DropTable(
                name: "SolarType");
        }
    }
}
