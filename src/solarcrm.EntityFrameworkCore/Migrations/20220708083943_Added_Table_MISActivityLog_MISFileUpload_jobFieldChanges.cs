﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Table_MISActivityLog_MISFileUpload_jobFieldChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isUpdated",
                table: "MISReport",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ApplicationStatusId",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DocumentVerifiedDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastComment",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastCommentDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastCommentRepliedDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PenaltyDaysPending",
                table: "Jobs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SanctionedContractLoad",
                table: "Jobs",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MISActivityLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MISId = table.Column<int>(type: "int", nullable: false),
                    ProjectName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LeadActionId = table.Column<int>(type: "int", nullable: false),
                    SectionId = table.Column<int>(type: "int", nullable: true),
                    MISActionNote = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MISActivityLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MISActivityLogs_LeadAction_LeadActionId",
                        column: x => x.LeadActionId,
                        principalTable: "LeadAction",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MISActivityLogs_MISReport_MISId",
                        column: x => x.MISId,
                        principalTable: "MISReport",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MISActivityLogs_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MISFileUploadList",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FileName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FilePath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileStatus = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MISFileUploadList", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jobs_ApplicationStatusId",
                table: "Jobs",
                column: "ApplicationStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_MISActivityLogs_LeadActionId",
                table: "MISActivityLogs",
                column: "LeadActionId");

            migrationBuilder.CreateIndex(
                name: "IX_MISActivityLogs_MISId",
                table: "MISActivityLogs",
                column: "MISId");

            migrationBuilder.CreateIndex(
                name: "IX_MISActivityLogs_SectionId",
                table: "MISActivityLogs",
                column: "SectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jobs_ApplicationStatus_ApplicationStatusId",
                table: "Jobs",
                column: "ApplicationStatusId",
                principalTable: "ApplicationStatus",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jobs_ApplicationStatus_ApplicationStatusId",
                table: "Jobs");

            migrationBuilder.DropTable(
                name: "MISActivityLogs");

            migrationBuilder.DropTable(
                name: "MISFileUploadList");

            migrationBuilder.DropIndex(
                name: "IX_Jobs_ApplicationStatusId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "isUpdated",
                table: "MISReport");

            migrationBuilder.DropColumn(
                name: "ApplicationStatusId",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "DocumentVerifiedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LastComment",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LastCommentDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LastCommentRepliedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "PenaltyDaysPending",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "SanctionedContractLoad",
                table: "Jobs");
        }
    }
}
