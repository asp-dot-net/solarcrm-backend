﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Payment_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsManualPricing",
                table: "SolarType",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "PaymentDetailsJob",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PaymentJobId = table.Column<int>(type: "int", nullable: false),
                    LessSubsidy = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    PaidTillDate = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    NetAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    BalanceOwing = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    PaymentDeliveryOption = table.Column<bool>(type: "bit", nullable: true),
                    PaymentAccountNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentDetailsJob", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentDetailsJob_Jobs_PaymentJobId",
                        column: x => x.PaymentJobId,
                        principalTable: "Jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMode",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMode", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(32)", maxLength: 32, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SSPaymentDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SSPaymentId = table.Column<int>(type: "int", nullable: false),
                    SSDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SSGSTNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SSPaymentApprovedById = table.Column<int>(type: "int", nullable: true),
                    SSNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SSPaymentDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SSPaymentDetails_PaymentDetailsJob_SSPaymentId",
                        column: x => x.SSPaymentId,
                        principalTable: "PaymentDetailsJob",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "STCPaymentDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    STCPaymentId = table.Column<int>(type: "int", nullable: false),
                    STCDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    STCPaymentApprovedById = table.Column<int>(type: "int", nullable: true),
                    STCNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_STCPaymentDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_STCPaymentDetails_PaymentDetailsJob_STCPaymentId",
                        column: x => x.STCPaymentId,
                        principalTable: "PaymentDetailsJob",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentReceiptUploadJob",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PaymentId = table.Column<int>(type: "int", nullable: false),
                    PaymentPayDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PaymentTotalPaid = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    PaymentTypeId = table.Column<int>(type: "int", nullable: true),
                    PaymentModeId = table.Column<int>(type: "int", nullable: true),
                    PaymentReceiptName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PaymentReceiptPath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PaymentAddedById = table.Column<int>(type: "int", nullable: true),
                    IsPaymentVerified = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentReceiptUploadJob", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentReceiptUploadJob_PaymentDetailsJob_PaymentId",
                        column: x => x.PaymentId,
                        principalTable: "PaymentDetailsJob",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentReceiptUploadJob_PaymentMode_PaymentModeId",
                        column: x => x.PaymentModeId,
                        principalTable: "PaymentMode",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_PaymentReceiptUploadJob_PaymentType_PaymentTypeId",
                        column: x => x.PaymentTypeId,
                        principalTable: "PaymentType",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaymentDetailsJob_PaymentJobId",
                table: "PaymentDetailsJob",
                column: "PaymentJobId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentModeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentModeId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentReceiptUploadJob_PaymentTypeId",
                table: "PaymentReceiptUploadJob",
                column: "PaymentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_SSPaymentDetails_SSPaymentId",
                table: "SSPaymentDetails",
                column: "SSPaymentId");

            migrationBuilder.CreateIndex(
                name: "IX_STCPaymentDetails_STCPaymentId",
                table: "STCPaymentDetails",
                column: "STCPaymentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentReceiptUploadJob");

            migrationBuilder.DropTable(
                name: "SSPaymentDetails");

            migrationBuilder.DropTable(
                name: "STCPaymentDetails");

            migrationBuilder.DropTable(
                name: "PaymentMode");

            migrationBuilder.DropTable(
                name: "PaymentType");

            migrationBuilder.DropTable(
                name: "PaymentDetailsJob");

            migrationBuilder.DropColumn(
                name: "IsManualPricing",
                table: "SolarType");
        }
    }
}
