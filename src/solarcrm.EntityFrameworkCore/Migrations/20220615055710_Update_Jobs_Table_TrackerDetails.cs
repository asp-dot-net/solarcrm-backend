﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Update_Jobs_Table_TrackerDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ApplicationDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EastimateDueDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EastimatePaidStatus",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EastimatePayDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "EastimateQuoteAmount",
                table: "Jobs",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "EastimateQuoteNumber",
                table: "Jobs",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "EstimatePaymentReceiptPath",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GUVNLRegisterationNo",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EastimateDueDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EastimatePaidStatus",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EastimatePayDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EastimateQuoteAmount",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EastimateQuoteNumber",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "EstimatePaymentReceiptPath",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "GUVNLRegisterationNo",
                table: "Jobs");
        }
    }
}
