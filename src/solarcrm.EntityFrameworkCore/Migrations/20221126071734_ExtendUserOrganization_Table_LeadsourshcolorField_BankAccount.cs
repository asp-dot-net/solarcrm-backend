﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class ExtendUserOrganization_Table_LeadsourshcolorField_BankAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LeadSourceColorClass",
                table: "LeadSources",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "BankAccountNumber",
                table: "Leads",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AbpUserOrganizationUnits",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UserWiseOrganization_Email",
                table: "AbpUserOrganizationUnits",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LeadSourceColorClass",
                table: "LeadSources");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AbpUserOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "UserWiseOrganization_Email",
                table: "AbpUserOrganizationUnits");

            migrationBuilder.AlterColumn<int>(
                name: "BankAccountNumber",
                table: "Leads",
                type: "int",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);
        }
    }
}
