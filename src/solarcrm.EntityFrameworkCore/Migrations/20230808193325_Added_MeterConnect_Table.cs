﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_MeterConnect_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "meterConnectDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InstallationId = table.Column<int>(type: "int", nullable: false),
                    MeterAgreementGivenTo = table.Column<string>(type: "nvarchar(80)", maxLength: 80, nullable: true),
                    MeterAgreementNo = table.Column<string>(type: "nvarchar(80)", maxLength: 80, nullable: true),
                    MeterAgreementSentDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    MeterApplicationReadyStatus = table.Column<int>(type: "int", nullable: true),
                    MeterSubmissionDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_meterConnectDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_meterConnectDetails_JobInstallationDetail_InstallationId",
                        column: x => x.InstallationId,
                        principalTable: "JobInstallationDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_meterConnectDetails_InstallationId",
                table: "meterConnectDetails",
                column: "InstallationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "meterConnectDetails");
        }
    }
}
