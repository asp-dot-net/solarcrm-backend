﻿using System.Linq;
using Abp;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Abp.Notifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using solarcrm.Authorization;
using solarcrm.Authorization.Roles;
using solarcrm.Authorization.Users;
using solarcrm.EntityFrameworkCore;
using solarcrm.Notifications;

namespace solarcrm.Migrations.Seed.Tenants
{
    public class TenantRoleAndUserBuilder
    {
        private readonly solarcrmDbContext _context;
        private readonly int _tenantId;

        public TenantRoleAndUserBuilder(solarcrmDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            //Admin role

            var adminRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRole == null)
            {
                adminRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.Admin) { IsStatic = true }).Entity;
                _context.SaveChanges();
            }

            //User role

            var userRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.User);
            if (userRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.User, StaticRoleNames.Tenants.User) { IsStatic = true, IsDefault = true });
                _context.SaveChanges();
            }

            //Channel Partner Role
            var channelPartnerRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ChannelPartner);
            if (channelPartnerRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ChannelPartner, StaticRoleNames.Tenants.ChannelPartner) { IsStatic = true });
                _context.SaveChanges();
            }
            
            //Installer Role
            var installerRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Installer);
            if (installerRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Installer, StaticRoleNames.Tenants.Installer) { IsStatic = true });
                _context.SaveChanges();
            }

            //Manager Role
            var managerRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Manager);
            if (managerRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Manager, StaticRoleNames.Tenants.Manager) { IsStatic = true });
                _context.SaveChanges();
            }

            //Installation Manager Role
            var installationManagerRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.InstallationManager);
            if (installationManagerRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.InstallationManager, StaticRoleNames.Tenants.InstallationManager) { IsStatic = true });
                _context.SaveChanges();
            }

            //Job Booking Role
            var jobBookingRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.JobBooking);
            if (jobBookingRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.JobBooking, StaticRoleNames.Tenants.JobBooking) { IsStatic = true });
                _context.SaveChanges();
            }
            
            //Invoice Role
            var invoiceRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Invoice);
            if (invoiceRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Invoice, StaticRoleNames.Tenants.Invoice) { IsStatic = true });
                _context.SaveChanges();
            }

            //admin user
            var adminUser = _context.Users.IgnoreQueryFilters().FirstOrDefault(u => u.TenantId == _tenantId && u.UserName == AbpUserBase.AdminUserName);
            if (adminUser == null)
            {
                adminUser = User.CreateTenantAdminUser(_tenantId, "admin@defaulttenant.com");
                adminUser.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>(new PasswordHasherOptions())).HashPassword(adminUser, "123qwe");
                adminUser.IsEmailConfirmed = true;
                adminUser.ShouldChangePasswordOnNextLogin = false;
                adminUser.IsActive = true;

                _context.Users.Add(adminUser);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));
                _context.SaveChanges();

                //User account of admin user
                if (_tenantId == 1)
                {
                    _context.UserAccounts.Add(new UserAccount
                    {
                        TenantId = _tenantId,
                        UserId = adminUser.Id,
                        UserName = AbpUserBase.AdminUserName,
                        EmailAddress = adminUser.EmailAddress
                    });
                    _context.SaveChanges();
                }

                //Notification subscription
                _context.NotificationSubscriptions.Add(new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(), _tenantId, adminUser.Id, AppNotificationNames.NewUserRegistered));
                _context.SaveChanges();
            }
        }
    }
}
