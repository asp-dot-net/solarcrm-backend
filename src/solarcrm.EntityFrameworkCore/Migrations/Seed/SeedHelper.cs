﻿using System;
using System.Transactions;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using solarcrm.EntityFrameworkCore;
using solarcrm.Migrations.Seed.Host;
using solarcrm.Migrations.Seed.Tenants;
using solarcrm.Migrations.Seed.DefaultCommonValues;

namespace solarcrm.Migrations.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<solarcrmDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(solarcrmDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            //Host seed
            new InitialHostDbBuilder(context).Create();

            //Default tenant seed (in host database).
            new DefaultTenantBuilder(context).Create();
            new TenantRoleAndUserBuilder(context, 1).Create();

            //Default Common Values
            new DefaultCommonValuesBuilder(context).Create();
            new DefaultStockCategoryCreator(context).Create();

            new DefaultLeadStatusCreator(context).Create();

            new DefaultLeadsActivityCreator(context).Create();
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using (var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
            {
                using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                {
                    var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

                    contextAction(context);

                    uow.Complete();
                }
            }
        }
    }
}
