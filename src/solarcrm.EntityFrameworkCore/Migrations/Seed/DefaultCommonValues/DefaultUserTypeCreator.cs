﻿using solarcrm.DataVaults.UserType;
using solarcrm.EntityFrameworkCore;
using System.Linq;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultUserTypeCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultUserTypeCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserType();
        }

        public void CreateUserType()
        {
            var employee = _context.UserTypes.FirstOrDefault(x => x.Name == "Employee");
            if (employee == null)
            {
                _context.UserTypes.Add(
                    new UserType
                    {
                        Name = "Employee",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var channelPartner = _context.UserTypes.FirstOrDefault(x => x.Name == "Channel Partner");
            if (channelPartner == null)
            {
                _context.UserTypes.Add(
                    new UserType
                    {
                        Name = "Channel Partner",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            
            var installer = _context.UserTypes.FirstOrDefault(x => x.Name == "Installer");
            if (installer == null)
            {
                _context.UserTypes.Add(
                    new UserType
                    {
                        Name = "Installer",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

        }
    }
}
