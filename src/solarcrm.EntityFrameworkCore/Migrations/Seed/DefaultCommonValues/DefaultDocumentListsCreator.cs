﻿using Abp.Configuration;
using Castle.Core.Internal;
using Castle.MicroKernel.Registration;
using Nito.Collections;
using PayPalCheckoutSdk.Orders;
using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.DocumentList;
using solarcrm.EntityFrameworkCore;
using Stripe;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Twilio.TwiML.Fax;
using Twilio.TwiML.Voice;
using static IdentityServer4.Models.IdentityResources;
using static solarcrm.Configuration.AppSettings.UserManagement;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultDocumentListsCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultDocumentListsCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDocumentLists();
        }
        public void CreateDocumentLists()
        {
            var PanCard = _context.DocumentLists.FirstOrDefault(x => x.Name == "Pan Card");
            if (PanCard == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Pan Card",
                        DocumenFormate = "PDF",
                        DocumenSize = "200",
                        DocumentShortName = "PC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var AadharCard = _context.DocumentLists.FirstOrDefault(x => x.Name == "Aadhar Card");
            if (AadharCard == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Aadhar Card",
                        DocumenFormate = "PDF",
                        DocumenSize= "1",
                        DocumentShortName = "AC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var PassportPhoto = _context.DocumentLists.FirstOrDefault(x => x.Name == "Passport Photo");
            if (PassportPhoto == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Passport Photo",
                        DocumenFormate = "JPG",
                        DocumenSize= "200",
                        DocumentShortName = "PP",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var ElectricityBill = _context.DocumentLists.FirstOrDefault(x => x.Name == "Electricity Bill");
            if (ElectricityBill == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Electricity Bill",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "EB",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var TaxBill = _context.DocumentLists.FirstOrDefault(x => x.Name == "Tax Bill");
            if (TaxBill == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Tax Bill",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "TB",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SerialNo = _context.DocumentLists.FirstOrDefault(x => x.Name == "Serial No");
            if (SerialNo == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Serial No",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SN",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var DiscomLetter = _context.DocumentLists.FirstOrDefault(x => x.Name == "Discom Letter");
            if (DiscomLetter == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Discom Letter",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "DL",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var MOU = _context.DocumentLists.FirstOrDefault(x => x.Name == "MOU");
            if (MOU == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "MOU",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "MU",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var TaxInvoice = _context.DocumentLists.FirstOrDefault(x => x.Name == "Tax Invoice");
            if (TaxInvoice == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Tax Invoice",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "TI",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var PaymentReceipt = _context.DocumentLists.FirstOrDefault(x => x.Name == "Payment Receipt");
            if (PaymentReceipt == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Payment Receipt",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "PR",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SelfCertificate = _context.DocumentLists.FirstOrDefault(x => x.Name == "Self Certificate");
            if (SelfCertificate == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Self Certificate",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var GEDAApplication = _context.DocumentLists.FirstOrDefault(x => x.Name == "GEDA Application");
            if (GEDAApplication == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "GEDA Application",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "GEDA",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SubsidySummarySheet = _context.DocumentLists.FirstOrDefault(x => x.Name == "Subsidy Summary Sheet");
            if (SubsidySummarySheet == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Subsidy Summary Sheet",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SSS",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var WarrantyRegistrationForm = _context.DocumentLists.FirstOrDefault(x => x.Name == "Warranty Registration Form");
            if (WarrantyRegistrationForm == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Warranty Registration Form",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "WRF",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InverterWarrantyForm = _context.DocumentLists.FirstOrDefault(x => x.Name == "Inverter Warranty Form");
            if (InverterWarrantyForm == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Inverter Warranty Form",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "IWF",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var MeterSealingReport = _context.DocumentLists.FirstOrDefault(x => x.Name == "Meter Sealing Report");
            if (MeterSealingReport == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Meter Sealing Report",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "MSR",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var MeterInstallation = _context.DocumentLists.FirstOrDefault(x => x.Name == "Meter Installation");
            if (MeterInstallation == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Meter Installation",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "MI",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var CustomerApplication = _context.DocumentLists.FirstOrDefault(x => x.Name == "Customer Application");
            if (CustomerApplication == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Customer Application",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "CA",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var FeasibilityReport = _context.DocumentLists.FirstOrDefault(x => x.Name == "Feasibility Report");
            if (FeasibilityReport == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Feasibility Report",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "FR",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var ShadowReport = _context.DocumentLists.FirstOrDefault(x => x.Name == "Shadow Report");
            if (ShadowReport == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Shadow Report",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SR",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var StructureReport = _context.DocumentLists.FirstOrDefault(x => x.Name == "Structure Report");
            if (StructureReport == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Structure Report",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SR",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SignedCustomerApplication = _context.DocumentLists.FirstOrDefault(x => x.Name == "Signed Customer Application");
            if (SignedCustomerApplication == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Signed Customer Application",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SCA",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SignQuotation = _context.DocumentLists.FirstOrDefault(x => x.Name == "Sign Quotation");
            if (SignQuotation == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Sign Quotation",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SQ",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var ReduceCapacityApplication = _context.DocumentLists.FirstOrDefault(x => x.Name == "Reduce Capacity Application");
            if (ReduceCapacityApplication == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Reduce Capacity Application",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "RCA",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var PreInspectionLetter = _context.DocumentLists.FirstOrDefault(x => x.Name == "Pre-Inspection Letter");
            if (PreInspectionLetter == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Pre-Inspection Letter",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "PIL",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var MeterReferencePhoto = _context.DocumentLists.FirstOrDefault(x => x.Name == "Meter Reference Photo");
            if (MeterReferencePhoto == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Meter Reference Photo",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "MRP",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SubsidyReferencePhoto = _context.DocumentLists.FirstOrDefault(x => x.Name == "Subsidy Reference Photo");
            if (SubsidyReferencePhoto == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Subsidy Reference Photo",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SRP",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var BillNameChange = _context.DocumentLists.FirstOrDefault(x => x.Name == "Bill Name Change");
            if (BillNameChange == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Bill Name Change",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "BNC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InvoicePayment = _context.DocumentLists.FirstOrDefault(x => x.Name == "Invoice Payment");
            if (InvoicePayment == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Invoice Payment",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "IP",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SitePhoto = _context.DocumentLists.FirstOrDefault(x => x.Name == "SitePhoto");
            if (SitePhoto == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Site Photo",
                        DocumenFormate = "PNG,JPG",
                        DocumenSize = "1",
                        DocumentShortName = "SP",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SSBill = _context.DocumentLists.FirstOrDefault(x => x.Name == "SS Bill");
            if (SSBill == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "SS Bill",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SS",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var STCBill = _context.DocumentLists.FirstOrDefault(x => x.Name == "STC Bill");
            if (STCBill == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "STC Bill",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "STC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var CancelCheque = _context.DocumentLists.FirstOrDefault(x => x.Name == "CancelCheque");
            if (PassportPhoto == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Cancel Cheque",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "CC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var OCCopy = _context.DocumentLists.FirstOrDefault(x => x.Name == "OC Copy");
            if (OCCopy == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "OC Copy",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "OC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var ScannedSelfCertificate = _context.DocumentLists.FirstOrDefault(x => x.Name == "Scanned Self Certificate");
            if (ScannedSelfCertificate == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Scanned Self Certificate",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "SSC",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var JIR = _context.DocumentLists.FirstOrDefault(x => x.Name == "JIR");
            if (JIR == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "JIR",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "JIR",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var EstimatePaymentReceipt = _context.DocumentLists.FirstOrDefault(x => x.Name == "Estimate Payment Receipt");
            if (EstimatePaymentReceipt == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Estimate Payment Receipt",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "EPR",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var LoadReduceLetter = _context.DocumentLists.FirstOrDefault(x => x.Name == "Load Reduce Letter");
            if (LoadReduceLetter == null)
            {
                _context.DocumentLists.Add(
                    new DocumentList
                    {
                        Name = "Load Reduce Letter",
                        DocumenFormate = "PDF",
                        DocumenSize = "1",
                        DocumentShortName = "LRL",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
