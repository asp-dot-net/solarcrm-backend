﻿using System.Linq;
using Abp.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using solarcrm.Editions;
using solarcrm.EntityFrameworkCore;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultCommonValuesBuilder
    {
        private readonly solarcrmDbContext _context;

        public DefaultCommonValuesBuilder(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            //new DefaultEditionCreator(_context).Create();
            //new DefaultLanguagesCreator(_context).Create();
            //new HostRoleAndUserCreator(_context).Create();
            //new DefaultSettingsCreator(_context).Create();
            
                new DefaultSectionCreator(_context).Create();
            
                new DefaultDataVaultActionCreator(_context).Create();
                new DefaultUserTypeCreator(_context).Create();
                //new DefaultStockCategoryCreator(_context).Create();
                //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.stockcategory ON;");
                //_context.SaveChanges();
                //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.stockcategory OFF;");

                new DefaultApplicationStatusCreator(_context).Create();
                new DefaultSolarTypeCreator(_context).Create();
               
                new DefaultLeadSourcesCreator(_context).Create();   
                //new DefaultLeadStatusCreator(_context).Create();
                //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadStatus ON;");
                //_context.SaveChanges();
                //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadStatus OFF;");
                new DefaultLeadsTypeCreator(_context).Create();
                new DefaultProjectStatusCreator(_context).Create();
                new DefaultProjectTypeCreator(_context).Create();
                //new DefaultLeadsActivityCreator(_context).Create();
                new DefaultCancelReasonsCreator(_context).Create();
                new DefaultTeamsCreator(_context).Create();
                //new DefaultDiscomsCreator(_context).Create();   
                new DefaultDocumentListsCreator(_context).Create();
                new DefaultHoleReasonsCreator(_context).Create();
                new DefaultInvoiceTypeCreator(_context).Create();
            //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadActivity ON;");
            _context.SaveChanges();
            //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadActivity OFF;");

            //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadStatus OFF;");

        }
    }
}