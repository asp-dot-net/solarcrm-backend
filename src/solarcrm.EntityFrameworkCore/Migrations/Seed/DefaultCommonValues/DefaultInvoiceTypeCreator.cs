﻿using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.InvoiceType;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultInvoiceTypeCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultInvoiceTypeCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateInvoiceType();
        }
        public void CreateInvoiceType()
        {
            var Installer = _context.InvoiceType.FirstOrDefault(x => x.Name == "Installer");
            if (Installer == null)
            {
                _context.InvoiceType.Add(
                    new InvoiceType
                    {
                        Name = "Installer",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var ChanelPartner = _context.InvoiceType.FirstOrDefault(x => x.Name == "Chanel Partner");
            if (ChanelPartner == null)
            {
                _context.InvoiceType.Add(
                    new InvoiceType
                    {
                        Name = "Chanel Partner",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Other = _context.InvoiceType.FirstOrDefault(x => x.Name == "Other");
            if (Other == null)
            {
                _context.InvoiceType.Add(
                    new InvoiceType
                    {
                        Name = "Other",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
