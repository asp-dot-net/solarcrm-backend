﻿using solarcrm.DataVaults.LeadActivity;
using solarcrm.DataVaults.LeadSource;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultLeadSourcesCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultLeadSourcesCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateLeadSources();
        }
        public void CreateLeadSources()
        {
            var Telemarketing = _context.LeadSources.FirstOrDefault(x => x.Name == "Telemarketing");
            if (Telemarketing == null)
            {
                _context.LeadSources.Add(
                    new LeadSource
                    {
                        //Id = 1,
                        Name = "Telemarketing",
                        LeadSourceColorClass = "",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var MobileApplication = _context.LeadSources.FirstOrDefault(x => x.Name == "Mobile Application");
            if (MobileApplication == null)
            {
                _context.LeadSources.Add(
                    new LeadSource
                    {
                        //Id = 2,
                        Name = "Mobile Application",
                        LeadSourceColorClass = "",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Referral = _context.LeadSources.FirstOrDefault(x => x.Name == "Referral");
            if (Referral == null)
            {
                _context.LeadSources.Add(
                    new LeadSource
                    {
                        //Id = 3,
                        Name = "Referral",
                        LeadSourceColorClass = "",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Facebook = _context.LeadSources.FirstOrDefault(x => x.Name == "Facebook");
            if (Facebook == null)
            {
                _context.LeadSources.Add(
                    new LeadSource
                    {
                        //Id = 4,
                        Name = "Facebook",
                        LeadSourceColorClass = "",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Others = _context.LeadSources.FirstOrDefault(x => x.Name == "Others");
            if (Others == null)
            {
                _context.LeadSources.Add(
                    new LeadSource
                    {
                        //Id = 5,
                        Name = "Others",
                        LeadSourceColorClass = "",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
