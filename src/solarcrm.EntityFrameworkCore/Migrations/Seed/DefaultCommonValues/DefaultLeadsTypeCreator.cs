﻿using solarcrm.DataVaults.SolarType;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultLeadsTypeCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultLeadsTypeCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateLeadType();
        }
        public void CreateLeadType()
        {
            var NewInquiry = _context.LeadTypes.FirstOrDefault(x => x.Name == "New Inquiry");
            if (NewInquiry == null)
            {
                _context.LeadTypes.Add(
                    new DataVaults.LeadType.LeadType
                    {
                        Name = "New Inquiry",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Prospect = _context.LeadTypes.FirstOrDefault(x => x.Name == "Prospect");
            if (Prospect == null)
            {
                _context.LeadTypes.Add(
                    new DataVaults.LeadType.LeadType
                    {
                        Name = "Prospect",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Customer = _context.LeadTypes.FirstOrDefault(x => x.Name == "Customer");
            if (Customer == null)
            {
                _context.LeadTypes.Add(
                    new DataVaults.LeadType.LeadType
                    {
                        Name = "Customer",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Cancelled = _context.LeadTypes.FirstOrDefault(x => x.Name == "Cancelled");
            if (Cancelled == null)
            {
                _context.LeadTypes.Add(
                    new DataVaults.LeadType.LeadType
                    {
                        Name = "Cancelled",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Reject = _context.LeadTypes.FirstOrDefault(x => x.Name == "Reject");
            if (Reject == null)
            {
                _context.LeadTypes.Add(
                    new DataVaults.LeadType.LeadType
                    {
                        Name = "Reject",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
