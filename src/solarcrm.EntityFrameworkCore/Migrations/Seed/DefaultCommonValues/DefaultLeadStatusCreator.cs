﻿using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.LeadSource;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Twilio.TwiML.Fax;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultLeadStatusCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultLeadStatusCreator(solarcrmDbContext context)
        {  
            _context = context;           
        }

        public void Create()
        {
            CreateLeadStatus();
            if (_context != null)
            {
                //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadStatus ON;");
                _context.SaveChanges();
              //  _context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadStatus OFF;");
            }
        }
        public void CreateLeadStatus()
        {
            var Newinquiry = _context.leadStatus.FirstOrDefault(x => x.Status == "New inquiry");
            if (Newinquiry == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                        //Id = 1,
                        Status = "New inquiry",
                        LeadStatusColorClass = "badge-primary",
                        LeadStatusIconClass = "las la-pen-square",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var ProjectCreated = _context.leadStatus.FirstOrDefault(x => x.Status == "Project Created");
            if (ProjectCreated == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                        //Id = 2,
                        Status = "Project Created",
                        LeadStatusColorClass = "badge-primary",
                        LeadStatusIconClass = "las la-edit",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var QuotationGiven = _context.leadStatus.FirstOrDefault(x => x.Status == "Quotation Given");
            if (QuotationGiven == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                        ///Id = 3,
                        Status = "Quotation Given",
                        LeadStatusColorClass = "badge-warning",
                        LeadStatusIconClass = "las la-file-invoice",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var DocumentReceived = _context.leadStatus.FirstOrDefault(x => x.Status == "Document Received");
            if (DocumentReceived == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                        //Id = 4,
                        Status = "Document Received",
                        LeadStatusColorClass = "badge-warning",
                        LeadStatusIconClass = "las la-folder",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var DepositReceived = _context.leadStatus.FirstOrDefault(x => x.Status == "Deposit Received");
            if (DepositReceived == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                        //Id = 5,
                        Status = "Deposit Received",
                        LeadStatusColorClass = "badge-warning",
                        LeadStatusIconClass = "las la-money-check-alt",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var ReadytoDispatch = _context.leadStatus.FirstOrDefault(x => x.Status == "Ready to Dispatch");
            if (ReadytoDispatch == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                       // Id = 6,
                        Status = "Ready to Dispatch",
                        LeadStatusColorClass = "badge-warning",
                        LeadStatusIconClass = "las la-shipping-fast",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InstallationBooked = _context.leadStatus.FirstOrDefault(x => x.Status == "Installation Booked");
            if (InstallationBooked == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                        //Id = 7,
                        Status = "Installation Booked",
                        LeadStatusColorClass = "badge-warning",
                        LeadStatusIconClass = "las la-calendar-check",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InstallationComplete = _context.leadStatus.FirstOrDefault(x => x.Status == "Installation Complete");
            if (InstallationComplete == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                        //Id =8,
                        Status = "Installation Complete",
                        LeadStatusColorClass = "badge-success",
                        LeadStatusIconClass = "las la-clipboard-check",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SubsidyClaimed = _context.leadStatus.FirstOrDefault(x => x.Status == "Subsidy Claimed");
            if (SubsidyClaimed == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                       // Id = 9,
                        Status = "Subsidy Claimed",
                        LeadStatusColorClass = "badge-info",
                        LeadStatusIconClass = "las la-file-invoice",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SubsidyinProcess = _context.leadStatus.FirstOrDefault(x => x.Status == "Subsidy in Process");
            if (SubsidyinProcess == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                       // Id = 10,
                        Status = "Subsidy in Process",
                        LeadStatusColorClass = "badge-info",
                        LeadStatusIconClass = "las la-file-invoice",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SubsidyReceived = _context.leadStatus.FirstOrDefault(x => x.Status == "Subsidy Received");
            if (SubsidyReceived == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                       // Id = 11,
                        Status = "Subsidy Received",
                        LeadStatusColorClass = "badge-success",
                        LeadStatusIconClass = "las la-file-invoice-dollar",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Cancelled = _context.leadStatus.FirstOrDefault(x => x.Status == "Cancelled");
            if (Cancelled == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                       // Id = 12,
                        Status = "Cancelled",
                        LeadStatusColorClass = "badge-danger",
                        LeadStatusIconClass = "las la-ban",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Rejected = _context.leadStatus.FirstOrDefault(x => x.Status == "Rejected");
            if (Rejected == null)
            {
                _context.leadStatus.Add(
                    new DataVaults.LeadStatus.LeadStatus
                    {
                     //   Id = 13,
                        Status = "Rejected",
                        LeadStatusColorClass = "badge-danger",
                        LeadStatusIconClass = "las la-ban",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }         
        }
    }
}
