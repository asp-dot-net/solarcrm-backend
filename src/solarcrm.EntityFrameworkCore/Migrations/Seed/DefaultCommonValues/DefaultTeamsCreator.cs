﻿using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.Teams;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultTeamsCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultTeamsCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateTeams();
        }
        public void CreateTeams()
        {
            var TeamA = _context.Teams.FirstOrDefault(x => x.Name == "Team A");
            if (TeamA == null)
            {
                _context.Teams.Add(
                    new Teams
                    {
                        Name = "Team A",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var TeamB = _context.Teams.FirstOrDefault(x => x.Name == "Team B");
            if (TeamB == null)
            {
                _context.Teams.Add(
                    new Teams
                    {
                        Name = "Team B",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
