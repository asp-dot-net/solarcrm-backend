﻿using Org.BouncyCastle.Crypto;
using solarcrm.DataVaults;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.StockCategory;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.ConstrainedExecution;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Twilio.TwiML.Voice;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultApplicationStatusCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultApplicationStatusCreator(solarcrmDbContext context)
        {
            _context = context;
            
        }

        public void Create()
        {
            CreateApplicationStatus();

           
        }
        public void CreateApplicationStatus()
        {
            var ApplicationSubmitted = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Application Submitted");
            if (ApplicationSubmitted == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Application Submitted",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var DocumentVerified = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Document Verified");
            if (DocumentVerified == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Document Verified",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var DisComLetter = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "DisCom Letter");
            if (DisComLetter == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "DisCom Letter",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var FeasibilityApproved = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Feasibility Approved");
            if (FeasibilityApproved == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {                       
                        ApplicationStatusName = "Feasibility Approved",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var WorkExecution = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Work Execution");
            if (WorkExecution == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Work Execution",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InspectionSelfCertification = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Inspection Self Certification");
            if (InspectionSelfCertification == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Inspection Self Certification",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var MeterInstallation = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Meter Installation");
            if (MeterInstallation == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Meter Installation",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SubsidyClaimed = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Subsidy Claimed");
            if (SubsidyClaimed == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Subsidy Claimed",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InspectionApprovedFromCEI = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Inspection Approved From CEI");
            if (InspectionApprovedFromCEI == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Inspection Approved From CEI",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var IntimationRejected = _context.applicationStatus.FirstOrDefault(x => x.ApplicationStatusName == "Intimation Rejected");
            if (IntimationRejected == null)
            {
                _context.applicationStatus.Add(
                    new ApplicationStatus
                    {
                        ApplicationStatusName = "Intimation Rejected",
                        ApplicationStatusLabelColor = "bg-success",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
