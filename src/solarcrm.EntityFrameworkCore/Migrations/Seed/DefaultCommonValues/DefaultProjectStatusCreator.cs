﻿using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.ProjectStatus;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Twilio.TwiML.Fax;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultProjectStatusCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultProjectStatusCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateProjectStatus();
        }
        public void CreateProjectStatus()
        {
            var ProjectCreated = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Project Created");
            if (ProjectCreated == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 1,
                        Name = "Project Created",                       
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var QuotationGiven = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Quotation Given");
            if (QuotationGiven == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 2,
                        Name = "Quotation Given",                        
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var DocumentReceived = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Document Received");
            if (DocumentReceived == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 3,
                        Name = "Document Received",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var DepositReceived = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Deposit Received");
            if (DepositReceived == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 4,
                        Name = "Deposit Received",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var ReadyToDispatch = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Ready To Dispatch");
            if (ReadyToDispatch == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 5,
                        Name = "Ready To Dispatch",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InstallationBooked = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Installation Booked");
            if (InstallationBooked == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 5,
                        Name = "Installation Booked",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var InstallationComplete = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Installation Complete");
            if (InstallationComplete == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 5,
                        Name = "Installation Complete",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SubcidyClaimed = _context.ProjectStatus.FirstOrDefault(x => x.Name == "Subcidy Claimed");
            if (SubcidyClaimed == null)
            {
                _context.ProjectStatus.Add(
                    new ProjectStatus
                    {
                        //Id = 5,
                        Name = "Subcidy Claimed",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
