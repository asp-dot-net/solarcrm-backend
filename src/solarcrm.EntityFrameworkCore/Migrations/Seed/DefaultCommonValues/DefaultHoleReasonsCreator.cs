﻿using solarcrm.DataVaults.HoldReasons;
using solarcrm.DataVaults.Teams;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultHoleReasonsCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultHoleReasonsCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateHoleReasons();
        }
        public void CreateHoleReasons()
        {
            var Inverter = _context.HoldReasonss.FirstOrDefault(x => x.Name == "Inverter & Panels Not In Stock");
            if (Inverter == null)
            {
                _context.HoldReasonss.Add(
                    new HoldReasons
                    {
                        Name = "Inverter & Panels Not In Stock",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }            
        }
    }
}
