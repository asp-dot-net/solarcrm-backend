﻿using Org.BouncyCastle.Asn1.Utilities;
using solarcrm.DataVaults.SolarType;
using solarcrm.DataVaults.StockCategory;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultSolarTypeCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultSolarTypeCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSolarType();
        }
        public void CreateSolarType()
        {
            var Residential = _context.solarTypes.FirstOrDefault(x => x.Name == "Residential");
            if (Residential == null)
            {
                _context.solarTypes.Add(
                    new SolarType
                    {
                        Name = "Residential",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Commercial = _context.solarTypes.FirstOrDefault(x => x.Name == "Commercial");
            if (Commercial == null)
            {
                _context.solarTypes.Add(
                    new SolarType
                    {
                        Name = "Commercial",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var SolarUpgrade = _context.solarTypes.FirstOrDefault(x => x.Name == "Solar Upgrade");
            if (SolarUpgrade == null)
            {
                _context.solarTypes.Add(
                    new SolarType
                    {
                        Name = "Solar Upgrade",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SolarPump = _context.solarTypes.FirstOrDefault(x => x.Name == "Solar Pump");
            if (SolarPump == null)
            {
                _context.solarTypes.Add(
                    new SolarType
                    {
                        Name = "Solar Pump",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }            
        }
    }
}
