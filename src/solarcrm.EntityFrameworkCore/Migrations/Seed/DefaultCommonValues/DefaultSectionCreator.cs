﻿using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultSectionCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultSectionCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSections();
        }

        public void CreateSections()
        {
            var leadSource = _context.Sections.FirstOrDefault(x => x.SectionName == "Lead Source");
            if (leadSource == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 1,
                        SectionName = "Lead Source"
                    });
            }
            
            var leadType = _context.Sections.FirstOrDefault(x => x.SectionName == "Lead Type");
            if (leadType == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 2,
                        SectionName = "Lead Type"
                    });
            }
            
            var docList = _context.Sections.FirstOrDefault(x => x.SectionName == "Document List");
            if (docList == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 3,
                        SectionName = "Document List"
                    });
            }
            
            var teams = _context.Sections.FirstOrDefault(x => x.SectionName == "Teams");
            if (teams == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 4,
                        SectionName = "Teams"
                    });
            }
            
            var locations = _context.Sections.FirstOrDefault(x => x.SectionName == "Locations");
            if (locations == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 5,
                        SectionName = "Locations"
                    });
            }

            var projectTypes = _context.Sections.FirstOrDefault(x => x.SectionName == "Project Types");
            if (projectTypes == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 6,
                        SectionName = "Project Types"
                    });
            }
            
            var projectStatus = _context.Sections.FirstOrDefault(x => x.SectionName == "Project Status");
            if (projectStatus == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 7,
                        SectionName = "Project Status"
                    });
            }
            
            var stockCategory = _context.Sections.FirstOrDefault(x => x.SectionName == "Stock Category");
            if (stockCategory == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 8,
                        SectionName = "Stock Category"
                    });
            }
            
            var invoiceType = _context.Sections.FirstOrDefault(x => x.SectionName == "Invoice Type");
            if (invoiceType == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 9,
                        SectionName = "Invoice Type"
                    });
            }
            
            var discom = _context.Sections.FirstOrDefault(x => x.SectionName == "Discom");
            if (discom == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 10,
                        SectionName = "Discom"
                    });
            }
            
            var division = _context.Sections.FirstOrDefault(x => x.SectionName == "Division");
            if (division == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 11,
                        SectionName = "Division"
                    });
            }
            
            var subDivision = _context.Sections.FirstOrDefault(x => x.SectionName == "Sub Division");
            if (subDivision == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 12,
                        SectionName = "Sub Division"
                    });
            }
			
			var state = _context.Sections.FirstOrDefault(x => x.SectionName == "State");
            if (state == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 13,
                        SectionName = "State"
                    });
            }
			
			var district = _context.Sections.FirstOrDefault(x => x.SectionName == "District");
            if (district == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 14,
                        SectionName = "District"
                    });
            }
			
			var taluka = _context.Sections.FirstOrDefault(x => x.SectionName == "Taluka");
            if (taluka == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 15,
                        SectionName = "Taluka"
                    });
            }
			
			var city = _context.Sections.FirstOrDefault(x => x.SectionName == "City");
            if (city == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 16,
                        SectionName = "City"
                    });
            }

            var cancelLeads = _context.Sections.FirstOrDefault(x => x.SectionName == "Cancel Leads");
            if (cancelLeads == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 17,
                        SectionName = "Cancel Leads"
                    });
            }
            
            var rejectlLeads = _context.Sections.FirstOrDefault(x => x.SectionName == "Reject Leads");
            if (rejectlLeads == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 18,
                        SectionName = "Reject Leads"
                    });
            }
            
            var jobCancel = _context.Sections.FirstOrDefault(x => x.SectionName == "Job Cancel");
            if (jobCancel == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 19,
                        SectionName = "Job Cancel"
                    });
            }

            var holdJob = _context.Sections.FirstOrDefault(x => x.SectionName == "Hold Job");
            if (holdJob == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 20,
                        SectionName = "Hold Job"
                    });
            }

            var transactionType = _context.Sections.FirstOrDefault(x => x.SectionName == "Transaction Type");
            if (transactionType == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 21,
                        SectionName = "Transaction Type"
                    });
            }

            var documentLibrary = _context.Sections.FirstOrDefault(x => x.SectionName == "Document Library");
            if (documentLibrary == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 22,
                        SectionName = "Document Library"
                    });
            }
            
            var smsTemplates = _context.Sections.FirstOrDefault(x => x.SectionName == "SMS Templates");
            if (smsTemplates == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 23,
                        SectionName = "SMS Templates"
                    });
            }
            
            var emailTemplates = _context.Sections.FirstOrDefault(x => x.SectionName == "Email Templates");
            if (emailTemplates == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 24,
                        SectionName = "Email Templates"
                    });
            }
            
            var stockItems = _context.Sections.FirstOrDefault(x => x.SectionName == "Stock Items");
            if (stockItems == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 25,
                        SectionName = "Stock Items"
                    });
            }

            var solarType = _context.Sections.FirstOrDefault(x => x.SectionName == "Solar Type");
            if (solarType == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 26,
                        SectionName = "Solar Type"
                    });
            }

            var HeightStructure = _context.Sections.FirstOrDefault(x => x.SectionName == "Height Structure");
            if (HeightStructure == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 27,
                        SectionName = "Height Structure"
                    });
            }
            var Variation = _context.Sections.FirstOrDefault(x => x.SectionName == "Variation");
            if (Variation == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 28,
                        SectionName = "Variation"
                    });
            }
            var ManageCustomer = _context.Sections.FirstOrDefault(x => x.SectionName == "Manage Customer");
            if (ManageCustomer == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 29,
                        SectionName = "Manage Customer"
                    });
            }
           
            var LeadStatus = _context.Sections.FirstOrDefault(x => x.SectionName == "Lead Status");
            if (LeadStatus == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 30,
                        SectionName = "Lead Status"
                    });
            }

            var Tender = _context.Sections.FirstOrDefault(x => x.SectionName == "Tender");
            if (Tender == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 31,
                        SectionName = "Tender"
                    });
            }

            var Price = _context.Sections.FirstOrDefault(x => x.SectionName == "Price");
            if (Price == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 32,
                        SectionName = "Price"
                    });
            }
            var Circle = _context.Sections.FirstOrDefault(x => x.SectionName == "Circle");
            if (Circle == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 33,
                        SectionName = "Circle"
                    });
            }
            var Department = _context.Sections.FirstOrDefault(x => x.SectionName == "Department");
            if (Department == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 34,
                        SectionName = "Department"
                    });
            }
            var ManageLead = _context.Sections.FirstOrDefault(x => x.SectionName == "Manage Lead");
            if (ManageLead == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 35,
                        SectionName = "Manage Lead"
                    });
            }
            var CustomeTag = _context.Sections.FirstOrDefault(x => x.SectionName == "CustomeTag");
            if (CustomeTag == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 37,
                        SectionName = "CustomeTag"
                    });
            }
            var ApplicationTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "ApplicationTracker");
            if (ApplicationTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 38,
                        SectionName = "ApplicationTracker"
                    });
            }
            var EstimatePaidTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "EstimatePaidTracker");
            if (EstimatePaidTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 39,
                        SectionName = "EstimatePaidTracker"
                    });
            }
            var DispatchTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "DispatchTracker");
            if (DispatchTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 40,
                        SectionName = "DispatchTracker"
                    });
            }
            var PaymentIssuedTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "PaymentIssuedTracker");
            if (PaymentIssuedTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 41,
                        SectionName = "PaymentIssuedTracker"
                    });
            }
            var PaymentPaidTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "PaymentPaidTracker");
            if (PaymentPaidTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 42,
                        SectionName = "PaymentPaidTracker"
                    });
            }
            var MeterConnectTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "MeterConnectTracker");
            if (MeterConnectTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 43,
                        SectionName = "MeterConnectTracker"
                    });
            }
            var SerialNoPhotosTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "SerialNoPhotosTracker");
            if (SerialNoPhotosTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 44,
                        SectionName = "SerialNoPhotosTracker"
                    });
            }
            var MISReport = _context.Sections.FirstOrDefault(x => x.SectionName == "MISReport");
            if (MISReport == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 45,
                        SectionName = "MISReport"
                    });
            }
            var PdfTemplate = _context.Sections.FirstOrDefault(x => x.SectionName == "Pdf Templates");
            if (MISReport == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 46,
                        SectionName = "Pdf Templates"
                    });
            }

            var paymentType = _context.Sections.FirstOrDefault(x => x.SectionName == "PaymentType");
            if (paymentType == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 47,
                        SectionName = "PaymentType"
                    });
            }
            var paymentMode = _context.Sections.FirstOrDefault(x => x.SectionName == "PaymentMode");
            if (paymentMode == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 48,
                        SectionName = "PaymentMode"
                    });
            }
            var paymentDetails = _context.Sections.FirstOrDefault(x => x.SectionName == "paymentDetails");
            if (paymentDetails == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 49,
                        SectionName = "paymentDetails"
                    });
            }
            var paymentReceiptUpload = _context.Sections.FirstOrDefault(x => x.SectionName == "paymentReceiptUpload");
            if (paymentReceiptUpload == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 50,
                        SectionName = "paymentReceiptUpload"
                    });
            }
            var leadDocument = _context.Sections.FirstOrDefault(x => x.SectionName == "Lead Document");
            if (leadDocument == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 51,
                        SectionName = "Lead Document"
                    });
            }
            var jobInstallation = _context.Sections.FirstOrDefault(x => x.SectionName == "Job Installation");
            if (jobInstallation == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 52,
                        SectionName = "Job Installation"
                    });
            }
            var jobQuotation = _context.Sections.FirstOrDefault(x => x.SectionName == "Job Quotation");
            if (jobQuotation == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 53,
                        SectionName = "Job Quotation"
                    });
            }
            var JobRefund = _context.Sections.FirstOrDefault(x => x.SectionName == "Job Refund");
            if (JobRefund == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 54,
                        SectionName = "Job Refund"
                    });
            }
            var RefundReason = _context.Sections.FirstOrDefault(x => x.SectionName == "Refund Reason");
            if (RefundReason == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 55,
                        SectionName = "Refund Reason"
                    });
            }
            var Vehicalmaster = _context.Sections.FirstOrDefault(x => x.SectionName == "Vehical");
            if (Vehicalmaster == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 56,
                        SectionName = "Vehical"
                    });
            }
            var DispatchType = _context.Sections.FirstOrDefault(x => x.SectionName == "DispatchType");
            if (DispatchType == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 57,
                        SectionName = "DispatchType"
                    });
            }
            var CancelReason = _context.Sections.FirstOrDefault(x => x.SectionName == "Cancel Reason");
            if (CancelReason == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 58,
                        SectionName = "Cancel Reason"
                    });
            }
            var HoldReason = _context.Sections.FirstOrDefault(x => x.SectionName == "Hold Reason");
            if (HoldReason == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 59,
                        SectionName = "Hold Reason"
                    });
            }
            var RejectReason = _context.Sections.FirstOrDefault(x => x.SectionName == "Reject Reason");
            if (RejectReason == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 60,
                        SectionName = "Reject Reason"
                    });
            }

            var billOfMaterial = _context.Sections.FirstOrDefault(x => x.SectionName == "Bill Of Material");
            if (billOfMaterial == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 61,
                        SectionName = "Bill Of Material"
                    });
            }
            var dispatchExtraMaterial = _context.Sections.FirstOrDefault(x => x.SectionName == "Dispatch Extra Material");
            if (dispatchExtraMaterial == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 62,
                        SectionName = "Dispatch Extra Material"
                    });
            }
            var dispatchStockItemTransfer = _context.Sections.FirstOrDefault(x => x.SectionName == "Dispatch StockItem Transfer");
            if (dispatchStockItemTransfer == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 63,
                        SectionName = "Dispatch StockItem Transfer"
                    });
            }
            var transferDetails = _context.Sections.FirstOrDefault(x => x.SectionName == "Transfer Item Details");
            if (dispatchStockItemTransfer == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 64,
                        SectionName = "Transfer Item Details"
                    });
            }
            var manageJobs = _context.Sections.FirstOrDefault(x => x.SectionName == "Manage Job");
            if (manageJobs == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 65,
                        SectionName = "Manage Job"
                    });
            }
            var picklistJob = _context.Sections.FirstOrDefault(x => x.SectionName == "Picklist Job");
            if (picklistJob == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 66,
                        SectionName = "Picklist Job"
                    });
            }
            var TransportTracker = _context.Sections.FirstOrDefault(x => x.SectionName == "TransportTracker");
            if (TransportTracker == null)
            {
                _context.Sections.Add(
                    new Sections.Section
                    {
                        SectionId = 67,
                        SectionName = "TransportTracker"
                    });
            }
            
        }
        
    }
}
