﻿using Org.BouncyCastle.Asn1.Utilities;
using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.ProjectType;
using solarcrm.EntityFrameworkCore;
using Stripe;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultProjectTypeCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultProjectTypeCreator(solarcrmDbContext context)
        {
            _context = context;
        }
       
        public void Create()
        {
            CreateLeadSources();
        }
        public void CreateLeadSources()
        {
            var SolarService = _context.ProjectType.FirstOrDefault(x => x.Name == "Solar Service");
            if (SolarService == null)
            {
                _context.ProjectType.Add(
                    new DataVaults.ProjectType.ProjectType
                    {
                        //Id = 1,
                        Name = "Solar Service",                      
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var SolarUpgrade = _context.ProjectType.FirstOrDefault(x => x.Name == "Solar Upgrade");
            if (SolarUpgrade == null)
            {
                _context.ProjectType.Add(
                    new ProjectType
                    {
                        //Id = 2,
                        Name = "Solar Upgrade",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var InstallationService = _context.ProjectType.FirstOrDefault(x => x.Name == "Installation Service");
            if (InstallationService == null)
            {
                _context.ProjectType.Add(
                    new ProjectType
                    {
                        //Id = 3,
                        Name = "Installation Service",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var SolarPump = _context.ProjectType.FirstOrDefault(x => x.Name == "Solar Pump");
            if (SolarPump == null)
            {
                _context.ProjectType.Add(
                    new ProjectType
                    {
                        //Id = 4,
                        Name = "Solar Pump",                      
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Solar = _context.ProjectType.FirstOrDefault(x => x.Name == "Solar");
            if (Solar == null)
            {
                _context.ProjectType.Add(
                    new ProjectType
                    {
                        //Id = 5,
                        Name = "Solar",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
