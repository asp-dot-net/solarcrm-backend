﻿using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.StockCategory;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultStockCategoryCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultStockCategoryCreator(solarcrmDbContext context)
        {
            _context = context;           
        }

        public void Create()
        {
            CreateStockCategory();
            if (_context != null)
            {
               // _context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.stockcategory ON;");
                _context.SaveChanges();
               // _context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.stockcategory OFF;");

            }
        }
        public void CreateStockCategory()
        {
            var Panels = _context.StockCategory.FirstOrDefault(x => x.Id == 1);
            if (Panels == null)
            {
                _context.StockCategory.Add(
                    new StockCategory
                    {
                        //Id = 1,
                        Name = "Panels",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Inverter = _context.StockCategory.FirstOrDefault(x => x.Id == 2);
            if (Inverter == null)
            {
                _context.StockCategory.Add(
                    new StockCategory
                    {
                       // Id = 2,
                        Name = "Inverter",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Mounting = _context.StockCategory.FirstOrDefault(x => x.Id == 3);
            if (Mounting == null)
            {
                _context.StockCategory.Add(
                    new StockCategory
                    {
                        //Id = 3,
                        Name = "Mounting",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Electricals = _context.StockCategory.FirstOrDefault(x => x.Id == 4);
            if (Electricals == null)
            {
                _context.StockCategory.Add(
                    new StockCategory
                    {
                        //Id = 4,
                        Name = "Electricals",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var RawMaterials = _context.StockCategory.FirstOrDefault(x => x.Id == 5);
            if (RawMaterials == null)
            {
                _context.StockCategory.Add(
                    new StockCategory
                    {
                        //Id = 5,
                        Name = "Raw Materials",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Others = _context.StockCategory.FirstOrDefault(x => x.Id == 6);
            if (Others == null)
            {
                _context.StockCategory.Add(
                    new StockCategory
                    {
                        //Id = 6,
                        Name = "Others",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
