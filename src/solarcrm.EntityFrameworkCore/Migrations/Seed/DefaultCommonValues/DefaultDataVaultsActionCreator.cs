﻿using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultDataVaultActionCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultDataVaultActionCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDataVaultsAtions();
        }

        public void CreateDataVaultsAtions()
        {
            var DataVaultsActions_Created = _context.DataVaultsActions.FirstOrDefault(x => x.Name == "Created");
            if (DataVaultsActions_Created == null)
            {
                _context.DataVaultsActions.Add(
                    new DataVaults.ActivityLog.DataVaultsAction.DataVaultsAction
                    {
                        Name = "Created"
                    });
            }
            
            var DataVaultsActions_Modified = _context.DataVaultsActions.FirstOrDefault(x => x.Name == "Modified");
            if (DataVaultsActions_Modified == null)
            {
                _context.DataVaultsActions.Add(
                    new DataVaults.ActivityLog.DataVaultsAction.DataVaultsAction
                    {
                        Name = "Modified"
                    });
            }
            
            var DataVaultsActions_Deleted = _context.DataVaultsActions.FirstOrDefault(x => x.Name == "Deleted");
            if (DataVaultsActions_Deleted == null)
            {
                _context.DataVaultsActions.Add(
                    new DataVaults.ActivityLog.DataVaultsAction.DataVaultsAction
                    {
                        Name = "Deleted"
                    });
            }
        }
    }
}
