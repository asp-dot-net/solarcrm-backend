﻿using solarcrm.DataVaults.CancelReasons;
using solarcrm.DataVaults.LeadSource;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio.Http;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultCancelReasonsCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultCancelReasonsCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateCancelReasons();
        }
        public void CreateCancelReasons()
        {
            var NoResponse = _context.CancelReasonss.FirstOrDefault(x => x.Name == "No Response");
            if (NoResponse == null)
            {
                _context.CancelReasonss.Add(
                    new CancelReasons
                    {                        
                        Name = "No Response",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var FundIssue = _context.CancelReasonss.FirstOrDefault(x => x.Name == "Fund Issue");
            if (FundIssue == null)
            {
                _context.CancelReasonss.Add(
                    new CancelReasons
                    {
                        Name = "Fund Issue",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var SpaceIssue = _context.CancelReasonss.FirstOrDefault(x => x.Name == "Space Issue");
            if (SpaceIssue == null)
            {
                _context.CancelReasonss.Add(
                    new CancelReasons
                    {
                        Name = "Space Issue",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }            
        }
    }
}
