﻿using PayPalCheckoutSdk.Orders;
using solarcrm.DataVaults.Discom;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultDiscomsCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultDiscomsCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDiscoms();
           
        }
        public void CreateDiscoms()
        {
            var TorrentPowerSurat = _context.Discoms.FirstOrDefault(x => x.Name == "Torrent Power Surat");
            if (TorrentPowerSurat == null)
            {
                _context.Discoms.Add(
                    new Discom
                    {
                        Name = "Torrent Power Surat",
                        ABB = "Torrent",
                        LocationId = 1,
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow,
                        PaymentLink = "https://connect.torrentpower.com/tplcp/index.php/payment/solarview"
                    });
            }

            var Pgvcl = _context.Discoms.FirstOrDefault(x => x.Name == "Paschim Gujarat Vij Company Ltd.(PGVCL)");
            if (Pgvcl == null)
            {
                _context.Discoms.Add(
                    new Discom
                    {
                        Name = "Paschim Gujarat Vij Company Ltd.(PGVCL)",
                        ABB = "PGVCL",
                        LocationId = 1,
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow,
                        PaymentLink = "https://epayment.guvnl.in/SRPayPG/"
                    });
            }

            var Dgvcl = _context.Discoms.FirstOrDefault(x => x.Name == "Dakshin Gujarat Vij Company Limited (DGVCL)");
            if (Dgvcl == null)
            {
                _context.Discoms.Add(
                    new Discom
                    {
                        Name = "Dakshin Gujarat Vij Company Limited (DGVCL)",
                        ABB = "DGVCL",
                        LocationId = 1,
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow,
                        PaymentLink = "https://epayment.guvnl.in/SRPayDG/"
                    });
            }
            var Mgvcl = _context.Discoms.FirstOrDefault(x => x.Name == "Madhya Gujarat Vij Company Ltd.(MGVCL)");
            if (Mgvcl == null)
            {
                _context.Discoms.Add(
                    new Discom
                    {                       
                        Name = "Madhya Gujarat Vij Company Ltd.(MGVCL)",
                        ABB = "MGVCL",
                        LocationId = 1,
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow,
                        PaymentLink = "https://epayment.guvnl.in/SRPayMG/"
                    });
            }
            var TorrentPowerAhmedabad = _context.Discoms.FirstOrDefault(x => x.Name == "Torrent Power Ahmedabad");
            if (TorrentPowerAhmedabad == null)
            {
                _context.Discoms.Add(
                    new Discom
                    {
                        Name = "Torrent Power Ahmedabad",
                        ABB = "Torrent",
                        LocationId = 1,
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow,
                        PaymentLink = "https://connect.torrentpower.com/tplcp/index.php/payment/solarview"
                    });
            }

            var Ugvcl = _context.Discoms.FirstOrDefault(x => x.Name == "Uttar Gujarat Vij Company Ltd.(UGVCL)");
            if (Ugvcl == null)
            {
                _context.Discoms.Add(
                    new Discom
                    {
                        // Id = 6,
                        Name = "Uttar Gujarat Vij Company Ltd.(UGVCL)",
                        ABB = "UGVCL",
                        LocationId = 1,
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow,
                        PaymentLink = "https://epayment.guvnl.in/SRPayUG/"
                    });
            }           
        }
    }
}
