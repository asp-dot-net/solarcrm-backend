﻿using solarcrm.DataVaults.HoldReasons;
using solarcrm.DataVaults.JobCancellationReason;
using solarcrm.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultJobCancelReasonsCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultJobCancelReasonsCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateJobCancelReasons();
        }
        public void CreateJobCancelReasons()
        {
            var StockIssue = _context.JobCancellationReasons.FirstOrDefault(x => x.Name == "Stock Issue");
            if (StockIssue == null)
            {
                _context.JobCancellationReasons.Add(
                    new JobCancellationReason
                    {
                        Name = "Stock Issue",
                        IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
