﻿using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using solarcrm.DataVaults.LeadActivity;
using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.State;
using solarcrm.DataVaults.StockCategory;
using solarcrm.EntityFrameworkCore;
using solarcrm.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Migrations.Seed.DefaultCommonValues
{
    public class DefaultLeadsActivityCreator
    {
        private readonly solarcrmDbContext _context;

        public DefaultLeadsActivityCreator(solarcrmDbContext context)
        {
            _context = context;
        }

        public async void Create()
        {
            CreateLeadsActivity();
            if (_context != null)
            {
                var locationfound = _context.Location.FirstOrDefault(x => x.Id == 1);
                if(locationfound == null)
                {
                    var sql = System.IO.File.ReadAllText("wwwroot\\DbScript\\SolarCrmDbMasterScript.Sql");
                   if(sql != null)
                     _context.Database.ExecuteSqlRaw(sql);
                    
                    //var OrgID =  _context.extendOrganizationUnits.FirstOrDefault(x => x.Id == 1);
                    //if(OrgID != null)
                    //{
                    //    _context.Database.ExecuteSqlRaw("update Locations set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update Discoms set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update Circle set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update Divisions set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update SubDivisions set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update State set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update Taluka set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update District set TenantId = " + OrgID.TenantId + "}");
                    //    _context.Database.ExecuteSqlRaw("update City set TenantId = " + OrgID.TenantId + "}");
                    //}
                    

                    //var leadsource = _context.LeadSources.ToList();
                    //if (leadsource != null)
                    //{
                    //    foreach (var item in leadsource)
                    //    {
                    //        try
                    //        {
                    //            MultipleFieldOrganizationUnitSelection leadSourcerg = new MultipleFieldOrganizationUnitSelection();
                    //            leadSourcerg.LeadSourceOrganizationId = item.Id;
                    //            leadSourcerg.OrganizationUnitId = OrgID.Id;
                    //            leadSourcerg.TenantId = OrgID.TenantId;
                    //            _context.multipleFieldOrganizationUnitSelections.Add(leadSourcerg);
                    //        }
                    //        catch (System.Exception ex)
                    //        {
                    //            throw ex;
                    //        }
                    //    }
                    //}
                
                }
                
                // _context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadActivity ON;");
                _context.SaveChanges();
               // _context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.LeadActivity OFF;");
            }
        }
        public void CreateLeadsActivity()
        {
            var SMS = _context.LeadActivitys.FirstOrDefault(x => x.LeadActivityName == "SMS");
            if (SMS == null)
            {
                _context.LeadActivitys.Add(
                    new LeadActivity
                    {
                        //Id = 1,
                        LeadActivityName = "SMS",
                        //IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Email = _context.LeadActivitys.FirstOrDefault(x => x.LeadActivityName == "Email");
            if (Email == null)
            {
                _context.LeadActivitys.Add(
                    new LeadActivity
                    {
                       // Id = 2,
                        LeadActivityName = "Email",
                        //IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }

            var Reminder = _context.LeadActivitys.FirstOrDefault(x => x.LeadActivityName == "Reminder");
            if (Reminder == null)
            {
                _context.LeadActivitys.Add(
                    new LeadActivity
                    {
                      //  Id = 3,
                        LeadActivityName = "Reminder",
                        //IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Notify = _context.LeadActivitys.FirstOrDefault(x => x.LeadActivityName == "Notify");
            if (Notify == null)
            {
                _context.LeadActivitys.Add(
                    new LeadActivity
                    {
                      //  Id = 4,
                        LeadActivityName = "Notify",
                        //IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var Comment = _context.LeadActivitys.FirstOrDefault(x => x.LeadActivityName == "Comment");
            if (Comment == null)
            {
                _context.LeadActivitys.Add(
                    new LeadActivity
                    {
                       // Id = 5,
                        LeadActivityName = "Comment",
                        //IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
            var ToDo = _context.LeadActivitys.FirstOrDefault(x => x.LeadActivityName == "ToDo");
            if (ToDo == null)
            {
                _context.LeadActivitys.Add(
                    new LeadActivity
                    {
                      //  Id = 6,
                        LeadActivityName = "ToDo",
                        //IsActive = true,
                        CreatorUserId = 1,
                        CreationTime = System.DateTime.UtcNow
                    });
            }
        }
    }
}
