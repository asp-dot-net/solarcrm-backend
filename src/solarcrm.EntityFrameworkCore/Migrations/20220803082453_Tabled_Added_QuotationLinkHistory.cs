﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Tabled_Added_QuotationLinkHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "QuotationSignLatitude",
                table: "QuotationData",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "QuotationSignLongitude",
                table: "QuotationData",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QuotationSignedFileName",
                table: "QuotationData",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QuotationSignedIP",
                table: "QuotationData",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "QuotationSignedImagePath",
                table: "QuotationData",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "QuotationLinkHistorys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuotationId = table.Column<int>(type: "int", nullable: true),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Expired = table.Column<bool>(type: "bit", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationLinkHistorys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationLinkHistorys_QuotationData_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "QuotationData",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuotationLinkHistorys_QuotationId",
                table: "QuotationLinkHistorys",
                column: "QuotationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuotationLinkHistorys");

            migrationBuilder.DropColumn(
                name: "QuotationSignLatitude",
                table: "QuotationData");

            migrationBuilder.DropColumn(
                name: "QuotationSignLongitude",
                table: "QuotationData");

            migrationBuilder.DropColumn(
                name: "QuotationSignedFileName",
                table: "QuotationData");

            migrationBuilder.DropColumn(
                name: "QuotationSignedIP",
                table: "QuotationData");

            migrationBuilder.DropColumn(
                name: "QuotationSignedImagePath",
                table: "QuotationData");
        }
    }
}
