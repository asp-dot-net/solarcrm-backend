﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class addedBranchName_FieldLeads_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadActivityLog_LeadAction_LeadActionId",
                table: "LeadActivityLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Discoms_DiscomId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_HeightStructure_HeightStructureId",
                table: "Leads");

            migrationBuilder.AlterColumn<int>(
                name: "HeightStructureId",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DiscomId",
                table: "Leads",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BankBrachName",
                table: "Leads",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LeadActionId",
                table: "LeadActivityLog",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_LeadActivityLog_LeadAction_LeadActionId",
                table: "LeadActivityLog",
                column: "LeadActionId",
                principalTable: "LeadAction",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Discoms_DiscomId",
                table: "Leads",
                column: "DiscomId",
                principalTable: "Discoms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_HeightStructure_HeightStructureId",
                table: "Leads",
                column: "HeightStructureId",
                principalTable: "HeightStructure",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LeadActivityLog_LeadAction_LeadActionId",
                table: "LeadActivityLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_Discoms_DiscomId",
                table: "Leads");

            migrationBuilder.DropForeignKey(
                name: "FK_Leads_HeightStructure_HeightStructureId",
                table: "Leads");

            migrationBuilder.DropColumn(
                name: "BankBrachName",
                table: "Leads");

            migrationBuilder.AlterColumn<int>(
                name: "HeightStructureId",
                table: "Leads",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "DiscomId",
                table: "Leads",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "LeadActionId",
                table: "LeadActivityLog",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_LeadActivityLog_LeadAction_LeadActionId",
                table: "LeadActivityLog",
                column: "LeadActionId",
                principalTable: "LeadAction",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_Discoms_DiscomId",
                table: "Leads",
                column: "DiscomId",
                principalTable: "Discoms",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Leads_HeightStructure_HeightStructureId",
                table: "Leads",
                column: "HeightStructureId",
                principalTable: "HeightStructure",
                principalColumn: "Id");
        }
    }
}
