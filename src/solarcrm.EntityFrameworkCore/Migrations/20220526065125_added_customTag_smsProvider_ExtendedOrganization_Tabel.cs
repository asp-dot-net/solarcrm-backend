﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class added_customTag_smsProvider_ExtendedOrganization_Tabel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProviderTemp_Id",
                table: "SmsTemplates",
                type: "nvarchar(80)",
                maxLength: 80,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "LeadStatusColorClass",
                table: "LeadStatus",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeadStatusIconClass",
                table: "LeadStatus",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StatusCode",
                table: "LeadStatus",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeadActionColorClass",
                table: "LeadAction",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LeadActionIconClass",
                table: "LeadAction",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AbpOrganizationUnits",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "MerchantId",
                table: "AbpOrganizationUnits",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OrganizationCode",
                table: "AbpOrganizationUnits",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_Address",
                table: "AbpOrganizationUnits",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_ContactNo",
                table: "AbpOrganizationUnits",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_Email",
                table: "AbpOrganizationUnits",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Organization_GSTNumber",
                table: "AbpOrganizationUnits",
                type: "int",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_LogoFileName",
                table: "AbpOrganizationUnits",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_LogoFilePath",
                table: "AbpOrganizationUnits",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_Mobile",
                table: "AbpOrganizationUnits",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_ProjectId",
                table: "AbpOrganizationUnits",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_defaultFromAddress",
                table: "AbpOrganizationUnits",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_defaultFromDisplayName",
                table: "AbpOrganizationUnits",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PublishKey",
                table: "AbpOrganizationUnits",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecrateKey",
                table: "AbpOrganizationUnits",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "cardNumber",
                table: "AbpOrganizationUnits",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "cardholderName",
                table: "AbpOrganizationUnits",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "cvn",
                table: "AbpOrganizationUnits",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "expiryDateMonth",
                table: "AbpOrganizationUnits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "expiryDateYear",
                table: "AbpOrganizationUnits",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "paymentMethod",
                table: "AbpOrganizationUnits",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CustomeTag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TagTableName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    TagTableFieldName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomeTag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LeadDocuments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadDocumentId = table.Column<int>(type: "int", nullable: false),
                    DocumentId = table.Column<int>(type: "int", nullable: false),
                    DocumentNumber = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    LeadDocumentPath = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LeadDocuments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LeadDocuments_DocumentLists_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "DocumentLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LeadDocuments_Leads_LeadDocumentId",
                        column: x => x.LeadDocumentId,
                        principalTable: "Leads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "sms_ServiceProviderOrganization",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SMSProviderOrganizationId = table.Column<long>(type: "bigint", nullable: false),
                    Client_Id = table.Column<string>(type: "nvarchar(55)", maxLength: 55, nullable: true),
                    Sender_Id = table.Column<string>(type: "nvarchar(55)", maxLength: 55, nullable: true),
                    msgtype = table.Column<string>(type: "nvarchar(55)", maxLength: 55, nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sms_ServiceProviderOrganization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_sms_ServiceProviderOrganization_AbpOrganizationUnits_SMSProviderOrganizationId",
                        column: x => x.SMSProviderOrganizationId,
                        principalTable: "AbpOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LeadDocuments_DocumentId",
                table: "LeadDocuments",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_LeadDocuments_LeadDocumentId",
                table: "LeadDocuments",
                column: "LeadDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_sms_ServiceProviderOrganization_SMSProviderOrganizationId",
                table: "sms_ServiceProviderOrganization",
                column: "SMSProviderOrganizationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomeTag");

            migrationBuilder.DropTable(
                name: "LeadDocuments");

            migrationBuilder.DropTable(
                name: "sms_ServiceProviderOrganization");

            migrationBuilder.DropColumn(
                name: "ProviderTemp_Id",
                table: "SmsTemplates");

            migrationBuilder.DropColumn(
                name: "LeadStatusColorClass",
                table: "LeadStatus");

            migrationBuilder.DropColumn(
                name: "LeadStatusIconClass",
                table: "LeadStatus");

            migrationBuilder.DropColumn(
                name: "StatusCode",
                table: "LeadStatus");

            migrationBuilder.DropColumn(
                name: "LeadActionColorClass",
                table: "LeadAction");

            migrationBuilder.DropColumn(
                name: "LeadActionIconClass",
                table: "LeadAction");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "MerchantId",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "OrganizationCode",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_Address",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_ContactNo",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_Email",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_GSTNumber",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_LogoFileName",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_LogoFilePath",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_Mobile",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_ProjectId",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_defaultFromAddress",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "Organization_defaultFromDisplayName",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "PublishKey",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "SecrateKey",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "cardNumber",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "cardholderName",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "cvn",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "expiryDateMonth",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "expiryDateYear",
                table: "AbpOrganizationUnits");

            migrationBuilder.DropColumn(
                name: "paymentMethod",
                table: "AbpOrganizationUnits");
        }
    }
}
