﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class addedcancelrejectLeadReasontable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tender",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(32)",
                oldMaxLength: 32);

            migrationBuilder.CreateTable(
                name: "CancelRejectLeadReason",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadId = table.Column<int>(type: "int", nullable: true),
                    CancelReasonsLeadId = table.Column<int>(type: "int", nullable: true),
                    RejectReasonLeadId = table.Column<int>(type: "int", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CancelRejectLeadReason", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CancelRejectLeadReason_CancelReasons_CancelReasonsLeadId",
                        column: x => x.CancelReasonsLeadId,
                        principalTable: "CancelReasons",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CancelRejectLeadReason_Leads_LeadId",
                        column: x => x.LeadId,
                        principalTable: "Leads",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_CancelRejectLeadReason_RejectReasons_RejectReasonLeadId",
                        column: x => x.RejectReasonLeadId,
                        principalTable: "RejectReasons",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_CancelRejectLeadReason_CancelReasonsLeadId",
                table: "CancelRejectLeadReason",
                column: "CancelReasonsLeadId");

            migrationBuilder.CreateIndex(
                name: "IX_CancelRejectLeadReason_LeadId",
                table: "CancelRejectLeadReason",
                column: "LeadId");

            migrationBuilder.CreateIndex(
                name: "IX_CancelRejectLeadReason_RejectReasonLeadId",
                table: "CancelRejectLeadReason",
                column: "RejectReasonLeadId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CancelRejectLeadReason");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tender",
                type: "nvarchar(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
