﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Table_Multiselect_AreaType_pricetable_with_organizationBankDetailsAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "DepositeRequired",
                table: "QuotationDataDetail",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Organization_Website",
                table: "AbpOrganizationUnits",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BankDetailsOrganization",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BankDetailOrganizationId = table.Column<long>(type: "bigint", nullable: false),
                    OrgBankName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OrgBankBrachName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OrgBankAccountNumber = table.Column<int>(type: "int", nullable: true),
                    OrgBankAccountHolderName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OrgBankIFSC_Code = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    OrgBankQrCode_path = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankDetailsOrganization", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BankDetailsOrganization_AbpOrganizationUnits_BankDetailOrganizationId",
                        column: x => x.BankDetailOrganizationId,
                        principalTable: "AbpOrganizationUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PriceMultiAreaType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PriceID = table.Column<int>(type: "int", nullable: false),
                    PriceAreaTypeMulti = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceMultiAreaType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PriceMultiAreaType_Prices_PriceID",
                        column: x => x.PriceID,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankDetailsOrganization_BankDetailOrganizationId",
                table: "BankDetailsOrganization",
                column: "BankDetailOrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_PriceMultiAreaType_PriceID",
                table: "PriceMultiAreaType",
                column: "PriceID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankDetailsOrganization");

            migrationBuilder.DropTable(
                name: "PriceMultiAreaType");

            migrationBuilder.DropColumn(
                name: "DepositeRequired",
                table: "QuotationDataDetail");

            migrationBuilder.DropColumn(
                name: "Organization_Website",
                table: "AbpOrganizationUnits");
        }
    }
}
