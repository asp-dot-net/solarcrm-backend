﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Table_added_MisReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MISReport",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProjectName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ApplicationNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GUVNLRegistrationNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Lat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Long = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PVCapacity = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DiscomName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConsumerNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TariffCategory = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Circle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DivisionZone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Subdivision = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SanctionedContractLoad = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhaseofproposedSolarInverter = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Category = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConsumerEmail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConsumerMobile = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NamePrefix = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MiddleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LandlineNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StreetHouseNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Taluka = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    District = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CityVillage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Pin = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CommonMeter = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AadhaarNoEntered = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OTPVerifiedon = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SignedDocumentUploadedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastComment = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastCommentDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastCommentRepliedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DocumentVerifiedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    EstimateQuotationNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DisComEstimateAmount = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DueDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PaymentReceived = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PaymentMadeon = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SelfCertification = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CEIDrawingApplicationID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CEIDrawingApplicationApprovalDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BidirectionalMeterMake = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BidirectionalMeterNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SolarMeterMake = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SolarMeterNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateofInstallationofSolarMeter = table.Column<DateTime>(type: "datetime2", nullable: true),
                    AgreementSigningDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ApplicationStatus = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IntimationReceivedStatus = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IntimationApprovedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IntimationStatus = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PenaltyDaysPending = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PCRCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PCRSubmittedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    OrganizationUnitId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MISReport", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MISReport");
        }
    }
}
