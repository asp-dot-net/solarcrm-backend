﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_Field_Jobs_Table_loadReduceapp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ApplicationDate",
                table: "Jobs",
                newName: "SignApplicationDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "ApplicationSubmitedDate",
                table: "Jobs",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LoadReducePathApplication",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LoadReduceReasonApplication",
                table: "Jobs",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationSubmitedDate",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LoadReducePathApplication",
                table: "Jobs");

            migrationBuilder.DropColumn(
                name: "LoadReduceReasonApplication",
                table: "Jobs");

            migrationBuilder.RenameColumn(
                name: "SignApplicationDate",
                table: "Jobs",
                newName: "ApplicationDate");
        }
    }
}
