﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Update_Prices_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NoofPanels",
                table: "Prices");

            migrationBuilder.DropColumn(
                name: "SizeRange",
                table: "Prices");

            migrationBuilder.RenameColumn(
                name: "SystemPrice",
                table: "Prices",
                newName: "SystemSize");

            migrationBuilder.AlterColumn<decimal>(
                name: "Rate",
                table: "Prices",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SystemSize",
                table: "Prices",
                newName: "SystemPrice");

            migrationBuilder.AlterColumn<long>(
                name: "Rate",
                table: "Prices",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NoofPanels",
                table: "Prices",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SizeRange",
                table: "Prices",
                type: "decimal(18,2)",
                nullable: true);
        }
    }
}
