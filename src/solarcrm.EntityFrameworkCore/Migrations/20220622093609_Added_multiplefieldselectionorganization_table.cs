﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace solarcrm.Migrations
{
    public partial class Added_multiplefieldselectionorganization_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MultipleFieldOrganizationUnitSelection",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LeadSourceOrganizationId = table.Column<long>(type: "bigint", nullable: false),
                    LeadSourceId = table.Column<int>(type: "int", nullable: true),
                    StockItemOrganizationId = table.Column<long>(type: "bigint", nullable: false),
                    StockItemId = table.Column<int>(type: "int", nullable: true),
                    OrganizationUnitId = table.Column<long>(type: "bigint", nullable: false),
                    TenantId = table.Column<int>(type: "int", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatorUserId = table.Column<long>(type: "bigint", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastModifierUserId = table.Column<long>(type: "bigint", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeleterUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MultipleFieldOrganizationUnitSelection", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_LeadSourceId",
                        column: x => x.LeadSourceId,
                        principalTable: "LeadSources",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MultipleFieldOrganizationUnitSelection_LeadSources_StockItemId",
                        column: x => x.StockItemId,
                        principalTable: "LeadSources",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_LeadSourceId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "LeadSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_MultipleFieldOrganizationUnitSelection_StockItemId",
                table: "MultipleFieldOrganizationUnitSelection",
                column: "StockItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MultipleFieldOrganizationUnitSelection");
        }
    }
}
