﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.DataVaults;
using solarcrm.DataVaults.Leads;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace solarcrm.Job
{
    [Table("Jobs")]
    public class Jobs : FullAuditedEntity, IMustHaveTenant
    {      

        //[Required]
        public virtual int LeadId { get; set; }
        
        [ForeignKey("LeadId")]
        public virtual Leads LeadFK { get; set; }

        [StringLength(JobsConsts.JobNumber, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string JobNumber { get; set; }     
        public virtual string JobNotes { get; set; }

        [StringLength(JobsConsts.InstallerNotes, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string InstallerNotes { get; set; }

        [StringLength(JobsConsts.NotesForInstallationDepartment, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string NotesForInstallationDepartment { get; set; }

        public virtual decimal? ActualSystemCost { get; set; }

        public virtual decimal? NetSystemCost { get; set; }
        public virtual decimal? JobSubsidy20 { get; set; }
        public virtual decimal? JobSubsidy40 { get; set; }
        public virtual decimal? JobTotalSubsidyAmount { get; set; }
        
        public virtual decimal? DepositeAmount { get; set; }

        public virtual decimal? TotalJobCost { get; set; }

        [StringLength(JobsConsts.PhaseOfInverter, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string PhaseOfInverter { get; set; }   //PhaseofproposedSolarInverter

        public virtual decimal? PvCapacityKw { get; set; }    //PVCapacity

        [StringLength(JobsConsts.ApplicationNumber, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string ApplicationNumber { get; set; }

        [StringLength(JobsConsts.GUVNLRegisterationNo, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string GUVNLRegisterationNo { get; set; }
        public virtual DateTime? ApplicationDate { get; set; }  //OTPVerifiedon

        [StringLength(JobsConsts.EastimatePaymentRefNumber, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string EastimatePaymentRefNumber { get; set; }     //EstimatePaymentReferenceNo
       
        public virtual int? EastimateQuoteNumber { get; set; }     //EstimateQuotationNo

        public virtual decimal? EastimateQuoteAmount { get; set; }   //DisComEstimateAmount

        public virtual DateTime? EastimateDueDate { get; set; }    //DueDate

        public virtual DateTime? EastimatePayDate { get; set; }     //PaymentMadeon

        [StringLength(JobsConsts.EastimatePaidStatus, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string EastimatePaidStatus { get; set; }   //PaymentReceived

        [StringLength(JobsConsts.EastimatePaymentResponse, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string EastimatePaymentResponse { get; set; }     //EstimatePaymentResponse

        [StringLength(JobsConsts.EstimatePaymentReceiptPath, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string EstimatePaymentReceiptPath { get; set; }

        [StringLength(JobsConsts.OtpVerifed, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string OtpVerifed { get; set; }
        public virtual DateTime? SignApplicationDate { get; set; }

        [StringLength(JobsConsts.SignApplicationPath, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string SignApplicationPath { get; set; }

        [StringLength(JobsConsts.LoadReduceReasonApplication, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string LoadReduceReasonApplication { get; set; }

        [StringLength(JobsConsts.LoadReducePathApplication, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string LoadReducePathApplication { get; set; }

        public virtual int? ApplicationStatusId { get; set; }
        [ForeignKey("ApplicationStatusId")]
        public virtual ApplicationStatus ApplicationStatusIdFK { get; set; }
        public virtual decimal? SanctionedContractLoad  { get; set; }

        [StringLength(JobsConsts.LastComment, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string LastComment { get; set; }    //queryDescription

        public virtual DateTime? LastCommentDate { get; set; }  //QueryRaisedOn

        public virtual DateTime? LastCommentRepliedDate { get; set; }  //QueryRepliedOn

        public virtual DateTime? DocumentVerifiedDate { get; set; }   //DocumentVerifiedOn

        public virtual int? PenaltyDaysPending { get; set; }    //PenaltyDays

        public virtual int JobLoanOption { get; set; }   // 0 = Regular Customer , 1 = Loan Customer

        public virtual DateTime? FirstDepositRecivedDate { get; set; }
        public virtual DateTime? DepositRecivedDate { get; set; }

        [StringLength(JobsConsts.DepositNotes, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string DepositNotes { get; set; }

        public virtual int OrganizationUnitId { get; set; }

        public virtual int TenantId { get; set; }
    }
}
