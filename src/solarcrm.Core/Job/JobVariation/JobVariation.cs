﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.Variation;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.Job.JobVariation
{
    [Table("JobVariations")]
    public class JobVariation : FullAuditedEntity, IMustHaveTenant
	{
		public int TenantId { get; set; }
		
		public virtual decimal? Cost { get; set; }

		public virtual int? VariationId { get; set; }

		[ForeignKey("VariationId")]
		public Variation VariationFk { get; set; }

		public virtual int? JobId { get; set; }

		[ForeignKey("JobId")]
		public Jobs JobFk { get; set; }
	}
}
