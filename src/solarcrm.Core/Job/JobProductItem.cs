﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.StockItem;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.Job
{
    [Table("JobProductItems")]
    public class JobProductItem : FullAuditedEntity, IMustHaveTenant
    {
		public int TenantId { get; set; }

		public virtual int? Quantity { get; set; }

		public virtual int? JobId { get; set; }

		[ForeignKey("JobId")]
		public Job.Jobs JobFk { get; set; }

		public virtual int? ProductItemId { get; set; }

		[ForeignKey("ProductItemId")]
		public StockItem ProductItemFk { get; set; }
	}
}
