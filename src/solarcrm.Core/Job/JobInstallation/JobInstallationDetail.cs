﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Authorization.Users;
using solarcrm.DataVaults.Locations;
using solarcrm.DataVaults.StockItem;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.JobInstallation
{
    [Table("JobInstallationDetail")]
    public class JobInstallationDetail : FullAuditedEntity
    {
        //[Required]
        public virtual int JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Job.Jobs JobFKId { get; set; }

        public virtual DateTime? InstallStartDate { get; set; }

        public virtual int StoreLocationId { get; set; }

        [ForeignKey("StoreLocationId")]
        public virtual Locations LocationFKId { get; set; }

        public virtual long JobInstallerID { get; set; }

        //[ForeignKey("JobInstallerID")]
        //public virtual User UserFKId { get; set; }

        public virtual DateTime? InstallCompleteDate { get; set; }

        
    }
}
