﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.JobInstallation
{
    [Table("DiscomMeterInstallationDetail")]
    public class DiscomMeterInstallationDetail : FullAuditedEntity
    {        
        public virtual int JobInstallationId { get; set; }
        [ForeignKey("JobInstallationId")]
        public virtual JobInstallationDetail JobInstallationFKId { get; set; }

        public virtual DateTime? DiscomCompleteDate { get; set; }

        [StringLength(JobsConsts.DiscomMeterNo, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string DiscomMeterNo { get; set; }

        [StringLength(JobsConsts.SolarMeterNo, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string SolarMeterNo { get; set; }

        [StringLength(JobsConsts.DiscomBidirectionalMeterPath, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string DiscomBidirectionMeterInstallImgPath { get; set; }
    }
}
