﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.JobInstallation
{
    [Table("BiDirectionalCertificateDetail")]
    public class BidirectionalCertificateDetail : FullAuditedEntity
    {      
        public virtual int DiscomMeterInstallationId { get; set; }
        [ForeignKey("DiscomMeterInstallationId")]
        public virtual DiscomMeterInstallationDetail DiscomMeterInstallationFKId { get; set; }

        public virtual DateTime? BidirectionalCompleteDate { get; set; }

        [StringLength(JobsConsts.DiscomBidirectionalMeterPath, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string CertificateBidirectionMeterInstallImgPath { get; set; }
    }
}
