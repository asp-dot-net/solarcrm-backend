﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.DataVaults.PaymentType;
using solarcrm.DataVaults.RefundReasons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Job.JobRefunds
{
    [Table("JobRefunds")]
    public class JobRefunds : FullAuditedEntity
    {
        public virtual int RefundJobId { get; set; }
        [ForeignKey("RefundJobId")]
        public virtual Jobs JobIDFK { get; set; }

        public virtual int RefundReasonId { get; set; }
        [ForeignKey("RefundReasonId")]
        public virtual RefundReasons RefundResIDFK { get; set; }

        public virtual int PaymentTypeId { get; set; }
        [ForeignKey("PaymentTypeId")]
        public virtual PaymentType PaymentTypeIDFK { get; set; }
        public virtual decimal RefundPaidAmount { get; set; }
        public virtual DateTime? RefundPaidDate { get; set; }
        
        [StringLength(JobRefundConsts.NotesLength, MinimumLength = JobRefundConsts.MinNameLength)]
        public virtual string RefundPaidNotes { get; set; }
        public virtual bool IsRefundVerify { get; set; }
        public virtual DateTime? RefundVerifyDate { get; set; }

        [StringLength(JobRefundConsts.NotesLength, MinimumLength = JobRefundConsts.MinNameLength)]
        public virtual string RefundVerifyNotes { get; set; }

        public virtual int? RefundVerifyById { get; set; }
        public virtual long? BankAccountNumber { get; set; }

        [StringLength(JobRefundConsts.BankAccountHolderNameLength, MinimumLength = JobRefundConsts.MinNameLength)]
        public virtual string BankAccountHolderName { get; set; }

        [StringLength(JobRefundConsts.BankAccountHolderNameLength, MinimumLength = JobRefundConsts.MinNameLength)]
        public virtual string BankBrachName { get; set; }

        [StringLength(JobRefundConsts.BankAccountHolderNameLength, MinimumLength = JobRefundConsts.MinNameLength)]
        public virtual string BankName { get; set; }

        [StringLength(JobRefundConsts.IFSCCodeLength, MinimumLength = JobRefundConsts.MinNameLength)]

        public virtual string IFSC_Code { get; set; }

        [StringLength(JobRefundConsts.RefundReceiptPathLength, MinimumLength = JobRefundConsts.MinNameLength)]
        public virtual string RefundRecPath { get; set; }
        [StringLength(JobRefundConsts.RefundReceiptNo, MinimumLength = JobRefundConsts.MinNameLength)]
        public virtual string ReceiptNo { get; set; }
        public virtual bool? JobRefundEmailSend { get; set; }
        public virtual DateTime? JobRefundEmailSendDate { get; set; }
        public virtual bool JobRefundSmsSend { get; set; }
        public virtual DateTime? JobRefundSmsSendDate { get; set; }
    }
}
