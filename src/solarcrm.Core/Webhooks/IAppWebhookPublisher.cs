﻿using System.Threading.Tasks;
using solarcrm.Authorization.Users;

namespace solarcrm.WebHooks
{
    public interface IAppWebhookPublisher
    {
        Task PublishTestWebhook();
    }
}
