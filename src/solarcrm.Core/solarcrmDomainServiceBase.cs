﻿using Abp.Domain.Services;

namespace solarcrm
{
    public abstract class solarcrmDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected solarcrmDomainServiceBase()
        {
            LocalizationSourceName = solarcrmConsts.LocalizationSourceName;
        }
    }
}
