﻿using Microsoft.Extensions.Configuration;

namespace solarcrm.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
