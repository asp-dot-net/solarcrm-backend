﻿using System.Threading.Tasks;
using Abp.Dependency;
using Castle.Core.Logging;
using Org.BouncyCastle.Asn1.Crmf;
using RestSharp;
namespace solarcrm.Net.Sms
{
    public class SmsSender : ISmsSender, ITransientDependency
    {
        public ILogger Logger { get; set; }

        public SmsSender()
        {
            Logger = NullLogger.Instance;
        }

        public Task SendAsync(string number, string message)
        {
            /* Implement this service to send SMS to users (can be used for two factor auth). */

            Logger.Warn("Sending SMS is not implemented! Message content:");
            Logger.Warn("Number  : " + number);
            Logger.Warn("Message : " + message);

            return Task.FromResult(0);
        }
        public async Task SendSMSConfiure(string SMSBaseUrl, string SMS_AuthorizationKey,string Client_Id,string smsBody)
        {
            var baseUrl = SMSBaseUrl;// solarcrmConsts.SMSBaseUrl; //"https://insprl.com";
            var client = new RestClient(baseUrl);
            var request = new RestRequest("/api/sms/send-msg", Method.Post);
            var requesturl = new RestRequest("/api/url/get-short-url", Method.Post);
            RestResponse smsResponse = new RestResponse();

            requesturl.AddHeader("content-type", "application/x-www-form-urlencoded");
            requesturl.AddHeader("Authorization", SMS_AuthorizationKey); //"SyMTYj4X9&8CA*P6s1iDl^75"
            requesturl.AddParameter("client_id", Client_Id); //"Csprl_DP2AEBO9FQN4UKT"
            requesturl.AddParameter("long_url", smsBody); //"APSOLR"
            requesturl.AddParameter("tag_type", "random");
            requesturl.AddParameter("tag_keyword", "");//"Dear [Pravin test], Greetings from Australian Premium Solar (India) Pvt. Ltd. Your application for solar rooftop system is successfully registered with us. Thank you for choosing APS. Feel free to call on [07966006600] for any query. Regards, Team APS");
            requesturl.AddParameter("domain", "");  //"txn"
            requesturl.AddParameter("expiry_date", "");
            smsResponse = await client.ExecuteAsync(requesturl);
           // return Task.FromResult(0);
        }
    }
}
