﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;


namespace solarcrm.Sections
{
    [Table("Sections")]
    public class Section : FullAuditedEntity
    {
        public virtual int SectionId { get; set; }

        public virtual string SectionName { get; set; }
    }
}
