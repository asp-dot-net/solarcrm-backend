namespace solarcrm.MultiTenancy
{
    public enum EndSubscriptionResult
    {
        TenantSetInActive,
        AssignedToAnotherEdition
    }
}