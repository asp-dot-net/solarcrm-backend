﻿using Abp.Events.Bus;

namespace solarcrm.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}