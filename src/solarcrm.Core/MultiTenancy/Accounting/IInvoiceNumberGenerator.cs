﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace solarcrm.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}