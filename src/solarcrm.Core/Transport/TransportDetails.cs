﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.DataVaults.Vehical;
using solarcrm.PickList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.Transport
{
    [Table("TransportDetails")]
    public class TransportDetails : FullAuditedEntity
    {
        public virtual int TransportJobId { get; set; }
        [ForeignKey("TransportJobId")]
        public virtual Job.Jobs TransportJobsIdFK { get; set; }

        public virtual int? VehicalId { get; set; }
        [ForeignKey("VehicalId")]
        public virtual Vehical VehicalIdFK { get; set; }

        [StringLength(TransportConsts.Name, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string DriverName { get; set; }

        public virtual DateTime? TransportDate { get; set; }

        [StringLength(TransportConsts.TransportNotes, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string TransportNote { get; set; }

        
    }
}
