﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.DataVaults.Discom;
using solarcrm.DataVaults.Division;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.Subsidy
{
    [Table("SubsidyClaimDetails")]
    public class SubsidyClaimDetails : FullAuditedEntity
    {
        //public virtual int MeterConnectId { get; set; }
        //[ForeignKey("MeterConnectId")]
        //public virtual MeterConnectDetails MeterConnectFKId { get; set; }

        //public virtual int SubsidyDiscomId { get; set; }
        //[ForeignKey("SubsidyDiscomId")]
        //public virtual Discom SubsidyDiscomFKId { get; set; }
        
        public virtual int? SubsidyDivisionId { get; set; }
        [ForeignKey("SubsidyDivisionId")]
        public virtual Division SubsidyDivisionFKId { get; set; }

        [StringLength(SubsidyClaimConsts.SubsidyClaimNo, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string SubsidyClaimNo { get; set; }

        [StringLength(SubsidyClaimConsts.SubsidyClaimNotes, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string SubsidyNote { get; set; }
        public virtual DateTime? SubcidyClaimDate { get; set; }       
        public virtual bool IsSubcidyFileVerify { get; set; }
        public virtual  bool IsSubcidyReceived { get; set; }

        [StringLength(SubsidyClaimConsts.OCCopyPath, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string OCCopyPath { get; set; }
        public virtual DateTime? ClaimFileSubmissionDate { get;set; }

        [StringLength(SubsidyClaimConsts.SubcidyReceiptsPath, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string SubcidyReceiptsPath { get; set; }
        public virtual int OrganizationUnitId { get; set; }
        public virtual int TenantId { get; set; }
    }
}
