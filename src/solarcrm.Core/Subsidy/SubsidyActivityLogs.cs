﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.DataVaults.LeadAction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace solarcrm.Subsidy
{
    [Table("SubsidyActivityLogs")]
    public class SubsidyActivityLogs : FullAuditedEntity
    {
        public virtual int SubsidyId { get; set; }

        [ForeignKey("SubsidyId")]
        public SubsidyClaimFileUploadLists SubsidyReportIdFk { get; set; }

        [StringLength(SubsidyClaimConsts.SubsidyProjectNumber, MinimumLength = JobsConsts.MinNameLength)]
        public string SubsidyProjectName { get; set; }

        public virtual int SubsidyLeadActionId { get; set; }

        [ForeignKey("SubsidyLeadActionId")]
        public LeadAction LeadActionIdFk { get; set; }

        public virtual int? SectionId { get; set; }
        [ForeignKey("SectionId")]
        public Sections.Section SectionIdFk { get; set; }

        [StringLength(ActivityLogConsts.MaxNameLength, MinimumLength = ActivityLogConsts.MinNameLength)]
        public virtual string SubsidyActionNote { get; set; }

        public virtual int? TenantId { get; set; }
        public virtual int? OrganizationUnitId { get; set; }

    
}
}
