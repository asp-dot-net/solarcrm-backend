﻿using Abp.Domain.Entities.Auditing;
using Castle.Core.Internal;
using solarcrm.DataVaults.Division;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Subsidy
{
    //[Table("SubsidyClaimPaymentDetails")]
    public class SubsidyClaimPaymentDetails : FullAuditedEntity
    {
        public virtual int SubsidyClaimId { get; set; }
        [ForeignKey("SubsidyClaimId")]
        public virtual SubsidyClaimDetails SubsidyClaimFKId { get; set; }

        public virtual int? TotalApplications { get; set; }
        public virtual decimal? ClaimRegisterCapacityKW { get; set; }
        public virtual decimal? ClaimInstalledPVModuleCapacityKW { get;set; }
        public virtual decimal? ClaimInstalledInverterCapacityKW { get; set; }
        public virtual decimal? ClaimTotalProjectCost { get;set; }
        public virtual decimal? ClaimTotalSubsidyAmount { get; set; }
        public virtual decimal? ClaimPBGDeduction { get; set; }
        public virtual decimal? ClaimPenaltyDeduction { get; set; }
        public virtual decimal? ClaimAnyotherduesdeduction { get; set; }
        public virtual decimal? ClaimNetSubsidyPayable { get; set; }

    }
}
