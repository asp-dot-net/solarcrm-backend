﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.MeterConnect;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Subsidy
{
    [Table("SubsidyClaimFileUploadLists")]
    public class SubsidyClaimFileUploadLists : FullAuditedEntity
    {
        [StringLength(SubsidyClaimConsts.FileName, MinimumLength = JobsConsts.MinNameLength)]
        public string SubsidyFileName { get; set; }

        [StringLength(SubsidyClaimConsts.FilePath, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string SubsidyFilePath { get; set; }

        [StringLength(SubsidyClaimConsts.FileStatus, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string SubsidyFileStatus { get; set; }
        public int SubsidyTenantId { get; set; }
        public virtual int? SubsidyOrganizationUnitId { get; set; }        
    }
}
