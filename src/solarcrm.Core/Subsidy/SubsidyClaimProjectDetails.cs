﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.Division;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Subsidy
{
    [Table("SubsidyClaimProjectDetails")]
    public class SubsidyClaimProjectDetails : FullAuditedEntity
    {
        public virtual int SubsidyJobId { get; set; }
        [ForeignKey("SubsidyJobId")]
        public virtual Job.Jobs SubsidyJobFKId { get; set; }

        public virtual int SubsidyClaimId { get; set; }
        [ForeignKey("SubsidyClaimId")]
        public virtual SubsidyClaimDetails SubsidyClaimFKId { get; set; }

    }
}
