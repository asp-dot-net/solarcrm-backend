﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using Abp.Localization;
using Abp.Notifications;
using solarcrm.Authorization.Users;
using solarcrm.MultiTenancy;

namespace solarcrm.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task GdprDataPrepared(UserIdentifier user, Guid binaryObjectId);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);

        Task SendMessageAsync(UserIdentifier user, LocalizableString localizableMessage, IDictionary<string, object> localizableMessageData = null, NotificationSeverity severity = NotificationSeverity.Info);

        Task TenantsMovedToEdition(UserIdentifier user, string sourceEditionName, string targetEditionName);

        Task SomeUsersCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName);

        //For StockItems
        Task SomeStockItemCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName);

        Task SomeMISReportCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName);

        Task SomeSubsidyClaimReportCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName);
        Task NewLead(User user, string message, NotificationSeverity notificationSeverity);
        Task LeadAssiged(User user, string message, NotificationSeverity notificationSeverity);

        Task LeadComment(User user, string message, NotificationSeverity notificationSeverity);

        Task SomeLeadsCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName);

        Task SomePriceCouldntBeImported(UserIdentifier user, string fileToken, string fileType, string fileName);
    }
}
