﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Quotation
{
    [Table("QuotationData")]
    public class QuotationData : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int QuoteJobId { get; set; }

        [ForeignKey("QuoteJobId")]
        public virtual Job.Jobs JobsIdFK { get; set; }

        [StringLength(QuotationConsts.QuotationNumber, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string QuotationNumber { get; set; }

        public virtual DateTime? QuotationGenerateDate { get; set; }

        [StringLength(QuotationConsts.QuoteSignFileName, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string QuotationSignedFileName { get; set; }

        [StringLength(QuotationConsts.QuoteSignFilePath, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string QuotationSignedImagePath { get; set; }

       
        public virtual decimal? QuotationSignLongitude { get; set; }

        public virtual decimal? QuotationSignLatitude { get; set; }

        [StringLength(QuotationConsts.QuoteSignSignIP, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string QuotationSignedIP { get; set; }
        public virtual bool IsQuotateSigned { get; set; }

        public virtual DateTime? QuotationSignedDate { get; set; }
        public virtual string QuotationNotes { get; set; }
        public virtual int OrganizationUnitId { get; set; }

        public virtual int TenantId { get; set; }

        public virtual int QuoteMode { get; set; } // 0 = Regular Quote , 1 = Loan Quote

    }
}
