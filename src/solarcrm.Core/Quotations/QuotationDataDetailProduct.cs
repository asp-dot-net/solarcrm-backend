﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Quotation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Job.Quotation
{
    [Table("QuotationDataDetailProduct")]
    public class QuotationDataDetailProduct : FullAuditedEntity 
    {
        public virtual int QuotationDetailId { get; set; }
        [ForeignKey("QuotationDetailId")]
        public virtual QuotationDataDetails QuoteDataDetailIdFK { get; set; }

        public virtual string QuoteProductType { get; set; }

        public virtual string QuoteProductName { get; set; }

        public virtual string QuoteProductModelNumber { get; set; }

        public virtual string QuoteProductQuantity { get; set; }

    
    }
}
