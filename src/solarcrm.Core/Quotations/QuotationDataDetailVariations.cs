﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Quotation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Job.Quotation
{
    [Table("QuotationDataDetailVariations")]
    public class QuotationDataDetailVariations : FullAuditedEntity
    {
        public virtual int QuotationDetailId { get; set; }
        [ForeignKey("QuotationDetailId")]
        public virtual QuotationDataDetails QuoteDetailIdFK { get; set; }

        public  virtual string QuoteVarationName { get; set; }

        public virtual decimal? QuoteVarationCost { get; set; }      
    }
}
