﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.Quotation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Job.Quotation
{
    [Table("QuotationDataDetail")]
    public class QuotationDataDetails : FullAuditedEntity
    {
        public virtual int QuoteDataId { get; set; }

        [ForeignKey("QuoteDataId")]
        public virtual QuotationData QuoteDataIdFK { get; set; }

        [StringLength(QuotationConsts.CustomerName, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string CustomerName { get; set; }

        [StringLength(QuotationConsts.Address, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string Address1 { get; set; }

        [StringLength(QuotationConsts.Address, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string Address2 { get; set; }

        [StringLength(QuotationConsts.Mobile, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string Mobile { get; set; }

        [StringLength(QuotationConsts.Email, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string Email { get; set; }

        [StringLength(QuotationConsts.SolarAdvisiorName, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string SolarAdvisorName { get; set; }      //Lead Assign User Name

        [StringLength(QuotationConsts.Mobile, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string SolarAdvisorMobile { get; set; }   //Lead Assign User Mobile Number

        [StringLength(QuotationConsts.Email, MinimumLength = QuotationConsts.MinNameLength)]
        public virtual string SolarAdvisorEmail { get; set; }    //Lead Assign User Email

        public virtual string PvCapacityKw { get; set; }    //Syatem Size or System Cost

        public virtual decimal? ActualSystemCost { get; set; }    // Actual Price

        public virtual decimal? Subcidy40 { get; set; }

        public virtual decimal? Subcidy20 { get; set; }

       // public virtual decimal? SubcidyTotal { get; set; }

        public virtual decimal? BillToPay { get; set; }

        public virtual decimal? DepositeRequired { get; set; }
        public virtual string DiscomName { get; set; }
        public virtual string HeightOfStructure { get; set; }

        public virtual string DivisionName { get; set; } 
     
    }
}
