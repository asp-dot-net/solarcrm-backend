﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Quotations
{
	[Table("QuotationLinkHistorys")]
	public class QuotationLinkHistory : FullAuditedEntity
	{
		public virtual int? QuotationId { get; set; }
		[ForeignKey("QuotationId")]
		public virtual Quotation.QuotationData QuotationIdFK { get; set; }	

		public virtual string Token { get; set; }
		
		public virtual bool? Expired { get; set; }
	}
}
