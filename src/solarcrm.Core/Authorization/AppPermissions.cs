﻿namespace solarcrm.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";
        public const string Pages_Administration_Languages_ChangeDefaultLanguage = "Pages.Administration.Languages.ChangeDefaultLanguage";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
        public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
        public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
        public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
        public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
        public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
        public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

        public const string Pages_Administration_DynamicProperties = "Pages.Administration.DynamicProperties";
        public const string Pages_Administration_DynamicProperties_Create = "Pages.Administration.DynamicProperties.Create";
        public const string Pages_Administration_DynamicProperties_Edit = "Pages.Administration.DynamicProperties.Edit";
        public const string Pages_Administration_DynamicProperties_Delete = "Pages.Administration.DynamicProperties.Delete";

        public const string Pages_Administration_DynamicPropertyValue = "Pages.Administration.DynamicPropertyValue";
        public const string Pages_Administration_DynamicPropertyValue_Create = "Pages.Administration.DynamicPropertyValue.Create";
        public const string Pages_Administration_DynamicPropertyValue_Edit = "Pages.Administration.DynamicPropertyValue.Edit";
        public const string Pages_Administration_DynamicPropertyValue_Delete = "Pages.Administration.DynamicPropertyValue.Delete";

        public const string Pages_Administration_DynamicEntityProperties = "Pages.Administration.DynamicEntityProperties";
        public const string Pages_Administration_DynamicEntityProperties_Create = "Pages.Administration.DynamicEntityProperties.Create";
        public const string Pages_Administration_DynamicEntityProperties_Edit = "Pages.Administration.DynamicEntityProperties.Edit";
        public const string Pages_Administration_DynamicEntityProperties_Delete = "Pages.Administration.DynamicEntityProperties.Delete";

        public const string Pages_Administration_DynamicEntityPropertyValue = "Pages.Administration.DynamicEntityPropertyValue";
        public const string Pages_Administration_DynamicEntityPropertyValue_Create = "Pages.Administration.DynamicEntityPropertyValue.Create";
        public const string Pages_Administration_DynamicEntityPropertyValue_Edit = "Pages.Administration.DynamicEntityPropertyValue.Edit";
        public const string Pages_Administration_DynamicEntityPropertyValue_Delete = "Pages.Administration.DynamicEntityPropertyValue.Delete";
        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

        //TENANT-SPECIFIC PERMISSIONS -- Manual
        public const string Pages_Tenant_DataVaults = "Pages.Tenant.DataVaults";

        //Locations Permissions
        public const string Pages_Tenant_DataVaults_Locations = "Pages.Tenant.DataVaults.Locations";
        public const string Pages_Tenant_DataVaults_Locations_Create = "Pages.Tenant.DataVaults.Locations.Create";
        public const string Pages_Tenant_DataVaults_Locations_Edit = "Pages.Tenant.DataVaults.Locations.Edit";
        public const string Pages_Tenant_DataVaults_Locations_Delete = "Pages.Tenant.DataVaults.Locations.Delete";
        public const string Pages_Tenant_DataVaults_Locations_ExportToExcel = "Pages.Tenant.DataVaults.Locations.ExportToExcel";

        //Lead Source Permissions
        public const string Pages_Tenant_DataVaults_LeadSource = "Pages.Tenant.DataVaults.LeadSource";
        public const string Pages_Tenant_DataVaults_LeadSource_Create = "Pages.Tenant.DataVaults.LeadSource.Create";
        public const string Pages_Tenant_DataVaults_LeadSource_Edit = "Pages.Tenant.DataVaults.LeadSource.Edit";
        public const string Pages_Tenant_DataVaults_LeadSource_Delete = "Pages.Tenant.DataVaults.LeadSource.Delete";
        public const string Pages_Tenant_DataVaults_LeadSource_ExportToExcel = "Pages.Tenant.DataVaults.LeadSource.ExportToExcel";

        //Lead Type Permissions
        public const string Pages_Tenant_DataVaults_LeadType = "Pages.Tenant.DataVaults.LeadType";
        public const string Pages_Tenant_DataVaults_LeadType_Create = "Pages.Tenant.DataVaults.LeadType.Create";
        public const string Pages_Tenant_DataVaults_LeadType_Edit = "Pages.Tenant.DataVaults.LeadType.Edit";
        public const string Pages_Tenant_DataVaults_LeadType_Delete = "Pages.Tenant.DataVaults.LeadType.Delete";
        public const string Pages_Tenant_DataVaults_LeadType_ExportToExcel = "Pages.Tenant.DataVaults.LeadType.ExportToExcel";

        //Document List Permissions
        public const string Pages_Tenant_DataVaults_DocumentList = "Pages.Tenant.DataVaults.DocumentList";
        public const string Pages_Tenant_DataVaults_DocumentList_Create = "Pages.Tenant.DataVaults.DocumentList.Create";
        public const string Pages_Tenant_DataVaults_DocumentList_Edit = "Pages.Tenant.DataVaults.DocumentList.Edit";
        public const string Pages_Tenant_DataVaults_DocumentList_Delete = "Pages.Tenant.DataVaults.DocumentList.Delete";
        public const string Pages_Tenant_DataVaults_DocumentList_ExportToExcel = "Pages.Tenant.DataVaults.DocumentList.ExportToExcel";

        //Teams Permissions
        public const string Pages_Tenant_DataVaults_Teams = "Pages.Tenant.DataVaults.Teams";
        public const string Pages_Tenant_DataVaults_Teams_Create = "Pages.Tenant.DataVaults.Teams.Create";
        public const string Pages_Tenant_DataVaults_Teams_Edit = "Pages.Tenant.DataVaults.Teams.Edit";
        public const string Pages_Tenant_DataVaults_Teams_Delete = "Pages.Tenant.DataVaults.Teams.Delete";
        public const string Pages_Tenant_DataVaults_Teams_ExportToExcel = "Pages.Tenant.DataVaults.Teams.ExportToExcel";

        //Discom Permissions
        public const string Pages_Tenant_DataVaults_Discom = "Pages.Tenant.DataVaults.Discom";
        public const string Pages_Tenant_DataVaults_Discom_Create = "Pages.Tenant.DataVaults.Discom.Create";
        public const string Pages_Tenant_DataVaults_Discom_Edit = "Pages.Tenant.DataVaults.Discom.Edit";
        public const string Pages_Tenant_DataVaults_Discom_Delete = "Pages.Tenant.DataVaults.Discom.Delete";
        public const string Pages_Tenant_DataVaults_Discom_ExportToExcel = "Pages.Tenant.DataVaults.Discom.ExportToExcel";

        //Division Permissions
        public const string Pages_Tenant_DataVaults_Division = "Pages.Tenant.DataVaults.Division";
        public const string Pages_Tenant_DataVaults_Division_Create = "Pages.Tenant.DataVaults.Division.Create";
        public const string Pages_Tenant_DataVaults_Division_Edit = "Pages.Tenant.DataVaults.Division.Edit";
        public const string Pages_Tenant_DataVaults_Division_Delete = "Pages.Tenant.DataVaults.Division.Delete";
        public const string Pages_Tenant_DataVaults_Division_ExportToExcel = "Pages.Tenant.DataVaults.Division.ExportToExcel";

        //Sub Division Permissions
        public const string Pages_Tenant_DataVaults_SubDivision = "Pages.Tenant.DataVaults.SubDivision";
        public const string Pages_Tenant_DataVaults_SubDivision_Create = "Pages.Tenant.DataVaults.SubDivision.Create";
        public const string Pages_Tenant_DataVaults_SubDivision_Edit = "Pages.Tenant.DataVaults.SubDivision.Edit";
        public const string Pages_Tenant_DataVaults_SubDivision_Delete = "Pages.Tenant.DataVaults.SubDivision.Delete";
        public const string Pages_Tenant_DataVaults_SubDivision_ExportToExcel = "Pages.Tenant.DataVaults.SubDivision.ExportToExcel";

        //Transaction Type Permissions
        public const string Pages_Tenant_DataVaults_TransactionType = "Pages.Tenant.DataVaults.TransactionType";
        public const string Pages_Tenant_DataVaults_TransactionType_Create = "Pages.Tenant.DataVaults.TransactionType.Create";
        public const string Pages_Tenant_DataVaults_TransactionType_Edit = "Pages.Tenant.DataVaults.TransactionType.Edit";
        public const string Pages_Tenant_DataVaults_TransactionType_Delete = "Pages.Tenant.DataVaults.TransactionType.Delete";
        public const string Pages_Tenant_DataVaults_TransactionType_ExportToExcel = "Pages.Tenant.DataVaults.TransactionType.ExportToExcel";

        //Stock Item Permissions
        public const string Pages_Tenant_DataVaults_StockItem = "Pages.Tenant.DataVaults.StockItem";
        public const string Pages_Tenant_DataVaults_StockItem_Create = "Pages.Tenant.DataVaults.StockItem.Create";
        public const string Pages_Tenant_DataVaults_StockItem_Edit = "Pages.Tenant.DataVaults.StockItem.Edit";
        public const string Pages_Tenant_DataVaults_StockItem_Delete = "Pages.Tenant.DataVaults.StockItem.Delete";
        public const string Pages_Tenant_DataVaults_StockItem_ExportToExcel = "Pages.Tenant.DataVaults.StockItem.ExportToExcel";
        public const string Pages_Tenant_DataVaults_StockItem_ImportToExcel = "Pages.Tenant.DataVaults.StockItem.ImportToExcel";

        //ProjectType Permissions
        public const string Pages_Tenant_DataVaults_ProjectType = "Pages.Tenant.DataVaults.ProjectType";
        public const string Pages_Tenant_DataVaults_ProjectType_Create = "Pages.Tenant.DataVaults.ProjectType.Create";
        public const string Pages_Tenant_DataVaults_ProjectType_Edit = "Pages.Tenant.DataVaults.ProjectType.Edit";
        public const string Pages_Tenant_DataVaults_ProjectType_Delete = "Pages.Tenant.DataVaults.ProjectType.Delete";
        public const string Pages_Tenant_DataVaults_ProjectType_ExportToExcel = "Pages.Tenant.DataVaults.ProjectType.ExportToExcel";

        //ProjectStatus Permissions
        public const string Pages_Tenant_DataVaults_ProjectStatus = "Pages.Tenant.DataVaults.ProjectStatus";
        public const string Pages_Tenant_DataVaults_ProjectStatus_Create = "Pages.Tenant.DataVaults.ProjectStatus.Create";
        public const string Pages_Tenant_DataVaults_ProjectStatus_Edit = "Pages.Tenant.DataVaults.ProjectStatus.Edit";
        public const string Pages_Tenant_DataVaults_ProjectStatus_Delete = "Pages.Tenant.DataVaults.ProjectStatus.Delete";
        public const string Pages_Tenant_DataVaults_ProjectStatus_ExportToExcel = "Pages.Tenant.DataVaults.ProjectStatus.ExportToExcel";

        //StockCategory  Permissions
        public const string Pages_Tenant_DataVaults_StockCategory = "Pages.Tenant.DataVaults.StockCategory";
        public const string Pages_Tenant_DataVaults_StockCategory_Create = "Pages.Tenant.DataVaults.StockCategory.Create";
        public const string Pages_Tenant_DataVaults_StockCategory_Edit = "Pages.Tenant.DataVaults.StockCategory.Edit";
        public const string Pages_Tenant_DataVaults_StockCategory_Delete = "Pages.Tenant.DataVaults.StockCategory.Delete";
        public const string Pages_Tenant_DataVaults_StockCategory_ExportToExcel = "Pages.Tenant.DataVaults.StockCategory.ExportToExcel";

        //InvoiceType  Permissions
        public const string Pages_Tenant_DataVaults_InvoiceType = "Pages.Tenant.DataVaults.InvoiceType";
        public const string Pages_Tenant_DataVaults_InvoiceType_Create = "Pages.Tenant.DataVaults.InvoiceType.Create";
        public const string Pages_Tenant_DataVaults_InvoiceType_Edit = "Pages.Tenant.DataVaults.InvoiceType.Edit";
        public const string Pages_Tenant_DataVaults_InvoiceType_Delete = "Pages.Tenant.DataVaults.InvoiceType.Delete";
        public const string Pages_Tenant_DataVaults_InvoiceType_ExportToExcel = "Pages.Tenant.DataVaults.InvoiceType.ExportToExcel";

        //State
        public const string Pages_Tenant_DataVaults_State = "Pages.Tenant.DataVaults.State";
        public const string Pages_Tenant_DataVaults_State_Create = "Pages.Tenant.DataVaults.State.Create";
        public const string Pages_Tenant_DataVaults_State_Edit = "Pages.Tenant.DataVaults.State.Edit";
        public const string Pages_Tenant_DataVaults_State_Delete = "Pages.Tenant.DataVaults.State.Delete";
        public const string Pages_Tenant_DataVaults_State_ExportToExcel = "Pages.Tenant.DataVaults.State.ExportToExcel";

        //District
        public const string Pages_Tenant_DataVaults_District = "Pages.Tenant.DataVaults.District";
        public const string Pages_Tenant_DataVaults_District_Create = "Pages.Tenant.DataVaults.District.Create";
        public const string Pages_Tenant_DataVaults_District_Edit = "Pages.Tenant.DataVaults.District.Edit";
        public const string Pages_Tenant_DataVaults_District_Delete = "Pages.Tenant.DataVaults.District.Delete";
        public const string Pages_Tenant_DataVaults_District_ExportToExcel = "Pages.Tenant.DataVaults.District.ExportToExcel";

        //Taluka
        public const string Pages_Tenant_DataVaults_Taluka = "Pages.Tenant.DataVaults.Taluka";
        public const string Pages_Tenant_DataVaults_Taluka_Create = "Pages.Tenant.DataVaults.Taluka.Create";
        public const string Pages_Tenant_DataVaults_Taluka_Edit = "Pages.Tenant.DataVaults.Taluka.Edit";
        public const string Pages_Tenant_DataVaults_Taluka_Delete = "Pages.Tenant.DataVaults.Taluka.Delete";
        public const string Pages_Tenant_DataVaults_Taluka_ExportToExcel = "Pages.Tenant.DataVaults.Taluka.ExportToExcel";

        //City
        public const string Pages_Tenant_DataVaults_City = "Pages.Tenant.DataVaults.City";
        public const string Pages_Tenant_DataVaults_City_Create = "Pages.Tenant.DataVaults.City.Create";
        public const string Pages_Tenant_DataVaults_City_Edit = "Pages.Tenant.DataVaults.City.Edit";
        public const string Pages_Tenant_DataVaults_City_Delete = "Pages.Tenant.DataVaults.City.Delete";
        public const string Pages_Tenant_DataVaults_City_ExportToExcel = "Pages.Tenant.DataVaults.City.ExportToExcel";

        //CancelReasons
        public const string Pages_Tenant_DataVaults_CancelReasons = "Pages.Tenant.DataVaults.CancelReasons";
        public const string Pages_Tenant_DataVaults_CancelReasons_Create = "Pages.Tenant.DataVaults.CancelReasons.Create";
        public const string Pages_Tenant_DataVaults_CancelReasons_Edit = "Pages.Tenant.DataVaults.CancelReasons.Edit";
        public const string Pages_Tenant_DataVaults_CancelReasons_Delete = "Pages.Tenant.DataVaults.CancelReasons.Delete";
        public const string Pages_Tenant_DataVaults_CancelReasons_ExportToExcel = "Pages.Tenant.DataVaults.CancelReasons.ExportToExcel";

        //HoldReasons
        public const string Pages_Tenant_DataVaults_HoldReasons = "Pages.Tenant.DataVaults.HoldReasons";
        public const string Pages_Tenant_DataVaults_HoldReasons_Create = "Pages.Tenant.DataVaults.HoldReasons.Create";
        public const string Pages_Tenant_DataVaults_HoldReasons_Edit = "Pages.Tenant.DataVaults.HoldReasons.Edit";
        public const string Pages_Tenant_DataVaults_HoldReasons_Delete = "Pages.Tenant.DataVaults.HoldReasons.Delete";
        public const string Pages_Tenant_DataVaults_HoldReasons_ExportToExcel = "Pages.Tenant.DataVaults.HoldReasons.ExportToExcel";

        //RejectReasons
        public const string Pages_Tenant_DataVaults_RejectReasons = "Pages.Tenant.DataVaults.RejectReasons";
        public const string Pages_Tenant_DataVaults_RejectReasons_Create = "Pages.Tenant.DataVaults.RejectReasons.Create";
        public const string Pages_Tenant_DataVaults_RejectReasons_Edit = "Pages.Tenant.DataVaults.RejectReasons.Edit";
        public const string Pages_Tenant_DataVaults_RejectReasons_Delete = "Pages.Tenant.DataVaults.RejectReasons.Delete";
        public const string Pages_Tenant_DataVaults_RejectReasons_ExportToExcel = "Pages.Tenant.DataVaults.RejectReasons.ExportToExcel";

        //JobCancellationReason
        public const string Pages_Tenant_DataVaults_JobCancellationReason = "Pages.Tenant.DataVaults.JobCancellationReason";
        public const string Pages_Tenant_DataVaults_JobCancellationReason_Create = "Pages.Tenant.DataVaults.JobCancellationReason.Create";
        public const string Pages_Tenant_DataVaults_JobCancellationReason_Edit = "Pages.Tenant.DataVaults.JobCancellationReason.Edit";
        public const string Pages_Tenant_DataVaults_JobCancellationReason_Delete = "Pages.Tenant.DataVaults.JobCancellationReason.Delete";
        public const string Pages_Tenant_DataVaults_JobCancellationReason_ExportToExcel = "Pages.Tenant.DataVaults.JobCancellationReason.ExportToExcel";

        //DocumentLibrary
        public const string Pages_Tenant_DataVaults_DocumentLibrary = "Pages.Tenant.DataVaults.DocumentLibrary";
        public const string Pages_Tenant_DataVaults_DocumentLibrary_Create = "Pages.Tenant.DataVaults.DocumentLibrary.Create";
        public const string Pages_Tenant_DataVaults_DocumentLibrary_Edit = "Pages.Tenant.DataVaults.DocumentLibrary.Edit";
        public const string Pages_Tenant_DataVaults_DocumentLibrary_Delete = "Pages.Tenant.DataVaults.DocumentLibrary.Delete";
        public const string Pages_Tenant_DataVaults_DocumentLibrary_ExportToExcel = "Pages.Tenant.DataVaults.DocumentLibrary.ExportToExcel";

        //SmsTemplates
        public const string Pages_Tenant_DataVaults_SmsTemplates = "Pages.Tenant.DataVaults.SmsTemplates";
        public const string Pages_Tenant_DataVaults_SmsTemplates_Create = "Pages.Tenant.DataVaults.SmsTemplates.Create";
        public const string Pages_Tenant_DataVaults_SmsTemplates_Edit = "Pages.Tenant.DataVaults.SmsTemplates.Edit";
        public const string Pages_Tenant_DataVaults_SmsTemplates_Delete = "Pages.Tenant.DataVaults.SmsTemplates.Delete";
        public const string Pages_Tenant_DataVaults_SmsTemplates_View = "Pages.Tenant.DataVaults.SmsTemplates.View";
        public const string Pages_Tenant_DataVaults_SmsTemplates_ExportToExcel = "Pages.Tenant.DataVaults.SmsTemplates.ExportToExcel";      

        //EmailTemplates
        public const string Pages_Tenant_DataVaults_EmailTemplates = "Pages.Tenant.DataVaults.EmailTemplates";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Create = "Pages.Tenant.DataVaults.EmailTemplates.Create";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Edit = "Pages.Tenant.DataVaults.EmailTemplates.Edit";
        public const string Pages_Tenant_DataVaults_EmailTemplates_Delete = "Pages.Tenant.DataVaults.EmailTemplates.Delete";
        public const string Pages_Tenant_DataVaults_EmailTemplates_ExportToExcel = "Pages.Tenant.DataVaults.EmailTemplates.ExportToExcel";

       
        //SolarType
        public const string Pages_Tenant_DataVaults_SolarType = "Pages.Tenant.DataVaults.SolarType";
        public const string Pages_Tenant_DataVaults_SolarType_Create = "Pages.Tenant.DataVaults.SolarType.Create";
        public const string Pages_Tenant_DataVaults_SolarType_Edit = "Pages.Tenant.DataVaults.SolarType.Edit";
        public const string Pages_Tenant_DataVaults_SolarType_Delete = "Pages.Tenant.DataVaults.SolarType.Delete";
        public const string Pages_Tenant_DataVaults_SolarType_ExportToExcel = "Pages.Tenant.DataVaults.SolarType.ExportToExcel";

        //HeightStructure
        public const string Pages_Tenant_DataVaults_HeightStructure = "Pages.Tenant.DataVaults.HeightStructure";
        public const string Pages_Tenant_DataVaults_HeightStructure_Create = "Pages.Tenant.DataVaults.HeightStructure.Create";
        public const string Pages_Tenant_DataVaults_HeightStructure_Edit = "Pages.Tenant.DataVaults.HeightStructure.Edit";
        public const string Pages_Tenant_DataVaults_HeightStructure_Delete = "Pages.Tenant.DataVaults.HeightStructure.Delete";
        public const string Pages_Tenant_DataVaults_HeightStructure_ExportToExcel = "Pages.Tenant.DataVaults.HeightStructure.ExportToExcel";

        //LeadStatus
        public const string Pages_Tenant_DataVaults_LeadStatus = "Pages.Tenant.DataVaults.LeadStatus";
        public const string Pages_Tenant_DataVaults_LeadStatus_Create = "Pages.Tenant.DataVaults.LeadStatus.Create";
        public const string Pages_Tenant_DataVaults_LeadStatus_Edit = "Pages.Tenant.DataVaults.LeadStatus.Edit";
        public const string Pages_Tenant_DataVaults_LeadStatus_Delete = "Pages.Tenant.DataVaults.LeadStatus.Delete";
        public const string Pages_Tenant_DataVaults_LeadStatus_ExportToExcel = "Pages.Tenant.DataVaults.LeadStatus.ExportToExcel";

        //Tender
        public const string Pages_Tenant_DataVaults_Tender = "Pages.Tenant.DataVaults.Tender";
        public const string Pages_Tenant_DataVaults_Tender_Create = "Pages.Tenant.DataVaults.Tender.Create";
        public const string Pages_Tenant_DataVaults_Tender_Edit = "Pages.Tenant.DataVaults.Tender.Edit";
        public const string Pages_Tenant_DataVaults_Tender_Delete = "Pages.Tenant.DataVaults.Tender.Delete";
        public const string Pages_Tenant_DataVaults_Tender_ExportToExcel = "Pages.Tenant.DataVaults.Tender.ExportToExcel";

        //Price
        public const string Pages_Tenant_DataVaults_Prices = "Pages.Tenant.DataVaults.Prices";
        public const string Pages_Tenant_DataVaults_Prices_Create = "Pages.Tenant.DataVaults.Prices.Create";
        public const string Pages_Tenant_DataVaults_Prices_Edit = "Pages.Tenant.DataVaults.Prices.Edit";
        public const string Pages_Tenant_DataVaults_Prices_Delete = "Pages.Tenant.DataVaults.Prices.Delete";
        public const string Pages_Tenant_DataVaults_Prices_ExportToExcel = "Pages.Tenant.DataVaults.Prices.ExportToExcel";
        public const string Pages_Tenant_DataVaults_Prices_ImportToExcel = "Pages.Tenant.DataVaults.Prices.ImportToExcel";

        //Circle
        public const string Pages_Tenant_DataVaults_Circle = "Pages.Tenant.DataVaults.Circle";
        public const string Pages_Tenant_DataVaults_Circle_Create = "Pages.Tenant.DataVaults.Circle.Create";
        public const string Pages_Tenant_DataVaults_Circle_Edit = "Pages.Tenant.DataVaults.Circle.Edit";
        public const string Pages_Tenant_DataVaults_Circle_Delete = "Pages.Tenant.DataVaults.Circle.Delete";
        public const string Pages_Tenant_DataVaults_Circle_ExportToExcel = "Pages.Tenant.DataVaults.Circle.ExportToExcel";

        //ActivityLog Datavaults
        public const string Pages_Tenant_DataVaults_ActivityLogMasterVault = "Pages.Tenant.DataVaults.ActivityLogMasterVault";
        public const string Pages_Tenant_DataVaults_ActivityLogMasterVault_Create = "Pages.Tenant.DataVaults.ActivityLogMasterVault.Create";
        public const string Pages_Tenant_DataVaults_ActivityLogMasterVault_Edit = "Pages.Tenant.DataVaults.ActivityLogMasterVault.Edit";
        public const string Pages_Tenant_DataVaults_ActivityLogMasterVault_Delete = "Pages.Tenant.DataVaults.ActivityLogMasterVault.Delete";
        public const string Pages_Tenant_DataVaults_ActivityLogMasterVault_ExportToExcel = "Pages.Tenant.DataVaults.ActivityLogMasterVault.ExportToExcel";

        //LeadActivity
        public const string Pages_Tenant_DataVaults_LeadActivity = "Pages.Tenant.DataVaults.LeadActivity";
        public const string Pages_Tenant_DataVaults_LeadActivity_Create = "Pages.Tenant.DataVaults.LeadActivity.Create";
        public const string Pages_Tenant_DataVaults_LeadActivity_Edit = "Pages.Tenant.DataVaults.LeadActivity.Edit";
        public const string Pages_Tenant_DataVaults_LeadActivity_Delete = "Pages.Tenant.DataVaults.LeadActivity.Delete";
        public const string Pages_Tenant_DataVaults_LeadActivity_ExportToExcel = "Pages.Tenant.DataVaults.LeadActivity.ExportToExcel";

        //LeadAction
        public const string Pages_Tenant_DataVaults_LeadAction = "Pages.Tenant.DataVaults.LeadAction";
        public const string Pages_Tenant_DataVaults_LeadAction_Create = "Pages.Tenant.DataVaults.LeadAction.Create";
        public const string Pages_Tenant_DataVaults_LeadAction_Edit = "Pages.Tenant.DataVaults.LeadAction.Edit";
        public const string Pages_Tenant_DataVaults_LeadAction_Delete = "Pages.Tenant.DataVaults.LeadAction.Delete";
        public const string Pages_Tenant_DataVaults_LeadAction_ExportToExcel = "Pages.Tenant.DataVaults.LeadAction.ExportToExcel";

        //Department
        public const string Pages_Tenant_DataVaults_Department = "Pages.Tenant.DataVaults.Department";
        public const string Pages_Tenant_DataVaults_Department_Create = "Pages.Tenant.DataVaults.Department.Create";
        public const string Pages_Tenant_DataVaults_Department_Edit = "Pages.Tenant.DataVaults.Department.Edit";
        public const string Pages_Tenant_DataVaults_Department_Delete = "Pages.Tenant.DataVaults.Department.Delete";
        public const string Pages_Tenant_DataVaults_Department_ExportToExcel = "Pages.Tenant.DataVaults.Department.ExportToExcel";

        //CustomeTag
        public const string Pages_Tenant_DataVaults_CustomeTag = "Pages.Tenant.DataVaults.CustomeTag";
        public const string Pages_Tenant_DataVaults_CustomeTag_Create = "Pages.Tenant.DataVaults.CustomeTag.Create";
        public const string Pages_Tenant_DataVaults_CustomeTag_Edit = "Pages.Tenant.DataVaults.CustomeTag.Edit";
        public const string Pages_Tenant_DataVaults_CustomeTag_Delete = "Pages.Tenant.DataVaults.CustomeTag.Delete";
        public const string Pages_Tenant_DataVaults_CustomeTag_ExportToExcel = "Pages.Tenant.DataVaults.CustomeTag.ExportToExcel";

        //Variation Permissions
        public const string Pages_Tenant_DataVaults_Variation = "Pages.Tenant.DataVaults.Variation";
        public const string Pages_Tenant_DataVaults_Variation_Create = "Pages.Tenant.DataVaults.Variation.Create";
        public const string Pages_Tenant_DataVaults_Variation_Edit = "Pages.Tenant.DataVaults.Variation.Edit";
        public const string Pages_Tenant_DataVaults_Variation_Delete = "Pages.Tenant.DataVaults.Variation.Delete";
        public const string Pages_Tenant_DataVaults_Variation_ExportToExcel = "Pages.Tenant.DataVaults.Variation.ExportToExcel";

        //PaymentMode
        public const string Pages_Tenant_DataVaults_PaymentMode = "Pages.Tenant.DataVaults.PaymentMode";
        public const string Pages_Tenant_DataVaults_PaymentMode_Create = "Pages.Tenant.DataVaults.PaymentMode.Create";
        public const string Pages_Tenant_DataVaults_PaymentMode_Edit = "Pages.Tenant.DataVaults.PaymentMode.Edit";
        public const string Pages_Tenant_DataVaults_PaymentMode_Delete = "Pages.Tenant.DataVaults.PaymentMode.Delete";
        public const string Pages_Tenant_DataVaults_PaymentMode_ExportToExcel = "Pages.Tenant.DataVaults.PaymentMode.ExportToExcel";

        //PaymentType
        public const string Pages_Tenant_DataVaults_PaymentType = "Pages.Tenant.DataVaults.PaymentType";
        public const string Pages_Tenant_DataVaults_PaymentType_Create = "Pages.Tenant.DataVaults.PaymentType.Create";
        public const string Pages_Tenant_DataVaults_PaymentType_Edit = "Pages.Tenant.DataVaults.PaymentType.Edit";
        public const string Pages_Tenant_DataVaults_PaymentType_Delete = "Pages.Tenant.DataVaults.PaymentType.Delete";
        public const string Pages_Tenant_DataVaults_PaymentType_ExportToExcel = "Pages.Tenant.DataVaults.PaymentType.ExportToExcel";

        //BillOfMaterial
        public const string Pages_Tenant_DataVaults_BillOfMaterial = "Pages.Tenant.DataVaults.BillOfMaterial";
        public const string Pages_Tenant_DataVaults_BillOfMaterial_Create = "Pages.Tenant.DataVaults.BillOfMaterial.Create";
        public const string Pages_Tenant_DataVaults_BillOfMaterial_Edit = "Pages.Tenant.DataVaults.BillOfMaterial.Edit";
        public const string Pages_Tenant_DataVaults_BillOfMaterial_Delete = "Pages.Tenant.DataVaults.BillOfMaterial.Delete";
        public const string Pages_Tenant_DataVaults_BillOfMaterial_ExportToExcel = "Pages.Tenant.DataVaults.BillOfMaterial.ExportToExcel";
        public const string Pages_Tenant_DataVaults_BillOfMaterial_ExportToExcelSample = "Pages.Tenant.DataVaults.BillOfMaterial.ExportToExcelSample";
        public const string Pages_Tenant_DataVaults_BillOfMaterial_ImportToExcel = "Pages.Tenant.DataVaults.BillOfMaterial.ImportToExcel";

        //RefundReasons
        public const string Pages_Tenant_DataVaults_RefundReasons = "Pages.Tenant.DataVaults.RefundReasons";
        public const string Pages_Tenant_DataVaults_RefundReasons_Create = "Pages.Tenant.DataVaults.RefundReasons.Create";
        public const string Pages_Tenant_DataVaults_RefundReasons_Edit = "Pages.Tenant.DataVaults.RefundReasons.Edit";
        public const string Pages_Tenant_DataVaults_RefundReasons_Delete = "Pages.Tenant.DataVaults.RefundReasons.Delete";
        public const string Pages_Tenant_DataVaults_RefundReasons_ExportToExcel = "Pages.Tenant.DataVaults.RefundReasons.ExportToExcel";

        //DispatchType
        public const string Pages_Tenant_DataVaults_DispatchType = "Pages.Tenant.DataVaults.DispatchType";
        public const string Pages_Tenant_DataVaults_DispatchType_Create = "Pages.Tenant.DataVaults.DispatchType.Create";
        public const string Pages_Tenant_DataVaults_DispatchType_Edit = "Pages.Tenant.DataVaults.DispatchType.Edit";
        public const string Pages_Tenant_DataVaults_DispatchType_Delete = "Pages.Tenant.DataVaults.DispatchType.Delete";
        public const string Pages_Tenant_DataVaults_DispatchType_ExportToExcel = "Pages.Tenant.DataVaults.DispatchType.ExportToExcel";
        
        //Vehical Master
        public const string Pages_Tenant_DataVaults_Vehical = "Pages.Tenant.DataVaults.Vehical";
        public const string Pages_Tenant_DataVaults_Vehical_Create = "Pages.Tenant.DataVaults.Vehical.Create";
        public const string Pages_Tenant_DataVaults_Vehical_Edit = "Pages.Tenant.DataVaults.Vehical.Edit";
        public const string Pages_Tenant_DataVaults_Vehical_Delete = "Pages.Tenant.DataVaults.Vehical.Delete";
        public const string Pages_Tenant_DataVaults_Vehical_ExportToExcel = "Pages.Tenant.DataVaults.Vehical.ExportToExcel";

        //PDFTemplate
        public const string Pages_Tenant_DataVaults_PDFTemplate = "Pages.Tenant.DataVaults.PDFTemplate";
        public const string Pages_Tenant_DataVaults_PDFTemplate_Create = "Pages.Tenant.DataVaults.PDFTemplate.Create";
        public const string Pages_Tenant_DataVaults_PDFTemplate_Edit = "Pages.Tenant.DataVaults.PDFTemplate.Edit";
        public const string Pages_Tenant_DataVaults_PDFTemplate_Delete = "Pages.Tenant.DataVaults.PDFTemplate.Delete";
        public const string Pages_Tenant_DataVaults_PDFTemplate_ExportToExcel = "Pages.Tenant.DataVaults.PDFTemplate.ExportToExcel";

        public const string Pages_Tenant_Leads = "Pages.Tenant.Leads";

        //Customer
        public const string Pages_Tenant_Leads_Customer = "Pages.Tenant.Leads.Customer";
        public const string Pages_Tenant_Leads_Customer_Create = "Pages.Tenant.Leads.Customer.Create";
        public const string Pages_Tenant_Leads_Customer_Edit = "Pages.Tenant.Leads.Customer.Edit";
        public const string Pages_Tenant_Leads_Customer_Delete = "Pages.Tenant.Leads.Customer.Delete";
        public const string Pages_Tenant_Leads_Customer_ExportToExcel = "Pages.Tenant.Leads.Customer.ExportToExcel";
        public const string Pages_Tenant_Leads_Customer_Assign = "Pages.Tenant.Leads.Customer.Assign";
        public const string Pages_Tenant_Leads_ChangeOrganization = "Pages.Tenant.Leads.ChangeOrganization";

        public const string Pages_Tenant_Leads_Customer_Summary = "Pages.Tenant.Leads.Customer.Summary";
        public const string Pages_Tenant_Leads_Customer_LeadAddActivity = "Pages.Tenant.Leads.Customer.LeadAddActivity";
        public const string Pages_Tenant_Leads_Customer_LeadActivity = "Pages.Tenant.Leads.Customer.LeadActivity";
        public const string Pages_Tenant_Leads_Customer_ViewMap = "Pages.Tenant.Leads.Customer.ViewMap";
        public const string Pages_Tenant_Leads_Customer_ViewDocument = "Pages.Tenant.Leads.Customer.ViewDocument";
        public const string Pages_Tenant_Leads_Customer_UploadDocument = "Pages.Tenant.Leads.Customer.UploadDocument";
        public const string Pages_Tenant_Leads_Customer_DeleteDocument = "Pages.Tenant.Leads.Customer.DeleteDocument";

        //ManageLead
        public const string Pages_Tenant_Leads_ManageLead = "Pages.Tenant.Leads.ManageLead";
        public const string Pages_Tenant_Leads_ManageLead_Create = "Pages.Tenant.Leads.ManageLead.Create";
        public const string Pages_Tenant_Leads_ManageLead_Edit = "Pages.Tenant.Leads.ManageLead.Edit";
        public const string Pages_Tenant_Leads_ManageLead_Delete = "Pages.Tenant.Leads.ManageLead.Delete";       
        public const string Pages_Tenant_Leads_ManageLead_Assign = "Pages.Tenant.Leads.ManageLead.Assign";
        public const string Pages_Tenant_Leads_ManageLead_ExportToExcel = "Pages.Tenant.Leads.ManageLead.ExportToExcel";
        public const string Pages_Tenant_Leads_ManageLead_ImportToExcel = "Pages.Tenant.Leads.ManageLead.ImportToExcel";
        public const string Pages_Tenant_Leads_ManageLead_Action = "Pages.Tenant.Leads.ManageLead.Action";
        public const string Pages_Tenant_Leads_ManageLead_ChangeOrganization = "Pages.Tenant.Leads.ManageLead.ChangeOrganization";


        //MIS Report       
        public const string Pages_Tenant_Leads_MISReport = "Pages.Tenant.Leads.MISReport";
        public const string Pages_Tenant_Leads_MISReport_ImportToExcel = "Pages.Tenant.Leads.MISReport.ImportToExcel";

        //jobs
        public const string Pages_Tenant_Jobs = "Pages.Tenant.Jobs";

        //jobs Sales
        public const string Pages_Tenant_Jobs_Sales = "Pages.Tenant.Jobs.Sales";
        public const string Pages_Tenant_Jobs_Sales_Create = "Pages.Tenant.Jobs.Sales.Create";
        public const string Pages_Tenant_Jobs_Sales_Edit = "Pages.Tenant.Jobs.Sales.Edit";
        public const string Pages_Tenant_jobs_Sales_Delete = "Pages.Tenant.Jobs.Sales.Delete";
        public const string Pages_Tenant_Jobs_Sales_ExportToExcel = "Pages.Tenant.Jobs.Sales.ExportToExcel";

        //jobs Quotation
        public const string Pages_Tenant_Jobs_Quotation = "Pages.Tenant.Jobs.Quotation";
        public const string Pages_Tenant_Jobs_Quotation_Create = "Pages.Tenant.Jobs.Quotation.Create";
        public const string Pages_Tenant_Jobs_Quotation_Edit = "Pages.Tenant.Jobs.Quotation.Edit";
        public const string Pages_Tenant_jobs_Quotation_Delete = "Pages.Tenant.Jobs.Quotation.Delete";
        public const string Pages_Tenant_Jobs_Quotation_ExportToExcel = "Pages.Tenant.Jobs.Quotation.ExportToExcel";

        //jobs Payment Details
        public const string Pages_Tenant_Jobs_Payment = "Pages.Tenant.Jobs.Payment";
        public const string Pages_Tenant_Jobs_Payment_Create = "Pages.Tenant.Jobs.Payment.Create";
        public const string Pages_Tenant_Jobs_Payment_Edit = "Pages.Tenant.Jobs.Payment.Edit";
        public const string Pages_Tenant_jobs_Payment_Delete = "Pages.Tenant.Jobs.Payment.Delete";
        public const string Pages_Tenant_Jobs_Payment_ExportToExcel = "Pages.Tenant.Jobs.Payment.ExportToExcel";

        //jobs Payment Receipt Details
        public const string Pages_Tenant_Jobs_PaymentReceipt = "Pages.Tenant.Jobs.PaymentReceipt";
        public const string Pages_Tenant_Jobs_PaymentReceipt_Create = "Pages.Tenant.Jobs.PaymentReceipt.Create";
        public const string Pages_Tenant_Jobs_PaymentReceipt_Edit = "Pages.Tenant.Jobs.PaymentReceipt.Edit";
        public const string Pages_Tenant_jobs_PaymentReceipt_Delete = "Pages.Tenant.Jobs.PaymentReceipt.Delete";
        public const string Pages_Tenant_Jobs_PaymentReceipt_ExportToExcel = "Pages.Tenant.Jobs.PaymentReceipt.ExportToExcel";

        //jobs Installation
        public const string Pages_Tenant_Jobs_Installation = "Pages.Tenant.Jobs.Installation";
        public const string Pages_Tenant_Jobs_Installation_Create = "Pages.Tenant.Jobs.Installation.Create";
        public const string Pages_Tenant_Jobs_Installation_Edit = "Pages.Tenant.Jobs.Installation.Edit";
        public const string Pages_Tenant_jobs_Installation_Delete = "Pages.Tenant.Jobs.Installation.Delete";

        //jobs Refund
        public const string Pages_Tenant_Jobs_Refund = "Pages.Tenant.Jobs.Refund";
        public const string Pages_Tenant_Jobs_Refund_Create = "Pages.Tenant.Jobs.Refund.Create";
        public const string Pages_Tenant_Jobs_Refund_Edit = "Pages.Tenant.Jobs.Refund.Edit";
        public const string Pages_Tenant_jobs_Refund_Delete = "Pages.Tenant.Jobs.Refund.Delete";

        //jobs DiscomMeterInstallation
        public const string Pages_Tenant_Jobs_DiscomMeter_Installation = "Pages.Tenant.Jobs.DiscomMeter.Installation";
        public const string Pages_Tenant_Jobs_DiscomMeter_Installation_Create = "Pages.Tenant.Jobs.DiscomMeter.Installation.Create";
        public const string Pages_Tenant_Jobs_DiscomMeter_Installation_Edit = "Pages.Tenant.Jobs.DiscomMeter.Installation.Edit";
        public const string Pages_Tenant_jobs_DiscomMeter_Installation_Delete = "Pages.Tenant.Jobs.Installation.DiscomMeter.Delete";

        //jobs BidirectionCertificate
        public const string Pages_Tenant_Jobs_BidirectionCertificate = "Pages.Tenant.Jobs.BidirectionCertificate";
        public const string Pages_Tenant_Jobs_BidirectionCertificate_Create = "Pages.Tenant.Jobs.BidirectionCertificate.Create";
        public const string Pages_Tenant_Jobs_BidirectionCertificate_Edit = "Pages.Tenant.Jobs.BidirectionCertificate.Edit";
        public const string Pages_Tenant_jobs_BidirectionCertificate_Delete = "Pages.Tenant.Jobs.BidirectionCertificate.Delete";

        //Job Type
        public const string Pages_Tenant_Jobs_JobType = "Pages.Tenant.Jobs.JobType";
        public const string Pages_Tenant_Jobs_JobType_ExportToExcel = "Pages.Tenant.Jobs.JobType.ExportToExcel";
        public const string Pages_Tenant_Jobs_JobType_JobViewDetails = "Pages.Tenant.Jobs.JobType.JobViewDetails";
        public const string Pages_Tenant_Jobs_JobType_Reminder = "Pages.Tenant.Jobs.JobType.Reminder";
        public const string Pages_Tenant_Jobs_JobType_SMS = "Pages.Tenant.Jobs.JobType.SMS";
        public const string Pages_Tenant_Jobs_JobType_ToDoList = "Pages.Tenant.Jobs.JobType.ToDoList";
        public const string Pages_Tenant_Jobs_JobType_Comment = "Pages.Tenant.Jobs.JobType.Comment";

        //Tracker
        public const string Pages_Tenant_Tracker = "Pages.Tenant.Tracker";

        //Application Tracker
        public const string Pages_Tenant_Tracker_Application = "Pages.Tenant.Tracker.Application";
        public const string Pages_Tenant_Tracker_Application_LeadViewDetails = "Pages.Tenant.Tracker.Application.LeadViewDetails";
        public const string Pages_Tenant_Tracker_Application_LeadAddActivity = "Pages.Tenant.Tracker.Application.LeadAddActivity";
        public const string Pages_Tenant_Tracker_Application_ExportToExcel = "Pages.Tenant.Tracker.Application.ExportToExcel";
        public const string Pages_Tenant_Tracker_Application_QuickView  = "Pages.Tenant.Tracker.Application.QuickView";
        public const string Pages_Tenant_Tracker_Application_ApplicationDetails  = "Pages.Tenant.Tracker.Application.ApplicationDetails";
        public const string Pages_Tenant_Tracker_Application_ApplicationReadyStatus = "Pages.Tenant.Tracker.Application.ApplicationReadyStatus";
        public const string Pages_Tenant_Tracker_Application_OTPVerifyStatus = "Pages.Tenant.Tracker.Application.OTPVerifyStatus";
        public const string Pages_Tenant_Tracker_Application_UploadSignApplication = "Pages.Tenant.Tracker.Application.UploadSignApplication";
        public const string Pages_Tenant_Tracker_Application_LoadReduce  = "Pages.Tenant.Tracker.Application.LoadReduce";
        public const string Pages_Tenant_Tracker_Application_Reminder = "Pages.Tenant.Tracker.Application.Reminder";
        public const string Pages_Tenant_Tracker_Application_SMS = "Pages.Tenant.Tracker.Application.SMS";
        public const string Pages_Tenant_Tracker_Application_ToDoList = "Pages.Tenant.Tracker.Application.ToDoList";
        public const string Pages_Tenant_Tracker_Application_Comment = "Pages.Tenant.Tracker.Application.Comment";
        public const string Pages_Tenant_Tracker_Application_ChangeOrganization = "Pages.Tenant.Tracker.Application.ChangeOrganization";

        //EstimatedPaid Tracker
        public const string Pages_Tenant_Tracker_EstimatedPaid = "Pages.Tenant.Tracker.EstimatedPaid";
        public const string Pages_Tenant_Tracker_EstimatedPaid_LeadViewDetails = "Pages.Tenant.Tracker.EstimatedPaid.LeadViewDetails";
        public const string Pages_Tenant_Tracker_EstimatedPaid_LeadAddActivity = "Pages.Tenant.Tracker.EstimatedPaid.LeadAddActivity";
        public const string Pages_Tenant_Tracker_EstimatedPaid_ExportToExcel = "Pages.Tenant.Tracker.EstimatedPaid.ExportToExcel";
        public const string Pages_Tenant_Tracker_EstimatedPaid_QuickView = "Pages.Tenant.Tracker.EstimatedPaid.QuickView";
        public const string Pages_Tenant_Tracker_EstimatedPaid_ApplicationDetails = "Pages.Tenant.Tracker.EstimatedPaid.ApplicationDetails";        
        public const string Pages_Tenant_Tracker_EstimatedPaid_PaymentReciept = "Pages.Tenant.Tracker.EstimatedPaid.PaymentReciept";
        public const string Pages_Tenant_Tracker_EstimatedPaid_Reminder = "Pages.Tenant.Tracker.EstimatedPaid.Reminder";
        public const string Pages_Tenant_Tracker_EstimatedPaid_Notify = "Pages.Tenant.Tracker.EstimatedPaid.Notify";
        public const string Pages_Tenant_Tracker_EstimatedPaid_Comment = "Pages.Tenant.Tracker.EstimatedPaid.Comment";
        public const string Pages_Tenant_Tracker_EstimatedPaid_ChangePayResponse = "Pages.Tenant.Tracker.EstimatedPaid.ChangePayResponse";

        //Dispatch Tracker
        public const string Pages_Tenant_Tracker_Dispatch = "Pages.Tenant.Tracker.Dispatch";
        public const string Pages_Tenant_Tracker_Dispatch_LeadViewDetails = "Pages.Tenant.Tracker.Dispatch.LeadViewDetails";
        public const string Pages_Tenant_Tracker_Dispatch_LeadAddActivity = "Pages.Tenant.Tracker.Dispatch.LeadAddActivity";
        public const string Pages_Tenant_Tracker_Dispatch_ExportToExcel = "Pages.Tenant.Tracker.Dispatch.ExportToExcel";
        public const string Pages_Tenant_Tracker_Dispatch_QuickView = "Pages.Tenant.Tracker.Dispatch.QuickView";
        public const string Pages_Tenant_Tracker_Dispatch_SelectMaterial = "Pages.Tenant.Tracker.Dispatch.SelectMaterial";
        public const string Pages_Tenant_Tracker_Dispatch_SendExtraMaterial = "Pages.Tenant.Tracker.Dispatch.SendExtraMaterial";
        public const string Pages_Tenant_Tracker_Dispatch_StockTransfer = "Pages.Tenant.Tracker.Dispatch.StockTransfer";
        public const string Pages_Tenant_Tracker_Dispatch_Reminder = "Pages.Tenant.Tracker.Dispatch.Reminder";
        public const string Pages_Tenant_Tracker_Dispatch_Notify = "Pages.Tenant.Tracker.Dispatch.Notify";
        public const string Pages_Tenant_Tracker_Dispatch_Comment = "Pages.Tenant.Tracker.Dispatch.Comment";
        public const string Pages_Tenant_Tracker_Dispatch_ToDo = "Pages.Tenant.Tracker.Dispatch.ToDo";

        //Refund Tracker
        public const string Pages_Tenant_Tracker_Refund = "Pages.Tenant.Tracker.Refund";
        public const string Pages_Tenant_Tracker_Refund_LeadViewDetails = "Pages.Tenant.Tracker.Refund.LeadViewDetails";
        public const string Pages_Tenant_Tracker_Refund_LeadAddActivity = "Pages.Tenant.Tracker.Refund.LeadAddActivity";
        public const string Pages_Tenant_Tracker_Refund_ExportToExcel = "Pages.Tenant.Tracker.Refund.ExportToExcel";
        public const string Pages_Tenant_Tracker_Refund_QuickView = "Pages.Tenant.Tracker.Refund.QuickView";
        public const string Pages_Tenant_Tracker_Refund_Verify = "Pages.Tenant.Tracker.Refund.Verify";       
        public const string Pages_Tenant_Tracker_Refund_Reminder = "Pages.Tenant.Tracker.Refund.Reminder";
        public const string Pages_Tenant_Tracker_Refund_Notify = "Pages.Tenant.Tracker.Refund.Notify";
        public const string Pages_Tenant_Tracker_Refund_Comment = "Pages.Tenant.Tracker.Refund.Comment";
        public const string Pages_Tenant_Tracker_Refund_ToDo = "Pages.Tenant.Tracker.Refund.ToDo";

        //PickList Tracker
        public const string Pages_Tenant_Tracker_PickList = "Pages.Tenant.Tracker.PickList";
        public const string Pages_Tenant_Tracker_PickList_LeadViewDetails = "Pages.Tenant.Tracker.PickList.LeadViewDetails";
        public const string Pages_Tenant_Tracker_PickList_LeadAddActivity = "Pages.Tenant.Tracker.PickList.LeadAddActivity";
        public const string Pages_Tenant_Tracker_PickList_ExportToExcel = "Pages.Tenant.Tracker.PickList.ExportToExcel";
        public const string Pages_Tenant_Tracker_PickList_QuickView = "Pages.Tenant.Tracker.PickList.QuickView";
        public const string Pages_Tenant_Tracker_PickList_Verify = "Pages.Tenant.Tracker.PickList.Verify";
        public const string Pages_Tenant_Tracker_PickList_Reminder = "Pages.Tenant.Tracker.PickList.Reminder";
        public const string Pages_Tenant_Tracker_PickList_Notify = "Pages.Tenant.Tracker.PickList.Notify";
        public const string Pages_Tenant_Tracker_PickList_Comment = "Pages.Tenant.Tracker.PickList.Comment";
        public const string Pages_Tenant_Tracker_PickList_ToDo = "Pages.Tenant.Tracker.PickList.ToDo";

        //Deposit Tracker
        public const string Pages_Tenant_Tracker_Deposit = "Pages.Tenant.Tracker.Deposit";
        public const string Pages_Tenant_Tracker_Deposit_LeadViewDetails = "Pages.Tenant.Tracker.Deposit.LeadViewDetails";
        public const string Pages_Tenant_Tracker_Deposit_LeadAddActivity = "Pages.Tenant.Tracker.Deposit.LeadAddActivity";
        public const string Pages_Tenant_Tracker_Deposit_ExportToExcel = "Pages.Tenant.Tracker.Deposit.ExportToExcel";
        public const string Pages_Tenant_Tracker_Deposit_QuickView = "Pages.Tenant.Tracker.Deposit.QuickView";
        public const string Pages_Tenant_Tracker_Deposit_VerifyDetails = "Pages.Tenant.Tracker.Deposit.VerifyDetails";
        public const string Pages_Tenant_Tracker_Deposit_Reminder = "Pages.Tenant.Tracker.Deposit.Reminder";
        public const string Pages_Tenant_Tracker_Deposit_Notify = "Pages.Tenant.Tracker.Deposit.Notify";
        public const string Pages_Tenant_Tracker_Deposit_Comment = "Pages.Tenant.Tracker.Deposit.Comment";
        public const string Pages_Tenant_Tracker_Deposit_ToDo = "Pages.Tenant.Tracker.Deposit.ToDo";

        //Installation Tracker
        public const string Pages_Tenant_Tracker_Installation = "Pages.Tenant.Tracker.Installation";
        public const string Pages_Tenant_Tracker_Installation_LeadViewDetails = "Pages.Tenant.Tracker.Installation.LeadViewDetails";
        public const string Pages_Tenant_Tracker_Installation_LeadAddActivity = "Pages.Tenant.Tracker.Installation.LeadAddActivity";
        public const string Pages_Tenant_Tracker_Installation_ExportToExcel = "Pages.Tenant.Tracker.Installation.ExportToExcel";
        public const string Pages_Tenant_Tracker_Installation_QuickView = "Pages.Tenant.Tracker.Installation.QuickView";       
        public const string Pages_Tenant_Tracker_Installation_Reminder = "Pages.Tenant.Tracker.Installation.Reminder";
        public const string Pages_Tenant_Tracker_Installation_Notify = "Pages.Tenant.Tracker.Installation.Notify";
        public const string Pages_Tenant_Tracker_Installation_Comment = "Pages.Tenant.Tracker.Installation.Comment";
        public const string Pages_Tenant_Tracker_Installation_ToDo = "Pages.Tenant.Tracker.Installation.ToDo";

        //Transport Tracker
        public const string Pages_Tenant_Tracker_Transport = "Pages.Tenant.Tracker.Transport";
        public const string Pages_Tenant_Tracker_Transport_LeadViewDetails = "Pages.Tenant.Tracker.Transport.LeadViewDetails";
        public const string Pages_Tenant_Tracker_Transport_LeadAddActivity = "Pages.Tenant.Tracker.Transport.LeadAddActivity";
        public const string Pages_Tenant_Tracker_Transport_ExportToExcel = "Pages.Tenant.Tracker.Transport.ExportToExcel";
        public const string Pages_Tenant_Tracker_Transport_QuickView = "Pages.Tenant.Tracker.Transport.QuickView";
        public const string Pages_Tenant_Tracker_Transport_Reminder = "Pages.Tenant.Tracker.Transport.Reminder";
        public const string Pages_Tenant_Tracker_Transport_SMS = "Pages.Tenant.Tracker.Transport.SMS";
        public const string Pages_Tenant_Tracker_Transport_Notify = "Pages.Tenant.Tracker.Transport.Notify";
        public const string Pages_Tenant_Tracker_Transport_Comment = "Pages.Tenant.Tracker.Transport.Comment";
        public const string Pages_Tenant_Tracker_Transport_ToDo = "Pages.Tenant.Tracker.Transport.ToDo";

        //MeterConnect Tracker
        public const string Pages_Tenant_Tracker_MeterConnect = "Pages.Tenant.Tracker.MeterConnect";
        public const string Pages_Tenant_Tracker_MeterConnect_LeadViewDetails = "Pages.Tenant.Tracker.MeterConnect.LeadViewDetails";
        public const string Pages_Tenant_Tracker_MeterConnect_LeadAddActivity = "Pages.Tenant.Tracker.MeterConnect.LeadAddActivity";
        public const string Pages_Tenant_Tracker_MeterConnect_ExportToExcel = "Pages.Tenant.Tracker.MeterConnect.ExportToExcel";
        public const string Pages_Tenant_Tracker_MeterConnect_QuickView = "Pages.Tenant.Tracker.MeterConnect.QuickView";
        public const string Pages_Tenant_Tracker_MeterConnect_Reminder = "Pages.Tenant.Tracker.MeterConnect.Reminder";
        public const string Pages_Tenant_Tracker_MeterConnect_Notify = "Pages.Tenant.Tracker.MeterConnect.Notify";
        public const string Pages_Tenant_Tracker_MeterConnect_Comment = "Pages.Tenant.Tracker.MeterConnect.Comment";
        public const string Pages_Tenant_Tracker_MeterConnect_ToDo = "Pages.Tenant.Tracker.MeterConnect.ToDo";
        public const string Pages_Tenant_Tracker_MeterConnect_DownloadDocument = "Pages.Tenant.Tracker.MeterConnect.DownloadDocument";
        public const string Pages_Tenant_Tracker_MeterConnect_SelfCertificate = "Pages.Tenant.Tracker.MeterConnect.SelfCertificate";
        //Subsidy 
        public const string Pages_Tenant_Subsidy = "Pages.Tenant.Subsidy";

        //Subsidy Detail Tracker
        public const string Pages_Tenant_Tracker_SubsidyDetail = "Pages.Tenant.Tracker.SubsidyDetail";
        public const string Pages_Tenant_Tracker_SubsidyDetail_EditSubsidy = "Pages.Tenant.Tracker.SubsidyDetail.EditSubsidy";
        public const string Pages_Tenant_Tracker_SubsidyDetail_VerifySubsidy = "Pages.Tenant.Tracker.SubsidyDetail.VerifySubsidy";
        public const string Pages_Tenant_Tracker_SubsidyDetail_ExportToExcel = "Pages.Tenant.Tracker.SubsidyDetail.ExportToExcel";
        public const string Pages_Tenant_Tracker_SubsidyDetail_QuickView = "Pages.Tenant.Tracker.SubsidyDetail.QuickView";
        public const string Pages_Tenant_Tracker_SubsidyDetail_UploadOCCopy = "Pages.Tenant.Tracker.SubsidyDetail.UploadOCCopy";
        public const string Pages_Tenant_Tracker_SubsidyDetail_Delete = "Pages.Tenant.Tracker.SubsidyDetail.Delete";
        public const string Pages_Tenant_Tracker_SubsidyDetail_Reminder = "Pages.Tenant.Tracker.SubsidyDetail.Reminder";
        public const string Pages_Tenant_Tracker_SubsidyDetail_Notify = "Pages.Tenant.Tracker.SubsidyDetail.Notify";
        public const string Pages_Tenant_Tracker_SubsidyDetail_Comment = "Pages.Tenant.Tracker.SubsidyDetail.Comment";
        public const string Pages_Tenant_Tracker_SubsidyDetail_ToDo = "Pages.Tenant.Tracker.SubsidyDetail.ToDo";
       
        //Subsidy Claim Tracker
        public const string Pages_Tenant_Tracker_SubsidyClaim = "Pages.Tenant.Tracker.SubsidyClaim";
        public const string Pages_Tenant_Tracker_SubsidyClaim_LeadViewDetails = "Pages.Tenant.Tracker.SubsidyClaim.LeadViewDetails";
        public const string Pages_Tenant_Tracker_SubsidyClaim_LeadAddActivity = "Pages.Tenant.Tracker.SubsidyClaim.LeadAddActivity";
        public const string Pages_Tenant_Tracker_SubsidyClaim_ExportToExcel = "Pages.Tenant.Tracker.SubsidyClaim.ExportToExcel";
        public const string Pages_Tenant_Tracker_SubsidyClaim_QuickView = "Pages.Tenant.Tracker.SubsidyClaim.QuickView";
        public const string Pages_Tenant_Tracker_SubsidyClaim_Reminder = "Pages.Tenant.Tracker.SubsidyClaim.Reminder";
        public const string Pages_Tenant_Tracker_SubsidyClaim_Notify = "Pages.Tenant.Tracker.SubsidyClaim.Notify";
        public const string Pages_Tenant_Tracker_SubsidyClaim_Comment = "Pages.Tenant.Tracker.SubsidyClaim.Comment";
        public const string Pages_Tenant_Tracker_SubsidyClaim_ToDo = "Pages.Tenant.Tracker.SubsidyClaim.ToDo";

        //Job Accounts
        public const string Pages_Tenant_Accounts = "Pages.Tenant.Accounts";


        //Payment Issued
        public const string Pages_Tenant_Accounts_PaymentIssued = "Pages.Tenant.Accounts.PaymentIssued";
        public const string Pages_Tenant_Accounts_PaymentIssued_LeadViewDetails = "Pages.Tenant.Accounts.PaymentIssued.LeadViewDetails";
        public const string Pages_Tenant_Accounts_PaymentIssued_LeadAddActivity = "Pages.Tenant.Accounts.PaymentIssued.LeadAddActivity";
        public const string Pages_Tenant_Accounts_PaymentIssued_ExportToExcel = "Pages.Tenant.Accounts.PaymentIssued.ExportToExcel";
        public const string Pages_Tenant_Accounts_PaymentIssued_QuickView = "Pages.Tenant.Accounts.PaymentIssued.QuickView";
        public const string Pages_Tenant_Accounts_PaymentIssued_ApplicationDetails = "Pages.Tenant.Accounts.PaymentIssued.ApplicationDetails";
        public const string Pages_Tenant_Accounts_PaymentIssued_ApplicationReadyStatus = "Pages.Tenant.Accounts.PaymentIssued.ApplicationReadyStatus";
        public const string Pages_Tenant_Accounts_PaymentIssued_OTPVerifyStatus = "Pages.Tenant.Accounts.PaymentIssued.OTPVerifyStatus";
        public const string Pages_Tenant_Accounts_PaymentIssued_UploadSignApplication = "Pages.Tenant.Accounts.PaymentIssued.UploadSignApplication";
        public const string Pages_Tenant_Accounts_PaymentIssued_LoadReduce = "Pages.Tenant.Accounts.PaymentIssued.LoadReduce";
        public const string Pages_Tenant_Accounts_PaymentIssued_Reminder = "Pages.Tenant.Accounts.PaymentIssued.Reminder";
        public const string Pages_Tenant_Accounts_PaymentIssued_Notify = "Pages.Tenant.Accounts.PaymentIssued.Notify";
        public const string Pages_Tenant_Accounts_PaymentIssued_ToDoList = "Pages.Tenant.Accounts.PaymentIssued.ToDoList";
        public const string Pages_Tenant_Accounts_PaymentIssued_Comment = "Pages.Tenant.Accounts.PaymentIssued.Comment";
        public const string Pages_Tenant_Accounts_PaymentIssued_ChangeOrganization = "Pages.Tenant.Accounts.PaymentIssued.ChangeOrganization";

        public const string Pages_Tenant_Accounts_PaymentIssued_SSBillPaymentPDFView = "Pages.Tenant.Accounts.PaymentIssued.SSBillPaymentPDFView";
        public const string Pages_Tenant_Accounts_PaymentIssued_STCBillPaymentPDFView = "Pages.Tenant.Accounts.PaymentIssued.STCBillPaymentPDFView";
        public const string Pages_Tenant_Accounts_PaymentIssued_SSBillPaymentView = "Pages.Tenant.Accounts.PaymentIssued.SSBillPaymentView";
        public const string Pages_Tenant_Accounts_PaymentIssued_STCBillPaymentView = "Pages.Tenant.Accounts.PaymentIssued.STCBillPaymentView";
        public const string Pages_Tenant_Accounts_PaymentIssued_DeliveryPaymentView = "Pages.Tenant.Accounts.PaymentIssued.DeliveryPaymentView";
        public const string Pages_Tenant_Accounts_PaymentIssued_RemoveBillSSorSTC = "Pages.Tenant.Accounts.PaymentIssued.RemoveBillSSorSTC";
        public const string Pages_Tenant_Accounts_PaymentIssued_DispatchStatus = "Pages.Tenant.Accounts.PaymentIssued.DispatchStatus";
        public const string Pages_Tenant_Accounts_PaymentIssued_SSBillAddorEdit = "Pages.Tenant.Accounts.PaymentIssued.SSBillAddorEdit";
        public const string Pages_Tenant_Accounts_PaymentIssued_STCBillAddorEdit = "Pages.Tenant.Accounts.PaymentIssued.STCBillAddorEdit";
        public const string Pages_Tenant_Accounts_PaymentIssued_DeliverPaymentAddorEdit = "Pages.Tenant.Accounts.PaymentIssued.DeliverPaymentAddorEdit";

        //Payment Paid
        public const string Pages_Tenant_Accounts_PaymentPaid = "Pages.Tenant.Accounts.PaymentPaid";
        public const string Pages_Tenant_Accounts_PaymentPaid_LeadViewDetails = "Pages.Tenant.Accounts.PaymentPaid.LeadViewDetails";
        public const string Pages_Tenant_Accounts_PaymentPaid_LeadAddActivity = "Pages.Tenant.Accounts.PaymentPaid.LeadAddActivity";
        public const string Pages_Tenant_Accounts_PaymentPaid_ExportToExcel = "Pages.Tenant.Accounts.PaymentPaid.ExportToExcel";
        public const string Pages_Tenant_Accounts_PaymentPaid_QuickView = "Pages.Tenant.Accounts.PaymentPaid.QuickView";
        public const string Pages_Tenant_Accounts_PaymentPaid_Reminder = "Pages.Tenant.Accounts.PaymentPaid.Reminder";
        public const string Pages_Tenant_Accounts_PaymentPaid_Notify = "Pages.Tenant.Accounts.PaymentPaid.Notify";
        public const string Pages_Tenant_Accounts_PaymentPaid_ToDoList = "Pages.Tenant.Accounts.PaymentPaid.ToDoList";
        public const string Pages_Tenant_Accounts_PaymentPaid_Comment = "Pages.Tenant.Accounts.PaymentPaid.Comment";
        public const string Pages_Tenant_Accounts_PaymentPaid_ImportToExcel = "Pages.Tenant.Accounts.PaymentPaid.ImportToExcel";
        public const string Pages_Tenant_Accounts_PaymentPaid_PaymentVerified = "Pages.Tenant.Accounts.PaymentPaid.PaymentVerified";
    }
}
