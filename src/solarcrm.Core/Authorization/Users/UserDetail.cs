﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Authorization.Users
{
    [Table("UserDetails")]
    public class UserDetail : FullAuditedEntity
    {
        public virtual int? UserId { get; set; }

        public virtual int UserType { get; set; }

        public string MiddleName { get; set; }

        public string CompanyMobileNo { get; set; }

        public string EmergancyContactNo { get; set; }

        public string EmergancyContactName { get; set; }

        public string BloodGroup { get; set; }

        public string Gender { get; set; }

        public string DateOfBirth { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public virtual int? State { get; set; }

        public virtual int? District { get; set; }

        public virtual int? Taluko { get; set; }

        public virtual int? City { get; set; }

        public virtual long? Pincode { get; set; }

        public string BankName { get; set; }

        public string TypeOfAccount { get; set; }

        public string AccountNo { get; set; }

        public string IFSCCode { get; set; }

        public string UANNo { get; set; }

        public string PancardNo { get; set; }

        public string Designation { get; set; }

        public string JoiningDate { get; set; }

        public string ShiftStartAt { get; set; }

        public string ShiftEndAt { get; set; }

        public string TypeOfCompany { get; set; }

        public string BelongsTo { get; set; }

        public string AdharNo { get; set; }

        public string GSTNo { get; set; }

        public virtual int? ManageBy { get; set; }

        //public string Department { get; set; }
        public int? Discom { get; set; }

        public string Department { get; set; }

    }
}
