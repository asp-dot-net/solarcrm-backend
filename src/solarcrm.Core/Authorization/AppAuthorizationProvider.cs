﻿using Abp.Application.Features;
using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;
using solarcrm.DataVaults.DispatchType;
using solarcrm.Features;

namespace solarcrm.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));
            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"), multiTenancySides: MultiTenancySides.Host);

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeDefaultLanguage, L("ChangeDefaultLanguage"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription, L("Webhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create, L("CreatingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit, L("EditingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity, L("ChangingWebhookActivity"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail, L("DetailingSubscription"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts, L("ListingSendAttempts"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook, L("ResendingWebhook"));

            var dynamicProperties = administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties, L("DynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Create, L("CreatingDynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Edit, L("EditingDynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Delete, L("DeletingDynamicProperties"));

            var dynamicPropertyValues = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue, L("DynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Create, L("CreatingDynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Edit, L("EditingDynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Delete, L("DeletingDynamicPropertyValue"));

            var dynamicEntityProperties = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties, L("DynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Create, L("CreatingDynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Edit, L("EditingDynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Delete, L("DeletingDynamicEntityProperties"));

            var dynamicEntityPropertyValues = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue, L("EntityDynamicPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Create, L("CreatingDynamicEntityPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Edit, L("EditingDynamicEntityPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Delete, L("DeletingDynamicEntityPropertyValue"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);

            //TENANT-SPECIFIC PERMISSIONS
            var dataVaultsFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults);
            var dataVaults = pages.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults, L("DataVaults"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: dataVaultsFeatureDependency);

            //Location
            var locationFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Locations);
            var Locations = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Locations, L("Locations"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: locationFeatureDependency);
            Locations.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Locations_Create, L("CreatingNewLocations"), multiTenancySides: MultiTenancySides.Tenant);
            Locations.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Locations_Edit, L("EditingLocations"), multiTenancySides: MultiTenancySides.Tenant);
            Locations.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Locations_Delete, L("DeletingLocations"), multiTenancySides: MultiTenancySides.Tenant);
            Locations.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Locations_ExportToExcel, L("ExportToExcelLocations"), multiTenancySides: MultiTenancySides.Tenant);

            //Lead Source
            var leadSourceFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_LeadSource);
            var leadSource = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadSource, L("LeadSource"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadSourceFeatureDependency);
            leadSource.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadSource_Create, L("CreatingNewLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
            leadSource.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadSource_Edit, L("EditingLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
            leadSource.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadSource_Delete, L("DeletingLeadSource"), multiTenancySides: MultiTenancySides.Tenant);
            leadSource.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadSource_ExportToExcel, L("ExportToExcelLeadSource"), multiTenancySides: MultiTenancySides.Tenant);

            //Lead Type
            var leadTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_LeadType);
            var leadType = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadType, L("LeadType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadTypeFeatureDependency);
            leadType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadType_Create, L("CreatingNewLeadType"), multiTenancySides: MultiTenancySides.Tenant);
            leadType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadType_Edit, L("EditingLeadType"), multiTenancySides: MultiTenancySides.Tenant);
            leadType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadType_Delete, L("DeletingLeadType"), multiTenancySides: MultiTenancySides.Tenant);
            leadType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadType_ExportToExcel, L("ExportToExcelLeadType"), multiTenancySides: MultiTenancySides.Tenant);

            //Decument List
            var docListFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_DocumentList);
            var docList = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentList, L("DocumentList"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: docListFeatureDependency);
            docList.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentList_Create, L("CreatingNewDocumentList"), multiTenancySides: MultiTenancySides.Tenant);
            docList.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentList_Edit, L("EditingDocumentList"), multiTenancySides: MultiTenancySides.Tenant);
            docList.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentList_Delete, L("DeletingDocumentList"), multiTenancySides: MultiTenancySides.Tenant);
            docList.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentList_ExportToExcel, L("ExportToExcelDocumentList"), multiTenancySides: MultiTenancySides.Tenant);

            //Teams
            var teamsFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Teams);
            var teams = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Teams, L("Teams"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: teamsFeatureDependency);
            teams.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Teams_Create, L("CreatingNewTeams"), multiTenancySides: MultiTenancySides.Tenant);
            teams.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Teams_Edit, L("EditingTeam"), multiTenancySides: MultiTenancySides.Tenant);
            teams.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Teams_Delete, L("DeletingTeam"), multiTenancySides: MultiTenancySides.Tenant);
            teams.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Teams_ExportToExcel, L("ExportToExcelTeams"), multiTenancySides: MultiTenancySides.Tenant);

            //Discom
            var discomFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Discom);
            var discom = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Discom, L("Discom"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: discomFeatureDependency);
            discom.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Discom_Create, L("CreatingNewDiscom"), multiTenancySides: MultiTenancySides.Tenant);
            discom.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Discom_Edit, L("EditingDiscom"), multiTenancySides: MultiTenancySides.Tenant);
            discom.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Discom_Delete, L("DeletingDiscom"), multiTenancySides: MultiTenancySides.Tenant);
            discom.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Discom_ExportToExcel, L("ExportToExcelDiscom"), multiTenancySides: MultiTenancySides.Tenant);

            //Division
            var divisionFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Division);
            var division = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Division, L("Division"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: divisionFeatureDependency);
            division.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Division_Create, L("CreatingNewDivision"), multiTenancySides: MultiTenancySides.Tenant);
            division.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Division_Edit, L("EditingDivision"), multiTenancySides: MultiTenancySides.Tenant);
            division.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Division_Delete, L("DeletingDivision"), multiTenancySides: MultiTenancySides.Tenant);
            division.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Division_ExportToExcel, L("ExportToExcelDivision"), multiTenancySides: MultiTenancySides.Tenant);

            //Sub Division
            var subDivisionFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_SubDivision);
            var subDivision = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SubDivision, L("SubDivision"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: subDivisionFeatureDependency);
            subDivision.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SubDivision_Create, L("CreatingNewSubDivision"), multiTenancySides: MultiTenancySides.Tenant);
            subDivision.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SubDivision_Edit, L("EditingSubDivision"), multiTenancySides: MultiTenancySides.Tenant);
            subDivision.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SubDivision_Delete, L("DeletingSubDivision"), multiTenancySides: MultiTenancySides.Tenant);
            subDivision.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SubDivision_ExportToExcel, L("ExportToExcelSubDivision"), multiTenancySides: MultiTenancySides.Tenant);

            //Transactions Type
            var transactionTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_TransactionType);
            var transactionType = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_TransactionType, L("TransactionType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: transactionTypeFeatureDependency);
            transactionType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_TransactionType_Create, L("CreatingNewTransactionType"), multiTenancySides: MultiTenancySides.Tenant);
            transactionType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_TransactionType_Edit, L("EditingTransactionType"), multiTenancySides: MultiTenancySides.Tenant);
            transactionType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_TransactionType_Delete, L("DeletingTransactionType"), multiTenancySides: MultiTenancySides.Tenant);
            transactionType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_TransactionType_ExportToExcel, L("ExportToExcelTransactionType"), multiTenancySides: MultiTenancySides.Tenant);

            //Stock Item
            var stockItemFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_StockItem);
            var stockItem = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockItem, L("StockItem"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: stockItemFeatureDependency);
            stockItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockItem_Create, L("CreatingNewStockItem"), multiTenancySides: MultiTenancySides.Tenant);
            stockItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockItem_Edit, L("EditingStockItem"), multiTenancySides: MultiTenancySides.Tenant);
            stockItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockItem_Delete, L("DeletingStockItem"), multiTenancySides: MultiTenancySides.Tenant);
            stockItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockItem_ExportToExcel, L("ExportToExcelStockItem"), multiTenancySides: MultiTenancySides.Tenant);
            stockItem.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockItem_ImportToExcel, L("ImportToExcelStockItem"), multiTenancySides: MultiTenancySides.Tenant);

            //ProjectType
            var projectTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_ProjectType);
            var ProjectType = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectType, L("ProjectType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: projectTypeFeatureDependency);
            ProjectType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectType_Create, L("CreatingNewProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectType_Edit, L("EditingProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectType_Delete, L("DeletingProjectType"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectType_ExportToExcel, L("ExportToExcelProjectType"), multiTenancySides: MultiTenancySides.Tenant);

            //ProjectStatus
            var projectStatusFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_ProjectStatus);
            var ProjectStatus = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus, L("ProjectStatus"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: projectStatusFeatureDependency);
            ProjectStatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_Create, L("CreatingNewProjectStatus"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectStatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_Edit, L("EditingProjectStatus"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectStatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_Delete, L("DeletingProjectStatus"), multiTenancySides: MultiTenancySides.Tenant);
            ProjectStatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ProjectStatus_ExportToExcel, L("ExportToExcelProjectStatus"), multiTenancySides: MultiTenancySides.Tenant);

            //StockCategory
            var stockCategoryFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_StockCategory);
            var StockCategory = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockCategory, L("StockCategory"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: stockCategoryFeatureDependency);
            StockCategory.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Create, L("CreatingNewStockCategory"), multiTenancySides: MultiTenancySides.Tenant);
            StockCategory.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Edit, L("EditingStockCategory"), multiTenancySides: MultiTenancySides.Tenant);
            StockCategory.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockCategory_Delete, L("DeletingStockCategory"), multiTenancySides: MultiTenancySides.Tenant);
            StockCategory.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_StockCategory_ExportToExcel, L("ExportToExcelStockCategory"), multiTenancySides: MultiTenancySides.Tenant);

            //InvoiceType
            var InvoiceTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_InvoiceType);
            var InvoiceType = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InvoiceType, L("InvoiceType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: InvoiceTypeFeatureDependency);
            InvoiceType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_Create, L("CreatingNewInvoiceType"), multiTenancySides: MultiTenancySides.Tenant);
            InvoiceType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_Edit, L("EditingInvoiceType"), multiTenancySides: MultiTenancySides.Tenant);
            InvoiceType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_Delete, L("DeletingInvoiceType"), multiTenancySides: MultiTenancySides.Tenant);
            InvoiceType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_InvoiceType_ExportToExcel, L("ExportToExcelInvoiceType"), multiTenancySides: MultiTenancySides.Tenant);

            //State
            var stateFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_State);
            var state = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_State, L("State"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: stateFeatureDependency);
            state.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_State_Create, L("CreatingNewState"), multiTenancySides: MultiTenancySides.Tenant);
            state.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_State_Edit, L("EditingState"), multiTenancySides: MultiTenancySides.Tenant);
            state.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_State_Delete, L("DeletingState"), multiTenancySides: MultiTenancySides.Tenant);
            state.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_State_ExportToExcel, L("ExportToExcelState"), multiTenancySides: MultiTenancySides.Tenant);

            //District
            var districtFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_District);
            var district = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_District, L("District"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: districtFeatureDependency);
            district.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_District_Create, L("CreatingNewDistrict"), multiTenancySides: MultiTenancySides.Tenant);
            district.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_District_Edit, L("EditingDistrict"), multiTenancySides: MultiTenancySides.Tenant);
            district.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_District_Delete, L("DeletingDistrict"), multiTenancySides: MultiTenancySides.Tenant);
            district.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_District_ExportToExcel, L("ExportToExcelDistrict"), multiTenancySides: MultiTenancySides.Tenant);

            //Taluka
            var talukaFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Taluka);
            var taluka = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Taluka, L("Taluka"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: talukaFeatureDependency);
            taluka.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Taluka_Create, L("CreatingNewTaluka"), multiTenancySides: MultiTenancySides.Tenant);
            taluka.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Taluka_Edit, L("EditingTaluka"), multiTenancySides: MultiTenancySides.Tenant);
            taluka.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Taluka_Delete, L("DeletingTaluka"), multiTenancySides: MultiTenancySides.Tenant);
            taluka.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Taluka_ExportToExcel, L("ExportToExcelTaluka"), multiTenancySides: MultiTenancySides.Tenant);

            //City
            var cityFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_City);
            var city = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_City, L("City"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: cityFeatureDependency);
            city.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_City_Create, L("CreatingNewCity"), multiTenancySides: MultiTenancySides.Tenant);
            city.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_City_Edit, L("EditingCity"), multiTenancySides: MultiTenancySides.Tenant);
            city.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_City_Delete, L("DeletingCity"), multiTenancySides: MultiTenancySides.Tenant);
            city.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_City_ExportToExcel, L("ExportToExcelCity"), multiTenancySides: MultiTenancySides.Tenant);

            //CancelReasons
            var cancelReasonsFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_CancelReasons);
            var CancelReasons = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CancelReasons, L("CancelReasons"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: cancelReasonsFeatureDependency);
            CancelReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_Create, L("CreatingNewCancelReasons"), multiTenancySides: MultiTenancySides.Tenant);
            CancelReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_Edit, L("EditingCancelReasons"), multiTenancySides: MultiTenancySides.Tenant);
            CancelReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_Delete, L("DeletingCancelReasons"), multiTenancySides: MultiTenancySides.Tenant);
            CancelReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CancelReasons_ExportToExcel, L("ExportToExcelCancelReasons"), multiTenancySides: MultiTenancySides.Tenant);

            //HoldReasons
            var holdReasonsFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_HoldReasons);
            var HoldReasons = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HoldReasons, L("HoldReasons"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: holdReasonsFeatureDependency);
            HoldReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_Create, L("CreatingNewHoldReasons"), multiTenancySides: MultiTenancySides.Tenant);
            HoldReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_Edit, L("EditingHoldReasons"), multiTenancySides: MultiTenancySides.Tenant);
            HoldReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_Delete, L("DeletingHoldReasons"), multiTenancySides: MultiTenancySides.Tenant);
            HoldReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HoldReasons_ExportToExcel, L("ExportToExcelHoldReasons"), multiTenancySides: MultiTenancySides.Tenant);

            //RejectReasons
            var rejectReasonsFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_RejectReasons);
            var RejectReasons = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RejectReasons, L("RejectReasons"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: rejectReasonsFeatureDependency);
            RejectReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_Create, L("CreatingNewRejectReasons"), multiTenancySides: MultiTenancySides.Tenant);
            RejectReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_Edit, L("EditingRejectReasons"), multiTenancySides: MultiTenancySides.Tenant);
            RejectReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_Delete, L("DeletingRejectReasons"), multiTenancySides: MultiTenancySides.Tenant);
            RejectReasons.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RejectReasons_ExportToExcel, L("ExportToExcelRejectReasons"), multiTenancySides: MultiTenancySides.Tenant);

            //JobCancellationReason
            var jobCancellationReasonFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_JobCancellationReason);
            var JobCancellationReason = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason, L("JobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: jobCancellationReasonFeatureDependency);
            JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_Create, L("CreatingNewJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
            JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_Edit, L("EditingJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
            JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_Delete, L("DeletingJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);
            JobCancellationReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_JobCancellationReason_ExportToExcel, L("ExportToExcelJobCancellationReason"), multiTenancySides: MultiTenancySides.Tenant);

            //DocumentLibrary
            var documentLibraryFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_DocumentLibrary);
            var DocumentLibrary = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary, L("DocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: documentLibraryFeatureDependency);
            DocumentLibrary.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_Create, L("CreatingNewDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
            DocumentLibrary.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_Edit, L("EditingDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
            DocumentLibrary.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_Delete, L("DeletingDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);
            DocumentLibrary.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DocumentLibrary_ExportToExcel, L("ExportToExcelDocumentLibrary"), multiTenancySides: MultiTenancySides.Tenant);

            //SmsTemplates
            var smsTemplatesFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_SmsTemplates);
            var SmsTemplates = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates, L("SmsTemplates"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: smsTemplatesFeatureDependency);
            SmsTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Create, L("CreatingNewSmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            SmsTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Edit, L("EditingSmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            SmsTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_Delete, L("DeletingSmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            SmsTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_View, L("ViewSmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            SmsTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SmsTemplates_ExportToExcel, L("ExportToExcelSmsTemplates"), multiTenancySides: MultiTenancySides.Tenant);

            //EmailTemplates
            var emailTemplatesFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_EmailTemplates);
            var EmailTemplates = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates, L("EmailTemplates"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: emailTemplatesFeatureDependency);
            EmailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Create, L("CreatingNewEmailTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            EmailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Edit, L("EditingEmailTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            EmailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_Delete, L("DeletingEmailTemplates"), multiTenancySides: MultiTenancySides.Tenant);
            EmailTemplates.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_EmailTemplates_ExportToExcel, L("ExportToExcelEmailTemplates"), multiTenancySides: MultiTenancySides.Tenant);

            //SolarType
            var solarTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_SolarType);
            var SolarType = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SolarType, L("SolarType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: solarTypeFeatureDependency);
            SolarType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SolarType_Create, L("CreatingNewSolarType"), multiTenancySides: MultiTenancySides.Tenant);
            SolarType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SolarType_Edit, L("EditingSolarType"), multiTenancySides: MultiTenancySides.Tenant);
            SolarType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SolarType_Delete, L("DeletingSolarType"), multiTenancySides: MultiTenancySides.Tenant);
            SolarType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_SolarType_ExportToExcel, L("ExportToExcelSolarType"), multiTenancySides: MultiTenancySides.Tenant);

            //HeightStructure
            var heightStructureFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_HeightStructure);
            var HeightStructure = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HeightStructure, L("HeightStructure"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: heightStructureFeatureDependency);
            HeightStructure.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_Create, L("CreatingNewHeightStructure"), multiTenancySides: MultiTenancySides.Tenant);
            HeightStructure.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_Edit, L("EditingHeightStructure"), multiTenancySides: MultiTenancySides.Tenant);
            HeightStructure.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_Delete, L("DeletingHeightStructure"), multiTenancySides: MultiTenancySides.Tenant);
            HeightStructure.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_HeightStructure_ExportToExcel, L("ExportToExcelHeightStructure"), multiTenancySides: MultiTenancySides.Tenant);


            //Price
            var priceFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Prices);
            var price = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Prices, L("Prices"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: priceFeatureDependency);
            price.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Prices_Create, L("CreatingNewPrices"), multiTenancySides: MultiTenancySides.Tenant);
            price.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Prices_Edit, L("EditingPrices"), multiTenancySides: MultiTenancySides.Tenant);
            price.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Prices_Delete, L("DeletingPrices"), multiTenancySides: MultiTenancySides.Tenant);
            price.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Prices_ExportToExcel, L("ExportToExcelPrices"), multiTenancySides: MultiTenancySides.Tenant);
            price.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Prices_ImportToExcel, L("ImportToExcelPrices"), multiTenancySides: MultiTenancySides.Tenant);

            //Circle
            var circleFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Circle);
            var circle = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Circle, L("Circle"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: circleFeatureDependency);
            circle.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Circle_Create, L("CreatingNewCircle"), multiTenancySides: MultiTenancySides.Tenant);
            circle.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Circle_Edit, L("EditingCircle"), multiTenancySides: MultiTenancySides.Tenant);
            circle.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Circle_Delete, L("DeletingCircle"), multiTenancySides: MultiTenancySides.Tenant);
            circle.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Circle_ExportToExcel, L("ExportToExcelCircle"), multiTenancySides: MultiTenancySides.Tenant);

            //Tender
            var tenderFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Tender);
            var tender = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Tender, L("Tender"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: tenderFeatureDependency);
            tender.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Tender_Create, L("CreatingNewTender"), multiTenancySides: MultiTenancySides.Tenant);
            tender.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Tender_Edit, L("EditingTender"), multiTenancySides: MultiTenancySides.Tenant);
            tender.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Tender_Delete, L("DeletingTender"), multiTenancySides: MultiTenancySides.Tenant);
            tender.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Tender_ExportToExcel, L("ExportToExcelTender"), multiTenancySides: MultiTenancySides.Tenant);

            //Data Vault ActivityLog
            var ActivityLogMasterVaultFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_ActivityLogMasterVault);
            var ActivityLogMasterVault = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ActivityLogMasterVault, L("ActivityLogMasterVault"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: ActivityLogMasterVaultFeatureDependency);
            //ActivityLogMasterVault.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ActivityLogMasterVault_Create, L("CreatingNewActivityLogMasterVault"), multiTenancySides: MultiTenancySides.Tenant);
            //ActivityLogMasterVault.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ActivityLogMasterVault_Edit, L("EditingActivityLogMasterVault"), multiTenancySides: MultiTenancySides.Tenant);
            //ActivityLogMasterVault.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ActivityLogMasterVault_Delete, L("DeletingActivityLogMasterVault"), multiTenancySides: MultiTenancySides.Tenant);
            ActivityLogMasterVault.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_ActivityLogMasterVault_ExportToExcel, L("ExportToExcelActivityLogMasterVault"), multiTenancySides: MultiTenancySides.Tenant);

            //PDFTemplate
            var pdfTemplatFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_CancelReasons);
            var pdfTemplate = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PDFTemplate, L("PDFTemplate"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: pdfTemplatFeatureDependency);
            pdfTemplate.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PDFTemplate_Create, L("CreatingNewPDFTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            pdfTemplate.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PDFTemplate_Edit, L("EditingPDFTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            pdfTemplate.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PDFTemplate_Delete, L("DeletingPDFTemplate"), multiTenancySides: MultiTenancySides.Tenant);
            pdfTemplate.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PDFTemplate_ExportToExcel, L("ExportToExcelPDFTemplate"), multiTenancySides: MultiTenancySides.Tenant);


            //Lead Status
            var leadstatusFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_LeadStatus);
            var Leadstatus = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadStatus, L("LeadStatus"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadstatusFeatureDependency);
            Leadstatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Create, L("CreatingNewLeadStatus"), multiTenancySides: MultiTenancySides.Tenant);
            Leadstatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Edit, L("EditingLeadStatus"), multiTenancySides: MultiTenancySides.Tenant);
            Leadstatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_Delete, L("DeletingLeadStatus"), multiTenancySides: MultiTenancySides.Tenant);
            Leadstatus.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_LeadStatus_ExportToExcel, L("ExportToExcelLeadStatus"), multiTenancySides: MultiTenancySides.Tenant);

            //Booll Of Payments
            var billofMaterialFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_BillOfMaterial);
            var BillOfMaterial = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial, L("BillOfMaterial"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: billofMaterialFeatureDependency);
            BillOfMaterial.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Create, L("CreatingNewBillOfMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            BillOfMaterial.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Edit, L("EditingBillOfMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            BillOfMaterial.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_Delete, L("DeletingBillOfMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            BillOfMaterial.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_ExportToExcel, L("ExportToExcelBillOfMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            BillOfMaterial.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_ExportToExcelSample, L("ExportToExcelSampleBillOfMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            BillOfMaterial.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_BillOfMaterial_ImportToExcel, L("ImportToExcelBillOfMaterial"), multiTenancySides: MultiTenancySides.Tenant);

            var leadsFeatureDependency = new SimpleFeatureDependency(AppFeatures.Leads);
            var leads = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Leads, L("Leads"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: leadsFeatureDependency);

            //Customer
            var customersFeatureDependency = new SimpleFeatureDependency(AppFeatures.Leads_Customers);
            var Customers = leads.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer, L("Customer"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: customersFeatureDependency);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_Create, L("CreatingNewCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_Edit, L("EditingCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_Delete, L("DeletingCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_ExportToExcel, L("ExportToExcelCustomer"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_Assign, L("CustomerAssign"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ChangeOrganization, L("CustomerChangeOrganization"), multiTenancySides: MultiTenancySides.Tenant);

            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_Summary, L("CustomerLeadSummary"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_LeadAddActivity, L("CustomerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_LeadActivity, L("CustomerLeadActivity"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_ViewMap, L("CustomerViewMap"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_ViewDocument, L("CustomerViewDocument"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_UploadDocument, L("CustomerUploadDocument"), multiTenancySides: MultiTenancySides.Tenant);
            Customers.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_Customer_DeleteDocument, L("CustomerDeleteDocument"), multiTenancySides: MultiTenancySides.Tenant);
            //manageLeads
            var manageLeadsFeatureDependency = new SimpleFeatureDependency(AppFeatures.Leads_ManageLead);
            var ManageLead = leads.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead, L("ManageLeads"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: manageLeadsFeatureDependency);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_Create, L("CreatingNewManageLeads"), multiTenancySides: MultiTenancySides.Tenant);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_Edit, L("EditingManageLead"), multiTenancySides: MultiTenancySides.Tenant);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_Delete, L("DeletingManageLead"), multiTenancySides: MultiTenancySides.Tenant);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_ExportToExcel, L("ExportToExcelManageLead"), multiTenancySides: MultiTenancySides.Tenant);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_Assign, L("ManageLeadAssign"), multiTenancySides: MultiTenancySides.Tenant);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_ChangeOrganization, L("ManageLeadChangeOrganization"), multiTenancySides: MultiTenancySides.Tenant);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_ImportToExcel, L("ImportToExcelManageLead"), multiTenancySides: MultiTenancySides.Tenant);
            ManageLead.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_Action, L("ManageLeadAction"), multiTenancySides: MultiTenancySides.Tenant);

            //Jobs
            var jobsFeatureDependency = new SimpleFeatureDependency(AppFeatures.Jobs);
            var jobs = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs, L("Jobs"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: jobsFeatureDependency);

            ////Jobs sales
            var salesJobFeatureDependency = new SimpleFeatureDependency(AppFeatures.Jobs_Sales);
            var salesJob = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Sales, L("SalesJob"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: salesJobFeatureDependency);
            salesJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Sales_Create, L("CreatingNewSalesJob"), multiTenancySides: MultiTenancySides.Tenant);
            salesJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Sales_Edit, L("EditingSalesJob"), multiTenancySides: MultiTenancySides.Tenant);
            salesJob.CreateChildPermission(AppPermissions.Pages_Tenant_jobs_Sales_Delete, L("DeletingSalesJob"), multiTenancySides: MultiTenancySides.Tenant);
            salesJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Sales_ExportToExcel, L("ExportToExcelSalesJob"), multiTenancySides: MultiTenancySides.Tenant);

            ////Jobs quotation
            var quotationJobFeatureDependency = new SimpleFeatureDependency(AppFeatures.Jobs_Quotation);
            var quotationJob = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Quotation, L("QuotationJob"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: quotationJobFeatureDependency);
            quotationJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Quotation_Create, L("CreatingNewQuotationJob"), multiTenancySides: MultiTenancySides.Tenant);
            quotationJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Quotation_Edit, L("EditingQuotationJob"), multiTenancySides: MultiTenancySides.Tenant);
            quotationJob.CreateChildPermission(AppPermissions.Pages_Tenant_jobs_Quotation_Delete, L("DeletingQuotationJob"), multiTenancySides: MultiTenancySides.Tenant);
            quotationJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Quotation_ExportToExcel, L("ExportToExcelQuotationJob"), multiTenancySides: MultiTenancySides.Tenant);

            ////Jobs Installation
            var InstallationJobFeatureDependency = new SimpleFeatureDependency(AppFeatures.Jobs_Installation);
            var InstallationJob = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Installation, L("InstallationJob"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: InstallationJobFeatureDependency);
            InstallationJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Installation_Create, L("CreatingNewInstallationJob"), multiTenancySides: MultiTenancySides.Tenant);
            InstallationJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Installation_Edit, L("EditingInstallationJob"), multiTenancySides: MultiTenancySides.Tenant);
            InstallationJob.CreateChildPermission(AppPermissions.Pages_Tenant_jobs_Installation_Delete, L("DeletingInstallationJob"), multiTenancySides: MultiTenancySides.Tenant);
            //InstallationJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Installation_ExportToExcel, L("ExportToExcelQuotationJob"), multiTenancySides: MultiTenancySides.Tenant);

            ////Jobs Payment
            var paymentJobFeatureDependency = new SimpleFeatureDependency(AppFeatures.Jobs_Payment);
            var paymentJob = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Payment, L("paymentJob"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: paymentJobFeatureDependency);
            paymentJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Payment_Create, L("CreatingNewPaymentJob"), multiTenancySides: MultiTenancySides.Tenant);
            paymentJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Payment_Edit, L("EditingPaymentJob"), multiTenancySides: MultiTenancySides.Tenant);
            paymentJob.CreateChildPermission(AppPermissions.Pages_Tenant_jobs_Payment_Delete, L("DeletingPaymentJob"), multiTenancySides: MultiTenancySides.Tenant);
            paymentJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Payment_ExportToExcel, L("ExportToExcelPaymentJob"), multiTenancySides: MultiTenancySides.Tenant);

            ////Jobs PaymentRefund
            var paymentRefundJobFeatureDependency = new SimpleFeatureDependency(AppFeatures.Jobs_PaymentRefund);
            var paymentRefundJob = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Refund, L("PaymentRefundJob"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: paymentRefundJobFeatureDependency);
            paymentRefundJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Refund_Create, L("CreatingNewPaymentRefundJob"), multiTenancySides: MultiTenancySides.Tenant);
            paymentRefundJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Refund_Edit, L("EditingPaymentRefundJob"), multiTenancySides: MultiTenancySides.Tenant);
            paymentRefundJob.CreateChildPermission(AppPermissions.Pages_Tenant_jobs_Refund_Delete, L("DeletingPaymentRefundJob"), multiTenancySides: MultiTenancySides.Tenant);
            //paymentRefundJob.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Refund_ExportToExcel, L("ExportToExcelPaymentRefundJob"), multiTenancySides: MultiTenancySides.Tenant);

            // job JobType
            var jobTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.Jobs_JobType);
            var jobType = jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_JobType, L("JobType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: jobTypeFeatureDependency);
            jobType.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_JobType_JobViewDetails, L("JobViewDetailsJobType"), multiTenancySides: MultiTenancySides.Tenant);
            jobType.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_JobType_ExportToExcel, L("ExportToExcelJobType"), multiTenancySides: MultiTenancySides.Tenant);
            jobType.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_JobType_Reminder, L("ReminderJobType"), multiTenancySides: MultiTenancySides.Tenant);
            jobType.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_JobType_SMS, L("SMSJobType"), multiTenancySides: MultiTenancySides.Tenant);
            jobType.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_JobType_ToDoList, L("ToDoListJobType"), multiTenancySides: MultiTenancySides.Tenant);
            jobType.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_JobType_Comment, L("CommentJobType"), multiTenancySides: MultiTenancySides.Tenant);
            
            //MisReport
            var MisReportFeatureDependency = new SimpleFeatureDependency(AppFeatures.Leads_MISReport);
            var MISReport = leads.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_MISReport, L("MISReport"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: MisReportFeatureDependency);
            MISReport.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_MISReport_ImportToExcel, L("ImportToExcelMISReport"), multiTenancySides: MultiTenancySides.Tenant);

            // MISReport.CreateChildPermission(AppPermissions.Pages_Tenant_Leads_ManageLead_Edit, L("EditingManageLead"), multiTenancySides: MultiTenancySides.Tenant);


            //Department
            var departmentFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Department);
            var Department = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Department, L("Department"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: departmentFeatureDependency);
            Department.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Department_Create, L("CreatingNewDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            Department.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Department_Edit, L("EditingDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            Department.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Department_Delete, L("DeletingDepartment"), multiTenancySides: MultiTenancySides.Tenant);
            Department.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Department_ExportToExcel, L("ExportToExcelDepartment"), multiTenancySides: MultiTenancySides.Tenant);


            //Variation
            var variationFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_Variation);
            var Variation = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Variation, L("Variation"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: variationFeatureDependency);
            Variation.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Variation_Create, L("CreatingNewVariation"), multiTenancySides: MultiTenancySides.Tenant);
            Variation.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Variation_Edit, L("EditingVariation"), multiTenancySides: MultiTenancySides.Tenant);
            Variation.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Variation_Delete, L("DeletingVariation"), multiTenancySides: MultiTenancySides.Tenant);
            Variation.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_Variation_ExportToExcel, L("ExportToExcelVariation"), multiTenancySides: MultiTenancySides.Tenant);

            //CustomeTag
            var customeTagFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_CustomeTag);
            var CustomeTag = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CustomeTag, L("CustomeTag"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: customeTagFeatureDependency);
            CustomeTag.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_Create, L("CreatingNewCustomeTag"), multiTenancySides: MultiTenancySides.Tenant);
            CustomeTag.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_Edit, L("EditingCustomeTag"), multiTenancySides: MultiTenancySides.Tenant);
            CustomeTag.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_Delete, L("DeletingCustomeTag"), multiTenancySides: MultiTenancySides.Tenant);
            CustomeTag.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_CustomeTag_ExportToExcel, L("ExportToExcelCustomeTag"), multiTenancySides: MultiTenancySides.Tenant);

            //PaymentMode
            var paymentModeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_PaymentMode);
            var PaymentMode = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentMode, L("PaymentMode"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: paymentModeFeatureDependency);
            PaymentMode.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_Create, L("CreatingNewPaymentMode"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentMode.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_Edit, L("EditingPaymentMode"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentMode.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_Delete, L("DeletingPaymentMode"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentMode.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentMode_ExportToExcel, L("ExportToExcelPaymentMode"), multiTenancySides: MultiTenancySides.Tenant);

            //PaymentType
            var paymentTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_PaymentType);
            var PaymentType = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentType, L("PaymentType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: paymentTypeFeatureDependency);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentType_Create, L("CreatingNewPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentType_Edit, L("EditingPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentType_Delete, L("DeletingPaymentType"), multiTenancySides: MultiTenancySides.Tenant);
            PaymentType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_PaymentType_ExportToExcel, L("ExportToExcelPaymentType"), multiTenancySides: MultiTenancySides.Tenant);

            //RefundReasons
            var refundReasonFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_RefundReasons);
            var RefundReason = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RefundReasons, L("RefundReason"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: refundReasonFeatureDependency);
            RefundReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_Create, L("CreatingNewRefundReason"), multiTenancySides: MultiTenancySides.Tenant);
            RefundReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_Edit, L("EditingRefundReason"), multiTenancySides: MultiTenancySides.Tenant);
            RefundReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_Delete, L("DeletingRefundReason"), multiTenancySides: MultiTenancySides.Tenant);
            RefundReason.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_RefundReasons_ExportToExcel, L("ExportToExcelRefundReason"), multiTenancySides: MultiTenancySides.Tenant);

            //dispatchType
            var dispatchTypeFeatureDependency = new SimpleFeatureDependency(AppFeatures.DataVaults_DispatchType);
            var DispatchType = dataVaults.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DispatchType, L("DispatchType"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: dispatchTypeFeatureDependency);
            DispatchType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DispatchType_Create, L("CreatingNewDispatchType"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DispatchType_Edit, L("EditingDispatchType"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DispatchType_Delete, L("DeletingDispatchType"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchType.CreateChildPermission(AppPermissions.Pages_Tenant_DataVaults_DispatchType_ExportToExcel, L("ExportToExcelDispatchType"), multiTenancySides: MultiTenancySides.Tenant);


            //Tracker
            var trackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker);
            var tracker = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker, L("Tracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: trackerFeatureDependency);
            //Application Tracker
            var applicationTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_Application);
            var ApplicationTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application, L("ApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: applicationTrackerFeatureDependency);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_LeadViewDetails, L("LeadViewDetailsApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_LeadAddActivity, L("LeadAddActivityApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_QuickView, L("QuickViewApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_ApplicationDetails, L("ApplicationDetailsTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_ApplicationReadyStatus, L("ApplicationReadyStatusTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_OTPVerifyStatus, L("OTPVerifyStatusApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_UploadSignApplication, L("UploadSignApplicationApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_LoadReduce, L("LoadReduceApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_Reminder, L("ReminderApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_SMS, L("SMSApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_ToDoList, L("ToDoListApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_Comment, L("CommentApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);
            ApplicationTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Application_ExportToExcel, L("ExportToExcelApplicationTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //EstimatedPaid Tracker
            var estimatedPaidTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_EstimatedPaid);
            var EstimatedPaidTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid, L("EstimatedPaidTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: estimatedPaidTrackerFeatureDependency);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_LeadViewDetails, L("LeadViewDetailsEstimatedPaidTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_LeadAddActivity, L("LeadAddActivityEstimatedPaidTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_QuickView, L("QuickViewEstimatedPaidTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_ApplicationDetails, L("EstimatedPaidDetailsTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_Reminder, L("ReminderEstimatedPaidTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_Notify, L("SMSEstimatedTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_Comment, L("CommentEstimatedPaidTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_ExportToExcel, L("ExportToExcelEstimatedPaidTracker"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_ChangePayResponse, L("AddPayResponse"), multiTenancySides: MultiTenancySides.Tenant);
            EstimatedPaidTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_EstimatedPaid_PaymentReciept, L("AddPaymentReciept"), multiTenancySides: MultiTenancySides.Tenant);

            
            //Refund Tracker
            var refundTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_Refund);
            var RefundTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund, L("RefundTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: refundTrackerFeatureDependency);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_LeadViewDetails, L("RefundTrackerLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_LeadAddActivity, L("RefundTrackerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_QuickView, L("QuickViewRefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_Reminder, L("ReminderRefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_Notify, L("SMSRefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_Comment, L("CommentRefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_ToDo, L("ToDoRefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
            RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Refund_ExportToExcel, L("ExportToExcelRefundTracker"), multiTenancySides: MultiTenancySides.Tenant);
            //RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_SelectMaterial, L("DispatchSelectMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            //RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_SendExtraMaterial, L("DispatchSendExtraMaterial"), multiTenancySides: MultiTenancySides.Tenant);
            //RefundTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_StockTransfer, L("DispatchStockTransfer"), multiTenancySides: MultiTenancySides.Tenant);

            //Deposit Tracker
            var depositeTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_Deposit);
            var DepositeTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit, L("DepositTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: depositeTrackerFeatureDependency);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_LeadViewDetails, L("DepositTrackerLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_LeadAddActivity, L("DepositTrackerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_QuickView, L("QuickViewDepositTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_Reminder, L("ReminderDepositTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_Notify, L("SMSDepositTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_Comment, L("CommentDepositTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_ToDo, L("ToDoDepositTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DepositeTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Deposit_ExportToExcel, L("ExportToExcelDepositTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //dispatch Tracker
            var dispatchTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_Dispatch);
            var DispatchTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch, L("DispatchTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: dispatchTrackerFeatureDependency);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_LeadViewDetails, L("DispatchTrackerLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_LeadAddActivity, L("DispatchTrackerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_QuickView, L("QuickViewDispatchTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_Reminder, L("ReminderDispatchTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_Notify, L("SMSDispatchTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_Comment, L("CommentDispatchTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_ToDo, L("ToDoDispatchTracker"), multiTenancySides: MultiTenancySides.Tenant);
            DispatchTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Dispatch_ExportToExcel, L("ExportToExcelDispatchTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //picklist Tracker
            var picklistTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_PickList);
            var PickListTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList, L("PickListTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: picklistTrackerFeatureDependency);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_LeadViewDetails, L("PickListTrackerLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_LeadAddActivity, L("PickListTrackerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_QuickView, L("QuickViewPickListTracker"), multiTenancySides: MultiTenancySides.Tenant);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_Reminder, L("ReminderPickListTracker"), multiTenancySides: MultiTenancySides.Tenant);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_Notify, L("SMSPickListTracker"), multiTenancySides: MultiTenancySides.Tenant);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_Comment, L("CommentPickListTracker"), multiTenancySides: MultiTenancySides.Tenant);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_ToDo, L("ToDoPickListTracker"), multiTenancySides: MultiTenancySides.Tenant);
            PickListTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_PickList_ExportToExcel, L("ExportToExcelPickListTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //Transport Tracker
            var transportTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_Transport);
            var TransportTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport, L("TransportTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: transportTrackerFeatureDependency);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_LeadViewDetails, L("TransportTrackerLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_LeadAddActivity, L("TransportTrackerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_QuickView, L("QuickViewTransportTracker"), multiTenancySides: MultiTenancySides.Tenant);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_Reminder, L("ReminderTransportTracker"), multiTenancySides: MultiTenancySides.Tenant);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_Notify, L("SMSTransportTracker"), multiTenancySides: MultiTenancySides.Tenant);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_Comment, L("CommentTransportTracker"), multiTenancySides: MultiTenancySides.Tenant);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_ToDo, L("ToDoTransportTracker"), multiTenancySides: MultiTenancySides.Tenant);
            TransportTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_Transport_ExportToExcel, L("ExportToExcelTransportTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //MeterConnect Tracker
            var meterConnectTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Tracker_MeterConnect);
            var MeterConnectTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect, L("MeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: meterConnectTrackerFeatureDependency);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_LeadViewDetails, L("MeterConnectTrackerLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_LeadAddActivity, L("MeterConnectTrackerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_QuickView, L("QuickViewMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_Reminder, L("ReminderMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_Notify, L("SMSMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_Comment, L("CommentMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_ToDo, L("ToDoMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_ExportToExcel, L("ExportToExcelMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_DownloadDocument, L("DownloadDocumentMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);
            MeterConnectTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_MeterConnect_SelfCertificate, L("SelfCertificateMeterConnectTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //Subsidy
            var subsidyFeatureDependency = new SimpleFeatureDependency(AppFeatures.Subsidy);
            var Subsidy = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Subsidy, L("Subsidy"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: subsidyFeatureDependency);


            //SubsidyDetail Tracker
            var subsidyDetailTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Subsidy_SubsidyDetail);
            var SubsidyDetailTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail, L("SubsidyDetailTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: subsidyDetailTrackerFeatureDependency);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_EditSubsidy, L("SubsidyDetailTrackerLeadEditSubsidy"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_VerifySubsidy, L("SubsidyDetailTrackerVerifySubsidy"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_UploadOCCopy, L("SubsidyDetailTrackerUploadOCCopy"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_Delete, L("SubsidyDetailTrackerDelete"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_QuickView, L("QuickViewSubsidyDetailTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_Reminder, L("ReminderSubsidyDetailTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_Notify, L("SMSSubsidyDetailTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_Comment, L("CommentSubsidyDetailTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_ToDo, L("ToDoSubsidyDetailTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyDetailTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyDetail_ExportToExcel, L("ExportToExcelSubsidyDetailTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //SubsidyClaim Tracker
            var subsidyClaimTrackerFeatureDependency = new SimpleFeatureDependency(AppFeatures.Subsidy_SubsidyClaim);
            var SubsidyClaimTracker = tracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim, L("SubsidyClaimTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: subsidyClaimTrackerFeatureDependency);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_LeadViewDetails, L("SubsidyClaimTrackerLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_LeadAddActivity, L("SubsidyClaimTrackerLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_QuickView, L("QuickViewSubsidyClaimTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_Reminder, L("ReminderSubsidyClaimTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_Notify, L("SMSSubsidyClaimTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_Comment, L("CommentSubsidyClaimTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_ToDo, L("ToDoSubsidyClaimTracker"), multiTenancySides: MultiTenancySides.Tenant);
            SubsidyClaimTracker.CreateChildPermission(AppPermissions.Pages_Tenant_Tracker_SubsidyClaim_ExportToExcel, L("ExportToExcelSubsidyClaimTracker"), multiTenancySides: MultiTenancySides.Tenant);

            //Account
            var accountsFeatureDependency = new SimpleFeatureDependency(AppFeatures.Accounts);
            var accounts = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts, L("Accounts"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: accountsFeatureDependency);

            //PaymentIssued 
            var paymentIssuedFeatureDependency = new SimpleFeatureDependency(AppFeatures.Accounts_PaymentIssued);
            var paymentissued = accounts.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued, L("PaymentIssuedAccounts"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: paymentIssuedFeatureDependency);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_LeadViewDetails, L("LeadViewDetailsPaymentIssuedAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_LeadAddActivity, L("LeadAddActivityPaymentIssuedAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_QuickView, L("QuickViewPaymentIssuedAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_Reminder, L("ReminderPaymentIssuedAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_Notify, L("NotifyEstimatedPaidAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_ToDoList, L("ToDoListPaymentIssuedAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_Comment, L("CommentEstimatedPaidAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_ExportToExcel, L("ExportToExcelPaymentIssuedAccounts"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_SSBillPaymentPDFView, L("SSBillPaymentPDFView"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_STCBillPaymentPDFView, L("STCBillPaymentPDFView"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_SSBillPaymentView, L("SSBillPaymentView"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_STCBillPaymentView, L("STCBillPaymentView"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_DeliveryPaymentView, L("DeliveryPaymentView"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_RemoveBillSSorSTC, L("RemoveBillSSorSTC"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_DispatchStatus, L("DispatchStatus"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_SSBillAddorEdit, L("SSBillAddorEdit"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_STCBillAddorEdit, L("STCBillAddorEdit"), multiTenancySides: MultiTenancySides.Tenant);
            paymentissued.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentIssued_DeliverPaymentAddorEdit, L("DeliverPaymentAddorEdit"), multiTenancySides: MultiTenancySides.Tenant);

            //Paymentpaid 
            var paymentPaidFeatureDependency = new SimpleFeatureDependency(AppFeatures.Accounts_PaymentPaid);
            var paymentPaid = accounts.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid, L("PaymentPaidTracker"), multiTenancySides: MultiTenancySides.Tenant, featureDependency: paymentPaidFeatureDependency);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_LeadViewDetails, L("PaymentPaidLeadViewDetails"), multiTenancySides: MultiTenancySides.Tenant);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_LeadAddActivity, L("PaymentPaidLeadAddActivity"), multiTenancySides: MultiTenancySides.Tenant);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_QuickView, L("QuickViewPaymentpaid"), multiTenancySides: MultiTenancySides.Tenant);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_Reminder, L("ReminderPaymentPaid"), multiTenancySides: MultiTenancySides.Tenant);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_Notify, L("NotifyPaymentPaid"), multiTenancySides: MultiTenancySides.Tenant);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_ToDoList, L("ToDoListPaymentPaid"), multiTenancySides: MultiTenancySides.Tenant);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_Comment, L("CommentPaymentPaid"), multiTenancySides: MultiTenancySides.Tenant);
            paymentPaid.CreateChildPermission(AppPermissions.Pages_Tenant_Accounts_PaymentPaid_ExportToExcel, L("ExportToExcelPaymentPaid"), multiTenancySides: MultiTenancySides.Tenant);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, solarcrmConsts.LocalizationSourceName);
        }
    }
}
