﻿using Abp.Authorization;
using solarcrm.Authorization.Roles;
using solarcrm.Authorization.Users;

namespace solarcrm.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
