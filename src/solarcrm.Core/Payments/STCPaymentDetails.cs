﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Authorization.Users;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments
{
    [Table("STCPaymentDetails")]
    public class STCPaymentDetails : FullAuditedEntity
    {
        public virtual int STCPaymentId { get; set; }

        [ForeignKey("STCPaymentId")]
        public virtual PaymentDetailsJob PaymentIdFK { get; set; }

        public virtual DateTime? STCDate { get; set; }

        public virtual int? STCPaymentApprovedById { get; set; }

        //[ForeignKey("STCPaymentApprovedById")]
        //public virtual User UserIdFK { get; set; }
        [StringLength(PaymentDetailsConsts.PaymentSTCNotes, MinimumLength = PaymentDetailsConsts.MinNameLength)]
        public virtual string STCNotes { get; set; }

    }
}
