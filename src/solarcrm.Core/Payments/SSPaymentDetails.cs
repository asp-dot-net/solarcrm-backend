﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Authorization.Users;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments
{
    [Table("SSPaymentDetails")]
    public class SSPaymentDetails : FullAuditedEntity
    {
        public virtual int SSPaymentId { get; set; }

        [ForeignKey("SSPaymentId")]
        public virtual PaymentDetailsJob PaymentIdFK { get; set; }

        public virtual DateTime? SSDate { get; set; }

        [StringLength(PaymentDetailsConsts.SSGSTNo, MinimumLength = PaymentDetailsConsts.MinNameLength)]
        public virtual string SSGSTNo { get; set; }

        public virtual int SSPaymentApprovedById { get; set; }

        //[ForeignKey("SSPaymentApprovedById")]
        //public virtual User UserIdFK { get; set; }
        [StringLength(PaymentDetailsConsts.PaymentSSNotes, MinimumLength = PaymentDetailsConsts.MinNameLength)]
        public virtual string SSNotes { get; set; }

        //public virtual decimal HSN_SAC { get; set; }
    }
}
