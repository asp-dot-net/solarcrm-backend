﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments
{
    [Table("PaymentDetailsJob")]
    public class PaymentDetailsJob : FullAuditedEntity
    {
        public virtual int PaymentJobId { get; set; }

        [ForeignKey("PaymentJobId")]
        public virtual Job.Jobs JobsIdFK { get; set; }

        public virtual  decimal? LessSubsidy { get; set; }

        public virtual decimal? PaidTillDate { get; set; }

        public virtual decimal? NetAmount { get; set; }

        public virtual decimal? BalanceOwing { get; set; }
        public virtual bool? PaymentDeliveryOption { get; set; }

        [StringLength(PaymentDetailsConsts.PaymentAccountInvoiceNotes, MinimumLength = PaymentDetailsConsts.MinNameLength)]
        public virtual string PaymentAccountInvoiceNotes { get; set; }

    }
}
