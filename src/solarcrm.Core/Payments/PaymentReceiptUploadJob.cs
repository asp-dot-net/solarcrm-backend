﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Authorization.Users;
using solarcrm.Consts;
using solarcrm.DataVaults.PaymentMode;
using solarcrm.DataVaults.PaymentType;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Payments
{
    [Table("PaymentReceiptUploadJob")]
    public class PaymentReceiptUploadJob : FullAuditedEntity
    {
        public virtual int PaymentId { get; set; }

        [ForeignKey("PaymentId")]
        public virtual PaymentDetailsJob PaymentIdFK { get; set; }

        public virtual DateTime? PaymentRecEnterDate { get; set; }
        public virtual DateTime? PaymentRecPaidDate { get; set; }

        [StringLength(PaymentReceiptConsts.PaymentRecRefNo, MinimumLength = PaymentReceiptConsts.MinNameLength)]
        public virtual string PaymentRecRefNo { get; set; }

        public virtual decimal? PaymentRecTotalPaid { get; set; }

        public virtual int? PaymentRecTypeId { get; set; }

        [ForeignKey("PaymentRecTypeId")]
        public virtual PaymentType PaymentTypeIdFK { get; set; }

        public virtual int? PaymentRecModeId { get; set; }

        [ForeignKey("PaymentRecModeId")]
        public virtual PaymentMode PaymentModeIdFK { get; set; }

        [StringLength(PaymentReceiptConsts.PaymentRecChequeStatus, MinimumLength = PaymentReceiptConsts.MinNameLength)]
        public virtual string PaymentRecChequeStatus { get; set; }

        [StringLength(PaymentReceiptConsts.PaymentReceiptName, MinimumLength = PaymentReceiptConsts.MinNameLength)]
        public virtual string PaymentReceiptName { get; set; }

        [StringLength(PaymentReceiptConsts.PaymentReceiptPath, MinimumLength = PaymentReceiptConsts.MinNameLength)]
        public virtual string PaymentReceiptPath { get; set; }

        public virtual int? PaymentRecAddedById { get; set; }

        public virtual bool IsPaymentRecVerified { get; set; }

        public virtual int? PaymentRecVerifiedById { get; set; }

        [StringLength(PaymentReceiptConsts.PaymentRecNotes, MinimumLength = PaymentReceiptConsts.MinNameLength)]
        public virtual string PaymentRecNotes { get; set; }
    }
}
