﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DocumentRequests
{
    [Table("DocumentRequestLinkHistorys")]
    public class DocumentRequestLinkHistory : FullAuditedEntity
    {
        public virtual string Token { get; set; }

        public virtual int? DocumentRequestId { get; set; }

        public virtual bool? Expired { get; set; }
    }
}
