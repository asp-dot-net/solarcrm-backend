﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.DocumentList;
using solarcrm.DataVaults.Leads;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace solarcrm.DocumentRequests
{
    [Table("DocumentRequests")]
    public class DocumentRequest : FullAuditedEntity
    {
        public int LeadId { get; set; }

        [ForeignKey("LeadId")]
        public Leads LeadFk { get; set; }

        public int DocTypeId { get; set; }

        [ForeignKey("DocTypeId")]
        public DocumentList DocTypeFk { get; set; }

        public bool IsSubmitted { get; set; }
    }
}
