﻿ABOUT LOCALIZATION FILES
------------------------------------------------------------
This folder contains localization files for the application.

You can add more languages here with appropriate postfixes.
For instance, to add a German localization, you can add "solarcrm-de.xml" or "solarcrm-de-DE.xml".

See http://www.aspnetboilerplate.com/Pages/Documents/Localization for more information.
