﻿using System.Globalization;

namespace solarcrm.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}