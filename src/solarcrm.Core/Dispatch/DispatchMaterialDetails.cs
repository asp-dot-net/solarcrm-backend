﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.DispatchType;
using solarcrm.DataVaults.HeightStructure;
using solarcrm.DataVaults.StockItem;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace solarcrm.Dispatch
{
    [Table("DispatchMaterialDetails")]
    public class DispatchMaterialDetails : FullAuditedEntity
    {
        public virtual int DispatchJobId { get; set; }
        [ForeignKey("DispatchJobId")]
        public virtual Job.Jobs DispatchJobsIdFK { get; set; }

        public virtual int? DispatchStockItemId { get; set; }
        [ForeignKey("DispatchStockItemId")]
        public virtual StockItem DispatchStockItemIdFK { get; set; }

        public virtual int? DispatchHeightStructureId { get; set; }
        [ForeignKey("DispatchHeightStructureId")]
        public virtual HeightStructure DispatchHeightStructureIdFK { get; set; }

        public virtual int? DispatchTypeId { get; set; }
        [ForeignKey("DispatchTypeId")]
        public virtual DispatchType DispatchTypeIdFK { get; set; }  
        
        public virtual DateTime DispatchDate { get;set; }       

        public virtual string DispatchNotes { get; set; }

        public virtual DateTime? DispatchReturnDate { get; set; }

        public virtual string DispatchReturnNotes { get; set; }

        [StringLength(500, MinimumLength = 1)]
        public string ExtraMaterialNotes { get; set; }
        public bool IsExtraMaterialApproved { get; set; }

        [StringLength(500, MinimumLength = 1)]
        public virtual string ExtraMaterialApprovedNotes { get; set; }
        public virtual bool IsActive { get; set; }

    }
}
