﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.Dispatch
{
    [Table("DispatchStockMaterialTransfer")]
    public class DispatchStockMaterialTransfer : FullAuditedEntity //, IMustHaveTenant
    {
        public virtual int DispatchId { get; set; }
        [ForeignKey("DispatchId")]
        public virtual DispatchMaterialDetails DispatchMaterialDetailsIdFK { get; set; }

        [StringLength(JobsConsts.JobNumber, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string JobNumber { get; set; }

        [StringLength(500, MinimumLength = 1)]
        public virtual string Reason { get; set; }
    }
}
