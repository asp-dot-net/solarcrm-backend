﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.StockItem;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Dispatch
{
    [Table("DispatchReturnMaterials")]
    public class DispatchReturnMaterials : FullAuditedEntity
    {
        public virtual int DispatchId { get; set; }
        [ForeignKey("DispatchId")]
        public virtual DispatchMaterialDetails DispatchMaterialDetailsIdFK { get; set; }

        public virtual int? DispatchStockItemId { get; set; }
        [ForeignKey("DispatchStockItemId")]
        public virtual StockItem StockItemIdFK { get; set; }

        public decimal ActualQuantity { get; set; }

    }
}
