﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Authorization.Users;
using solarcrm.Consts;
using solarcrm.DataVaults.StockItem;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace solarcrm.Dispatch
{
    [Table("DispatchExtraMaterials")]
    public class DispatchExtraMaterials : FullAuditedEntity
    {
        public virtual int DispatchId { get; set; }
        [ForeignKey("DispatchId")]
        public virtual DispatchMaterialDetails DispatchMaterialDetailsIdFK { get; set; }

        public virtual int? DispatchStockItemId { get; set; }
        [ForeignKey("DispatchStockItemId")]
        public virtual StockItem StockItemIdFK { get; set; }

        public decimal Quantity { get; set; }        

        public virtual long? UserManagerId { get; set; }
        [ForeignKey("UserManagerId")]
        public virtual User UserManagerIdFK { get; set; }

        
        public virtual long? MaterilApproveById { get; set; }
        [ForeignKey("MaterilApproveById")]
        public virtual User MaterialApproveByIdFK { get; set; }        
    }
}
