﻿using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using solarcrm.DataVaults.StockItem;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Dispatch
{
    [Table("DispatchProductItems")]
    public class DispatchProductItem : FullAuditedEntity
    {
        public virtual int? DispatchId { get; set; }
        [ForeignKey("DispatchId")]
        public DispatchMaterialDetails DispatchMaterialFk { get; set; }

        public virtual int? ProductItemId { get; set; }
        [ForeignKey("ProductItemId")]
        public StockItem ProductItemFk { get; set; }

        public virtual int? Quantity { get; set; }
    }
}
