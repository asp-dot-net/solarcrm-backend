﻿using Abp.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Organizations
{
    public class ExtendUserOrganizationUnit : UserOrganizationUnit
    {
        [StringLength(150)]
        public string UserWiseOrganization_Email { get; set; }
    }
}
