﻿using Abp.Domain.Entities.Auditing;
using solarcrm.DataVaults.LeadSource;
using solarcrm.DataVaults.StockItem;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Organizations
{
    [Table("MultipleFieldOrganizationUnitSelection")]
    public class MultipleFieldOrganizationUnitSelection : FullAuditedEntity
    {
        public virtual int? LeadSourceOrganizationId { get; set; }
        [ForeignKey("LeadSourceOrganizationId")]
        public LeadSource LeadSourceIdFk { get; set; }

        public virtual int? StockItemOrganizationId { get; set; }
        [ForeignKey("StockItemOrganizationId")]
        public StockItem StockItemIdFk { get; set; }

        public virtual long OrganizationUnitId { get; set; }        

        public virtual int? TenantId { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
