﻿using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Organizations
{
    [Table("BankDetailsOrganization")]
    public class BankDetailsOrganization : FullAuditedEntity
    {
        public virtual long BankDetailOrganizationId { get; set; }
        [ForeignKey("BankDetailOrganizationId")]
        public OrganizationUnit OrganizationIdFk { get; set; }

        [StringLength(LeadsConsts.BankAccountHolderNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string OrgBankName { get; set; }

         [StringLength(LeadsConsts.BankAccountHolderNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string OrgBankBrachName { get; set; }

        public virtual long? OrgBankAccountNumber { get; set; }

        [StringLength(LeadsConsts.BankAccountHolderNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string OrgBankAccountHolderName { get; set; }      

        [StringLength(LeadsConsts.IFSCCodeLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string OrgBankIFSC_Code { get; set; }
        [StringLength(LeadsConsts.BankQrCodePathLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string OrgBankQrCode_path { get; set; }

        public virtual int? TenantId { get; set; }
        public virtual bool IsActive { get; set; }

    }
}
