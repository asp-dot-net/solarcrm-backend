﻿using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Organizations
{
    public class ExtendOrganizationUnit : OrganizationUnit
    {

        [StringLength(100)]
        public string OrganizationCode { get; set; }

        [StringLength(50)]
        public string Organization_ProjectId { get; set; }

        [StringLength(50)]
        public string Organization_GSTNumber { get; set; }

        [StringLength(50)]
        public string Organization_PANNumber { get; set; }

        [StringLength(50)]
        public string Organization_CIN_Number { get; set; }

        [StringLength(300)]
        public string Organization_Address { get; set; }

        [StringLength(20)]
        public string Organization_ContactNo { get; set; }

        [StringLength(150)]
        public string Organization_defaultFromAddress { get; set; }

        [StringLength(150)]
        public string Organization_defaultFromDisplayName { get; set; }

        [StringLength(20)]
        public string Organization_Mobile { get; set; }
        
        [StringLength(150)]
        public string Organization_Email { get; set; }
        
        [StringLength(150)]
        public string Organization_LogoFileName { get; set; }
        
        [StringLength(250)]
        public string Organization_LogoFilePath { get; set; }
        
        [StringLength(50)]
        public string paymentMethod { get; set; }
        
        [StringLength(50)]
        public string cardNumber { get; set; }

        public string expiryDateMonth { get; set; }
        public string expiryDateYear { get; set; }
        [StringLength(10)]
        public string cvn { get; set; }
        [StringLength(50)]
        public string cardholderName { get; set; }
        [StringLength(50)]
        public string MerchantId { get; set; }
        [StringLength(50)]
        public string PublishKey { get; set; }
        [StringLength(50)]
        public string SecrateKey { get; set; }

        [StringLength(100)]
        public string Organization_Website { get; set; }

        public virtual string FoneDynamicsPhoneNumber { get; set; }

        public virtual string FoneDynamicsPropertySid { get; set; }

        public virtual string FoneDynamicsAccountSid { get; set; }

        public virtual string FoneDynamicsToken { get; set; }
        // public virtual SMS_ServiceProviderOrganization smsProvider { get; set; }


    }
}
