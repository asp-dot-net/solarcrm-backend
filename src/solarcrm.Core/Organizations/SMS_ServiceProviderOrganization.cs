﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.Organizations
{
    [Table("SMS_ServiceProviderOrganization")]
    public class SMS_ServiceProviderOrganization : FullAuditedEntity
    {
        public virtual long SMSProviderOrganizationId { get; set; }
        [ForeignKey("SMSProviderOrganizationId")]
        public OrganizationUnit OrganizationIdFk { get; set; }

        [StringLength(55)]
        public virtual string SMS_AuthorizationKey { get; set; }
                
        [StringLength(55)]
        public virtual string Client_Id { get; set; }

        [StringLength(55)]
        public virtual string Sender_Id { get; set; }

        [StringLength(55)]
        public virtual string msgtype { get; set; }

    }
}
