﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.DataVaults.BillOfMaterial;
using solarcrm.Dispatch;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.PickList
{
    [Table("PickListData")]
    public class PickListData : FullAuditedEntity
    {
        public virtual int PickListDisptchId { get; set; }
        [ForeignKey("PickListDisptchId")]
        public virtual DispatchMaterialDetails DispatchIdFK { get; set; }

        public virtual int BillOfMaterialId { get; set; }
        [ForeignKey("BillOfMaterialId")]
        public virtual BillOfMaterial BillOfMaterialIdFK { get; set; }

        public virtual int? PickListQty { get; set; }
    }
}
