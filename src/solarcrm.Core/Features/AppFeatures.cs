﻿namespace solarcrm.Features
{
    public static class AppFeatures
    {
        public const string MaxUserCount = "App.MaxUserCount";
        public const string ChatFeature = "App.ChatFeature";
        public const string TenantToTenantChatFeature = "App.ChatFeature.TenantToTenant";
        public const string TenantToHostChatFeature = "App.ChatFeature.TenantToHost";
        //public const string TestCheckFeature = "App.TestCheckFeature";
        //public const string TestCheckFeature2 = "App.TestCheckFeature2";
        //public const string Administration_DemoUIComponents = "App.Administration.DemoUIComponents";

        public const string DataVaults = "App.DataVaults";
        public const string DataVaults_Locations = "App.DataVaults.Locations";
        public const string DataVaults_LeadSource = "App.DataVaults.LeadSource";
        public const string DataVaults_LeadType = "App.DataVaults.LeadType";
        public const string DataVaults_LeadStatus = "App.DataVaults.LeadStatus";
        public const string DataVaults_DocumentList = "App.DataVaults.DocumentList";
        public const string DataVaults_Teams = "App.DataVaults.Teams";
        public const string DataVaults_Discom = "App.DataVaults.Discom";
        public const string DataVaults_Division = "App.DataVaults.Division";
        public const string DataVaults_SubDivision = "App.DataVaults.SubDivision";
        public const string DataVaults_TransactionType = "App.DataVaults.TransactionType";
        public const string DataVaults_StockItem = "App.DataVaults.StockItem";
        public const string DataVaults_ProjectType = "App.DataVaults.ProjectType";
        public const string DataVaults_ProjectStatus = "App.DataVaults.ProjectStatus";
        public const string DataVaults_StockCategory = "App.DataVaults.StockCategory";
        public const string DataVaults_InvoiceType = "App.DataVaults.InvoiceType";
        public const string DataVaults_State = "App.DataVaults.State";
        public const string DataVaults_District = "App.DataVaults.District";
        public const string DataVaults_Taluka = "App.DataVaults.Taluka";
        public const string DataVaults_City = "App.DataVaults.City";
        public const string DataVaults_CancelReasons = "App.DataVaults.CancelReasons";
        public const string DataVaults_HoldReasons = "App.DataVaults.HoldReasons";
        public const string DataVaults_RejectReasons = "App.DataVaults.RejectReasons";
        public const string DataVaults_JobCancellationReason = "App.DataVaults.JobCancellationReason";
        public const string DataVaults_DocumentLibrary = "App.DataVaults.DocumentLibrary";
        public const string DataVaults_EmailTemplates = "App.DataVaults.EmailTemplates";
        public const string DataVaults_SmsTemplates = "App.DataVaults.SmsTemplates";
        public const string DataVaults_SolarType = "App.DataVaults.SolarType";
        public const string DataVaults_HeightStructure = "App.DataVaults.HeightStructure";
        public const string DataVaults_Department = "App.DataVaults.Department";
        public const string DataVaults_Prices = "App.DataVaults.Prices";
        public const string DataVaults_Tender = "App.DataVaults.Tender";
        public const string DataVaults_Circle = "App.DataVaults.Circle";
        public const string DataVaults_ActivityLogMasterVault = "App.DataVaults.ActivityLogMasterVault";
        public const string DataVaults_Variation = "App.DataVaults.Variation";
        public const string DataVaults_CustomeTag = "App.DataVaults.CustomeTag";
        public const string DataVaults_PdfTemplates = "App.DataVaults.PdfTemplates";
        public const string DataVaults_PaymentMode = "App.DataVaults.PaymentMode";
        public const string DataVaults_PaymentType = "App.DataVaults.PaymentType";
        public const string DataVaults_BillOfMaterial = "App.DataVaults.BillOfMaterial";
        public const string DataVaults_RefundReasons = "App.DataVaults.RefundReasons";
        public const string DataVaults_DispatchType = "App.DataVaults.DispatchType";
        public const string DataVaults_Vehical = "App.DataVaults.Vehical";
        public const string DataVaults_PDFTemplate = "App.DataVaults.PDFTemplate";

        public const string Leads = "App.Leads";
        public const string Leads_Customers = "App.Leads.Customers";
        public const string Leads_ManageLead = "App.Leads.ManageLead";
        public const string Leads_MISReport = "App.Leads.MISReport";

        public const string Jobs = "App.Jobs";
        public const string Jobs_JobType = "App.Jobs.JobType";
        public const string Jobs_Sales = "App.Jobs.Sales";
        public const string Jobs_Quotation = "App.Jobs.Quotation";
        public const string Jobs_Installation = "App.Jobs.Installation";
        public const string Jobs_Payment = "App.Jobs.Payment";
        public const string Jobs_PaymentRefund = "App.Jobs.PaymentRefund";

        public const string Accounts = "App.Accounts";
        public const string Accounts_PaymentIssued = "App.Accounts.PaymentIssued";
        public const string Accounts_PaymentPaid = "App.Accounts.PaymentPaid";

        public const string Tracker = "App.Tracker";
        public const string Tracker_Application = "App.Tracker.Application";
        public const string Tracker_EstimatedPaid = "App.Tracker.EstimatedPaid";
        public const string Tracker_Dispatch = "App.Tracker.Dispatch";
        public const string Tracker_PaymentIssued = "App.Tracker.PaymentIssued";
        public const string Tracker_PaymentPaid = "App.Tracker.PaymentPaid";        
        public const string Tracker_SerialNoPhoto = "App.Tracker.SerialNoPhoto";
        public const string Tracker_Refund = "App.Tracker.Refund";
        public const string Tracker_PickList = "App.Tracker.PickList";
        public const string Tracker_Deposit = "App.Tracker.deposit";
        public const string Tracker_Installation = "App.Tracker.Installation";
        public const string Tracker_Transport = "App.Tracker.Transport";
        public const string Tracker_MeterConnect = "App.Tracker.MeterConnect";

        public const string Subsidy = "App.Subsidy";
        public const string Subsidy_SubsidyDetail = "App.Subsidy.SubsidyDetail";
        public const string Subsidy_SubsidyClaim = "App.Subsidy.SubsidyClaim";
    }
}