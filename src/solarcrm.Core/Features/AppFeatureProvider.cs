﻿using Abp.Application.Features;
using Abp.Localization;
using Abp.Runtime.Validation;
using Abp.UI.Inputs;

namespace solarcrm.Features
{
    public class AppFeatureProvider : FeatureProvider
    {
        public override void SetFeatures(IFeatureDefinitionContext context)
        {
            context.Create(
                AppFeatures.MaxUserCount,
                defaultValue: "0", //0 = unlimited
                displayName: L("MaximumUserCount"),
                description: L("MaximumUserCount_Description"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                ValueTextNormalizer = value => value == "0" ? L("Unlimited") : new FixedLocalizableString(value),
                IsVisibleOnPricingTable = true
            };

            #region ######## Example Features - You can delete them #########
            //context.Create("TestTenantScopeFeature", "false", L("TestTenantScopeFeature"), scope: FeatureScopes.Tenant);
            //context.Create("TestEditionScopeFeature", "false", L("TestEditionScopeFeature"), scope: FeatureScopes.Edition);

            //context.Create(
            //    AppFeatures.TestCheckFeature,
            //    defaultValue: "false",
            //    displayName: L("TestCheckFeature"),
            //    inputType: new CheckboxInputType()
            //)[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            //{
            //    IsVisibleOnPricingTable = true,
            //    TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            //};

            //context.Create(
            //    AppFeatures.TestCheckFeature2,
            //    defaultValue: "true",
            //    displayName: L("TestCheckFeature2"),
            //    inputType: new CheckboxInputType()
            //)[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            //{
            //    IsVisibleOnPricingTable = true,
            //    TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            //};
            #endregion

            var chatFeature = context.Create(
                AppFeatures.ChatFeature,
                defaultValue: "false",
                displayName: L("ChatFeature"),
                inputType: new CheckboxInputType()
            );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToTenantChatFeature,
                defaultValue: "false",
                displayName: L("TenantToTenantChatFeature"),
                inputType: new CheckboxInputType()
            );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToHostChatFeature,
                defaultValue: "false",
                displayName: L("TenantToHostChatFeature"),
                inputType: new CheckboxInputType()
            );

            //Manual Features
            var DataVaults = context.Create(
                AppFeatures.DataVaults,
                defaultValue: "false",
                displayName: L("DataVaults"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_Locations,
                defaultValue: "false",
                displayName: L("Locations"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_LeadSource,
                defaultValue: "false",
                displayName: L("LeadSource"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_LeadType,
                defaultValue: "false",
                displayName: L("LeadType"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_LeadStatus,
                defaultValue: "false",
                displayName: L("LeadStatus"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_DocumentList,
                defaultValue: "false",
                displayName: L("DocumentList"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_Teams,
                defaultValue: "false",
                displayName: L("Teams"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_Discom,
                defaultValue: "false",
                displayName: L("Discom"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_Division,
                defaultValue: "false",
                displayName: L("Division"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_SubDivision,
                defaultValue: "false",
                displayName: L("SubDivision"),
                inputType: new CheckboxInputType()
            );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_TransactionType,
               defaultValue: "false",
               displayName: L("TransactionType"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_StockItem,
               defaultValue: "false",
               displayName: L("StockItem"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_ProjectType,
               defaultValue: "false",
               displayName: L("ProjectType"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_ProjectStatus,
               defaultValue: "false",
               displayName: L("ProjectStatus"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_StockCategory,
               defaultValue: "false",
               displayName: L("StockCategory"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_InvoiceType,
               defaultValue: "false",
               displayName: L("InvoiceType"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_State,
               defaultValue: "false",
               displayName: L("State"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_District,
               defaultValue: "false",
               displayName: L("District"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_Taluka,
               defaultValue: "false",
               displayName: L("Taluka"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_City,
               defaultValue: "false",
               displayName: L("City"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_CancelReasons,
               defaultValue: "false",
               displayName: L("CancelReasons"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_HoldReasons,
               defaultValue: "false",
               displayName: L("HoldReasons"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_RejectReasons,
               defaultValue: "false",
               displayName: L("RejectReasons"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_JobCancellationReason,
               defaultValue: "false",
               displayName: L("JobCancellationReason"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_DocumentLibrary,
               defaultValue: "false",
               displayName: L("DocumentLibrary"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_EmailTemplates,
               defaultValue: "false",
               displayName: L("EmailTemplates"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_SmsTemplates,
               defaultValue: "false",
               displayName: L("SmsTemplates"),
               inputType: new CheckboxInputType()
           );
            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_PdfTemplates,
               defaultValue: "false",
               displayName: L("PdfTemplates"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_SolarType,
               defaultValue: "false",
               displayName: L("SolarType"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_HeightStructure,
               defaultValue: "false",
               displayName: L("HeightStructure"),
               inputType: new CheckboxInputType()
           );

            DataVaults.CreateChildFeature(
               AppFeatures.DataVaults_Department,
               defaultValue: "false",
               displayName: L("Department"),
               inputType: new CheckboxInputType()
           );
            DataVaults.CreateChildFeature(
              AppFeatures.DataVaults_Prices,
              defaultValue: "false",
              displayName: L("Prices"),
              inputType: new CheckboxInputType()
          );
            DataVaults.CreateChildFeature(
              AppFeatures.DataVaults_Tender,
              defaultValue: "false",
              displayName: L("Tender"),
              inputType: new CheckboxInputType()
          );
            DataVaults.CreateChildFeature(
              AppFeatures.DataVaults_Circle,
              defaultValue: "false",
              displayName: L("Circle"),
              inputType: new CheckboxInputType()
          );
            DataVaults.CreateChildFeature(
            AppFeatures.DataVaults_ActivityLogMasterVault,
            defaultValue: "false",
            displayName: L("ActivityMasterVault"),
            inputType: new CheckboxInputType()
          );

            DataVaults.CreateChildFeature(
            AppFeatures.DataVaults_Variation,
            defaultValue: "false",
            displayName: L("Variation"),
            inputType: new CheckboxInputType()
          );
            DataVaults.CreateChildFeature(
              AppFeatures.DataVaults_CustomeTag,
              defaultValue: "false",
              displayName: L("CustomeTag"),
              inputType: new CheckboxInputType()
          );
            DataVaults.CreateChildFeature(
             AppFeatures.DataVaults_PaymentMode,
             defaultValue: "false",
             displayName: L("PaymentMode"),
             inputType: new CheckboxInputType()
         );
            DataVaults.CreateChildFeature(
             AppFeatures.DataVaults_PaymentType,
             defaultValue: "false",
             displayName: L("PaymentType"),
             inputType: new CheckboxInputType()
         );
            DataVaults.CreateChildFeature(
             AppFeatures.DataVaults_BillOfMaterial,
             defaultValue: "false",
             displayName: L("BillOfMaterial"),
             inputType: new CheckboxInputType()
         );
            DataVaults.CreateChildFeature(
             AppFeatures.DataVaults_DispatchType,
             defaultValue: "false",
             displayName: L("DispatchType"),
             inputType: new CheckboxInputType()
         );
            DataVaults.CreateChildFeature(
             AppFeatures.DataVaults_RefundReasons,
             defaultValue: "false",
             displayName: L("RefundReasons"),
             inputType: new CheckboxInputType()
         );
            DataVaults.CreateChildFeature(
             AppFeatures.DataVaults_Vehical,
             defaultValue: "false",
             displayName: L("Vehical"),
             inputType: new CheckboxInputType()
         );
            DataVaults.CreateChildFeature(
                AppFeatures.DataVaults_PDFTemplate,
                defaultValue: "false",
                displayName: L("PDFTemplate"),
                inputType: new CheckboxInputType()
            );

            //Leads
            var Leads = context.Create(
                AppFeatures.Leads,
                defaultValue: "false",
                displayName: L("Leads"),
                inputType: new CheckboxInputType()
            );

            Leads.CreateChildFeature(
               AppFeatures.Leads_Customers,
               defaultValue: "false",
               displayName: L("Customers"),
               inputType: new CheckboxInputType()
           );
            Leads.CreateChildFeature(
              AppFeatures.Leads_ManageLead,
              defaultValue: "false",
              displayName: L("ManageLeads"),
              inputType: new CheckboxInputType()
          );
            Leads.CreateChildFeature(
              AppFeatures.Leads_MISReport,
              defaultValue: "false",
              displayName: L("MISReport"),
              inputType: new CheckboxInputType()
          );
            //Jobs
            var Jobsd = context.Create(
                AppFeatures.Jobs,
                defaultValue: "false",
                displayName: L("Jobs"),
                inputType: new CheckboxInputType()
            );

            Jobsd.CreateChildFeature(
               AppFeatures.Jobs_Sales,
               defaultValue: "false",
               displayName: L("SalesJob"),
               inputType: new CheckboxInputType()
           );
            Jobsd.CreateChildFeature(
              AppFeatures.Jobs_Quotation,
              defaultValue: "false",
              displayName: L("QuotationJob"),
              inputType: new CheckboxInputType()
          );
            Jobsd.CreateChildFeature(
             AppFeatures.Jobs_Installation,
             defaultValue: "false",
             displayName: L("InstallationJob"),
             inputType: new CheckboxInputType()
         );
            Jobsd.CreateChildFeature(
             AppFeatures.Jobs_Payment,
             defaultValue: "false",
             displayName: L("PaymentJob"),
             inputType: new CheckboxInputType()
         );
            Jobsd.CreateChildFeature(
              AppFeatures.Jobs_PaymentRefund,
              defaultValue: "false",
              displayName: L("PaymentRefundJob"),
              inputType: new CheckboxInputType()
          );
            Jobsd.CreateChildFeature(
               AppFeatures.Jobs_JobType,
               defaultValue: "false",
               displayName: L("JobType"),
               inputType: new CheckboxInputType()
           );

            //Tracker
            var Tracker = context.Create(
                AppFeatures.Tracker,
                defaultValue: "false",
                displayName: L("Tracker"),
                inputType: new CheckboxInputType()
            );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_Application,
               defaultValue: "false",
               displayName: L("ApplicationTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_EstimatedPaid,
               defaultValue: "false",
               displayName: L("EstimatedPaidTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_Refund,
               defaultValue: "false",
               displayName: L("RefundTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_Deposit,
               defaultValue: "false",
               displayName: L("DepositTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_Dispatch,
               defaultValue: "false",
               displayName: L("DispatchTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_PickList,
               defaultValue: "false",
               displayName: L("PickListTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_Transport,
               defaultValue: "false",
               displayName: L("TransportTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_Installation,
               defaultValue: "false",
               displayName: L("InstallationTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_SerialNoPhoto,
               defaultValue: "false",
               displayName: L("SerialNoPhotoTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_MeterConnect,
               defaultValue: "false",
               displayName: L("MeterConnectTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_PaymentIssued,
               defaultValue: "false",
               displayName: L("PaymentIssuedTracker"),
               inputType: new CheckboxInputType()
           );
            Tracker.CreateChildFeature(
               AppFeatures.Tracker_PaymentPaid,
               defaultValue: "false",
               displayName: L("PaymentPaidTracker"),
               inputType: new CheckboxInputType()
           );

            var Subsidy = context.Create(
                AppFeatures.Subsidy,
                defaultValue: "false",
                displayName: L("Subsidy"),
                inputType: new CheckboxInputType()
            );
            Subsidy.CreateChildFeature(
                AppFeatures.Subsidy_SubsidyDetail,
                defaultValue: "false",
                displayName: L("SubsidyDetail"),
                inputType: new CheckboxInputType()
            );
            Subsidy.CreateChildFeature(
                AppFeatures.Subsidy_SubsidyClaim,
                defaultValue: "false",
                displayName: L("SubsidyClaimTracker"),
                inputType: new CheckboxInputType()
            );

            //Account
            var Accounts = context.Create(
               AppFeatures.Accounts,
               defaultValue: "false",
               displayName: L("Accounts"),
               inputType: new CheckboxInputType()
           );
            Accounts.CreateChildFeature(
               AppFeatures.Accounts_PaymentIssued,
               defaultValue: "false",
               displayName: L("PaymentIssued"),
               inputType: new CheckboxInputType()
           );
            Accounts.CreateChildFeature(
               AppFeatures.Accounts_PaymentPaid,
               defaultValue: "false",
               displayName: L("PaymentPaid"),
               inputType: new CheckboxInputType()
           );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, solarcrmConsts.LocalizationSourceName);
        }
    }
}
