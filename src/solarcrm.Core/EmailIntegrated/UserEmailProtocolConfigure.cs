﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.EmailIntegrated
{
    [Table("UserEmailProtocolConfigure")]
    public class UserEmailProtocolConfigure : FullAuditedEntity
    {
        public virtual int? UserId { get; set; }

        public virtual int EmailProtocolId { get; set; }

        [ForeignKey("EmailProtocolId")]
        public virtual EmailServerProtocol EmailProtocolFK { get; set; }

        public virtual string UserProtocolEmailAddress { get; set; }

        public virtual string UserProtocolEmailPassword { get; set; }

        public bool IsDefaultEmail { get; set; }

        public bool IsActive { get; set; }

    }
}
