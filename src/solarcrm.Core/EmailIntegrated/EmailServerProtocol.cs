﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.EmailIntegrated
{
    [Table("EmailServerProtocol")]
    public class EmailServerProtocol : FullAuditedEntity
    {
        public virtual string ProtocolName { get; set; }

        public virtual string ProtocolServerHostURL { get; set; }

        public virtual string ProtocolStandardPort { get; set; }

        public virtual string ProtocolSSLPort { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
        public virtual int OrganizationUnitId { get; set; }
    }
}
