﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MISReport.MISFileUploadList
{

    [Table("MISFileUploadList")]
    public class MISFileUploadLists : FullAuditedEntity, IMustHaveTenant
    {
        public string FileName { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string FileStatus { get; set; }
        public int TenantId { get; set; }
        public virtual int? OrganizationUnitId { get; set; }
    }
}
