﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using solarcrm.DataVaults.LeadAction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace solarcrm.MISReport.MISActivityLog
{
  
    [Table("MISActivityLogs")]
    public class MISActivityLogs : FullAuditedEntity
    {
        public virtual int MISId { get; set; }

        [ForeignKey("MISId")]
        public MISReport MISReportIdFk { get; set; }

        public string ProjectName { get; set; }

        public virtual int LeadActionId { get; set; }

        [ForeignKey("LeadActionId")]
        public LeadAction LeadActionIdFk { get; set; }
     
        public virtual int? SectionId { get; set; }
        [ForeignKey("SectionId")]
        public Sections.Section SectionIdFk { get; set; }

        [StringLength(ActivityLogConsts.MaxNameLength, MinimumLength = ActivityLogConsts.MinNameLength)]
        public virtual string MISActionNote { get; set; }

        public virtual int? TenantId { get; set; }
        public virtual int? OrganizationUnitId { get; set; }       
      
    }
}
