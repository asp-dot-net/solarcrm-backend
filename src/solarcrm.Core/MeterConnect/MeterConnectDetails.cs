﻿using Abp.Domain.Entities.Auditing;
using Org.BouncyCastle.Crypto.Agreement;
using solarcrm.Consts;
using solarcrm.JobInstallation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.MeterConnect
{
    public class MeterConnectDetails : FullAuditedEntity
    {
        public virtual int InstallationId { get; set; }
        [ForeignKey("InstallationId")]
        public virtual JobInstallationDetail InstallationFKId { get; set; }

        [StringLength(JobsConsts.MeterAgreementGivenTo, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string MeterAgreementGivenTo { get; set; }

        [StringLength(JobsConsts.MeterAgreementNo, MinimumLength = JobsConsts.MinNameLength)]
        public virtual string MeterAgreementNo { get; set; }
        public virtual DateTime? MeterAgreementSentDate { get; set; }
        public virtual int? MeterApplicationReadyStatus { get; set; }
        public virtual DateTime? MeterSubmissionDate { get; set; }
    }
}
