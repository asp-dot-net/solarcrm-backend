﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Locations
{
    [Table("Locations")]
    public class Locations : FullAuditedEntity
    {      
        [StringLength(LocationConsts.MaxNameLength, MinimumLength = LocationConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
