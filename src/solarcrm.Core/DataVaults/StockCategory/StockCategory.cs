﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.StockCategory
{
    [Table("StockCategory")]
    public class StockCategory : FullAuditedEntity
    {      
        [StringLength(StockCategoryConsts.MaxNameLength, MinimumLength = StockCategoryConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
