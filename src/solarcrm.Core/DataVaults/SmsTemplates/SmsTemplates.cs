﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.SmsTemplates
{
    [Table("SmsTemplates")]
    public class SmsTemplates : FullAuditedEntity
    {       

        [Required]
        [StringLength(SmsTemplateConsts.MaxNameLength, MinimumLength = SmsTemplateConsts.MinNameLength)]
        public virtual string Name { get; set; }

        [Required]
        public virtual string Text { get; set; }

        [Required]
        [StringLength(80)]
        public virtual string ProviderTemp_Id { get; set; }

        public virtual int? OrganizationUnitId { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }

    }
    
}
