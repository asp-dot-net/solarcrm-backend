﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Department
{
    [Table("Departments")]
    public class Department : FullAuditedEntity
    {
        [StringLength(DepartmentConsts.MaxNameLength, MinimumLength = DepartmentConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
