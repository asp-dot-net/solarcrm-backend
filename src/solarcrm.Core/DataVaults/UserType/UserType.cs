﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.UserType
{
    [Table("UserTypes")]
    public class UserType : FullAuditedEntity
    {
        [Required]
        [StringLength(UserTypeConsts.MaxNameLength, MinimumLength = UserTypeConsts.MinNameLength)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
