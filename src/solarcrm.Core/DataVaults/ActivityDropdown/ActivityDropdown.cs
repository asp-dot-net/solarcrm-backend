﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ActivityDropdown
{
    [Table("ActivityDropdown")]
    public class ActivityDropdown : FullAuditedEntity
    {
        [Required]
        [StringLength(TeamsConsts.MaxNameLength, MinimumLength = TeamsConsts.MinNameLength)]
        public virtual string Name { get; set; }
      
        //public virtual int LeadActivityId { get; set; }
        //[ForeignKey("LeadActivityId")]
        //public LeadActivity.LeadActivity LeadActivityIdFk { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
