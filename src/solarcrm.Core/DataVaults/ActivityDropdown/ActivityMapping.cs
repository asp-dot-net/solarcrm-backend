﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.ActivityDropdown
{
    [Table("ActivityMapping")]
    public class ActivityMapping : FullAuditedEntity
    {     

        public virtual int LeadActivityId { get; set; }
        [ForeignKey("LeadActivityId")]
        public LeadActivity.LeadActivity LeadActivityIdFk { get; set; }

        public virtual int ActivityDropdownId { get; set; }
        [ForeignKey("ActivityDropdownId")]
        public ActivityDropdown ActivityDropdownIdFk { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
