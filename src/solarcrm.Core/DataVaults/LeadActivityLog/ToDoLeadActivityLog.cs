﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadActivityLog
{
    [Table("ToDoLeadActivityLog")]
    public class ToDoLeadActivityLog : FullAuditedEntity
	{
		public virtual int? LeadActivityLogId { get; set; }

		[ForeignKey("LeadActivityLogId")]
		public LeadActivityLogs LeadActivityLogIdFk { get; set; }

		public virtual int? TodopriorityId { get; set; }

		[StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MaxNameLength)]
		public virtual string TodopriorityName { get; set; }
		public virtual bool? IsTodoComplete { get; set; }

		public virtual string TodoResponse { get; set; }
		public virtual string TodoTag { get; set; }
		public virtual DateTime? TodoDueDate { get; set; }
		public virtual DateTime? TodoresponseTime { get; set; }

		//[StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.ActivityNotesLength)]
		public virtual string TodoActivityNote { get; set; }
	}
}
