﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadActivityLog
{
    [Table("NotifyLeadActivityLog")]
    public class NotifyLeadActivityLog : FullAuditedEntity
	{
		//[StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.ActivityNotesLength)]
		public virtual int? LeadActivityLogId { get; set; }

		[ForeignKey("LeadActivityLogId")]
		public LeadActivityLogs LeadActivityLogIdFk { get; set; }


		public virtual string NotifyActivityNote { get; set; }

	}
}
