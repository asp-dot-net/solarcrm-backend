﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadActivityLog
{
    [Table("EmailLeadActivityLog")] 
    public class EmailLeadActivityLog : FullAuditedEntity
	{
		public virtual int? LeadActivityLogId { get; set; }

		[ForeignKey("LeadActivityLogId")]
		public LeadActivityLogs LeadActivityLogIdFk { get; set; }


		public virtual int? EmailTemplateId { get; set; }
		[ForeignKey("EmailTemplateId")]
		public EmailTemplates.EmailTemplates EmailTemplateIdFk { get; set; }


		[StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MaxNameLength)]
		public virtual string Subject { get; set; }
		public virtual string EmailActivityBody { get; set; }

	}
}
