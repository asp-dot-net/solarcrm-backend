﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadActivityLog
{
    [Table("ReminderLeadActivityLog")]
    public class ReminderLeadActivityLog : FullAuditedEntity
	{
		public virtual int? LeadActivityLogId { get; set; }

		[ForeignKey("LeadActivityLogId")]
		public LeadActivityLogs LeadActivityLogIdFk { get; set; }


		public virtual DateTime? ReminderDate { get; set; }

		//[StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.ActivityNotesLength)]
		public virtual string ReminderActivityNote { get; set; }

	}
}
