﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadActivityLog
{
    [Table("LeadActivityLog")]
    public class LeadActivityLogs : FullAuditedEntity  //, IMustHaveTenant
    {
        
        public virtual int LeadId { get; set; }

        [ForeignKey("LeadId")]
        public Leads.Leads LeadIdFk { get; set; }

        public virtual int LeadActionId { get; set; }
       
        [ForeignKey("LeadActionId")]
        public LeadAction.LeadAction LeadActionIdFk { get; set; }

        public virtual int? LeadAcitivityID { get; set; }

        [ForeignKey("LeadAcitivityID")]
        public LeadActivity.LeadActivity LeadActivityIdFk { get; set; }

        public virtual int? SectionId { get; set; }

        [ForeignKey("SectionId")]
        public Sections.Section SectionIdFk { get; set; }

        [StringLength(ActivityLogConsts.MaxNameLength, MinimumLength = ActivityLogConsts.MinNameLength)]
        public virtual string LeadActionNote { get; set; }

        [StringLength(ActivityLogConsts.MaxNameLength, MinimumLength = ActivityLogConsts.MinNameLength)]
        public virtual string ActionFrom { get; set; }
        //public virtual int? SmsLeadActivityLogId { get; set; }

        //[ForeignKey("SmsLeadActivityLogId")]
        //public SmsLeadActivityLog SmsLeadActivityIdFk { get; set; }

        //public virtual int? EmailLeadActivityLogId { get; set; }

        //[ForeignKey("EmailLeadActivityLogId")]
        //public EmailLeadActivityLog EmailLeadActivityIdFk { get; set; }

        //public virtual int? ReminderLeadActivityLogId { get; set; }
        //[ForeignKey("ReminderLeadActivityLogId")]
        //public ReminderLeadActivityLog ReminderLeadActivityIdFk { get; set; }

        //public virtual int? NotifyLeadActivityLogId { get; set; }
        //[ForeignKey("NotifyLeadActivityLogId")]
        //public NotifyLeadActivityLog NotifyLeadActivityIdFk { get; set; }

        //public virtual int? CommentLeadActivityLogId { get; set; }
        //[ForeignKey("CommentLeadActivityLogId")]
        //public CommentLeadActivityLog CommentLeadActivityIdFk { get; set; }


        //public virtual int? TodoLeadActivityLogId { get; set; }
        //[ForeignKey("TodoLeadActivityLogId")]
        //public ToDoLeadActivityLog TodoLeadActivityIdFk { get; set; }


        public virtual int? TenantId { get; set; }
        public virtual int? OrganizationUnitId { get; set; }
        //public virtual DateTime? ActivityDate { get; set; }

        //public virtual string Body { get; set; }

        //public virtual string ActivityNote { get; set; }

        public virtual string MessageId { get; set; }

        //public virtual int? ReferanceId { get; set; }

        public virtual bool? IsMark { get; set; }

        //public virtual int? TemplateId { get; set; }

        //public virtual string Subject { get; set; }

        //public virtual int? PromotionId { get; set; }

        //public virtual int? PromotionUserId { get; set; }



        //public virtual int? TodopriorityId { get; set; }
        //public virtual string Todopriority { get; set; }
        //public virtual bool? IsTodoComplete { get; set; }

        //public virtual string TodoResponse { get; set; }
        //public virtual string TodoTag { get; set; }
        //public virtual DateTime? TodoDueDate { get; set; }
        //public virtual DateTime? TodoresponseTime { get; set; }

        public virtual string LeadDocumentPath { get; set; }

    }
}
