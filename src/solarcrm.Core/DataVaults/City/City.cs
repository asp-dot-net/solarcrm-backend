﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using solarcrm.Consts;

namespace solarcrm.DataVaults.City
{
    [Table("City")]
    public class City : FullAuditedEntity
    {
        [Required]
        [StringLength(CityConsts.MaxNameLength, MinimumLength = CityConsts.MinNameLength)]
        public virtual string Name { get; set; }
        public virtual int TalukaId { get; set; }

        [ForeignKey("TalukaId")]
        public Taluka.Taluka TalukaFk { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; } 
       
    }
}
