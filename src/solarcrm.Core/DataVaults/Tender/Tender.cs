﻿//using Abp.Domain.Entities.Auditing;
//using solarcrm.Consts;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Tender
{
    [Table("Tender")]
    public class Tender : FullAuditedEntity
    {
       // [Required]
        //[StringLength(TeamsConsts.MaxNameLength, MinimumLength = TeamsConsts.MinNameLength)]
        public virtual string Name { get; set; }
        public virtual int SolarTypeId { get; set; }
        [ForeignKey("SolarTypeId")]
        public SolarType.SolarType SolarTypeIdFk { get; set; }


        public virtual DateTime StartDate { get; set; }

        public virtual DateTime EndDate { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
