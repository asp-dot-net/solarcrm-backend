﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Discom
{
    [Table("Discoms")]
    public class Discom : FullAuditedEntity
    {
        [Required]
        [StringLength(DiscomConsts.MaxNameLength, MinimumLength = DiscomConsts.MinNameLength)]
        public virtual string Name { get; set; }

        [Required]
        [StringLength(DiscomConsts.MaxABBLength, MinimumLength = DiscomConsts.MinABBLength)]
        public virtual string ABB { get; set; }        

        public virtual int LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Locations.Locations LocationsFk { get; set; }

        [Required]
        [StringLength(DiscomConsts.MaxPaymentLinkLength, MinimumLength = DiscomConsts.MinPaymentLinkLength)]
        public string PaymentLink { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
