﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadDocuments
{
	[Table("LeadDocuments")]
	public class LeadDocuments : FullAuditedEntity
	{
		[Required]
		public virtual int LeadDocumentId { get; set; }
		[ForeignKey("LeadDocumentId")]
		public Leads.Leads LeadIdFk { get; set; }

		[Required]
		public virtual int DocumentId { get; set; }
		[ForeignKey("DocumentId")]
		public DocumentList.DocumentList DocumentIdFk { get; set; }

		[Required]
		[StringLength(100)]
		public virtual string DocumentNumber { get; set; }


		[StringLength(150)]
		public virtual string LeadDocumentPath { get; set; }		

		public bool IsActive { get; set; }

		public int? TenantId { get; set; }
	}
}
