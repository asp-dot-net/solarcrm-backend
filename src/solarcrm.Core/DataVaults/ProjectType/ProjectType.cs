﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.ProjectType
{
    [Table("ProjectType")]
    public class ProjectType : FullAuditedEntity
    {      
        [StringLength(ProjectTypeConsts.MaxNameLength, MinimumLength = ProjectTypeConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
