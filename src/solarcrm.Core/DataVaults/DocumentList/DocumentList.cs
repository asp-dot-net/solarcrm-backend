﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;

namespace solarcrm.DataVaults.DocumentList
{
    [Table("DocumentLists")]
    public class DocumentList : FullAuditedEntity
    {
        [Required]
        [StringLength(DocumentListConsts.MaxNameLength, MinimumLength = DocumentListConsts.MinNameLength)]
        public virtual string Name { get; set; }


        [StringLength(50)]
        public virtual string DocumenSize { get; set; }

        [StringLength(100)]
        public virtual string DocumenFormate { get; set; }
        [StringLength(50)]
        public virtual string DocumentShortName { get; set; }


        public bool IsATFlag { get; set; }
        public bool IsActive { get; set; }

        public int? TenantId { get; set; }

        public virtual string DocumenStorageUnit { get; set; }


    }
}
