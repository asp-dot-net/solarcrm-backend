﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadAction
{
    [Table("LeadAction")]
    public class LeadAction : FullAuditedEntity
	{
		[StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MaxNameLength)]
		public virtual string LeadActionName { get; set; }

		[StringLength(50)]
		public virtual string LeadActionColorClass { get; set; }

		[StringLength(50)]
		public virtual string LeadActionIconClass { get; set; }


	}
}
