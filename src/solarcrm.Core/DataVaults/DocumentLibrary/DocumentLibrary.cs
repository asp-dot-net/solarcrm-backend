﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.DocumentLibrary
{
    [Table("DocumentLibrarys")]
    public class DocumentLibrary : FullAuditedEntity
    {
        [Required]
        [StringLength(DocumentLibraryConsts.MaxNameLength, MinimumLength = DocumentLibraryConsts.MinNameLength)]
       
        public virtual string Title { get; set; }
        public virtual string FileName { get; set; }

        public virtual string FileType { get; set; }

        public virtual string FilePath { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
