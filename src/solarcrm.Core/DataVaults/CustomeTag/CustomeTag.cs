﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.CustomeTag
{
    [Table("CustomeTag")]
    public class CustomeTag : FullAuditedEntity
    {
        [Required]
        [StringLength(100)]
        public virtual string TagTableName { get; set; }


        [Required]
        [StringLength(100)]
        public virtual string TagTableFieldName { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual int? TenantId { get; set; }
        public virtual int? OrganizationUnitId { get; set; }
    }
}
