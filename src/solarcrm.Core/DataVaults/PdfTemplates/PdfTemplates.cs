﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PdfTemplates
{
    [Table("PdfTemplate")]
    public class PdfTemplates : FullAuditedEntity
    {
        [Required]
        [StringLength(PdfTemplateConsts.MaxNameLength, MinimumLength = PdfTemplateConsts.MinNameLength)]
        public virtual string PdfTemplateName { get; set; }

        [Required]
        public virtual string ViewHtml { get; set; }

        public virtual string ApiHtml { get; set; }

        public bool IsActive { get; set; }
        public virtual int? OrganizationUnitId { get; set; }      

        public int? TenantId { get; set; }
    }
}
