﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Vehical
{
    [Table("Vehical")]
    public class Vehical : FullAuditedEntity
    {
        [Required]
        [StringLength(VehicalConsts.MaxNameLength, MinimumLength = VehicalConsts.MinNameLength)]
        public virtual string VehicalType { get; set; }


        [StringLength(VehicalConsts.MaxNameLength, MinimumLength = VehicalConsts.MinNameLength)]
        public virtual string VehicalRegNo { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
