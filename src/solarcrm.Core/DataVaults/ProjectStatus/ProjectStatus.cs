﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.ProjectStatus
{
    [Table("ProjectStatus")]
    public class ProjectStatus : FullAuditedEntity
    {      
        [StringLength(ProjectStatusConsts.MaxNameLength, MinimumLength = ProjectStatusConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
