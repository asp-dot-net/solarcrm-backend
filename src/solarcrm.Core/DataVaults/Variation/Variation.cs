﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Variation
{
    [Table("Variations")]
	public class Variation : FullAuditedEntity, IMustHaveTenant
    {
		public int TenantId { get; set; }

		public virtual string Name { get; set; }

		[Required]
		[StringLength(VariationConsts.MaxActionLength, MinimumLength = VariationConsts.MinActionLength)]
		public virtual string Action { get; set; }

		public bool IsActive { get; set; }
	}
}
