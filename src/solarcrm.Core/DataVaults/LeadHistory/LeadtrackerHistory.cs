﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadHistory
{
    [Table("LeadtrackerHistory")]
    public class LeadtrackerHistory : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual string FieldName { get; set; }
        public virtual string PrevValue { get; set; }
        public virtual string CurValue { get; set; }
        public virtual string Action { get; set; }

        // public virtual string DisplayField { get; set; }
       
        public int LeadId { get; set; }
        public int LeadActionId { get; set; }
    }
}
