﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.JobCancellationReason
{
    [Table("JobCancellationReason")]
    public class JobCancellationReason : FullAuditedEntity
    {
        [Required]
        [StringLength(JobCancellationReasonConsts.MaxNameLength, MinimumLength = JobCancellationReasonConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; } 

    
    }
}
