﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.LeadSource
{
    [Table("LeadSources")]
    public class LeadSource : FullAuditedEntity
    {
        [Required]
        [StringLength(LeadSourceConsts.MaxNameLength, MinimumLength = LeadSourceConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual string LeadSourceColorClass { get; set; }
        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
