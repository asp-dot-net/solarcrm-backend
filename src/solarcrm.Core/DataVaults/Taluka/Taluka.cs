﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using solarcrm.Consts;

namespace solarcrm.DataVaults.Taluka
{
    [Table("Taluka")]
    public class Taluka : FullAuditedEntity
    {
        [Required]
        [StringLength(TalukaConsts.MaxNameLength, MinimumLength = TalukaConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual int DistrictId { get; set; }

        [ForeignKey("DistrictId")]
        public District.District DistrictFk { get; set; }  
        

        public bool IsActive { get; set; }

        public int? TenantId { get; set; } 

       
    }
}
