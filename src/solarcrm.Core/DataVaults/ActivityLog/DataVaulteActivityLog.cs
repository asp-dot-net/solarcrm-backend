﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;
using solarcrm.Sections;

namespace solarcrm.DataVaults.ActivityLog
{
    [Table("DataVaulteActivityLogs")]
    public class DataVaulteActivityLog : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int SectionId { get; set; }

        [ForeignKey("SectionId")]
        public Section SectionFk { get; set; }

        public virtual int? ActionId { get; set; }

        [ForeignKey("ActionId")]
        public DataVaultsAction.DataVaultsAction DataVaultsActionFk { get; set; }

        public virtual int? SectionValueId { get; set; }

        public virtual string ActionNote { get; set; }
    }
}
