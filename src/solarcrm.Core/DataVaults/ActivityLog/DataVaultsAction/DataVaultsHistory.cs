﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using solarcrm.Sections;

namespace solarcrm.DataVaults.ActivityLog.DataVaultsAction
{
    [Table("DataVaultsHistories")]
    public class DataVaultsHistory : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual string FieldName { get; set; }
        
        public virtual string PrevValue { get; set; }
        
        public virtual string CurValue { get; set; }
        
        public virtual string Action { get; set; }

        public virtual int? ActionId { get; set; }
        
        public virtual int ActivityLogId { get; set; }

        public virtual int SectionId { get; set; }

        public virtual int? SectionValueId { get; set; }
    }
}
