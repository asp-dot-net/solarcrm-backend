﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;


namespace solarcrm.DataVaults.ActivityLog.DataVaultsAction
{
    [Table("DataVaultsActions")]
    public class DataVaultsAction : FullAuditedEntity
    {
        public virtual string Name { get; set; }
    }
}
