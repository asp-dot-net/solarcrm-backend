﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadActivity
{
    [Table("LeadActivity")]
    public class LeadActivity:  FullAuditedEntity
    {
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MaxNameLength)]
        public virtual string LeadActivityName { get; set; }
    }
}
