﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using solarcrm.Consts;

namespace solarcrm.DataVaults.LeadType
{
    [Table("LeadTypes")]
    public class LeadType : FullAuditedEntity
    {
        [Required]
        [StringLength(LeadTypeConsts.MaxNameLength, MinimumLength = LeadTypeConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; } 

       
    }
}
