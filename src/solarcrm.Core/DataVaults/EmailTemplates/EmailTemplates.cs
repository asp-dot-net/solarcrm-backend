﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.EmailTemplates
{
	[Table("EmailTemplate")]
	public class EmailTemplates: FullAuditedEntity
	{
		[Required]
		[StringLength(EmailTemplateConsts.MaxNameLength, MinimumLength = EmailTemplateConsts.MinNameLength)]
		public virtual string TemplateName { get; set; }

		[Required]
		public virtual string Body { get; set; }

		public virtual string Subject { get; set; }		

		public virtual int? OrganizationUnitId { get; set; }

		public bool IsActive { get; set; }

		public int? TenantId { get; set; }

	}
   
}
