﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.InvoiceType
{
    [Table("InvoiceType")]
    public class InvoiceType : FullAuditedEntity
    {      
        [StringLength(InvoiceTypeConsts.MaxNameLength, MinimumLength = InvoiceTypeConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
