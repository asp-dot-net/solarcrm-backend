﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using solarcrm.Consts;

namespace solarcrm.DataVaults.District
{
    [Table("District")]
    public class District : FullAuditedEntity
    {
        [Required]
        [StringLength(DistrictConsts.MaxNameLength, MinimumLength = DistrictConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual int StateId { get; set; }

        [ForeignKey("StateId")]
        public State.State StateFk { get; set; }    

        public bool IsActive { get; set; }

        public int? TenantId { get; set; } 

       
    }
}
