﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Teams
{
    [Table("Teams")]
    public class Teams : FullAuditedEntity
    {
        [Required]
        [StringLength(TeamsConsts.MaxNameLength, MinimumLength = TeamsConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
