﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.BillOfMaterial
{
    [Table("BillOfMaterials")]
    public class BillOfMaterial : FullAuditedEntity
    {
        public virtual int StockItemId { get; set; }
        [ForeignKey("StockItemId")]
        public StockItem.StockItem StockItemFk { get; set; }
        public virtual int HeightStructureId { get; set; }
        [ForeignKey("HeightStructureId")]
        public HeightStructure.HeightStructure HeightStructureIdFk { get; set; }
        public virtual int? Panel_S { get; set; }
        public virtual int? Panel_E { get; set; }
        public virtual int? Quantity { get; set; }
        public virtual bool Active { get; set; }
        public int? TenantId { get; set; }
    }
}
