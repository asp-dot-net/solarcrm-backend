﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Prices
{
    [Table("PriceMultiAreaType")]
    public class PriceMultiAreaType : FullAuditedEntity
    {
        public virtual int PriceID { get; set; }
        [ForeignKey("PriceID")]
        public Price.Prices PriceIdFk { get; set; }

        public virtual string PriceAreaTypeMulti { get; set; }
    }
}
