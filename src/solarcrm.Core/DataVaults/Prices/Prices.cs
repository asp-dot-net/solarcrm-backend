﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Price
{
    [Table("Prices")]
    public class Prices : FullAuditedEntity
    {       
        public virtual int TenderId { get; set; }
        [ForeignKey("TenderId")]
        public Tender.Tender TenderIdFk { get; set; }       
        public virtual decimal? SystemSize { get; set; }
        public virtual decimal? CentralRate { get; set; }
        public virtual decimal? StateRate { get; set; }
        public virtual decimal? ActualPrice { get; set; }

        public virtual decimal? Subcidy40 { get; set; }

        public virtual decimal? Subcidy20 { get; set; }

        public virtual int? MeterType { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
