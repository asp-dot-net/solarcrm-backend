﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults
{
    [Table("ApplicationStatus")]
    public class ApplicationStatus : FullAuditedEntity
    {
        [Required]
        [StringLength(SolarTypeConsts.MaxNameLength, MinimumLength = SolarTypeConsts.MinNameLength)]
        public virtual string ApplicationStatusName { get; set; }

        public virtual string ApplicationStatusLabelColor { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }

    }
}
