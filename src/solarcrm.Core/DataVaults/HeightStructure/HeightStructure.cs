﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.HeightStructure
{
    [Table("HeightStructure")]
    public class HeightStructure : FullAuditedEntity
    {
        [Required]
        [StringLength(HeightStructureConsts.MaxNameLength, MinimumLength = HeightStructureConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }


    }
}
