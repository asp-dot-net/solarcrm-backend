﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Leads
{
    [Table("Leads")]
    public class Leads : FullAuditedEntity
    {
        [Required]
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string FirstName { get; set; }

        //[Required]
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string MiddleName { get; set; }

        [Required]
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string LastName { get; set; }

        [Required]
        [StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string CustomerName { get; set; }

        [Required]
        [StringLength(LeadsConsts.MobileLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string MobileNumber { get; set; }

        //[Required]
        [StringLength(LeadsConsts.EmailLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string EmailId { get; set; }

        [StringLength(LeadsConsts.MobileLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string Alt_Phone { get; set; }

        [StringLength(LeadsConsts.AddressLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string AddressLine1 { get; set; }

        [StringLength(LeadsConsts.AddressLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string AddressLine2 { get; set; }

        public virtual int? StateId { get; set; }
        [ForeignKey("StateId")]
        public virtual State.State StateIdFk { get; set; }

        public virtual int? DistrictId { get; set; }
        [ForeignKey("DistrictId")]
        public virtual  District.District DistrictIdFk { get; set; }

        public virtual int? TalukaId { get; set; }
        [ForeignKey("TalukaId")]
        public virtual Taluka.Taluka TalukaIdFk { get; set; }

        public virtual int? CityId { get; set; }
        [ForeignKey("CityId")]
        public virtual City.City CityIdFk { get; set; }


        public virtual int? Pincode { get; set; }

        public virtual decimal? Latitude { get; set; }

        public virtual decimal? Longitude { get; set; }


        //[Required]  -1
        public virtual int LeadTypeId { get; set; }

        [ForeignKey("LeadTypeId")]
        public virtual LeadType.LeadType LeadTypeIdFk { get; set; }


        [Required]
        public virtual int LeadSourceId { get; set; }

        [ForeignKey("LeadSourceId")]
        public virtual LeadSource.LeadSource LeadSourceIdFk { get; set; }

        //[Required]   -1
        public virtual int LeadStatusId { get; set; }
        [ForeignKey("LeadStatusId")]
        public virtual LeadStatus.LeadStatus LeadStatusIdFk { get; set; }

        //[Required]    --2
        public virtual int SolarTypeId { get; set; }

        [ForeignKey("SolarTypeId")]
        public virtual SolarType.SolarType SolarTypeIdFk { get; set; }

        [Required]
        public virtual bool CommonMeter { get; set; }

        // [Required]
        public virtual long? ConsumerNumber { get; set; }

        public virtual int? AvgmonthlyUnit { get; set; }

        public virtual int? AvgmonthlyBill { get; set; }



        //[Required]
        public virtual int? DiscomId { get; set; }

        [ForeignKey("DiscomId")]
        public virtual Discom.Discom DiscomIdFk { get; set; }

        //[Required]
        public virtual int? DivisionId { get; set; }

        [ForeignKey("DivisionId")]
        public virtual Division.Division DivisionIdFk { get; set; }

        //[Required]
        public virtual int? SubDivisionId { get; set; }

        [ForeignKey("SubDivisionId")]
        public virtual SubDivision.SubDivision SubDivisionIdFk { get; set; }
        //[Required]
        public virtual int? CircleId { get; set; }


        [ForeignKey("CircleId")]
        public virtual Circle.Circle CircleIdFk { get; set; }
        // [Required]
        //public virtual decimal? RequiredCapacity { get; set; }
        public virtual int? RequiredCapacity { get; set; }  //KiloWattSystem

        //[Required]   -1
        public virtual int? StrctureAmmount { get; set; }

        //[Required]   -1
        public virtual int? HeightStructureId { get; set; }

        [ForeignKey("HeightStructureId")]
        public virtual HeightStructure.HeightStructure HeightStructureIdFk { get; set; }

        [StringLength(LeadsConsts.NotesLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string Notes { get; set; }

        [StringLength(LeadsConsts.CategoryLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string Category { get; set; }

        [StringLength(LeadsConsts.CategoryLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string AreaType { get; set; }

        public virtual int? ChanelPartnerID { get; set; }

        public virtual long? BankAccountNumber { get; set; }

        [StringLength(LeadsConsts.BankAccountHolderNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string BankAccountHolderName { get; set; }

        [StringLength(LeadsConsts.BankAccountHolderNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string BankBrachName { get; set; }

        [StringLength(LeadsConsts.BankAccountHolderNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string BankName { get; set; }

        [StringLength(LeadsConsts.IFSCCodeLength, MinimumLength = LeadsConsts.MinNameLength)]

        public virtual string IFSC_Code { get; set; }

        public virtual bool? IsDuplicate { get; set; }

        public virtual bool? IsWebDuplicate { get; set; }
        public DateTime? ChangeStatusDate { get; set; }
        public DateTime? LeadAssignDate { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual bool IsVerified { get; set; }

        public virtual int? TenantId { get; set; }
        public virtual int OrganizationUnitId { get; set; }

        public bool? HideDublicate { get; set; }

        public int? DublicateLeadId { get; set; }

    }
}
