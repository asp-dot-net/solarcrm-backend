﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Leads
{
    [Table("ExternalLeads")]
    public class ExternalLeads : FullAuditedEntity
    {
        public virtual int? LeadId { get; set; }
        [ForeignKey("LeadId")]
        public Leads LeadIdFk { get; set; }

        [StringLength(LeadsConsts.StateNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string StateName { get; set; }

        [StringLength(LeadsConsts.DistrictNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string DistrictName { get; set; }

        [StringLength(LeadsConsts.TalukaNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string TalukaName { get; set; }

        [StringLength(LeadsConsts.CityNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string CityName { get; set; }

        [StringLength(LeadsConsts.SourceNameLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string SourceName { get; set; }

        
        public virtual int? IsExternalLead { get; set; }

        [StringLength(LeadsConsts.ExcelAddressLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string ExcelAddress { get; set; }


        [StringLength(LeadsConsts.GoogleLength, MinimumLength = LeadsConsts.MinNameLength)]
        public virtual string IsGoogle { get; set; }


        public virtual bool IsPromotion { get; set; }

    }
}
