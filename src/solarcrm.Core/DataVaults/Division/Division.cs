﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.Division
{
    [Table("Divisions")]
    public class Division : FullAuditedEntity
    {
        [Required]
        [StringLength(DivisionConsts.MaxNameLength, MinimumLength = DivisionConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual int CircleId { get; set; }

        [ForeignKey("CircleId")]
        public Circle.Circle CircleFk { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
