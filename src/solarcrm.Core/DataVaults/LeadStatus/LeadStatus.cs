﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.LeadStatus
{
	[Table("LeadStatus")]
	public class LeadStatus : FullAuditedEntity
	{
		[Required]
		[StringLength(LeadsConsts.MaxNameLength, MinimumLength = LeadsConsts.MinNameLength)]
		public virtual string Status { get; set; }

		public virtual string StatusCode { get; set; }

		[StringLength(50)]
		public virtual string LeadStatusIconClass { get; set; }

		[StringLength(50)]
		public virtual string LeadStatusColorClass { get; set; }
		public bool IsActive { get; set; }

		public int? TenantId { get; set; }
	}
}
