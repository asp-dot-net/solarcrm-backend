﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using solarcrm.Consts;

namespace solarcrm.DataVaults.State
{
    [Table("State")]
    public class State : FullAuditedEntity
    {
        [Required]
        [StringLength(StateConsts.MaxNameLength, MinimumLength = StateConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; } 

       
    }
}
