﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.Circle
{
    [Table("Circle")]
    public class Circle : FullAuditedEntity
    {

        [Required]
        [StringLength(CircleConsts.MaxNameLength, MinimumLength = CircleConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual int? DiscomId { get; set; }
        [ForeignKey("DiscomId")]
        public Discom.Discom DiscomIdFk { get; set; }

        public virtual int? DivisionId { get; set; }
        [ForeignKey("DivisionId")]
        public Division.Division DivisionIdFk { get; set; }

        public virtual int? SubDivisionId { get; set; }
        [ForeignKey("SubDivisionId")]
        public SubDivision.SubDivision SubDivisionIdFk { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
