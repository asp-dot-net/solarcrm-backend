﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults
{
    [Table("CancelRejectLeadReason")]
    public class CancelRejectLeadReason : FullAuditedEntity
    {

        public virtual int? LeadId { get; set; }
        [ForeignKey("LeadId")]
        public Leads.Leads LeadIdFk { get; set; }


        public virtual int? CancelReasonsLeadId { get; set; }
        [ForeignKey("CancelReasonsLeadId")]
        public CancelReasons.CancelReasons CancelReasonIdFk { get; set; }

        public virtual int? RejectReasonLeadId { get; set; }
        [ForeignKey("RejectReasonLeadId")]
        public RejectReasons.RejectReasons RejectReasonIdFk { get; set; }
        
        public virtual string Notes { get; set; }
    }
}
