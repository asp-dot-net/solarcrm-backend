﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.SubDivision
{
    public class SubDivision : FullAuditedEntity
    {
        [Required]
        [StringLength(SubDivisionConsts.MaxNameLength, MinimumLength = SubDivisionConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public virtual int DivisionId { get; set; }

        [ForeignKey("DivisionId")]
        public Division.Division DivisionFk { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
