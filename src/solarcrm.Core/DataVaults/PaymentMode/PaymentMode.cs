﻿using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.PaymentMode
{
    [Table("PaymentMode")]
    public class PaymentMode : FullAuditedEntity
    {
        [Required]
        [StringLength(LeadTypeConsts.MaxNameLength, MinimumLength = LeadTypeConsts.MinNameLength)]
        public virtual string Name { get; set; }

        public bool IsActive { get; set; }

        public int? TenantId { get; set; }
    }
}
