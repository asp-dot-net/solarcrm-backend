﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace solarcrm.DataVaults.StockItem
{
    [Table("StockItemLocations")]
    public class StockItemLocation : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int StockItemId { get; set; }
        [ForeignKey("StockItemId")]
        public StockItem StockItemFk { get; set; }

        public int LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Locations.Locations LocationsFk { get; set; }

        public int? MinQty { get; set; }

        public virtual bool IsActive { get; set; }

    }
}
