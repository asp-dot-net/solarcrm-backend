﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using solarcrm.Consts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace solarcrm.DataVaults.StockItem
{
    [Table("StockItems")]
    public class StockItem : FullAuditedEntity
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(StockItemConsts.MaxNameLength, MinimumLength = StockItemConsts.MinNameLength)]
        public virtual string Name { get; set; }

        [StringLength(StockItemConsts.MaxManufacturerLength, MinimumLength = StockItemConsts.MinManufacturerLength)]
        public virtual string Manufacturer { get; set; }

        [StringLength(StockItemConsts.MaxModelLength, MinimumLength = StockItemConsts.MinModelLength)]
        public virtual string Model { get; set; }

        [StringLength(StockItemConsts.MaxSeriesLength, MinimumLength = StockItemConsts.MinSeriesLength)]
        public virtual string Series { get; set; }

        public virtual decimal? Size { get; set; }

        public virtual int? Phase { get; set; }

        public virtual int CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public StockCategory.StockCategory StockCategoryFk { get; set; }

        [StringLength(StockItemConsts.MaxUMOLength, MinimumLength = StockItemConsts.MinUMOLength)]
        public virtual string UOM { get; set; }
        public virtual int Seq { get; set; }
        public virtual bool Active { get; set; }

        public virtual string FileName { get; set; }

        public virtual string FileType { get; set; }

        public virtual string FilePath { get; set; }

    }
}
