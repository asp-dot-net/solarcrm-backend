﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Abp.Dependency;
using Abp.ObjectMapping;
using Acr.UserDialogs;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using solarcrm.Core.Dependency;
using solarcrm.Localization;
using solarcrm.Services.Modal;
using solarcrm.Services.Navigation;
using solarcrm.ViewModels.Project;
using solarcrm.Views;
using solarcrm.Views.Project;
using Xamarin.Forms;

namespace solarcrm.ViewModels.Base
{
    public abstract class XamarinViewModel : ExtendedBindableObject, ITransientDependency
    {
        protected XamarinViewModel()
        {
            ModalService = DependencyResolver.Resolve<IModalService>(); ;
            NavigationService = DependencyResolver.Resolve<INavigationService>();
            ObjectMapper = NullObjectMapper.Instance;
        }

        private bool _isBusy;
        protected readonly INavigationService NavigationService;
        protected readonly IModalService ModalService;
        public IObjectMapper ObjectMapper { get; set; }

        private string _homeIcon = "Home_active";
        public string HomeIcon
        {
            get => _homeIcon;
            set { _homeIcon = value; OnPropertyChanged(nameof(HomeIcon)); }
        }

        private string _invoiceIcon = "invoice";
        public string InvoiceIcon
        {
            get => _invoiceIcon;
            set { _invoiceIcon = value; OnPropertyChanged(nameof(InvoiceIcon)); }
        }

        private string _projectIcon = "project";
        public string ProjectIcon
        {
            get => _projectIcon;
            set { _projectIcon = value; OnPropertyChanged(nameof(ProjectIcon)); }
        }

        private string _accountIcon = "account";
        public string AccountIcon
        {
            get => _accountIcon;
            set { _accountIcon = value; OnPropertyChanged(nameof(AccountIcon)); }
        }

        private Color _homeMenuLabelColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
        public Color HomeMenuLabelColor
        {
            get => _homeMenuLabelColor;
            set { _homeMenuLabelColor = value; OnPropertyChanged(nameof(HomeMenuLabelColor)); }
        }

        private Color _invoiceMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
        public Color InvoiceMenuLabelColor
        {
            get => _invoiceMenuLabelColor;
            set { _invoiceMenuLabelColor = value; OnPropertyChanged(nameof(InvoiceMenuLabelColor)); }
        }

        private Color _projectMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
        public Color ProjectMenuLabelColor
        {
            get => _projectMenuLabelColor;
            set { _projectMenuLabelColor = value; OnPropertyChanged(nameof(ProjectMenuLabelColor)); }
        }

        private Color _accountMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
        public Color AccountMenuLabelColor
        {
            get => _accountMenuLabelColor;
            set { _accountMenuLabelColor = value; OnPropertyChanged(nameof(AccountMenuLabelColor)); }
        }

        public bool IsNotBusy => !IsBusy;

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
                RaisePropertyChanged(() => IsNotBusy);
            }
        }

        public virtual async Task InitializeAsync(object navigationData)
        {
            await Task.FromResult(false);
        }

        public object GetPropertyValue(string propertyName)
        {
            return GetType().GetProperty(propertyName).GetValue(this, null);
        }

        public T GetPropertyValue<T>(string propertyName)
        {
            var value = GetPropertyValue(propertyName);
            return (T)Convert.ChangeType(value, typeof(T));
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public async Task SetBusyAsync(Func<Task> func, string loadingMessage = null)
        {
            if (loadingMessage == null)
            {
                loadingMessage = L.Localize("LoadWithThreeDot");
            }

            UserDialogs.Instance.ShowLoading(loadingMessage, MaskType.Black);
            IsBusy = true;

            try
            {
                await func();
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
                IsBusy = false;
            }
        }

        public ICommand NavigationCommand { get { return new Command<string>(NavigationToPage); } }
        public async void NavigationToPage(string title)
        {
            try
            {
                if(title == "HOME")
                {
                    await App.Current.MainPage.Navigation.PopToRootAsync();
                }
                else if(title == "PROJECTS")
                {
                    ProjectView projectView = new ProjectView();
                    ProjectViewModel projectVM = new ProjectViewModel();
                    projectView.BindingContext = projectVM;

                    await App.Current.MainPage.Navigation.PushAsync(projectView, false);
                }
                else if (title == "INVOICE")
                {
                    InvoicePage invoicePage = new InvoicePage();
                    InvoicePageViewModel invoicePageVM = new InvoicePageViewModel();
                    invoicePage.BindingContext = invoicePageVM;

                    await App.Current.MainPage.Navigation.PushAsync(invoicePage, false);
                }
                else
                {
                    UploadDocumentView uploadDocumentView = new UploadDocumentView();
                    UploadDocumentViewModel uploadDocumentVM = new UploadDocumentViewModel();
                    uploadDocumentView.BindingContext = uploadDocumentVM;

                    await App.Current.MainPage.Navigation.PushAsync(uploadDocumentView, false);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public ICommand HomeCommand { get { return new Command(HomeIconClick); } }
        public async void HomeIconClick()
        {
            HomeIcon = "Home_active";
            InvoiceIcon = "invoice";
            ProjectIcon = "project";
            AccountIcon = "account";
            HomeMenuLabelColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
            InvoiceMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            ProjectMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            AccountMenuLabelColor = (Color)App.Current.Resources["LabelColor"];

            await App.Current.MainPage.Navigation.PopToRootAsync();
        }

        public ICommand InvoiceCommand { get { return new Command(InvoiceIconClick); } }
        public async void InvoiceIconClick()
        {
            HomeIcon = "Home";
            InvoiceIcon = "invoice_active";
            ProjectIcon = "project";
            AccountIcon = "account";
            HomeMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            InvoiceMenuLabelColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
            ProjectMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            AccountMenuLabelColor = (Color)App.Current.Resources["LabelColor"];

            //await NavigationService.SetMainPage<InvoicePage>();

            InvoicePage invoicePage = new InvoicePage();
            InvoicePageViewModel invoicePageVM = new InvoicePageViewModel();
            invoicePage.BindingContext = invoicePageVM;

            await App.Current.MainPage.Navigation.PushAsync(invoicePage, false);
        }

        public ICommand ProjectCommand { get { return new Command(ProjectIconClick); } }
        public void ProjectIconClick()
        {
            HomeIcon = "Home";
            InvoiceIcon = "invoice";
            ProjectIcon = "project_active";
            AccountIcon = "account";
            HomeMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            InvoiceMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            ProjectMenuLabelColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
            AccountMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
        }

        public ICommand AccountCommand { get { return new Command(AccountIconClick); } }
        public void AccountIconClick()
        {
            HomeIcon = "Home";
            InvoiceIcon = "invoice";
            ProjectIcon = "project";
            AccountIcon = "account_active";
            HomeMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            InvoiceMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            ProjectMenuLabelColor = (Color)App.Current.Resources["LabelColor"];
            AccountMenuLabelColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
        }

        public static async Task ShowPopup(PopupPage popup, bool animate = true)
        {
            try
            {
                //If popup is not present in stack push popup to poup stack.
                var isPopupPresentInStack = PopupNavigation.Instance.PopupStack?.Any(p => p.GetType() == popup.GetType()) ?? false;
                if (!isPopupPresentInStack)
                {
                    await PopupNavigation.Instance.PushAsync(popup, animate);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public ICommand ClosePopupCommand { get { return new Command(async () => { await ClosePopup(); }); } }
        public static async Task ClosePopup(bool animate = false)
        {
            try
            {
                if (PopupNavigation.Instance.PopupStack != null && PopupNavigation.Instance.PopupStack.Count > 0)
                {
                    await PopupNavigation.Instance.PopAsync(animate);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public ICommand BackCommand { get { return new Command(BackClick); } }
        private async void BackClick(object obj)
        {
            try
            {
                await NavigationService.GoBackAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}