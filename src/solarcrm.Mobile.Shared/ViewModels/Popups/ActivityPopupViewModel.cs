﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using solarcrm.ViewModels.Base;
using Xamarin.Forms;
using XF.Material.Forms.Models;

namespace solarcrm.ViewModels.Popups
{
	public class ActivityPopupViewModel : XamarinViewModel
	{
        #region Constructor

        public ActivityPopupViewModel()
        {
            ActivitiesList = new ObservableCollection<MaterialMenuItem>();
            ActivitiesList.Add(new MaterialMenuItem() { Text = "Reminder" });
            ActivitiesList.Add(new MaterialMenuItem() { Text = "SMS" });
            ActivitiesList.Add(new MaterialMenuItem() { Text = "Comment" });

            TemplatesList = new ObservableCollection<MaterialMenuItem>();
            TemplatesList.Add(new MaterialMenuItem() { Text = "Template 1" });
            TemplatesList.Add(new MaterialMenuItem() { Text = "Template 2" });
            TemplatesList.Add(new MaterialMenuItem() { Text = "Template 3" });
        }

        #endregion

        #region Private Properties

        private string _selectedActivity = "Select Activity";
        private string _selectedTemplate = "Select Template";
        private string _selectedDateTime = "Select Date & Time";
        private string _addNote;

        private bool _isShowRemiderView = true;
        private bool _isShowSmsView;
        private bool _isShowCommentsView;
        private bool _isSetButtonVisible = true;
        private bool _isSendButtonVisible;

        private ObservableCollection<MaterialMenuItem> _activitiesList;
        private ObservableCollection<MaterialMenuItem> _templatesList;

        #endregion

        #region Public Properties

        public string SelectedActivity
        {
            get { return _selectedActivity; }
            set { _selectedActivity = value; OnPropertyChanged(nameof(SelectedActivity)); }
        }

        public string SelectedTemplate
        {
            get { return _selectedTemplate; }
            set { _selectedTemplate = value; OnPropertyChanged(nameof(SelectedTemplate)); }
        }

        public string SelectedDateTime
        {
            get { return _selectedDateTime; }
            set { _selectedDateTime = value; OnPropertyChanged(nameof(SelectedDateTime)); }
        }

        public string AddNote
        {
            get { return _addNote; }
            set { _addNote = value; OnPropertyChanged(nameof(AddNote)); }
        }

        public bool IsShowRemiderView
        {
            get { return _isShowRemiderView; }
            set { _isShowRemiderView = value; OnPropertyChanged(nameof(IsShowRemiderView)); }
        }

        public bool IsShowSmsView
        {
            get { return _isShowSmsView; }
            set { _isShowSmsView = value; OnPropertyChanged(nameof(IsShowSmsView)); }
        }

        public bool IsShowCommentsView
        {
            get { return _isShowCommentsView; }
            set { _isShowCommentsView = value; OnPropertyChanged(nameof(IsShowCommentsView)); }
        }

        public bool IsSetButtonVisible
        {
            get { return _isSetButtonVisible; }
            set { _isSetButtonVisible = value; OnPropertyChanged(nameof(IsSetButtonVisible)); }
        }

        public bool IsSendButtonVisible
        {
            get { return _isSendButtonVisible; }
            set { _isSendButtonVisible = value; OnPropertyChanged(nameof(IsSendButtonVisible)); }
        }

        public ObservableCollection<MaterialMenuItem> ActivitiesList
        {
            get { return _activitiesList; }
            set { _activitiesList = value; OnPropertyChanged(nameof(ActivitiesList)); }
        }

        public ObservableCollection<MaterialMenuItem> TemplatesList
        {
            get { return _templatesList; }
            set { _templatesList = value; OnPropertyChanged(nameof(TemplatesList)); }
        }

        #endregion

        #region Commands

        public ICommand SelectActivityCommand => new Command<MaterialMenuResult>((s) => SelectActivity(s));
        public ICommand SelectTemplateCommand => new Command<MaterialMenuResult>((s) => SelectTemplate(s));

        #endregion

        #region Private Methods

        private void SelectActivity(MaterialMenuResult item)
        {
            try
            {
                if (item.Index != -1)
                {
                    SelectedActivity = ActivitiesList[item.Index].Text;

                    if (SelectedActivity == "Reminder")
                    {
                        IsShowRemiderView = true;
                        IsShowSmsView = false;
                        IsShowCommentsView = false;
                        IsSetButtonVisible = true;
                        IsSendButtonVisible = false;
                    }
                    else if (SelectedActivity == "SMS")
                    {
                        IsShowRemiderView = false;
                        IsShowSmsView = true;
                        IsShowCommentsView = false;
                        IsSetButtonVisible = false;
                        IsSendButtonVisible = true;
                    }
                    else
                    {
                        IsShowRemiderView = false;
                        IsShowSmsView = false;
                        IsShowCommentsView = true;
                        IsSetButtonVisible = false;
                        IsSendButtonVisible = true;
                    }

                    Debug.WriteLine(SelectedActivity);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void SelectTemplate(MaterialMenuResult item)
        {
            try
            {
                if (item.Index != -1)
                {
                    SelectedTemplate = TemplatesList[item.Index].Text;
                    Debug.WriteLine(SelectedTemplate);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
        #endregion


    }
}


