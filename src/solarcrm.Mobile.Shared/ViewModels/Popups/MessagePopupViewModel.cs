﻿using System;
using System.Windows.Input;
using solarcrm.ViewModels.Base;
using Xamarin.Forms;

namespace solarcrm.ViewModels.Popups
{
	public class MessagePopupViewModel : XamarinViewModel
	{
        #region Constructor

        public MessagePopupViewModel()
		{
		}

        #endregion

        #region Private Properties

        private string _alertTitle { get; set; }
        private string _message { get; set; }
        private string _messageDateTime { get; set; }

        #endregion

        #region Public Properties

        public string AlertTitle
        {
            get { return _alertTitle; }
            set
            {
                _alertTitle = value;
                OnPropertyChanged(nameof(AlertTitle));
            }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; OnPropertyChanged(nameof(Message)); }
        }

        public string MessageDateTime
        {
            get { return _messageDateTime; }
            set { _messageDateTime = value; OnPropertyChanged(nameof(MessageDateTime)); }
        }

        #endregion

        #region Commands
        #endregion

        #region Private Methods
        #endregion

    }
}

