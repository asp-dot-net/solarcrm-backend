﻿using System;
using System.Windows.Input;
using MvvmHelpers.Commands;
using solarcrm.ViewModels.Base;

namespace solarcrm.ViewModels.Popups
{
	public class UpdatePricePopupViewModel : XamarinViewModel
	{
		public UpdatePricePopupViewModel()
		{
		}

        #region Private Proeprties

		private double _depositeAmount;
		private double _systemStrengthCost;
		private double _agreementCharges;
		private double _netMeterCharges;

		#endregion

		#region Public Proeprties

		public double DepositeAmount
		{
            get { return _depositeAmount; }
            set { SetProperty(ref _depositeAmount, value); }
		}

		public double SystemStrengthCos
		{
			get { return _systemStrengthCost; }
			set { SetProperty(ref _systemStrengthCost, value); }
		}

		public double AgreementCharges
		{
			get { return _agreementCharges; }
			set { SetProperty(ref _agreementCharges, value); }
		}

		public double NetMeterCharges
		{
			get { return _netMeterCharges; }
			set { SetProperty(ref _netMeterCharges, value); }
		}

		public EventHandler<bool> ConfirmUpdate;

		#endregion

		#region Command

		public ICommand UpdateCommand { get { return new Command(UpdateClick); } }

		#endregion

		#region Public Method

		public void UpdateClick(object obj)
		{
			ConfirmUpdate.Invoke(this, true);
		}

        #endregion
    }
}

