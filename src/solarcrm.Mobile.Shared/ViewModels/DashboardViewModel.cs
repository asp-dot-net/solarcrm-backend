﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using solarcrm.Models;
using solarcrm.ViewModels.Base;
using solarcrm.ViewModels.Popups;
using solarcrm.Views.Popups;
using Xamarin.Forms;

namespace solarcrm.ViewModels
{
	public class DashboardViewModel : XamarinViewModel
	{
		public DashboardViewModel()
		{
			DashboardItemsList = new ObservableCollection<DashboardModel>()
			{
				new DashboardModel
				{
					Title = "50000",
					Description = "Total earning",
					Icon = "money.png",
					BgColor = "#5D5FEF",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "15",
					Description = "Total applications submitted",
					Icon = "sheet.png",
					BgColor = "#A5A6F6",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "6",
					SubTitle = "kw",
					Description = "Total system capicity",
					Icon = "bulb.png",
					BgColor = "#EF5DA8",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "0",
					Description = "Total projects ready to claim",
					Icon = "sheild.png",
					BgColor = "#DE1212",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "0",
					Description = "Total outstanding amount from customer",
					Icon = "money.png",
					BgColor = "#5D5FEF",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "110",
					Description = "Total KW (installation completed)",
					Icon = "lightning.png",
					BgColor = "#A5A6F6",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "320",
					Description = "Number of Panels (installation completed)",
					Icon = "solar_panel.png",
					BgColor = "#DE1212",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "30",
					SubTitle = "kw",
					Description = "Number of kilowatt (installation booked)",
					Icon = "verified_account.png",
					BgColor = "#EF5DA8",
					IsVisible = true
				},
				new DashboardModel
				{
					Title = "90",
					Description = "Number of Panels (installation booked)",
					Icon = "solar_panel.png",
					BgColor = "#019EE3",
					IsVisible = true
				},
			};
		}

		private ObservableCollection<DashboardModel> _dashboardItemsList { get; set; }
		public ObservableCollection<DashboardModel> DashboardItemsList
		{
			get => _dashboardItemsList;
            set { _dashboardItemsList = value; OnPropertyChanged(nameof(DashboardItemsList)); }
		}

		public ICommand DashboardItemTapCommand { get { return new Command(DashboardItemTapped); } }

        private async void DashboardItemTapped(object obj)
        {
			try
			{
				//ActivityPopup activityPopup = new ActivityPopup();
				//ActivityPopupViewModel activityPopupVm = new ActivityPopupViewModel();
				//activityPopup.BindingContext = activityPopupVm;

				await App.Current.MainPage.DisplayAlert("Alert", "Coming soon...", "OK");
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}
    }
}

