﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using solarcrm.Models;
using solarcrm.ViewModels.Base;
using solarcrm.Views.ProjectDetails;
using Xamarin.Forms;

namespace solarcrm.ViewModels.Project
{
	public class ProjectViewModel : XamarinViewModel
	{
        #region Constructor

        public ProjectViewModel()
		{
            GetProjects();

            TotalProjectsCount = Projects.Count();
		}

        #endregion

        #region Private Properties

        private int _totalProjectsCount;

        private ObservableCollection<ProjectModel> _projects;

        #endregion

        #region Public Properties

        public int TotalProjectsCount
        {
            get { return _totalProjectsCount; }
            set { SetProperty(ref _totalProjectsCount, value); }
        }

        public ObservableCollection<ProjectModel> Projects
        {
            get { return _projects; }
            set { SetProperty(ref _projects, value); }
        }

        #endregion

        #region Commands

        public ICommand ProjectTappedCommand { get { return new Command<ProjectModel>(ProjectClick); } }
        public ICommand ActionCommand { get { return new Command(ActionClick); } }
        public ICommand EditProjectDetailsCommand { get { return new Command(async () => await EditProjectDetails()); } }
        public ICommand UploadCommand { get { return new Command(ActionClick); } }

        #endregion

        #region Private Methods

        private void GetProjects()
        {
            Projects = new ObservableCollection<ProjectModel>()
            {
                new ProjectModel
                {
                    Id = 1,
                    UserName = "Hippal S Patel",
                    TagNumber = 34442,
                    Address = "Vijapur (M+OG), Gujarat, Mahesana 38",
                    ModifierName = "Hippal Patel"
                },
                new ProjectModel
                {
                    Id = 2,
                    UserName = "Nilesh Dhimar",
                    TagNumber = 34441,
                    Address = "Gujarat, Mahesana 38",
                    ModifierName = "Ravindra Gohel"
                },
            };
        }

        private async void ProjectClick(ProjectModel project)
        {
            try
            {
                await NavigationService.SetDetailPageAsync(typeof(ProjectDetailsView), navigationParameter: project, pushToStack: true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async void ActionClick()
        {
            await App.Current.MainPage.DisplayAlert("Alert", "Comming soon", "OK");
        }

        private async Task EditProjectDetails()
        {
            await App.Current.MainPage.DisplayAlert("Alert", "Edit project feature Comming soon", "OK");
        }

        #endregion
    }
}

