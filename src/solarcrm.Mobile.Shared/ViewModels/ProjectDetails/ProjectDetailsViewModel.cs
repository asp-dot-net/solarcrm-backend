﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using solarcrm.Helpers;
using solarcrm.Models;
using solarcrm.ViewModels.Base;
using solarcrm.ViewModels.Popups;
using solarcrm.Views.Popups;
using Xamarin.Forms;

namespace solarcrm.ViewModels.ProjectDetails
{
	public class ProjectDetailsViewModel : XamarinViewModel
	{
        #region Constructor

        public ProjectDetailsViewModel()
		{
            ProjectDetailsViewInitialization();
        }

        #endregion

        #region Private Properties

        private bool _detailTabIsVisible = true;
        private bool _salesTabIsVisible;
        private bool _quotesTabIsVisible;
       
        private Color _detailLabelColor;
        private Color _salesLabelColor;
        private Color _quotesLabelColor;
        private Color _detailIndicatorColor;
        private Color _salesIndicatorColor;
        private Color _quotesIndicatorColor;

        private ObservableCollection<QuoteModel> _quotes;
        private ObservableCollection<PanelDetailModel> _panelDetails;
        private ObservableCollection<InverterDetailsModel> _inverterDetails;
        private ObservableCollection<PricingDetailsModel> _pricingDetails;

        #endregion

        #region Public Properties

        public bool DetailTabIsVisible
        {
            get { return _detailTabIsVisible; }
            set { SetProperty(ref _detailTabIsVisible, value); }
        }

        public bool SalesTabIsVisible
        {
            get { return _salesTabIsVisible; }
            set { SetProperty(ref _salesTabIsVisible, value); }
        }

        public bool QuotesTabIsVisible
        {
            get { return _quotesTabIsVisible; }
            set { SetProperty(ref _quotesTabIsVisible, value); }
        }

        public Color DetailLabelColor
        {
            get { return _detailLabelColor; }
            set { SetProperty(ref _detailLabelColor, value); }
        }

        public Color SalesLabelColor
        {
            get { return _salesLabelColor; }
            set { SetProperty(ref _salesLabelColor, value); }
        }

        public Color QuotesLabelColor
        {
            get { return _quotesLabelColor; }
            set { SetProperty(ref _quotesLabelColor, value); }
        }

        public Color DetailIndicatorColor
        {
            get { return _detailIndicatorColor; }
            set { SetProperty(ref _detailIndicatorColor, value); }
        }

        public Color SalesIndicatorColor
        {
            get { return _salesIndicatorColor; }
            set { SetProperty(ref _salesIndicatorColor, value); }
        }

        public Color QuotesIndicatorColor
        {
            get { return _quotesIndicatorColor; }
            set { SetProperty(ref _quotesIndicatorColor, value); }
        }

        public ObservableCollection<QuoteModel> Quotes
        {
            get { return _quotes; }
            set { SetProperty(ref _quotes, value); }
        }

        public ObservableCollection<PanelDetailModel> PanelDetails
        {
            get { return _panelDetails; }
            set { SetProperty(ref _panelDetails, value); }
        }

        public ObservableCollection<InverterDetailsModel> InverterDetails
        {
            get { return _inverterDetails; }
            set { SetProperty(ref _inverterDetails, value); }
        }

        public ObservableCollection<PricingDetailsModel> PricingDetails
        {
            get { return _pricingDetails; }
            set { SetProperty(ref _pricingDetails, value); }
        }

        #endregion

        #region Commands

        public ICommand DetailTabCommand { get { return new Command(DetailTabClick); } }
        public ICommand SalesTabCommand { get { return new Command(SalesTabClicked); } }
        public ICommand QuotesTabCommand { get { return new Command(QuotesTablick); } }
        public ICommand UpdatePriceCommand { get { return new Command(UpdatePriceClick); } }

        #endregion

        #region Public Methods

        public async override Task InitializeAsync(object navigationData)
        {
            var project = navigationData as ProjectModel;
        }

        public void DetailTabClick()
        {
            DetailTabIsVisible = true;
            SalesTabIsVisible = false;
            QuotesTabIsVisible = false;

            ProjectDetailsViewInitialization();
        }

        public void SalesTabClicked()
        {
            DetailTabIsVisible = false;
            SalesTabIsVisible = true;
            QuotesTabIsVisible = false;

            DetailLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            SalesLabelColor = (Color)App.Current.Resources["PacificBlueColor"];
            QuotesLabelColor = (Color)App.Current.Resources["BodyTextColor"];

            DetailIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            SalesIndicatorColor = (Color)App.Current.Resources["PacificBlueColor"];
            QuotesIndicatorColor = (Color)App.Current.Resources["BlackColor"];

            GetSalesTabData();
        }

        public void QuotesTablick()
        {
            DetailTabIsVisible = false;
            SalesTabIsVisible = false;
            QuotesTabIsVisible = true;

            DetailLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            SalesLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            QuotesLabelColor = (Color)App.Current.Resources["PacificBlueColor"];

            DetailIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            SalesIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            QuotesIndicatorColor = (Color)App.Current.Resources["PacificBlueColor"];

            GetQuotes();
           
        }

        public void ProjectDetailsViewInitialization()
        {
            DetailLabelColor = (Color)App.Current.Resources["PacificBlueColor"];
            SalesLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            QuotesLabelColor = (Color)App.Current.Resources["BodyTextColor"];

            DetailIndicatorColor = (Color)App.Current.Resources["PacificBlueColor"];
            SalesIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            QuotesIndicatorColor = (Color)App.Current.Resources["BlackColor"];
        }

        public void GetQuotes()
        {
            Quotes = new ObservableCollection<QuoteModel>()
            {
                new QuoteModel
                {
                    Id = 1,
                    QuoteNo = 2052,
                    ProjectNo = 33832,
                    QuoteDate = new DateTime(2022,06,15)
                },
                new QuoteModel
                {
                    Id = 2,
                    QuoteNo = 2052,
                    ProjectNo = 33832,
                    QuoteDate = new DateTime(2022,06,15)
                }
            };
        }

        public void GetSalesTabData()
        {
            PanelDetails = new ObservableCollection<PanelDetailModel>()
            {
                new PanelDetailModel
                {
                    Id = 1,
                    Title = "Panel",
                    Value = "APS 330 Wp Panel"
                },
                new PanelDetailModel
                {
                    Id = 2,
                    Title = "Brand",
                    Value = "Australian Premium Solar"
                },
                new PanelDetailModel
                {
                    Id = 3,
                    Title = "No. of Panels",
                    Value = "3"
                },
                new PanelDetailModel
                {
                    Id = 4,
                    Title = "Watts/Panel",
                    Value = "330"
                },
                new PanelDetailModel
                {
                    Id = 5,
                    Title = "System Capacity",
                    Value = "0.99"
                },
                new PanelDetailModel
                {
                    Id = 5,
                    Title = "Model",
                    Value = "APSAP6-330/72"
                },
            };

            InverterDetails = new ObservableCollection<InverterDetailsModel>()
            {
                new InverterDetailsModel
                {
                    Id = 1,
                    Title = "Inverter",
                    Value = "APS 3.3 kw"
                },
                new InverterDetailsModel
                {
                    Id = 2,
                    Title = "Brand",
                    Value = "Australian Premium Solar"
                },
                new InverterDetailsModel
                {
                    Id = 3,
                    Title = "Quantiry",
                    Value = "1"
                },
                new InverterDetailsModel
                {
                    Id = 4,
                    Title = "Series",
                    Value = "APS"
                },
                new InverterDetailsModel
                {
                    Id = 5,
                    Title = "Model",
                    Value = "APSG-1-3.3k-in"
                },
            };

            PricingDetails = new ObservableCollection<PricingDetailsModel>()
            {
                new PricingDetailsModel
                {
                    Id = 1,
                    Title = AppStrings.ActualCost,
                    Value = 135276.00,
                    IsFinalValue = true,
                },
                new PricingDetailsModel
                {
                    Id = 2,
                    Title = AppStrings.MaxSubsidy,
                    Value = 49192.00
                },
                new PricingDetailsModel
                {
                    Id = 3,
                    Title = AppStrings.MinSubsidy,
                    Value = 2460.00
                },
                new PricingDetailsModel
                {
                    Id = 4,
                    Title = AppStrings.ActualSubsidyAmt,
                    Value = 51652
                },
                new PricingDetailsModel
                {
                    Id = 5,
                    Title = AppStrings.TotalPayableAmt,
                    Value = 83624.00
                },
                new PricingDetailsModel
                {
                    Id = 6,
                    Title = AppStrings.DepositeAmount,
                    Value = 8326.00
                }
                ,new PricingDetailsModel
                {
                    Id = 7,
                    Title = AppStrings.SystemStrengtheingCost,
                    Value = 500.00
                },
                new PricingDetailsModel
                {
                    Id = 8,
                    Title = AppStrings.MeterCharges,
                    Value = 500.00
                },
                new PricingDetailsModel
                {
                    Id = 9,
                    Title = AppStrings.NetTotal,
                    Value = 84625.00,
                    IsFinalValue = true,
                },
            };
        }

        private async void UpdatePriceClick()
        {
            UpdatePricePopup updatePricePopup = new UpdatePricePopup();
            UpdatePricePopupViewModel updatePricePopupVM = new UpdatePricePopupViewModel();

            updatePricePopupVM.DepositeAmount = PricingDetails.First(x => x.Title == AppStrings.DepositeAmount).Value;
            updatePricePopupVM.SystemStrengthCos = PricingDetails.First(x => x.Title == AppStrings.SystemStrengtheingCost).Value;
            updatePricePopupVM.AgreementCharges = (PricingDetails.First(x => x.Title == AppStrings.MeterCharges).Value * 60) / 100;
            updatePricePopupVM.NetMeterCharges = (PricingDetails.First(x => x.Title == AppStrings.MeterCharges).Value * 40) / 100;

            updatePricePopup.BindingContext = updatePricePopupVM;

            await ShowPopup(updatePricePopup);

            updatePricePopupVM.ConfirmUpdate += async (sender, e) =>
            {
                PricingDetails.ToList().ForEach((obj) =>
                {
                    if(obj.Title == AppStrings.DepositeAmount)
                    {
                        obj.Value = updatePricePopupVM.DepositeAmount;
                    }
                });
                await App.Current.MainPage.DisplayAlert("Alert", "Pricing details updated", "OK");
                await ClosePopup();
            };
        }


        #endregion
    }
}

