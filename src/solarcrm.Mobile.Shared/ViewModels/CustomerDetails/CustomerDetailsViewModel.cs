﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Abp.Application.Services.Dto;
using solarcrm.Helpers;
using solarcrm.Localization.Resources;
using solarcrm.MobileService.MCustomer;
using solarcrm.MobileService.MCustomer.Dto;
using solarcrm.Models;
using solarcrm.ViewModels.Base;
using solarcrm.ViewModels.Popups;
using solarcrm.Views.Popups;
using Xamarin.Forms;
using XF.Material.Forms.Models;

namespace solarcrm.ViewModels.CustomerDetails
{
    public class CustomerDetailsViewModel : XamarinViewModel
    {
        #region Constructor 

        public CustomerDetailsViewModel(ICustomerAppService customerAppService)
        {
            _customerAppService = customerAppService;
            CustomerDetailsViewInitialization();

            GetActionItems();
        }

        #endregion

        #region Private Properties

        private readonly ICustomerAppService _customerAppService;
        private bool _isInitialized;
        private GetAllCustomerInput _filter;
        private bool _summaryTabIsVisible = true;
        private bool _activityTabIsVisible;
        private bool _documentsTabIsVisible;
        private bool _addDocumentTabIsVisible;
        private bool _isLeadStatusVisible;
        private bool _isApplicationStatusVisible;

        private string _pickedDocumentName = AppStrings.SelectDocumentText;
        private string _jobNumber;
        private string _customerName;
        private string _address;
        private string _mobileNumber;
        private string _leadStatus;
        private string _applicationStatus;

        private Color _summaryLabelColor;
        private Color _activityLabelColor;
        private Color _documentsLabelColor;
        private Color _summaryIndicatorColor;
        private Color _activityIndicatorColor;
        private Color _documentsIndicatorColor;
        private Color _pickedDocTextColor = (Color)App.Current.Resources["LightTextColor"];

        private ObservableCollection<DocumentModel> _documents { get; set; }
        private ObservableCollection<TechnicalDetailsDto> _technicalDetails { get; set; }
        private ObservableCollection<MaterialMenuItem> _actionsList { get; set; }
        private ObservableCollection<ActivityModel> _activities { get; set; }

        #endregion

        #region Public Properties

        public int CustomerId;

        public bool SummaryTabIsVisible
        {
            get { return _summaryTabIsVisible; }
            set { _summaryTabIsVisible = value; OnPropertyChanged(nameof(SummaryTabIsVisible)); }
        }

        public bool ActivityTabIsVisible
        {
            get { return _activityTabIsVisible; }
            set { _activityTabIsVisible = value; OnPropertyChanged(nameof(ActivityTabIsVisible)); }
        }

        public bool DocumentsTabIsVisible
        {
            get { return _documentsTabIsVisible; }
            set { _documentsTabIsVisible = value; OnPropertyChanged(nameof(DocumentsTabIsVisible)); }
        }

        public bool AddDocumentTabIsVisible
        {
            get { return _addDocumentTabIsVisible; }
            set { _addDocumentTabIsVisible = value; OnPropertyChanged(nameof(AddDocumentTabIsVisible)); }
        }

        public bool IsLeadStatusVisible
        {
            get { return _isLeadStatusVisible; }
            set { _isLeadStatusVisible = value; OnPropertyChanged(nameof(IsLeadStatusVisible)); }
        }

        public bool IsApplicationStatusVisible
        {
            get { return _isApplicationStatusVisible; }
            set { _isApplicationStatusVisible = value; OnPropertyChanged(nameof(IsApplicationStatusVisible)); }
        }


        public string PickedDocumentName
        {
            get { return _pickedDocumentName; }
            set { _pickedDocumentName = value; OnPropertyChanged(nameof(PickedDocumentName)); }
        }

        public string JobNumber
        {
            get { return _jobNumber; }
            set { _jobNumber = value; OnPropertyChanged(nameof(JobNumber)); }
        }

        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; OnPropertyChanged(nameof(CustomerName)); }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged(nameof(Address)); }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
            set { _mobileNumber = value; OnPropertyChanged(nameof(MobileNumber)); }
        }

        public string LeadStatus
        {
            get { return _leadStatus; }
            set { _leadStatus = value; OnPropertyChanged(nameof(LeadStatus)); }
        }

        public string ApplicationStatus
        {
            get { return _applicationStatus; }
            set { _applicationStatus = value; OnPropertyChanged(nameof(ApplicationStatus)); }
        }

        public Color SummaryLabelColor
        {
            get { return _summaryLabelColor; }
            set { _summaryLabelColor = value; OnPropertyChanged(nameof(SummaryLabelColor)); }
        }

        public Color ActivityLabelColor
        {
            get { return _activityLabelColor; }
            set { _activityLabelColor = value; OnPropertyChanged(nameof(ActivityLabelColor)); }
        }

        public Color DocumentsLabelColor
        {
            get { return _documentsLabelColor; }
            set { _documentsLabelColor = value; OnPropertyChanged(nameof(DocumentsLabelColor)); }
        }

        public Color SummaryIndicatorColor
        {
            get { return _summaryIndicatorColor; }
            set { _summaryIndicatorColor = value; OnPropertyChanged(nameof(SummaryIndicatorColor)); }
        }

        public Color ActivityIndicatorColor
        {
            get { return _activityIndicatorColor; }
            set { _activityIndicatorColor = value; OnPropertyChanged(nameof(ActivityIndicatorColor)); }
        }

        public Color DocumentsIndicatorColor
        {
            get { return _documentsIndicatorColor; }
            set { _documentsIndicatorColor = value; OnPropertyChanged(nameof(DocumentsIndicatorColor)); }
        }

        public Color PickedDocTextColor
        {
            get { return _pickedDocTextColor; }
            set { _pickedDocTextColor = value; OnPropertyChanged(nameof(PickedDocTextColor)); }
        }

        public ObservableCollection<DocumentModel> Documents
        {
            get => _documents;
            set { _documents = value; OnPropertyChanged(nameof(Documents)); }
        }

        public ObservableCollection<TechnicalDetailsDto> TechnicalDetails
        {
            get => _technicalDetails;
            set { _technicalDetails = value; OnPropertyChanged(nameof(TechnicalDetails)); }
        }

        public ObservableCollection<MaterialMenuItem> ActionsList
        {
            get => _actionsList;
            set { _actionsList = value; OnPropertyChanged(nameof(ActionsList)); }
        }

        public ObservableCollection<ActivityModel> Activities
        {
            get => _activities;
            set { _activities = value; OnPropertyChanged(nameof(Activities)); }
        }

        #endregion

        #region Commands

        public ICommand AddDocumentCommand { get { return new Command(AddDocumentClick); } }
        public ICommand ActivityTabCommand { get { return new Command(ActivityTabClicked); } }
        public ICommand AddActivityCommand { get { return new Command(AddActivityClick); } }
        public ICommand DocumentsTabCommand { get { return new Command(DocumentsTabClicked); } }
        public ICommand SummaryTabCommand { get { return new Command(SummaryTabClicked); } }
        public ICommand SelectDocumentCommand => new Command<MaterialMenuResult>((s) => SelectDocumentClick(s));
        public ICommand ShowMessageCommand { get { return new Command<ActivityModel>(ShowMessagePopup); } }
        public ICommand UploadDocumentCommand { get { return new Command(UploadDocument); } }

        #endregion

        #region Private Methods

        private void SummaryTabClicked()
        {
            SummaryTabIsVisible = true;
            ActivityTabIsVisible = false;
            DocumentsTabIsVisible = false;
            AddDocumentTabIsVisible = false;

            CustomerDetailsViewInitialization();
        }

        private void ActivityTabClicked()
        {
            SummaryTabIsVisible = false;
            ActivityTabIsVisible = true;
            DocumentsTabIsVisible = false;
            AddDocumentTabIsVisible = false;

            SummaryLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            ActivityLabelColor = (Color)App.Current.Resources["PacificBlueColor"];
            DocumentsLabelColor = (Color)App.Current.Resources["BodyTextColor"];

            SummaryIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            ActivityIndicatorColor = (Color)App.Current.Resources["PacificBlueColor"];
            DocumentsIndicatorColor = (Color)App.Current.Resources["BlackColor"];

            GetActivities();
        }

        private void DocumentsTabClicked()
        {
            SummaryTabIsVisible = false;
            ActivityTabIsVisible = false;
            DocumentsTabIsVisible = true;
            AddDocumentTabIsVisible = false;

            SummaryLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            ActivityLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            DocumentsLabelColor = (Color)App.Current.Resources["PacificBlueColor"];

            SummaryIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            ActivityIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            DocumentsIndicatorColor = (Color)App.Current.Resources["PacificBlueColor"];

            GetDocuments();
        }

        private void SelectDocumentClick(MaterialMenuResult item)
        {
            try
            {
                if (item.Index != -1)
                {
                    PickedDocumentName = ActionsList[item.Index].Text;
                    PickedDocTextColor = PickedDocumentName != AppStrings.SelectDocumentText
                                                            ? (Color)App.Current.Resources["BlackColor"]
                                                            : (Color)App.Current.Resources["LightTextColor"];
                    Debug.WriteLine(PickedDocumentName);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void AddDocumentClick()
        {
            SummaryTabIsVisible = false;
            ActivityTabIsVisible = false;
            DocumentsTabIsVisible = false;
            AddDocumentTabIsVisible = true;

            SummaryLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            ActivityLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            DocumentsLabelColor = (Color)App.Current.Resources["PacificBlueColor"];

            SummaryIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            ActivityIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            DocumentsIndicatorColor = (Color)App.Current.Resources["PacificBlueColor"];
        }

        private async void ShowMessagePopup(ActivityModel activity)
        {
            try
            {
                MessagePopup messagePopup = new MessagePopup();
                MessagePopupViewModel messagePopupVM = new MessagePopupViewModel();

                messagePopupVM.AlertTitle = $"{activity.User} - {activity.SubTitle}";
                messagePopupVM.Message = "Dear himanshu, Greetings from Australian Premium Solar (India) Pvt. Ltd. The agreement for your solar rooftop system application number himanshu is submitted to the himanshuhimanshu on himanshu. Feel free to call on himanshu for any query. Regards, Team APS";
                messagePopupVM.MessageDateTime = activity.LogDateTime;

                messagePopup.BindingContext = messagePopupVM;

                await ShowPopup(messagePopup);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }


        private async void AddActivityClick()
        {
            try
            {
                ActivityPopup activityPopup = new ActivityPopup();
                ActivityPopupViewModel activityPopupVm = new ActivityPopupViewModel();
                activityPopup.BindingContext = activityPopupVm;

                await ShowPopup(activityPopup);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void UploadDocument()
        {
            CommonUtility.DocumentPicker();
        }

        #endregion

        #region Public Methods

        public async override Task InitializeAsync(object navigationData)
        {
            var customer = navigationData as CustomerDto;

            CustomerId = customer.Id;
            CustomerName = customer.CustomerName;
            Address = $"{customer.Address1} {customer.Address2} {customer.Address3}";
            MobileNumber = customer.Mobile;

            IsLeadStatusVisible = customer.IsLeadStatusVisible;
            IsApplicationStatusVisible = customer.IsApplicationStatusVisible;
            LeadStatus = customer.LeadStatus;
            ApplicationStatus = customer.ApplicationStatus;

            if (_isInitialized)
            {
                return;
            }

            await SetBusyAsync(async () =>
            {
                await FetchDataAsync();
            });

            _isInitialized = true;
        }

        private async Task FetchDataAsync()
        {
            try
            {
                var result = await _customerAppService.GetCustomerForView(new NullableIdDto<int>(CustomerId));
                if (result.TechnicalDetails != null && result.TechnicalDetails.Count > 0)
                {
                    JobNumber = !string.IsNullOrEmpty(result.JobNumber) ? $"#{result.JobNumber}" : "";
                    TechnicalDetails = new ObservableCollection<TechnicalDetailsDto>(result.TechnicalDetails);
                }
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Error", ex.Message, "OK");
                Debug.WriteLine(ex.Message);
            }
        }

        public void CustomerDetailsViewInitialization()
        {
            SummaryLabelColor = (Color)App.Current.Resources["PacificBlueColor"];
            ActivityLabelColor = (Color)App.Current.Resources["BodyTextColor"];
            DocumentsLabelColor = (Color)App.Current.Resources["BodyTextColor"];

            SummaryIndicatorColor = (Color)App.Current.Resources["PacificBlueColor"];
            ActivityIndicatorColor = (Color)App.Current.Resources["BlackColor"];
            DocumentsIndicatorColor = (Color)App.Current.Resources["BlackColor"];
        }

        public void GetActionItems()
        {
            ActionsList = new ObservableCollection<MaterialMenuItem>()
            {
                new MaterialMenuItem
                {
                    Text = "Document 1"
                },
                new MaterialMenuItem
                {
                     Text = "Document 2"
                },
                new MaterialMenuItem
                {
                     Text = "Document 3"
                },
                new MaterialMenuItem
                {
                    Text = "Document 4"
                },
            };
        }

        public void GetActivities()
        {
            Activities = new ObservableCollection<ActivityModel>();
            Activities.Add(new ActivityModel { Id = 1, Icon = "sms_icon", Title = "Manage Lead", SubTitle = "SMS Sent", User = "Admin", LogDateTime = "10-06-2022 12:01:08" });
            Activities.Add(new ActivityModel { Id = 2, Icon = "bell_icon", Title = "District", SubTitle = "Reminder Sent", User = "Nilesh", LogDateTime = "10-06-2022 12:01:08" });
            Activities.Add(new ActivityModel { Id = 3, Icon = "notes_icon", Title = "Manage Lead", SubTitle = "Lead Modified", User = "Hippal Patel", LogDateTime = "10-06-2022 12:01:08" });
            Activities.Add(new ActivityModel { Id = 4, Icon = "todolist_icon", Title = "Manage Lead", SubTitle = "To Do", User = "Super Admin", LogDateTime = "10-06-2022 12:01:08" });
            Activities.Add(new ActivityModel { Id = 4, Icon = "todolist_icon", Title = "Manage Lead", SubTitle = "To Do", User = "Super Admin", LogDateTime = "10-06-2022 12:01:08" });
        }

        public void GetDocuments()
        {
            Documents = new ObservableCollection<DocumentModel>();
            Documents.Add(new DocumentModel { Id = 1, Icon = "image_thumbnail.png", SerialNumber = 1223, Title = "Aadhar Card", UploadDate = "10/06/2022" });
            Documents.Add(new DocumentModel { Id = 2, Icon = "pdf_thumbnail.png", SerialNumber = 1220, Title = "Customer Photo", UploadDate = "09/06/2022" });
        }

        #endregion
    }
}

