﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using MvvmHelpers.Commands;
using solarcrm.Models;
using solarcrm.ViewModels.Base;
using solarcrm.Views.CustomerDetails;

namespace solarcrm.ViewModels
{
    public class ActionsMenuPopupViewModel : XamarinViewModel
	{
		public ActionsMenuPopupViewModel()
		{
			GetActionItems();
		}

		public int CustomerId;

		public EventHandler<bool> ItemSelect;

		private ObservableCollection<ActionsModel> _actionsList { get; set; }
		public ObservableCollection<ActionsModel> ActionsList
		{
			get => _actionsList;
			set { _actionsList = value; OnPropertyChanged(nameof(ActionsList)); }
		}

		public ICommand ActionSelectedCommand { get { return new Command<ActionsModel>(SelectedAction); } }
		private async void SelectedAction(ActionsModel model)
		{
			if (model.Id == 1 && model.Title == "Details") 
			{
				await ClosePopup();
				await NavigationService.SetDetailPageAsync(typeof(CustomerDetailsView), navigationParameter: CustomerId, pushToStack: true);
			}
		}

		public void GetActionItems()
		{
			ActionsList = new ObservableCollection<ActionsModel>()
			{
				new ActionsModel
				{
					Id = 1,
					Title = "Details",
					Icon = "eye.png"
				},
				new ActionsModel
				{
					Id = 2,
					Title = "Edit",
					Icon = "pencil.png"
				},
				new ActionsModel
				{
					Id = 3,
					Title = "Upload Document",
					Icon = "upload_fill.png"
				},
				new ActionsModel
				{
					Id = 4,
					Title = "Add Project",
					Icon = "plus.png"
				},
			};
		}
	}
}

