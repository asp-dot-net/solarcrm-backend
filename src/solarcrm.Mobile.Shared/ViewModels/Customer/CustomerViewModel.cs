﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Abp.Application.Services.Dto;
using Castle.Core.Resource;
using solarcrm.MobileService.MCustomer;
using solarcrm.MobileService.MCustomer.Dto;
using solarcrm.ViewModels.Base;
using solarcrm.Views.CreateCustomer;
using solarcrm.Views.CustomerDetails;
using solarcrm.Views.Subviews;
using Xamarin.Forms;
using Command = Xamarin.Forms.Command;

namespace solarcrm.ViewModels.Customer
{
    public class CustomerViewModel : XamarinViewModel
	{
		#region Constructor

		public CustomerViewModel(ICustomerAppService customerAppService)
		{
			_customerAppService = customerAppService;

			Device.BeginInvokeOnMainThread(async () =>
			{
				await PageAppearingAsync();
			});

			AddCustomerEventHandler += CutomerVm_AddCustomerEventHandler;
		}

        #endregion

        #region Private Properties

        private readonly ICustomerAppService _customerAppService;
		private string _searchText;
		private bool _isInitialized;
		private bool _isShowQuotationGivenBtn = false;
		private bool _isShowApplicationSubmittedBtn = false;
		private bool _isShowDocumentVerifiedBtn = false;

		private int _totalRecords = 0;
		private int _currentPage = 1;
		private int _lastPage = 0;

		private GetAllCustomerInput _filter;
		private ObservableCollection<CustomerDto> _customersList { get; set; }
		private ObservableCollection<CustomerDto> _tempCustomersList { get; set; }

		#endregion

		#region Public Properties

		public static EventHandler<bool> AddCustomerEventHandler;

		public string SearchText
		{
			get { return _searchText; }
			set { _searchText = value; OnPropertyChanged(nameof(SearchText)); }
		}

		public bool IsShowQuotationGivenBtn
		{
			get => _isShowQuotationGivenBtn;
			set { _isShowQuotationGivenBtn = value; OnPropertyChanged(nameof(IsShowQuotationGivenBtn)); }
		}

		public bool IsShowApplicationSubmittedBtn
		{
			get => _isShowApplicationSubmittedBtn;
			set { _isShowApplicationSubmittedBtn = value; OnPropertyChanged(nameof(IsShowApplicationSubmittedBtn)); }
		}

		public bool IsShowDocumentVerifiedBtn
		{
			get => _isShowDocumentVerifiedBtn;
			set { _isShowDocumentVerifiedBtn = value; OnPropertyChanged(nameof(IsShowDocumentVerifiedBtn)); }
		}

		public int TotalRecords
		{
			get { return _totalRecords; }
			set { _totalRecords = value; OnPropertyChanged(nameof(TotalRecords)); }
		}

		public int CurrentPage
		{
			get { return _currentPage; }
			set { _currentPage = value; OnPropertyChanged(nameof(CurrentPage)); }
		}

		public int LastPage
		{
			get { return _lastPage; }
			set { _lastPage = value; OnPropertyChanged(nameof(LastPage)); }
		}

		public ObservableCollection<CustomerDto> CustomersList
		{
			get => _customersList;
			set { _customersList = value; OnPropertyChanged(nameof(CustomersList)); }
		}

		public ObservableCollection<CustomerDto> TempCustomersList
		{
			get => _tempCustomersList;
			set { _tempCustomersList = value; OnPropertyChanged(nameof(TempCustomersList)); }
		}

		#endregion

		#region Commands

		public ICommand AddActionCommand { get { return new Command(AddActionClick); } }
		public ICommand AddCustomerCommand { get { return new Command(AddCustomer); } }

        //public ICommand AddCustomerCommand { get { return new Command(AddCustomer); } }

        public ICommand CustomerTappedCommand { get { return new Command<CustomerDto>(CustomerClick); } }
		public ICommand EditActionCommand { get { return new Command<CustomerDto>(EditActionClick); } }
		public ICommand UploadActionCommand { get { return new Command(UploadActionClick); } }
		
		public ICommand SearchCommand { get { return new Command<string>(SearchClick); } }
		public ICommand LoadMoreCommand { get { return new Command(LoadMore); } }

		#endregion

		#region Private Properties

		private void CutomerVm_AddCustomerEventHandler(object sender, bool IsAdded)
		{
            if (IsAdded)
			{
                Device.BeginInvokeOnMainThread(async () =>
                {
					_isInitialized = false;
					await PageAppearingAsync();
                });
            }
		}

		private async void AddCustomer()
		{
            try
            {
				await NavigationService.SetDetailPageAsync(typeof(CreateCustomerView), pushToStack: true);
			}
			catch (Exception ex)
            {
				Debug.WriteLine(ex.Message);
            }
		}

		private async Task FetchDataAsync(string filterText = null, bool overwrite = false, int skipCount = 0)
		{
			_filter = new GetAllCustomerInput
			{
				Filter = filterText,
				MaxResultCount = PageDefaults.PageSize,
				SkipCount = skipCount
			};

			await FetchCustomerAsync(overwrite);
		}

		private async Task SearchDataAsync(string filterText = null, bool overwrite = false)
		{
			_filter = new GetAllCustomerInput
			{
				Filter = filterText,
			};

			await FetchCustomerAsync(overwrite);
		}

		private async Task FetchCustomerAsync(bool overwrite = false)
		{
			try
			{
				var result = await _customerAppService.GetAll(_filter);
				if (result.Items != null && result.Items.Count > 0)
				{
					TotalRecords = result.TotalCount;

					var data = new ObservableCollection<CustomerDto>();

					foreach (var item in result.Items)
					{
						item.Address1 = !string.IsNullOrEmpty(item.Address1) ? item.Address1 : "Not available";
						item.Address2 = !string.IsNullOrEmpty(item.Address2) ? item.Address2 : string.Empty;
						item.Address3 = !string.IsNullOrEmpty(item.Address3) ? item.Address3 : string.Empty;
						item.Mobile = !string.IsNullOrEmpty(item.Mobile) ? item.Mobile : "Not available";
						item.IsLeadStatusVisible = !string.IsNullOrEmpty(item.LeadStatus) ? true : false;
						item.IsApplicationStatusVisible = !string.IsNullOrEmpty(item.ApplicationStatus) ? true : false;
						item.CustomerBadge = item.IsVerified == true ? "verified_user.png" : "unverified_user.png";
						item.IsJob = item.IsVerified == true && item.IsJob == false ? true : false;
						data.Add(item);
					}

					CustomersList = new ObservableCollection<CustomerDto>(data);
					TempCustomersList = new ObservableCollection<CustomerDto>(data);
				}
			}
			catch (Exception ex)
			{
				_isInitialized = true;
				Debug.WriteLine(ex.Message);
			}


			/*if (overwrite)
			{
				Customers.ReplaceRange(ObjectMapper.Map<List<CustomerListModel>>(result.Items));
			}
			else
			{
				Customers.AddRange(ObjectMapper.Map<List<CustomerListModel>>(result.Items));
			}

			TotalCustomerCount = result.TotalCount;*/
		}

		private async void CustomerClick(CustomerDto customer)
		{
			try
			{
				await NavigationService.SetDetailPageAsync(typeof(CustomerDetailsView), navigationParameter: customer, pushToStack: true);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}

		private async void EditActionClick(CustomerDto customer)
		{
			try
			{				
                var result = await _customerAppService.GetCustomerForEdit(new NullableIdDto<int>(customer.Id));
				if (result.Customer != null)
				{
                    await NavigationService.SetDetailPageAsync(typeof(CreateCustomerView), navigationParameter: result.Customer, pushToStack: true);
                }
                    
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            
            //await App.Current.MainPage.DisplayAlert("Alert", "Edit Action Clicked123", "OK");
            //try
            //{
            //	ActionsMenuPopup actionsMenuPopup = new ActionsMenuPopup();
            //	ActionsMenuPopupViewModel actionsMenuPopupVM = new ActionsMenuPopupViewModel();

            //	actionsMenuPopupVM.CustomerId = customer.Id;
            //	actionsMenuPopup.BindingContext = actionsMenuPopupVM;

            //	await ShowPopup(actionsMenuPopup);
            //}
            //catch (Exception ex)
            //{
            //	Debug.WriteLine(ex.Message);
            //}
        }

		private async void UploadActionClick()
		{
			await App.Current.MainPage.DisplayAlert("Alert", "Upload Action Clicked", "OK");
		}

		private async void AddActionClick()
		{
			await App.Current.MainPage.DisplayAlert("Alert", "Add Action Clicked", "OK");
		}

        #endregion

        #region Public Methods

        public async Task PageAppearingAsync()
		{
			if (_isInitialized)
			{
				return;
			}

			await SetBusyAsync(async () =>
			{
				await FetchDataAsync(overwrite: true);
			});

			_isInitialized = true;
		}

		public async void SearchClick(string keyword)
		{
			try
			{
				if (keyword != null && keyword.Count() >= 2)
				{
					await FetchDataAsync(filterText: keyword.ToLower());
				}
				else
				{
					await SetBusyAsync(async () =>
					{
						await FetchDataAsync(overwrite: true);
					});
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}

		public async void LoadMore()
		{
			try
			{
				_filter = new GetAllCustomerInput
				{
					SkipCount = CustomersList.Count(),
					MaxResultCount = 10,
				};

				var tempOpenData = new ObservableCollection<CustomerDto>(CustomersList);

				var result = await _customerAppService.GetAll(_filter);
				if (result.Items != null && result.Items.Count > 0)
				{
					foreach (var item in result.Items)
					{
						item.Address1 = !string.IsNullOrEmpty(item.Address1) ? item.Address1 : "Not available";
						item.Address2 = !string.IsNullOrEmpty(item.Address2) ? item.Address2 : string.Empty;
						item.Address3 = !string.IsNullOrEmpty(item.Address3) ? item.Address3 : string.Empty;
						item.Mobile = !string.IsNullOrEmpty(item.Mobile) ? item.Mobile : "Not available";
						item.IsLeadStatusVisible = !string.IsNullOrEmpty(item.LeadStatus) ? true : false;
						item.IsApplicationStatusVisible = !string.IsNullOrEmpty(item.ApplicationStatus) ? true : false;
						item.CustomerBadge = item.IsVerified == true ? "verified_user.png" : "unverified_user.png";
						tempOpenData.Add(item);
					}

					ObservableCollection<CustomerDto> OrerbyIdDesc = new ObservableCollection<CustomerDto>(tempOpenData.OrderBy(x => x.Id));
					CustomersList = new ObservableCollection<CustomerDto>(OrerbyIdDesc);
					TempCustomersList = new ObservableCollection<CustomerDto>(OrerbyIdDesc);
				}
			}
			catch (Exception ex)
			{
				_isInitialized = true;
				Debug.WriteLine(ex.Message);
			}
		}

		#endregion

	}

}

