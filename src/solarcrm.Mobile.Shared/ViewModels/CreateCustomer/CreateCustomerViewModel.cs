﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Abp.Application.Services.Dto;
using Castle.Core.Resource;
using solarcrm.Core.Threading;
using solarcrm.Helpers;
using solarcrm.MobileService.MCommon;
using solarcrm.MobileService.MCommon.Dto;
using solarcrm.MobileService.MCustomer;
using solarcrm.MobileService.MCustomer.Dto;
using solarcrm.Models;
using solarcrm.ViewModels.Base;
using solarcrm.ViewModels.Customer;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace solarcrm.ViewModels.CreateCustomer
{
    public class CreateCustomerViewModel : XamarinViewModel
    {
        #region Constructor

        public CreateCustomerViewModel(IMobileCommonAppService mobileCommonAppService, ICustomerAppService customerAppService)
        {
            _mobileCommonAppService = mobileCommonAppService;
            _customerAppService = customerAppService;

            Device.BeginInvokeOnMainThread(async () =>
            {
                await GetAllDrowpDownData();
            });

            TabItems = new ObservableCollection<TabModel>();
            TabItems.Add(new TabModel { Id = 1, Icon = "tab1_selected.png", Title = AppStrings.CustomerText, IsSelected = true, TextColor = (Color)Application.Current.Resources["PacificBlueColor"] });
            TabItems.Add(new TabModel { Id = 2, Icon = "tab2.png", Title = AppStrings.AddressText });
            TabItems.Add(new TabModel { Id = 3, Icon = "tab3.png", Title = AppStrings.ApplicationText });
            TabItems.Add(new TabModel { Id = 4, Icon = "tab4.png", Title = AppStrings.LeadText });
            TabItems.Add(new TabModel { Id = 5, Icon = "tab5.png", Title = AppStrings.BankText });

            CheckConsumerNoIsDuplicateEvent += CheckDuplicateConsumerNoEventHandler;
        }

        #endregion

        #region Private Properties

        private readonly IMobileCommonAppService _mobileCommonAppService;
        private readonly ICustomerAppService _customerAppService;

        private bool _isShowTabs = true;
        private bool _isCustomerTabVisible = true;
        private bool _isAddressTabVisible;
        private bool _isAppTabVisible;
        private bool _isLeadTabVisible;
        private bool _isBankTabVisible;
        private bool _isSuccessTabVisible;
        private bool _isChannelPartnersSearchVisible;
        private bool _isCPListVisible;
        private bool _isSubDivListVisible;
        private bool _isCityListVisible;

        private int _customerNo;
        private int _avgMonthlyUnit;
        private int _avgMonthlyBill;
        private int _requiredCapacity;

        private string _addressLine1;
        private string _addressLine2;
        private string _selectedSolarType;
        
        private string _firstName;
        private string _middleName;
        private string _lastName;
        private string _customerName;
        private string _mobile;
        private string _email;
        private string _altContactNo;
        private string _taluka;
        private string _district;
        private string _state;
        private string _selectedSubDivision;
        private string _division;
        private string _circle;
        private string _discom;
        private string _latitude;
        private string _longitude;

        private string _structureAmount;
        private string _category;
        private string _heightStructure;
        private string _leadSource;
        private string _leadStatus;
        private string _leadType;
        private string _commonMeterConnection;
        private string _areaType;
        private string _selectedChannelsPartner;
        private string _searchCity;
        private string _selectedCity;
        private string _searchSubDivision;
        private string _searchChannelPartner;

        private string _solarTypeErrorMsg;

        private string _customerNameErrorMsg;
        private string _firstNameErrorMsg;
        private string _middleNameErrorMsg;
        private string _lastNameErrorMsg;
        private string _mobileErrorMsg;
        private string _emailErrorMsg;
        private string _additionalNotes;
        private GetCustomerForEditOutput _EditCustomer;

        private ObservableCollection<TabModel> _tabItems;
        private ObservableCollection<string> _solarTypeList;
        private ObservableCollection<string> _citiesList;

        private ObservableCollection<string> _subDivisionList;
        private ObservableCollection<string> _divisionList;
        private ObservableCollection<string> _circleList;
        private ObservableCollection<string> _discomList;
        private ObservableCollection<string> _categoryList;
        private ObservableCollection<string> _heightStructureList;
        private ObservableCollection<string> _leadSourceList;
        private ObservableCollection<string> _leadStatusList;
        private ObservableCollection<string> _leadTypesList;
        private ObservableCollection<string> _commonMeterConnectionList;
        private ObservableCollection<string> _areaTypeList;
        private ObservableCollection<string> _channelsPartnersList;

        #endregion

        #region Public Properties

        public static EventHandler<int> CheckConsumerNoIsDuplicateEvent;
        public static EventHandler<int> GetCustomerForEditEvent;

        public bool IsDuplicateConsumerNuber;

        public bool IsShowTabs
        {
            get { return _isShowTabs; }
            set { SetProperty(ref _isShowTabs, value); }
        }

        public bool IsCustomerTabVisible
        {
            get { return _isCustomerTabVisible; }
            set { SetProperty(ref _isCustomerTabVisible, value); }
        }

        public bool IsAddressTabVisible
        {
            get { return _isAddressTabVisible; }
            set { SetProperty(ref _isAddressTabVisible, value); }
        }

        public bool IsAppTabVisible
        {
            get { return _isAppTabVisible; }
            set { SetProperty(ref _isAppTabVisible, value); }
        }

        public bool IsLeadTabVisible
        {
            get { return _isLeadTabVisible; }
            set { SetProperty(ref _isLeadTabVisible, value); }
        }

        public bool IsBankTabVisible
        {
            get { return _isBankTabVisible; }
            set { SetProperty(ref _isBankTabVisible, value); }
        }

        public bool IsSuccessTabVisible
        {
            get { return _isSuccessTabVisible; }
            set { SetProperty(ref _isSuccessTabVisible, value); }
        }

        public bool IsChannelPartnersSearchVisible
        {
            get { return _isChannelPartnersSearchVisible; }
            set { SetProperty(ref _isChannelPartnersSearchVisible, value); }
        }

        public bool IsCityListVisible
        {
            get { return _isCityListVisible; }
            set { SetProperty(ref _isCityListVisible, value); }
        }

        public bool IsCPListVisible
        {
            get { return _isCPListVisible; }
            set { SetProperty(ref _isCPListVisible, value); }
        }

        public bool IsSubDivListVisible
        {
            get { return _isSubDivListVisible; }
            set { SetProperty(ref _isSubDivListVisible, value); }
        }

        public int CustomerNo
        {
            get { return _customerNo; }
            set { SetProperty(ref _customerNo, value); }
        }

        public string AddressLine1
        {
            get { return _addressLine1; }
            set { SetProperty(ref _addressLine1, value); }
        }

        public string AddressLine2
        {
            get { return _addressLine2; }
            set { SetProperty(ref _addressLine2, value); }
        }

        public string SelectedSolarType
        {
            get { return _selectedSolarType; }
            set { SetProperty(ref _selectedSolarType, value); }
        }
        
        public string FirstName
        {
            get { return _firstName; }
            set { SetProperty(ref _firstName, value); }
        }

        public string MiddleName
        {
            get { return _middleName; }
            set { SetProperty(ref _middleName, value); }
        }

        public string LastName
        {
            get { return _lastName; }
            set { SetProperty(ref _lastName, value); }
        }

        public string CustomerName
        {
            get { return _customerName; }
            set { SetProperty(ref _customerName, value); }
        }

        public string Mobile
        {
            get { return _mobile; }
            set { SetProperty(ref _mobile, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        public string AltContactNo
        {
            get { return _altContactNo; }
            set { SetProperty(ref _altContactNo, value); }
        }

        public string SearchCity
        {
            get { return _searchCity; }
            set { SetProperty(ref _searchCity, value); }
        }       

        public string SelectedCity
        {
            get { return _selectedCity; }
            set
            {
                SetProperty(ref _selectedCity, value);
                if (!string.IsNullOrEmpty(value))
                {
                    SearchCity = value;
                    IsCityListVisible = false;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await GetTalukaDistState();
                    });
                }
            }
        }

        public string Taluka
        {
            get { return _taluka; }
            set { SetProperty(ref _taluka, value); }
        }

        public string District
        {
            get { return _district; }
            set { SetProperty(ref _district, value); }
        }

        public string State
        {
            get { return _state; }
            set { SetProperty(ref _state, value); }
        }

        public string SelectedSubDivision
        {
            get { return _selectedSubDivision; }
            set
            {
                SetProperty(ref _selectedSubDivision, value);

                if (!string.IsNullOrEmpty(value))
                {
                    SearchSubdivision = value;
                    IsSubDivListVisible = false;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await GetSubDivisionsResult();
                    });
                }
            }
        }

        public string Division
        {
            get { return _division; }
            set { SetProperty(ref _division, value); }
        }

        public string Circle
        {
            get { return _circle; }
            set { SetProperty(ref _circle, value); }
        }

        public string Discom
        {
            get { return _discom; }
            set { SetProperty(ref _discom, value); }
        }

        public string Latitude
        {
            get { return _latitude; }
            set { SetProperty(ref _latitude, value); }
        }

        public string Longitude
        {
            get { return _longitude; }
            set { SetProperty(ref _longitude, value); }
        }

        public int RequiredCapacity
        {
            get { return _requiredCapacity; }
            set { SetProperty(ref _requiredCapacity, value); }
        }

        public string StructureAmount
        {
            get { return _structureAmount; }
            set { SetProperty(ref _structureAmount, value); }
        }

        public int AvgMonthlyUnit
        {
            get { return _avgMonthlyUnit; }
            set { SetProperty(ref _avgMonthlyUnit, value); }
        }

        public int AvgMonthlyBill
        {
            get { return _avgMonthlyBill; }
            set { SetProperty(ref _avgMonthlyBill, value); }
        }

        public string Category
        {
            get { return _category; }
            set { SetProperty(ref _category, value); }
        }

        public string HeightStructure
        {
            get { return _heightStructure; }
            set { SetProperty(ref _heightStructure, value); }
        }

        public string LeadSource
        {
            get { return _leadSource; }
            set { SetProperty(ref _leadSource, value); }
        }

        public string LeadStatus
        {
            get { return _leadStatus; }
            set { SetProperty(ref _leadStatus, value); }
        }

        public string LeadType
        {
            get { return _leadType; }
            set { SetProperty(ref _leadType, value); }
        }

        public string CommonMeterConnection
        {
            get { return _commonMeterConnection; }
            set { SetProperty(ref _commonMeterConnection, value); }
        }

        public string AreaType
        {
            get { return _areaType; }
            set { SetProperty(ref _areaType, value); }
        }

        public string SelectedChannelsPartner
        {
            get { return _selectedChannelsPartner; }
            set
            {
                SetProperty(ref _selectedChannelsPartner, value);

                if (!string.IsNullOrEmpty(value))
                {
                    SearchChannelPartner = value;
                    IsCPListVisible = false;
                }
            }
        }

        public string SolarTypeErrorMsg
        {
            get { return _solarTypeErrorMsg; }
            set { SetProperty(ref _solarTypeErrorMsg, value); }
        }

        public string CustomerNameErrorMsg
        {
            get { return _customerNameErrorMsg; }
            set { SetProperty(ref _customerNameErrorMsg, value); }
        }

        public string FirstNameErrorMsg
        {
            get { return _firstNameErrorMsg; }
            set { SetProperty(ref _firstNameErrorMsg, value); }
        }

        public string MiddleNameErrorMsg
        {
            get { return _middleNameErrorMsg; }
            set { SetProperty(ref _middleNameErrorMsg, value); }
        }

        public string LastNameErrorMsg
        {
            get { return _lastNameErrorMsg; }
            set { SetProperty(ref _lastNameErrorMsg, value); }
        }

        public string MobileErrorMsg
        {
            get { return _mobileErrorMsg; }
            set { SetProperty(ref _mobileErrorMsg, value); }
        }

        public string EmailErrorMsg
        {
            get { return _emailErrorMsg; }
            set { SetProperty(ref _emailErrorMsg, value); }
        }

        public string SearchChannelPartner
        {
            get { return _searchChannelPartner; }
            set { SetProperty(ref _searchChannelPartner, value); }
        }

        public string SearchSubdivision
        {
            get { return _searchSubDivision; }
            set { SetProperty(ref _searchSubDivision, value); }
        }

        public string AdditionalNotes
        {
            get { return _additionalNotes; }
            set { SetProperty(ref _additionalNotes, value); }
        }

        public ObservableCollection<TabModel> TabItems
        {
            get { return _tabItems; }
            set { SetProperty(ref _tabItems, value); }
        }

        public ObservableCollection<string> SolarTypeList
        {
            get { return _solarTypeList; }
            set { SetProperty(ref _solarTypeList, value); }
        }

        public ObservableCollection<string> CitiesList
        {
            get { return _citiesList; }
            set { SetProperty(ref _citiesList, value); }
        }

        public ObservableCollection<string> SubDivisionList
        {
            get { return _subDivisionList; }
            set { SetProperty(ref _subDivisionList, value); }
        }

        public ObservableCollection<string> DivisionList
        {
            get { return _divisionList; }
            set { SetProperty(ref _divisionList, value); }
        }

        public ObservableCollection<string> CircleList
        {
            get { return _circleList; }
            set { SetProperty(ref _circleList, value); }
        }

        public ObservableCollection<string> DiscomList
        {
            get { return _discomList; }
            set { SetProperty(ref _discomList, value); }
        }

        public ObservableCollection<string> CategoryList
        {
            get { return _categoryList; }
            set { SetProperty(ref _categoryList, value); }
        }

        public ObservableCollection<string> HeightStructureList
        {
            get { return _heightStructureList; }
            set { SetProperty(ref _heightStructureList, value); }
        }

        public ObservableCollection<string> LeadSourceList
        {
            get { return _leadSourceList; }
            set { SetProperty(ref _leadSourceList, value); }
        }

        public ObservableCollection<string> LeadStatusList
        {
            get { return _leadStatusList; }
            set { SetProperty(ref _leadStatusList, value); }
        }

        public ObservableCollection<string> LeadTypesList
        {
            get { return _leadTypesList; }
            set { SetProperty(ref _leadTypesList, value); }
        }

        public ObservableCollection<string> CommonMeterConnectionList
        {
            get { return _commonMeterConnectionList; }
            set { SetProperty(ref _commonMeterConnectionList, value); }
        }

        public ObservableCollection<string> AreaTypeList
        {
            get { return _areaTypeList; }
            set { SetProperty(ref _areaTypeList, value); }
        }

        public ObservableCollection<string> ChannelsPartnersList
        {
            get { return _channelsPartnersList; }
            set { SetProperty(ref _channelsPartnersList, value); }
        }
        public GetCustomerForEditOutput EditCustomer
        {
            get { return _EditCustomer; }
            set { SetProperty(ref _EditCustomer, value); }
        }

        #endregion

        #region Commands

        public ICommand SaveBasicInfoCommand { get { return new Command(BasicInfoSaveClick); } }
        public ICommand AddressTabBackCommand { get { return new Command(AddressViewBackClick); } }
        public ICommand SaveAddressCommand { get { return new Command(SaveAddressClick); } }
        public ICommand AppTabBackCommand { get { return new Command(ApplicationTabBackClick); } }
        public ICommand SaveApplicationCommand { get { return new Command(SaveApplicationClick); } }
        public ICommand LeadTabBackCommand { get { return new Command(LeadTabBackClick); } }
        public ICommand SaveLeadCommand { get { return new Command(SaveLeadClick); } }
        public ICommand BankTabBackCommand { get { return new Command(BankTabBackClick); } }
        public ICommand SavePersonCommand => AsyncCommand.Create(SubmitClick);
        public ICommand EditPersonCommand => AsyncCommand.Create(SubmitClick);
        public ICommand SearchCityCommand { get { return new Command(SearchCityMethod); } }
        public ICommand SearchSubdivisionCommand { get { return new Command(SearchSubDivision); } }
        public ICommand SearchChannelPartnerCommand { get { return new Command(SearchChannelPartnerClick); } }
        public ICommand GetLatLongCommnad { get { return new Command(GetLatLongValue); } }

        #endregion

        #region Private Methods

        private void BasicInfoSaveClick()
        {
            try
            {
                if (IsDuplicateConsumerNuber)
                {
                    App.Current.MainPage.DisplayAlert(AppStrings.ErrorText, "Consumer number already exist.", AppStrings.OkText);
                    return;
                }

                SolarTypeErrorMsg = string.IsNullOrEmpty(SelectedSolarType) ? AppStrings.SolarTypeErrorMsg : string.Empty;
                FirstNameErrorMsg = string.IsNullOrEmpty(FirstName) ? AppStrings.FirstNameRequiredText : string.Empty;
                MiddleNameErrorMsg = string.IsNullOrEmpty(MiddleName) ? AppStrings.MiddleNameRequiredText : string.Empty;
                LastNameErrorMsg = string.IsNullOrEmpty(LastName) ? AppStrings.LastNameRequiredText : string.Empty;
                CustomerNameErrorMsg = string.IsNullOrEmpty(CustomerName) ? AppStrings.CustomerNameRequiredMsg : string.Empty;

                if (!string.IsNullOrEmpty(Mobile))
                {
                    MobileErrorMsg = ValidationHelper.IsValidPhoneNumber(Mobile) ? string.Empty : AppStrings.MobileNoErrorMsg;
                }
                else
                {
                    MobileErrorMsg = AppStrings.MobileNoRequiredText;
                }

                if (!string.IsNullOrEmpty(Email))
                {
                    EmailErrorMsg = ValidationHelper.IsValidEmail(Email) ? string.Empty : AppStrings.EmailErrorMsg;
                }
                else
                {
                    EmailErrorMsg = string.Empty;
                }

                if (string.IsNullOrEmpty(FirstNameErrorMsg) && string.IsNullOrEmpty(MiddleNameErrorMsg) && string.IsNullOrEmpty(LastNameErrorMsg)
                       && string.IsNullOrEmpty(EmailErrorMsg) && string.IsNullOrEmpty(MobileErrorMsg)
                       && string.IsNullOrEmpty(CustomerNameErrorMsg) && string.IsNullOrEmpty(SolarTypeErrorMsg))
                {
                    TabItems.Where(s => s.Id == 2).ToList().ForEach(s =>
                    {
                        s.IsSelected = true;
                        s.TextColor = (Color)Application.Current.Resources["PacificBlueColor"];
                        s.Icon = $"tab{s.Id}_selected.png";
                    });

                    IsCustomerTabVisible = false;
                    IsAddressTabVisible = true;
                    IsAppTabVisible = false;
                    IsLeadTabVisible = false;
                    IsBankTabVisible = false;
                    IsSuccessTabVisible = false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void AddressViewBackClick()
        {
            IsCustomerTabVisible = true;
            IsAddressTabVisible = false;
            IsAppTabVisible = false;
            IsLeadTabVisible = false;
            IsBankTabVisible = false;
            IsSuccessTabVisible = false;
        }

        private void SaveAddressClick()
        {
            try
            {
                TabItems.Where(s => s.Id == 3).ToList().ForEach(s =>
                {
                    s.IsSelected = true;
                    s.TextColor = (Color)Application.Current.Resources["PacificBlueColor"];
                    s.Icon = $"tab{s.Id}_selected.png";
                });

                IsCustomerTabVisible = false;
                IsAddressTabVisible = false;
                IsAppTabVisible = true;
                IsLeadTabVisible = false;
                IsBankTabVisible = false;
                IsSuccessTabVisible = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void ApplicationTabBackClick()
        {
            IsCustomerTabVisible = false;
            IsAddressTabVisible = true;
            IsAppTabVisible = false;
            IsLeadTabVisible = false;
            IsBankTabVisible = false;
            IsSuccessTabVisible = false;
        }

        private async void GetLatLongValue()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium, TimeSpan.FromSeconds(10));
                var cts = new CancellationTokenSource();
                var location = await Geolocation.GetLocationAsync(request, cts.Token);

                if (location != null)
                {
                    Latitude = location.Latitude.ToString();
                    Longitude = location.Longitude.ToString();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async void SearchCityMethod()
        {
            try
            {
                StringFilter stringFilter = new StringFilter { FlterText = SearchCity };

                if (SearchCity.Length > 3)
                {
                    await SetBusyAsync(async () =>
                    {
                        var cities = await _mobileCommonAppService.SearchCity(stringFilter);
                        if (cities != null && cities.Count > 0)
                        {
                            var data = new ObservableCollection<string>();

                            foreach (var item in cities)
                            {
                                data.Add(item.Name);
                            }

                            IsCityListVisible = true;
                            CitiesList = new ObservableCollection<string>(data);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async Task GetTalukaDistState()
        {
            try
            {
                StringFilter stringFilter = new StringFilter { FlterText = SelectedCity };

                await SetBusyAsync(async () =>
                {
                    var cityResult = await _mobileCommonAppService.CityResult(stringFilter);
                    if (cityResult != null)
                    {
                        Taluka = cityResult.Taluka;
                        District = cityResult.District;
                        State = cityResult.State;
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void SaveApplicationClick()
        {
            try
            {
                TabItems.Where(s => s.Id == 4).ToList().ForEach(s =>
                {
                    s.IsSelected = true;
                    s.TextColor = (Color)Application.Current.Resources["PacificBlueColor"];
                    s.Icon = $"tab{s.Id}_selected.png";
                });

                IsCustomerTabVisible = false;
                IsAddressTabVisible = false;
                IsAppTabVisible = false;
                IsLeadTabVisible = true;
                IsBankTabVisible = false;
                IsSuccessTabVisible = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void LeadTabBackClick()
        {
            IsCustomerTabVisible = false;
            IsAddressTabVisible = false;
            IsAppTabVisible = true;
            IsLeadTabVisible = false;
            IsBankTabVisible = false;
            IsSuccessTabVisible = false;
        }

        private void SaveLeadClick()
        {
            try
            {
                TabItems.Where(s => s.Id == 5).ToList().ForEach(s =>
                {
                    s.IsSelected = true;
                    s.TextColor = (Color)Application.Current.Resources["PacificBlueColor"];
                    s.Icon = $"tab{s.Id}_selected.png";
                });

                IsCustomerTabVisible = false;
                IsAddressTabVisible = false;
                IsAppTabVisible = false;
                IsLeadTabVisible = false;
                IsBankTabVisible = true;
                IsSuccessTabVisible = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void BankTabBackClick()
        {
            IsCustomerTabVisible = false;
            IsAddressTabVisible = false;
            IsAppTabVisible = false;
            IsLeadTabVisible = true;
            IsBankTabVisible = false;
            IsSuccessTabVisible = false;
        }

        private async void SearchSubDivision()
        {
            try
            {
                StringFilter stringFilter = new StringFilter { FlterText = SearchSubdivision };

                if (SearchSubdivision.Length > 3)
                {
                    await SetBusyAsync(async () =>
                    {
                        var subDivisions = await _mobileCommonAppService.SearchSubDivision(stringFilter);
                        if (subDivisions != null && subDivisions.Count > 0)
                        {
                            var data = new ObservableCollection<string>();

                            foreach (var item in subDivisions)
                            {
                                data.Add(item.Name);
                            }

                            IsSubDivListVisible = true;
                            SubDivisionList = new ObservableCollection<string>(data);
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                IsSubDivListVisible = false;
                Debug.WriteLine(ex.Message);
            }
        }

        private async Task GetSubDivisionsResult()
        {
            try
            {
                StringFilter stringFilter = new StringFilter { FlterText = SearchSubdivision };

                await SetBusyAsync(async () =>
                {
                    var subDivisionResult = await _mobileCommonAppService.SubDivisionResult(stringFilter);
                    if (subDivisionResult != null)
                    {
                        Division = subDivisionResult.Division;
                        Circle = subDivisionResult.Circle;
                        Discom = subDivisionResult.Discom;
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private async void SearchChannelPartnerClick()
        {
            try
            {
                StringFilter stringFilter = new StringFilter { FlterText = SearchChannelPartner };

                await SetBusyAsync(async () =>
                {
                    var channelsPartner = await _mobileCommonAppService.SearchChannelPartner(stringFilter);
                    if (channelsPartner != null && channelsPartner.Count > 0)
                    {
                        var data = new ObservableCollection<string>();

                        foreach (var item in channelsPartner)
                        {
                            data.Add(item.Name);
                        }

                        IsCPListVisible = true;
                        ChannelsPartnersList = new ObservableCollection<string>(data);
                    }
                });

            }
            catch (Exception ex)
            {
                IsSubDivListVisible = false;
                Debug.WriteLine(ex.Message);
            }
        }

        #endregion

        #region Public Methods

        public async Task GetAllDrowpDownData()
        {
            await SetBusyAsync(async () =>
            {
                await FetchDataAsync();
            });
        }

        public async Task FetchDataAsync()
        {
            try
            {
                CommonMeterConnectionList = new ObservableCollection<string>() { "Yes", "No" };
                AreaTypeList = new ObservableCollection<string>() { "Urban", "Ruler" };

                var solarTypes = await _mobileCommonAppService.GetAllSolarTypeForDropdown();
                if (solarTypes != null && solarTypes.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in solarTypes)
                    {
                        data.Add(item.Name);
                    }

                    SolarTypeList = new ObservableCollection<string>(data);
                }

                var divisions = await _mobileCommonAppService.GetAllDivisionForDropdown();
                if (divisions != null && divisions.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in divisions)
                    {
                        data.Add(item.Name);
                    }

                    DivisionList = new ObservableCollection<string>(data);
                }

                var circles = await _mobileCommonAppService.GetAllCircleForDropdown();
                if (circles != null && circles.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in circles)
                    {
                        data.Add(item.Name);
                    }

                    CircleList = new ObservableCollection<string>(data);
                }

                var discomes = await _mobileCommonAppService.GetAllDiscomForDropdown();
                if (discomes != null && discomes.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in discomes)
                    {
                        data.Add(item.Name);
                    }

                    DiscomList = new ObservableCollection<string>(data);
                }

                var categories = await _mobileCommonAppService.GetAllCategoryForDropdown();
                if (categories != null && categories.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in categories)
                    {
                        data.Add(item.Name);
                    }

                    CategoryList = new ObservableCollection<string>(data);
                }

                var heightStructure = await _mobileCommonAppService.GetAllHeightOfStructureForDropdown();
                if (heightStructure != null && heightStructure.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in heightStructure)
                    {
                        data.Add(item.Name);
                    }

                    HeightStructureList = new ObservableCollection<string>(data);
                }
               
                var leadSource = await _mobileCommonAppService.GetAllLeadSourceForDropdown();
                if (leadSource != null && leadSource.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in leadSource)
                    {
                        data.Add(item.Name);
                    }

                    LeadSourceList = new ObservableCollection<string>(data);
                    LeadSource = LeadSourceList.FirstOrDefault().ToString();
                }

                var leadStatus = await _mobileCommonAppService.GetAllLeadStatusForDropdown();
                if (leadStatus != null && leadStatus.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in leadStatus)
                    {
                        data.Add(item.Name);
                    }

                    LeadStatusList = new ObservableCollection<string>(data);
                    LeadStatus = LeadStatusList.FirstOrDefault().ToString();
                }

                var leadTypes = await _mobileCommonAppService.GetAllLeadTypeForDropdown();
                if (leadTypes != null && leadTypes.Count > 0)
                {
                    var data = new ObservableCollection<string>();

                    foreach (var item in leadTypes)
                    {
                        data.Add(item.Name);
                    }

                    LeadTypesList = new ObservableCollection<string>(data);
                    LeadType = LeadTypesList.FirstOrDefault().ToString();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public async Task SubmitClick()
        {
            try
            {
                CreateOrEditCustomerDto createOrEditCustomer = new CreateOrEditCustomerDto();
                createOrEditCustomer.FirstName = FirstName;
                createOrEditCustomer.MiddleName = MiddleName;
                createOrEditCustomer.LastName = LastName;
                createOrEditCustomer.CustomerName = CustomerName;
                createOrEditCustomer.MobileNumber = Mobile;
                createOrEditCustomer.EmailId = Email;
                createOrEditCustomer.Alt_Phone = AltContactNo;
                createOrEditCustomer.AddressLine1 = AddressLine1;
                createOrEditCustomer.AddressLine2 = AddressLine2;
                createOrEditCustomer.City = SelectedCity;
                createOrEditCustomer.State = State;
                createOrEditCustomer.Taluka = Taluka;
                createOrEditCustomer.District = District;
                createOrEditCustomer.Pincode = 0;
                createOrEditCustomer.LeadSource = LeadSource;
                createOrEditCustomer.LeadStatus = LeadStatus;
                createOrEditCustomer.LeadType = LeadType;
                createOrEditCustomer.SolarType = SelectedSolarType;
                createOrEditCustomer.CommonMeter = CommonMeterConnection == "Yes" ? true : false;
                createOrEditCustomer.ConsumerNumber = CustomerNo;
                createOrEditCustomer.AvgmonthlyBill = AvgMonthlyBill;
                createOrEditCustomer.AvgmonthlyUnit = AvgMonthlyUnit;
                createOrEditCustomer.Discom = Discom;
                createOrEditCustomer.Division = Division;
                createOrEditCustomer.SubDivision = SelectedSubDivision;
                createOrEditCustomer.Circle = Circle;
                createOrEditCustomer.RequiredCapacity = RequiredCapacity;
                createOrEditCustomer.HeightStructure = HeightStructure;
                createOrEditCustomer.Category = Category;
                createOrEditCustomer.ChanelPartner = SelectedChannelsPartner;
                createOrEditCustomer.Latitude = Convert.ToDecimal(Latitude);
                createOrEditCustomer.Longitude = Convert.ToDecimal(Longitude);
                createOrEditCustomer.Notes = AdditionalNotes;
                createOrEditCustomer.AreaType = AreaType;

                await SetBusyAsync(async () =>
                {
                    var result = await _customerAppService.CreateOrEdit(createOrEditCustomer);
                    if (result.IsSuccess && result.StatusCode==200)
                    {
                        IsCustomerTabVisible = false;
                        IsAddressTabVisible = false;
                        IsAppTabVisible = false;
                        IsLeadTabVisible = false;
                        IsBankTabVisible = false;
                        IsSuccessTabVisible = true;
                        IsShowTabs = false;

                        await Task.Delay(3000);

                        CustomerViewModel.AddCustomerEventHandler.Invoke(this, true);
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert(AppStrings.ErrorText, result.Message, AppStrings.OkText);
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
         
        public async void CheckDuplicateConsumerNoEventHandler(object sender, int consumerNo)
        {
            try
            {
                DuplicateConsumerFilter stringFilter = new DuplicateConsumerFilter { consumerNumber = consumerNo , solartype = "residential" };
                await SetBusyAsync(async () =>
                {
                    var value = await _mobileCommonAppService.CheckDuplicateConsumerNoMobile(stringFilter);
                    if (value != null && value.IsSuccess)
                    {
                        IsDuplicateConsumerNuber = true;
                        await App.Current.MainPage.DisplayAlert("Alert", value.Message, "OK");
                    }
                    else
                    {
                        IsDuplicateConsumerNuber = false;
                    }
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
       
      

        public override Task InitializeAsync(object navigationData)
        {
            GetCustomerForEditOutput testab = new GetCustomerForEditOutput();
            var yyyy = testab.Customer;
            var output = new GetCustomerForEditOutput();

            var customer = ObjectMapper.Map<CreateOrEditCustomerDto>(navigationData);//navigationData as CustomerDto;  //GetCustomerForEditOutput
            if (customer != null)
            {                
                CustomerNo = Convert.ToInt32(customer.Id);                
                CustomerName = customer.CustomerName;
                SelectedSolarType = customer.SolarType;
                FirstName = customer.FirstName;
                MiddleName = customer.MiddleName;
                LastName = customer.LastName;
                Email = customer.EmailId;
                Mobile = customer.MobileNumber;
                AltContactNo = customer.Alt_Phone;
                AddressLine1 = customer.AddressLine1;
                AddressLine2 =  customer.AddressLine2;
                SelectedCity = customer.City;
                Taluka = customer.Taluka;               
                District = customer.District;
                State = customer.State;
                //postcode
            }
           
           //AddressLine1 = $"{customer.Address1} {customer.Address2} {customer.Address3}";
           // Mobile = customer.Mobile;


            //if (_isInitialized)
            //{
                
                
            //}

            return Task.CompletedTask;
            //_isInitialized = true;
           
        }
       
        #endregion
    }
}

