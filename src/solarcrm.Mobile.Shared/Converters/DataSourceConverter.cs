﻿using System;
using System.Collections;
using System.Globalization;
using AutoMapper;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace solarcrm.Converters
{
	public class DataSourceConverter : IValueConverter, IMarkupExtension
	{
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return true;
            }

            return ((IList)value).Count == 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}

