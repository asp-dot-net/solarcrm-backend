﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace solarcrm
{
    [DependsOn(typeof(solarcrmClientModule), typeof(AbpAutoMapperModule))]
    public class solarcrmXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(solarcrmXamarinSharedModule).GetAssembly());
        }
    }
}