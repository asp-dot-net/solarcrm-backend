﻿using System;
using System.Collections;
using Xamarin.Forms;

namespace solarcrm.Controls
{
	public class BindablePicker : Picker
	{
		public BindablePicker()
		{
            if (this.IsAutoSelect)
            {
                this.SelectedIndexChanged += OnSelectedIndexChanged;
            }

            if (!this.IsAutoSelect)
            {
                this.Unfocused += OnUnfocused;
            }
        }

        private void OnUnfocused(object sender, FocusEventArgs e)
        {
            if (SelectedIndex < 0 || SelectedIndex > Items.Count - 1)
            {
                SelectedItem = null;
            }
            else
            {
                SelectedItem = ItemsSource[SelectedIndex];
            }
        }

        public static BindableProperty ItemChangedProperty =
            BindableProperty.Create(nameof(ItemChanged), typeof(object), typeof(BindablePicker), default(object), propertyChanged: OnItemChanged);

        private static void OnItemChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var picker = bindable as BindablePicker;
            if (newvalue != null)
            {
                picker.SelectedIndex = (picker.ItemsSource != null) ? (newvalue is int || newvalue is float || newvalue is decimal || newvalue is double) ? picker.ItemsSource.IndexOf(newvalue?.ToString()) : picker.ItemsSource.IndexOf(newvalue) : -1;
            }
        }

        public IEnumerable ItemChanged
        {
            get { return (string)GetValue(ItemChangedProperty); }
            set { SetValue(ItemChangedProperty, value); }
        }

        private void OnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            var picker = sender as BindablePicker;

            if (picker.IsFocused)
            {
                picker.Unfocus();
            }

            if (SelectedIndex < 0 || SelectedIndex > Items.Count - 1)
            {
                //SelectedItem = null; // 11_March_2019_Picker_Change
            }
            else
            {
                SelectedItem = ItemsSource[SelectedIndex];
            }
        }

        public static readonly BindableProperty IsAutoSelectProperty =
            BindableProperty.Create(nameof(IsAutoSelect), typeof(bool), typeof(BindablePicker), true, BindingMode.TwoWay, propertyChanged: OnIsAutoSelectChanged);

        private static void OnIsAutoSelectChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var picker = bindable as BindablePicker;

            if (picker.IsAutoSelect)
            {
                picker.SelectedIndexChanged += picker.OnSelectedIndexChanged;
                picker.Unfocused -= picker.OnUnfocused;
            }

            if (!picker.IsAutoSelect)
            {
                picker.Unfocused += picker.OnUnfocused;
                picker.SelectedIndexChanged -= picker.OnSelectedIndexChanged;
            }
        }

        public bool IsAutoSelect
        {
            get
            {
                return (bool)GetValue(IsAutoSelectProperty);
            }
            set
            {
                SetValue(IsAutoSelectProperty, value);
            }
        }

        public static readonly BindableProperty IsCustomFocusedProperty =
            BindableProperty.Create("IsCustomFocused", typeof(bool), typeof(BindablePicker), false);

        public bool IsCustomFocused
        {
            get
            {
                return (bool)GetValue(IsCustomFocusedProperty);
            }
            set
            {
                SetValue(IsCustomFocusedProperty, value);
            }
        }

        public static readonly BindableProperty BorderWidthProperty =
            BindableProperty.Create("BorderWidth", typeof(double), typeof(BindablePicker), double.MinValue);

        public double BorderWidth
        {
            get
            {
                return (double)GetValue(BorderWidthProperty);
            }
            set
            {
                SetValue(BorderWidthProperty, value);
            }
        }

        public static readonly BindableProperty XAlignProperty =
            BindableProperty.Create("XAlign", typeof(TextAlignment), typeof(BindablePicker), TextAlignment.Start);

        public TextAlignment XAlign
        {
            get
            {
                return (TextAlignment)GetValue(XAlignProperty);
            }
            set
            {
                SetValue(XAlignProperty, value);
            }
        }

        public static readonly BindableProperty YAlignProperty =
            BindableProperty.Create("YAlign", typeof(TextAlignment), typeof(EntryControl), TextAlignment.Center);

        public TextAlignment YAlign
        {
            get
            {
                return (TextAlignment)GetValue(YAlignProperty);
            }
            set
            {
                SetValue(YAlignProperty, value);
            }
        }

        public static new BindableProperty SelectedItemProperty =
            BindableProperty.Create(nameof(SelectedItem), typeof(object), typeof(BindablePicker), default(object), propertyChanged: OnSelectedItemChanged);

        private static void OnSelectedItemChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var picker = bindable as BindablePicker;

            if (newvalue != null)
            {
                picker.SelectedIndex = (picker.ItemsSource != null) ? (newvalue is int || newvalue is float || newvalue is decimal || newvalue is double) ? picker.ItemsSource.IndexOf(newvalue?.ToString()) : picker.ItemsSource.IndexOf(newvalue) : -1;
            }
        }

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

    }
}

