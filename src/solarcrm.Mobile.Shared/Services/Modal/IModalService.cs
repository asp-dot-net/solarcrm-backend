﻿using System.Threading.Tasks;
using solarcrm.Views;
using Xamarin.Forms;

namespace solarcrm.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}
