﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Abp.Dependency;
using MvvmHelpers;
using solarcrm.Helpers;
using solarcrm.Localization;
using solarcrm.Models.NavigationMenu;
using solarcrm.Services.Permission;
using solarcrm.Views;
using solarcrm.Views.Customer;
using solarcrm.Views.Project;

namespace solarcrm.Services.Navigation
{
    public class MenuProvider : ISingletonDependency, IMenuProvider
    {
        /* For more icons:
            https://material.io/icons/
        */
        private static IEnumerable<NavigationMenuItem> MenuItems => new Collection<NavigationMenuItem>
        {
            /* Date: 16-Jun-2022
             Change by Divyesh Bhatt
             */

            new NavigationMenuItem
            {
                Title = AppStrings.DashboardText,
                Icon = "dashboard.png",
                //ViewType = typeof(CustomerDetailsView),
                ViewType = typeof(DashboardView),
                NoSubMenuItem = true,
                //RequiredPermissionName = PermissionKey.Tenants,
            },
            new NavigationMenuItem
            {
                Title = AppStrings.CustomerText,
                Icon = "bill.png",
                ViewType = typeof(CustomerView),
                NoSubMenuItem = true,
                //RequiredPermissionName = PermissionKey.Users,
            },
            new NavigationMenuItem
            {
                Title = AppStrings.ProjectText,
                Icon = "project_setup.png",
                ViewType = typeof(ProjectView), //TenantsView
                NoSubMenuItem = true,
                //RequiredPermissionName = PermissionKey.Tenants,
            },
            new NavigationMenuItem
            {
                Title = AppStrings.CommisionClaimText,
                Icon = "paid_bill.png",
                ViewType = typeof(TenantsView),
                NoSubMenuItem = true,
                //RequiredPermissionName = PermissionKey.Tenants,
            },
            new NavigationMenuItem
            {
                Title = AppStrings.TrackerText,
                Icon = "tracker.png",
                ViewType = typeof(TenantsView),
                NoSubMenuItem = false,

                SubMenuItems = new ObservableCollection<SubMenuItemsModel>()
                {
                    new SubMenuItemsModel()
                    {
                        ItemText="Application",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Payment issued",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Eastimate paid",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Dispatch",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Pickplist",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Install booking",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Job photos",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Commision Report",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Meter connect",
                    },
                    new SubMenuItemsModel()
                    {
                        ItemText="Subcidy",
                    },
                },

                //RequiredPermissionName = PermissionKey.Tenants,
            },
            new NavigationMenuItem
            {
                Title = AppStrings.UploadDocumentsText,
                Icon = "upload_docs.png",
                ViewType = typeof(TenantsView),
                NoSubMenuItem = true,
                //RequiredPermissionName = PermissionKey.Tenants,
            },
            new NavigationMenuItem
            {
                Title = AppStrings.SettingsText,
                Icon = "settings.png",
                ViewType = typeof(MySettingsView),
                NoSubMenuItem = true,
                //RequiredPermissionName = PermissionKey.Tenants,
            },
            new NavigationMenuItem
            {
                Title = AppStrings.LogoutText,
                Icon = "logout.png",
                ViewType = typeof(TenantsView),
                NoSubMenuItem = true,
                //RequiredPermissionName = PermissionKey.Users,
            },

            //new NavigationMenuItem
            //{
            //    Title = L.Localize("Tenants"),
            //    Icon = "Tenants.png",
            //    ViewType = typeof(TenantsView),
            //    RequiredPermissionName = PermissionKey.Tenants,
            //},
            //new NavigationMenuItem
            //{
            //    Title = L.Localize("Users"),
            //    Icon = "UserList.png",
            //    ViewType = typeof(UsersView),
            //    RequiredPermissionName = PermissionKey.Users,
            //},
            //new NavigationMenuItem
            //{
            //    Title = L.Localize("MySettings"),
            //    Icon = "Settings.png",
            //    ViewType = typeof(MySettingsView)
            //},


            /*This is a sample menu item to guide how to add a new item.
                        ,new NavigationMenuItem
                        {
                            Title = "Sample View",
                            Icon = "MyIcon.png",
                            TargetType = typeof(_SampleView),
                            Order = 10
                        }
                    */
        };

        public ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions)
        {
            var authorizedMenuItems = new ObservableRangeCollection<NavigationMenuItem>();
            foreach (var menuItem in MenuItems)
            {
                if (menuItem.RequiredPermissionName == null)
                {
                    authorizedMenuItems.Add(menuItem);
                    continue;
                }

                if (grantedPermissions != null &&
                    grantedPermissions.ContainsKey(menuItem.RequiredPermissionName))
                {
                    authorizedMenuItems.Add(menuItem);
                }
            }

            return authorizedMenuItems;
        }
    }
}