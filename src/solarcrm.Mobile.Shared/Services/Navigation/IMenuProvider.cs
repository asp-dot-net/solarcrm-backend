using System.Collections.Generic;
using MvvmHelpers;
using solarcrm.Models.NavigationMenu;

namespace solarcrm.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}