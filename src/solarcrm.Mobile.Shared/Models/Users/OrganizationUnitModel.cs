﻿using Abp.AutoMapper;
using solarcrm.Organizations.Dto;

namespace solarcrm.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}