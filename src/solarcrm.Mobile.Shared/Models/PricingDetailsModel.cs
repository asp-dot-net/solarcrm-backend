﻿using System;
namespace solarcrm.Models
{
	public class PricingDetailsModel
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public double Value { get; set; }
		public bool IsFinalValue { get; set; }
	}
}

