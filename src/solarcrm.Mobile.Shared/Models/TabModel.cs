﻿using System;
using solarcrm.ViewModels.Base;
using Xamarin.Forms;

namespace solarcrm.Models
{
	public class TabModel : XamarinViewModel
	{
        public int Id { get; set; }
        private string _icon;
        private string _title;
        private bool _isSelected;
        private Color _textColor;

        public string Icon
        {
            get { return _icon; }
            set { _icon = value; OnPropertyChanged(nameof(Icon)); }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; OnPropertyChanged(nameof(Title)); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; OnPropertyChanged(nameof(IsSelected)); }
        }

        public Color TextColor
        {
            get { return _textColor; }
            set { _textColor = value; OnPropertyChanged(nameof(TextColor)); }
        }
    }
}

