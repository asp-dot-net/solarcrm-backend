﻿using System;
namespace solarcrm.Models
{
	public class TechnicalDetailsModel
	{
		public string Title { get; set; }
		public string Value { get; set; }
	}
}

