﻿using System;
namespace solarcrm.Models
{
	public class ProjectModel
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public int TagNumber { get; set; }
		public string Address { get; set; }
		public string ModifierName { get; set; }
		public bool ProjectStatus { get; set; }
	}
}

