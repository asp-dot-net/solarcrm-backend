﻿using System;
namespace solarcrm.Models
{
	public class InverterDetailsModel
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Value { get; set; }
	}
}

