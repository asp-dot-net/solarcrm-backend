﻿using System;
namespace solarcrm.Models
{
	public class ActivityModel
	{
		public int Id { get; set; }
		public string Icon { get; set; }
		public string Title { get; set; }
		public string SubTitle { get; set; }
		public string User { get; set; }
		public string LogDateTime { get; set; }
	}
}

