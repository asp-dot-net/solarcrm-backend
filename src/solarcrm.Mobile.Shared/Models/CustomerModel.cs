﻿using System;
namespace solarcrm.Models
{
	public class CustomerModel
	{
		public string CustomerName { get; set; }
		public string Address { get; set; }
		public string ContactNumber { get; set; }
		public string ApplicationStatus { get; set; }
	}
}

