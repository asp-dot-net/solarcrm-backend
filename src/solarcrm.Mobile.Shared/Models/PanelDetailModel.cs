﻿using System;
namespace solarcrm.Models
{
	public class PanelDetailModel
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Value { get; set; }
	}
}

