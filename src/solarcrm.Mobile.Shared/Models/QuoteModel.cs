﻿using System;
namespace solarcrm.Models
{
	public class QuoteModel
	{
		public int Id { get; set; }
		public int QuoteNo { get; set; }
		public int ProjectNo { get; set; }
		public DateTime QuoteDate { get; set; }
	}
}

