﻿using System;
namespace solarcrm.Models
{
	public class ActionsModel
	{
        public int Id { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
    }
}

