﻿using System;
namespace solarcrm.Models
{
	public class DashboardModel
	{
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string BgColor { get; set; }
        public bool IsVisible { get; set; }
    }
}

