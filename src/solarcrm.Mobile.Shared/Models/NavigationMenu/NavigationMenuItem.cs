﻿using System;
using System.Collections.ObjectModel;
using solarcrm.ViewModels.Base;

namespace solarcrm.Models.NavigationMenu
{
    public class NavigationMenuItem : ExtendedBindableObject
    {
        private bool _isSelected;

        public string Title { get; set; }

        public string Icon { get; set; }

        public Type ViewType { get; set; }

        public object NavigationParameter { get; set; }

        public string RequiredPermissionName { get; set; }

        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                RaisePropertyChanged(() => IsSelected);
            }
        }

        public bool NoSubMenuItem { get; set; }
        public ObservableCollection<SubMenuItemsModel> SubMenuItems { get; set; }
    }

    public class SubMenuItemsModel
    {
        public string ItemImage { get; set; }
        public string ItemText { get; set; }
    }
}
