﻿using System;
namespace solarcrm.Models
{
	public class DocumentModel
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Icon { get; set; }
		public int SerialNumber { get; set; }
		public string UploadDate { get; set; }
	}
}

