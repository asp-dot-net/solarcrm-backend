﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

/*Open Sans fonts */
[assembly: ExportFont("OpenSans-Bold.ttf", Alias = "OpenSansBold")]
[assembly: ExportFont("OpenSans-SemiBold.ttf", Alias = "OpenSansSemiBold")]
[assembly: ExportFont("OpenSans-Medium.ttf", Alias = "OpenSansMedium")]
[assembly: ExportFont("OpenSans-Regular.ttf", Alias = "OpenSansRegular")]

/* Font Awesome Fonts */
[assembly: ExportFont("font_awesome_free_regular.otf", Alias = "FontAwesomeRegular")]
[assembly: ExportFont("font_awesome_free_solid.otf", Alias = "FontAwesomeSolid")]
