﻿using System;
using System.Text.RegularExpressions;

namespace solarcrm.Helpers
{
	public static class ValidationHelper
	{
        public static bool IsValidEmail(string email)
        {
            string expresion = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            Regex reg = new Regex(expresion, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(email);
        }

        public static bool IsValidPhoneNumber(string phoneNumber)
        {
            if (Regex.Match(phoneNumber, @"^(\d{10})$").Success)
                return true;
            else
                return false;
        }

    }
}

