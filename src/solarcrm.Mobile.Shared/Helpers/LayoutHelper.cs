﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace solarcrm.Helpers
{
	public static class LayoutHelper
	{
		static LayoutHelper()
		{
		}

        public static void ContentPageInit(ContentPage page)
        {
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(page, false);
            if (Device.RuntimePlatform == Device.iOS)
            {
                page.On<iOS>().SetUseSafeArea(true);
            }
            page.Padding = (Thickness)App.Current.Resources["SafeAreaPadding"];
            page.BackgroundColor = Color.White;
        }

        public static void Init()
        {
            App.Current.Resources["SafeAreaPadding"] = new Thickness(0, 0, 0, 0);

            if (Device.RuntimePlatform == Device.iOS && DeviceInfo.Version < Version.Parse("11.0"))
            {
                App.Current.Resources["SafeAreaPadding"] = new Thickness(0, 20, 0, 0);
            }
        }
    }
}

