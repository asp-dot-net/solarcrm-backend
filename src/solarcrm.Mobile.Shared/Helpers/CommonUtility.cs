﻿using System;
using System.Diagnostics;
using System.IO;
using solarcrm.Localization.Resources;
using Xamarin.Essentials;

namespace solarcrm.Helpers
{
	public static class CommonUtility
	{
		public static async void DocumentPicker()
		{
            try
            {
				var action = await App.Current.MainPage.DisplayActionSheet(null, LocalTranslation.Cancel, null, LocalTranslation.FromCamera, LocalTranslation.FromGallery);

				if (action == LocalTranslation.FromCamera)
				{
					var cameraPermissionStatus = await Permissions.CheckStatusAsync<Permissions.Camera>();
					var storageWritePermissionStatus = await Permissions.CheckStatusAsync<Permissions.StorageWrite>();
					var storageReadPermissionStatus = await Permissions.CheckStatusAsync<Permissions.StorageRead>();
					var PhotoPath = "";

					var photo = await MediaPicker.CapturePhotoAsync();
					if (photo == null)
					{
						PhotoPath = null;
						return;
					}
					// save the file into local storage
					var newFile = Path.Combine(FileSystem.CacheDirectory, photo.FileName);
					using (var stream = await photo.OpenReadAsync())
					using (var newStream = File.OpenWrite(newFile))
						await stream.CopyToAsync(newStream);

					PhotoPath = newFile;
				}
			}
			catch (Exception ex)
            {
				Debug.WriteLine(ex.Message);	
            }
		}
	}

}

