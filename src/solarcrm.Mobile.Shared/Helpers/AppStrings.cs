﻿using System;
using solarcrm.Localization.Resources;

namespace solarcrm.Helpers
{
	public class AppStrings
	{
		public static string ORText
		{
            get { return LocalTranslation.OR.ToUpper(); }
		}

		public static string CurrentTenantText
		{
			get { return LocalTranslation.CurrentTenant; }
		}

		public static string SwitchTenantText
		{
			get { return LocalTranslation.SwitchTenant; }
		}

		public static string EmailAddressText
		{
			get { return LocalTranslation.EmailAddress; }
		}

		public static string PasswordText
		{
			get { return LocalTranslation.Password; }
		}

		public static string LoginText
		{
			get { return LocalTranslation.Login; }
		}

		public static string HomeText
		{
			get { return LocalTranslation.Home; }
		}

		public static string InvoiceText
		{
			get { return LocalTranslation.Invoice; }
		}

		public static string ProjectText
		{
			get { return LocalTranslation.Project; }
		}

		public static string AccountText
		{
			get { return LocalTranslation.Account; }
		}

		public static string DashboardText
		{
			get { return LocalTranslation.Dashboard; }
		}

		public static string CustomerText
		{
			get { return LocalTranslation.Customer; }
		}

		public static string CustomersText
		{
			get { return LocalTranslation.Customers; }
		}

		public static string CommisionClaimText
		{
			get { return LocalTranslation.CommisionClaim; }
		}

		public static string UploadDocumentsText
		{
			get { return LocalTranslation.UploadDocuments; }
		}

		public static string SettingsText
		{
			get { return LocalTranslation.Settings; }
		}

		public static string LogoutText
		{
			get { return LocalTranslation.Logout; }
		}

		public static string TrackerText
		{
			get { return LocalTranslation.Tracker; }
		}

		public static string QuotationGivenText
		{
			get { return LocalTranslation.QuotationGiven; }
		}

		public static string CancelledText
		{
			get { return LocalTranslation.Cancelled; }
		}

		public static string CancelText
		{
			get { return LocalTranslation.Cancel; }
		}

		public static string ApplicationSubmittedText
		{
			get { return LocalTranslation.ApplicationSubmitted; }
		}

		public static string DocumentVerifiedText
		{
			get { return LocalTranslation.DocumentVerified; }
		}

		public static string SummaryText
		{
			get { return LocalTranslation.Summary; }
		}

		public static string ActivityText
		{
			get { return LocalTranslation.Activity; }
		}

		public static string DocumentsText
		{
			get { return LocalTranslation.Documents; }
		}

		public static string DocText
		{
			get { return LocalTranslation.Doc; }
		}

		public static string UploadedAtText
		{
			get { return LocalTranslation.UploadedAt; }
		}

		public static string DocumentNameText
		{
			get { return LocalTranslation.DocumentName; }
		}

		public static string DocumentNumberText
		{
			get { return LocalTranslation.DocumentNumber; }
		}

		public static string UploadFileText
		{
			get { return LocalTranslation.UploadFileHere; }
		}

		public static string EnterDocNumberText
		{
			get { return LocalTranslation.EnterDocNumber; }
		}

		public static string SubmitText
		{
			get { return LocalTranslation.Submit; }
		}

		public static string SelectDocumentText
		{
			get { return LocalTranslation.SelectDocument; }
		}

		public static string LoadMoreText
		{
			get { return LocalTranslation.LoadMore; }
		}

		public static string AddText
		{
			get { return LocalTranslation.Add; }
		}

		public static string AllActivityText
		{
			get { return LocalTranslation.AllActivity; }
		}

		public static string NoCustomerFoundText
		{
			get { return LocalTranslation.NoCustomerFound; }
		}

		public static string TechnicalDetailsText
		{
			get { return LocalTranslation.TechnicalDetails; }
		}

		public static string AddActivityText
		{
			get { return LocalTranslation.AddActivity; }
		}

		public static string ReminderToText
		{
			get { return LocalTranslation.ReminderTo; }
		}

		public static string AddNoteText
		{
			get { return LocalTranslation.AddNote; }
		}

		public static string SetText
		{
			get { return LocalTranslation.Set; }
		}

		public static string SmsToText
		{
			get { return LocalTranslation.SMSTo; }
		}

		public static string TemplateText
		{
			get { return LocalTranslation.Template; }
		}

		public static string MessageText
		{
			get { return LocalTranslation.Message; }
		}

		public static string TotalText
		{
			get { return LocalTranslation.Total; }
		}

		public static string CreditText
		{
			get { return LocalTranslation.Credit; }
		}

		public static string SendText
		{
			get { return LocalTranslation.Send; }
		}

		public static string DateAndTimeText
		{
			get { return LocalTranslation.DateAndTime; }
		}

		public static string CreateCustomerText
		{
			get { return LocalTranslation.CreateCustomer; }
		}

		public static string AddressText
		{
			get { return LocalTranslation.Address; }
		}

		public static string ApplicationText
		{
			get { return LocalTranslation.Application; }
		}

		public static string LeadText
		{
			get { return LocalTranslation.Lead; }
		}

		public static string BankText
		{
			get { return LocalTranslation.Bank; }
		}

		public static string AddCustomerSuccessMessageText
		{
			get { return LocalTranslation.AddCustomerSuccessMessage; }
		}

		public static string SaveAndContinueText
		{
			get { return LocalTranslation.SaveAndContinue; }
		}

		public static string BackText
		{
			get { return LocalTranslation.Back; }
		}

		public static string SolarTypeText
		{
			get { return LocalTranslation.SolarType; }
		}

		public static string ConsumerNo
		{
			get { return LocalTranslation.ConsumerNo; }
		}

		public static string NameText
		{
			get { return LocalTranslation.Name; }
		}

		public static string FirstNameText
		{
			get { return LocalTranslation.FirstName; }
		}

		public static string MiddleNameText
		{
			get { return LocalTranslation.MiddleName; }
		}

		public static string LastNameText
		{
			get { return LocalTranslation.LastName; }
		}

		public static string CustomerNameText
		{
			get { return LocalTranslation.CustomerName; }
		}

		public static string MobileText
		{
			get { return LocalTranslation.Mobile; }
		}

		public static string EmailText
		{
			get { return LocalTranslation.Email; }
		}

		public static string AltContactNoText
		{
			get { return LocalTranslation.AltContactNo; }
		}

		public static string PleaseInputText
		{
			get { return LocalTranslation.PleaseInput; }
		}

		public static string OkText
		{
			get { return LocalTranslation.Ok.ToUpper(); }
		}

		public static string ErrorText
		{
			get { return LocalTranslation.Error; }
		}

		public static string RequiredFieldMessageText
		{
			get { return LocalTranslation.RequiredFieldMessage; }
		}

		public static string CustomerNoRequiredMsg
		{
			get { return LocalTranslation.CustomerNoRequiredMsg; }
		}

		public static string CustomerNameRequiredMsg
		{
			get { return LocalTranslation.CustomerNameRequiredMsg; }
		}

		public static string FirstNameRequiredText
		{
			get { return LocalTranslation.FirtNameRequiredMsg; }
		}

		public static string MiddleNameRequiredText
		{
			get { return LocalTranslation.MiddleNameRequiredMsg; }
		}

		public static string LastNameRequiredText
		{
			get { return LocalTranslation.LastNameRequiredMsg; }
		}

		public static string MobileNoRequiredText
		{
			get { return LocalTranslation.MobileRequiredMsg; }
		}

		public static string MobileNoErrorMsg
		{
			get { return LocalTranslation.MobileErrorMsg; }
		}

		public static string EmailErrorMsg
		{
			get { return LocalTranslation.EmailErrorMsg; }
		}

		public static string SolarTypeErrorMsg
		{
			get { return LocalTranslation.SolarTypeErrorMsg; }
		}

		public static string Address1Text
		{
			get { return LocalTranslation.Address1; }
		}

		public static string Address2Text
		{
			get { return LocalTranslation.Address2; }
		}

		public static string CityText
		{
			get { return LocalTranslation.City; }
		}

		public static string TalukaText
		{
			get { return LocalTranslation.Taluka; }
		}

		public static string DistrictText
		{
			get { return LocalTranslation.District; }
		}

		public static string StateText
		{
			get { return LocalTranslation.State; }
		}

		public static string PostcodeText
		{
			get { return LocalTranslation.Postcode; }
		}

		public static string SubDivision
		{
			get { return LocalTranslation.SubDivision; }
		}

		public static string Division
		{
			get { return LocalTranslation.Division; }
		}

		public static string Circle
		{
			get { return LocalTranslation.Circle; }
		}

		public static string Discom
		{
			get { return LocalTranslation.Discom; }
		}

		public static string Latitude
		{
			get { return LocalTranslation.Latitude; }
		}

		public static string Longitude
		{
			get { return LocalTranslation.Longitude; }
		}

		public static string RequiredCapacity
		{
			get { return LocalTranslation.RequiredCapacity; }
		}

		public static string StructureAmount
		{
			get { return LocalTranslation.StructureAmount; }
		}

		public static string AverageMonthlyUnit
		{
			get { return LocalTranslation.AverageMonthlyUnit; }
		}

		public static string AverageMonthlyBill
		{
			get { return LocalTranslation.AverageMonthlyBill; }
		}

		public static string Category
		{
			get { return LocalTranslation.Category; }
		}

		public static string HeightStructure
		{
			get { return LocalTranslation.HeightStructure; }
		}

		public static string SelectRequiredFieldMsg
		{
			get { return LocalTranslation.SelectRequiredFieldMsg; }
		}

		public static string EnterRequiredFieldMsg
		{
			get { return LocalTranslation.EnterRequiredFieldMsg; }
		}

		public static string LeadSource
		{
			get { return LocalTranslation.LeadSource; }
		}

		public static string LeadType
		{
			get { return LocalTranslation.LeadType; }
		}

		public static string LeadStatus
		{
			get { return LocalTranslation.LeadStatus; }
		}

		public static string InquiryType
		{
			get { return LocalTranslation.InquiryType; }
		}

		public static string CommonMeterConnection
		{
			get { return LocalTranslation.CommonMeterConnection; }
		}

		public static string AreaType
		{
			get { return LocalTranslation.AreaType; }
		}

		public static string ChannelsPartners
		{
			get { return LocalTranslation.ChannelsPartners; }
		}

		public static string BankAccountNumber
		{
			get { return LocalTranslation.BankAccountNumber; }
		}

		public static string AccountHolderName
		{
			get { return LocalTranslation.AccountHolderName; }
		}

		public static string BankName
		{
			get { return LocalTranslation.BankName; }
		}

		public static string BranchName
		{
			get { return LocalTranslation.BranchName; }
		}

		public static string IFSCCode
		{
			get { return LocalTranslation.IFSCCode; }
		}

		public static string AdditionalNotes
		{
			get { return LocalTranslation.AdditionalNotes; }
		}

		public static string AdditionalNotesPlaceholder
		{
			get { return LocalTranslation.AdditionalNotesPlaceholder; }
		}

		public static string Projects
		{
			get { return LocalTranslation.Projects; }
		}

		public static string UpdatedBy
		{
			get { return LocalTranslation.UpdatedBy; }
		}

		public static string Created
		{
			get { return LocalTranslation.Created; }
		}

		public static string Detail
		{
			get { return LocalTranslation.Detail; }
		}

		public static string Sales
		{
			get { return LocalTranslation.Sales; }
		}

		public static string Quotes
		{
			get { return LocalTranslation.Quotes; }
		}

		public static string ProjectNo
		{
			get { return LocalTranslation.ProjectNo; }
		}

		public static string ProjectQuoteDate
		{
			get { return LocalTranslation.ProjectQuoteDate; }
		}

		public static string PanelDetails
		{
			get { return LocalTranslation.PanelDetails; }
		}

		public static string ActualCost
		{
			get { return LocalTranslation.ActualCost; }
		}

		public static string InverterDetails
		{
			get { return LocalTranslation.InverterDetails; }
		}

		public static string PricingDetails
		{
			get { return LocalTranslation.PricingDetails; }
		}

		public static string QuoteDetails
		{
			get { return LocalTranslation.QuoteDetails; }
		}

		public static string MaxSubsidy
		{
			get { return LocalTranslation.MaxSubsidy; }
		}

		public static string MinSubsidy
		{
			get { return LocalTranslation.MinSubsidy; }
		}

		public static string ActualSubsidyAmt
		{
			get { return LocalTranslation.ActualSubsidyAmt; }
		}

		public static string TotalPayableAmt
		{
			get { return LocalTranslation.TotalPayableAmt; }
		}

		public static string DepositeAmount
		{
			get { return LocalTranslation.DepositeAmount; }
		}

		public static string SystemStrengtheingCost
		{
			get { return LocalTranslation.SystemStrengtheingCost; }
		}

		public static string MeterCharges
		{
			get { return LocalTranslation.MeterCharges; }
		}

		public static string NetTotal
		{
			get { return LocalTranslation.NetTotal; }
		}

		public static string UpdatedPrice
		{
			get { return LocalTranslation.UpdatePrice; }
		}

		public static string AgreementCharges
		{
			get { return LocalTranslation.AgreementCharges; }
		}

		public static string NetMeterCharges
		{
			get { return LocalTranslation.NetMeterCharges; }
		}

		public static string Update
		{
			get { return LocalTranslation.Update; }
		}
	}
}

