﻿using System;
using System.Collections.Generic;
using solarcrm.Helpers;
using Xamarin.Forms;

namespace solarcrm.Views.CreateCustomer
{	
	public partial class CreateCustomerView : ContentPage, IXamarinView
	{	
		public CreateCustomerView ()
		{
			LayoutHelper.ContentPageInit(this);
			InitializeComponent ();
		}
	}
}

