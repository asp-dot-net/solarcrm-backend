﻿using System;
using System.Collections.Generic;
using solarcrm.ViewModels.CreateCustomer;
using Xamarin.Forms;

namespace solarcrm.Views.CreateCustomer.Components
{	
	public partial class BasicInfoTabView : ContentView
	{	
		public BasicInfoTabView ()
		{
			InitializeComponent ();

            consumerNumber.Unfocused += ConsumerNumber_Unfocused;
		}

        private void ConsumerNumber_Unfocused(object sender, FocusEventArgs e)
        {
            int value =Convert.ToInt16(((Entry)sender).Text);
            CreateCustomerViewModel.CheckConsumerNoIsDuplicateEvent.Invoke(this, value);
        }
    }
}

