﻿using System;
using System.Collections.Generic;
using solarcrm.ViewModels.Base;
using Xamarin.Forms;

namespace solarcrm.Views
{	
	public partial class FooterView : ContentView
	{	
		public FooterView ()
		{
			InitializeComponent ();
		}

        public static BindableProperty SelectedPageProperty =
           BindableProperty.Create(nameof(SelectedPage), typeof(string), typeof(FooterView),
                                   default(string), BindingMode.TwoWay, propertyChanged: OnSelectedPageChanged);

        public string SelectedPage
        {
            get
            {
                return (string)this.GetValue(SelectedPageProperty);
            }
            set
            {
                this.SetValue(SelectedPageProperty, value);
            }
        }

        private static void OnSelectedPageChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var picker = bindable as FooterView;
            picker.SelectedPage = (string)newvalue;

            picker.SetTheme();
        }

        public void SetTheme()
        {
            HomeTabLbl.TextColor = (Color)App.Current.Resources["BlackColor"];
            ProjTabLbl.TextColor = (Color)App.Current.Resources["BlackColor"];
            InvoiceTabLbl.TextColor = (Color)App.Current.Resources["BlackColor"];
            UploadTabLbl.TextColor = (Color)App.Current.Resources["BlackColor"];

            HomeTabIcon.Source = "Home";
            ProjTabIcon.Source = "project";
            InvoiceTabIcon.Source = "invoice";
            UploadTabIcon.Source = "account";

            if (SelectedPage.ToString() == "HOME")
            {
                HomeTabIcon.Source = "Home_active";
                HomeTabLbl.TextColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
            }
            else if (SelectedPage.ToString() == "PROJECTS")
            {
                ProjTabIcon.Source = "project_active";
                ProjTabLbl.TextColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
            }
            else if (SelectedPage.ToString() == "INVOICE")
            {
                InvoiceTabIcon.Source = "invoice_active";
                InvoiceTabLbl.TextColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
            }
            else if(SelectedPage.ToString() == "UPLOAD")
            {
                UploadTabIcon.Source = "upload_docs";
                UploadTabLbl.TextColor = (Color)App.Current.Resources["ButtonBackgroundColor"];
            }
        }

        void NavigationTapped(object sender, EventArgs e)
        {
            var args = e as TappedEventArgs;
            var context = ((XamarinViewModel)(this.BindingContext));
            context.NavigationToPage(args.Parameter.ToString());
        }
    }
}

