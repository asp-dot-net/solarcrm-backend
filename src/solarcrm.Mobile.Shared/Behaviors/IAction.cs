﻿using Xamarin.Forms.Internals;

namespace solarcrm.Behaviors
{
    [Preserve(AllMembers = true)]
    public interface IAction
    {
        bool Execute(object sender, object parameter);
    }
}